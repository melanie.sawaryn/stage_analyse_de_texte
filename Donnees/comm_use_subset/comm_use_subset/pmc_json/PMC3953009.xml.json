{
    "paper_id": "PMC3953009",
    "metadata": {
        "title": "Congenital Malaria in China",
        "authors": [
            {
                "first": "Zhi-yong",
                "middle": [],
                "last": "Tao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Qiang",
                "middle": [],
                "last": "Fang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xue",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Richard",
                "middle": [],
                "last": "Culleton",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Li",
                "middle": [],
                "last": "Tao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hui",
                "middle": [],
                "last": "Xia",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Qi",
                "middle": [],
                "last": "Gao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kenji",
                "middle": [],
                "last": "Hirayama",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Malaria is a mosquito-borne infectious parasitic disease that is prevalent in tropical and subtropical areas. It is estimated that 660,000 lives were lost to the disease in 2010, mostly in sub-Saharan Africa, a region hyperendemic for P. falciparum, the most virulent of the species that cause malaria in humans. Over 86% of malarial deaths in this area were of children under 5 years of age [1], [2]. In China, P. vivax is the most prevalent species of malaria parasite [3]. Following a dedicated malaria control effort, the incidence of malaria in China has been reduced to just 0.0334 cases per 10,000 people (data from 2011) [4]. In 2010, a national elimination programme, the \u201cAction Plan of China Malaria Elimination (2010\u20132020),\u201d was initiated [5], [6]. Despite this, there are continued outbreaks of P. vivax malaria in the central region of the country, where unstable transmission of the parasite remains [7]. Congenital malaria due to P. falciparum is reportedly rare in hyperendemic areas, where pregnant woman can pass on antibodies against the parasite to their newborn babies. Thus, even though maternal asymptomatic malaria is common, congenital malaria is not [8]\u2013[11]. However, recent reports suggest that congenital P. falciparum malaria is not as rare as previously believed [12]\u2013[16]. Whilst there are differing opinions regarding the incidences of congenital malaria, the effects on neonates of this prenatal disease are clear [17]. In low P. vivax-endemic regions, immunity to vivax malaria is also low, and, thus, there is the possibility that congenital vivax malaria poses a significant threat to the health of the foetus [18]. During the long battle against malaria, numerous congenital malaria cases were reported in the Chinese medical literature. However, because of language barriers, this valuable information has not been accessible to the malaria research community outside of China. Here, we review congenital malaria cases from three major publicly searchable Chinese databases, concentrating on data from 1915 through 2011 [19], [20].",
            "cite_spans": [
                {
                    "start": 392,
                    "end": 395,
                    "mention": "[1]",
                    "ref_id": "BIBREF31"
                },
                {
                    "start": 397,
                    "end": 400,
                    "mention": "[2]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 471,
                    "end": 474,
                    "mention": "[3]",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 629,
                    "end": 632,
                    "mention": "[4]",
                    "ref_id": "BIBREF33"
                },
                {
                    "start": 751,
                    "end": 754,
                    "mention": "[5]",
                    "ref_id": "BIBREF27"
                },
                {
                    "start": 756,
                    "end": 759,
                    "mention": "[6]",
                    "ref_id": "BIBREF32"
                },
                {
                    "start": 915,
                    "end": 918,
                    "mention": "[7]",
                    "ref_id": "BIBREF38"
                },
                {
                    "start": 1177,
                    "end": 1180,
                    "mention": "[8]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1181,
                    "end": 1185,
                    "mention": "[11]",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 1295,
                    "end": 1299,
                    "mention": "[12]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1300,
                    "end": 1304,
                    "mention": "[16]",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 1449,
                    "end": 1453,
                    "mention": "[17]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1648,
                    "end": 1652,
                    "mention": "[18]",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 2060,
                    "end": 2064,
                    "mention": "[19]",
                    "ref_id": "BIBREF28"
                },
                {
                    "start": 2066,
                    "end": 2070,
                    "mention": "[20]",
                    "ref_id": "BIBREF34"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "We examined all published cases of congenital malaria between 1915 and May 2011 in the Chinese literature electronically available in the CNKI (China National Knowledge Infrastructure, www.cnki.net), Wanfang Data (www.wanfangdata.com), and CSTJ (China Science and Technology Journal Database, www.cqvip.com) databases [20]. The keywords for the literature search strategy were \u201ccongenital OR neonatal AND malaria\u201d. All pertinent case reports were downloaded and analysed in the context of the epidemiology of the disease in China. The criteria used to diagnose congenital malaria were: (1) whether the time of the neonate's onset of malaria symptoms was within 7 days of delivery; (2) in malaria nonendemic areas, if the onset of symptoms was within 30 days of birth; (3) in malaria endemic areas, if the onset of symptoms was within 30 days of delivery (with a corresponding diagnosis of malaria in the mother at the time of birth); and (4) if the onset of both the neonate's and the mother's symptoms and the time of delivery were during the nontransmission season (and therefore due to relapse caused by activated hypnozoites in the mother). Cases of congenital malaria reported in English were also included [21]. SPSS 14.0 (SPSS Inc., United States) was used for statistical analysis.",
            "cite_spans": [
                {
                    "start": 318,
                    "end": 322,
                    "mention": "[20]",
                    "ref_id": "BIBREF34"
                },
                {
                    "start": 1212,
                    "end": 1216,
                    "mention": "[21]",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "In this review, through the collection and interpretation of data originating between 1915 and 2011 and deposited in electronic periodical databases, there were 103 congenital malaria cases reported by 61 medical articles in Chinese and one case occurring in mainland China reported in English [21] that met the requirements of the diagnostic criteria described above. The earliest report dates from 1963 and the latest was from 2010. The majority of the reports (81.73%) were published in the 1980s and 1990s. Congenital malaria mainly occurred in the eastern, central, and southern regions of China, and also in the low-elevation area in southwest China (cases in these areas account for 95.19%). The provinces with the highest incidences were the Anhui (19 cases, 18.29%), Jiangsu (15 cases, 14.42%), and Hubei (13 cases, 12.50%) provinces in the river valley sections of the Yangtze and Huaihe River (Figure 1).",
            "cite_spans": [
                {
                    "start": 294,
                    "end": 298,
                    "mention": "[21]",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "Temporal and spatial distribution of congenital malaria ::: Results",
            "ref_spans": [
                {
                    "start": 905,
                    "end": 913,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Of the 104 cases of congenital malaria, 80 clearly identified the species of malaria parasite involved. The predominant species was P. vivax (74 cases, 92.50%), followed by P. falciparum (five cases, 6.25%) and P. malariae (one case, 1.25%). All diagnoses were performed by microscopy. In 85 cases, parasites were found initially in peripheral blood films, while in nine cases, parasites were initially detected in bone marrow smears. For the remaining ten case reports, there were no descriptions of which material had been used for diagnosis.",
            "cite_spans": [],
            "section": "Laboratory diagnosis ::: Results",
            "ref_spans": []
        },
        {
            "text": "Of the 104 cases of congenital malaria, 96 reported clinical symptoms (Figure 2), of which 86 (89.58%) described fever. In this fever group, 78 had fever as the main presentation; only 27 described intermittent fever. The other clinical presentations were anaemia (33 cases, 34.38%), jaundice (27 cases, 28.13%), paleness (21 cases, 21.88%), diarrhoea (six cases, 6.25%), vomiting (five cases, 5.21%), weakness and inactivity (five cases, 5.21%), shivers (four cases, 4.17%), and abdominal distention (four cases, 4.17%). Among the 60 cases of congenital malaria in which liver palpation results were described, 57 cases had a liver that was palpable below the right costal margin. Of the 51 cases in which the enlarged liver was measured, 41 infants had livers enlarged with a span of \u22652 cm below the right costal margin. In addition, four infants were reported to be hepatomegalous, but the exact measurements were not given. Spleen palpations were recorded in 63 cases. Of these, 54 cases were palpable below the left costal margin. 40 cases of enlarged spleen were measured, of which 35 cases presented enlargement \u22652 cm; an additional nine cases were described only as splenomegaly.",
            "cite_spans": [],
            "section": "Case presentation ::: Results",
            "ref_spans": [
                {
                    "start": 71,
                    "end": 79,
                    "mention": "Figure 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Of 59 infants with congenital malaria for whom haemoglobin concentrations (Hb) were recorded, 51 had Hb<120 g/L, 43 had Hb<90 g/L, 26 had Hb<60 g/L and four had Hb<30 g/L. Thirteen of the 20 infants with congenital malaria for whom BPCs (blood platelet counts) were performed had thrombocytopenia (PLTs [platelets])<100\u00d7109/L. Of 53 infants with congenital malaria for whom WBC (white blood cell) counts were performed, ten infants had WBC>12.5\u00d7109/L.",
            "cite_spans": [],
            "section": "Laboratory examination ::: Results",
            "ref_spans": []
        },
        {
            "text": "The 88 cases of congenital malaria in newborns whose ages at the onset of symptoms were recorded ranged from 1 to 50 days (median, 8 days). In those cases in which there was a clear diagnosis of malaria (n = 67), the age distribution was 4 to 126 days (median, 30 days). The time interval from the symptom onset to diagnosis ranged from 1 to 111 days (median, 15 days).",
            "cite_spans": [],
            "section": "Diagnosis ::: Results",
            "ref_spans": []
        },
        {
            "text": "Of 86 women with maternal history documented and newborns diagnosed as having congenital malaria, all had at least one record of malaria. In five cases, the malaria occurred during their first trimester of pregnancy, 20 cases had malaria in the second trimester, 17 during their third trimester, 46 during prenatal, and four cases were unspecified. Eight women had recorded malaria cases prior to their pregnancy (five of which occurred only before their pregnancy). Three women had no recorded malaria history.",
            "cite_spans": [],
            "section": "Maternal history ::: Results",
            "ref_spans": []
        },
        {
            "text": "Of the 104 infants with congenital malaria, 102 were cured with one or two types of drugs after receiving antimalarial treatment (Figure 3). Sixty-three were treated with chloroquine, four were treated with artemether, two were treated with artesunate, nine were treated with quinine, one was treated with pyrimethamine, one was treated with cycloguanil, and 28 were treated with unspecified antimalarial drug(s). In addition, nine infants received primaquine along with an antimalarial drug related to those described above. However, one infant's family refused and halted treatment, and another infant was too ill and died two hours after diagnosis.",
            "cite_spans": [],
            "section": "Treatment and results ::: Results",
            "ref_spans": [
                {
                    "start": 130,
                    "end": 138,
                    "mention": "Figure 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "As China is a country with a large population spread over a vast geographical area, health care and economic and social development levels differ greatly, especially between urban and rural areas. Usually, malaria-endemic areas in China occur in underdeveloped and more remote rural areas with very limited resources; these facts mean that medical personnel should pay more attention to the possibility of congenital malaria in order to ensure children's health and well-being.",
            "cite_spans": [],
            "section": "Conclusions ::: Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Abbreviations of affected provinces: AH: Anhui; CQ: Chongqin; GS: Gansu; GX: Guangxi; GD: Guangdong; HB: Hubei; HLJ: Heilongjiang; HN1: Hunan; HN2: Hainan; HN3: Henan; JS: Jiangsu; JX: Jiangxi; SC: Sichuan; SD: Shandong; XZ: Xizang; YN: Yunnan; ZJ: Zhejiang.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Scant parasitemia in BALB/c mice with congenital malaria infection",
            "authors": [],
            "year": 2000,
            "venue": "J Parasitol",
            "volume": "86",
            "issn": "",
            "pages": "1030-1034",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "China finally throws full weight behind efforts to contain SARS",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Epidemiology and burden of malaria in pregnancy",
            "authors": [],
            "year": 2007,
            "venue": "Lancet Infect Dis",
            "volume": "7",
            "issn": "",
            "pages": "93-104",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Application of the indirect fluorescent antibody assay in the study of malaria infection in the Yangtze River Three Gorges Reservoir, China",
            "authors": [],
            "year": 2009,
            "venue": "Malar J",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Estimates of child deaths prevented from malaria prevention scale-up in Africa 2001\u20132010",
            "authors": [],
            "year": 2012,
            "venue": "Malar J",
            "volume": "11",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Congenital malaria: An African survey",
            "authors": [],
            "year": 1997,
            "venue": "Clin Pediatr (Phila)",
            "volume": "36",
            "issn": "",
            "pages": "411-413",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "The effect of China's one-child family policy after 25 years",
            "authors": [],
            "year": 2005,
            "venue": "N Engl J Med",
            "volume": "353",
            "issn": "",
            "pages": "1171-1176",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "An Accurate and Cost Effective Approach to Blood Cell Count",
            "authors": [],
            "year": 2012,
            "venue": "Int J Comput Appl",
            "volume": "50",
            "issn": "",
            "pages": "18-24",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "A study of malaria parasitaemia in pregnant women, placentae, cord blood and newborn babies in Lagos, Nigeria",
            "authors": [],
            "year": 1993,
            "venue": "West Afr J Med",
            "volume": "12",
            "issn": "",
            "pages": "213-217",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Congenital malaria in a hyperendemic area",
            "authors": [],
            "year": 1991,
            "venue": "Am J Trop Med Hyg",
            "volume": "45",
            "issn": "",
            "pages": "587-592",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Congenital malaria in the United States: A review of cases from 1966 to 2005",
            "authors": [],
            "year": 2007,
            "venue": "Arch Pediatr Adolesc Med",
            "volume": "161",
            "issn": "",
            "pages": "1062-1067",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Major trends in human parasitic diseases in China",
            "authors": [],
            "year": 2010,
            "venue": "Trends Parasitol",
            "volume": "26",
            "issn": "",
            "pages": "264-270",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Effect of pregnancy on exposure to malaria mosquitoes",
            "authors": [],
            "year": 2000,
            "venue": "Lancet",
            "volume": "355",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "A case of congenital plasmodium vivax malaria from a temperate region in Central China",
            "authors": [],
            "year": 2012,
            "venue": "Malar J",
            "volume": "11",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Congenital malaria: The least known consequence of malaria in pregnancy",
            "authors": [],
            "year": 2007,
            "venue": "Semin Fetal Neonatal Med",
            "volume": "12",
            "issn": "",
            "pages": "207-213",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Accuracy of malaria diagnosis by microscopy, rapid diagnostic test, and PCR methods and evidence of antimalarial overprescription in non-severe febrile patients in two Tanzanian hospitals",
            "authors": [],
            "year": 2009,
            "venue": "Am J Trop Med Hyg",
            "volume": "80",
            "issn": "",
            "pages": "712-717",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Prevalence of congenital malaria in Ile-ife, Nigeria",
            "authors": [],
            "year": 2005,
            "venue": "J Trop Pediatr",
            "volume": "51",
            "issn": "",
            "pages": "219-222",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "Transplacental transmission of Plasmodium falciparum in a highly malaria endemic area of Burkina Faso",
            "authors": [],
            "year": 2012,
            "venue": "J Trop Med",
            "volume": "2012",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "Congenital malaria in Urab\u00e1, Colombia",
            "authors": [],
            "year": 2011,
            "venue": "Malar J",
            "volume": "10",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF19": {
            "title": "[Prevention and control of malaria in China, in last 50 years]",
            "authors": [],
            "year": 2000,
            "venue": "[Article in Chinese] Zhonghua Liu Xing Bing Xue Za Zhi",
            "volume": "21",
            "issn": "",
            "pages": "225-227",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF20": {
            "title": "Malaria in pregnancy: Pathogenesis and immunity",
            "authors": [],
            "year": 2007,
            "venue": "Lancet Infect Dis",
            "volume": "7",
            "issn": "",
            "pages": "105-117",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF21": {
            "title": "Design of malaria diagnostic criteria for the Sysmex XE-2100 hematology analyzer",
            "authors": [],
            "year": 2010,
            "venue": "Am J Trop Med Hyg",
            "volume": "82",
            "issn": "",
            "pages": "402-411",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF22": {
            "title": "Neonatal malaria in Nigeria\u2014A 2\u2009year review",
            "authors": [],
            "year": 2006,
            "venue": "BMC Pediatr",
            "volume": "6",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF23": {
            "title": "China's medical periodicals: From localization to internationalization",
            "authors": [],
            "year": 2010,
            "venue": "Learn Publ",
            "volume": "23",
            "issn": "",
            "pages": "303-311",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF24": {
            "title": "Resurgence of vivax malaria in Henan Province, China",
            "authors": [],
            "year": 1998,
            "venue": "Bull World Health Organ",
            "volume": "76",
            "issn": "",
            "pages": "265-270",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF25": {
            "title": "Impact of asymptomatic maternal malaria parasitaemia at parturition on perinatal outcome",
            "authors": [],
            "year": 2002,
            "venue": "J Obstet Gynaecol",
            "volume": "22",
            "issn": "",
            "pages": "25-28",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF26": {
            "title": "[Achievements in the research on the prevention and treatment of malaria in China]",
            "authors": [],
            "year": 1999,
            "venue": "[Article in Chinese] Zhongguo Ji Sheng Chong Xue Yu Ji Sheng Chong Bing Za Zhi",
            "volume": "17",
            "issn": "",
            "pages": "257-259",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF27": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF28": {
            "title": "The discovery of artemisinin (qinghaosu) and gifts from Chinese medicine",
            "authors": [],
            "year": 2011,
            "venue": "Nat Med",
            "volume": "17",
            "issn": "",
            "pages": "1217-1220",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF29": {
            "title": "Emergence and control of infectious diseases in China",
            "authors": [],
            "year": 2008,
            "venue": "Lancet",
            "volume": "372",
            "issn": "",
            "pages": "1598-1605",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF30": {
            "title": "Malaria rapid diagnostic tests",
            "authors": [],
            "year": 2012,
            "venue": "Clin Infect Dis",
            "volume": "54",
            "issn": "",
            "pages": "1637-1641",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF31": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF32": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF33": {
            "title": "[Malaria situation in the People's Republic of China in 2011]",
            "authors": [],
            "year": 2012,
            "venue": "[Article in Chinese] Zhongguo Ji Sheng Chong Xue Yu Ji Sheng Chong Bing Za Zhi",
            "volume": "30",
            "issn": "",
            "pages": "419-422",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF34": {
            "title": "Five large Chinese biomedical bibliographic databases: Accessibility and coverage",
            "authors": [],
            "year": 2008,
            "venue": "Health Info Libr J",
            "volume": "25",
            "issn": "",
            "pages": "55-61",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF35": {
            "title": "Malaria in Hubei Province, China: Approaching eradication",
            "authors": [],
            "year": 1994,
            "venue": "J Trop Med Hyg",
            "volume": "97",
            "issn": "",
            "pages": "277-281",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF36": {
            "title": "Malaria control in Henan Province, People's Republic of China",
            "authors": [],
            "year": 2006,
            "venue": "Am J Trop Med Hyg",
            "volume": "74",
            "issn": "",
            "pages": "564-567",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF37": {
            "title": "Performance of two rapid diagnostic tests for malaria diagnosis at the China-Myanmar border area",
            "authors": [],
            "year": 2013,
            "venue": "Malar J",
            "volume": "12",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF38": {
            "title": "Spatial-temporal analysis of malaria and the effect of environmental factors on its incidence in Yongcheng, China, 2006\u20132010",
            "authors": [],
            "year": 2012,
            "venue": "BMC Public Health",
            "volume": "12",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}