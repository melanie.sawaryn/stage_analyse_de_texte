{
    "paper_id": "PMC4976505",
    "metadata": {
        "title": "System effectiveness of detection, brief intervention and refer to treatment for the people with post-traumatic emotional distress by MERS: a case report of community-based proactive intervention in South Korea",
        "authors": [
            {
                "first": "Mi-Kyung",
                "middle": [],
                "last": "Yoon",
                "suffix": "",
                "email": "sara0314@hanmail.net",
                "affiliation": {}
            },
            {
                "first": "Soon-Young",
                "middle": [],
                "last": "Kim",
                "suffix": "",
                "email": "poshksy@naver.com",
                "affiliation": {}
            },
            {
                "first": "Hye-Sun",
                "middle": [],
                "last": "Ko",
                "suffix": "",
                "email": "blunt1225@hanmail.net",
                "affiliation": {}
            },
            {
                "first": "Myung-Soo",
                "middle": [],
                "last": "Lee",
                "suffix": "",
                "email": "mslee1010@gmail.com",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Gyeonggi Province in South Korea is a regional governmental body with a population of 12 million people; it is composed of 31 municipalities. Natural and social disasters of various severity have occurred in Gyeonggi Province; in 2014, 26.5 % of all natural disasters in Korea occurred in Gyeonggi Province [1, 2]. Among such disasters, the 2015 middle eastern respiratory syndrome (MERS) outbreak imposed great psychological stress on almost all Korean citizens. One hundred and eighty-six South Koreans were infected by MERS; 38 died and 146 recovered. A further, 16,752 were placed in quarantine [3]. In Gyeonggi Province, where the first MERS patient was discovered in Korea, 70 people were infected by MERS (37 % of Korean MERS patients), eight died (21 % of MERS deaths), and 11,318 were monitored or placed in quarantine (67 % of the quarantined population).",
            "cite_spans": [
                {
                    "start": 308,
                    "end": 309,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 311,
                    "end": 312,
                    "mention": "2",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 600,
                    "end": 601,
                    "mention": "3",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "People who recover from infectious diseases with a high mortality rate such as MERS, often show emotional instability resulting from the experience of facing death [4]. Family members of people killed by such diseases also often become depressed following the sudden loss [5]. Risk communication regarding infectious diseases with a high mortality rate tends to arouse public anxiety and fear [6, 7]. The National Disaster Management Institute examined South Koreans\u2019 emotional responses on social networking services during the MERS outbreak; those responses were as follows: despair during the first 9 days of the outbreak, anxiety on the 15th\u201319th days, and anger on the 20th\u201331st days [8]. Over a quarter of Hong Kong residents who experienced the severe acute respiratory syndrome (SARS) outbreak showed symptoms of chronic fatigue syndrome (27.1 %); 42.5 % reported experiencing psychological problems 4 years after the outbreak [9]. Patients infected by the Ebola virus also experienced post-traumatic stress disorder, depression, anxiety disorder and survivor guilt [10]. Families and friends whose loved ones were killed by the Ebola virus disease experienced psychological problems, including depression, sleep disturbances, abnormal behavior and post-traumatic stress symptoms, additionally, their use of alcohol and nicotine increased following the loss [11].",
            "cite_spans": [
                {
                    "start": 165,
                    "end": 166,
                    "mention": "4",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 273,
                    "end": 274,
                    "mention": "5",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 394,
                    "end": 395,
                    "mention": "6",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 397,
                    "end": 398,
                    "mention": "7",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 690,
                    "end": 691,
                    "mention": "8",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 936,
                    "end": 937,
                    "mention": "9",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 1075,
                    "end": 1077,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1367,
                    "end": 1369,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "The healthcare system provided mental health services to government employees and livestock farmers who were required to kill animals due to foot-and-mouth disease [12\u201314]; however, South Korea has never operated mental health services for infectious diseases affecting humans. The Korean health management system reacted relatively well to the Ebola and SARS viruses (which also caused major infectious outbreaks); little motivation has therefore existed to implement psychological interventions. Gyeonggi Province\u2019s proactive intervention with MERS victims constitutes part of the national reinforcement of disaster mental health services, which have recently attracted increased attention. Particularly, following the 2014 Sewol Ferry accident (which resulted in the fourth highest number of deaths), the national and local governments have worked to establish disaster mental health service systems, and Gyeonggi Province, which has experienced various disasters, is participating particularly actively.",
            "cite_spans": [
                {
                    "start": 165,
                    "end": 167,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 168,
                    "end": 170,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "In order to provide an effective disaster mental health service, it is important to find professional experts and establish good programs; moreover, it is also essential to establish sensible governance between central and local governments, between administrative institutions and institutions that provide services, and between public and civil organizations. In Korea, the law has been amended following the Sewol Ferry accident, and the Ministry of Public Safety and Security now controls disaster response. Seventeen Psychological Support Centers for Disasters affiliated with the Ministry of Public Safety and Security are being operated in South Korea. Moreover, the National Center for Crisis Mental Health Management (NCMHM) is located in a national mental hospital affiliated with the Ministry of Health and Welfare, and 15 regional mental health centers and 209 local Community Mental Health Centers (CMHCs) are providing services addressing various mental health problems. The Ministry of Public Safety and Security\u2019s system and that of the Ministry of Health and Welfare occasionally over-lap or operate in cooperation. Additionally, regarding mental health support for MERS, the two ministries took different roles based on the current situation and provided required services in each affected region. In Gyeonggi Province, Psychological Support Centers for Disasters and regional mental health centers are operating in concert.",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "This study makes suggestions for implementing disaster-related mental health service systems tailored to disasters\u2019 type and timing, by analyzing the example of Gyeonggi Province, which proactively intervened with residents\u2019 psychological problems caused by the large-scale outbreak of an infectious disease.",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "Each PHC monitored quarantined individuals\u2019 physical symptoms twice daily. PHCs provided information to quarantined individuals during monitoring concerning managing psychological difficulties that might arise during quarantine and using the consultation service. CMHCs also psychologically evaluated all quarantined individuals during monitoring. The mental health professionals of CHMCs asked key questions about depression, \u201cfor the last 2 weeks or after being in quarantine, do you feel depressed or hopelessness? Do you feel loss of interest in any part of your life?\u201d After the key questions, they provided psychological support, psycho-educational approach and providing information.",
            "cite_spans": [],
            "section": "Systems for people who experienced quarantine ::: Case description",
            "ref_spans": []
        },
        {
            "text": "Six thousand two hundred and thirty-one people were evaluated; 6157 individuals was evaluated via phone call (99.6 %), outreach evaluation (33 cases; 0.5 %), and on-site evaluation (18 cases; 0.3 %). Initially, CMHC staff were only able to evaluate and consult quarantined individuals via phone call due to quarantine requirements. Moreover, many individuals were unwilling to provide personal information due to anticipated stigma or negative local perception of MERS, further raising the proportion of telephone consultation.",
            "cite_spans": [],
            "section": "Systems for people who experienced quarantine ::: Case description",
            "ref_spans": []
        },
        {
            "text": "Of the 6231 people placed in quarantine, 1221 showed emotional disturbances such as depression (19.3 %). Of this population, 871 (71.3 %) received only one consultation and 350 (28.7 %) required continuing services. Among the latter group, 124 (35.4; 2.0 % of quarantined individuals) received continuing services; the remaining 226 could not be reached (64.6; 3.6 % of quarantined individuals). The 124 people who received continuing services received an average of 4.7 consultations; 51 individuals in this group received more than one face-to-face service (41, 0.8 % of quarantined individuals) and CMHC staff evaluated five of these 51 individuals as facing a high-risk of mental illness and referred them to psychiatric treatment (Fig. 2).",
            "cite_spans": [],
            "section": "Systems for people who experienced quarantine ::: Case description",
            "ref_spans": [
                {
                    "start": 741,
                    "end": 742,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The NCMHM conducted the initial evaluation of all deceased patients\u2019 family members and recovered patients. The NCMHM referred individuals who required further medical treatment to five national mental hospitals and those who required continuous mental health services to local CMHCs; however, of those who used the disaster mental health service in Gyeonggi Province, the NCMHM referred only four of 37 (10.8 %) deceased patients\u2019 family members and 11 of 62 (17.7 %) recovered patients. This means that in terms of disaster mental health service system, a considerably higher number of family members and recovered patients used the Gyeonggi Province service directly, rather than the national system. This result shows us that the referral system from the national level to regional or local level does not working and it would be more efficient to develop open entry system at the local level rather than linear triage system.",
            "cite_spans": [],
            "section": "Systems for recovered MERS patients and family members of deceased patients ::: Case description",
            "ref_spans": []
        },
        {
            "text": "This study was done in a real world not an experimental situation. Mental health service interventions for MERS victims were performed by the law not by the research design. We tried to find out the way how the system works for MERS victims, systematic characteristics how the victims use mental health services, the needs of victims and the possible strategy how to develop mental health service system in the near future especially for the disaster by infectious disease.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Nonetheless, it remains significant that the service utilization rate analyzed in this study was much higher than other general mental health situation in Korea; the professionals administering it actively approached and contacted people with problems rather than passively providing information. The need for mental health services is increasing and traditional systems centered on hospitals and medical facilities are ill-suited to addressing many mental health problems. The core value of public mental health services is adequate public accessibility; it is therefore essential for governments to strengthen their professional competence and establish effective systems. These criteria should also be applied to psychological problems caused by disastrous infectious disease outbreaks.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig.\u00a01: Psychological intervention system for MERS victims",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig.\u00a02: Service algorithm and numbers of clients on each level of process",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "WHO meeting on survivors of Ebola virus disease: clinical care of EVD survivors, Freetown 3\u20134 August 2015; meeting report 2015",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "The current mental health status of Ebola survivors in Western Africa",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Yadav",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Rawal",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Clin Diagn Res",
            "volume": "9",
            "issn": "10",
            "pages": "LA01-LA02",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "The current status of kill animals due to foot-and-mouth disease in 2014, 2015 of South Korea",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 2011,
            "venue": "White paper on Korean foot-and-mouth disease 2010\u20132011",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "List of disasters in Korea 2014",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Communicating through crisis, a strategy for organizational survival",
            "authors": [
                {
                    "first": "DL",
                    "middle": [],
                    "last": "Sturges",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Manag Commun Q",
            "volume": "7",
            "issn": "",
            "pages": "297-316",
            "other_ids": {
                "DOI": [
                    "10.1177/0893318994007003004"
                ]
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [
                {
                    "first": "SA",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Research on risk communication strategy of Korean Government, the case study on MERS epidemic",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "JU",
                    "middle": [],
                    "last": "Jeong",
                    "suffix": ""
                },
                {
                    "first": "DG",
                    "middle": [],
                    "last": "Hwang",
                    "suffix": ""
                },
                {
                    "first": "CG",
                    "middle": [],
                    "last": "Son",
                    "suffix": ""
                },
                {
                    "first": "SM",
                    "middle": [],
                    "last": "Yun",
                    "suffix": ""
                },
                {
                    "first": "SM",
                    "middle": [],
                    "last": "Chae",
                    "suffix": ""
                },
                {
                    "first": "MR",
                    "middle": [],
                    "last": "Cha",
                    "suffix": ""
                },
                {
                    "first": "HJ",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Causes of the psycho-social anxiety in Korea and response strategy",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Making public anxiety by focusing on the numbers of the deaths and patients",
            "authors": [
                {
                    "first": "WJ",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Newsp Broadcast",
            "volume": "468",
            "issn": "",
            "pages": "72-75",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Community psycho-behavioural surveillance and related impact on outbreak control in Hong Kong and Singapore during the SARS epidemic",
            "authors": [
                {
                    "first": "GM",
                    "middle": [],
                    "last": "Leung",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Quah",
                    "suffix": ""
                },
                {
                    "first": "LM",
                    "middle": [],
                    "last": "Ho",
                    "suffix": ""
                },
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Ho",
                    "suffix": ""
                },
                {
                    "first": "AJ",
                    "middle": [],
                    "last": "Hedley",
                    "suffix": ""
                },
                {
                    "first": "HP",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "TH",
                    "middle": [],
                    "last": "Lam",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Hong Kong Med J",
            "volume": "15",
            "issn": "",
            "pages": "530-534",
            "other_ids": {
                "DOI": []
            }
        }
    }
}