{
    "paper_id": "PMC6800302",
    "metadata": {
        "title": "A field\u2010deployable insulated isothermal RT\u2010PCR assay for identification of influenza A (H7N9) shows good performance in the laboratory",
        "authors": [
            {
                "first": "Ken",
                "middle": [],
                "last": "Inui",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tung",
                "middle": [],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hsin\u2010Jou",
                "middle": [],
                "last": "Tseng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "ChuanFu",
                "middle": [
                    "Mark"
                ],
                "last": "Tsai",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yun\u2010Long",
                "middle": [],
                "last": "Tsai",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Simon",
                "middle": [],
                "last": "Chung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Pawin",
                "middle": [],
                "last": "Padungtod",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Huachen",
                "middle": [],
                "last": "Zhu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yi",
                "middle": [],
                "last": "Guan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wantanee",
                "middle": [],
                "last": "Kalpravidh",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Filip",
                "middle": [],
                "last": "Claes",
                "suffix": "",
                "email": "filip.claes@fao.org",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "By the end of March 2018, totally 1625 human cases were confirmed to be infected with avian influenza A (H7N9) virus in 27 provinces of mainland China.1 The 2016\u201017 H7N9 epidemic wave [5th wave] began earlier, spread to more districts and counties in affected provinces, and had more confirmed human cases than previous epidemics. In this wave, highly pathogenic avian influenza (HPAI) was detected for the first in a human infection in Guangdong Province on February 13, 2017, with illness onset in December 2016. Thus far, 28 HPAI human infections have been identified, among which 27 reported in Guangdong, Guangxi, Hunan, Inner Mongolia, and Hebei, and 1 imported case to Taiwan [WHO, 2018]. Following the 5th wave, the Chinese Ministry of Agriculture (MOA) decided to implement nationwide H7N9 vaccination starting in September 2017. Inactivated bivalent vaccine (H5N1 Re\u20108 strain + H7N9 H7\u2010Rel strain) replaced the previously used H5 inactivated vaccine, and vaccination was applied to all kept chicken, duck, goose and domestic quail, pigeon, and rare birds. During the 6th wave (winter season 2017\u20102018), only 3 human cases and 15 animal outbreaks were reported (as of June 2018). Currently, H7N9 circulation seems to be much lower in the poultry population, with only 1 outbreak was reported in the 2018\u20102019 influenza season2 and only one human case was recently reported in April 2019 in Jiuquan City in Gansu Province.3\n",
            "cite_spans": [],
            "section": "INTRODUCTION",
            "ref_spans": []
        },
        {
            "text": "Recent molecular characterization of H7N9 viruses showed a continuous evolution of the viruses similar to the H5N1 evolution.4 The latest circulating H7N9 viruses are clustered into two main lineages: the Yangtze River Delta (YRD) and Pearl River Delta (PRD) HA lineage. Additionally, certain H7N9 viruses of the YRD lineage acquired multiple basic amino acids at the cleavage site of their HA gene, converting them into highly pathogenic avian influenza (HPAI) variants. These HPAI viruses have been isolated from poultry, environmental samples, and human patients.",
            "cite_spans": [],
            "section": "INTRODUCTION",
            "ref_spans": []
        },
        {
            "text": "In neighboring countries of China (Vietnam, Lao PDR, and Myanmar), the Food and Agriculture Organization of the United Nations (FAO) has been supporting risk\u2010based active surveillance for influenza A and H7N9 in live bird markets and along the poultry value chain. In Vietnam, active surveillance was conducted in live bird markets (LBMs) in 26 out of 63 provinces for H5N1 and in 14 provinces for H7N9 during 2016\u20102017. From April 2017 to March 2018, the surveillance has detected influenza A virus in 38 out of 107 markets sampled (22% positive at sample level) (FAO, unpublished results). These markets were located in all 26 provinces. To date, H7N9 has not been detected through surveillance activities and no human infection has been reported, suggesting the virus is not currently present in Vietnam. Based on recent risk assessments, the threat of introduction of H7N9 into Vietnam remains moderate to high based on value chain studies, which have found undocumented movement of poultry into Vietnam from China and the recent occurrence of HPAI H7N9 in poultry in China.",
            "cite_spans": [],
            "section": "INTRODUCTION",
            "ref_spans": []
        },
        {
            "text": "The current avian influenza surveillance in Vietnam relies on the testing of samples at regional animal health offices (RAHO's) or the National Centre for Veterinary Diagnosis (NCVD). The average time to obtain a laboratory result is 2.5 days, with the majority of this time being attributed to sample shipment and transport to the laboratories (6\u201024 hours on average). Novel technologies, such as the insulated isothermal PCR (iiRT\u2010PCR), are portable PCR systems that can be applied under field conditions by the fast PCR reaction (in 42 minutes). The iiRT\u2010PCR portable system is a miniature portable device for field test, using freeze\u2010dried thermostable reagents and powered by rechargeable lithium batteries. The iiRT\u2010PCR applies the concept of Rayleigh\u2010Benard convection to drive a PCR reaction in capillary tubes. The primers/probe design rules are the same as for RT\u2010PCR. While conventional PCR requires multiple cycles of heating and cooling, iiRT\u2010PCR is performed through the creation of a temperature gradient in a capillary tube with a single heating source at the bottom of a capillary tube and establishment of thermal convection within the tube, mimicking the cycles of conventional real\u2010time PCR. This system has recently been developed to rapidly detect pathogenic viruses, including white spot syndrome virus, classical swine fever, foot and mouth disease, equine influenza, bluetongue virus, and MERS\u2010CoV.5, 6, 7, 8, 9, 10\n",
            "cite_spans": [],
            "section": "INTRODUCTION",
            "ref_spans": []
        },
        {
            "text": "Taking advantage of the iiRT\u2010PCR portable system, the aim of this study was to validate the performance of the iiRT\u2010PCR portable system for H7N9 detection.",
            "cite_spans": [],
            "section": "INTRODUCTION",
            "ref_spans": []
        },
        {
            "text": "This study used a panel of 59 virus isolates for analytical specificity that included 28 H7N9 AIVs, 7 H7 AIVs of Eurasian lineage but not in the cluster of recent H7N9 virus in China, 15 non\u2010H7 AIVs, seven non\u2010H7 influenza viruses of swine and human origin, two poultry viruses, Newcastle disease virus, and infectious bursal disease virus (Table 1).",
            "cite_spans": [],
            "section": "Viruses ::: MATERIALS AND METHODS",
            "ref_spans": [
                {
                    "start": 347,
                    "end": 348,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "A total of 50 oropharyngeal swab samples were collected from 25 chickens and 25 ducks experimentally infected with six different strains of AIV subtype H7N9. They were confirmed to be positive for AIV H7N9 by virus isolation and used as positive samples. Another set of 50 oropharyngeal swabs were collected from the same chickens and ducks before infection. They were negative for AIV H7N9 by virus isolation and used as negative samples.",
            "cite_spans": [],
            "section": "Clinical samples ::: MATERIALS AND METHODS",
            "ref_spans": []
        },
        {
            "text": "Nucleic acid extraction from clinical swab samples was performed with taco\u2122 preloaded DNA/RNA Extraction Kit (GeneReach USA) on taco\u2122 mini Nucleic Acid Automatic Extraction System (GeneReach USA) and according to the manufacturer's instructions. The Taco mini system can be driven by AC and DC current (net or battery power), and its 5 kilograms allows users to transport it to the field (Figure 1). The taco preloaded DNA/RNA  Extraction Kit is a magnetic bead\u2010based total nucleic extraction reagent including Lysis buffer, Washing Buffer A, Washing Buffer B, and Elution Solution are all preloaded and sealed by foil. They can be easily transported under ambient temperature and used upon needed. Briefly, 100 \u03bcL of each swab sample was added to individual wells in the first row of preloaded 48\u2010well extraction plate. The preloaded 48\u2010well extraction plate was inserted into the instrument, and the \u201cstart\u201d button was pressed. The extraction time was 25 minutes, and the eluted nucleic acid was available in the last row of the preloaded 48\u2010well extraction plate. The nucleic acid was used immediately or stored at \u221280\u00b0C until use.",
            "cite_spans": [],
            "section": "Nucleic acid extraction ::: MATERIALS AND METHODS",
            "ref_spans": [
                {
                    "start": 396,
                    "end": 397,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "In vitro transcribed RNA (IVT RNA) was generated from a plasmid containing a fragment of HA gene of AIV subtype H7N9 A/Anhui/1/2013 using MAXIscript T7 kit (Ambion). Residual DNA was removed using the Ambion Turbo DNA\u2010free kit (Life Technologies). The concentration of RNA was measured by a NanoDrop 2000 Spectrophotometer (Thermo Fisher Scientific). Serial dilutions of in vitro transcribed RNA were made in 40 ng/\u03bcL yeast tRNA to determine the limit of detection. Single use aliquots were stored at \u221280\u00b0C until use.",
            "cite_spans": [],
            "section": "In vitro transcribed RNA ::: MATERIALS AND METHODS",
            "ref_spans": []
        },
        {
            "text": "POCKIT\u2122 iiRT\u2010PCR assay for AIV subtype H7 was performed by using POCKIT\u2122 influenza H7 reagent set (GeneReach USA). The N9 iiRT\u2010PCR was developed by using the primers and probes in the RRT\u2010PCR for avian influenza A (H7N9) described by WHO without any modifications.11 The primers and probes used are listed in Table 2. iiRT\u2010PCR reagent is a single dose lyophilized format which can be shipped under ambient temperature for one week.",
            "cite_spans": [],
            "section": "POCKIT\u2122 iiRT\u2010PCR assay for AIV subtype H7 and N9 ::: MATERIALS AND METHODS",
            "ref_spans": [
                {
                    "start": 315,
                    "end": 316,
                    "mention": "2",
                    "ref_id": "TABREF1"
                }
            ]
        },
        {
            "text": "Briefly, the lyophilized reagent was reconstituted with 50 \u03bcL Premix Buffer B and mixed with 5 \u03bcL nucleic acid extract. Subsequently, 50 \u03bcL of the final mixture was transferred to an R\u2010tube (GeneReach USA) and sealed with a cap, spun briefly, and placed into the POCKIT\u2122 Micro Plus Nucleic Acid Analyzer (GeneReach USA). Qualitative results were generated by the built\u2010in algorithm and shown on the display screen within 42 minutes. Combining nucleic acid extraction (25 minutes) with Taco mini and the iiRT\u2010PCR procedure (42 minutes) with POCKIT Micro Plus can generate the qualitative test results in 90 minutes.",
            "cite_spans": [],
            "section": "POCKIT\u2122 iiRT\u2010PCR assay for AIV subtype H7 and N9 ::: MATERIALS AND METHODS",
            "ref_spans": []
        },
        {
            "text": "The M Influenza A, H7 CODA, and H7 CNIC RRT\u2010PCR for detection of influenza A and subtype H7 were carried out as described previously with some modifications (WHO 2017, Borm et al 2009). Primers and probes used are listed in Table 1. The Superscript III Platinum One\u2010Step qRT\u2010PCR kits (Invitrogen, Thermo Fisher Scientific) were used for all tests. The test was performed on MIC real\u2010time PCR platform (Bio Molecular Systems, Australia). Briefly, each 15 \u03bcL reaction mixture included 3 \u03bcL of nucleic acid sample, 7.5 \u03bcL of 2\u00d7 Premix buffer, 0.8 \u03bcM of forward and reverse primers, 0.4 \u03bcM of TaqMan probe for M InfA and H7 CNIC, 0.2 \u03bcM for H7 CODA, and 0.3 \u03bcL of SuperScript III RT/Platinum Taq mix. The thermocycling program was set up as follows: 50\u00b0C for 30 minutes, 95\u00b0C for 2 minutes, followed by 40 cycles of 95\u00b0C for 15 seconds, and 60\u00b0C for 45 seconds. C\nt thresholds were set automatically as per manufacturers\u2019 defaults.",
            "cite_spans": [],
            "section": "Real\u2010time RT\u2010PCR (RRT\u2010PCR) assay for universal influenza A virus and AIV subtype H7 ::: MATERIALS AND METHODS",
            "ref_spans": [
                {
                    "start": 230,
                    "end": 231,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "Limit of detection 95% (LOD95%) of a reaction was determined by probit analysis at 95% confidence interval by SPSS v14 (SPSS). The 2 \u00d7 2 contingency tables were analyzed by kappa statistic using SPSS to determine the inter\u2010assay agreement.",
            "cite_spans": [],
            "section": "Statistical analysis ::: MATERIALS AND METHODS",
            "ref_spans": []
        },
        {
            "text": "The analytical sensitivity of AIV H7 iiRT\u2010PCR was determined by using 3\u2010fold serial dilutions of IVT RNA. Analysis of replicates of 100, 33, 10, and 0 copies of IVT RNA showed that 100% (20/20), 100% (20/20), 90% (18/20), and 0% (0/0), respectively, of the reactions produced positive signals. The LoD95% was estimated to be 11 copies/reaction of IVT RNA by probit regression analysis. The hit rates of the AIV N9 iiRT\u2010PCR for 300, 100, 33, 10, and 0 were 100% (5/5), 58.33% (7/12), 57.14% (4/7), 0% (0/7), and 0% (0/12), with an estimated LoD9595% of 171 copies/reaction.",
            "cite_spans": [],
            "section": "Analytical sensitivity of iiRT\u2010PCR for AIV subtype H7 ::: RESULTS",
            "ref_spans": []
        },
        {
            "text": "The analytical sensitivity of AIV H7 iiRT\u2010PCR was further analyzed using 10\u2010fold serial dilutions (up to 10\u22128) of representative viruses from four groups of AIV H7N9 viruses. The detection endpoint (all triplicates positive) was compared to that of RRT\u2010PCR assays using primers and probe set of H7 CODA and H7 CNIC. The sensitivity of AIV H7 iiRT\u2010PCR was shown to be comparable to that of H7 CODA RRT\u2010PCR assay in detecting the 4 representative H7N9 viruses, while having at least one log higher sensitivity when compared to that of H7 CNIC RRT\u2010PCR assay (Table 3). The sensitivity of AIV N9 iiRT\u2010PCR was shown to be about one log lower than that of H7 CODA RRT\u2010PCR assay in detecting the four representative H7N9 viruses, and sensitivity similar to that of H7 CNIC RRT\u2010PCR assay (Table 3).",
            "cite_spans": [],
            "section": "Analytical sensitivity of iiRT\u2010PCR for AIV subtype H7 ::: RESULTS",
            "ref_spans": [
                {
                    "start": 562,
                    "end": 563,
                    "mention": "3",
                    "ref_id": "TABREF2"
                },
                {
                    "start": 787,
                    "end": 788,
                    "mention": "3",
                    "ref_id": "TABREF2"
                }
            ]
        },
        {
            "text": "The analytical specificities of the AIV H7 and N9 iiRT\u2010PCR assay were evaluated using RNA extracted from 34 AIV H7 viruses (11 H7N9 Yangtze River lineage, 11 H7N9 Pearl River lineage, five highly pathogenic H7N9 strains, seven H7 of Eurasian lineage but not in the cluster of recent Chinese H7N9) and 24 non\u2010H7 viruses (15 from H1\u2014H6 and H8\u2014H 13, three swine influenza virus H1 and H3, four human influenza virus H1 and H3, one Newcastle disease virus, and one infectious bursal disease virus; Table 1). The assay showed 100% specificity detecting all 34 H7 viruses and showing no cross\u2010reactivity with non\u2010H7 viruses. The analytical specificity of the AIV H7 iiRT\u2010PCR assay was shown to be comparable to that of H7 CODA RRT\u2010PCR assay.",
            "cite_spans": [],
            "section": "Analytical specificity of iiRT\u2010PCR for AIV subtype H7 and N9 ::: RESULTS",
            "ref_spans": [
                {
                    "start": 500,
                    "end": 501,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The analytical specificity of the AIV N9 iiRT\u2010PCR assay was evaluated similarly using RNA extracted from 29 AIV N9 viruses and 30 non\u2010N9 viruses. The assay showed 100% specificity, detecting all 29 N9 viruses and showing no cross\u2010reactivity with non\u2010N9 viruses.",
            "cite_spans": [],
            "section": "Analytical specificity of iiRT\u2010PCR for AIV subtype H7 and N9 ::: RESULTS",
            "ref_spans": []
        },
        {
            "text": "A total of 50 virus isolation\u2010positive and 50 virus isolation\u2010negative reference samples from chickens and ducks experimentally infected with different subtype H7N9 AIVs were tested by the H7 iiRT\u2010PCR and compared in a 2 \u00d7 2 table (Table 4). Using virus isolation as the gold standard, the iiRT\u2010PCR showed a sensitivity of 98% and a specificity of 100%.",
            "cite_spans": [],
            "section": "Diagnostic sensitivity and specificity of AIV H7 iiRT\u2010PCR evaluated with oropharyngeal swabs from experimentally infected chickens and ducks ::: RESULTS",
            "ref_spans": [
                {
                    "start": 238,
                    "end": 239,
                    "mention": "4",
                    "ref_id": "TABREF3"
                }
            ]
        },
        {
            "text": "To our knowledge, this is the first time that the iiRT\u2010PCR was validated for H7N9 detection. The validation design, including a broad set of reference viruses and the inclusion of oropharyngeal swabs of H7N9 infected birds, ensured that the analytical and diagnostic testing was performed according to the guidelines in the OIE Terrestrial Manual12.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Validation results show that the iiRT\u2010PCR system's performance is equivalent to a widely used laboratory\u2010based real\u2010time PCR setup. The analytical detection limits of the H7 and N9 iiRT\u2010PCR reagents are similar to RRT\u2010PCR, and excellent specificity to H7 and N9 viruses was observed, respectively. It should be noted that the sensitivity of the N9 protocol is about one log lower than the H7 iiRT\u2010PCR. A possible explanation is that the H7 primers and probes were optimized for use on the Pockit PCR, while for N9 the exact CNIC China design was used. It was shown before that also in qPCR the CNIC N9 protocol is less sensitive than various H7 protocols tested.13\n",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "The diagnostic performance of the H7 iiRT\u2010PCR with a relatively high sensitivity of 98% and a 100% specificity shows that the system would be performant to be used as a diagnostic platform. Ideally, the diagnostic performance would be tested on a larger set of samples, yet practically it showed difficult to obtain sufficient clinical samples for validation purposes. This current validation was aimed to validate the fitness of the iiRT\u2010PCR for the early diagnosis of H7 and N9 in animal samples (oropharyngeal swabs). Further validation is needed to show the possibility to use other sample types (environmental samples, human swabs) or to use the system to detect different HA (H5, H9) or NA\u2010types (N6 or N8).",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Recently, other H7Nx were also found to be circulating in China and Southeast Asia. These H7Nx belong to the Eurasian H7\u2010lineage and are distinct from the Chinese H7N9. The current H7 test will most likely also detect these viruses as the H7 iiRT\u2010PCR primer/probe design targets a broad spectrum of H7. For public health purposes and early warning, this possible cross\u2010reactivity can be considered beneficial as any type of H7 is potentially zoonotic and detection of H7Nx in livestock samples can serve as an early warning system to raise public awareness of circulating animal influenza with pandemic potential. Also, the N9 protocol can detect other non\u2010H7N9 viruses, for example, H1N9 or H12N9 as shown in Table 1. Further steps in these cases should be confirmation of the subtype in the laboratory, followed by full HA and NA (or full genome) sequencing.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": [
                {
                    "start": 716,
                    "end": 717,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The innate characteristics of the iiRT\u2010PCR system make it a very useful tool for onsite diagnosis. The device is compact (hand\u2010held), uses lyophilized thermostable PCR reagents, and runs on rechargeable batteries, enabling it to be used in various field conditions (live bird markets, farms, quarantine stations, veterinary stations, road check points, or hospitals) and thus reducing time of sample transportation. Having a quick result onsite will make it possible to initiate a quick response, such as initial movement control and quarantine measures in live bird markets or on farms, reducing the risks of further spread and potential human infections of H7N9. In Vietnam for example, the portable PCR was introduced into the active live bird market surveillance for the early detection of H7N9. Once a case of H7 would be detected in the field through the portable system, immediate movement restrictions will be put in place while waiting for confirmatory diagnosis from the laboratory.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Results from field pilot studies (unpublished data) showed that it was feasible to install the system rapidly at any given sites and that personnel in the flied could be trained in the use of the iiRT\u2010PCR system within 2 days. One of the challenges of the system is how it will be incorporated into current ongoing active surveillance designs. iiRT\u2010PCR can be complementary to current surveillance designs, yet it needs to be clear if and how results will be confirmed by standard laboratory\u2010based tests (real\u2010time RT\u2010PCR, virus isolation, or sequencing) and how data sharing will occur from local levels to province or central levels.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "In conclusion, this study showed that field\u2010based portable PCR (iiRT\u2010PCR) can be used for the early diagnosis of H7N9 as an alternative approach to laboratory\u2010based real\u2010time PCR. Using field\u2010based test will reduce the time to obtain a result and will enable possible quick response measures in the field, reducing the risk of further spread and human infections with H7N9 in currently non\u2010infected countries.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1: Viruses used in this study and the analytical specificity results of real\u2010time RT\u2010PCR and insulated isothermal RT\u2010PCR\n",
            "type": "table"
        },
        "TABREF1": {
            "text": "Table 2: Target genes, design, and sequences of primers and probes used in this study\n",
            "type": "table"
        },
        "TABREF2": {
            "text": "Table 3: Diagnostic sensitivity. Comparison of triplicate testing of ten\u2010fold dilutions of four avian influenza virus H7N9 strains in H7 CODA protocol and H7 CNIC protocol in real\u2010time RT\u2010PCR and in H7 and N9 insulated isothermal RT\u2010PCR\n",
            "type": "table"
        },
        "TABREF3": {
            "text": "Table 4: Diagnostic sensitivity and specificity of avian influenza virus H7 iiRT\u2010PCR as compared to virus isolation\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Figure 1: The iiRT\u2010PCR portable setup. This Pockit Combo set includes a Taco mini portable extractor which runs on battery or net power, four Pockit mini DNA/RNA amplifiers, a set of pipettes, and a hard case suitcase for safe transportation to the field",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "A7 Evolution of influenza A(H7N9) viruses from waves I to IV",
            "authors": [],
            "year": 2017,
            "venue": "Virus Evolution",
            "volume": "3",
            "issn": "suppl_1",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Reverse transcription\u2010insulated isothermal PCR (RT\u2010iiRT\u2010PCR) assay for rapid and sensitive detection of foot\u2010and\u2010mouth disease virus",
            "authors": [],
            "year": 2016,
            "venue": "Transbound Emerg Dis",
            "volume": "64",
            "issn": "",
            "pages": "1610-1623",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "A rapid field\u2010deployable reverse transcription\u2010insulated isothermal polymerase chain reaction assay for sensitive and specific detection of bluetongue virus",
            "authors": [],
            "year": 2015,
            "venue": "Transbound Emerg Dis",
            "volume": "64",
            "issn": "",
            "pages": "476-486",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Rapid detection of equine influenza virus H3N8 subtype by insulated isothermal RT\u2010PCR (iiRT\u2010PCR) assay using the POCKIT nucleic acid analyzer",
            "authors": [],
            "year": 2014,
            "venue": "J Virol Methods",
            "volume": "207",
            "issn": "",
            "pages": "66-72",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Evaluation and clinical validation of two field\u2010deployable reverse transcription\u2010insulated isothermal PCR assays for the detection of the middle East respiratory syndrome\u2010coronavirus",
            "authors": [],
            "year": 2017,
            "venue": "J Mol Diagn",
            "volume": "19",
            "issn": "6",
            "pages": "817-827",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Insulated isothermal reverse transcriptase PCR (iiRT\u2010PCR) for rapid and sensitive detection of classical swine fever virus",
            "authors": [],
            "year": 2016,
            "venue": "Transbound Emerg Dis",
            "volume": "63",
            "issn": "",
            "pages": "395-402",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Validation of a commercial insulated isothermal PCR\u2010based POCKIT test for rapid and easy detection of white spot syndrome virus infection in Litopenaeus vannamei",
            "authors": [],
            "year": 2014,
            "venue": "PLoS ONE",
            "volume": "9",
            "issn": "3",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}