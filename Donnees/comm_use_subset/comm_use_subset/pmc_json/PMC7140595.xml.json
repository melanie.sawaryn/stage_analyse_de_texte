{
    "paper_id": "PMC7140595",
    "metadata": {
        "title": "Only strict quarantine measures can curb the coronavirus disease (COVID-19) outbreak in Italy, 2020",
        "authors": [
            {
                "first": "Henrik",
                "middle": [],
                "last": "Sj\u00f6din",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Annelies",
                "middle": [],
                "last": "Wilder-Smith",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sarah",
                "middle": [],
                "last": "Osman",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Zia",
                "middle": [],
                "last": "Farooq",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Joacim",
                "middle": [],
                "last": "Rockl\u00f6v",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In February 2020, Italy became the epicentre for coronavirus disease (COVID-19) in Europe, with many exportations to other countries, and widespread community transmission, particularly in Northern Italy [1]. As a public health response, on 22 February 2020, Italy imposed a lockdown with shutdown of businesses, schools and public places plus physical distancing in \u2018hotspot\u2019 regions close to Milan and Venice. Approximately 50,000 people could not enter or leave several towns in Veneto and Lombardy for 14 days without special permission. The population sizes in these towns range from 927 to 15,293 individuals [2]. As at 9 March 2020, 7,375 laboratory-confirmed cases of COVID-19 and 366 deaths had been observed in Italy, so on that date, the community quarantine was extended to include all of Northern Italy until 3 April. Here we aim to investigate the extent of physical distancing needed to effectively control the outbreak in a lockdown situation in a small size town setting typical of Italy. We specifically estimate the disease burden and the time required until the quarantine can be lifted, by taking into account the time spent by individuals in the public (i.e. outside of the home) and the household size.",
            "cite_spans": [
                {
                    "start": 205,
                    "end": 206,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 616,
                    "end": 617,
                    "mention": "2",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "European epicentre",
            "ref_spans": []
        },
        {
            "text": "To account for the importance of stochasticity in individual-based processes within smaller cities and within households, we modelled the outbreak progression as a continuous-time Markov process, specifically by developing a susceptible-exposed-infectious-recovered (SEIR) epidemiological model in the form of a Master equation [3-5] (Supplement 1). The model was parameterised to COVID-19 based on published data on incubation time and infectious period [6,7]. We modelled a scenario where 0.1% of the population in the quarantined town would be in the latent period (i.e. the period of time between the point of infection and the onset of infectiousness) at the time of implementing the quarantine policy on 22 February, and that all symptomatic cases would have been moved out the locked-down town (e.g. placed in a hospital for care and isolation). This corresponds to five latent persons in a city of 5,000 persons. Further, we assumed that all persons were isolated after 1 day of symptoms. We also assumed a pre-symptomatic period of infectiousness of 1 day. Of all infected persons, we modelled different proportions of asymptomatic infections (scenarios of 10%, 20% or 50%) based on our preliminary knowledge on such proportions [8,9]. Persons with asymptomatic infections would not be isolated and continue to contribute to transmission. Parameters are summarised in the Table.",
            "cite_spans": [
                {
                    "start": 329,
                    "end": 330,
                    "mention": "3",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 331,
                    "end": 332,
                    "mention": "5",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 456,
                    "end": 457,
                    "mention": "6",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 458,
                    "end": 459,
                    "mention": "7",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 1239,
                    "end": 1240,
                    "mention": "8",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 1241,
                    "end": 1242,
                    "mention": "9",
                    "ref_id": "BIBREF19"
                }
            ],
            "section": "Stochastic individual based processes",
            "ref_spans": [
                {
                    "start": 1381,
                    "end": 1386,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Importantly, the standard reproductive rate [5], equal to the product between contact rate and the probability \ud835\udf0f of transmission given a contact event, was set within a household to \u03b2 = c\u03c4 = 2.1 [10], and within public locations in the community to \u03b2~=c~\u03c4=0.27; lower than in mainland China or on the cruise ship Diamond Princess [10,11], as population densities in European towns are lower. The within-household contact-rate, \ud835\udc50, was thus assumed to be eight times higher than the contact-rate \ud835\udc50\u0303 at public locations. We could then apply a quarantine adherence parameter \ud835\udf11, to model dynamically the amount of time spent in households relative to that in public locations (see Supplement 1 for a more detailed description).",
            "cite_spans": [
                {
                    "start": 45,
                    "end": 46,
                    "mention": "5",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 196,
                    "end": 198,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 331,
                    "end": 333,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 334,
                    "end": 336,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Stochastic individual based processes",
            "ref_spans": []
        },
        {
            "text": "We modelled the effectiveness of quarantine based on the degree of adherence to quarantine, measured by the number of hours per person spent in the public per day. Complete noncompliance to community quarantine corresponds to a reference quarantine level, where individuals perform their every-day out-of-household activities (i.e. working, shopping, socialising) as normal, for an average 10 hours per day. Medium adherence to community quarantine restricts every-day out-of-household activities to 50% of normal, i.e. 5 hours a day. A complete community quarantine corresponds to no out-of-household activities at all, i.e. 0 hours a day. For any degree of quarantine adherence, we tested for four different average household sizes: (i) larger average households of six persons, (ii) medium average households of three persons, (iii) small average households of two persons, and lastly (iv) single-person households. ",
            "cite_spans": [],
            "section": "Quarantine scenarios",
            "ref_spans": []
        },
        {
            "text": "Given that Italy had initially implemented a 14-days lockdown with community quarantine in several towns in Northern Italy, we estimated the number of secondary cases, including asymptomatic cases, in a town of 5,000 persons by the end of this time period in relation to the above scenarios (Figure 1). For any degree of quarantine adherence between 0 and 10 hours, Figure1A, Figure 1B and Figure 1C provide the expected number of secondary infections over 14 days of lockdown, and the number of latent infectious persons and the number of infectious persons respectively at Day 14 of lockdown. These reported numbers relate to a population where 10% of all infected persons are asymptomatic, and would increase for 20% (Figure 1 D\u2013F) or 50% (Figure 1 G\u2013I) asymptomatic cases.",
            "cite_spans": [],
            "section": "Quarantine scenarios",
            "ref_spans": [
                {
                    "start": 292,
                    "end": 300,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 366,
                    "end": 374,
                    "mention": "Figure1A",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 376,
                    "end": 385,
                    "mention": "Figure 1B",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 390,
                    "end": 399,
                    "mention": "Figure 1C",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 721,
                    "end": 733,
                    "mention": "Figure 1 D\u2013F",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 743,
                    "end": 755,
                    "mention": "Figure 1 G\u2013I",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Our model shows how the number of secondary cases within the town increases with the time spent in the public, and also with the average household size (i.e. the quarantine unit). Looking at the extremes, for a six-person household and no community quarantine, we predicted 43 new infections over the 14 days period. In contrast, for a single-person household and complete community quarantine (no time outside of homes), no secondary cases were predicted over the 14 days period. The average household size in Italy is 2.58 according to the Organisation for Economic Co-operation and Development (OECD) [12]. For an average household size of two persons with complete, near-complete, medium and no community quarantine (i.e. 0, 1, 5, and 10 hours respectively in the community), we predict 3, 4, 7 and 11 secondary infections during the lockdown. With an average three-person household size, 7, 8, 12 and 20 secondary infections are predicted, respectively. The average Italian household size 2.58 is thus in-between that of a two-person and three-person household size. With a six-person average household size, 16, 19, 29 and 43 secondary infections would be predicted to occur over the 14-days period, respectively.",
            "cite_spans": [
                {
                    "start": 605,
                    "end": 607,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Quarantine scenarios",
            "ref_spans": []
        },
        {
            "text": "In addition, our model indicates that the number of secondary, latent and infected cases has a linear relationship with the population size of a lockdown region, provided same population densities apply between cities. In a locked-down area with 50,000 people, we would expect for an average household size of two persons with complete, near-complete, medium and no community quarantine 30, 40, 70 and 110 secondary infections over the 14-days period, respectively.",
            "cite_spans": [],
            "section": "Quarantine scenarios",
            "ref_spans": []
        },
        {
            "text": "The objective of the lockdown with community quarantine is to contain the outbreak within a manageable duration. Figure 2 shows the results on lockdown durations required for average household sizes of 1, 2, 3 and 6, and for various degrees of strictness of quarantine restrictions. Assuming 10% asymptomatic infections (Figure 2a), for a three-person average household-size situation, around 30 days will be a sufficient length under conditions of near-complete community quarantine adherence. With only medium adherence a duration of 54 days would be necessary. Less strict quarantine will result in much longer lockdown periods, which then become unfeasible for any society. These results are only marginally different to a situation with 50% asymptomatic cases (Figure 2b). In addition, the definition to declare an outbreak over requires waiting two times the maximum incubation period after the last case, e.g. 2 \u00d7 14 days.",
            "cite_spans": [],
            "section": "Duration of the quarantine",
            "ref_spans": [
                {
                    "start": 113,
                    "end": 121,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 321,
                    "end": 330,
                    "mention": "Figure 2a",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 766,
                    "end": 775,
                    "mention": "Figure 2b",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The lockdown in China with government enforced movement restriction outdoors combined with facility-based case isolation, rigorous contact tracing and quarantine of all contacts, had a substantial impact on interrupting the chain of human-to-human transmission in Wuhan, thus effectively containing the outbreak [16]. While the outbreak in Wuhan involved a highly urbanised setting, the current lockdown in Italy involves small villages with a different social culture and behaviour, and different mechanisms of quarantine enforcement. Our findings suggest that the degree of quarantine adherence needs to be very high regardless of population size in order to be effective. We note, however, that a less strict community quarantine could still flatten the curve of the outbreak compared to no quarantine [17]. In any case, quarantine adherence has an Important and notable impact on reducing the outbreak, but some transmission will still occur within households. We showed that in a theoretical scenario of a single-person household with very strict community quarantine measures, no secondary infections would occur. While a single-person household does not reflect the reality of any society, it does suggest that if all cases could be isolated, e.g. moved out of the community, the epidemic curve would decline much faster and the lockdown duration could be reduced. This means that more efforts need to be done at household level: keeping physical distance even within a household combined with wearing face masks and segregated within-household isolation, or better all symptomatic cases ideally need to be promptly moved out of the household, and isolated in a designated facility. Prompt testing is therefore needed for timely diagnosis and immediate isolation. We also show that a 14-days lockdown period is not sufficient for most scenarios; a longer lockdown duration is needed. On 8 March, Italy announced the need to extend the lockdown to include around 16 million people for 25 additional days until 3 April. Such longer duration should in fact be expected to be required, to have a positive impact, which is going to be very challenging for affected communities to be supplied in food, essential services and to be able to cope psychologically. If lockdown is enforced, it must be done rigorously to truly interrupt transmission, and this would mean near 100% restriction of contacts between persons within the community combined with prompt isolation of new cases. Less strict quarantine adherence would imply even longer lockdown periods, and longer lockdown periods will likely present even greater socioeconomic challenges. By implementing the world\u2019s largest lockdown combined with prompt case isolation, contact tracing of contacts and with strict enforcement of physical distancing [16], containment of COVID-19 in China was shown to be feasible. Remarkably, in South Korea, the control of the outbreak, which had been temporarily lost, was regained without lockdown but with rigorous active case finding, by liberal testing, prompt isolation, and by using novel digital technologies to maximise contact tracing and quarantine of all contacts [18]. In certain places like Taiwan [19], Singapore [20] and Hong Kong, a flat epidemic curve was maintained for COVID-19 by applying very liberal testing, prompt case isolation outside of the community (no home isolation even of the mildest cases), and technologically enhanced contact tracing, very early in the outbreak. If the lockdown in Italy, and meanwhile in many other European countries, is aimed at containment, close to 100% restriction of contact time within communities combined with prompt case detection and immediate isolation of infected persons need to be achieved.",
            "cite_spans": [
                {
                    "start": 313,
                    "end": 315,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 806,
                    "end": 808,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 2807,
                    "end": 2809,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 3168,
                    "end": 3170,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 3204,
                    "end": 3206,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 3220,
                    "end": 3222,
                    "mention": "20",
                    "ref_id": "BIBREF12"
                }
            ],
            "section": "Quarantine in the wider context ::: Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Estimation of the number of total secondary cases (panels A, D, G) during the 14-days period and the (panels B, E, H) latent and (panels C, F, I) infectious cases of COVID-19 at the end of a 14-days lockdown of a city with a population of 5,000 persons, depending on the degree of community quarantine adherencea, the size of quarantine units and the proportion of asymptomatic cases",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Estimation of the required duration of a lockdown with quarantine, to contain a COVID-19 outbreak in a city of 5,000 inhabitants, depending on the quarantine adherence strictness and household size",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "The global community needs to swiftly ramp up the response to contain COVID-19.",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Fisher",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wilder-Smith",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30679-6"
                ]
            }
        },
        "BIBREF1": {
            "title": "The scaling of contact rates with population density for the infectious disease models.",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Nigmatulina",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Eckhoff",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Math Biosci",
            "volume": "244",
            "issn": "2",
            "pages": "125-34",
            "other_ids": {
                "DOI": [
                    "10.1016/j.mbs.2013.04.013"
                ]
            }
        },
        "BIBREF2": {
            "title": "COVID-19 outbreak on the Diamond Princess cruise ship: estimating the epidemic potential and effectiveness of public health countermeasures.",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rockl\u00f6v",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Sj\u00f6din",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wilder-Smith",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Travel Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1093/jtm/taaa030"
                ]
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "High population densities catalyze the spread of COVID-19.",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rockl\u00f6v",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Sj\u00f6din",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Travel Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1093/jtm/taaa038"
                ]
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "The positive impact of lockdown in Wuhan on containing the COVID-19 outbreak in China.",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Lau",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Khosrawipour",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Kocbach",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mikolajczyk",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Schubert",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Bania",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Travel Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1093/jtm/taaa037"
                ]
            }
        },
        "BIBREF8": {
            "title": "How will country-based mitigation measures influence the course of the COVID-19 epidemic?",
            "authors": [
                {
                    "first": "RM",
                    "middle": [],
                    "last": "Anderson",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Heesterbeek",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Klinkenberg",
                    "suffix": ""
                },
                {
                    "first": "TD",
                    "middle": [],
                    "last": "Hollingsworth",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "10228",
            "pages": "931-4",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30567-5"
                ]
            }
        },
        "BIBREF9": {
            "title": "Contact Transmission of COVID-19 in South Korea: Novel Investigation Techniques for Tracing Contacts.",
            "authors": [],
            "year": 2020,
            "venue": "Osong Public Health Res Perspect",
            "volume": "11",
            "issn": "1",
            "pages": "60-3",
            "other_ids": {
                "DOI": [
                    "10.24171/j.phrp.2020.11.1.09"
                ]
            }
        },
        "BIBREF10": {
            "title": "Response to COVID-19 in Taiwan: Big Data Analytics, New Technology, and Proactive Testing.",
            "authors": [
                {
                    "first": "CJ",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "CY",
                    "middle": [],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "RH",
                    "middle": [],
                    "last": "Brook",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.3151"
                ]
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Interrupting transmission of COVID-19: lessons from containment efforts in Singapore.",
            "authors": [
                {
                    "first": "VJ",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "CJ",
                    "middle": [],
                    "last": "Chiew",
                    "suffix": ""
                },
                {
                    "first": "WX",
                    "middle": [],
                    "last": "Khong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Travel Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1093/jtm/taaa039"
                ]
            }
        },
        "BIBREF13": {
            "title": "Population-level consequences of heterospecific density-dependent movements in predator-prey systems.",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Sj\u00f6din",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Br\u00e4nnstr\u00f6m",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "S\u00f6derquist",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Englund",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "J Theor Biol",
            "volume": "342",
            "issn": "",
            "pages": "93-106",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jtbi.2013.09.019"
                ]
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "SARS-CoV-2 Viral Load in Upper Respiratory Specimens of Infected Patients.",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zou",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Ruan",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Liang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Hong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "12",
            "pages": "1177-9",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMc2001737"
                ]
            }
        },
        "BIBREF17": {
            "title": "Early Transmission Dynamics in Wuhan, China, of Novel Coronavirus-Infected Pneumonia.",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Tong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "13",
            "pages": "1199-207",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001316"
                ]
            }
        },
        "BIBREF18": {
            "title": "Estimating the asymptomatic proportion of coronavirus disease 2019 (COVID-19) cases on board the Diamond Princess cruise ship, Yokohama, Japan, 2020.",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Mizumoto",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kagaya",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Zarebski",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Chowell",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Euro Surveill",
            "volume": "25",
            "issn": "10",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.2807/1560-7917.ES.2020.25.10.2000180"
                ]
            }
        },
        "BIBREF19": {
            "title": "Estimation of the asymptomatic ratio of novel coronavirus infections (COVID-19).",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Nishiura",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kobayashi",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Suzuki",
                    "suffix": ""
                },
                {
                    "first": "SM",
                    "middle": [],
                    "last": "Jung",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Hayashi",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Kinoshita",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Int J Infect Dis",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1016/j.ijid.2020.03.020"
                ]
            }
        }
    }
}