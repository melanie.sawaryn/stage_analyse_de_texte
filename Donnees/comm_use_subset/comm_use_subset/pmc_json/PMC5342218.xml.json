{
    "paper_id": "PMC5342218",
    "metadata": {
        "title": "Worry experienced during the 2015 Middle East Respiratory Syndrome (MERS) pandemic in Korea",
        "authors": [
            {
                "first": "Jun-Soo",
                "middle": [],
                "last": "Ro",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jin-Seok",
                "middle": [],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sung-Chan",
                "middle": [],
                "last": "Kang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hye-Min",
                "middle": [],
                "last": "Jung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Gui-Quan",
                "middle": [],
                "last": "Sun",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Korea recently experienced an outbreak of Middle East Respiratory Syndrome (MERS). According to the Korea Center for Disease Control and Prevention (KCDC), 186 people were definitively diagnosed with MERS between May 20 and November 30, 2015; of those, 38 died from the disease. The people diagnosed with MERS consisted of 82 hospital patients, 65 family member or visitors, and 39 hospital workers. Moreover, 16,752 individuals were quarantined for infection management [1]. Simulating the disease spread process during the period of MERS by using a mathematical model, the result was 4.42 R0 value that indicating infectivity [2]. According to the criteria set by the World Health Organization (WHO), the MERS pandemic ended by late December; however, the Korean government actually declared late July as the end of the pandemic.",
            "cite_spans": [
                {
                    "start": 472,
                    "end": 473,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 629,
                    "end": 630,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The Korea-WHO MERS Joint Mission determined that the MERS outbreak in Korea was due to initial response failure by the Korean government. The cause of this initial response failure was attributed a lack of transparent disclosure and timely information [3].",
            "cite_spans": [
                {
                    "start": 253,
                    "end": 254,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "According to the Crisis and Emergency Risk Communication (CERC) guidelines from the US Center for Disease Control and Prevention (CDC), when a national crisis, such as infectious epidemic, occurs, the most important action is timely dissemination spread of information and communication. However, the Korean government was unable to disclose information immediately after the MERS outbreak, which resulted in outbreak spread and increased levels of worry about MERS among its citizens [4].",
            "cite_spans": [
                {
                    "start": 486,
                    "end": 487,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Disease-related worry plays an important role in disease management, especially with contagious disease management. Disease-related worry is an emotional response to a disease and it is known to have a positive effect on disease management by providing individuals with motivation to participate in health promotion activities [5\u20137]. However, at times, disease-related worry can result in stigmatization, which can have a negative effect on disease management.",
            "cite_spans": [
                {
                    "start": 328,
                    "end": 329,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 330,
                    "end": 331,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Due to the government\u2019s failure to share timely information in the early stage of the MERS outbreak in Korea, the outbreak increased worries among Korean citizens and presented difficulties in disease management. Therefore, in order to effectively manage future disease outbreaks, it is important to understand the levels of disease-related worry and its influencing factors. Moreover, assessment of the levels of disease-related worry by key outbreak periods may also inform effective intervention points.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "However, virtually no studies to date in Korea have examined disease-related worry, particularly contagious disease-related worry, during disease outbreak and its influencing factors. To our knowledge, no study has investigated disease-related worry according to specific outbreak periods. Accordingly, the objective of the present study was to identify the overall level of MERS-related worry during the MERS outbreak period as well as the levels of disease-related worry during key MERS outbreak periods. Structure of the study is shown in Fig 1.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": [
                {
                    "start": 542,
                    "end": 547,
                    "mention": "Fig 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The participants in this study consisted of 1,000 adults more than 18 years old who resided in Korea. The sample was selected by proportional stratified sampling between panels of professional research agency (Research & Research Cooperation). For the representativeness, the sample proportion of was determined by sexual, regional, age population ratio in Korea. Anyone who submitted his or her E-mail address and reply 2\u20133 simple questions via website of our research agency could be a panel. The survey was conducted in an online format from August 24th, 2015 to September 11th, 2016.",
            "cite_spans": [],
            "section": "Participants ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "This study received a review exemption from the institutional review board (IRB) of Seoul National University Hospital (IRB No.1508-064-694).",
            "cite_spans": [],
            "section": "Participants ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "So the dependent variables in the study were the overall level of worry during the MERS outbreak and the level of worry during specific outbreak periods. The overall level of worry during the MERS outbreak was surveyed with a five-point scale, using the question (S2 and S3 Files), \u201cHave you ever worried about being infected with MERS during the MERS outbreak period?\u201d The level of MERS-related worry according to specific outbreak periods was also surveyed with a five-point scale, using the question (S2 and S3 Files), \u201cDid you worry about being infected with MERS when the first patient with MERS had a definitive diagnosis/when the first patient with MERS died/when the number of patients continued to rise/when MERS outbreak ended?\u201d. The MERS-related worry question was based on a question used in a precedent study [8].",
            "cite_spans": [
                {
                    "start": 823,
                    "end": 824,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Survey instrument ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "Demographic variables for identification of participant characteristics included age, sex, education level, average income per month, type of medical insurance and occupation type. In addition, to assess the physical and mental status of the participants, self-rated health status, chronic disease status (hypertension, diabetes) and daily life stress level were surveyed (S2 and S3 Files). The Survey conducted by classified age groups: \u201819\u201329\u2019, \u2018thirties\u2019, \u2018forties\u2019, \u2018fifties\u2019, \u2018over sixties\u2019. In this analysis, Age groups are categorized as \u2018under 29\u2019, \u2018over thirty under forty\u2019 and \u2018over fifty\u2019 following by precedent study [9]. The Survey answer types of a education level were classified in \u2018below middle school graduation\u2019, \u2018high school graduation\u2019, \u2018College student or graduation\u2019 and \u2018graduate school student or graduation\u2019. In this analysis, Education levels are categorized as \u2018below high school graduation\u2019, \u2018College student or graduation\u2019, and \u2018graduate school student or graduation\u2019. The classification needed is necessary in this study because the college entrance rate is high (approximately 70%) in Korea. The Survey answer types of an occupation were categorized in \u2018Agriculture/Fishing/Forestry/Animal Husbandry\u2019, \u2018Self-employment\u2019, \u2018Sales/Service\u2019, \u2018Technical engineer/Skilled labor\u2019, \u2018Simple labor(Production employee, Sweeper, Janitor etc.)\u2019, \u2018Office job(Office worker, Public officer etc.)\u2019, \u2018Manager\u2019, \u2018Specialized job(Lawyer, Doctor, Architect, Professor etc.)\u2019, \u2018Housewife\u2019, \u2018Student\u2019 and \u2018unemployed\u2019. In this analysis, the three of occupations: \u2018office job\u2019, \u2018Manager\u2019 and \u2018specialized job\u2019 are classified as a white collar worker. \u2018Housewife\u2019, \u2018student\u2019 and \u2018unemployed\u2019 are classified as an \u2018unemployed\u2019. The other occupations are classified as a blue collar worker. The Survey answer types of medical insurance status were categorized in \u2018Regional medical insurance\u2019, \u2018Workplace medical insurance\u2019 and \u2018Medical Aid\u2019. In this analysis, medical insurance statuses are categorized in \u2018Medical insurance\u2019 or \u2018Medical Aid\u2019. The Survey answer types of income per a month were categorized in \u2018Below 1,000$\u2019, \u20181,000$-2,000$\u2019, \u20182,000$-3,000$\u2019, \u20183,000$-4,000$\u2019, \u20184,000$-5,000$\u2019, \u20185,000$-6,000$\u2019, \u2018Over 6,000$\u2019. In this analysis, incomes are categorized in \u2018Under 2,000$\u2019, \u20182,000$-4,000$\u2019, \u20184,000$-6,000$\u2019, \u2018Over 6,000$\u2019 following by income quartile in Korea. The five-point scale survey conducted on Self-rated health status followed by quartile of income status in Korea. In this survey, if respondents have hypertension or diabetes, they categorized in \u2018Chronic disease present\u2019. Stress level in daily life was surveyed with a five-point scale.",
            "cite_spans": [
                {
                    "start": 630,
                    "end": 631,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Survey instrument ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "A univariate analysis using t-test and one-way analysis of variance (AVOVA) was performed to examine the relationships between the overall level of MERS-related worry and each independent variable. Moreover, to control for confounding variables, a multivariate analysis was performed through an ordinal logistic regression. The reliability of the level of MERS-related worry by outbreak periods was analyzed via Fleiss Kappa and intra-class correlation (ICC). IBM SPSS Statistics for Windows, version 20.0 was used for all analyses.",
            "cite_spans": [],
            "section": "Statistical analysis ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "The results of univariate analysis between the general characteristics of the participants and the overall level of MERS-related worry are shown in Table 1. The mean MERS-related worry score, scored on a five-point scale, was 2.44 points. In the univariate analysis, sex, self-rated health status, and daily life stress level showed statistically significant results. Men showed a lower overall level of MERS-related worry than women did. The group of participants that reported \u2018very good\u2019 health status showed a lower overall level of MERS-related worry than the groups that responded \u2018bad\u2019, \u2018normal\u2019, and \u2018good\u2019. Moreover, the group that responded \u2018little\u2019 to daily life stress had a lower level of MERS-related worry than group that responded \u2018very much\u2019.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 148,
                    "end": 155,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Ordinal logistic regression analysis results also revealed statistically significant associations with overall level of MERS-related worry with respect to sex, self-rated health status, and daily life stress, just as in the univariate analysis. Men showed lower odds ratios (ORs) than women. The group that responded as having \u2018very good\u2019 self-rated health status showed lower ORs than all other groups, except for the group that responded \u2018very bad\u2019. Moreover, the group that responded as having \u2018little\u2019 daily life stress showed lower ORs than the group that responded as having \u2018very much\u2019 daily life stress (Table 2). These results satisfied the parallel assumptions of ordinal logistic regression.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 612,
                    "end": 619,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The levels of MERS-related worry by MERS outbreak periods showed scores of 2.32, 2.45, 2.73, and 2.24 when the first patient with MERS had a definitive diagnosis (index case), the first patient with MERS died (first death), the number of patients continued to rise (increasing patients), and the MERS outbreak ended (end of outbreak), respectively (Table 3).",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 349,
                    "end": 356,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The scores from the levels of MERS-related worry by MERS outbreak periods were compared via a reliability test. Fleiss Kappa and intra-class correlation test results showed that worry scores from each period were statistically significantly consistent (Table 4).",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 253,
                    "end": 260,
                    "mention": "Table 4",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The objective of the present study was to investigate the overall level of MERS-related worry among the Korean population during the MERS outbreak, its influencing factors, and levels of worry during key outbreak periods, in order to assist management of future disease outbreaks.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The mean overall MERS-related worry score was 2.44 points. According to a 2006 study by Chapman et al., when American college employees were surveyed during 2001\u20132002 regarding their worry about influenza, the level of worry ranged from 1.93 to 2.24 points, depending on whether the person had been vaccinated for influenza [10]. In comparison, the level of MERS-related worry in Korea was higher than that of level of worry associated with infection by usual respiratory diseases.",
            "cite_spans": [
                {
                    "start": 325,
                    "end": 327,
                    "mention": "10",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Until now, no precedent studies have reported on MERS-related worry. However, many precedent studies have investigated the level of worry during influenza A (H1N1) pandemics. As these studies used different instruments from the ones used in this study, it couldn\u2019t be compared directly. However, the overall trend showed higher levels of worry than the MERS-related worry found in the present this study. In a study conducted in the Netherlands, a five-point scale was used to investigate the level of worry, just as that used in the present study [9]. In that study, 36% of the respondents reported feeling worried to very worried in 36%, a rate higher than the 9.2% of worried to very worried responses during the first period (index case observed) in the present study. In a study conducted in Saudi Arabia, the level of concern was investigated at three intervals, and a relatively high response \u2018extremely concerned\u2019 (54.3%) was observed among respondents [11]. A study conducted in Malaysia and Europe investigated the levels of concern in four intervals and found that 26% and 25% felt very concerned and somewhat concerned, respectively [12].",
            "cite_spans": [
                {
                    "start": 549,
                    "end": 550,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 962,
                    "end": 964,
                    "mention": "11",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1146,
                    "end": 1148,
                    "mention": "12",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Ordinal logistic regression was used to analyze the influencing factors of the overall level of MERS-related worry, revealing statistically significant results for sex, self-rated health status, and daily life stress. With respect to sex, men showed lower ORs than women, indicating higher level of MERS-related worry among women. This finding was consistent with the results of a precedent study [13]. The level of worry was likely higher in women was because they have higher perceived susceptibility. Precedent studies have perceived susceptibility to be higher in women than in men, and higher perceived susceptibility results in increased disease-related worry [14, 15].",
            "cite_spans": [
                {
                    "start": 398,
                    "end": 400,
                    "mention": "13",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 667,
                    "end": 669,
                    "mention": "14",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 671,
                    "end": 673,
                    "mention": "15",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The group that reported their health status to be \u2018very good\u2019 had lower ORs than all other groups, a finding that can be interpreted two ways. First, the group that responded as having very good self-rated health status had confidence in their physical health, thereby having a lower perceived susceptibility than the other groups. The second reason may be attributed to differences in the level of trust in informal information. According to a precedent study, having greater trust in informal information correlates with increased levels of disease-related worry [15]. The group that reported having very good self-rated health status had greater interest in health than the other groups; as a result, they may have increased interest in the situation involving the MERS outbreak and acquiring more information on the disease and methods for its prevention. Because of this, they had less trust in informal information, which can result in lower levels of MERS-related worry.",
            "cite_spans": [
                {
                    "start": 566,
                    "end": 568,
                    "mention": "15",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The group that reported having very high daily life stress showed higher ORs than the group that reported having little stress. This suggests that the group with very high daily life stress was mentally vulnerable and more sensitive to the MERS outbreak.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Investigation of level of worry by key outbreak periods demonstrated that the level of worry increased from the point of the index case, to the first death, and to the period when the disease was spreading. The level of worry then decreased at the point of outbreak ending. The reliability test results showed statistically significantly consistent for scores from all four periods. This finding can be interpreted to mean that the level of worry in the initial stage was closely associated with the level of worry in the stages that followed.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Increase in the level of worry over time contradicts findings in other studies. According to a study from the Netherlands conducted during an influenza A (H1N1) pandemic, the level of worry was highest in the initial stage, after which the level decreased and was maintained at a consistently low level. A study from Hong Kong also indicated that the level of perceived worry was high initially, but continued to decrease thereafter. The reason for these somewhat conflicting results in comparison to precedent studies is because the influenza A (H1N1) outbreak was touted early on by the government as a very serious disease, which resulted in high levels of worry. However, as people gradually realized that the disease was not much different than any previous influenza outbreaks, the level of worry decreased accordingly [10, 16]. In contrast, in the early stage of MERS outbreak in Korea, it was announced that the disease was not any more serious than other influenzas; however, the increasing number of infected patients and mortalities is believed to have resulted in increased levels of worry. These findings can be used as evidence that effectively reducing the level of worry in the population during the early stage would result in a persistently low level of worry.",
            "cite_spans": [
                {
                    "start": 826,
                    "end": 828,
                    "mention": "10",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 830,
                    "end": 832,
                    "mention": "16",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Precedent studies on emerging respiratory infectious disease also emphasize the importance of formal outbreak prevention information. A 2010 study by Cowling et al. attributed the lack of risk communication as the reason why the initial perceived susceptibility in the population did not change even when the 2009 influenza A (H1N1) outbreak in Hong Kong continued to spread [16], while a 2009 study by Raude et al [13]. indicated that not providing sufficient information during the pandemic of influenza A (H1N1) had a negative effect on the prevention and spread of influenza. In particular, a 2009 study on the swine flu, conducted by Rubin et al., reported that providing people with practical information to increase their knowledge of infection prevention and outbreak is important, and that emphasizing recommended preventative measures and possible outbreak duration can be helpful in managing the disease outbreak. These study results are in agreement with the findings of the present study [17].",
            "cite_spans": [
                {
                    "start": 376,
                    "end": 378,
                    "mention": "16",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 416,
                    "end": 418,
                    "mention": "13",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1002,
                    "end": 1004,
                    "mention": "17",
                    "ref_id": "BIBREF16"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The public health implications of this study are as follows. It is effective to control outbreaks by lowering the perceived susceptibility and successfully controlling the spread of informal information in the early stage of the epidemic. Recent research has shown that the changes of spatial diffusion of disease predict an epidemic, so it can be using early warning indicators. Therefore, continual checking the spatial spread of disease at the every stage is helpful to disease-related worry intervention [18]. According to the Suggestion from the precedent study that analyzing the MERS epidemic in Korea through mathematical model, the interventions; limiting the theatrical performances such as rallies and fairs, publishing information on diseases at appropriate times and distributing them to the local community could also be helpful to manage of disease. Through these interventions, it is possible to lowering down the perceived susceptibility rate and preventing the spread of informal information [2].",
            "cite_spans": [
                {
                    "start": 509,
                    "end": 511,
                    "mention": "18",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 1011,
                    "end": 1012,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The present study had several limitations. First, the present study was a cross-sectional study; as such, it may contain recall bias, which can especially influence the level of MERS-related worry during key outbreak periods. Because the worry scores for the four different periods were surveyed simultaneously, approximately three months had elapsed from the time of the index case to when the survey was conducted; therefore, the levels of worry during the corresponding periods may not have been recalled accurately, thereby resulting in inaccurate responses. Future studies should conduct prospective surveys throughout the duration of infection outbreaks in order to more accurately assess the levels of disease-related worry during key outbreak periods. Second, emotional responses to epidemics can be assessed using various questions. Besides the experienced worry assessed in the present study, various other questions, including anxiety level, may offer a deeper understanding of the emotional responses to epidemics.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "This study analyzed factors associated with level of worry during the MERS outbreak period. The factors that decreased the level of MERS-related worry were being men and having very good self-rated health status, whereas having very high daily life stress increased the level of worry. These findings were to suggest that the level of worry increased among those with higher perceived susceptibility and with greater trust in informal information. Moreover, with respect to the levels of worry according to MERS outbreak periods, the level of worry in the initial stage was closely associated with the levels during the stages that followed. This observation underscores the importance of managing the level of worry in the early stage of outbreaks through proper intervention. Therefore, for proper disease management, it is necessary to make efforts to reduce the level of perceived susceptibility by disseminating timely and accurate information on disease prevention measures and current status through formal routes in the early stage of the outbreak, in addition to reducing the level of worry in the early stage by blocking distribution of informal information. For this, it is helpful to outbreak control by publishing disease information at the appropriate time and distributing it to the community, and to educate each group about disease prevention in a personalized manner.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Modeling the Transmission of Middle East Respirator Syndrome Corona Virus in the Republic of Korea",
            "authors": [],
            "year": 2015,
            "venue": "PloS one",
            "volume": "10",
            "issn": "12",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Public health crisis response and establishment of a crisis communication system in South Korea: lessons learned from the MERS outbreak",
            "authors": [],
            "year": 2015,
            "venue": "Journal of the Korean Medical Association/Taehan Uisa Hyophoe Chi",
            "volume": "58",
            "issn": "",
            "pages": "624-634",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Responses to information about psychosocial consequences of genetic testing for breast cancer susceptibility: Influences of cancer worry and risk perceptions",
            "authors": [],
            "year": 2001,
            "venue": "J Health Psychol",
            "volume": "6",
            "issn": "",
            "pages": "47-59",
            "other_ids": {
                "DOI": [
                    "10.1177/135910530100600104"
                ]
            }
        },
        "BIBREF5": {
            "title": "Risk perceptions, worry, and attitudes about genetic testing for breast cancer susceptibility",
            "authors": [],
            "year": 2006,
            "venue": "Psychol Health",
            "volume": "21",
            "issn": "",
            "pages": "211-230",
            "other_ids": {
                "DOI": [
                    "10.1080/14768320500230318"
                ]
            }
        },
        "BIBREF6": {
            "title": "Specific worry about breast cancer predicts mammography use in women at risk for breast and ovarian cancer",
            "authors": [],
            "year": 1999,
            "venue": "Health Psychol",
            "volume": "18",
            "issn": "",
            "pages": "532-536",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Anxiety, worry and cognitive risk estimate in relation to protective behaviors during the 2009 influenza A/H1N1 pandemic in Hong Kong: ten cross-sectional surveys",
            "authors": [],
            "year": 2014,
            "venue": "BMC Infect Dis",
            "volume": "14",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/1471-2334-14-169"
                ]
            }
        },
        "BIBREF8": {
            "title": "Perceived risk, anxiety, and behavioural responses of the general public during the early phase of the Influenza A (H1N1) pandemic in the Netherlands: results of three consecutive online surveys",
            "authors": [],
            "year": 2001,
            "venue": "BMC Public Health",
            "volume": "11",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Emotions and preventive health behavior: worry, regret, and influenza vaccination",
            "authors": [],
            "year": 2006,
            "venue": "Health Psychol",
            "volume": "25",
            "issn": "",
            "pages": "82-90",
            "other_ids": {
                "DOI": [
                    "10.1037/0278-6133.25.1.82"
                ]
            }
        },
        "BIBREF10": {
            "title": "Awareness, attitudes, and practices related to the swine influenza pandemic among the Saudi public",
            "authors": [],
            "year": 2010,
            "venue": "BMC Infect Dis",
            "volume": "10",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Initial psychological responses to Influenza A, H1N1 (\"Swine flu\")",
            "authors": [],
            "year": 2009,
            "venue": "BMC Infect Dis",
            "volume": "9",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Lay perceptions of the pandemic influenza threat",
            "authors": [],
            "year": 2009,
            "venue": "Eur J Epidemiol",
            "volume": "24",
            "issn": "",
            "pages": "339-342",
            "other_ids": {
                "DOI": [
                    "10.1007/s10654-009-9351-x"
                ]
            }
        },
        "BIBREF13": {
            "title": "Knowledge and attitudes of university students toward pandemic influenza: a cross-sectional study from Turkey",
            "authors": [],
            "year": 2010,
            "venue": "BMC Public Health",
            "volume": "10",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "The influence of social-cognitive factors on personal hygiene practices to protect against influenzas: using modelling to compare avian A/H5N1 and 2009 pandemic A/H1N1 influenzas in Hong Kong",
            "authors": [],
            "year": 2011,
            "venue": "Int J Behav Med",
            "volume": "18",
            "issn": "",
            "pages": "93-104",
            "other_ids": {
                "DOI": [
                    "10.1007/s12529-010-9123-8"
                ]
            }
        },
        "BIBREF15": {
            "title": "Community psychological and behavioral responses through the first wave of the 2009 influenza A (H1N1) pandemic in Hong Kong",
            "authors": [],
            "year": 2010,
            "venue": "J Infect Dis",
            "volume": "202",
            "issn": "",
            "pages": "867-876",
            "other_ids": {
                "DOI": [
                    "10.1086/655811"
                ]
            }
        },
        "BIBREF16": {
            "title": "Public perceptions, anxiety, and behaviour change in relation to the swine flu outbreak: cross sectional telephone survey",
            "authors": [],
            "year": 2009,
            "venue": "BMJ",
            "volume": "339",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1136/bmj.b2651"
                ]
            }
        },
        "BIBREF17": {
            "title": "Pattern transitions in spatial epidemics: Mechanisms and emergent properties",
            "authors": [],
            "year": 2016,
            "venue": "Physics of Life Reviews",
            "volume": "19",
            "issn": "",
            "pages": "43-73",
            "other_ids": {
                "DOI": [
                    "10.1016/j.plrev.2016.08.002"
                ]
            }
        }
    }
}