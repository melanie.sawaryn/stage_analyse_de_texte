{
    "paper_id": "PMC7154566",
    "metadata": {
        "title": "Why tocilizumab could be an effective treatment for severe COVID-19?",
        "authors": [
            {
                "first": "Binqing",
                "middle": [],
                "last": "Fu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xiaoling",
                "middle": [],
                "last": "Xu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Haiming",
                "middle": [],
                "last": "Wei",
                "suffix": "",
                "email": "ustcwhm@ustc.edu.cn",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In the past decades, two known pathogenic human coronaviruses, severe acute respiratory syndrome CoV (SARS-CoV) and Middle East respiratory syndrome CoV (MERS-CoV), have been reported to damage the respiratory tract and cause high morbidity and mortality [1]. Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) is a newly discovered coronavirus, was reported at December 2019 (2019-nCoV) in the city Wuhan, Hubei province, China [2]. Up to 21th of March 2020, 81,416 cases have been reported with 3261 fatal cases according to the Chinese Center for Disease Control and Prevention (CDC). Meanwhile, 190,000 cases have been reported with 7992 fatal cases in other countries except China. In Italy, to date there are about 47.021 infected and 4.032 deaths [3]. A global outbreak of the SARS-CoV-2 caused Corona Virus Disease (COVID-19) seems inevitable. Among these COVID-19 patients, most of them have the common symptoms including fever, cough, and myalgia or fatigue at onset. The majority of patients can recover, however, about 25% of patients will progress into severe complications including acute respiratory distress syndrome (ARDS), which may worsen rapidly into respiratory failure, need an intensive care unit (ICU) and even cause multiple organ failure [4, 5]. Therefore, the exploration for the mortality causes and advancing novel therapeutic development of severe COVID-19 is crucially important at the moment.",
            "cite_spans": [
                {
                    "start": 256,
                    "end": 257,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 440,
                    "end": 441,
                    "mention": "2",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 765,
                    "end": 766,
                    "mention": "3",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1275,
                    "end": 1276,
                    "mention": "4",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1278,
                    "end": 1279,
                    "mention": "5",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Although virus-induced cytopathic effects and viral evasion of host immune responses are believed to be important in disease severity, studies from humans who died of SARS and MERS suggested that an aberrant host immune response resulting in an inflammatory cytokine storm and lethal disease [1]. Similar to the inflammatory cytokines in SARS and MERS, patients with COVID-19 also have increased plasma concentrations of inflammatory cytokines, such as tumour necrosis factor \u03b1 (TNF-\u03b1),interleukins (IL) 2, 7, and 10, granulocyte-colony stimulating factor (G-CSF), monocyte chemoattractant protein 1, macrophage inflammatory protein 1 alpha, and interferon-\u03b3-inducible protein 10, especially in ICU patients, which implied a cytokine storm occurred [4].",
            "cite_spans": [
                {
                    "start": 293,
                    "end": 294,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 750,
                    "end": 751,
                    "mention": "4",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "What is the crucial cause for mortality in COVID-19?",
            "ref_spans": []
        },
        {
            "text": "Moreover, COVID-19 patients have decreased lymphocytes in peripheral blood and characteristic pulmonary ground glass changes on imaging [4, 5]. Most importantly, in the biopsy samples at autopsy from patients who died from COVID-19, histological examination showed bilateral diffuse alveolar damage including edema, proteinaceous exudate, focal reactive hyperplasia of pneumocytes with patchy inflammatory cellular infiltration, and multinucleated giant cells [6, 7]. It also has been recovered from autopsy examination that Type II alveolar epithelial cells proliferate markedly, with some cells exfoliated. The alveolar septum is hyperemic, edematous, with clear intravascular thrombosis. Focal monocytes, lymphocytes and plasma cells are infiltrating into pulmonary interstitium. Immunohistochemistry results showed positive for immunity cells including CD3, CD4, CD8, CD20, CD79a, CD5, CD38 and CD68 [8]. These phenomena further suggest severe pulmonary inflammatory immune cells exist in SARS-CoV-2 infection. Therefore, increased alveolar exudate caused by aberrant host immune response and inflammatory cytokine storm probably impedes alveolar gas exchange and contributes to the high mortality of severe COVID-19 patients.",
            "cite_spans": [
                {
                    "start": 137,
                    "end": 138,
                    "mention": "4",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 140,
                    "end": 141,
                    "mention": "5",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 461,
                    "end": 462,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 464,
                    "end": 465,
                    "mention": "7",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 905,
                    "end": 906,
                    "mention": "8",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "What is the crucial cause for mortality in COVID-19?",
            "ref_spans": []
        },
        {
            "text": "Inflammatory storm refers to an excessive inflammatory response flaring out of control and the immune system gone awry. To identify which kind of immune cells are involved in and which inflammatory cytokine is the critical target in these severe COVID-19 patients, we analyzed peripheral blood samples from patients with severe or critical COVID-19 from The First Affiliated Hospital of University of Science and Technology of China and observed monocytes and T cells from severe or critical COVID-19 patients decreased significantly compared to normal controls. These aberrant pathogenic T cells from critical ICU care COVID-19 patients showed activated characteristic accompanied with co-expressing IFN-\u03b3 and GM-CSF. This phenomenon aroused our alarm, for GM-CSF has the capability to control diverse pathogenic capabilities of inflammatory myeloid cells, especially monocytes [9]. As expected, inflammatory monocyte with CD14+CD16+ phenotype exists in peripheral blood of COVID-19 patients and has larger population in critical COVID-19 patients from ICU. Note that without any re-stimulation with PMA or incubation with monensin, large amount of IL-6 could be tested from these inflammatory monocytes especially in ICU patients. Therefore, these pathogenic Th1 cells (GM-CSF+IFN-\u03b3+) and inflammatory monocytes (CD14+CD16+ with high expression of IL-6) exist especially in critical ICU COVID-19 patients [10]. Given that large amount of mononuclear inflammatory lymphocytes have been observed in the biopsy samples at autopsy from COVID-19 patients, we believe that these pathogenic T cells and inflammatory monocytes may enter the pulmonary circulation in large numbers and incite inflammatory storm in severe or critical COVID-19 patients (Fig. 1).",
            "cite_spans": [
                {
                    "start": 880,
                    "end": 881,
                    "mention": "9",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1408,
                    "end": 1410,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "IL-6 is a potential blocking target to calm inflammatory storm",
            "ref_spans": [
                {
                    "start": 1750,
                    "end": 1751,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Tocilizumab is the first marketed IL-6 blocking antibody through targeting IL-6 receptors and has proved its safety and effectiveness in therapy for rheumatoid arthritis (Fig. 2). In order to verify whether targeted IL-6, may potentially be the effective and safe way to reduce mortality of COVID-19, 21 patients diagnosed as severe or critical COVID-19 from The First Affiliated Hospital of University of Science and Technology of China and Anhui Fuyang Second People\u2019s Hospital were recruited and given tocilizumab therapy (Table 1). Patients received standard treatment according to the Diagnosis and Treatment Protocol for COVID-19 (7th edition), including lopinavir, methylprednisolone, other symptom relievers and oxygen therapy. The results of tocilizumab treatment are inspiring. The temperature of all the patients returned to normal very quickly. The respiratory function and all other symptoms improved remarkably. Among these 21 patients, 20 patients have been recovered and discharged within 2 weeks after the tocilizumab therapy. One left patient is recovering and out of ICU care. No adverse drug reactions were reported during the treatment with tocilizumab [11]. With these promising preliminary clinical results, we further launched the multicenter, large-scale clinical trials (ChiCTR2000029765) and have already about 500 severe or critical patients treated this way.",
            "cite_spans": [
                {
                    "start": 1175,
                    "end": 1177,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Tocilizumab treatment is effective to reduce\u00a0the mortality of severe COVID-19",
            "ref_spans": [
                {
                    "start": 176,
                    "end": 177,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 532,
                    "end": 533,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The immunotherapy strategy about Tocilizumab treatment has been formally included in the diagnosis and treatment program of COVID-19 (7th edition) of the national health commission of China since 3th March 2020 as following: Tocilizumab can be used in patients with extensive bilateral lung lesions opacity or in severe or critical patients, who have elevated laboratory detected IL-6 levels. The first dose is 4\u20138 mg/kg (the recommended dose is 400 mg, diluted to 100 ml with 0.9% normal saline, and the infusion time is more than 1 h). For patients with poor initial efficacy, an additional application can be made after 12 h (the dose is the same as before). The maximum number of times of administration is two, and the maximum dose of a single dose should not exceed 800 mg. Note that patients with allergic reactions, such as tuberculosis and other active infection are contraindicated. We suggest that IL-6 concentrations can be detected if fever persists for more than 3 days. By chemiluminescence detection, if serum IL-6 content is over 20 pg/ml, Tocilizumab can be used. The IL-6 will be temporarily increased in serum in the next few days, for its receptors have been blocked by Tocilizumab. Together, Tocilizumab treatment is recommended to reduce the mortality of severe COVID-19.",
            "cite_spans": [],
            "section": "Tocilizumab treatment is effective to reduce\u00a0the mortality of severe COVID-19",
            "ref_spans": []
        },
        {
            "text": "All three coronaviruses, including SARS-CoV, MERS-CoV and SARS-CoV-2, induce aberrant non-effective host immune responses that are associated with severe lung pathology. The new SARS-CoV-2 additionally causes serious alveolar mucus infiltration and multiple organ failure. As the SARS-CoV-2 continues to spread, the numbers of fatal cases rise exponentially in many countries, advancing novel therapeutic development becomes crucial to minimize the number of deaths from COVID-19. In the absence of specific antiviral drugs, existing host-directed therapies could potentially be repurposed to treat COVID-19. China\u2019s plan of Tocilizumab treatment has shown its remarkable effectiveness and safety in clinical practice over the past 2 months, hoping it will benefit other countries fighting the pandemic and reduce the mortality of severe COVID-19 as well.\n",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table\u00a01: Patients can be considered to use Tocilizumab and the exclusion criteria\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig.\u00a01: Pathogenic T cells and inflammatory monocytes with high IL-6 secretion may enter the pulmonary circulation in large numbers,incite the inflammatory storm and lead an immune disorder in severe COVID-19 patients",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig.\u00a02: Tocilizumab calms the inflammatory storm through blocking IL-6 receptors",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Pathogenic human coronavirus infections: causes and consequences of cytokine storm and immunopathology",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Channappanavar",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Perlman",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Semin Immunopathol",
            "volume": "39",
            "issn": "",
            "pages": "529-539",
            "other_ids": {
                "DOI": [
                    "10.1007/s00281-017-0629-x"
                ]
            }
        },
        "BIBREF1": {
            "title": "Pathogenic T cells and inflammatory monocytes incite inflammatory storm in severe COVID-19 patients",
            "authors": [
                {
                    "first": "YG",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "BQ",
                    "middle": [],
                    "last": "Fu",
                    "suffix": ""
                },
                {
                    "first": "XH",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "DS",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "YJ",
                    "middle": [],
                    "last": "Qi",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Sun",
                    "suffix": ""
                },
                {
                    "first": "ZG",
                    "middle": [],
                    "last": "Tian",
                    "suffix": ""
                },
                {
                    "first": "XL",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "HM",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Nat Sci Rev",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1093/nsr/nwaa041"
                ]
            }
        },
        "BIBREF2": {
            "title": "Effective treatment of severe COVID-19 patients with tocilizumab",
            "authors": [
                {
                    "first": "XL",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "MF",
                    "middle": [],
                    "last": "Han",
                    "suffix": ""
                },
                {
                    "first": "TT",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Sun",
                    "suffix": ""
                },
                {
                    "first": "DS",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "BQ",
                    "middle": [],
                    "last": "Fu",
                    "suffix": ""
                },
                {
                    "first": "YG",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "XH",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "XY",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "XH",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "AJ",
                    "middle": [],
                    "last": "Pan",
                    "suffix": ""
                },
                {
                    "first": "HM",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "ChinaXiv",
            "volume": "202003",
            "issn": "00026",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "A novel coronavirus outbreak of global health concern",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "PW",
                    "middle": [],
                    "last": "Horby",
                    "suffix": ""
                },
                {
                    "first": "FG",
                    "middle": [],
                    "last": "Hayden",
                    "suffix": ""
                },
                {
                    "first": "GF",
                    "middle": [],
                    "last": "Gao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "10223",
            "pages": "470-473",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30185-9"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan, China",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Ren",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Gu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "10223",
            "pages": "497-506",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30183-5"
                ]
            }
        },
        "BIBREF6": {
            "title": "Clinical characteristics of 138 hospitalized patients With 2019 novel coronavirus-infected pneumonia in Wuhan, China",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Xiang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Cheng",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Xiong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.1585"
                ]
            }
        },
        "BIBREF7": {
            "title": "Pulmonary pathology of early phase 2019 novel coronavirus (COVID-19) pneumonia in two patients with lung cancer",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Tian",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Niu",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Xiao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Thorac Oncol",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1016/j.jtho.2020.02.010"
                ]
            }
        },
        "BIBREF8": {
            "title": "Pathological findings of COVID-19 associated with acute respiratory distress syndrome",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet Respir Med",
            "volume": "8",
            "issn": "4",
            "pages": "420-422",
            "other_ids": {
                "DOI": [
                    "10.1016/S2213-2600(20)30076-X"
                ]
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "The cytokine GM-CSF drives the inflammatory signature of CCR2\u2009+\u2009monocytes and licenses autoimmunity",
            "authors": [
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Croxford",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Lanzinger",
                    "suffix": ""
                },
                {
                    "first": "FJ",
                    "middle": [],
                    "last": "Hartmann",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Schreiner",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Mair",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Pelczar",
                    "suffix": ""
                },
                {
                    "first": "BE",
                    "middle": [],
                    "last": "Clausen",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Jung",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Greter",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Becher",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Immunity",
            "volume": "43",
            "issn": "",
            "pages": "502-514",
            "other_ids": {
                "DOI": [
                    "10.1016/j.immuni.2015.08.010"
                ]
            }
        }
    }
}