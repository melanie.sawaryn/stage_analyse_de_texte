{
    "paper_id": "PMC7048229",
    "metadata": {
        "title": "Molecular and serological investigation of 2019-nCoV infected patients: implication of multiple shedding routes",
        "authors": [
            {
                "first": "Wei",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rong-Hui",
                "middle": [],
                "last": "Du",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bei",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xiao-Shuang",
                "middle": [],
                "last": "Zheng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xing-Lou",
                "middle": [],
                "last": "Yang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ben",
                "middle": [],
                "last": "Hu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yan-Yi",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Geng-Fu",
                "middle": [],
                "last": "Xiao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bing",
                "middle": [],
                "last": "Yan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Zheng-Li",
                "middle": [],
                "last": "Shi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peng",
                "middle": [],
                "last": "Zhou",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Coronaviruses (CoVs) belong to the subfamily Orthocoronavirinae in the family Coronaviridae and the order Nidovirales. A human coronavirus (SARS-CoV) caused the severe acute respiratory syndrome coronavirus (SARS) outbreak in 2003. Most recently, an SARS-related CoV was implicated as the etiological agent responsible for the outbreak in Wuhan, central China. This outbreak is estimated to have started on 12th December 2019 and 17,332 laboratory confirmed cases with 361 deaths as of 3rd February 2020 in China [1]. The virus has spread to 23 other countries by travellers from Wuhan [1]. Typical symptoms are fever, malaise, shortness of breath and in severe cases, pneumonia [2\u20134]. The disease was first called unidentified viral pneumonia.",
            "cite_spans": [
                {
                    "start": 514,
                    "end": 515,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 587,
                    "end": 588,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 680,
                    "end": 683,
                    "mention": "2\u20134",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "We quickly identified the etiological agent, termed 2019-nCoV (virus name designated by the World Health Organization). The newly identified virus is an SARS-related virus (SARSr-CoV) but shares only 74.5% genome identity to SARS-CoV [2]. We developed molecular detection tools based on viral spike genes. Our previous studies indicate that qPCR method can be used for the detection of 2019-nCoV in oral swabs or in bronchoalveolar lavage fluid (BALF) [5]. Additionally, we developed IgM and IgG detection methods using a cross-reactive nucleocapsid protein (NP) from another SARSr-CoV Rp3 [6], which is 92% identical to 2019-nCoV NP. Using these serological tools, we demonstrate viral antibody titres increase in patients infected with 2019-nCoV [5].",
            "cite_spans": [
                {
                    "start": 235,
                    "end": 236,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 453,
                    "end": 454,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 591,
                    "end": 592,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 749,
                    "end": 750,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Like SARS-CoV, 2019-nCoV induced pneumonia through respiratory tract by clinical observation. Therefore, the presence of viral antigen in oral swabs was used as detection standard for 2019-nCoV. Similarly, two times of oral swabs negative in a 24-h interval was considered as viral clearance by patients officially. Here we launched an investigation of 2019-nCoV in a Wuhan hospital, aiming to investigate the other possible transmission route of this virus.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Human samples, including oral swabs, anal swabs and blood samples were collected by Wuhan pulmonary hospital with the consent from all patients and approved by the ethics committee of the designated hospital for emerging infectious diseases. Two investigations were performed. In the first investigation, we collected samples from 39 patients, 7 of which were in severe conditions. In the second investigation, we collected samples from 139 patients, yet their clinical records were not available. We only showed patients who were viral nucleotide detection positive. Patients were sampled without gender or age preference unless where indicated. For swabs, 1.5 ml DMEM+2% FBS medium was added in each tube. Supernatant was collected after 2500 rpm, 60 s vortex and 15\u201330 min standing. Supernatant from swabs were added to lysis buffer for RNA extraction. Serum was separated by centrifugation at 3000 g for 15 min within 24 h of collection, followed by 56\u00b0C 30 min inactivation, and then stored at 4\u00b0C until use.",
            "cite_spans": [],
            "section": "Sample collection ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "Whenever commercial kits were used, manufacturer\u2019s instructions were followed without modification. RNA was extracted from 200 \u03bcl of samples with the High Pure Viral RNA Kit (Roche). RNA was eluted in 50 \u03bcl of elution buffer and used as the template for RT\u2013PCR. QPCR detection method based on 2019-nCoV S gene can be found in the previous study [5]. In brief, RNA extracted from above used in qPCR by HiScript\u00ae II One Step qRT-PCR SYBR\u00ae Green Kit (Vazyme Biotech Co., Ltd). The 20 \u03bcl qPCR reaction mix contained 10 \u03bcl 2\u00d7 One Step SYBR Green Mix, 1 \u03bcl One Step SYBR Green Enzyme Mix, 0.4 \u03bcl 50 \u00d7 ROX Reference Dye 1, 0.4 \u03bcl of each primer (10 \u03bcM) and 2 \u03bcl template RNA. Amplification was performed as follows: 50\u00b0C for 3 min, 95\u00b0C for 30 s followed by 40 cycles consisting of 95\u00b0C for 10 s, 60\u00b0C for 30 s, and a default melting curve step in an ABI 7500 machine.",
            "cite_spans": [
                {
                    "start": 346,
                    "end": 347,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "RNA extraction and qRT-PCR ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "In-house anti-SARSr-CoV IgG and IgM ELISA kits were developed using SARSr-CoV Rp3 NP as antigen, which shared above 90% amino acid identity to all SARSr-CoVs, as reported previously [5]. For IgG test, MaxiSorp Nunc-immuno 96 well ELISA plates were coated (100 ng/well) overnight with recombinant NP. Human sera were used at 1:20 dilution for 1 h at 37\u00b0C. An anti-Human IgG-HRP conjugated monoclonal antibody (Kyab Biotech Co., Ltd, Wuhan, China) was used at a dilution of 1:40,000. The OD value (450\u2013630) was calculated. For IgM test, MaxiSorp Nunc-immuno 96 wellELISA plates were coated (500 ng/well) overnight with anti-human IgM (\u00b5 chain). Human sera were used at 1:100 dilution for 40 min at 37\u00b0C, followed by anti-Rp3 NP-HRP conjugated (Kyab Biotech Co., Ltd, Wuhan, China) at a dilution of 1:4000. The OD value (450\u2013630) was calculated.",
            "cite_spans": [
                {
                    "start": 183,
                    "end": 184,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Serological test ::: Materials and methods",
            "ref_spans": []
        },
        {
            "text": "In the first investigation, we aimed to test whether viral positive can be found in anal swab and blood as well as oral swabs. We conducted a molecular investigation to patients in Wuhan pulmonary hospital, who were detected as oral swabs positive for 2019-nCoV upon admission. We collected blood, oral swabs and anal swabs for 2019-nCoV qPCR test using previously established method [5].",
            "cite_spans": [
                {
                    "start": 385,
                    "end": 386,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Results",
            "ref_spans": []
        },
        {
            "text": "We found 15 patients who still carry virus following days of medical treatments. Of these patients, 8 were oral swabs positive (53.3%), 4 were anal swabs positive (26.7%), 6 blood positives (40%) and 3 serum positives (20%). Two patients were positive by both oral swab and anal swab, yet none of the blood positive was also swabs positive. Not surprisingly, all serum positives were also whole serum positive (Table 1). In summary, viral nucleotide can be found in anal swab or blood even if it cannot be detected in oral swabs. It should be noted that although swabs may be negative, the patient might still be viremic.\n",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 411,
                    "end": 418,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "We then did another investigation to find out the dynamic changes of viral presence in two consecutive studies in both oral and anal swabs in another group of patients. The target patients were those who received around 10 days of medical treatments upon admission. We tested for both viral antibody and viral nucleotide levels by previously established method [5]. We showed that both IgM and IgG titres were relatively low or undetectable in day 0 (the day of first sampling). On day 5, an increase of viral antibodies can be seen in nearly all patients, which was normally considered as a transition from earlier to later period of infection (Figure 1 and supplementary table 1). IgM positive rate increased from 50% (8/16) to 81% (13/16), whereas IgG positive rate increased from 81% (13/16) to 100% (16/16). This is in contrast to a relatively low detection positive rate from molecular test (below).\n",
            "cite_spans": [
                {
                    "start": 362,
                    "end": 363,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 646,
                    "end": 654,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "For molecular detection, we found 8 oral swabs positive (50%) and 4 anal swabs (25%) in these 16 people on day 0. On day 5, we were only able to find 4 oral swabs positive (25%). In contrast, we found 6 anal swabs positive (37.5%). When counting all swab positives together, we found most of the positives came from oral swab (8/10, 80%) on day 0. However, this trend appears to change on day 5. We found more (6/8, 75%) anal swab positive than oral swab positive (4/8, 50%). Another observation is the reoccurrence of virus in 6 patients who were detected negative on day 0. Of note, 4 of these 6 viral positives were from anal swabs (Table 2). These data suggested a shift from more oral positive during early period (as indicated by antibody titres) to more anal positive during later period might happen.\n",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 636,
                    "end": 643,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Within 1 month of the 2019-nCoV disease outbreak, we rapidly developed molecular and serological detection tools. This is the first molecular and serological study on this virus after the initial identification of 2019-NCoV from 7 patients diagnosed with unidentified viral pneumonia [5]. We detected the virus in oral swabs, anal swabs and blood, thus infected patients can potentially shed this pathogen through respiratory, fecal\u2013oral or body fluid routes. In addition, we successfully applied serology test a large population and showed which could greatly improved detection positive rate.",
            "cite_spans": [
                {
                    "start": 285,
                    "end": 286,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "We show that the current strategy for the detection of viral RNA in oral swabs used for 2019-nCoV diagnosis is not perfect. The virus may be present in anal swabs or blood of patients when oral swabs detection negative. In SARS-CoV and MERS-CoV infected patients, intestinal infection was observed at later stages of infection [7\u20139]. However, patients infected with 2019-nCoV may harbour the virus in the intestine at the early or late stage of disease. It is also worth to note none of the patients with viremia blood had positive swabs. These patients would likely be considered as 2019-nCoV negative through routine surveillance, and thus pose a threat to other people. In contrast, we found viral antibodies in near all patients, indicating serology should be considered for 2019-nCoV epidemiology. A possible shift from oral positive during early infection to anal swab positive during late infection can be observed. This observation implied that we cannot discharge a patient purely based on oral swabs negative, who may still shed the virus by oral\u2013fecal route. Above all, we strongly suggest using viral IgM and IgG serological test to confirm an infection, considering the unreliable results from oral swabs detection.",
            "cite_spans": [
                {
                    "start": 328,
                    "end": 331,
                    "mention": "7\u20139",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In summary, we provide a cautionary warning that 2019-nCoV may be transmitted through multiple routes. Both molecular and serological tests are needed to definitively confirm a virus carrier.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1.: Serological detection of 2019-nCoV. Dashed line indicates cutoff, which was determined based on data from healthy controls.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan, China",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30183-5"
                ]
            }
        },
        "BIBREF2": {
            "title": "A novel coronavirus from patients with pneumonia in China, 2019",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001017"
                ]
            }
        },
        "BIBREF3": {
            "title": "Early transmission dynamics in Wuhan, China, of novel coronavirus-infected pneumonia",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001316"
                ]
            }
        },
        "BIBREF4": {
            "title": "Discovery of a novel coronavirus associated with the recent pneumonia outbreak in humans and its potential bat origin",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "XL",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "XG",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "BioRxiv",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Serological evidence of bat SARS-related coronavirus infection in humans, China",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "XL",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Virol Sin",
            "volume": "33",
            "issn": "",
            "pages": "104-107",
            "other_ids": {
                "DOI": [
                    "10.1007/s12250-018-0012-7"
                ]
            }
        },
        "BIBREF6": {
            "title": "Severe acute respiratory syndrome associated coronavirus is detected in intestinal tissues of fatal cases",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Gong",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Gao",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Am J Gastroenterol.",
            "volume": "100",
            "issn": "1",
            "pages": "169-176",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1572-0241.2005.40377.x"
                ]
            }
        },
        "BIBREF7": {
            "title": "Organ distribution of severe acute respiratory syndrome (SARS) associated coronavirus (SARS-CoV) in SARS patients: implications for pathogenesis and virus transmission pathways",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Ding",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "He",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Pathol.",
            "volume": "203",
            "issn": "2",
            "pages": "622-630",
            "other_ids": {
                "DOI": [
                    "10.1002/path.1560"
                ]
            }
        },
        "BIBREF8": {
            "title": "Human intestinal tract serves as an alternative infection route for Middle East respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Sci Adv",
            "volume": "3",
            "issn": "11",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1126/sciadv.aao4966"
                ]
            }
        }
    }
}