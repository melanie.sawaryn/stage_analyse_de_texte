{
    "paper_id": "PMC7054935",
    "metadata": {
        "title": "No credible evidence supporting claims of the laboratory engineering of SARS-CoV-2",
        "authors": [
            {
                "first": "Shan-Lu",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Linda",
                "middle": [
                    "J."
                ],
                "last": "Saif",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Susan",
                "middle": [
                    "R."
                ],
                "last": "Weiss",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Lishan",
                "middle": [],
                "last": "Su",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The emergence and outbreak of a newly discovered acute respiratory disease in Wuhan, China, has affected greater than 40,000 people, and killed more than 1,000 as of Feb. 10, 2020. A new human coronavirus, SARS-CoV-2, was quickly identified, and the associated disease is now referred to as coronavirus disease discovered in 2019 (COVID-19) (https://globalbiodefense.com/novel-coronavirus-covid-19-portal/).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "According to what has been reported [1\u20133], COVID-2019 seems to have similar clinical manifestations to that of the severe acute respiratory syndrome (SARS) caused by SARS-CoV. The SARS-CoV-2 genome sequence also has \u223c80% identity with SARS-CoV, but it is most similar to some bat beta-coronaviruses, with the highest being >96% identity [4,5].",
            "cite_spans": [
                {
                    "start": 37,
                    "end": 40,
                    "mention": "1\u20133",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 338,
                    "end": 339,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 340,
                    "end": 341,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Currently, there are speculations, rumours and conspiracy theories that SARS-CoV-2 is of laboratory origin. Some people have alleged that the human SARS-CoV-2 was leaked directly from a laboratory in Wuhan where a bat CoV (RaTG13) was recently reported, which shared \u223c96% homology with the SARS-CoV-2 [4]. However, as we know, the human SARS-CoV and intermediate host palm civet SARS-like CoV shared 99.8% homology, with a total of 202 single-nucleotide (nt) variations (SNVs) identified across the genome [6]. Given that there are greater than 1,100 nt differences between the human SARS-CoV-2 and the bat RaTG13-CoV [4], which are distributed throughout the genome in a naturally occurring pattern following the evolutionary characteristics typical of CoVs, it is highly unlikely that RaTG13 CoV is the immediate source of SARS-CoV-2. The absence of a logical targeted pattern in the new viral sequences and a close relative in a wildlife species (bats) are the most revealing signs that SARS-CoV-2 evolved by natural evolution. A search for an intermediate animal host between bats and humans is needed to identify animal CoVs more closely related to human SARS-CoV-2. There is speculation that pangolins might carry CoVs closely related to SARS-CoV-2, but the data to substantiate this is not yet published (https://www.nature.com/articles/d41586-020-00364-2).",
            "cite_spans": [
                {
                    "start": 302,
                    "end": 303,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 507,
                    "end": 508,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 619,
                    "end": 620,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Another claim in Chinese social media points to a Nature Medicine paper published in 2015 [7], which reports the construction of a chimeric CoV with a bat CoV S gene (SHC014) in the backbone of a SARS CoV that has adapted to infect mice (MA15) and is capable of infecting human cells [8]. However, this claim lacks any scientific basis and must be discounted because of significant divergence in the genetic sequence of this construct with the new SARS-CoV-2 (>5,000 nucleotides).",
            "cite_spans": [
                {
                    "start": 91,
                    "end": 92,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 285,
                    "end": 286,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The mouse-adapted SARS virus (MA15) [9] was generated by serial passage of an infectious wildtype SARS CoV clone in the respiratory tract of BALB/c mice. After 15 passages in mice, the SARS-CoV gained elevated replication and lung pathogenesis in aged mice (hence M15), due to six coding genetic mutations associated with mouse adaptation. It is likely that MA15 is highly attenuated to replicate in human cells or patients due to the mouse adaptation.",
            "cite_spans": [
                {
                    "start": 37,
                    "end": 38,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "It was proposed that the S gene from bat-derived CoV, unlike that from human patients- or civets-derived viruses, was unable to use human ACE2 as a receptor for entry into human cells [10,11]. Civets were proposed to be an intermediate host of the bat-CoVs, capable of spreading SARS CoV to humans [6,12]. However, in 2013 several novel bat coronaviruses were isolated from Chinese horseshoe bats and the bat SARS-like or SL-CoV-WIV1 was able to use ACE2 from humans, civets and Chinese horseshoe bats for entry [8]. Combined with evolutionary evidence that the bat ACE2 gene has been positively selected at the same contact sites as the human ACE2 gene for interacting with SARS CoV [13], it was proposed that an intermediate host may not be necessary and that some bat SL-CoVs may be able to directly infect human hosts. To directly address this possibility, the exact S gene from bat coronavirus SL-SHC014 was synthesized and used to generate a chimeric virus in the mouse adapted MA15 SARS-CoV backbone. The resultant SL-SHC014-MA15 virus could indeed efficiently use human ACE2 and replicate in primary human airway cells to similar titres as epidemic strains of SARS-CoV. While SL-SHC014-MA15 can replicate efficiently in young and aged mouse lungs, infection was attenuated, and less virus antigen was present in the airway epithelium as compared to SARS MA15, which causes lethal outcomes in aged mice [7].",
            "cite_spans": [
                {
                    "start": 185,
                    "end": 187,
                    "mention": "10",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 188,
                    "end": 190,
                    "mention": "11",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 299,
                    "end": 300,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 301,
                    "end": 303,
                    "mention": "12",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 513,
                    "end": 514,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 685,
                    "end": 687,
                    "mention": "13",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1411,
                    "end": 1412,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Due to the elevated pathogenic activity of the SHC014-MA15 chimeric virus relative to MA15 chimeric virus with the original human SARS S gene in mice, such experiments with SL-SHC014-MA15 chimeric virus were later restricted as gain of function (GOF) studies under the US government-mandated pause policy (https://www.nih.gov/about-nih/who-we-are/nih-director/statements/nih-lifts-funding-pause-gain-function-research). The current COVID-2019 epidemic has restarted the debate over the risks of constructing such viruses that could have pandemic potential, irrespective of the finding that these bat CoVs already exist in nature. Regardless, upon careful phylogenetic analyses by multiple international groups [5,14], the SARS-CoV-2 is undoubtedly distinct from SL-SHC014-MA15, with >6,000 nucleotide differences across the whole genome. Therefore, once again there is no credible evidence to support the claim that the SARS-CoV-2 is derived from the chimeric SL-SHC014-MA15 virus.",
            "cite_spans": [
                {
                    "start": 711,
                    "end": 712,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 713,
                    "end": 715,
                    "mention": "14",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "There are also rumours that the SARS-CoV-2 was artificially, or intentionally, made by humans in the lab, and this is highlighted in one manuscript submitted to BioRxiv (a manuscript sharing site prior to any peer review), claiming that SARS-CoV-2 has HIV sequence in it and was thus likely generated in the laboratory. In a rebuttal paper led by an HIV-1 virologist Dr. Feng Gao, they used careful bioinformatics analyses to demonstrate that the original claim of multiple HIV insertions into the SARS-CoV-2 is not HIV-1 specific but random [15]. Because of the many concerns raised by the international community, the authors who made the initial claim have already withdrawn this report.",
            "cite_spans": [
                {
                    "start": 543,
                    "end": 545,
                    "mention": "15",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Evolution is stepwise and accrues mutations gradually over time, whereas synthetic constructs would typically use a known backbone and introduce logical or targeted changes instead of the randomly occurring mutations that are present in naturally isolated viruses such as bat CoV RaTG13. In our view, there is currently no credible evidence to support the claim that SARS-CoV-2 originated from a laboratory-engineered CoV. It is more likely that SARS-CoV-2 is a recombinant CoV generated in nature between a bat CoV and another coronavirus in an intermediate animal host. More studies are needed to explore this possibility and resolve the natural origin of SARS-CoV-2. We should emphasize that, although SARS-CoV-2 shows no evidence of laboratory origin, viruses with such great public health threats must be handled properly in the laboratory and also properly regulated by the scientific community and governments.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Clinical characteristics of 138 hospitalized patients with 2019 novel coronavirus-infected pneumonia in Wuhan, China",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.1585"
                ]
            }
        },
        "BIBREF1": {
            "title": "Epidemiologic and clinical characteristics of novel coronavirus infections involving 13 patients outside Wuhan, China",
            "authors": [
                {
                    "first": "LM",
                    "middle": [],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.1623"
                ]
            }
        },
        "BIBREF2": {
            "title": "Epidemiological and clinical characteristics of 99 cases of 2019 novel coronavirus pneumonia in Wuhan, China: a descriptive study",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Dong",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Lancet",
            "volume": "395",
            "issn": "10223",
            "pages": "507-513",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30211-7"
                ]
            }
        },
        "BIBREF3": {
            "title": "A pneumonia outbreak associated with a new coronavirus of probable bat origin",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "XL",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "XG",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Nature",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1038/s41586-020-2012-7"
                ]
            }
        },
        "BIBREF4": {
            "title": "A novel coronavirus from patients with pneumonia in China, 2019",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "8",
            "pages": "727-733",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001017"
                ]
            }
        },
        "BIBREF5": {
            "title": "Cross-host evolution of severe acute respiratory syndrome coronavirus in palm civet and human",
            "authors": [
                {
                    "first": "HD",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Tu",
                    "suffix": ""
                },
                {
                    "first": "GW",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Proc Natl Acad Sci USA",
            "volume": "102",
            "issn": "7",
            "pages": "2430-2435",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.0409608102"
                ]
            }
        },
        "BIBREF6": {
            "title": "A SARS-like cluster of circulating bat coronaviruses shows potential for human emergence",
            "authors": [
                {
                    "first": "VD",
                    "middle": [],
                    "last": "Menachery",
                    "suffix": ""
                },
                {
                    "first": "BL",
                    "middle": [],
                    "last": "Yount Jr.",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Debbink",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Nat Med",
            "volume": "21",
            "issn": "12",
            "pages": "1508-1513",
            "other_ids": {
                "DOI": [
                    "10.1038/nm.3985"
                ]
            }
        },
        "BIBREF7": {
            "title": "Isolation and characterization of a bat SARS-like coronavirus that uses the ACE2 receptor",
            "authors": [
                {
                    "first": "XY",
                    "middle": [],
                    "last": "Ge",
                    "suffix": ""
                },
                {
                    "first": "JL",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "XL",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Nature",
            "volume": "503",
            "issn": "7477",
            "pages": "535-538",
            "other_ids": {
                "DOI": [
                    "10.1038/nature12711"
                ]
            }
        },
        "BIBREF8": {
            "title": "A mouse-adapted SARS-coronavirus causes disease and mortality in BALB/c mice",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Roberts",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Deming",
                    "suffix": ""
                },
                {
                    "first": "CD",
                    "middle": [],
                    "last": "Paddock",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "PLoS Pathog",
            "volume": "3",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.ppat.0030005"
                ]
            }
        },
        "BIBREF9": {
            "title": "Structure of SARS coronavirus spike receptor-binding domain complexed with receptor",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farzan",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Science",
            "volume": "309",
            "issn": "5742",
            "pages": "1864-1868",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1116480"
                ]
            }
        },
        "BIBREF10": {
            "title": "Angiotensin-converting enzyme 2 is a functional receptor for the SARS coronavirus",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "MJ",
                    "middle": [],
                    "last": "Moore",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Vasilieva",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Nature",
            "volume": "426",
            "issn": "6965",
            "pages": "450-454",
            "other_ids": {
                "DOI": [
                    "10.1038/nature02145"
                ]
            }
        },
        "BIBREF11": {
            "title": "Isolation and characterization of viruses related to the SARS coronavirus from animals in southern China",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "YQ",
                    "middle": [],
                    "last": "He",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Science",
            "volume": "302",
            "issn": "5643",
            "pages": "276-278",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1087139"
                ]
            }
        },
        "BIBREF12": {
            "title": "Evidence for ACE2-utilizing coronaviruses (CoVs) related to severe acute respiratory syndrome CoV in bats",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Demogines",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farzan",
                    "suffix": ""
                },
                {
                    "first": "SL.",
                    "middle": [],
                    "last": "Sawyer",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "J Virol",
            "volume": "86",
            "issn": "11",
            "pages": "6350-6353",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.00311-12"
                ]
            }
        },
        "BIBREF13": {
            "title": "A new coronavirus associated with human respiratory disease in China",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Nature",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1038/s41586-020-2008-3"
                ]
            }
        },
        "BIBREF14": {
            "title": "HIV-1 did not contribute to the 2019-nCoV genome",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Xiao",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Emerg Microbes Infect",
            "volume": "9",
            "issn": "1",
            "pages": "378-381",
            "other_ids": {
                "DOI": [
                    "10.1080/22221751.2020.1727299"
                ]
            }
        }
    }
}