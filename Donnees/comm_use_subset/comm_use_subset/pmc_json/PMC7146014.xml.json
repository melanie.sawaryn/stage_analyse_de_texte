{
    "paper_id": "PMC7146014",
    "metadata": {
        "title": "Is Sub-Saharan Africa prepared for COVID-19?",
        "authors": [
            {
                "first": "Edwin",
                "middle": [],
                "last": "Nuwagira",
                "suffix": "",
                "email": "enuwagira@must.ac.ug",
                "affiliation": {}
            },
            {
                "first": "Conrad",
                "middle": [],
                "last": "Muzoora",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "To the Editor,",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A number of big cities in Asia and Europe have been deserted due to the fear of COVID-19, the most recent emerging infectious viral illness. Apart from the Antarctica, Africa is one of the least affected continents with less than 1000 cases across the continent and only 27 deaths as of March 21, 2020 [2]. This is probably a matter of time since there were until recently frequent and large travel volumes between Asia and Africa due to the good commercial relationships between the two continents and a number of countries in Africa receiving a high number of airline travelers from high-risk cities within China [4]. In fact, over the past decade, air transport between China and Africa has risen to about 630% as a result of the rapid expansion of Chinese investment in Africa [5].",
            "cite_spans": [
                {
                    "start": 303,
                    "end": 304,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 616,
                    "end": 617,
                    "mention": "4",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 782,
                    "end": 783,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "SSA, however, already faces several health challenges such as limited access to intensive care units, for example, in Uganda, there are only 55 intensive care unit (ICU) beds to serve 40 million people, 80% of which are located in the capital, and the nurse to patient ratio of 1:8 making it the worst in the region [6]. High-income countries (HICs) that have been affected by the disease are also facing inadequacy of ICUs. For example, Italy, which has the eighth-highest nominal gross domestic product (GDP) in the world [7], and one of the greatest health systems with approximately 3.2 hospital beds per 1000 people (compared to 2.8 in USA), has found it hard to meet the needs of the overwhelming numbers of critically ill patients, some of whom are specialized health workers [8, 9]. This shows that no one was adequately prepared for the alarming numbers of COVID-19 cases.",
            "cite_spans": [
                {
                    "start": 317,
                    "end": 318,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 525,
                    "end": 526,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 784,
                    "end": 785,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 787,
                    "end": 788,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Worse still, there is inadequate manpower especially of critical care doctors and consultant anesthetists to run the few ICUs in SSA. There is also a high burden of infectious diseases with emerging viral illnesses such as Ebola hemorrhagic fever and Lassa hemorrhagic fever in addition to the \u201cpriority infections\u201d HIV, malaria, and tuberculosis, and for all disease models, mortality has always been the highest in SSA compared to HICs. These infections present with fever, a screening symptom and sign for the COVID-19 making it hard to determine which patients to screen. In the West African Ebola outbreak, fever was used as a screening test, but 10% of the confirmed cases did not have fever, and a number of them are with malaria co-infection [10].",
            "cite_spans": [
                {
                    "start": 751,
                    "end": 753,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We therefore anticipate human-to-human transmission due to undiagnosed, symptomatic, and asymptomatic patients due to the limited access to personal protective equipment such as N95 masks, respiratory isolation rooms, goggles, and powered air-purifying respirators (PAPR). The application of the prevention measures such as heightened surveillance, rapid identification of cases, patient transfer to isolation, rapid diagnosis, and tracing and follow-up of contacts varies and needs to be improved at all entry points, but this comes at a cost that may not be sustainable. Modeling studies have shown that travel restrictions of up to 90% have a minimal effect on transmission if not coupled with other public health and behavioral change interventions [11]. The travel bans because of COVID-19 will also greatly affect the economy and delay achieving development goals given that most countries in SSA depend on foreign aid [12].",
            "cite_spans": [
                {
                    "start": 754,
                    "end": 756,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 926,
                    "end": 928,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "It is therefore important for African governments and WHO to invest immediately in preparedness for the worst-case scenario given that each African country is at a risk of importing COVID-19 from China or other affected countries because of the high volume of air traffic and trade between China and Africa [5]. The WHO, Centers for Disease Control and Prevention in Africa (Africa CDC), and other humanitarian organizations should be ready for an African crisis which we anticipate to be greater than it is in other regions due to its weak health systems. On Thursday, March 19, 2020, the Director-General of WHO, Dr. Tedros, stated that this was an opportunity for Africa to wake up and prepare for the worst [13].",
            "cite_spans": [
                {
                    "start": 308,
                    "end": 309,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 712,
                    "end": 714,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Epidemiologists should provide accurate predictions that will prepare African governments and political leaders with information that will guide them design control measures at different health centers and regional and national levels. Lastly, there is a lot that we can learn from the Ebola success story to save Africa especially by community engagement. The WHO noted the lack of capacity to contain the West Africa Ebola outbreak in 2014, making it difficult to implement infection prevention procedures [14]. We expect the same barriers from the community as noted in the recent Ebola hemorrhagic fever outbreak in the Democratic Republic of Congo (DRC), such as mistrust of response teams, beliefs that the virus was fabricated, and non-compliant intentions such as hiding from health authorities with discourse [15]. Behaviors like hand shaking, social gatherings, breach of quarantine rules, and contact with the dead may see the number of cases increase in Sub-Saharan Africa.",
            "cite_spans": [
                {
                    "start": 509,
                    "end": 511,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 819,
                    "end": 821,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Assessment of the current capacity of intensive care units in Uganda; a descriptive study",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Atumanya",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Sendagire",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wabule",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Mukisa",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Ssemogerere",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kwizera",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Crit Care.",
            "volume": "55",
            "issn": "",
            "pages": "95-99",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcrc.2019.10.019"
                ]
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Remuzzi",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Remuzzi",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "COVID-19 and Italy: what next? The Lancet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Malvy",
                    "suffix": ""
                },
                {
                    "first": "AK",
                    "middle": [],
                    "last": "McElroy",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "de Clerck",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "G\u00fcnther S",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}