{
    "paper_id": "PMC7029610",
    "metadata": {
        "title": "Critical care response to a hospital outbreak of the 2019-nCoV infection in Shenzhen, China",
        "authors": [
            {
                "first": "Yong",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jinxiu",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yongwen",
                "middle": [],
                "last": "Feng",
                "suffix": "",
                "email": "fengyongwensz@163.com",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The main challenge may include (1) early identification of outbreak, (2) rapid expansion of patients, (3) high risk of nosocomial transmission, (4) unpredictability of size impacted, and (5) lack of backup resource. These challenges have caused severe shortage of healthcare workers, medical materials, and beds with isolation. The Spring Festival holiday has greatly aggravated the shortage of human resources and heavy traffic flow due to the vacation of healthy workers and factory workers, which further magnified the risk of transmission. The key point is to discriminate the infectious disease outbreak from regular clustering cases of flu-like diseases at early stage. There is a trade-off between false alarm causing population panic and delayed identification leading to social crisis.",
            "cite_spans": [],
            "section": "The main challenge",
            "ref_spans": []
        },
        {
            "text": "Early identification of 2019-nCoV infection presents a major challenge for the frontline clinicians. Its clinical symptoms largely overlap with those of common acute respiratory illnesses, including fever (98%), cough (76%), and diarrhea (3%), often more severe in older adults with pre-existing chronic comorbidities [1]. Usually, the laboratory abnormalities include lymphocytopenia and hypoxemia [1]. The initial chest radiographs may vary from minimal abnormality to bilateral ground-glass opacity or subsegmental areas of consolidation [1]. In addition, asymptomatic cases and lack of diagnosis kits result in delayed or even missed diagnosis inevitable and makes many other patients, visitors, and healthcare workers exposed to the 2019-nCoV infection.",
            "cite_spans": [
                {
                    "start": 319,
                    "end": 320,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 400,
                    "end": 401,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 542,
                    "end": 543,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Clinical features",
            "ref_spans": []
        },
        {
            "text": "Critical care response to the outbreak of coronavirus should happen not only at the level of hospital, but also at the level of the city which is dominated by the government. At the early stage, the size of the patients\u2019 population is not beyond the capability of local infectious diseases hospital (IDH). The general hospital is responsible for fever triage, identifying suspected cases, and transferring to the local IDH. Such a plan is mandatory for every hospital. Shenzhen city has established a preexisting Infectious Disease Epidemic Plan (IDEP), which has facilitated managing and containing local outbreak of the 2019-nCoV. In case the patient load exceeds the hospital capability of the IDH, new IDHs should be considered either by building a temporary new IDH or reconstructing an existing hospital. Wuhan, the epicenter of the outbreak, is racing against time to build two specialized hospitals for nCoV patients, namely Huoshenshan and Leishenshan hospital, whereas a different strategy has been undertaken in Shenzhen city by reconstructing an existing hospital to become an IDH with capability of 800 beds.",
            "cite_spans": [],
            "section": "City level ::: Critical care response at the hospital and city level",
            "ref_spans": []
        },
        {
            "text": "2019-nCoV patients should be admitted to single-bedded, negative pressure rooms in isolated units with intensive care and monitoring [2]. Clinical engineering should have plans to reconstruct standard rooms [2]. Retrofitting the rooms with externally exhausted HEPA filters may be an expedient solution. Also, the general hospital may consider procedures such as suspending elective surgeries, canceling ambulatory clinics and outpatient diagnostic procedures, transferring patients to other institutions, and restricting hospital visitors [2]. More importantly, because the hospitals\u2019 ability to respond to the outbreak largely depends on their available ICU beds, the plan to increase ICU bed capacity needs to be determined.",
            "cite_spans": [
                {
                    "start": 134,
                    "end": 135,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 208,
                    "end": 209,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 541,
                    "end": 542,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Hospital level ::: Critical care response at the hospital and city level",
            "ref_spans": []
        },
        {
            "text": "Caring for 2019-nCoV patients represents a substantial exposure risk for ICU staff because of the following reasons: highly contagious with multiple transmission route, high exposure dose, long daily contact hours, and ICU stay. The basic reproductive number was estimated to be 2.2 (95% CI, 1.4 to 3.9) [3], or as high as between 3.6 and 4.0 [4]. The 2019-nCoV is proved to be transmitted by respiratory droplets, contact, and fecal-oral, even transmission through the eye is possible [5, 6]. The higher viral load and aerosol-generating procedures, such as non-invasive ventilation, magnify the exposure and transmission risk [2, 7, 8]. Moreover, virus shedding can be prolonged and last for > 3 weeks according to some literature and our unpublished data [2]. Healthcare providers and those in contact with infected patients should utilize contact, droplet, and airborne precautions with N95 respirator. Strict infection prevention and control practices have been implemented and audited in our units following the infection prevention and control plan published by China\u2019s National Health Committee (CNHC). In addition, well-equipped fever clinic as triage station with trained staff knowing 2019-nCoV case definitions is established. For suspected 2019-nCoV infection, several key points are crucial procedures: recording a detailed history, standardizing pneumonia workup, obtaining lower respiratory tract specimens [2, 8], and implementing droplet isolation to break the transmission chain in the healthcare setting [2].",
            "cite_spans": [
                {
                    "start": 305,
                    "end": 306,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 344,
                    "end": 345,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 487,
                    "end": 488,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 490,
                    "end": 491,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 629,
                    "end": 630,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 632,
                    "end": 633,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 635,
                    "end": 636,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 759,
                    "end": 760,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1424,
                    "end": 1425,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1427,
                    "end": 1428,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1525,
                    "end": 1526,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Key points for preventing transmission of infectious agents in healthcare settings",
            "ref_spans": []
        },
        {
            "text": "The risk of 2019-nCoV exposure may cause significant psychosocial stress on healthcare workers [2]. The death of a retired ENT physician from a 2019-nCoV infection has added to fears in January 2020. Psychotherapists have also been invited to join medical teams to evaluate and deal with potential stress and depression for the safety of the healthcare workers.",
            "cite_spans": [
                {
                    "start": 96,
                    "end": 97,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Psychosocial stress",
            "ref_spans": []
        },
        {
            "text": "2019-nCoV management was largely supportive, including intubation, early prone positioning, neuromuscular blockade, and extracorporeal membrane oxygenation (ECMO) according to the recommendations updated by CNHC. Low-dose systematic corticosteroids, lopinavir/ritonavir, and atomization inhalation of interferon were encouraged. These critical managements have worked well so far, as our 2019-nCoV patients had zero mortality. On the contrary, the previously reported mortality of 2019-nCoV patients in Wuhan ranged from 11 to 15% [1, 9].",
            "cite_spans": [
                {
                    "start": 532,
                    "end": 533,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 535,
                    "end": 536,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Critical management",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "The critical care response to a hospital outbreak of Middle East respiratory syndrome coronavirus (MERS-CoV) infection: an observational study",
            "authors": [
                {
                    "first": "HM",
                    "middle": [],
                    "last": "Al-Dorzi",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Ann Intensive Care",
            "volume": "6",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/s13613-016-0203-z"
                ]
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Aerosol generating procedures and risk of transmission of acute respiratory infections to healthcare workers: a systematic review",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Tran",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "PLoS One",
            "volume": "7",
            "issn": "4",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.pone.0035797"
                ]
            }
        },
        "BIBREF7": {
            "title": "Association of higher MERS-CoV virus load with severe disease and death, Saudi Arabia, 2014",
            "authors": [
                {
                    "first": "DR",
                    "middle": [],
                    "last": "Feikin",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Emerg Infect Dis",
            "volume": "21",
            "issn": "11",
            "pages": "2029-2035",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2111.150764"
                ]
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}