{
    "paper_id": "PMC7122309",
    "metadata": {
        "title": "Pathogens Causing Upper Respiratory Tract Infections in Outpatients",
        "authors": [
            {
                "first": "Mieczyslaw",
                "middle": [],
                "last": "Pokorski",
                "suffix": "",
                "email": "m_pokorski@hotmail.com",
                "affiliation": {}
            },
            {
                "first": "A.",
                "middle": [],
                "last": "Jama-Kmiecik",
                "suffix": "",
                "email": "agnieszka.jama-kmiecik@umed.wroc.pl",
                "affiliation": {}
            },
            {
                "first": "M.",
                "middle": [],
                "last": "Frej-M\u0105drzak",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "J.",
                "middle": [],
                "last": "Sarowska",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "I.",
                "middle": [],
                "last": "Choroszy-Kr\u00f3l",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Upper respiratory tract infections (URTIs) are caused by many different bacteria (including mycobacteria) and viruses, and rapid detection of pathogens in individual cases is crucial in achieving the best clinical management. The most common bacteria causing URTIs are Streptococcus pneumoniae, Haemophilus influenzae, Moraxella catarrhalis, Streptococcus pyogenes, and Mycoplasma pneumoniae. The viral side is represented mostly by respiratory syncytial viruses (RSV), adenoviruses, influenza, parainfluenza, with all possible types of crisscross co-infections. These infections clinically manifest as a group of disorders which include common cold, pharyngitis, tonsillitis, epiglottitis, sinusitis, bronchitis, rhinitis, and nasopharyngitis (Pettigrew et al. 2011). The infections are usually viral as only 10 % of cases are attributed to bacteria, although the percentage of bacterial infection is difficult to estimate precisely due to the all too often encountered bacterial superinfections superimposed on the earlier original viral infection. Nonetheless, most patients are treated with antibiotics (Kho et al. 2013; Costelloe et al. 2010). The rationale for the use of antibiotics might be that viral infections damaging the structure of airway epithelium pave the way for bacterial pathogens, cause inflammation, increase permeability of vascular endothelium, and also often times lead to bronchial hyperreactivity. Various signs and symptoms of URTIs have been reported, including stuffy and runny nose, sneezing, coughing, sore throat, fever, vomiting, loss of appetite, and watery eyes. Transmission of microorganisms causing URTIs has been known to occur by aerosol, droplet, and direct hand-to-hand contact with infected secretions. Children are more susceptible to URTIs than adults. This might be due to the lack of immunity to the many viruses and bacteria that cause URTIs and close person-to-person contact exercised by children (Hendaus et al. 2015; Pavia 2011; Don et al. 2009).",
            "cite_spans": [
                {
                    "start": 762,
                    "end": 766,
                    "mention": "2011",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1119,
                    "end": 1123,
                    "mention": "2013",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1142,
                    "end": 1146,
                    "mention": "2010",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1965,
                    "end": 1969,
                    "mention": "2015",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1977,
                    "end": 1981,
                    "mention": "2011",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1994,
                    "end": 1998,
                    "mention": "2009",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "An important role in URTIs plays the atypical bacterium Chlamydia pneumoniae. It is a Gram-negative intracellular bacterium dependent on host cell ATP deposits, forming inclusions in the cytoplasm of infected cells. Chlamydia pneumoniae has a unique developmental cycle and can exist in two forms: elementary body (EB), a form responsible for spreading the infection, and reticulate body (RB), an intracellular metabolically active form. Apart from this replication cycle occurring in acute infection, Chlamydia pneumoniae is able to persist in a non-replicating state (K\u00e4ding et al. 2014; Batteiger 2012). The purpose of this study was to investigate the frequency of RTIs infections and the underlying bacterial pathogenesis in a cohort representative for outpatient environment. The dominant symptom in all patients investigated was chronic cough of unclear origin.",
            "cite_spans": [
                {
                    "start": 584,
                    "end": 588,
                    "mention": "2014",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 600,
                    "end": 604,
                    "mention": "2012",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The investigation was performed in accordance with the Declaration of Helsinki for Human Research and the protocol was accepted by the Ethics Committee of the Medical University of Wroclaw, Poland. The study group consisted of 230 outpatients (112 female, 72 male, and 46 children) aged from 1 to 83 years. Patients were recruited in the period of September 2014 to September 2015. A major criterion for inclusion in the study was persisting cough. The material tested was pharyngeal swabs taken in the morning in fasted state. Specimens were collected before starting antibiotic therapy and were examined for the presence of Chlamydia pneumoniae antigen and for typical pathogens. The antigen was detected using an indirect immunofluorescence test (Chlamydia Cel PN-IFT Kit; Cellabs Pty Ltd, Sydney, Australia). Specimens were stained in two sequential steps; the first one using a suspension of monoclonal antibodies which bind to Chlamydia pneumoniae antigen and the second using an Fluorescein isothiocyanate (FITC)-conjugated goat anti-mouse antibody to visualize Chlamydia pneumoniae microorganisms. Identification of four or more chlamydial elementary bodies among epithelial cells was taken as the criterion of a positive diagnosis. Typical bacteria were detected using a classical culture of microbiological throat specimens.",
            "cite_spans": [],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "\nC. pneumoniae antigen was detected in 44 (19.1 %) outpatients, which included 23 (20.5 %) of women, 13 (18.1 %) of men, and 8 (17.4 %) of children. Typical respiratory tract pathogens such as Staphylococcus aureus strain MSSA, Streptococcus pyogenes, Moraxella catarrhalis and Haemophilus influenzae were detected in 65 (28.3 %) outpatients, which included 37 (33.0 %) of women, 14 (19.4 %) of men, and 14 (30.4 %) of children. The most frequently occurring typical pathogen was Staphylococcus aureus strain MSSA. Simulataneous occurrence of typical and atypical pathogens was found in 11 (4.8 %) of patients (Table 1).\n",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 617,
                    "end": 618,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "URTIs are the commonest acute problem dealt with in primary care; the \u2018bread and butter\u2019 of daily practice. These infections have been suggested to be mild and self-limiting, but are also reported to lead to life threatening complications. Different climatic conditions like rainy season, winter, low humidity conditions, and distorted immunity have been listed as being responsible for the occurrence of infection. The causative agents involved in URTIs are coronavirus, rhinovirus, human parainfluenza virus, adenovirus, enterovirus and human respiratory syncytial virus. The most common bacteria, on the other side, are beta-hemolytic streptococci, Corynebacterium diphtheriae, Neisseria gonorrhoeae, Chlamydia pneumoniae, Mycoplasma pneumoniae, Streptococcus pneumoniae, Haemophilus influenzae, Bordetella pertussis, and Moraxella catarrhalis. In diagnostic microbiology, it is essential to distinguish between a patient\u2019s commensal flora and the causative agent of an infection. That is none too often an easy process, since in some cases microbes belong to normal flora of certain anatomical location, but are considered pathogens when isolated from other sites.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In our previous study that covered a 1-year period of September 2013 to September 2014, ie, preceding that of the current study, typical pathogens such as Staphylococcus aureus strain MSSA, Streptococcus pyogenes, and Moraxella catarrhalis were detected in 73 (34.1 %) out of the 214 patients with chronic cough. The main infecting pathogen was then Staphylococcus aureus MSSA, followed by Streptococcus pyogenes, and Moraxella catarrhalis. Chlamydia pneumoniae antigen was found in 55 (25.7 %) of patients. Co-infections with typical and atypical pathogens occurred in 16 (7.5 %) of those 2014 patients tested positive (Jama-Kmiecik et al. 2014). In the present study, all categories of respiratory pathogens were detected less frequently compared with the study above outlined: typical pathogens in 65 (28.3 %), Chlamydia pneumoniae antigen in 44 (19.1 %), and co-infections in 11 (4.8 %) out of the 230 patients studied.",
            "cite_spans": [
                {
                    "start": 641,
                    "end": 645,
                    "mention": "2014",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Zubairi et al. (2012) have investigated 124 patients with cough and pneumonia. The most common microorganism detected was Mycoplasma pneumoniae (n = 21; 17.0 %), followed by Chlamydia pneumoniae (n = 15; 12.0 %), Streptococcus pneumoniae (n = 9; 7.0 %), Haemophilus influenzae (n = 2; 1.6 %), Klebsiella pneumoniae (n = 2; 1.6 %), and Staphylococcus aureus (n = 1; 0.8 %). Streptococcus pneumoniae was the most common organism isolated from blood cultures. That is somewhat in contrast to the present study in which the most frequently detected pathogen was Staphylococcus aureus. Tao et al. (2012) have investigated 593 patients with community acquired pneumonia and identified 242 strains of bacteria isolated from 225 patients. The most common pathogen was Streptococcus pneumoniae (79/242, 32.6 %), followed by Haemophilus influenzae (55/242, 22.7 %), and Klebsiella pneumoniae (25/242, 10.3 %). From a total of 527 patients who underwent serological test for atypical pathogens, Chlamydia pneumoniae infections were identified in 60 (11.4 %) cases. Overall, that study demonstrates a high prevalence of atypical and mixed pathogens. Miyashita et al. (2003) have conducted an investigation on nasopharyngeal specimens and serum samples obtained from 366 adult patients with persistent cough lasting in excess of 2 weeks. Chlamydia pneumoniae was detected in 2 (0.5 %) patients by cell culture, in 20 (5.5 %) patients by PCR, and in 24 (6.5 %) patients by enhanced IgG or IgM antibody titer against the pathogen using a fluorescence test. There was a positive diagnostic match between PCR and serology in 13 cases. The remaining seven PCR positive cases were serologically negative. Out of the 24 serologically positive cases, 11 were PCR negative. That gives an overall mismatch between the serologic and PCR yield of 18 cases, i.e., in about 41 % of Chlamydia pneumoniae infections. The corollary is that either diagnostic method is burdened with a substantial amount of false negative results and both methods should better be used complimentarily. Other respiratory tract pathogens noted in the study outlined above, including Bordetella pertussis and Mycoplasma pneumoniae, were identified by serology in 68 (18.5 %) and in 4 (1.1 %) patients, respectively. Dual infections by Chlamydia pneumoniae and Bordetella pertussis were found in 3 patients. However, no duality of Mycoplasma pneumoniae and Chlamydia pneumoniae or Mycoplasma pneumoniae and Bordetella pertussis infections were noted.",
            "cite_spans": [
                {
                    "start": 16,
                    "end": 20,
                    "mention": "2012",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 593,
                    "end": 597,
                    "mention": "2012",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1156,
                    "end": 1160,
                    "mention": "2003",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In conclusion, the present study demonstrates that the prevalence of Chlamydia pneumoniae infection is overall high and the infection is more prevalent in women than man. Co-infections are often associated with Chlamydia pneumoniae infection. The available diagnostic methods to detect the pathogen have not yet been accurately standardized, which leads to a wide interlaboratory discrepancy in test results, even when employing the same type of test.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1: Incidence of bacterial infections detected in patients with chronic cough\n\na\nStaphylococcus aureus strain MSSA (methicillin-sensitive S. aureus), Streptococcus pyogenes, Moraxella catarrhalis, and Haemophilus influenzae\n",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Chlamydia infection and epidemiology",
            "authors": [
                {
                    "first": "BE",
                    "middle": [],
                    "last": "Batteiger",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Intracellular Pathogens I: Chlamydiales",
            "volume": "",
            "issn": "",
            "pages": "1-26",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Viral-bacterial interactions and risk of acute otitis media complicating upper respiratory tract infection",
            "authors": [
                {
                    "first": "MM",
                    "middle": [],
                    "last": "Pettigrew",
                    "suffix": ""
                },
                {
                    "first": "JF",
                    "middle": [],
                    "last": "Gent",
                    "suffix": ""
                },
                {
                    "first": "RB",
                    "middle": [],
                    "last": "Pyles",
                    "suffix": ""
                },
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Miller",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Nokso-Koivisto",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Chonmaitree",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J Clin Microbiol",
            "volume": "49",
            "issn": "",
            "pages": "3750-3755",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.01186-11"
                ]
            }
        },
        "BIBREF2": {
            "title": "Etiology and antimicrobial resistance of community-acquired pneumonia in adult patients in China",
            "authors": [
                {
                    "first": "LL",
                    "middle": [],
                    "last": "Tao",
                    "suffix": ""
                },
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "LX",
                    "middle": [],
                    "last": "He",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                },
                {
                    "first": "HM",
                    "middle": [],
                    "last": "Xie",
                    "suffix": ""
                },
                {
                    "first": "BQ",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "HY",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "XH",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "CM",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "WW",
                    "middle": [],
                    "last": "Deng",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Chin Med J (Engl)",
            "volume": "125",
            "issn": "",
            "pages": "2967-2972",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Atypical pathogens causing community-acquired pneumonia in adults",
            "authors": [
                {
                    "first": "AB",
                    "middle": [],
                    "last": "Zubairi",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Zafar",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Salahuddin",
                    "suffix": ""
                },
                {
                    "first": "AS",
                    "middle": [],
                    "last": "Haque",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Waheed",
                    "suffix": ""
                },
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Khan",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "J Pak Med Assoc",
            "volume": "62",
            "issn": "",
            "pages": "653-656",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Effect of antibiotic prescribing in primary care on antimicrobial resistance in individual patients: systematic review and meta-analysis",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Costelloe",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Metcalfe",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Lovering",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Mant",
                    "suffix": ""
                },
                {
                    "first": "AD",
                    "middle": [],
                    "last": "Hay",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Br Med J",
            "volume": "340",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1136/bmj.c2096"
                ]
            }
        },
        "BIBREF5": {
            "title": "Differentiation of bacterial and viral community-acquired pneumonia in children",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Don",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Valent",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Korppi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Canciani",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Pediatr Int",
            "volume": "51",
            "issn": "",
            "pages": "91-96",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1442-200X.2008.02678.x"
                ]
            }
        },
        "BIBREF6": {
            "title": "Virus-induced secondary bacterial infection: a concise review",
            "authors": [
                {
                    "first": "MA",
                    "middle": [],
                    "last": "Hendaus",
                    "suffix": ""
                },
                {
                    "first": "FA",
                    "middle": [],
                    "last": "Jomha",
                    "suffix": ""
                },
                {
                    "first": "AH",
                    "middle": [],
                    "last": "Alhammadi",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Ther Clin Risk Manag",
            "volume": "24",
            "issn": "11",
            "pages": "1265-1271",
            "other_ids": {
                "DOI": [
                    "10.2147/TCRM.S87789"
                ]
            }
        },
        "BIBREF7": {
            "title": "Frequency of detection Chlamydophila pneumoniae antigen in children with cough",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Jama-Kmiecik",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Frej-M\u0105drzak",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Sarowska",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Teryks-Wo\u0142yniec",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Go\u015bciniak",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Choroszy-Kr\u00f3l",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Fam Med Prim Care Rev",
            "volume": "16",
            "issn": "",
            "pages": "233-235",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Imaging of Chlamydia and host cell metabolism",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "K\u00e4ding",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Szasz\u00e1k",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rupp",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Future Microbiol",
            "volume": "9",
            "issn": "",
            "pages": "509-521",
            "other_ids": {
                "DOI": [
                    "10.2217/fmb.14.13"
                ]
            }
        },
        "BIBREF9": {
            "title": "Antibiotic prescribing for upper respiratory tract infections in sarawak district hospitals",
            "authors": [
                {
                    "first": "BP",
                    "middle": [],
                    "last": "Kho",
                    "suffix": ""
                },
                {
                    "first": "CM",
                    "middle": [],
                    "last": "Ong",
                    "suffix": ""
                },
                {
                    "first": "FT",
                    "middle": [],
                    "last": "Tan",
                    "suffix": ""
                },
                {
                    "first": "CY",
                    "middle": [],
                    "last": "Wee",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Med J Malaysia",
            "volume": "68",
            "issn": "",
            "pages": "136-140",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Chlamydia pneumoniae infection in adult patients with persistent cough",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Miyashita",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Fukano",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Yoshida",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Niki",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Matsushima",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Med Microbiol",
            "volume": "52",
            "issn": "",
            "pages": "265-269",
            "other_ids": {
                "DOI": [
                    "10.1099/jmm.0.04986-0"
                ]
            }
        },
        "BIBREF11": {
            "title": "Viral infections of the lower respiratory tract: old viruses, new viruses, and the role of diagosis",
            "authors": [
                {
                    "first": "AT",
                    "middle": [],
                    "last": "Pavia",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Clin Infect Dis",
            "volume": "52",
            "issn": "",
            "pages": "284-289",
            "other_ids": {
                "DOI": [
                    "10.1093/cid/cir043"
                ]
            }
        }
    }
}