{
    "paper_id": "PMC3329003",
    "metadata": {
        "title": "Nucleocapsid Protein as Early Diagnostic Marker for SARS",
        "authors": [
            {
                "first": "Xiao-Yan",
                "middle": [],
                "last": "Che",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wei",
                "middle": [],
                "last": "Hao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yadi",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Biao",
                "middle": [],
                "last": "Di",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kai",
                "middle": [],
                "last": "Yin",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yin-Chao",
                "middle": [],
                "last": "Xu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Chang-Sen",
                "middle": [],
                "last": "Feng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Zhuo-Yue",
                "middle": [],
                "last": "Wan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Vincent",
                "middle": [
                    "C.C."
                ],
                "last": "Cheng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok-Yung",
                "middle": [],
                "last": "Yuen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "During the 2003 SARS epidemic in Guangzhou, 420 serum specimens were collected from 317 patients 1\u201390 days after the onset of symptoms. The condition of all patients was diagnosed according to the World Health Organization criteria and confirmed by seroconversion or a fourfold increase in antibody titer against SARS-CoV by means of immunofluorescent testing. The N protein\u2013capture ELISA was performed (7). Briefly, 100 \u00b5L of serum was added to the wells of a microtiter plate coated with a mixture of three anti-N protein monoclonal antibodies, and the plates were incubated at 37\u00b0C for 60 min. After the plates were washed, 100 \u00b5L of anti-N rabbit antiserum was added to the wells, and the plates were incubated at 37\u00b0C for 60 min. The wells were washed again and incubated for 1 h at 37\u00b0C with 100 \u00b5L of peroxidase-conjugated goat anti-rabbit immunoglobulin G (IgG). After the plates were washed, 100 \u00b5L of tetramethylbenzidine solution was added to each well. The experiments involving the use of serum samples from patients with SARS were performed within the safety cabinet of a biosafety level 2 laboratory.",
            "cite_spans": [
                {
                    "start": 404,
                    "end": 405,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The results for the 420 serum specimens tested by the N protein\u2013capture ELISA are shown in Figure 1. The N protein could be detected as early as day 1 and until day 18. In the 146 serum samples positive for N protein, the optical density (OD) value was highly variable from one sample to another on the same day. The sensitivity of detection was 94% (80 of 85 patients) with blood samples taken during the first 5 days and 78% (47 of 60 patients) for samples taken 6\u201310 days after onset of symptoms. The detection rate of N protein decreased to 27% on days 11\u201320 after onset of symptoms. Serum N protein was never detected beyond day 21. Using the same panel of the patient serum samples, we measured the N protein\u2013specific IgG (SARS-CoV N-IgG) and SARS-CoV\u2013specific IgG (SARS-CoV IgG) in serum samples by indirect ELISA, which progressively increased from day 7 onward (Figure 2). With the appearance of antibodies, the N protein detection rate decreased from day 10 after the onset of symptoms. However, from day 7 to day 18, a high level of N protein was still detectable in the serum samples from 11 patients with a mean of OD values of 1.65 when the SARS-CoV N-IgG had already increased to a level with a mean OD value of 1.18.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 91,
                    "end": 99,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 871,
                    "end": 879,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "Serum samples from patients collected 4 years previously were used as negative controls. Patients thought to have cases of SARS on admission and later found to be uninfected by serologic testing, and healthcare workers who had close contact with SARS patients were tested for circulating N protein. Of the serum samples from non-SARS patients collected 4 years previously (n = 400), only one was weakly positive for N protein (OD = 0.288). We tested a total of 110 acute-phase serum samples from 105 patients, initially considered to have suspected SARS and later proven to be negative for SARS-CoV by serologic testing of convalescent-phase serum samples taken >28 days after onset of symptoms, and 315 serum samples from healthcare workers. All were negative for the N protein by capture ELISA. This finding resulted in a test specificity of 99.9% (1 of 825).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Our results suggest that N protein in the serum samples of SARS patients can be detected as early as day 1 after disease onset. Although the level of circulating N protein was highly variable from one person to another from day 1 to day 18, development of SARS-CoV N-IgG appears not to be affected by N protein during the acute phase of the infection, 7 days after the onset of symptoms. The positive detection rate of N protein in serum samples within the first 10 days of infection is higher than that detected by RT-PCR (8,9). Furthermore, the variation in the reported sensitivity and specificity of RT-PCR may be related to the lack of standardization of the assay and the specimen collection (6). Therefore, this viral antigen-capture ELISA may have greater sensitivity, specificity, and ease and reliability of in-use performance than the nucleic acid amplification assay. Further comparative studies with nucleic acid amplification tests should be undertaken at clinical laboratories serving acute-care hospitals where rapid SARS diagnosis is vital.",
            "cite_spans": [
                {
                    "start": 524,
                    "end": 525,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 526,
                    "end": 527,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 699,
                    "end": 700,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: N protein detection in 420 serum samples from 317 patients with severe acute respiratory syndrome (SARS). Data represent the optical density at 450 nm (OD450) of undiluted serum samples. To establish the normal range of the N protein\u2013capture enzyme-linked immunosorbent assay, serum specimens from 400 healthy blood donors were analyzed. The mean OD450 for these specimens, as determined by the assay, was 0.078, with a standard deviation of 0.023. The cutoff OD450 of the assay was then calculated as follows: cutoff = mean of OD450 from 400 normal serum + 5 x standard deviations = 0.19. Solid line represents cutoff value. The result was considered positive if a sample yielded OD450 above the cutoff.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: The profile of N protein detection in blood and antibody response to severe acute respiratory syndrome-associated coronavirus (SARS-CoV) from onset of symptoms to the convalescent phase. IgG, immunoglobulin G.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "The genome sequence of the SARS-associated coronavirus.",
            "authors": [],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": "1399-404",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1085953"
                ]
            }
        },
        "BIBREF1": {
            "title": "Boosting the sensitivity of real-time polymerase-chain-reaction testing for SARS.",
            "authors": [],
            "year": 2004,
            "venue": "N Engl J Med",
            "volume": "350",
            "issn": "",
            "pages": "1577-9",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJM200404083501523"
                ]
            }
        },
        "BIBREF2": {
            "title": "Evaluation of reverse transcription-PCR assays for rapid diagnosis of severe acute respiratory syndrome associated with a novel coronavirus.",
            "authors": [],
            "year": 2003,
            "venue": "J Clin Microbiol",
            "volume": "41",
            "issn": "",
            "pages": "4521-4",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.41.10.4521-4524.2003"
                ]
            }
        },
        "BIBREF3": {
            "title": "Sensitive and quantitative detection of severe acute respiratory syndrome coronavirus infection by real-time nested polymerase chain reaction.",
            "authors": [],
            "year": 2004,
            "venue": "Clin Infect Dis",
            "volume": "38",
            "issn": "",
            "pages": "293-6",
            "other_ids": {
                "DOI": [
                    "10.1086/380841"
                ]
            }
        },
        "BIBREF4": {
            "title": "Detection of SARS coronavirus in patients with severe acute respiratory syndrome by conventional and real-time quantitative reverse transcription-PCR assays.",
            "authors": [],
            "year": 2004,
            "venue": "Clin Chem",
            "volume": "50",
            "issn": "",
            "pages": "67-72",
            "other_ids": {
                "DOI": [
                    "10.1373/clinchem.2003.023663"
                ]
            }
        },
        "BIBREF5": {
            "title": "Real-time reverse transcription-polymerase chain reaction assay for SARS-associated coronavirus.",
            "authors": [],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "311-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Sensitive and specific monoclonal antibody-based capture enzyme immunoassay for detection of nucleocapsid antigen in sera from patients with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2004,
            "venue": "J Clin Microbiol",
            "volume": "42",
            "issn": "",
            "pages": "2629-35",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.42.6.2629-2635.2004"
                ]
            }
        },
        "BIBREF7": {
            "title": "Detection of SARS coronavirus in plasma by real-time RT-PCR.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": "2468-9",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJM200312183492522"
                ]
            }
        },
        "BIBREF8": {
            "title": "Real-time polymerase chain reaction for detecting SARS coronavirus, Beijing, 2003.",
            "authors": [],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "300-3",
            "other_ids": {
                "DOI": []
            }
        }
    }
}