{
    "paper_id": "PMC6093444",
    "metadata": {
        "title": "The Great Opportunity: Cultivating Scientific Inquiry in Medical Residency",
        "authors": [
            {
                "first": "Jatin",
                "middle": [
                    "M"
                ],
                "last": "Vyas",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jayaraj",
                "middle": [],
                "last": "Rajagopal",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Caroline",
                "middle": [
                    "L"
                ],
                "last": "Sokol",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marc",
                "middle": [
                    "N"
                ],
                "last": "Wein",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Michael",
                "middle": [
                    "K"
                ],
                "last": "Mansour",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kathleen",
                "middle": [
                    "E"
                ],
                "last": "Corey",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mark",
                "middle": [
                    "C"
                ],
                "last": "Fishman",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Katrina",
                "middle": [
                    "A"
                ],
                "last": "Armstrong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Inspired by 2 former Massachusetts General Hospital (MGH) Medicine residents, Victor Fedorov, MD, PhD, and Lauren Zeitels, MD, PhD (who died tragically in an avalanche during training), the Pathways Service at MGH is a new initiative, led by Mark Fishman, MD (former Chief of MGH Cardiology, former chief executive officer of Novartis Institute for Biomedical Research, and current faculty member of the MGH Department of Medicine) and Katrina Armstrong, MD (Chair of Medicine, MGH), to enable exploration of a single patient over a 2-week rotation with a rare and difficult-to-diagnose disease [16]. The disassembly of a patient\u2019s problems into organ systems, for the purpose of consultation and care, has missed important opportunities for understanding the unity of disease based on fundamental mechanisms. This experience of care is reflected in medical education where trainees now spend much of their time on checklists of tasks rather than the critical thinking and diagnostic reasoning that brought them to internal medicine in the first place [17]. The Pathways Initiative is founded on the belief that curiosity driven by a single patient with a clear-cut and unexplained constellation of disorders can generate new insights into disease mechanism, creating the synergy across the missions of discovery, clinical care, and education that is fundamental to the societal value of academic medicine. Furthermore, with the technological platforms that have now arisen from molecular biology, there is a substantial opportunity to reconnect biology to the bedside by generating and testing hypotheses about potential unifying mechanisms from unexplained patients.",
            "cite_spans": [
                {
                    "start": 596,
                    "end": 598,
                    "mention": "16",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 1053,
                    "end": 1055,
                    "mention": "17",
                    "ref_id": "BIBREF16"
                }
            ],
            "section": "PATHWAYS: A NEW CLINICAL ROTATION FOCUSING ON CLINICAL INVESTIGATION",
            "ref_spans": []
        },
        {
            "text": "Two to 3 residents rotating through the Pathways service operate as a team to investigate a case referred to Pathways by developing a hypothesis about the patient\u2019s underlying mechanism of disease with both clinical and scientific outcomes. Patients with unexplained presentations/disorders are referred to the Pathways Consult Service from across the medical services. In 1 year, more than 75 inpatients have been referred to the Pathways service. The team will consult on a patient with an unexplained disorder and develop 1 or more hypotheses regarding the pathophysiology underlying the patient\u2019s disorder through meeting with the patient and care team, literature review, and meetings with relevant clinical and scientific experts at MGH and beyond. These hypotheses will be refined through an interdisciplinary conference with senior physician scientists. The rotation has several scheduled meetings with scientists, physician-scientists, and master clinicians from around the world\u2014all focused on this single patient. Most of the rotation is self-directed research time on the case and conference presentation. A number of patients selected for the Pathways elective have relevance to immunology, inflammation, or infection including the mechanism of epithelial damage and bronchiectasis in allergic bronchopulmonary aspergillosis, potential novel infectious agents in a patient with chronic inflammation and vascular leak, and discussion of the mechanism of chronic granulomas driven by unknown antigen.",
            "cite_spans": [],
            "section": "PATHWAYS: A NEW CLINICAL ROTATION FOCUSING ON CLINICAL INVESTIGATION",
            "ref_spans": []
        },
        {
            "text": "Ultimately, with the additional guidance of the director of the Pathways Clinical Faculty, the team will recommend clinical interventions and diagnostics based on their insights, as well as propose basic research questions and experiments that would help further elucidate the nature of the patient\u2019s presentation and underlying mechanism of disease. If the patient consents, there is the potential to execute the proposed experiments at MGH, or with other collaborators. In addition, Pathways residents will have an option to attend ambulatory clinics (such as genetics, infectious diseases, and immunology) chosen to enhance their understanding of the clinical manifestation of disease as well as the application of disease diagnostics.",
            "cite_spans": [],
            "section": "PATHWAYS: A NEW CLINICAL ROTATION FOCUSING ON CLINICAL INVESTIGATION",
            "ref_spans": []
        },
        {
            "text": "By providing the time and resources to focus on understanding the mechanism of disease in a single patient, the goals of this program are to:",
            "cite_spans": [],
            "section": "PATHWAYS: A NEW CLINICAL ROTATION FOCUSING ON CLINICAL INVESTIGATION",
            "ref_spans": []
        },
        {
            "text": "Through the general medical service, our residents care for patients of high complexity and acuity, seeing the pathology of every organ system, and delivering the best care to a socioeconomically and geographically diverse patient population. To emphasize a team-based approach to clinical care and education, the Bigelow Service, comprised of 6 resident teams, is the core of the MGH Department of Medicine\u2019s clinical teaching experience. Each Bigelow team is comprised of 4 interns, a junior assistant resident, and 2 attending physicians who oversee the care of 16\u201318 patients. All interns share responsibility for all patients, and rotate through a 4-day cycle of tasks. The \u201ccall\u201d intern admits up to 5 patients in a 24-hour call period and cross-covers all patients on the floor at night, with nighttime supervision and teaching provided by senior assistant residents. The \u201cplan\u201d intern leads team rounds on all previously admitted patients, creating the plan for the day and leading the daily follow-up bedside interview and examination. The \u201cswing\u201d intern takes the lead in communicating with consultants, coordinating and performing procedures, and managing key time-sensitive tasks that need to be completed before rounds. The junior assistant resident supervises the care provided by the team and leads discussion in work rounds. Interns and medical students experience both a wide breadth of patients and the educational opportunities of team dialogue in caring for patients when on a Bigelow team.",
            "cite_spans": [],
            "section": "THE RETURN OF PHYSICIAN-SCIENTISTS AS ATTENDING PHYSICIANS ON THE INPATIENT MEDICAL SERVICE",
            "ref_spans": []
        },
        {
            "text": "The opportunity to have 2 attending physicians on each Bigelow team permits pairing individuals with complementary skill sets. We have developed a robust set of core educator faculty who are national leaders in innovative medical and clinical education and provide expertise across a wide range of fields in academic medicine. During their nonclinical time, core educators teach in other educational venues, lead scholarly projects, advance research in medical education, and develop novel curricula. We have paired a number of different internists who may normally not fully engage in serving as the sole leader of an inpatient team with one of the core educators to broaden the exposure of internal medicine to residents. Specifically, we have reengaged our physician-scientists from the Department of Medicine, including infectious diseases, to serve as a co-attending in this model. Not only do these individuals bring their subspecialty expertise, but importantly, they serve as role models for trainees and can use the interstices of their daily schedule to excite residents and students about a career in investigation by using patients on the clinical service to drive intellectual curiosity and provide a framework on how we can investigate the unknowns. The ability to carry a smaller list of patients, but also have meaningful interactions with residents, permits younger physician-scientists to engage rather than actively excluding clinical work in the early part of their careers.",
            "cite_spans": [],
            "section": "THE RETURN OF PHYSICIAN-SCIENTISTS AS ATTENDING PHYSICIANS ON THE INPATIENT MEDICAL SERVICE",
            "ref_spans": []
        },
        {
            "text": "In addition to the inpatient service, the leadership of the residency has active research investigators. One of the authors (J. M. V.) is the Residency Program Director as well as an active physician-scientist. His laboratory remains active and has enjoyed continuous National Institutes of Health (NIH) funding. His area of interest is in innate immunity to fungal pathogens. Many of the residency leaders are also active investigators with successful research programs. This critical mass of faculty prominently seen by the residents can have a lasting impact on their subsequent choices for careers.",
            "cite_spans": [],
            "section": "THE RETURN OF PHYSICIAN-SCIENTISTS AS ATTENDING PHYSICIANS ON THE INPATIENT MEDICAL SERVICE",
            "ref_spans": []
        },
        {
            "text": "Tools of Human Investigation (THI) is a 2-week elective rotation for medicine residents that focuses on clinical investigation, analytic and presentation skills, and career development. The course director is a physician-scientist (K. E. C.) with substantial NIH funding for her research program and receives financial support as an Associate Program Director for the Residency Program. The course is offered 4\u20135 times throughout the academic year to retain a small-group, interactive atmosphere while ensuring that all residents participate. Residents do not have inpatient clinical responsibilities during the THI course. Residents are exposed to a broad array of investigators in the MGH community at all stages of their careers. At each session, faculty members summarize their own early career path, describe their research, and teach interpretation of the literature, presentation, writing, and research skills, exposing residents to a wide variety of research and academic careers and preparing them for leadership regardless of their chosen field. The specific aims of Tools of Human Investigation are to (1) introduce pathways for early career development; (2) provide an overview of hypothesis generation and testing using a variety of study designs; (3) provide introductory principles of clinical research, including clinical trials, genetics and genomics, epidemiology, health services research, and translational research; and (4) provide the critical thinking skills necessary to appraise the scientific literature and communicate their ideas effectively. Residents also work on existing research projects or develop a new research question based on a clinical question from their ward experiences and then propose methods to test their hypotheses. Residents present their projects to faculty mentors and program leadership on the final day of the course. Residents are also encouraged to submit their projects to the Annual Resident Research Day sponsored by the Department of Medicine. Selected recent lectures in the THI Course include \u201cThe Genetics of Type 2 Diabetes,\u201d \u201cStatistics for the Basic Scientist,\u201d and \u201cPerspectives on Academic Career Development Seen Through the History of 50 Years of Parathyroid Hormone Research.\u201d Ongoing analysis will measure the impact of these programs on resident career choices.",
            "cite_spans": [],
            "section": "EQUIPPING RESIDENTS WITH THE TOOLS FOR HUMAN INVESTIGATION",
            "ref_spans": []
        },
        {
            "text": "There has never been a better time to be in medicine than now. The fruits of years of basic research are now being realized. Therapies for HIV and hepatitis C virus (HCV) infections have been revolutionized through a mechanistic understanding of the viral life cycle. Drugs that convert HIV from a deadly infection to a chronic disease are routinely administered. We can now cure HCV. Progress in these areas has not stopped, and we hope to witness vaccines that will prevent these infections in the near future. The lifeblood of research-oriented academic medical centers are the physician-scientists. A concerted effort to ensure a steady supply of these investigation-oriented physicians is critical to the long-term success of biomedical research. Many challenges exist in the training environment including the fractured nature of training (between clinical and research training, opportunity costs as well as the overall length of training). These issues need to be addressed directly. The federal government as well as nongovernmental organizations (including Burroughs Wellcome Fund and the Howard Hughes Medical Institute) have begun to address this critical need. Along with these initiatives, we firmly believe that it is our responsibility to instill passion for scientific inquiry and investigation as a major component of the educational mission and culture of internal medicine residency programs.",
            "cite_spans": [],
            "section": "FUTURE PERSPECTIVES",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "History of science. A golden era of Nobel laureates",
            "authors": [
                {
                    "first": "JL",
                    "middle": [],
                    "last": "Goldstein",
                    "suffix": ""
                },
                {
                    "first": "MS",
                    "middle": [],
                    "last": "Brown",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Science",
            "volume": "338",
            "issn": "",
            "pages": "1033-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Creating a Nobel culture",
            "authors": [
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Flier",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Science",
            "volume": "339",
            "issn": "",
            "pages": "140-1",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Origin of the term internal medicine",
            "authors": [
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Bloomfield",
                    "suffix": ""
                }
            ],
            "year": 1959,
            "venue": "J Am Med Assoc",
            "volume": "169",
            "issn": "",
            "pages": "1628-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Inspiring the next generation of physician-scientists",
            "authors": [
                {
                    "first": "RJ",
                    "middle": [],
                    "last": "Lefkowitz",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Clin Invest",
            "volume": "125",
            "issn": "",
            "pages": "2905-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Rescuing the physician-scientist workforce: the time for action is now",
            "authors": [
                {
                    "first": "DM",
                    "middle": [],
                    "last": "Milewicz",
                    "suffix": ""
                },
                {
                    "first": "RG",
                    "middle": [],
                    "last": "Lorenz",
                    "suffix": ""
                },
                {
                    "first": "TS",
                    "middle": [],
                    "last": "Dermody",
                    "suffix": ""
                },
                {
                    "first": "LF",
                    "middle": [],
                    "last": "Brass",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Clin Invest",
            "volume": "125",
            "issn": "",
            "pages": "3742-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "The vanishing physician-scientist",
            "authors": [
                {
                    "first": "AI",
                    "middle": [],
                    "last": "Schafer",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Transl Res",
            "volume": "155",
            "issn": "",
            "pages": "1-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "The National Institutes of Health Physician-Scientist Workforce Working Group report: a roadmap for preserving the physician-scientist",
            "authors": [
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Feldman",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Clin Transl Sci",
            "volume": "7",
            "issn": "",
            "pages": "289-90",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "NIH research funding and early career physician scientists: continuing challenges in the 21st century",
            "authors": [
                {
                    "first": "HH",
                    "middle": [],
                    "last": "Garrison",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Deschamps",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "FASEB J",
            "volume": "28",
            "issn": "",
            "pages": "1049-58",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Trends in infectious disease mortality in the United States during the 20th century",
            "authors": [
                {
                    "first": "GL",
                    "middle": [],
                    "last": "Armstrong",
                    "suffix": ""
                },
                {
                    "first": "LA",
                    "middle": [],
                    "last": "Conn",
                    "suffix": ""
                },
                {
                    "first": "RW",
                    "middle": [],
                    "last": "Pinner",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "JAMA",
            "volume": "281",
            "issn": "",
            "pages": "61-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Infectious disease mortality trends in the United States, 1980\u20132014",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Hansen",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Oren",
                    "suffix": ""
                },
                {
                    "first": "LK",
                    "middle": [],
                    "last": "Dennis",
                    "suffix": ""
                },
                {
                    "first": "HE",
                    "middle": [],
                    "last": "Brown",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "JAMA",
            "volume": "316",
            "issn": "",
            "pages": "2149-51",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Direct identification and characterisation of beta-adrenergic receptors in rat brain",
            "authors": [
                {
                    "first": "RW",
                    "middle": [],
                    "last": "Alexander",
                    "suffix": ""
                },
                {
                    "first": "JN",
                    "middle": [],
                    "last": "Davis",
                    "suffix": ""
                },
                {
                    "first": "RJ",
                    "middle": [],
                    "last": "Lefkowitz",
                    "suffix": ""
                }
            ],
            "year": 1975,
            "venue": "Nature",
            "volume": "258",
            "issn": "",
            "pages": "437-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Expression of the familial hypercholesterolemia gene in heterozygotes: mechanism for a dominant disorder in man",
            "authors": [
                {
                    "first": "MS",
                    "middle": [],
                    "last": "Brown",
                    "suffix": ""
                },
                {
                    "first": "JL",
                    "middle": [],
                    "last": "Goldstein",
                    "suffix": ""
                }
            ],
            "year": 1974,
            "venue": "Science",
            "volume": "185",
            "issn": "",
            "pages": "61-3",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Contribution of dendritic cells to stimulation of the murine syngeneic mixed leukocyte reaction",
            "authors": [
                {
                    "first": "MC",
                    "middle": [],
                    "last": "Nussenzweig",
                    "suffix": ""
                },
                {
                    "first": "RM",
                    "middle": [],
                    "last": "Steinman",
                    "suffix": ""
                }
            ],
            "year": 1980,
            "venue": "J Exp Med",
            "volume": "151",
            "issn": "",
            "pages": "1196-212",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Immune control of HIV-1 after early treatment of acute infection",
            "authors": [
                {
                    "first": "ES",
                    "middle": [],
                    "last": "Rosenberg",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Altfeld",
                    "suffix": ""
                },
                {
                    "first": "SH",
                    "middle": [],
                    "last": "Poon",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Nature",
            "volume": "407",
            "issn": "",
            "pages": "523-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Endosomes from kidney collecting tubule cells contain the vasopressin-sensitive water channel",
            "authors": [
                {
                    "first": "AS",
                    "middle": [],
                    "last": "Verkman",
                    "suffix": ""
                },
                {
                    "first": "WI",
                    "middle": [],
                    "last": "Lencer",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Brown",
                    "suffix": ""
                },
                {
                    "first": "DA",
                    "middle": [],
                    "last": "Ausiello",
                    "suffix": ""
                }
            ],
            "year": 1988,
            "venue": "Nature",
            "volume": "333",
            "issn": "",
            "pages": "268-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Toward a culture of scientific inquiry\u2014the role of medical teaching services",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Armstrong",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Ranganathan",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Fishman",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "N Engl J Med",
            "volume": "378",
            "issn": "",
            "pages": "1-3",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Diagnostic reasoning: an endangered competency in internal medicine training",
            "authors": [
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Simpkin",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Vyas",
                    "suffix": ""
                },
                {
                    "first": "KA",
                    "middle": [],
                    "last": "Armstrong",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Ann Intern Med",
            "volume": "167",
            "issn": "",
            "pages": "507-8",
            "other_ids": {
                "DOI": []
            }
        }
    }
}