{
    "paper_id": "PMC4696717",
    "metadata": {
        "title": "Multiorgan WU Polyomavirus Infection in Bone Marrow Transplant Recipient",
        "authors": [
            {
                "first": "Erica",
                "middle": [
                    "A."
                ],
                "last": "Siebrasse",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Nang",
                "middle": [
                    "L."
                ],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Melisa",
                "middle": [
                    "J."
                ],
                "last": "Willby",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Dean",
                "middle": [
                    "D."
                ],
                "last": "Erdman",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marilyn",
                "middle": [
                    "A."
                ],
                "last": "Menegus",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In January 2001, a 27-month-old girl was admitted to an upstate New York area hospital for a 5/6 human leukocyte antigen\u2013matched cord blood transplant from an unrelated donor. The patient had been born by normal vaginal delivery after 40 weeks of gestation. Her medical history included leukocytosis at 3 months and splenomegaly at 6 months of age. Refractory juvenile myelomonocytic leukemia was diagnosed when she was 16 months of age, and she underwent splenectomy in September 2000. She had multiple infections before 2 years of age, including otitis media, a central vein catheter infection, and a urinary tract infection. She also demonstrated failure to thrive, developmental delay, mild pulmonic stenosis, and gastroesophageal reflux.",
            "cite_spans": [],
            "section": "The Case ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Three weeks after the bone marrow transplant, the child experienced fever; diarrhea; hepatomegaly; and erythema on her face, palms, and soles. She was evaluated for graft versus host disease, viral exanthema, and drug eruption. The results of skin biopsies performed at 4 weeks after transplantation ruled out graft versus host disease and drug eruption. A rectosigmoid biopsy performed at the same time as the skin biopsies showed mild stromal edema but was negative for adenovirus and cytomegalovirus by immunohistochemistry (IHC) staining. Throughout the course of the patient\u2019s hospitalization, adenovirus was intermittently isolated from her feces and urine and influenza B virus was detected in her nose and throat. PCR testing of the blood for cytomegalovirus was consistently negative. Treatment included cyclosporine; Solu-medrol (Pharmacia & Upjohn LLC, New York, NY, USA); intravenous immunoglobulin; ribavirin; Zosyn (Pfizer Inc., New York, NY, USA); Flagyl (Pfizer Inc.); Flutamine (Schering-Plough, Kenilworth, NJ, USA); Tamiflu (Roche Pharmaceuticals, Nutley, NJ, USA); Demerol (Sanofi-Aventis U.S. LLC, Bridgewater, NJ, USA); Zofran (GlaxoSmithKline, Philadelphia, PA, USA); Phenergan (Wyeth, Madison, NJ, USA); Tylenol (Johnson & Johnson, New Brunswick, NJ, USA); albuterol; isradipine; Spironolactone (Mylan Pharmaceuticals, Morgantown, WV, USA); hemotransfusion; and platelet transfusion. Despite this aggressive therapy, the patient\u2019s condition continued to deteriorate. On March 1, 2001, the patient was transferred to the pediatric intensive care unit because of respiratory failure. Chest radiographs revealed pulmonary edema. Her condition was stabilized 2 days later, but severe acute respiratory distress syndrome and distended abdomen developed on April 1. Treatment was continued and mechanical ventilation was added. A radiograph taken on April 15 (\u224811 weeks after transplantation) revealed free air in the abdominal cavity, but the source was not identified. The patient died later that day; the probable cause of death was viral pneumonitis. An autopsy was performed.",
            "cite_spans": [],
            "section": "The Case ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Lung tissues were fixed in formalin and then postfixed in 2.5% glutaraldehyde/0.1 mol/L Millonig phosphate buffer before processing into epoxy resin for sectioning and film photography. Microscopic examination was performed with a Hitachi 7100 transmission electron microscope (Hitachi High-Technologies Science America Inc., Northridge, CA, USA). ",
            "cite_spans": [],
            "section": "Electron Microscopy ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "IHC was performed as described previously (11). In brief, formalin-fixed paraffin-embedded blocks of tissue were deparaffinized, rehydrated, and treated with 3% hydrogen peroxide. Antigen was retrieved, and samples were blocked in 1.5% normal horse serum. A mouse monoclonal antibody against viral protein (VP) 1 from WUPyV (WU-VP1) (NN-Ab06) or an isotype-matched control antibody (mouse IgG2b; BD Biosciences, San Jose, CA, USA; no. 557351) was incubated overnight. After incubation with the biotinylated antimouse IgG secondary antibody (Vector BA-2000; Vector Laboratories, Inc., Burlingame, CA, USA), slides were developed by using the Vectastain ABC kit (Vector Laboratories, Inc.; no. PK-6100) and DAB (Vector Laboratories, Inc.; no. SK-4100), counterstained with hematoxylin, dehydrated, cleared, and mounted.",
            "cite_spans": [
                {
                    "start": 43,
                    "end": 45,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "IHC  ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "The double IHC (dIHC) staining protocol was similar to the regular IHC protocol with the addition of several steps. After the blocking step, slides were incubated with NN-Ab06 and then the secondary antibody. Development was accomplished by using the ABC kit and ImmPACT SG (Vector Laboratories, Inc.; no. SK-4705). Tissues were blocked with avidin and biotin (Vector Laboratories, Inc.; no. SP-2001) then with 1.5% normal horse serum. Slides were incubated in the second primary antibody against CD68 (Dako, Glostrup, Denmark; no. M081401) and then in biotinylated antimouse IgG secondary antibody. The second set of staining was developed by using the ABC kit and 3,3\u2032-diaminobenzidine, followed by dehydration. For the MUC5AC dIHC assay, staining was first performed with the monoclonal antibody against MUC5AC (Thermo Fischer, Rockford, IL, USA; no. MA1\u201338223), which was developed with 3,3\u2032-diaminobenzidine. Staining with NN-Ab06 and development with SG substrate (Vector Laboratories, Inc.) followed the blocking steps. Tissues stained by using the dIHC protocol were not counterstained. Control staining with an IgG2b isotype antibody (for NN-Ab06) and an isotype antibody against IgG1 (for the CD68 and MUC5AC antibodies) was also performed.",
            "cite_spans": [],
            "section": "Double IHC  ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "DNA was extracted from formalin-fixed paraffin-embedded samples by using the QIAGEN BioRobot M48 workstation and MagAttract DNA Mini Kit (QIAGEN, Valencia, CA, USA). A quantitative real-time PCR (qPCR) assay for detection and viral load estimation of WUPyV/KIPyV was developed and used to screen the extracted DNA. Primer and probe sequences for the qPCR assay were forward 5\u2032-GTAGCTGGAGGAGCAGAGGC-3\u2032; 5\u2032-CACCAAGRGCAGCTAARCCTTC-3\u2032; and probe 5\u2032-CTGGWTCTGGAGCTGCMATAGCWACTGGT-3\u2032. qPCRs were performed by using iQ Supermix reagents (Bio-Rad, Hercules, CA, USA); each 25-\u00b5L reaction mixture contained 0.6 \u03bcmol/L forward primer, 0.3 \u03bcmol/L reverse primer, 0.1 \u03bcmol/L probe, and 5 \u03bcL nucleic acid extract. Amplification was conducted on an iCycler iQ Real-time Detection System (Bio-Rad). Thermocycling conditions consisted of 3 min at 95\u00b0C for activation of the iTaq DNA polymerase and 45 cycles of 15 s at 95\u00b0C and 1 min at 60\u00b0C. Extracts were also tested for human bocavirus (HBoV) by using a previously described qPCR assay (12). For distinguishing between WUPyV and KIPyV, conventional PCR and sequencing was performed by using primers forward 5\u2032-GGAGCTGTAYAGAATGGAAAC-3\u2032 and reverse 5\u2032-TTCATCCAAYAGTGGAATTG-3\u2032.",
            "cite_spans": [
                {
                    "start": 1024,
                    "end": 1026,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Nucleic Acid Extraction and PCR ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Multiple segments of the WUPyV genome were amplified and sequenced by using overlapping primer sets (Table 1) designed from reference strain WU/Wuerzburg/02/07 (GenBank accession no. EU711057.1). Amplicons were directly sequenced by using the ABI Prism BigDye Terminator Cycle Sequencing Ready Reaction Kit, version 3.1, on an ABI 3130 XL DNA Sequencer (both from Applied Biosystems, Carlsbad, CA, USA). The complete genome sequence for isolate Rochester-7029 is available through GenBank under accession no. FJ794068.",
            "cite_spans": [],
            "section": "Complete Genome Sequencing ::: Materials and Methods",
            "ref_spans": [
                {
                    "start": 101,
                    "end": 108,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The recent discoveries of HBoV, another small, circular DNA virus, in 2005 and 2 new polyomaviruses, WU and KI (KIPyV), in 2007, in the respiratory tracts of children with acute respiratory illness prompted us to investigate the involvement of these viruses in this case. DNA extracted from formalin-fixed paraffin-embedded lung, liver, kidney, and gastrointestinal tissues was tested by qPCR for WUPyV, KIPyV, and HBoV. All 4 tissues were positive for WUPyV, whereas KIPyV and HBoV were not detected in any of the tissues. Virus loads estimated by qPCR were substantially higher in samples from the lung (cycle threshold 16.6) than in samples from the liver, kidney, and gastrointestinal tract (cycle threshold 30.2\u201330.8; Table 2). The entire WUPyV genome (designated Rochester-7029, GenBank accession no. FJ794068) was subsequently sequenced from lung tissue to 4\u00d7 coverage (each base sequenced independently 4 times) by using multiple primer sets and found to be 5,306 bp. Compared with 79 complete WUPyV genome sequences available in GenBank, nucleotide identity scores for Rochester-7029 ranged from 0.970 to 0.985. Compared with the reference sequence, Rochester-7029 had 8 single-nucleotide polymorphisms, 5 of which were in coding regions. Two of these 5 were synonymous mutations. We predicted an amino acid change in VP2 and VP3 from glutamic acid to glutamine at positions 250 and 107, respectively. We also predicted amino acid changes in large T-antigen: glutamine to glutamic acid at position 134 and isoleucine to leucine at position 594. Of note, the Rochester-7029 genome contained a 77-bp terminal duplication in the large T-antigen as compared with the reference WUPyV genome, which was not predicted to have any effect on the size or sequence of the translated protein because it was located 3\u2032 to the T-antigen stop codon.",
            "cite_spans": [],
            "section": "WUPyV in the Lungs ::: Results",
            "ref_spans": [
                {
                    "start": 723,
                    "end": 730,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "After detection of WUPyV in the patient\u2019s tissues by real-time qPCR, WUPyV-specific IHC with a previously described assay (11) was performed on available tissues (lung, liver, kidney, and gastrointestinal tract) to determine whether WU-VP1 antigen was also present (Figure 2). Liver, kidney, and gastrointestinal tissues were all negative (Table 2). Staining was observed in the lung (Figure 2, panels A, C) and the trachea (Figure 2, panel E), but no staining was observed in serial sections stained with an isotype control antibody (Figure 2, panels B, D, F). Serial sections stained with no primary or secondary antibodies were also negative (not shown). Overall, we saw 3 patterns of staining in the lung. In some cells, WU-VP1 staining was primarily in the nucleus. In others, the perimeter of the nucleus was strongly positive. And in others, the staining was diffuse, making it difficult to discern its position within cells. Of note, the tracheal staining was within a submucosal gland, where WUPyV tropism has not been previously described.",
            "cite_spans": [
                {
                    "start": 123,
                    "end": 125,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "WUPyV in the Lungs ::: Results",
            "ref_spans": [
                {
                    "start": 266,
                    "end": 274,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 385,
                    "end": 393,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 425,
                    "end": 433,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 535,
                    "end": 543,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 340,
                    "end": 347,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "A recent article described detection of WU-VP1 in epithelial cells obtained from a bronchoalveolar lavage of a lung transplant recipient with Job syndrome (11). We anticipated that some WUPyV-positive cells in lung tissues of the patient reported here were also epithelial cells, but we did not explicitly confirm this suspicion because of the limited amount of lung tissue available. Instead we chose to explore additional hypotheses regarding potential tropisms of WUPyV because of our recent detection of KIPyV in CD68-positive cells (16). CD68 is a glycoprotein present on monocytes and macrophages. We performed dIHC testing by using the monoclonal antibody against WU-VP1 (NN-Ab06) and a monoclonal antibody against CD68, which primarily labels macrophages and monocytes. Cells positive for both WU-VP1 and CD68 were detected within the patient\u2019s lung tissue (Figure 3). In addition, the cell shown in Figure 3, panel B (arrow) is morphologically consistent with a foamy macrophage, a specific morphotype of macrophage that is laden with lipid droplets in the cytoplasm (17). A serial section stained with isotype-matched antibodies (IgG2b for NN-Ab06 and IgG1 for the anti-CD68 antibody) was negative (not shown). In a recent study, KIPyV was also detected in a foamy macrophage (16). ",
            "cite_spans": [
                {
                    "start": 156,
                    "end": 158,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 538,
                    "end": 540,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1077,
                    "end": 1079,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1287,
                    "end": 1289,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "WU-VP1 in CD68-Positive Cells ::: Results",
            "ref_spans": [
                {
                    "start": 866,
                    "end": 874,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 908,
                    "end": 916,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "The initial IHC staining of tracheal tissue revealed positive cells within a submucosal gland. MUC5AC is the principal mucin produced by goblet cells, and MUC5B is produced by submucosal glands (18). We developed 2 dIHC assays: NN-Ab06 and a monoclonal antibody against MUC5AC and NN-Ab06 and a polyclonal antibody against MUC5B. Each assay was performed on control cell pellets (not shown). The MUC5AC dIHC assay yielded clearer staining, so we chose to apply this assay to the tracheal tissue from the patient. We detected WU-VP1\u2013positive cells in association with a cluster of cells showing MUC5-AC positivity (Figure 4). It is unclear whether the 2 antigens colocalize to the same cell. WU-VP1\u2013positive cells were also seen separate from MUC5AC-positive cells, suggesting that a subset of virus-positive cells do not produce mucin. We found 2 such areas in the tracheal tissue.",
            "cite_spans": [
                {
                    "start": 195,
                    "end": 197,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "WU-VP1 Antigen in Association with Mucin-Producing Cells ::: Results",
            "ref_spans": [
                {
                    "start": 614,
                    "end": 622,
                    "mention": "Figure 4",
                    "ref_id": "FIGREF3"
                }
            ]
        },
        {
            "text": "We describe a case of viral pneumonitis in a bone marrow transplant recipient who died in 2001. Before her death, influenza and adenovirus were identified from the patient. Although samples from the patient were initially tested for several viruses by IHC and culture, no agent was identified. Subsequent work since 2007 showed that multiple tissues from this patient were positive for WUPyV by real-time PCR and IHC, but the same tissues were negative for KIPyV and HBoV by PCR. To date, WUPyV has been the only virus detected in tissue samples from this patient. Crystalline lattices of polyomavirus-like particles were seen in the lung, which substantiated the high virus titers measured by qPCR. Collectively, these observations suggest a potential pathogenic role for WUPyV infection in this case. However, we cannot rule out the possibility that WUPyV infection was simply opportunistic in this severely immunocompromised patient.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Analysis of samples from this patient provided novel insights into fundamental properties of WUPyV infection in vivo. In the lungs, we detected WUPyV antigen in CD68-positive cells (probably of the macrophage/monocyte lineage) by immunohistochemistry. WUPyV is most closely related to KIPyV, and the viruses share many similarities, including an apparent tropism for CD68-positive cells (16). Other polyomaviruses have also been detected in cells of the monocytic lineage (16). We do not believe that this detection represents phagocytosis of other WUPyV-infected cell types because WU-VP1, a late-expressed protein, was detected in the nucleus of CD68-positive cells, suggesting an infection in this patient. Granted, we did not prove that infectious particles were produced.",
            "cite_spans": [
                {
                    "start": 388,
                    "end": 390,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 473,
                    "end": 475,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "WU-VP1 was also detected in close proximity to MUC5AC-positive cells in tracheal tissue. The detection of WUPyV in tracheal tissue was unexpected and expands the known tissue tropism of the virus. As previously mentioned, MUC5AC is a mucin primarily produced by goblet cells in the airway. Several viruses in glandular cells in the trachea have been described: adenovirus has grown in primary cultured peribronchial submucosal gland cells (19); rhinovirus has grown in human respiratory submucosal gland cells (20); severe acute respiratory syndrome coronavirus antigen and RNA have been detected in tracheal/bronchial serous gland epithelium (21); and BKPyV has been shown to replicate in salivary gland cells (22,23).",
            "cite_spans": [
                {
                    "start": 440,
                    "end": 442,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 511,
                    "end": 513,
                    "mention": "20",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 644,
                    "end": 646,
                    "mention": "21",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 712,
                    "end": 714,
                    "mention": "22",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 715,
                    "end": 717,
                    "mention": "23",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In conclusion, WUPyV was detected by multiple methods in the lung of a bone marrow transplant recipient who had viral pneumonitis at the time of death. Tracheal tissue from this patient was also positive for WU-VP1. Viral antigen was specifically detected in CD68-positive cells and in close association with MUC5AC-positive cells within a tracheal submucosal gland. The role of WUPyV as a human pathogen remains unclear, although the evidence for an infection of the respiratory tract in this patient is strong. This study expands our understanding of WUPyV biology and tropism beyond detection of virus in body fluids.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Electron micrographs of polyomavirus-like particles in lung from a child with fatal acute respiratory illness. A) Low-power view of a nucleus displaying multiple electron dense crystalline arrays. Scale bar indicates 0.5 \u03bcm; original magnification \u00d710,000. B) Higher-power magnification of nucleus in panel A. Scale bar indicates 100 nm; original magnification \u00d730,000. C) Large cluster of putative polyomavirus virions. Scale bar indicates 250 nm; original magnification \u00d720,000.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Immunohistochemical detection of WU polyomavirus viral protein 1 in respiratory tract of a child with fatal acute respiratory illness. Human lung tissue at original magnification of \u00d7200, stained with a monoclonal antibody against WU polyomavirus viral protein 1 (designated NN-Ab06) (A, C) or an isotype control antibody (B, D). Human tracheal tissue at original magnification of \u00d7200, stained with NN-Ab06 (E) or an isotype control antibody (F). The middle panels show insets from panels A, C, and E (dotted boxes) at higher original magnifications (\u00d7600).",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3: Detection of WU polyomavirus viral protein 1 in CD68-positive cells from a child with fatal acute respiratory illness. Lung tissue stained with NN-Ab06 (blue) and a monoclonal antibody against CD68 (brown). A) Tissue at original magnification of \u00d7400. B) Closer view of cell from panel A consistent with a foamy macrophage (arrow). Original magnification \u00d71,000. C) Closer view of cells from panel A. Original magnification \u00d71,000. D) Different field of the tissue section with another double-positive cell. Original magnification \u00d71,000.",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Figure 4: Detection of WU polyomavirus viral protein 1 in close proximity to MUC5AC-positive cells in the trachea of a child with fatal acute respiratory illness. Tracheal tissue stained with NN-Ab06 (blue) and a monoclonal antibody against MUC5AC (brown). A) Tissue at original magnification of \u00d7200. B) Tissue at original magnification of \u00d7600.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Discovery and epidemiology of the human polyomaviruses BK virus (BKV) and JC virus (JCV).",
            "authors": [],
            "year": 2006,
            "venue": "Adv Exp Med Biol",
            "volume": "577",
            "issn": "",
            "pages": "19-45",
            "other_ids": {
                "DOI": [
                    "10.1007/0-387-32957-9_2"
                ]
            }
        },
        "BIBREF1": {
            "title": "The novel KI, WU, MC polyomaviruses: possible human pathogens?",
            "authors": [],
            "year": 2011,
            "venue": "New Microbiol",
            "volume": "34",
            "issn": "",
            "pages": "1-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "WU polyomavirus in respiratory epithelial cells from lung transplant patient with Job syndrome.",
            "authors": [],
            "year": 2015,
            "venue": "Emerg Infect Dis",
            "volume": "21",
            "issn": "",
            "pages": "103-6",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2101.140855"
                ]
            }
        },
        "BIBREF3": {
            "title": "Real-time PCR assays for detection of bocavirus in human specimens.",
            "authors": [],
            "year": 2006,
            "venue": "J Clin Microbiol",
            "volume": "44",
            "issn": "",
            "pages": "3231-5",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.00889-06"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2007,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Observations on the fine structure of mature herpes simplex virus and on the composition of its nucleoid.",
            "authors": [],
            "year": 1962,
            "venue": "J Exp Med",
            "volume": "115",
            "issn": "",
            "pages": "1-12",
            "other_ids": {
                "DOI": [
                    "10.1084/jem.115.1.1"
                ]
            }
        },
        "BIBREF6": {
            "title": "Electron microscopy in the diagnosis of BK-polyoma virus infection in the transplanted kidney.",
            "authors": [],
            "year": 2005,
            "venue": "Ultrastruct Pathol",
            "volume": "29",
            "issn": "",
            "pages": "469-74",
            "other_ids": {
                "DOI": [
                    "10.1080/01913120500323399"
                ]
            }
        },
        "BIBREF7": {
            "title": "Immunohistochemical detection of KI polyomavirus in lung and spleen.",
            "authors": [],
            "year": 2014,
            "venue": "Virology",
            "volume": "468\u2013470",
            "issn": "",
            "pages": "178-84",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Foamy macrophages and the progression of the human tuberculosis granuloma.",
            "authors": [],
            "year": 2009,
            "venue": "Nat Immunol",
            "volume": "10",
            "issn": "",
            "pages": "943-8",
            "other_ids": {
                "DOI": [
                    "10.1038/ni.1781"
                ]
            }
        },
        "BIBREF9": {
            "title": "Epithelial cells and airway diseases.",
            "authors": [],
            "year": 2011,
            "venue": "Immunol Rev",
            "volume": "242",
            "issn": "",
            "pages": "186-204",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1600-065X.2011.01033.x"
                ]
            }
        },
        "BIBREF10": {
            "title": "Morphology of adenovirus type-3 infection of human respiratory epithelial cells in vitro.",
            "authors": [],
            "year": 2002,
            "venue": "Virchows Arch",
            "volume": "440",
            "issn": "",
            "pages": "512-8",
            "other_ids": {
                "DOI": [
                    "10.1007/s004280100534"
                ]
            }
        },
        "BIBREF11": {
            "title": "Trichodysplasia spinulosa is characterized by active polyomavirus infection.",
            "authors": [],
            "year": 2012,
            "venue": "J Clin Virol",
            "volume": "53",
            "issn": "",
            "pages": "225-30",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcv.2011.11.007"
                ]
            }
        },
        "BIBREF12": {
            "title": "Infection of human respiratory submucosal glands with rhinovirus: effects on cytokine and ICAM-1 production.",
            "authors": [],
            "year": 1999,
            "venue": "Am J Physiol",
            "volume": "277",
            "issn": "",
            "pages": "L362-71",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Organ distribution of severe acute respiratory syndrome (SARS) associated coronavirus (SARS-CoV) in SARS patients: implications for pathogenesis and virus transmission pathways.",
            "authors": [],
            "year": 2004,
            "venue": "J Pathol",
            "volume": "203",
            "issn": "",
            "pages": "622-30",
            "other_ids": {
                "DOI": [
                    "10.1002/path.1560"
                ]
            }
        },
        "BIBREF14": {
            "title": "BK virus has tropism for human salivary gland cells in vitro: implications for transmission.",
            "authors": [],
            "year": 2009,
            "venue": "Virology",
            "volume": "394",
            "issn": "",
            "pages": "183-93",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virol.2009.07.022"
                ]
            }
        },
        "BIBREF15": {
            "title": "Replication of oral BK virus in human salivary gland cells.",
            "authors": [],
            "year": 2014,
            "venue": "J Virol",
            "volume": "88",
            "issn": "",
            "pages": "559-73",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02777-13"
                ]
            }
        },
        "BIBREF16": {
            "title": "Update on human polyomaviruses and cancer.",
            "authors": [],
            "year": 2010,
            "venue": "Adv Cancer Res",
            "volume": "106",
            "issn": "",
            "pages": "1-51",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "Identification of a novel polyomavirus in a pancreatic transplant recipient with retinal blindness and vasculitic myopathy.",
            "authors": [],
            "year": 2014,
            "venue": "J Infect Dis",
            "volume": "210",
            "issn": "",
            "pages": "1595-9",
            "other_ids": {
                "DOI": [
                    "10.1093/infdis/jiu250"
                ]
            }
        },
        "BIBREF18": {
            "title": "A novel human polyomavirus closely related to the African green monkey\u2013derived lymphotropic polyomavirus.",
            "authors": [],
            "year": 2011,
            "venue": "J Virol",
            "volume": "85",
            "issn": "",
            "pages": "4586-90",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02602-10"
                ]
            }
        },
        "BIBREF19": {
            "title": "Identification of a novel polyomavirus from patients with acute respiratory tract infections.",
            "authors": [],
            "year": 2007,
            "venue": "PLoS Pathog",
            "volume": "3",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF20": {
            "title": "Seroepidemiology of human polyomaviruses.",
            "authors": [],
            "year": 2009,
            "venue": "PLoS Pathog",
            "volume": "5",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.ppat.1000363"
                ]
            }
        },
        "BIBREF21": {
            "title": "High prevalence of antibodies against polyomavirus WU, polyomavirus KI, and human bocavirus in German blood donors.",
            "authors": [],
            "year": 2010,
            "venue": "BMC Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/1471-2334-10-215"
                ]
            }
        },
        "BIBREF22": {
            "title": "Serologic evidence of frequent human infection with WU and KI polyomaviruses.",
            "authors": [],
            "year": 2009,
            "venue": "Emerg Infect Dis",
            "volume": "15",
            "issn": "",
            "pages": "1199-205",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1508.090270"
                ]
            }
        }
    }
}