{
    "paper_id": "PMC7095214",
    "metadata": {
        "title": "Public health: Life lessons",
        "authors": [
            {
                "first": "Laura",
                "middle": [
                    "Vargas"
                ],
                "last": "Parada",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Adela Guti\u00e9rrez, a door-to-door census-taker for the Mexican government, started feeling unwell with fever, headache and a sore throat, in early April 2009. Guti\u00e9rrez, a 39-year-old mother of three, lived in the southern state of Oaxaca. By the time she arrived at the state hospital, she was unable to breathe and was diagnosed with severe atypical pneumonia that might have been complicated by her diabetes. Within four days, Guti\u00e9rrez was dead.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Lab tests at the Oaxaca state hospital could not confirm the cause of death, so throat swabs were sent to the National Microbiology Laboratory in Winnipeg, Canada, and to the US Centers for Disease Control and Prevention (CDC) in Atlanta, Georgia. Ten days later, Cruz would become one of the first confirmed fatalities of a new influenza A (H1N1) virus, which originated in pigs. By then, nearly half of all the people who were in contact with Cruz in the hospital had developed respiratory symptoms and one pregnant nurse had fallen ill. Two months later, the World Health Organization (WHO) declared a global health emergency.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Early observations showed that this strain of flu disproportionately afflicted young people. A study of patients in Mexico found that the median age of fatalities was 39 years (ref. 1). Epidemiologists feared this outbreak could resemble the infamous 1918 influenza pandemic, also an H1N1 strain, which killed an estimated 50 million people (around 3% of the then global population) \u2014 half of whom were healthy adults between the ages of 20 years and 40 years old. The US President's Council of Advisors on Science and Technology calculated a possible scenario of 30,000\u201390,000 deaths in the United States alone. Fortunately, the transmission rate and virulence of the virus could not produce the pandemic that was initially dreaded.",
            "cite_spans": [
                {
                    "start": 182,
                    "end": 183,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nGaining more experience\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Nevertheless, the 2009 H1N1 pandemic proved to be a thorough test of the 2005 International Health Regulations (IHR), designed to be the world's first line of defence during public-health emergencies. (IHR is a legal agreement that dates back to 1969 and is binding by 194 party states, including all WHO members.) The 2009 outbreak was the first major assessment of the new IHR and raised critical questions about why it was so difficult to determine the severity of the threat, how effective the preparations were, and whether the crisis was properly managed.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In late 2009, director-general Margaret Chan of the WHO advised a review of the pandemic to learn from any lessons and evaluate whether the IHR fulfilled its purpose. The WHO assigned this task to a newly created IHR review committee, comprised of 25 international experts from diverse scientific fields with experience in public health. The committee presented its final report in May 2011, during the 64th World Health Assembly in Geneva, Switzerland.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The emergence of human infections of avian influenza A (H5N1) in 1997 and the corona-virus causing severe acute respiratory syndrome (SARS) in 2002 expedited international preparations for a pandemic. The IHR was updated in 2005 to respond to these new threats and entered into force worldwide in 2007, and are expected to be fully operational by 2012.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A major point of concern raised during the outbreaks of bird flu and SARS was the performance of disease warning systems in place. To assure a two-way channel of communication between the WHO and the party states, the IHR required each country to establish, by 2012, what it calls a focal point \u2014 offices that liaise with the WHO at all times and deploy resources for disease surveillance, early warning systems and the response to a disease outbreak.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The IHR review committee reported the IHR was still \u201cnot yet fully operational\u201d worldwide. \u201cThere are still many states that have not developed the plans and infrastructure specified in the IHR,\u201d says Jos\u00e9 Ignacio Santos, who heads the infectious disease unit of the school of medicine at the National Autonomous University of Mexico in Mexico City and a member of the IHR review committee. Although three quarters of the 194 party states had plans to co-ordinate national efforts in the case of a pandemic influenza outbreak, that statistic didn't tell the whole story. Only a dozen of the 128 countries that answered a WHO questionnaire had taken the steps required to put such a plan in practice, including: enacting legislation; allocating funding; putting enough people in place for detection and alert operations; and establishing procedures for surveillance, event detection, risk assessment and information provision2. In addition, many National IHR focal points are unable to communicate information about public health emergencies to the WHO in a timely fashion.",
            "cite_spans": [
                {
                    "start": 924,
                    "end": 925,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "According to the IHR review committee \u201cthe IHR played a central role in the global response to the pandemic.\u201d In Mexico, the IHR probably saved lives thanks to the progress made towards establishing plans and stocking antiviral medications. But what would happen if a new virus were to appear in countries that are not yet prepared, such as some in Africa and Asia?",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The WHO did not escape blame in the IHR review committee's assessment. For instance, the WHO was inconsistent in its definition of 'pandemic'. Some WHO documents described pandemics in terms of the number of deaths and illness caused by the disease, whereas the latest WHO definition is based on the degree of geographical spread. The WHO also failed to come up with consistent measures to assess the severity of an outbreak. The review committee stressed the need for the WHO to strengthen its capacity to mount a sustained response, and to improve its communication policies \u2014 in particular by issuing timely guidance in the organization's six official languages and holding routine press conferences. The review committee's report concluded with a stark warning: \u201cThe world is ill-prepared to respond to any similarly global, sustained and threatening public-health\nemergency.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 855,
                    "end": 868,
                    "mention": "public-health",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 869,
                    "end": 878,
                    "mention": "emergency",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "\nSharpening predictions\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Despite recent efforts to improve preparedness for a global influenza pandemic, there are still many things we don't understand about the virus. \u201cInfluenza is the name of one disease but is caused by a family of viruses,\u201d says Sylvie Briand, head of the global influenza programme at the WHO in Geneva, Switzerland. \u201cWe don't know very much about how these viruses circulate at the global level.\u201d It is still an enigma as to when, and to what extent, influenza viruses change their genetic make-up, and how they spread around the globe.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A reminder of how influenza can still surprise us came on 2 September, 2011, when a new swine influenza virus A (H3N2) was isolated from two children in the United States \u2014 one in Indiana and one in Pennsylvania3. Human H3 and H2 influenza strains were transmitted to pigs in the 1990s. Those viruses evolved in swine and now differ from the seasonal influenza that circulates in humans. People born before the 1990s \u201cstill have antibodies to the swine viruses because their immune system was exposed to the original viruses,\u201d says Nancy Cox, director of the influenza division at the CDC. \u201cBut young kids are very susceptible.\u201d The CDC has identified a vaccine candidate against this virus \u2014 just in case, says Cox.",
            "cite_spans": [
                {
                    "start": 211,
                    "end": 212,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In an ominous report published in 2011, researchers studied H2N2 viruses that have not circulated in human populations for several decades, but are still common infections of birds and swine4. The study found that people under 50 years of age have little or no immunity to this strain of the virus, reigniting fear that an H2N2 pandemic could, like H1N1, jump from animals to humans.",
            "cite_spans": [
                {
                    "start": 190,
                    "end": 191,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The 2009 swine flu was a reassortant virus that crossed from pigs to humans (a reassortant virus contains genetic material from two or more related viruses). Cox believes that this exchange between human and porcine viruses is more common than had been previously thought: \u201cThis is probably going on in a lot of other countries. In Switzerland they have detected swine infections in humans,\u201d Cox says. In a retrospective study published in 2007, researchers found evidence of 50 apparent cases of influenza that were transmitted from pigs to humans. Most of the cases were reported in the United States \u2014 probably, Cox says, because the US has a very good surveillance system, but there were also six cases in the Czech Republic, four in the Netherlands, three in Russia, three in Switzerland, and one case each in Canada and Hong Kong5.",
            "cite_spans": [
                {
                    "start": 835,
                    "end": 836,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Prior to 2009, three global networks were in place for the early detection and/or surveillance of influenza: the 2005 IHR, the WHO's Global Influenza Surveillance Network (GISN) and systematic event detection (that monitors information published not only formally in journals but also in newspapers and online forums). In March 2009, just weeks before the swine flu outbreak, more than half of WHO member states (104 of 193 countries) had no or very limited seasonal influenza surveillance capacity, according to a 2011 article co-authored by Briand6. (Both the United States and Mexico had good surveillance systems, which is why the new virus was identified in those countries early on.) The same report, echoing some of the themes articulated by the IHR review committee report, identified other deficiencies that were revealed during the H1N1 pandemic. Among them were a lack of standards for reporting illness, risk factors and mortality. As a result, different countries used different criteria to define influenza-like illness and collected information on different risk factors. This inconsistency makes it difficult to do the sort of comparisons that would help public health officials assess how well they are doing relative to other places in the world. After the swine flu pandemic, the WHO worked to better define standards and develop a web-based platform called FLuID (Flu Informed Decisions) for direct reporting of epidemiological data, such as intensity of influenza transmission, number of hospitalizations, and risk factors for severe disease. FLuID is similar to the WHO's FluNet surveillance system, which collects information concerning the viral type and subtype.",
            "cite_spans": [
                {
                    "start": 549,
                    "end": 550,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Scientists are confident that a good surveillance system, along with the GISN monitoring antibodies against the virus, will help detect new viruses before they become a threat to global health. And if this is the case, says Briand, \u201cwe have a protocol for rapid containment [if] we detect an outbreak early enough to contain it at its source.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nNew directions\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "One of the main challenges during an outbreak is to assess the severity of the pandemic disease. Even with today's technology, there are a few critical days of uncertainty after the outbreak is identified, but before enough epidemiological and clinical data are gathered to allow public health officials to plot the most effective response.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\u201cIn the 2009 pandemic, some decisions had to be taken before we had a clear picture,\u201d Briand says. Drawing on that experience, she says, health officials are trying to improve their ability to assess an outbreak at its earliest stages. \u201cIf we isolate the virus,\u201d she explains, \u201cwe can determine how different it is from the current circulating viruses and if it is susceptible to available antivirals.\u201d Testing blood samples for antibodies to influenza viruses will allow researchers to determine how many people have been exposed to the virus and how prevalence fluctuates over time.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The absence of important data can be overcome. During the 2009 pandemic, determining the fatality rate was sometimes difficult because information about the number of persons infected and the number of deaths was incomplete. Exacerbating the problem, the WHO, as well as national authorities, overwhelmed public health officers with requests for specific data, such as the number of cases. In countries with limited epidemiological and laboratory infrastructure, the need to deliver so many statistics led to an unfortunate diversion of personnel away from patient care. And in countries during the early stages of the pandemic, this information proved to be less useful for assessing severity than rates of hospitalization and complications, and actual number of deaths.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "As it turned out, however, other parameters proved to be good proxies to gauge the severity of disease. Measures included the proportion of cases that required hospitalization for treatment or that required intensive care and mechanical ventilation. Other useful data were the proportion of previously healthy individuals without underlying risk factors that developed severe disease. And as always, the severity of a pandemic will depend on conditions inside a country, such as access to health services, general health of the population, and various social and behavioral factors.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "As the influenza virus continues to circulate in human and animal populations across the globe, it is crucial to identify changes in the viral genome \u2014 especially those mutations that confer resistance to antiviral drugs. In 2009, Briand says, \u201cwe were fortunate because the virus was sensitive to neuraminidase inhibitors that we had in our stockpile.\u201d Neuraminidase is a viral surface protein that allows the virus to spread from infected cells to other cells (see 'Lines of defence', page S9). But any strategic stockpiling of anti-flu drugs needs to account for the limited shelf lives of the medications. At the WHO, public health officers are looking for technical solutions to prolong the usefulness of the antivirals to at least two or three more years beyond the roughly 5 year shelf lives of today's medications, as well as \u201cencouraging the development of new antivirals\u201d, says IHR review committee member Santos.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The best weapon against flu is still the vaccine. Once the H1N1 2009 virus was identified, the WHO needed just one month to select the pandemic virus and develop the seed strains for vaccine development. But that's only the first step. \u201cIt takes at least six months to produce significant quantities of a vaccine,\u201d Klaus St\u00f6hr of Novartis claimed in 2010 (ref. 7). St\u00f6hr is vice president of influenza strategy at Novartis Vaccines and Diagnostics in Cambridge, Massachusetts. One vexing question is how much vaccine to produce. By the time vaccines against the 2009 H1N1 virus were made available, the production was enough to treat only 10% of the population worldwide. Santos also highlights pharmaceutical deficits. \u201cThe technology to produce influenza vaccine has not changed much in the past 60 years,\u201d he says.",
            "cite_spans": [
                {
                    "start": 361,
                    "end": 362,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Even if vaccines are produced in abundance, there's no guarantee that the medication will get to the people who need it. \u201cVaccine distribution worldwide is the other challenge,\u201d says Santos. Access to vaccines, he says, can be improved by legal agreements between manufacturers and countries. Other paths to more equitable access to vaccines include differential pricing so that poorer countries can get the vaccines cheaper \u2014 already in practice, but says Santos, needs to be made more universal \u2014 as well as vaccine donations by manufacturers to the WHO. At the national level, there is the need to assure the maintenance of the cold chain \u2014 the continuous sequence of properly refrigerated vessels to transport the vaccines from factory to the patient \u2014 and to plan for local vaccine distribution and administration. \u201cOne way to facilitate both vaccine and antiviral distribution,\u201d Santos says, \u201ccould be to regionalize the effort, giving each WHO regional office the responsibility to secure the resources needed for the area.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "No one can predict with precision when and where the next global outbreak will arise. But, in view of what has been learned from recent influenza pandemics, it is possible to define the most likely scenarios and to prepare for them. \u201cIt is like your personal life,\u201d says Briand. \u201cYou cannot predict anything but there are scenarios that are more likely than others.\u201d The lessons learned from the latest pandemic \u201cwill certainly drive our preparedness to different directions now.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "One key lesson is the need to be flexible in our response to unexpected conditions and to be able to respond in times of uncertainty. We must, in other words, be like the flu virus itself.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: \nFROM LEFT: NATIONAL MUSEUM OF HEALTH AND MEDICINE/SCIENCE PHOTO LIBRARY; NATIONAL ARCHIVES/SCIENCE PHOTO LIBRARY; TIME, BETTMANN/CORBIS & LIFE PICTURES/GETTY IMAGES.\n",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: \nFROM LEFT: BETTMANN/CORBIS, BETTMANN/CORBIS, CDC/SCIENCE PHOTO LIBRARY, KPA/ZUMA/Rex Features.\n",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Dom\u00edguez-Cherit",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "JAMA",
            "volume": "302",
            "issn": "",
            "pages": "1880-1887",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2009.1536"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Nalluswami",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "MMWR",
            "volume": "60",
            "issn": "",
            "pages": "1213-1215",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [
                {
                    "first": "GJ",
                    "middle": [],
                    "last": "Nabel",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Nature",
            "volume": "471",
            "issn": "",
            "pages": "157-158",
            "other_ids": {
                "DOI": [
                    "10.1038/471157a"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [
                {
                    "first": "KP",
                    "middle": [],
                    "last": "Myers",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Clin. Infect. Dis.",
            "volume": "44",
            "issn": "",
            "pages": "1084-1088",
            "other_ids": {
                "DOI": [
                    "10.1086/512813"
                ]
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Briand",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Public Health",
            "volume": "125",
            "issn": "",
            "pages": "247-256",
            "other_ids": {
                "DOI": [
                    "10.1016/j.puhe.2010.12.007"
                ]
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Stohr",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Nature",
            "volume": "465",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1038/465161a"
                ]
            }
        }
    }
}