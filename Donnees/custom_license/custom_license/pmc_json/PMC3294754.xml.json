{
    "paper_id": "PMC3294754",
    "metadata": {
        "title": "Human Bocavirus Infection among Children, Jordan",
        "authors": [
            {
                "first": "Nasser",
                "middle": [
                    "M."
                ],
                "last": "Kaplan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Winifred",
                "middle": [],
                "last": "Dove",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ahmad",
                "middle": [
                    "F."
                ],
                "last": "Abu-Zeid",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hiyam",
                "middle": [
                    "E."
                ],
                "last": "Shamoon",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sawsan",
                "middle": [
                    "A."
                ],
                "last": "Abd-Eldayem",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "C.",
                "middle": [
                    "Anthony"
                ],
                "last": "Hart",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "From December 2003 to May 2004, all children <5 years of age admitted to the pediatric wards of King Hussein Medical Centre (KHMC) and Queen Alia Hospital (QAH) in Amman, Jordan, were enrolled into the study after informed consent was obtained from parents or guardians. The study, which determined the etiology, inflammatory responses, and clinical effects of ARI was approved by the research ethical approval committee of the Royal Medical Services, Amman, Jordan. KHMC and QAH provide all hospital pediatric care for Amman and its surroundings.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Diagnosis of ARI and assessment of its severity was made by using World Health Organization (WHO) standard protocol for ARI based on the presence of cough, tachypnea, chest indrawing, and wheezing for <7 days (9). Severe disease was defined in children with a respiratory rate >60/minute and chest indrawing. Oxygen saturation (pO2) was measured by using pulse oximetry (Nellor, Puritan Bennet, UK), and a pO2 <85% was used as the cutoff for giving supplementary oxygen. Nasopharyngeal aspirates (NPAs) were collected by instilling 1 mL sterile phosphate-buffered saline through a nasopharyngeal mucous extractor. The aspirate was frozen at \u201380\u00b0C and transported frozen to Liverpool for analysis.",
            "cite_spans": [
                {
                    "start": 210,
                    "end": 211,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "DNA and RNA were extracted from aspirates by using commercial kits (Qiagen, Basingstoke, UK). PCR or reverse transcription PCR (RT-PCR) detection of influenza A and B viruses, parainfluenza virus 1\u20134 (10), human metapneumovirus, RSV (11), adenovirus, Chlamydia spp., and Mycoplasma pneumoniae (12) was performed according to previously published protocols. HBoV primers 188F (5\u00b4-GAGCTCTGTAAGTACTATTAC-3\u00b4) and 542 R (5\u00b4-CTCTGTGTTGACTGAATACAG-3\u00b4) that target the NP-1 protein gene and produce a 354-bp amplicon were used as described and modified by Allander et al. (8). Other potential respiratory pathogens such as rhinoviruses and coronaviruses were not investigated because they are associated primarily with upper respiratory infections.",
            "cite_spans": [
                {
                    "start": 201,
                    "end": 203,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 234,
                    "end": 236,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 294,
                    "end": 296,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 565,
                    "end": 566,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "A total of 326 children were enrolled in the study, but sufficient nucleic acid was extractable from 312 NPAs for detection of each potential respiratory pathogen. For the remainder, the volume of NPA was too small for extraction of both DNA and RNA Of these, 57 (18.3%) children were infected with HBoV (Table). The median age of HBoV-infected patients was 8 months and 29 (51%) were male, compared with a median age of 6 months and 156 (61%) male patients in the HBoV-negative patients (p>0.2). HBoV was detected in 30 (21.7%) of 138 children with severe ARI and in 27 (15.5%) of 174 children with mild-to-moderate ARI (p>0.2). However, only HBoV was detected in 13 (48%) of the 27 patients with mild-to-moderate ARI and with adenovirus (10 patients), RSV (2 patients) Chlamydia spp. (1 patient), and RSV and adenovirus (1 patient) in the 14 remaining patients with mild-to-moderate disease. In patients with severe ARI in whom HBoV was detected, it was the only pathogen in 3 (10%) patients. In the remaining 27 cases, it was found as a mixed infection with RSV (9 patients), RSV and adenovirus (8 patients), RSV and Chlamydia spp. (2 patients), RSV and influenza A virus (1 patient), HMPV and Chlamydia spp. (1 patient) and adenovirus (6 patients). The median age was 3.5 months for those infected only with HBoV and 10 months (p = 0.012) for those co-infected with HBoV and other potential pathogens.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 305,
                    "end": 310,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Direct sequencing (Lark Technologies, Essex, UK) was undertaken for 14 (25%) of the amplicons. Four amplicons had the same sequence as the original Swedish strain. Five variants were detected. One cluster (DNA Data Bank of Japan accession no. AB243566 available from http://www.ddbj.nig.ac.jp) contained 5 strains with mutations at codons 21 (R\u2192K) and 59 (S\u2192N). Another cluster (AB 243570) contained 2 strains with 1 mutation at codon 79 (S\u2192N). Three other variants were detected with changes at codons 26 (R\u2192K), 29 (Q\u2192R), and 59 (S\u2192N) (AB243568), codons 21 (R\u2192K) and 79 (S\u2192N) (AB243569), and codon 42 (R\u2192Q) (AB243567), respectively. No connections were found between patients with different variants except for AB243570, in which 2 strains were isolated from 2 children at the same orphanage in Amman who came to the hospital on the same day. One had mild-to moderate-disease, and the other had severe disease.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "We detected HBoV in 57 (18.3%) of 312 children with ARI severe enough to require hospital admission. HBoV was detected in 30 (21.7%) of those admitted who were classified according to WHO criteria as having severe ARI. Other reported prevalences are 24 (3%) of 806 pediatric samples in Sweden (8), 18 (5.6%) of 324 children <3 years of age in Australia (6), and 18 (5.7%) of 318 children <3 years of age in Japan (13). These data support an association between the virus and ARI.",
            "cite_spans": [
                {
                    "start": 294,
                    "end": 295,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 354,
                    "end": 355,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 414,
                    "end": 416,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "As in the Australian study (6), mixed infections were common. In the Australian study, HBoV was detected with other potential respiratory pathogens in \u224856% of patients. In our study, the prevalence (72%) of mixed infection was even higher, occurring most often as a co-infection with RSV. HBoV was found as sole pathogen in 2% of cases of severe ARI and in 7.5% of mild-to-moderate ARI.",
            "cite_spans": [
                {
                    "start": 28,
                    "end": 29,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "This study was conducted during the peak period of ARI in Jordan, and the prevalence of detection of HBoV ranged from 12.9% in March to 37% in April. However, larger cross-sectional studies and longitudinal studies of HBoV-infected children are needed to determine whether HBoV causes ARI, its effect on children, and its seasonality. In addition, HBoV, similar to some adenoviruses (14) and other human parvoviruses, may show persistent shedding after an initial acute infection.",
            "cite_spans": [
                {
                    "start": 384,
                    "end": 386,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Finally, we have also obtained evidence for variations in the HBoV NP-1 gene. In addition to the original Swedish strain, we found 5 variants with point mutations in the gene causing amino acid substitution in the deduced protein. What role this might play in HBoV pathogenesis and whether other genes encoding nonstructural protein 1 (NS-1) and virion proteins 1/2 (VP1/2) show similar variability are unclear. However, 2 CPV-1 strains showed 96.5%, 92.5%, and 97.5% homology in their NS-1-, NP-1-, and VP1/2-deduced proteins (15).",
            "cite_spans": [
                {
                    "start": 528,
                    "end": 530,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Mortality by cause for eight regions of the world: global burden of disease.",
            "authors": [],
            "year": 1997,
            "venue": "Lancet",
            "volume": "349",
            "issn": "",
            "pages": "1269-76",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(96)07493-4"
                ]
            }
        },
        "BIBREF1": {
            "title": "Rapid and sensitive method using multiplex real-time PCR for diagnosis of infections by influenza A and influenza B viruses, respiratory syncytial virus, and parainfluenza viruses 1, 2, 3 and 4.",
            "authors": [],
            "year": 2004,
            "venue": "J Clin Microbiol",
            "volume": "42",
            "issn": "",
            "pages": "1564-9",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.42.4.1564-1569.2004"
                ]
            }
        },
        "BIBREF2": {
            "title": "Human metapneumovirus in severe respiratory syncytial virus bronchiolitis.",
            "authors": [],
            "year": 2003,
            "venue": "Emerg Infect Dis",
            "volume": "9",
            "issn": "",
            "pages": "372-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Detection of microorganisms in the tracheal aspirates of preterm infants by polymerase chain reaction: association of adenovirus with bronchopulmonary dysplasia.",
            "authors": [],
            "year": 2000,
            "venue": "Pediatr Res",
            "volume": "47",
            "issn": "",
            "pages": "225-32",
            "other_ids": {
                "DOI": [
                    "10.1203/00006450-200002000-00013"
                ]
            }
        },
        "BIBREF4": {
            "title": "Detection of human bocavirus in Japanese children with lower respiratory tract infections.",
            "authors": [],
            "year": 2006,
            "venue": "J Clin Microbiol",
            "volume": "44",
            "issn": "",
            "pages": "1132-4",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.44.3.1132-1134.2006"
                ]
            }
        },
        "BIBREF5": {
            "title": "Evidence for persistence of adenovirus in the tear film a decade following conjunctivitis.",
            "authors": [],
            "year": 2005,
            "venue": "J Med Virol",
            "volume": "77",
            "issn": "",
            "pages": "227-31",
            "other_ids": {
                "DOI": [
                    "10.1002/jmv.20440"
                ]
            }
        },
        "BIBREF6": {
            "title": "Sequence analysis of an Asian isolate of minute virus of canines (canine parvovirus type 1).",
            "authors": [],
            "year": 2004,
            "venue": "Virus Genes",
            "volume": "29",
            "issn": "",
            "pages": "291-6",
            "other_ids": {
                "DOI": [
                    "10.1007/s11262-004-7430-3"
                ]
            }
        },
        "BIBREF7": {
            "title": "Black RE and the WHO Child Health Epidemiology Reference Group. WHO estimation of the causes of death in children.",
            "authors": [],
            "year": 2005,
            "venue": "Lancet",
            "volume": "365",
            "issn": "",
            "pages": "1147-52",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(05)71877-8"
                ]
            }
        },
        "BIBREF8": {
            "title": "Bronchiolitis-associated mortality and estimates of respiratory syncytial virus-associated deaths among US children, 1979\u20131997.",
            "authors": [],
            "year": 2001,
            "venue": "J Infect Dis",
            "volume": "183",
            "issn": "",
            "pages": "16-22",
            "other_ids": {
                "DOI": [
                    "10.1086/317655"
                ]
            }
        },
        "BIBREF9": {
            "title": "A newly discovered human pneumonovirus isolated from young children with respiratory tract disease.",
            "authors": [],
            "year": 2001,
            "venue": "Nat Med",
            "volume": "7",
            "issn": "",
            "pages": "719-24",
            "other_ids": {
                "DOI": [
                    "10.1038/89098"
                ]
            }
        },
        "BIBREF10": {
            "title": "Clinical progression and viral load in a community outbreak of coronavirus-associated SARS pneumonia: a prospective study.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1767-72",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13412-5"
                ]
            }
        },
        "BIBREF11": {
            "title": "Evidence of human coronavirus HKU1 and human bocavirus in Australian children.",
            "authors": [],
            "year": 2005,
            "venue": "J Clin Virol",
            "volume": "35",
            "issn": "",
            "pages": "99-102",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcv.2005.09.008"
                ]
            }
        },
        "BIBREF12": {
            "title": "Identification of a new human coronavirus.",
            "authors": [],
            "year": 2004,
            "venue": "Nat Med",
            "volume": "10",
            "issn": "",
            "pages": "368-71",
            "other_ids": {
                "DOI": [
                    "10.1038/nm1024"
                ]
            }
        },
        "BIBREF13": {
            "title": "Cloning of a human parvovirus by screening of respiratory tract samples.",
            "authors": [],
            "year": 2005,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "102",
            "issn": "",
            "pages": "12891-6",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.0504666102"
                ]
            }
        },
        "BIBREF14": {
            "title": "Standard case management of pneumonia in children in developing countries: the cornerstone of the acute respiratory infection programme.",
            "authors": [],
            "year": 2003,
            "venue": "Bull World Health Organ",
            "volume": "81",
            "issn": "",
            "pages": "298-300",
            "other_ids": {
                "DOI": []
            }
        }
    }
}