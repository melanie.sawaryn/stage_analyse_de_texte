{
    "paper_id": "PMC3320400",
    "metadata": {
        "title": "SARS Transmission and Commercial Aircraft",
        "authors": [
            {
                "first": "J.",
                "middle": [
                    "Gabrielle"
                ],
                "last": "Breugelmans",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Phillip",
                "middle": [],
                "last": "Zucs",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Klaudia",
                "middle": [],
                "last": "Porten",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Susanne",
                "middle": [],
                "last": "Broll",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Matthias",
                "middle": [],
                "last": "Niedrig",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Andrea",
                "middle": [],
                "last": "Ammon",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "G\u00e9rard",
                "middle": [],
                "last": "Krause",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "To the Editor: Severe acute respiratory syndrome (SARS) is an emerging transmissible disease first reported in Asia in February 2003. The disease is characterized by acute onset of fever with nonproductive cough, myalgia, shortness of breath, or difficulty breathing (1). Approximately 14% of case-patients require mechanical ventilation (1,2). The syndrome is caused by the previously unrecognized SARS-associated coronavirus (SARS-CoV) (3). The primary mode of SARS transmission is through close person-to-person contact. In March 2003, the World Health Organization (WHO) issued two travel advisories to SARS-affected countries. Despite these advisories, probable case-patients traveled by air internationally, thereby spreading the disease globally. The extent of risk posed by probable cases for in-flight transmission of SARS is unclear.",
            "cite_spans": [
                {
                    "start": 268,
                    "end": 269,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 339,
                    "end": 340,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 341,
                    "end": 342,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 439,
                    "end": 440,
                    "mention": "3",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A study was conducted by the Robert Koch Institute in Berlin, Germany, to document SARS transmission during international flights. On April 11, 2003, the Institute was notified that a probable SARS-infected person had flown from Hong Kong to Frankfurt, Germany, on March 30 to 31, 2003, and then traveled extensively in Europe after onset of symptoms. In 5 days, the traveler, a 48-year-old Hong Kong businessman, had flown on seven flights throughout Europe (Table). On March 31, symptoms of SARS, including fever and general malaise developed; whether he had a cough at this time is unclear. He was admitted to a hospital in Hong Kong on April 8, and mechanical ventilation was initiated. He was reported to WHO as a suspected SARS patient on April 9 and diagnosed with SARS on April 10. Polymerase chain reaction analysis conducted on the patient's nasopharyngeal aspirate showed positive results for SARS-CoV on April 14.",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 460,
                    "end": 465,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Passenger manifests from the seven flights on which the patient had flown were requested by the local health departments and the Institute. In a previous study, Kenyon et al. indicated that airline passengers seated within two rows of an infectious tuberculosis patient were at greatest risk for infection (4). To determine an association between seating proximity to the SARS patient and transmission of SARS, a study that included all airline passengers seated within four rows (i.e., front, back, and same row) of the index patient (4) was conducted. Passengers >18 years of age who lived in Germany were contacted by the Institute and asked to participate in the study; all participants gave informed consent for inclusion in the study. Passengers in other countries were not included in the study because contact information was not available. Passengers <18 years of age were not included in the study; ethical approval from an Institutional Review Board, which would have delayed the study, would have been necessary. Contact information for study participants was forwarded to local health departments so that public health officials could provide follow-up care. Study participants were interviewed approximately 3 months after their flights because contact information was not available earlier. A standardized questionnaire was developed to collect information on demographics, flight details, countries visited before the flights, use of mask, and symptoms. Furthermore, 5\u201310 mL of whole blood was drawn and tested for SARS-CoV antibodies by using immunofluorescence assay.",
            "cite_spans": [
                {
                    "start": 307,
                    "end": 308,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 536,
                    "end": 537,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A total of 250 passengers were identified and selected for the study. Contact information was available for 109 passengers; 69 of the 109 were living in Germany. Sixty-two of those 69 passengers were contacted, and 41 passengers agreed to participate in the study. Thirty-six participants completed questionnaires and had blood samples taken. The male-to-female ratio was 3:5, and the median age was 41 years (25\u201359 years). Contact information was not available for five passengers, which made their inclusion in the study impossible. All serologic samples (N = 36) tested were negative for SARS-CoV immunoglobulin G antibodies, and none of the 36 passengers reported symptoms characteristic of SARS. Ten passengers complained of cough, headache, and muscle aches. One passenger reported a cough, muscle aches, and fever, but symptoms started 10 days after the flight. An analysis of the seating arrangement showed that the study participants were randomly distributed around the index patient.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "No SARS transmission was shown among contacted passengers seated in close proximity to the index patient; these results suggest that in-flight transmission of SARS is not common. These results are consistent with other studies that assessed the risk for in-flight transmission of SARS (5,6). The results also suggest that SARS-CoV is not efficiently transmitted, as reflected in its basic reproduction number R0 (range 2\u20134) (7). The SARS-infected patient on the indicated flights was in his first week of illness; infectivity is greatest in the second week (8). Therefore, the likelihood of SARS transmission on the indicated flights was not high. These results are further supported by the fact that all contacts were asymptomatic 13 days after their last contact with the SARS patient. No information was available on healthcare contacts. Although we did not observe any SARS transmission, we cannot rule out the possibility that it may have occurred. We had no contact information on 56% of the passengers on the indicated flights and, therefore, had to exclude them from the investigation. Obtaining complete contact information from the remaining passengers was difficult, which severely impeded the investigation. Similarly, we were unable to contact crew members and had to exclude them. Recent studies have documented SARS transmission to passengers seated more than four rows away from an index patient (5,9); thus, studying the passenger proximity to the patient may not be sufficient. Because of these limitations, our final sample size was small and probably biased. Since we did not observe any evidence to indicate in-flight transmission of SARS, we were unable to assess the importance of seat assignment proximity as a risk factor.",
            "cite_spans": [
                {
                    "start": 286,
                    "end": 287,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 288,
                    "end": 289,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 425,
                    "end": 426,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 558,
                    "end": 559,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1413,
                    "end": 1414,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1415,
                    "end": 1416,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The study shows that the roles of public health authorities and the aviation industry should be to \"harmonise the protection of public health without the need to avoid unnecessary disruption of trade and travel\" in public health emergencies such as global SARS transmission (10). We recommend strengthening the collaboration between national health authorities and the airline industry. Furthermore, the International Air Transport Association should establish procedures to ensure that complete contact information is available for all passengers and that rapid notification can be accomplished in case of potential exposure to infectious diseases.",
            "cite_spans": [
                {
                    "start": 275,
                    "end": 277,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Clinical features and short-term outcomes of 144 patients with SARS in the greater Toronto area.",
            "authors": [],
            "year": 2003,
            "venue": "JAMA",
            "volume": "289",
            "issn": "",
            "pages": "2801-9",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.289.21.JOC30885"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2002,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Severe acute respiratory syndrome: clinical outcome and prognostic correlates.",
            "authors": [],
            "year": 2003,
            "venue": "Emerg Infect Dis",
            "volume": "9",
            "issn": "",
            "pages": "1064-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Identification of a novel coronavirus in patients with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1967-76",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030747"
                ]
            }
        },
        "BIBREF4": {
            "title": "Transmission of multidrug-resistant Mycobacterium tuberculosis during a long airplane flight.",
            "authors": [],
            "year": 1996,
            "venue": "N Engl J Med",
            "volume": "334",
            "issn": "",
            "pages": "933-8",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJM199604113341501"
                ]
            }
        },
        "BIBREF5": {
            "title": "Transmission of the severe acute respiratory syndrome on aircraft.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": "2416-22",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa031349"
                ]
            }
        },
        "BIBREF6": {
            "title": "Low risk of transmission of severe acute respiratory syndrome on airplanes: the Singapore experience.",
            "authors": [],
            "year": 2003,
            "venue": "Trop Med Int Health",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1046/j.1360-2276.2003.01133.x"
                ]
            }
        },
        "BIBREF7": {
            "title": "Directly transmitted infectious diseases: control by vaccination.",
            "authors": [],
            "year": 1982,
            "venue": "Science",
            "volume": "215",
            "issn": "",
            "pages": "1053-60",
            "other_ids": {
                "DOI": [
                    "10.1126/science.7063839"
                ]
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Introduction of SARS in France, March\u2013April, 2003.",
            "authors": [],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "195-200",
            "other_ids": {
                "DOI": []
            }
        }
    }
}