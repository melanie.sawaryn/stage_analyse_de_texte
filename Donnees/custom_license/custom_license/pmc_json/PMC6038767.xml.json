{
    "paper_id": "PMC6038767",
    "metadata": {
        "title": "Detection of Respiratory Viruses in Deceased Persons, Spain, 2017",
        "authors": [
            {
                "first": "Ana",
                "middle": [],
                "last": "Navascu\u00e9s",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Itziar",
                "middle": [],
                "last": "Casado",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Alejandra",
                "middle": [],
                "last": "P\u00e9rez-Garc\u00eda",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Aitziber",
                "middle": [],
                "last": "Aguinaga",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Iv\u00e1n",
                "middle": [],
                "last": "Mart\u00ednez-Baz",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yugo",
                "middle": [],
                "last": "Florist\u00e1n",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Carmen",
                "middle": [],
                "last": "Ezpeleta",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jes\u00fas",
                "middle": [],
                "last": "Castilla",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "We performed this study in Navarre, Spain, during January 23\u2013February 19, 2017, during the seasonal influenza epidemic (9). Recruitment was conducted in 2 morgues by trained professionals. Persons >65 years of age who had died of natural causes regardless of the reported cause of death were included, after we obtained written informed consent from their closest relatives. We obtained nasopharyngeal swab specimens before the bodies were prepared for burial; we tested the swabs for influenza and RSV by reverse transcription PCR (RT-PCR). We tested negative samples for other respiratory viruses using multiple PCR (Allplex Respiratory Panel; Seegene, Seoul, South Korea).",
            "cite_spans": [
                {
                    "start": 120,
                    "end": 121,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "We obtained demographic information and previous diagnoses from the epidemiologic surveillance system. We retrieved hospitalization and laboratory confirmation for respiratory viruses within the 30 days before the death from electronic healthcare databases. We obtained the underlying causes of death from the regional mortality register and grouped them into 5 categories according to the International Classification of Diseases, 10th Revision: neoplasms (codes C00\u2013D49), nervous system diseases (codes G00\u2013G99), circulatory system diseases (codes I00\u2013I99), respiratory system diseases (codes J00\u2013J99), and all other causes. We used the 2 tailed Fisher exact test to compare proportions.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The study period included the last 4 weeks of the 2016\u201317 influenza epidemic in Navarre, starting 2 weeks after the peak. This period was characterized by a high but descending number of hospitalizations of patients with laboratory-confirmed influenza and 27% excess in all-cause mortality (Technical Appendix Figure).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "During the study period, 460 deceased persons >65 years of age were registered, 106 were attended in the participating morgues, and 57 (54%) were enrolled in the study. Nonparticipation resulted mainly from logistic problems and lack of signed consent.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Of the 57 participants in the study, 29 (51%) were women, 23 (40%) were <85 years of age, 50 (88%) had major chronic conditions, 5 (9%) had been resident in nursing homes, and only 12 (21%) had been hospitalized before death. Nonparticipants did not differ in these characteristics (Technical Appendix Table).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Respiratory viruses were detected in the postmortem study in 27 (47%) participants, but only 4 (7%) had received this diagnosis before death (Figure 1). Ten (18%) participants tested positive for influenza virus A(H3N2), 7 (12%) for RSV (4 subgroup A and 3 subgroup B), 7 (12%) for coronavirus (6 type 229E and 1 type OC43), and 4 (7%) for rhinovirus. Although postmortem detection of any respiratory virus was more likely among previously hospitalized persons, it was also frequent among those not hospitalized (75% vs. 40%; p = 0.050) (Table).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 142,
                    "end": 150,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 538,
                    "end": 543,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The postmortem detection of influenza or other respiratory viruses was not statistically associated with the analyzed covariates, with 2 exceptions: respiratory viruses other than influenza were detected more frequently in deceased men (46%; 13/28) than in women (14%; 4/29; p = 0.010), and respiratory viruses were more frequently detected among deceased persons who were reported with respiratory system diseases as the underlying cause of death than in those reported with other causes (86% vs. 42%; p = 0.045) (Table). Nevertheless, the percentage of deceased persons whose specimens tested positive for any respiratory virus was notable in all groups of nonrespiratory causes of death (range 38%\u201350%) (Table; Figure 2). Only 1 person (10%) whose specimen was detected as having influenza virus in the postmortem test had influenza registered as the cause of death; 5 (50%) were registered as having a cardiorespiratory cause of death.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 714,
                    "end": 722,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 515,
                    "end": 520,
                    "mention": "Table",
                    "ref_id": null
                },
                {
                    "start": 707,
                    "end": 712,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "This study demonstrates the feasibility of the detection of respiratory viruses in samples from deceased persons. Respiratory viruses were found in nearly half of the persons who died of natural causes in an influenza epidemic period, and 18% were confirmed for influenza virus A(H3N2), which was the same influenza virus subtype that dominated in patients during the 2016\u201317 season (10,11). The 2016\u201317 influenza season was characterized in Europe by an increase in deaths (2). Other respiratory viruses were detected during the influenza circulation period and may have contributed substantially to hospitalizations and deaths (1,5,12). RT-PCR seems to have high sensitivity for the detection of respiratory viruses in deceased persons, as previously shown in studies based on coronial autopsies (8,13).",
            "cite_spans": [
                {
                    "start": 384,
                    "end": 386,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 387,
                    "end": 389,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 475,
                    "end": 476,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 630,
                    "end": 631,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 632,
                    "end": 633,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 634,
                    "end": 636,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 799,
                    "end": 800,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 801,
                    "end": 803,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Respiratory virus infections are characterized by sudden onset; death may occur suddenly, even before the symptoms are evident. Respiratory viruses can trigger secondary bacterial infections or exacerbate existing chronic conditions, and these concurrent conditions usually prevail as the underlying cause of death. Half of the deaths with influenza virus detection in the postmortem test were registered as having a noncardiorespiratory cause of death, which is consistent with a previous hospital study (6). This finding demonstrates the difficulty in estimating the deaths related to respiratory viruses by using the mortality registers.",
            "cite_spans": [
                {
                    "start": 506,
                    "end": 507,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "These results raise relevant implications. Only a small proportion of deceased persons whose respiratory virus was detected in the postmortem test had been hospitalized and received this diagnosis before dying; therefore, the contribution of viral infections to death may be underestimated. Deaths related to respiratory viruses could be distributed among all causes of death. Although the burden of death has been estimated by indirect approaches (1\u20135), this study offers a complementary novel approach to assess the impact in terms of the proportion of all-cause deaths with respiratory virus detection (14).",
            "cite_spans": [
                {
                    "start": 449,
                    "end": 450,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 451,
                    "end": 452,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 606,
                    "end": 608,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "The surveillance of influenza based on laboratory-confirmed cases is implemented in primary healthcare and in hospitalized patients (9,11). Our results open the possibility and show the potential interest of adding a sentinel virological surveillance based on persons who die during the influenza season.",
            "cite_spans": [
                {
                    "start": 133,
                    "end": 134,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 135,
                    "end": 137,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Caution should be paid in the interpretation of these results, however. Virus detection does not necessarily imply a causal relationship between virus infection and death because respiratory viral shedding has been described in asymptomatic persons (15). Our study included 12% of deaths in the region during the last 4 weeks of the influenza epidemic, but the peak was not included; therefore, the representativeness is limited. Similar characteristics of participants and nonparticipants rule out selection bias. We cannot rule out false-negative results, however, because samples were obtained postmortem and the time from symptom onset to swabbing was unknown. Only negative samples for influenza and RSV were tested for other respiratory viruses, which might underestimate the frequency of the other respiratory codetections.",
            "cite_spans": [
                {
                    "start": 250,
                    "end": 252,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "In summary, we demonstrate the feasibility of detecting respiratory viruses in recently deceased persons. We frequently detected respiratory viruses postmortem in winter deaths, although most of these infections were not clinically diagnosed. Respiratory virus surveillance systems could be complemented by testing persons who die during the influenza circulation period for respiratory virus infections.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Clinical and postmortem detections of respiratory viruses among 57 deceased persons >65 years of age, Spain, 2017. As indicated, 47% of deceased patients tested positive for respiratory virus infection postmortem, but only 7% had received the same diagnosis before death. RSV, respiratory syncytial virus.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Postmortem detection of influenza and other respiratory virus infection by underlying cause of death among 57 deceased persons >65 years of age, Spain, 2017.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Mortality associated with influenza and respiratory syncytial virus in the United States.",
            "authors": [],
            "year": 2003,
            "venue": "JAMA",
            "volume": "289",
            "issn": "",
            "pages": "179-86",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.289.2.179"
                ]
            }
        },
        "BIBREF1": {
            "title": "Early 2016/17 vaccine effectiveness estimates against influenza A(H3N2): I-MOVE multicentre case control studies at primary care and hospital levels in Europe.",
            "authors": [],
            "year": 2017,
            "venue": "Euro Surveill",
            "volume": "22",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.2807/1560-7917.ES.2017.22.7.30464"
                ]
            }
        },
        "BIBREF2": {
            "title": "Combined effectiveness of prior and current season influenza vaccination in northern Spain: 2016/17 mid-season analysis.",
            "authors": [],
            "year": 2017,
            "venue": "Euro Surveill",
            "volume": "22",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.2807/1560-7917.ES.2017.22.7.30465"
                ]
            }
        },
        "BIBREF3": {
            "title": "Estimates of mortality attributable to influenza and RSV in the United States during 1997-2009 by influenza type or subtype, age, cause of death, and risk status.",
            "authors": [],
            "year": 2014,
            "venue": "Influenza Other Respi Viruses",
            "volume": "8",
            "issn": "",
            "pages": "507-15",
            "other_ids": {
                "DOI": [
                    "10.1111/irv.12258"
                ]
            }
        },
        "BIBREF4": {
            "title": "Influenza and respiratory syncytial virus are the major respiratory viruses detected from prospective testing of pediatric and adult coronial autopsies.",
            "authors": [],
            "year": 2013,
            "venue": "Influenza Other Respi Viruses",
            "volume": "7",
            "issn": "",
            "pages": "1113-21",
            "other_ids": {
                "DOI": [
                    "10.1111/irv.12139"
                ]
            }
        },
        "BIBREF5": {
            "title": "Repeated influenza vaccination for preventing severe and fatal influenza infection in older adults: a multicentre case-control study.",
            "authors": [],
            "year": 2018,
            "venue": "CMAJ",
            "volume": "190",
            "issn": "",
            "pages": "E3-12",
            "other_ids": {
                "DOI": [
                    "10.1503/cmaj.170910"
                ]
            }
        },
        "BIBREF6": {
            "title": "Viral shedding and transmission potential of asymptomatic and paucisymptomatic influenza virus infections in the community.",
            "authors": [],
            "year": 2017,
            "venue": "Clin Infect Dis",
            "volume": "64",
            "issn": "",
            "pages": "736-42",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Excess all-cause and influenza-attributable mortality in Europe, December 2016 to February 2017.",
            "authors": [],
            "year": 2017,
            "venue": "Euro Surveill",
            "volume": "22",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.2807/1560-7917.ES.2017.22.14.30506"
                ]
            }
        },
        "BIBREF8": {
            "title": "Use of electronic death certificates for influenza death surveillance.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "78-82",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2001.130471"
                ]
            }
        },
        "BIBREF9": {
            "title": "Trends for influenza-related deaths during pandemic and epidemic seasons, Italy, 1969-2001.",
            "authors": [],
            "year": 2007,
            "venue": "Emerg Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "694-9",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1305.061309"
                ]
            }
        },
        "BIBREF10": {
            "title": "Mortality attributable to 9 common infections: significant effect of influenza A, respiratory syncytial virus, influenza B, norovirus, and parainfluenza in elderly persons.",
            "authors": [],
            "year": 2012,
            "venue": "J Infect Dis",
            "volume": "206",
            "issn": "",
            "pages": "628-39",
            "other_ids": {
                "DOI": [
                    "10.1093/infdis/jis415"
                ]
            }
        },
        "BIBREF11": {
            "title": "Cause of death in hospitalized patients with laboratory-confirmed influenza.",
            "authors": [],
            "year": 2015,
            "venue": "An Sist Sanit Navar",
            "volume": "38",
            "issn": "",
            "pages": "263-8",
            "other_ids": {
                "DOI": [
                    "10.4321/S1137-66272015000200010"
                ]
            }
        },
        "BIBREF12": {
            "title": "Other respiratory viruses are important contributors to adult respiratory hospitalizations and mortality even during peak weeks of the influenza season.",
            "authors": [],
            "year": 2014,
            "venue": "Open Forum Infect Dis",
            "volume": "1",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1093/ofid/ofu086"
                ]
            }
        },
        "BIBREF13": {
            "title": "The use of coroner\u2019s autopsy reports to validate the use of targeted swabbing rather than tissue collection for rapid confirmation of virological causes of sudden death in the community.",
            "authors": [],
            "year": 2015,
            "venue": "J Clin Virol",
            "volume": "63",
            "issn": "",
            "pages": "59-62",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcv.2014.11.031"
                ]
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": 2017,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}