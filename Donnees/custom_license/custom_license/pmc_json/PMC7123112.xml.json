{
    "paper_id": "PMC7123112",
    "metadata": {
        "title": "Emerging Infectious Diseases: A Historical and Scientific Review",
        "authors": [
            {
                "first": "Godfrey",
                "middle": [
                    "B."
                ],
                "last": "Tangwa",
                "suffix": "",
                "email": "gbtangwa@gmail.com",
                "affiliation": {}
            },
            {
                "first": "Akin",
                "middle": [],
                "last": "Abayomi",
                "suffix": "",
                "email": "abayomi@sun.ac.za",
                "affiliation": {}
            },
            {
                "first": "Samuel",
                "middle": [
                    "J."
                ],
                "last": "Ujewe",
                "suffix": "",
                "email": "sujewe@yahoo.com",
                "affiliation": {}
            },
            {
                "first": "Nchangwi",
                "middle": [
                    "Syntia"
                ],
                "last": "Munung",
                "suffix": "",
                "email": "mnnnch001@myuct.ac.za",
                "affiliation": {}
            },
            {
                "first": "Gibril",
                "middle": [],
                "last": "Ndow",
                "suffix": "",
                "email": "gndow@mrc.gm",
                "affiliation": {}
            },
            {
                "first": "J.",
                "middle": [
                    "Radeino"
                ],
                "last": "Ambe",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Oyewale",
                "middle": [],
                "last": "Tomori",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Emerging Infectious Diseases (EIDs) are diseases caused by an infectious pathogen that has recently evolved and entered a given population for the first time; or a disease that had occurred before but whose incidence, impact and/or geographic range has increased or is expected to increase within a given time-frame (Jones 2008; CDC 1994; Morse and Schluederberg 1990). EIDs include novel infections in human, animal and plant populations.",
            "cite_spans": [
                {
                    "start": 323,
                    "end": 327,
                    "mention": "2008",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 333,
                    "end": 337,
                    "mention": "1994",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 363,
                    "end": 367,
                    "mention": "1990",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "Although the phrase \u201cemerging diseases\u201d has been used in scientific literature and publications since the early 1960s (Maurer 1962), it was not until after outbreaks of genital herpes and HIV/AIDS in the 1970s (Fleming et al. 1997) and early 1980s respectively that concern over EIDs and the use of the term became more widespread. Key landmark events and publications, notably the first conference on the prediction and prevention of EIDs jointly hosted by the Rockefeller University and National Institute for Health (NIH) in May 1989, and publication by the Institute of Medicine (IOM) in 1992 identifying the significance of EIDs and providing a guide for their recognition and prevention, stirred interest and attention towards EIDs and opened doors for more research, funding and publications (IOM 1992). The establishment of the Program for Monitoring Emerging Diseases (ProMED), devoted to rapidly disseminate information on infectious disease outbreaks and promote communication and collaboration amongst the international infectious diseases community during EID outbreaks, shortly followed this conference and publication. The Centres for Disease Control (CDC), in 1995, launched the Emerging Infectious Diseases journal, dedicated to rapidly disseminate reliable information on the emergence, prevention and elimination of EIDs.",
            "cite_spans": [
                {
                    "start": 126,
                    "end": 130,
                    "mention": "1962",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 226,
                    "end": 230,
                    "mention": "1997",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 804,
                    "end": 808,
                    "mention": "1992",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "Preceding these developments in EID dedicated programs and journals, research centres spread across the globe were developing vaccines and conducting trials to prevent various emerging infectious threats throughout history. The Institut Pasteur (IP), established in Paris in 1888, is one of the oldest of such centres for research on EIDs. The Institut, which conducts research at the frontline of EID outbreaks, aims to make discoveries to prevent, control, diagnose and treat EIDs. Soon after its creation, in 1896, Emile Marchoux established a microbiology laboratory in Saint-Louis, Senegal \u2013 the site where the first outbreak of yellow fever was reported in 1778 (Augustine 1909). This laboratory in Saint-Louis studied the epidemiology of malaria and sleeping sickness, and developed vaccines against smallpox, rabies and the plague. The laboratory was later transferred to Dakar in 1924 following an agreement with IP Paris, and was renamed the Institut Pasteur of Dakar. It is now a World Health Organisation (WHO) reference centre and a partner in the WHO Global Outbreak Alert and Response Network (GOARN). In addition to Senegal, Institut Pasteur is present in 9 other African countries: Guinea Conakry, Ivory Coast, Niger, Cameroon, Central African Republic, Madagascar, Morocco, Algeria and Tunisia; making the Institut Pasteur International Network a major player in almost every EID outbreak today.",
            "cite_spans": [
                {
                    "start": 679,
                    "end": 683,
                    "mention": "1909",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "In the 1920s, the West African Yellow Fever Commission was formed, run by a colonial research team from the Rockefeller Foundation. The Foundation build research centres and laboratories in Nigeria (Yaba, Lagos) and Ghana where significant studies and discoveries were made including the isolation of the yellow fever virus (Bigon 2014) and the eventual development of the yellow fever vaccine in 1937 by Max Theller (McNeill 2004). These centres would also pave the way for the establishment of the West African Council for Medical Research (WACMR) in 1954 by the British colonial government. Following independence in 1960, Nigeria took over the facilities of the WACMR and established the Medical Research Council (MRC). The facilites of this MRC were, in 1977, used by the Federal Military Government to establish the National Institute for Medical Research (NIMR) through the Research Institute Establishment Order 1977, pursuant to the National Science and Technology Development Agency Decree (No 5) of 1977.",
            "cite_spans": [
                {
                    "start": 331,
                    "end": 335,
                    "mention": "2014",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 426,
                    "end": 430,
                    "mention": "2004",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "The most developed of these early research establishments was perhaps the Medical Research Council (MRC) Unit in The Gambia. Established in 1947, it has since led infectious disease and public health research, making significant contributions in the prevention of endemic and emerging pathologies in the region including malaria, tuberculosis and Haemophilus influenza. It remains a beacon of excellence in bench, field and clinical research on infectious diseases, training and hosting some of Africa\u2019s leading scientists and producing a significant proportion of the region\u2019s research output. During the peak of the HIV epidemic in West Africa, the Unit supported the establishment of a field station in Caio, Guinea Bissau dedicated to the study and prevention of HIV infection. The MRC Unit in The Gambia is now incorporated within the London School of Hygiene & Tropical Medicine, and continues to forge partnerships with several other institutions in the West African region.",
            "cite_spans": [],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "In the mid-1950s to 1970s, some African nations through national assembly acts, created what are perhaps the oldest indigenous government supported research centres in sub-Saharan Africa, tasked purposely to improve the health and quality of life of their respective populations. The National Research Centre (NRC) of Egypt, the South African Medical Research Centre (SAMRC), Kenya Medical Research Institute (KEMRI), the Medical Research Council of Zimbabwe (MRCZ), the National Institute for Medical Research (NIMR) in Tanzania and the Medical Research Council of Nigeria represent such indigenous centres created between 1956 and 1979. KEMRI, in 1989, went on to form a partnership with the Wellcome Trust and the University of Oxford to create the KEMRI-Wellcome Trust Research Programme. This world-renowned health research unit trains scientific leaders and champions innovative and novel ideas to improve healthcare in Africa. Its Genomics and Infectious Diseases platform studies the transmission of infectious diseases, including EIDs, in order to inform disease outbreak policies and understand the evolution of resistance.",
            "cite_spans": [],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "Both the NIMR and the Noguchi Memorial Institute for Medical research, respectively the leading medical research institutes in Nigeria and Ghana, focus on preventing and controlling infectious diseases of the highest priority to their respective populations and the regions. Their research work includes studies to control and eradicate malaria, onchocerciasis, schistosomiasis, childhood diarrhoeal diseases, tuberculosis, HIV, leprosy, guinea worm, Ebola virus disease, yellow fever and Lassa fever.",
            "cite_spans": [],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "In 1988, at the height of the HIV epidemic in sub-Saharan Africa, an agreement between the governments of Uganda and Britain led to the establishment of the MRC Unit in Uganda. This unit, hosted by the Uganda Virus Research Institute (UVRI), primarily conducts research to investigate the determinants of HIV and related infections, their subsequent disease progression, and evaluate new preventive strategies and interventions for their control in Africa. The UVRI also hosts a CDC supported laboratory for rapid diagnosis of haemorrhagic fever viruses. This diagnostic infrastructure, coupled with other public health measures, has helped in a more rapid detection and control of outbreaks in Uganda. Now a hotspot for Ebola experts, Uganda and its centres of excellence have assisted in controlling outbreaks within the region.",
            "cite_spans": [],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "In 2004, the CDC in coordination with the WHO, established a Global Disease Detection (GDD) Regional Centre in Kenya, co-located with KEMRI. The GDD regional centre connects regions and countries throughout Africa, and assists in detecting and responding to disease outbreaks. A similar GDD regional centre was established in Thailand that year to address the complexity of EIDs in the Asia-Pacific region. Between 2005 and 2011, eight additional GDD centres were established in Bangladesh, Guatemala, Kazakhstan, China, Egypt, India, South Africa and Georgia, with all centres aiming to detect and stop disease outbreaks at their source thereby limiting spread and preventing epidemics. These centres, combined, have responded to hundreds of outbreaks; detected and identified scores of novel strains and pathogens; and improved regional capacity in personnel, diagnostics and surveillance.",
            "cite_spans": [],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "In addition to the GDD program, the WHO created the Global Outbreak Alert and Response Network (GOARN) to facilitate harnessing of international resources at the request of WHO member states during outbreaks. This network augments country and regional responses to ongoing or potential public health emergencies. GOARN, in 2014, facilitated the deployment of nearly 900 experts to West Africa during the Ebola outbreak. The WHO and CDC publish recommendations to guide the coordination of national surveillance and response standards for EIDs and communicable diseases.",
            "cite_spans": [],
            "section": "Overview and History",
            "ref_spans": []
        },
        {
            "text": "\n1. PATHOGEN FACTORSAdaptation to new hostVirulenceShort incubation periodRate of mutationMode, speed and ease of transmissionReplication cycleMortality rate2. HUMAN FACTORSDemographics and behaviourSusceptibility to new infection (lack of immunity)International travelPovertyDependence on livestock for food and labour3. ENVIRONMENTAL FACTORSWeather and climate changeChanging ecosystemDroughtFamine4. POLITICAL FACTORSLack of political willPoor healthcare infrastructureLack of adequate public health measuresEconomic developmentInstability (war, civil unrest)Social inequality5. OTHER FACTORSBioterrorismTechnology\n",
            "cite_spans": [],
            "section": "Box 3.1: Factors That Drive EIDs ::: Stages in the\u00a0Emergence of an\u00a0Infectious Disease",
            "ref_spans": []
        },
        {
            "text": "Despite the systems in place for preventing EIDs, the frequency of outbreaks have remained stable over the past decades. Between 1980 and 2017, more than two dozen EID outbreaks have been reports. Whilst some of these were re-emerging pathogens, there were more than ten newly emerging infectious diseases reported for the first time in humans during this time (Table 3.1). Unsurprisingly, most of these newly reported EIDs are new zoonotic viral infections or mutated forms of previously circulating viruses. Discussions of the epidemiology, the pathogen characteristics, and clinical features of these EIDs is beyond the scope of this chapter.\n",
            "cite_spans": [],
            "section": "Reported EID Outbreaks",
            "ref_spans": [
                {
                    "start": 368,
                    "end": 371,
                    "mention": "3.1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The increase in EID outbreaks in part reflects advances in medical and technological capabilities to detect and diagnose infections. However, increased globalisation, changes in global climate and human behaviour, natural and man-made disasters (poverty, war), weak healthcare systems, and lapses in public health measures all contribute to the increase in EIDs. The importance of these factors is borne in the fact that the majority of EIDs represent changes in the epidemiology, virulence or adaptation of previously known pathogens, with novel pathogens accounting for a minority of EID outbreaks.",
            "cite_spans": [],
            "section": "Reported EID Outbreaks",
            "ref_spans": []
        },
        {
            "text": "As previously mentioned, the WHO had set up GDD Centers in Kenya and other African countries. There are 12 offices in all, one of which is in South Africa. The One Health Program was founded as a collaborative effort to bring the best possible conditions for health to people, animals and the environment. It pulls together professionals from international, national and local regions in agreement with the objectives of the WHO. The One Health Program focuses on public health and zoonotic diseases in an effort to curtail zoonotic outbreaks that affect morbidity and mortality of people and animals alike. The South Africa Regional Global Disease Detection Center (GDD) provides support for the training and human capital development to prepare health officials in the detection and response to zoonotic diseases according to the GHS initiatives. The strengthening of laboratory surveillance, research programs and an enhanced surveillance infrastructure are objectives of the GDD and One Health. Priority Zoonotic diseases are:Viral, endemic zoonotic diseases: Rabies, Ebola, MarburgBacterial diseases: Anthrax, Brucellosis, Bovine Tuberculosis. Leptospirosis, Rickettsial diseases and food borne bacteria like SalmonellaWHO neglected diseases: Toxoplasmosis, Cysticercosis, Cryptosporidium\n",
            "cite_spans": [],
            "section": "One Health Program, South Africa ::: Animal Diseases of Economic and/or Zoonotic Impact",
            "ref_spans": []
        },
        {
            "text": "Rinderpest is German for \u201ccattle plague\u201d. It is a highly infectious viral disease that can affect domesticated and wild, cloven-hoofed mammals. African cattle and buffalo are extremely susceptible to the disease while sheep and goats experience a mild form that is not as acute, resulting in lower fatalities. In susceptible animal, Rinderpest infection has a fatality rate greater than 90% and a mortality rate of 100%. In the 1890\u2019s, an outbreak of Rinderpest South of the River Zambezi killed about 5.2 million cattle, sheep, goats and oxen, and an unknown number of wild giraffe, buffalo and wildebeest. This Rinderpest epidemic, happening at a time of much historical tumult, was the most devastating epidemic to hit southern Africa in the late nineteenth century. There is no conclusive evidence on how Rinderpest was brought to Africa. It is thought to have been brought through Indian cattle by Italian colonial masters who introduced the cattle to the Horn of Africa. The disease is spread through ingested contaminated matter and/or through direct contact. Endemic areas have caused animals to develop immunity through exposure or vaccinations. In sub-Saharan Africa, Rinderpest outbreaks resulted in the death of 90% of domestic oxen. The resulting famine, both from loss of livestock and diminished agricultural produce, led to significant mortality among Ethiopian and Masai populations. European nations gradually eliminated Rinderpest in the early twentieth century through surveillance, early detection and culling of sick and exposed animals. It however continued to afflict developing African and Asian countries well into the second half of the twentieth century, re-emerging after several eradication campaigns were prematurely shut down due to mistaken belief that the virus had been eradicated. It was not until 2011 that Rinderpest was eradicated, marking one of the greatest veterinary achievements in recent history.",
            "cite_spans": [],
            "section": "Rinderpest ::: Animal Diseases of Economic and/or Zoonotic Impact",
            "ref_spans": []
        },
        {
            "text": "Rift Valley Fever (RVF) is a virus from the Phlebovirus genus, first identified in the Rift Valley of Kenya in 1931. RVF primarily infects animals but has the ability to also affect humans. Since 1931, there have been repeated outbreaks in sub-Saharan Africa. Infected cattle along the River Nile and its irrigation system caused an outbreak of RVF in Egypt in 1977 that killed approximately 600 people. In 1987 there was an outbreak in West Africa that was connected with the Senegal River Project construction. In 1997\u201398, there was another outbreak due to El Nino in Tanzania, Kenya and Somalia amidst extensive flooding. Humans can get RVF through the bites of infected insects and contact with blood of infected animals.",
            "cite_spans": [],
            "section": "Rift Valley Fever ::: Animal Diseases of Economic and/or Zoonotic Impact",
            "ref_spans": []
        },
        {
            "text": "Also known as Ovine Rinderpest, PPR is a disease that affects smaller ruminants (primarily sheep and goats) in Central and Southern Africa, North Africa, the Middle East and the Indian subcontinent. It is a highly contagious and fatal disease, making its impact on livestock and human livelihood very important, and its global eradication an urgent need (Baron et al. 2011).",
            "cite_spans": [
                {
                    "start": 368,
                    "end": 372,
                    "mention": "2011",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Peste de Petits Ruminants (PPR) ::: Animal Diseases of Economic and/or Zoonotic Impact",
            "ref_spans": []
        },
        {
            "text": "\nEstablishment and/or standardization of national disease surveillance systemsImprovement of hospitals and laboratories to be able to identify, characterise and contain EIDsEstablishment of strong EID databases and pathways to notify local and regional authorities of emerging or suspected EIDsAlignment of primary health care structures with surveillance systemsDevelopment of easy algorithms for detecting clusters of new or unusual diseases, especially in rural parts of low- and middle-income countries.\n",
            "cite_spans": [],
            "section": "Surveillance ::: Control of EIDs and Deadly Outbreaks in Humans",
            "ref_spans": []
        },
        {
            "text": "\nRapid, efficient response mechanisms to emerging outbreaksWell-funded emergency response systems able to mobilise investigative and control efforts of local agencies\n",
            "cite_spans": [],
            "section": "Response Mechanism ::: Control of EIDs and Deadly Outbreaks in Humans",
            "ref_spans": []
        },
        {
            "text": "\nDevelop national research agenda prioritizing EID researchDedicated EID research funding to drive and address local prioritiesPromote research on EID control measures, surveillance methods, and development of vaccines and drugsTrain competent field epidemiologists, scientists, clinicians and other relevant personnel for EID surveillance and control.Create stockpiles of vaccines and drugs for known EIDs, ready for deployment in the event of an outbreak.\n",
            "cite_spans": [],
            "section": "Research and Training ::: Control of EIDs and Deadly Outbreaks in Humans",
            "ref_spans": []
        },
        {
            "text": "\nPublic health training to interrupt transmission of known pathogens, and reduce ongoing transmission during outbreaksPublic engagement and collaboration during EID outbreaksUnderstanding cultural practices of relevance to EID outbreaks and disease transmission, and considering such practices when designing control and response mechanismsGood governance, policy and political will to prevent\n",
            "cite_spans": [],
            "section": "Public Health Control Measures ::: Control of EIDs and Deadly Outbreaks in Humans",
            "ref_spans": []
        },
        {
            "text": "It has been 50 years or more since many African countries became independent. During this period, Africa has experienced numerous disease outbreaks, many of which have become endemic and uncontrolled. It is sorrowful and painful to note that Africa remains the verdant pasture for EIDs despite global improvements in disease surveillance systems and better reliable laboratory diagnostic facilities. Behind Africa\u2019s failure to successfully control EID outbreaks is the pervasive state of poor governance in many African countries, with governments continuing to pay lip service to health issues including disease control, and African healthcare systems remaining weak despite repeated EID outbreaks. In the absence of good governance, priorities get misplaced and an atmosphere of poor planning and corruption is easily promoted resulting in poor allocation and mismanagement of funds for disease control and surveillance. It is essential for African governments to note that a healthy population is a sine qua non for orderly economic and social development, and that EID outbreaks negatively impact national economies and other development plans. It is only through good governance and political/economic stability that African countries can improve health security and prevent EIDs and preventable loss of life.",
            "cite_spans": [],
            "section": "Conclusion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 3.1: Emerging Infectious Diseases between 1980 and 2017\n",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Augustine",
                    "suffix": ""
                }
            ],
            "year": 1909,
            "venue": "History of yellow fever",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Emerging viruses: The evolution of viruses and viral diseases",
            "authors": [
                {
                    "first": "SS",
                    "middle": [],
                    "last": "Morse",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Schluederberg",
                    "suffix": ""
                }
            ],
            "year": 1990,
            "venue": "The Journal of Infectious Diseases",
            "volume": "162",
            "issn": "",
            "pages": "1-7",
            "other_ids": {
                "DOI": [
                    "10.1093/infdis/162.1.1"
                ]
            }
        },
        "BIBREF2": {
            "title": "Ecological origins of novel human pathogens",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Woolhouse",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Gaunt",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Critical Reviews in Microbiology",
            "volume": "33",
            "issn": "4",
            "pages": "231-242",
            "other_ids": {
                "DOI": [
                    "10.1080/10408410701647560"
                ]
            }
        },
        "BIBREF3": {
            "title": "Peste des petits ruminants: A suitable candidate for eradication?",
            "authors": [
                {
                    "first": "MD",
                    "middle": [],
                    "last": "Baron",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Parida",
                    "suffix": ""
                },
                {
                    "first": "CA",
                    "middle": [],
                    "last": "Oura",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "The Veterinary Record",
            "volume": "169",
            "issn": "1",
            "pages": "16-21",
            "other_ids": {
                "DOI": [
                    "10.1136/vr.d3947"
                ]
            }
        },
        "BIBREF4": {
            "title": "Translational networks of administering disease and urban planning in West Africa: The inter-colonial conference on yellow fever, Dakar, 1928",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Bigon",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "GeoJournal",
            "volume": "79",
            "issn": "1",
            "pages": "103-111",
            "other_ids": {
                "DOI": [
                    "10.1007/s10708-013-9476-z"
                ]
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Herpes simplex virus type 2 in the United States, 1976 to 1994",
            "authors": [
                {
                    "first": "DT",
                    "middle": [],
                    "last": "Fleming",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "The New England Journal of Medicine",
            "volume": "337",
            "issn": "",
            "pages": "1105-1111",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJM199710163371601"
                ]
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 1992,
            "venue": "Emerging infections: Microbial threats to health in the United States",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Global trends in emerging infectious diseases",
            "authors": [
                {
                    "first": "KE",
                    "middle": [],
                    "last": "Jones",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Nature",
            "volume": "451",
            "issn": "",
            "pages": "990-993",
            "other_ids": {
                "DOI": [
                    "10.1038/nature06536"
                ]
            }
        },
        "BIBREF9": {
            "title": "Equine piroplasmosis\u00a0\u2013 Another emerging disease",
            "authors": [
                {
                    "first": "FD",
                    "middle": [],
                    "last": "Maurer",
                    "suffix": ""
                }
            ],
            "year": 1962,
            "venue": "Journal of the American Veterinary Medical Association",
            "volume": "141",
            "issn": "",
            "pages": "699-702",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Yellow jack and geopolitics: Environment, epidemics, and the struggles for empire in the American tropics, 1650\u20131825",
            "authors": [
                {
                    "first": "JR",
                    "middle": [],
                    "last": "McNeill",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "OAH Magazine of History",
            "volume": "18",
            "issn": "3",
            "pages": "9-13",
            "other_ids": {
                "DOI": [
                    "10.1093/maghis/18.3.9"
                ]
            }
        }
    }
}