{
    "paper_id": "PMC3375803",
    "metadata": {
        "title": "Medical Students and Pandemic Influenza",
        "authors": [
            {
                "first": "Benjamin",
                "middle": [],
                "last": "Herman",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rhonda",
                "middle": [
                    "J."
                ],
                "last": "Rosychuk",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tracey",
                "middle": [],
                "last": "Bailey",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Robert",
                "middle": [],
                "last": "Lake",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Olive",
                "middle": [],
                "last": "Yonge",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Thomas",
                "middle": [
                    "J."
                ],
                "last": "Marrie",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "As part of pandemic influenza planning at the University of Alberta, researchers developed a Web-based questionnaire (4) comprising 42 questions to assess demographic information, risk perception of pandemic influenza (likelihood of developing and dying from the illness), general knowledge about pandemic influenza, willingness to volunteer, and suggested consequences of not volunteering during a pandemic. Response options were generally either Yes/No or a 5-point Likert-type scale (e.g., ranging from very unlikely to very likely). The final version was administered through the Internet from September 12, 2006, until October 31, 2006. The 5-point scale responses were dichotomized. Respondents with missing values were excluded from data summaries and statistical tests.",
            "cite_spans": [
                {
                    "start": 118,
                    "end": 119,
                    "mention": "4",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "All 510 medical students at the University of Alberta were invited by email to complete the questionnaire; 354 (69%) responded. Most (62%) respondents were women, and all but 1 were reported to be in good health. A variety of past volunteer activities were reported. Although medical students obtained health information from varying sources, they had the highest degree of confidence in the information provided by physicians.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Medical students\u2019 knowledge regarding the spread, prevention, and treatment of influenza is summarized in Table 1. Also included is the perceived likelihood of infection and outcome of such infection; 146 (41.2%) of students believed they were likely to be infected, but only 16 (4.5%) believed they would die from such an infection. Regarding volunteering during a future pandemic, most (247) believed that healthcare students have an obligation to do so (Table 2). A minority (30) agreed with penalties for heathcare students and academic staff who refused to provide services as required by the government. Healthcare and emergency workers were given the highest priority when assigning scarce resources; politicians were assigned the lowest priority.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 106,
                    "end": 113,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 457,
                    "end": 464,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "This study shows that medical students have knowledge gaps with respect to various aspects of pandemic influenza. Most medical students knew that the main route of influenza transmission is by direct contact and respiratory droplets from coughing and sneezing. However, despite no supporting medical evidence, 88 (24.9%) medical students responded that blood transfusion is a major route of transmission (5); 75 (21.2%) students also indicated influenza could be transmitted through sexual contact. Although direct sexual contact can transmit influenza, it is unlikely that this would be a major route of transmission.",
            "cite_spans": [
                {
                    "start": 405,
                    "end": 406,
                    "mention": "5",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Preventive measures are important not just during a pandemic, but at all times to decrease the risk of transmission of bacterial and viral illnesses. Most medical students (93.5% and 81.6%, respectively) correctly identified hand washing and cough etiquette, respectively, as effective preventive measures (it is important to note, however, that there is a difference between students knowing and complying). Social distancing can help reduce the peak incidence of an epidemic and spread it over several weeks instead of a few, thus avoiding a healthcare surge (6). Household quarantine is also effective at reducing attack rates in the community but only if the compliance rate is high (7). Knowledge regarding treatment of pandemic influenza is difficult to analyze because determining what measures would be effective is complex. Given the time lag between identification of the pandemic strain and the time needed to produce a vaccine, any known treatment strategy would not be effective unless it could be applied to the strain that is causing the pandemic.",
            "cite_spans": [
                {
                    "start": 562,
                    "end": 563,
                    "mention": "6",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 688,
                    "end": 689,
                    "mention": "7",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Although it is encouraging that most students believe healthcare students have an obligation to volunteer during a pandemic, a dichotomy exists when it comes to setting a policy for actual deployment. One argument is that medical students should be encouraged to volunteer during a crisis because this will provide valuable medical training at a time when a great deal of the workforce\u2014including physicians and nurses\u2014will be ill and unable to work. During the 1918 influenza pandemic, some medical students in the United States graduated early from medical school and had expedited medical board examinations to increase the number of medical personnel available to provide services (8). Conversely, some may argue that placing the next generation of doctors on the front lines as volunteers will put them at increased risk for contracting illness. It is possible in this situation that the numbers of future physicians could decrease and adversely affect recovery efforts after a pandemic. During the severe acute respiratory syndrome crisis in Toronto, medical students were removed from the wards (D. Low, pers. comm.) (9). Although this was done with good intentions and safety in mind, students experienced a great deal of frustration regarding the suspension of their education and research activities (9).",
            "cite_spans": [
                {
                    "start": 685,
                    "end": 686,
                    "mention": "8",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1124,
                    "end": 1125,
                    "mention": "9",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1310,
                    "end": 1311,
                    "mention": "9",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Other recent events may offer insight into the desirability of using medical students as volunteers. Anecdotes from the terrorist attacks in New York City on September 11, 2001, relate how medical students helped in various capacities immediately following the tragedy (10). However, no official school or government mandate was issued forcing these persons to volunteer. In contrast, after the devastation of Hurricane Rita, college leaders at Texas A&M University College of Medicine made a decision to cancel classes and provided orientation sessions for students so that they could begin volunteering as soon as possible (11). Volunteer tasks in this situation were limited, and many voiced frustration that their relatively limited medical skills greatly restricted their ability to contribute. In a more positive vein, students also claimed that \u201cthe experience made them even more driven to become the best doctors they can\u201d (11).",
            "cite_spans": [
                {
                    "start": 270,
                    "end": 272,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 626,
                    "end": 628,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 933,
                    "end": 935,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Our study was limited in that only 69% of the medical students at 1 medical school in Canada responded. However, the results do show that planning and debate should begin before a pandemic occurs and that expectations of the role of medical students must be explicit (12). A government must consider the virulence and characteristics of the illness as well as the number of volunteers needed before deciding on an appropriate course of action. Ultimately, it remains to be seen what the climate of the next pandemic will be.",
            "cite_spans": [
                {
                    "start": 268,
                    "end": 270,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2006,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2005,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Virulent epidemics and scope of healthcare workers\u2019 duty of care.",
            "authors": [],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "1238-41",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "H5N1 outbreaks and enzootic influenza.",
            "authors": [],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "3-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Non-pharmaceutical interventions for pandemic influenza, national and community measures.",
            "authors": [],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "88-94",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Does clinical experience affect medical students' knowledge, attitudes, and compliance with universal precautions?",
            "authors": [],
            "year": 1998,
            "venue": "Infect Control Hosp Epidemiol",
            "volume": "19",
            "issn": "",
            "pages": "767-71",
            "other_ids": {
                "DOI": [
                    "10.1086/647721"
                ]
            }
        },
        "BIBREF10": {
            "title": "Influenza in 1918: recollections of the epidemic in Philadelphia. 1976.",
            "authors": [],
            "year": 2006,
            "venue": "Ann Intern Med",
            "volume": "145",
            "issn": "",
            "pages": "138-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Fear of SARS thwarts medical education in Toronto.",
            "authors": [],
            "year": 2003,
            "venue": "BMJ",
            "volume": "326",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1136/bmj.326.7393.784/c"
                ]
            }
        }
    }
}