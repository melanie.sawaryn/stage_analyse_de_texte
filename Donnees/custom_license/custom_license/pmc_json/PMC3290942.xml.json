{
    "paper_id": "PMC3290942",
    "metadata": {
        "title": "Public Understanding of Pandemic Influenza, United Kingdom",
        "authors": [
            {
                "first": "Ravindra",
                "middle": [
                    "K."
                ],
                "last": "Gupta",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Martina",
                "middle": [],
                "last": "Toby",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Gagori",
                "middle": [],
                "last": "Bandopadhyay",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mary",
                "middle": [],
                "last": "Cooke",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [],
                "last": "Gelb",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jonathan",
                "middle": [
                    "S."
                ],
                "last": "Nguyen-Van-Tam",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "To the Editor: Widespread outbreaks of influenza A (H5N1) in poultry and severe infections in humans have raised the possibility of an influenza pandemic. The 3 influenza A pandemics of the 20th century (1) were associated with considerable socioeconomic disruption as well as many deaths and pressure on health services. Experiences in the United States during the 1918\u20131920 pandemic (2) suggest that government advice that conflicts with personal or societal beliefs may not be followed, thus jeopardizing public health measures. Experience from the outbreak of severe acute respiratory syndrome has highlighted some pitfalls in achieving public understanding (3) and compliance (4) in the era of mass communication. Even if initial compliance is achieved, previous behavior patterns may reemerge during a pandemic as people begin to perceive that they have little control over the threat (5) or reduce their estimation of the risk (6).",
            "cite_spans": [
                {
                    "start": 204,
                    "end": 205,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 386,
                    "end": 387,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 663,
                    "end": 664,
                    "mention": "3",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 682,
                    "end": 683,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 892,
                    "end": 893,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 935,
                    "end": 936,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Building robust public understanding has been made a priority in preparedness and response plans (7). However, despite widespread media coverage, little attention has been paid to assessment of public knowledge about the threat for pandemic influenza and surrounding issues. Such information may be essential to optimize public education strategies.",
            "cite_spans": [
                {
                    "start": 98,
                    "end": 99,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A questionnaire-based population survey was administered in March 2005 by 2 of the authors (MT and GB) to identify public knowledge about pandemic influenza, awareness of its potential effects, key information needs, and willingness to follow advice about public health measures. A structured interview consisting of 20 questions was used. Participants were approached at random and interviewed (in English) in public places including parks, shopping malls, libraries, and train stations in northern London. This area has considerable ethnic diversity (55% of the population is nonwhite) and a socioeconomic status similar to the rest of London. Recruited participants were >18 years of age and resided in the United Kingdom. They were excluded if another family member had previously completed the survey. Age and sex ratios were selected to reflect population centiles calculated from the 2001 UK population census. Statistical analyses were conducted with Fisher exact tests and epidemiologic tabulations in Stata version 8.2 (Stata Corp., College Station, TX, USA).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Of 273 persons approached for interview, 225 accepted and were eligible. Nine questionnaires were incomplete and therefore excluded, leaving 216 (79%) for analysis. Demographic characteristics of participants are summarized in the Table A1. Half the respondents chose the correct definition of a pandemic from 5 options. Statistical analysis demonstrated that those 32\u201344 years of age were more likely than those of other age groups to choose correctly (p = 0.001). Persons who left school at ages >17 years were more likely than those who left school earlier to select the correct answer (p = 0.007).",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 231,
                    "end": 239,
                    "mention": "Table A1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Sex of the respondent did not influence correct response; 56% of those 18\u201331 years of age versus 86% of those >60 years of age were aware of the threat of pandemic influenza (p = 0.006). When asked the likelihood of a pandemic during the next 10 years, 71% responded that it was likely or very likely, whereas 16% considered it unlikely or very unlikely. When offered a list of 4 possible negative affects identified by experts (healthcare service, food distribution, fuel distribution, and disruption to tourism), only one fourth thought that all 4 would occur. Details about symptoms of pandemic influenza were most frequently cited as the main public information need in the event of a pandemic. Television was rated by 68% of respondents as their preferred means of receiving information during a pandemic. Almost all respondents (97%) would wash their hands >5 times each day if requested, and 86% would definitely or probably be willing to stay away from public gatherings (unspecified) if asked. However, only 61% would stay away from work (unspecified period) as a means of avoiding pandemic influenza.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "As far as we know, this is the first population-based study of knowledge and understanding of pandemic influenza. Public understanding of this threat and its potential effect in the United Kingdom appears to be limited. Our findings that older adults are more aware than younger persons has also been found in other settings (8) as has the increased public health awareness in more educated groups (9,10). Economic considerations retain high importance even with a potentially fatal threat, a phenomenon that has been previously noted with regard to self-quarantine (4). Our study did not address whether reluctance to take time off from work was more likely to be associated with public or private sector employment or self-employment. Further study in this area would help preparedness strategy.",
            "cite_spans": [
                {
                    "start": 326,
                    "end": 327,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 399,
                    "end": 400,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 401,
                    "end": 403,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 567,
                    "end": 568,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "This study was limited by a relatively small sample size, and its setting in 1 region of London may have implications regarding the extent to which the findings are applicable elsewhere. Further, larger assessments are needed both before and after specific pandemic influenza awareness programs as part of the ongoing process of pandemic preparedness.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "The epidemiology and clinical impact of pandemic influenza.",
            "authors": [],
            "year": 2003,
            "venue": "Vaccine",
            "volume": "21",
            "issn": "",
            "pages": "1762-8",
            "other_ids": {
                "DOI": [
                    "10.1016/S0264-410X(03)00069-0"
                ]
            }
        },
        "BIBREF1": {
            "title": "Socioeconomic differences in glaucoma patients' knowledge, need for information and expectations of treatments.",
            "authors": [],
            "year": 2006,
            "venue": "Acta Ophthalmol Scand",
            "volume": "84",
            "issn": "",
            "pages": "84-91",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1600-0420.2005.00587.x"
                ]
            }
        },
        "BIBREF2": {
            "title": "Implications of pandemic influenza for bioterrorism response.",
            "authors": [],
            "year": 2000,
            "venue": "Clin Infect Dis",
            "volume": "31",
            "issn": "",
            "pages": "1409-13",
            "other_ids": {
                "DOI": [
                    "10.1086/317493"
                ]
            }
        },
        "BIBREF3": {
            "title": "Lessons from the severe acute respiratory syndrome outbreak in Hong Kong.",
            "authors": [],
            "year": 2003,
            "venue": "Emerg Infect Dis",
            "volume": "9",
            "issn": "",
            "pages": "1042-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Quarantine stressing voluntary compliance.",
            "authors": [],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "1778-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Rating threat mitigators: faith in experts, governments, and individuals themselves to create a safer world.",
            "authors": [],
            "year": 1998,
            "venue": "Risk Anal",
            "volume": "18",
            "issn": "",
            "pages": "547-56",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1539-6924.1998.tb00368.x"
                ]
            }
        },
        "BIBREF6": {
            "title": "Avian influenza risk perception, Hong Kong.",
            "authors": [],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "677-82",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Public knowledge of heart attack in a Nepalese population survey.",
            "authors": [],
            "year": 2006,
            "venue": "Heart Lung",
            "volume": "35",
            "issn": "",
            "pages": "164-9",
            "other_ids": {
                "DOI": [
                    "10.1016/j.hrtlng.2005.12.003"
                ]
            }
        },
        "BIBREF9": {
            "title": "Cancer knowledge and disparities in the information age.",
            "authors": [],
            "year": 2006,
            "venue": "J Health Commun",
            "volume": "11",
            "issn": "Suppl 1",
            "pages": "1-17",
            "other_ids": {
                "DOI": [
                    "10.1080/10810730600637426"
                ]
            }
        }
    }
}