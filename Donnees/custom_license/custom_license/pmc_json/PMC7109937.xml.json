{
    "paper_id": "PMC7109937",
    "metadata": {
        "title": "Lack of Association between Infection with a Novel Human Coronavirus (HCoV), HCoV-NH, and Kawasaki Disease in Taiwan",
        "authors": [
            {
                "first": "Luan-Yin",
                "middle": [],
                "last": "Chang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bor-Luen",
                "middle": [],
                "last": "Chiang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Chuan-Liang",
                "middle": [],
                "last": "Kao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mei-Hwan",
                "middle": [],
                "last": "Wu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Pei-Jer",
                "middle": [],
                "last": "Chen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ben",
                "middle": [],
                "last": "Berkhout",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hui-Ching",
                "middle": [],
                "last": "Yang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Li-Min",
                "middle": [],
                "last": "Huang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Kawasaki disease (KD) is an acute systemic febrile illness of unknown etiology that predominantly affects children <5 years old. Initially described in 1967 by Kawasaki [1], it has become the most common cause of acquired heart disease in children in the developed world, as rheumatic heart disease occurs much less frequently than before. Although normally self-limiting, KD is associated with a range of complications, the most important of which is the development of life-threatening coronary arterial abnormalities",
            "cite_spans": [
                {
                    "start": 170,
                    "end": 171,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Since the implementation of National Health Insurance (NHI) in Taiwan in 1995, >95% of Taiwan\u2019s population has been entitled to use the same health care system, and a database has been built that includes data on hospitalization and outpatient care. Using the NHI data, we have found that there is a seasonal clustering of KD, usually in the summer, and that the annual incidence in Taiwan is 50\u201370 cases/100,000 children <5 years old [2], which is much higher than the annual incidence in Western countries and is the second highest annual incidence in the world, just after Japan\u2019s [3\u20138]. Therefore, KD can be considered one of the most important childhood diseases in Taiwan, and it should be thoroughly investigated",
            "cite_spans": [
                {
                    "start": 436,
                    "end": 437,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 585,
                    "end": 588,
                    "mention": "3\u20138",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The evidence that suggests that the etiology of KD can be traced to an infectious agent includes temporal clustering and marked seasonality, geographic clustering, a strong association between the frequency of KD and infectious-disease surveillance [2, 9], and age distribution (with the highest incidence among children 6 months to 2 years old, who have little maternal antibody and are, in general, the most susceptible to infection) [2]. Various infectious agents have been implicated as potential causes of KD, as have certain immunological agents and toxins, including bacterial toxin\u2013mediated superantigens, allergens such as anionic detergents and house dust mites, and some chemicals. To date, no link between any of these individual agents and KD has been irrefutably established. Recently, infection with a novel human coronavirus (HCoV), called \u201cNew Haven coronavirus\u201d (HCoV-NH) [10]\u2014which is similar to and likely represents the same species as another novel HCoV, HCoV-NL63 [11]\u2014was reported to be associated with KD [12]. Shortly thereafter, 2 groups of researchers reported that they did not find any association between HCoV-NH infection and KD [13, 14]. Therefore, controversy exists as to whether there is an association between HCoV-NH infection and KD, and further studies are needed to investigate whether the virus plays a role in the etiology of KD. Thus, we evaluated whether HCoV-NH infection is associated with KD in Taiwan",
            "cite_spans": [
                {
                    "start": 250,
                    "end": 251,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 253,
                    "end": 254,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 437,
                    "end": 438,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 891,
                    "end": 893,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 988,
                    "end": 990,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1031,
                    "end": 1033,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1162,
                    "end": 1164,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1166,
                    "end": 1168,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nPatients, materials, and methodsThe Institutional Review Board of National Taiwan University Hospital approved the collection of clinical specimens and data for the present study. Between February 2004 and April 2005, patients with KD were enrolled at National Taiwan University Hospital and Taiwan Adventist Hospital in the city of Taipei, at Far Eastern Memorial Hospital in Taipei County, and at Min-Sheng Hospital in Tao-Yuan County. We enrolled patients with KD who had at least 5 of the following 6 manifestations: fever for >5 days, skin rash, neck lymphadenopathy, nonpurulent bulbar conjunctivitis, red lip with fissure and/or strawberry tongue, and palm/sole erythema and induration followed by desquamation. We also enrolled patients with incomplete KD, defined as occurring in patients who had <5 of the above manifestations but who had a coronary arterial abnormality. A coronary arterial abnormality was defined as a lumen diameter (inner border to inner border) of \u2a7e3 mm in patients \u2a7d5 years old and of \u2a7e4 mm in patients >5 years old. After informed consent was obtained from the parents of the patients, serum, peripheral-blood mononuclear cells (PBMCs), throat swabs, nasopharyngeal aspirates, and rectal swabs were obtained during hospitalization (acute illness) and at convalescence (\u223c2 weeks after hospital discharge). The acute-illness specimens were assayed for viral isolation by bacterial culture with HEp-2, MK-2, RD, HEL, and Vero cells and for specific pathogens by polymerase chain reaction (PCR). A questionnaire was also administered, by which contact history and clinical symptoms were ascertained. For all patients, 2-dimensional echocardiography was performed during hospitalization and again \u223c8 weeks after hospital discharge. Laboratory data and data on responses to intravenous immunoglobulin (IVIG) and coronary arterial abnormalities were also collected",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Serum, PBMCs, nasopharyngeal aspirates, throat swabs, and rectal swabs were processed for RNA extraction, which was performed using an RNA isolation kit in accordance with the manufacturer\u2019s instructions (QIAamp Viral RNA Mini Kit; Qiagen). For detection of HCoV-NL63 (GenBank accession number AY567487), the forward primer (5\u2032-CTAGTTCTTCTGGTACTTCCACTCC-3\u2032; nt 26746\u201326770), the reverse primer (5\u2032-TCTGGTAGGAACACGCTTCCAA-3\u2032; nt 26858\u201326837), and the TaqMan probe (5\u2032-TAAGCCTCTTTCTCAACCCAGGGC-3\u2032; nt 26783\u201326806) were designed for detection of the N gene by real-time reverse-transcriptase (RT) PCR (LightCycler; Roche). A 255-bp product covering this region was generated by RT-PCR with the forward primer 5\u2032-GATAACCAGTCGAAGTCACCTAGTTC-3\u2032 (nt 26727\u201326752), the reverse primer 5\u2032-ATTAGGAATCAATTCAGCAAGCTGTG-3\u2032 (nt 26981\u201326956), and the RNA template derived from the HCoV-NL63 strain (gift from L. van der Hoek, University of Amsterdam). The RT-PCR product was cloned into a TA cloning vector (yT&A vector; Yeastern Biotech), to generate the construct. The in vitro\u2013transcribed RNA was purified and quantified to determine the copy number and was subsequently used as the positive template for real-time RT-PCR. An aliquot (2.6 \u03bcL) of RNA isolated from the clinical specimens, the positive template (10, 100, 1000, and 10,000 copies of the in vitro\u2013transcribed RNA), and the negative control (distilled water) were subjected to real-time RT-PCR. The real-time RT-PCR amplification program was as follows: 20 min at 60\u00b0C, for reverse transcription; 2 min at 95\u00b0C, to denature the cDNA; 45 cycles of 5 s at 95\u00b0C and 10 s at 60\u00b0C, for amplification; and 30 s at 40\u00b0C, for cooling. The sensitivity of this real-time RT-PCR was \u2a7d10 copies of in vitro\u2013transcribed RNA, and the linear regression coefficient of the standard dilution series was 0.99 to 1. To investigate possible inhibitors in the clinical specimens, a spike assay with 100 copies of the in vitro\u2013transcribed RNA was performed on randomly selected HCoV-NL63\u2013negative specimens, including 20 rectal swabs, 10 throat swabs, and 8 serum specimens. The mean copy number of the spiked clinical specimens was 78 copies/100 copies of the in vitro\u2013transcribed RNA (SD, 22 copies; SE, 5.1 copies) and did not differ significantly from the standard value of 67 copies/100 copies of the in vitro\u2013transcribed RNA (P=.08). Thus, the PCR conditions for the clinical and standard specimens were comparable, and no significant PCR inhibitors were found in our clinical specimens",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "This assay has been successfully used to identify circulation of HCoV-NL63 in Taiwan within the period of the present study. We screened 219 clinical respiratory tract specimens that had been collected between May and October 2004 and identified 5 children (2.3%) infected with HCoV-NL63 (authors\u2019 unpublished data)",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Following the method of Esper et al. [10, 12], we screened for HCoV-NH by RT-PCR with primers specific for the HCoV-NH replicase 1a gene (forward primer, 5\u2032-GCGCTATGAGGGTGGTTGTAAC-3\u2032; reverse primer, 5\u2032 -CGCGCAGTTAAAAGTCCAGAATTAAC-3\u2032 [underscoring indicates G/C clamps]). Each set of RT-PCR amplifications included appropriate positive (HCoV-NL63 RNA) and negative (distilled water) controls. PCR products were analyzed by agarose gel electrophoresis",
            "cite_spans": [
                {
                    "start": 38,
                    "end": 40,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 42,
                    "end": 44,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nResultsA total of 53 Taiwanese patients with KD were enrolled. Clinical information for these patients is shown in table 1, and the numbers of specimens obtained for analysis are shown in table 2. Fifty-two patients (98%) fulfilled the criteria for KD (28 fulfilled all 6 of the criteria, and 24 fulfilled 5 of the criteria), and only 1 patient had incomplete KD (4 criteria plus left coronary arterial dilatation of 3.9 mm in diameter). The mean \u00b1 SD age of the patients was 2.30 \u00b1 1.93 years, and 27 (51%) were male. All of the patients had fever; the mean \u00b1 SD duration of fever was 8.2 \u00b1 2.8 days, and the mean \u00b1 SD peak body temperature was 39.7\u00b0C \u00b1 0.6\u00b0C. Of the patients, 96% had skin rash, 40% had erythema and induration over the bacille Calmette-Gu\u00e9rin scar, 96% had nonpurulent bulbar conjunctivitis, 92% had red lip with fissure and/or strawberry tongue, 74% had neck lymphadenopathy, 85% had palm/sole erythema and induration, and 100% had desquamation (96% had periungual desquamation, and 40% had perianal desquamation). In addition to the above typical symptoms of KD, 75% had cough, 72% had rhinorrhea, and 72% had diarrhea",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 116,
                    "end": 123,
                    "mention": "table 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 189,
                    "end": 196,
                    "mention": "table 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "All of the blood cultures were negative, no group A streptococcus was identified in throat swabs, and no significant urine culture was obtained. Echocardiography showed that 4 (8%) of the 53 patients had a coronary arterial abnormality during the acute stage of KD: the first with a left coronary arterial diameter of 4.4 mm and a right coronary arterial diameter of 4.2, the second with a left anterior descending coronary arterial diameter of 4.2 mm, the third with a left coronary arterial diameter of 3.9 mm, and the fourth with a left coronary arterial diameter of 3.3 mm. All 53 patients received a second echocardiography \u223c8 weeks after hospital discharge, and it was found that 2 (the first and the fourth) of the 4 patients who had had a coronary arterial abnormality during the acute stage still had a coronary arterial abnormality: the first with a right coronary arterial diameter of 3.7 mm and a normal left coronary arterial diameter (2.2 mm), and the fourth with a left coronary arterial diameter of 3.3 mm and a right coronary arterial diameter of 2.7 mm. Fifty-one patients (96%) received IVIG treatment (2 g/kg), and 2 did not (because their fevers subsided spontaneously). Two patients needed re-treatment with IVIG. All patients received low-dose aspirin (3\u20135 mg/kg/day) for 8\u201310 weeks or until their coronary arterial aneurysms subsided",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The mean \u00b1 SD interval between specimen collection and disease onset was 6.1 \u00b1 1.9 days, and the range of the interval was 3\u201311 days (median, 6 days). Specimens were obtained within 10 days of onset of illness for 51 patients (96%). Serum, PBMCs, throat swabs, nasopharyngeal aspirates, and rectal swabs were processed for real-time RT-PCR detection of HCoV-NL63, and the throat swabs, nasopharyngeal aspirates, and rectal swabs were also processed for RT-PCR detection of HCoV-NH. The sample sizes for the tested specimens are shown in table 2. All PCR results were negative for both HCoV-NL63 and HCoV-NH",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 537,
                    "end": 544,
                    "mention": "table 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "\nDiscussionThe novel HCoVs identified in New Haven, Connecticut (HCoV-NH), and The Netherlands (HCoV-NL63) are similar and likely represent the same species [10, 11]. A study by Esper et al. suggested an association between HCoV-NH infection and KD [12], a finding that required confirmation in broader epidemiologic settings, using data from other locations, other years, and other populations, as well as other detection methods [15]. We tested for HCoV-NH and HCoV-NL63 in another location, Taiwan, but failed to confirm the association in 53 patients with KD. Ebihara et al. studied 19 patients with KD in Japan [13], and Belay et al. studied 10 patients with KD in San Diego, California [14]; neither group found an association between HCoV-NH infection and KD, either. Further study is warranted to determine whether this new HCoV is associated with KD",
            "cite_spans": [
                {
                    "start": 158,
                    "end": 160,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 162,
                    "end": 164,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 250,
                    "end": 252,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 432,
                    "end": 434,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 617,
                    "end": 619,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 693,
                    "end": 695,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Identification of the infectious etiology of KD has proved to be very difficult. Since KD was first reported in 1967, >30 years have passed. Despite plenty of hard work by many scientists and physicians worldwide, the causative pathogen has remained unknown. Although Esper et al. reported that HCoV-NH infection is associated with KD [12], we did not find such a correlation. There are several possibilities with respect to the etiology of KD. The disease might be caused by 1 pathogen, or it might be caused by the interaction of >1 pathogen. Furthermore, different pathogens might be responsible for KD in different places or in different seasons. Because recent new evidence has failed to further support the association between HCoV-NH infection and KD, further investigations are required to discover the real causative pathogen of KD and whether it differs in different countries and in different time periods",
            "cite_spans": [
                {
                    "start": 336,
                    "end": 338,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nKawasaki Disease Research GroupThe Kawasaki Disease Research Group of National Taiwan University Hospital, College of Medicine, National Taiwan University, is composed of Ding-Shinn Chen, Chin-Yun Lee, Ih-Jen Su, Bor-Luen Chiang, Li-Min Huang, Mei-Hwan Wu, Ping-Ing Lee, Jou-Kou Wang, Luan-Yin Chang, Chun-Yi Lu, Jin-Town Wang, Chuan-Liang Kao, Po-Ren Hseuh, Sui-Yuan Chang, Hong-Nerng Ho, Pei-Jer Chen, Yao-Hsu Yang, Li-Chieh Wang, and Po-Nien Tsao",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Table 1: Clinical manifestations in 53 patients with Kawasaki disease",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Table 2: Sample sizes of different kinds of specimens assayed for human coronavirus (HCoV)\u2013NL63 by real-time reverse-transcriptase (RT) polymerase chain reaction (PCR) and for HCoV-NH by RT-PCR",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Acute febrile mucocutaneous syndrome with lymphoid involvement with specific desquamation of the fingers and toes in children [in Japanese]",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kawasaki",
                    "suffix": ""
                }
            ],
            "year": 1967,
            "venue": "Arerugi",
            "volume": "16",
            "issn": "",
            "pages": "178-222",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Evidence of a novel human coronavirus that is associated with respiratory tract disease in infants and young children",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Esper",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Weibel",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ferguson",
                    "suffix": ""
                },
                {
                    "first": "ML",
                    "middle": [],
                    "last": "Landry",
                    "suffix": ""
                },
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Kahn",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "191",
            "issn": "",
            "pages": "492-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Identification of a new human coronavirus",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "van der Hoek",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Pyrc",
                    "suffix": ""
                },
                {
                    "first": "MF",
                    "middle": [],
                    "last": "Jebbink",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat Med",
            "volume": "10",
            "issn": "",
            "pages": "368-73",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Association between a novel human coronavirus and Kawasaki disease",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Esper",
                    "suffix": ""
                },
                {
                    "first": "ED",
                    "middle": [],
                    "last": "Shapiro",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Weibel",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ferguson",
                    "suffix": ""
                },
                {
                    "first": "ML",
                    "middle": [],
                    "last": "Landry",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "191",
            "issn": "",
            "pages": "499-502",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Lack of association between New Haven coronavirus and Kawasaki disease",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ebihara",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Endo",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Ma",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Ishiguro",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kikuta",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "192",
            "issn": "",
            "pages": "351-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Kawasaki disease and human coronavirus",
            "authors": [
                {
                    "first": "ED",
                    "middle": [],
                    "last": "Belay",
                    "suffix": ""
                },
                {
                    "first": "DD",
                    "middle": [],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "LJ",
                    "middle": [],
                    "last": "Anderson",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "192",
            "issn": "",
            "pages": "352-3",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Coronaviruses in the limelight",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "McIntosh",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "191",
            "issn": "",
            "pages": "489-91",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Epidemiological features of Kawasaki disease in Taiwan from 1996 to 2002",
            "authors": [
                {
                    "first": "LY",
                    "middle": [],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "IS",
                    "middle": [],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "CY",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Pediatrics",
            "volume": "114",
            "issn": "",
            "pages": "e678-82",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Epidemiologic pictures of Kawasaki disease in Japan: from the nationwide incidence survey in 1991 and 1992",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Yanagawa",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Yashiro",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Nakamura",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Pediatrics",
            "volume": "95",
            "issn": "",
            "pages": "475-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Kawasaki syndrome hospitalizations in the United States, 1997 and 2000",
            "authors": [
                {
                    "first": "RC",
                    "middle": [],
                    "last": "Holman",
                    "suffix": ""
                },
                {
                    "first": "AT",
                    "middle": [],
                    "last": "Curns",
                    "suffix": ""
                },
                {
                    "first": "ED",
                    "middle": [],
                    "last": "Belay",
                    "suffix": ""
                },
                {
                    "first": "CA",
                    "middle": [],
                    "last": "Steiner",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Schonberger",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Pediatrics",
            "volume": "112",
            "issn": "",
            "pages": "495-501",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Kawasaki syndrome in Hawaii",
            "authors": [
                {
                    "first": "RC",
                    "middle": [],
                    "last": "Holman",
                    "suffix": ""
                },
                {
                    "first": "AT",
                    "middle": [],
                    "last": "Curns",
                    "suffix": ""
                },
                {
                    "first": "ED",
                    "middle": [],
                    "last": "Belay",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Pediatr Infect Dis J",
            "volume": "24",
            "issn": "",
            "pages": "429-33",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Incidence of Henoch-Schonlein purpura, Kawasaki disease, and rare vasculitides in children of different ethnic origins",
            "authors": [
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Gardner-Medwin",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Dolezalova",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Cummins",
                    "suffix": ""
                },
                {
                    "first": "TR",
                    "middle": [],
                    "last": "Southwood",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Lancet",
            "volume": "360",
            "issn": "",
            "pages": "1197-202",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Kawasaki disease in Australia, 1993\u201395",
            "authors": [
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Royle",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Williams",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Elliott",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Arch Dis Child",
            "volume": "78",
            "issn": "",
            "pages": "33-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Kawasaki disease in Sweden: incidence and clinical features",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Schiller",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Fasth",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Bjorkhem",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Elinder",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Acta Paediatr",
            "volume": "84",
            "issn": "",
            "pages": "769-74",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Seasonality and temporal clustering of Kawasaki syndrome",
            "authors": [
                {
                    "first": "JC",
                    "middle": [],
                    "last": "Burns",
                    "suffix": ""
                },
                {
                    "first": "DR",
                    "middle": [],
                    "last": "Cayan",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Tong",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Epidemiology",
            "volume": "16",
            "issn": "",
            "pages": "220-5",
            "other_ids": {
                "DOI": []
            }
        }
    }
}