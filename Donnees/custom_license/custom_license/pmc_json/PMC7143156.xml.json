{
    "paper_id": "PMC7143156",
    "metadata": {
        "title": "Intersecting U.S. Epidemics: COVID-19 and Lack of Health Insurance",
        "authors": [
            {
                "first": "Steffie",
                "middle": [],
                "last": "Woolhandler",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [
                    "U."
                ],
                "last": "Himmelstein",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "We estimated the likely effects of current job losses on the number of uninsured persons by using data from the U.S. Census Bureau's March 2019 Current Population Survey on health insurance coverage rates among persons who lost or left a job. The uninsurance rate among unemployed persons who had lost or left a job was 26.3% versus 10.7% among those with jobs. Applying the 15.6\u2013percentage point difference to the 9.955 million who filed new unemployment claims last week, we estimate that 1.553 million newly unemployed persons will lose health coverage. This figure excludes family members who will become uninsured because a breadwinner lost coverage and self-employed persons who may lose coverage because their businesses were shuttered, but are ineligible for unemployment benefits. If, as the Federal Reserve economist projects, an additional 47.05 million people become unemployed, 7.3 million workers (along with several million family members) are likely to join the ranks of the U.S. uninsured population.",
            "cite_spans": [],
            "section": "Estimating Coverage Losses",
            "ref_spans": []
        },
        {
            "text": "Coverage losses are likely to be steepest in states that have turned down the Patient Protection and Affordable Care Act's Medicaid expansion. In expansion states, the share of persons who have lost or left a job who lacked coverage was 22.1% versus 8.3% for employed persons\u2014a difference of 13.8 percentage points. In nonexpansion states, the uninsurance rate among such unemployed persons was 38.4% versus 15.8% for employed persons\u2014a difference of 22.6 percentage points. In other words, nearly 1 in 4 newly unemployed workers in nonexpansion states are likely to lose coverage, bringing their overall uninsurance rate to nearly 40%.",
            "cite_spans": [],
            "section": "Estimating Coverage Losses",
            "ref_spans": []
        },
        {
            "text": "Our projections are based on differences in coverage rates for employed and unemployed persons in 2019, but there is little reason to believe that the predicament of unemployed workers has improved since then. Although many who lose their jobs are likely to be eligible for Medicaid or subsidized Affordable Care Act coverage, and some will purchase continuing coverage under COBRA (Consolidated Omnibus Budget Reconciliation Act), the same was true in 2019. Indeed, the situation may be worse today because some laid-off workers probably gained coverage through an employed spouse in 2019, an option less likely to be available in the face of the impending massive layoffs.",
            "cite_spans": [],
            "section": "Estimating Coverage Losses",
            "ref_spans": []
        },
        {
            "text": "With jobs and health insurance coverage disappearing as the COVID-19 pandemic rages, states that have declined to expand Medicaid should urgently reconsider. Yet, the high uninsurance rate among unemployed persons in Medicaid expansion states underlines the need for action in Washington. Tax revenues are plunging, and all states except Vermont are required to balance their budgets annually. Hence, only the federal government has the wherewithal to address the impending crisis.",
            "cite_spans": [],
            "section": "Urgent Policy Needs and Longer-Term Solutions",
            "ref_spans": []
        },
        {
            "text": "Thus far, neither Congress nor the administration has offered plans to expand coverage. Some have suggested that the federal government cover COVID-19\u2013related care for uninsured persons through Medicaid (2), but some states would probably decline such a Medicaid expansion, leaving many newly jobless persons\u2014and the 28 million who were uninsured before the pandemic\u2014without coverage. Instead, we advocate for passage of an emergency measure authorizing Medicare coverage for all persons eligible for unemployment benefits.",
            "cite_spans": [
                {
                    "start": 204,
                    "end": 205,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Urgent Policy Needs and Longer-Term Solutions",
            "ref_spans": []
        },
        {
            "text": "Although the COVID-19 crisis demands urgent action, it also exposes the imprudence of tying health insurance to employment, and the need for more thoroughgoing reform. A trickle of families facing the dual disaster of job loss and health insurance loss can remain under Washington's radar. However, the current tsunami of job and coverage losses along with a heightened risk for severe illness demands action. A decade ago, Victor Fuchs (3) forecasted that \u201cNational health insurance will probably come to the United States after a major change in the political climate\u2014the kind of change that often accompanies a war, a depression, or large-scale civil unrest.\u201d Such a major change may be upon us.",
            "cite_spans": [
                {
                    "start": 438,
                    "end": 439,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Urgent Policy Needs and Longer-Term Solutions",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}