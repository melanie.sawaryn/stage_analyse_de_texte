{
    "paper_id": "PMC7108079",
    "metadata": {
        "title": "\nEditorial Commentary: Plethora of Respiratory Viruses and Respiratory Virus Data",
        "authors": [
            {
                "first": "Gregory",
                "middle": [
                    "A."
                ],
                "last": "Storch",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "\n(See the Major Article by Byington et al on pages 1217\u201324.)\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In this issue of Clinical Infectious Diseases, Byington et al report on findings from the BIG-LoVE (Better Identification of Germs Longitudinal Viral Epidemiology) study [1]. Based on serial molecular testing for respiratory viruses carried out on a defined population, BIG-LoVE provides a wealth of information about the relationship between respiratory symptoms and respiratory viral infection. The study stands squarely in the tradition of the historic community-based studies of viral respiratory infection, including the Cleveland Family study [2], the Tecumseh study [3], and the New York and Seattle Virus Watches [4, 5]. Those studies, which were based on viral culture and/or serology, established our current conceptual framework for the community epidemiology of viral respiratory tract infection. BIG-LoVE is one of a new generation of community-based studies [6] that \u201cflesh out\u201d that framework by using molecular methods that are more sensitive than previous methods.",
            "cite_spans": [
                {
                    "start": 171,
                    "end": 172,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 550,
                    "end": 551,
                    "mention": "2",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 574,
                    "end": 575,
                    "mention": "3",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 622,
                    "end": 623,
                    "mention": "4",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 625,
                    "end": 626,
                    "mention": "5",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 873,
                    "end": 874,
                    "mention": "6",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "BIG-LoVE resembles the classic studies by being community based, with specimens obtained from study participants on a regular basis, regardless of the presence or absence of symptoms, but the study stands out by virtue of the intensity of virologic and clinical surveillance undertaken. A total of 108 individuals in 26 households from the University of Utah campus community provided symptom diaries and self-collected anterior nasal swab samples weekly for 52 consecutive weeks. Diaries also included information on absence from work or school and on medical visits. To the credit of the investigative team, diaries and specimens were available for analysis from 77% of episodes. Each week was characterized with regard to occurrence of illness based on the symptom diary and presence or absence of respiratory viruses as measured by the molecular test. Illness episodes were defined as continuous periods of presence of symptoms, and virus episodes were defined as continuous periods of detection of the same virus.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "While the study has the significant strengths described above, it also has limitations. The numbers of households and participants were relatively small. In addition, the study was confined to a single community during a single year. With participants being recruited from a university community, the study population was probably more educated and from a higher socioeconomic stratum than the general US population. The molecular test was a developmental version of the FilmArray Respiratory Panel multiplex respiratory panel [7], and the performance characteristics of this test version were not provided. Each of these factors suggests caution in generalizing the results of the findings. Furthermore, molecular detection of viral nucleic acid is not conclusive proof of active infection. With these caveats in mind, the study nevertheless provides highly useful information. Molecular detection is rapidly becoming the major method used for detection of respiratory viruses in clinical settings, and BIG-LoVE provides data that will help clinicians interpret the results of current molecular assays.",
            "cite_spans": [
                {
                    "start": 528,
                    "end": 529,
                    "mention": "7",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Like previous studies [6, 8], BIG-LoVE documents the impressive frequency of respiratory symptoms and infection with respiratory viruses that are present in the community at any given time. Respiratory symptoms were present in 38% of person-weeks for children aged <5 years and 20% of person-weeks for older children and adults. Participants experienced a mean of 5 respiratory illness episodes per year, with more in children aged <5 years. Sixty percent of respiratory illness episodes were associated with detection of 1 or more viruses. Interestingly, virus-positive episodes were no more likely to be associated with missed school and/or work than virus-negative episodes. The illness that occurred was relatively mild, requiring only 2 emergency department visits and no hospitalizations. Viruses were detected in 50% of all person-weeks for children aged <5 years, 25% for children 5\u201317 years, and 16% for adults. Of the virus episodes, the 3 most common viruses were rhinovirus, bocavirus, and coronavirus. Strikingly, these 3 viruses were present in 99% of virus-positive weeks. Rhinovirus was detected at least once in 100 of the 108 study participants. The more highly pathogenic viruses were much less common, with influenza A and respiratory syncytial virus accounting for only 5% and 2% of episodes, respectively. Fifty-six percent of virus-positive episodes and 56% of rhinovirus-positive episodes were associated with symptoms, meaning that 44% were asymptomatic. For other viruses, the proportion of episodes that was symptomatic ranged from 83% for human metapneumovirus to 46% for bocavirus.",
            "cite_spans": [
                {
                    "start": 23,
                    "end": 24,
                    "mention": "6",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 26,
                    "end": 27,
                    "mention": "8",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Some of the most useful data from BIG-LoVE was related to duration of polymerase chain reaction (PCR) positivity during virus-positive episodes. Overall, 83% of virus-positive episodes were PCR positive for less than 2 weeks, and the mean duration of positivity was 1.7 weeks. Among the 17% of episodes that were positive for 3 weeks or longer, 82% were accounted for by rhinovirus or bocavirus. Of the 416 rhinovirus-positive episodes, the mean duration of PCR positivity was less than 2 weeks, but 22% of episodes were positive for 3 weeks or longer. Likewise for bocavirus, the mean duration of positivity was less than 2 weeks, but 14% of 151 episodes remained positive for 3 or more weeks. A limitation of this analysis is that since the rhinoviruses were not typed, it was not possible to distinguish between continuous positivity of a single infection vs 2 or more closely spaced infections with different rhinovirus types. In a recent report by Loeffelholz et al, 14.5% of episodes of rhinovirus positivity in infants aged <1 year were actually consecutive infections with 2 or more different rhinovirus types [9]. Based on the findings from BIG-LoVE, the authors conclude that prolonged PCR positivity in acute respiratory infection may confound the interpretation of a positive PCR test.",
            "cite_spans": [
                {
                    "start": 1119,
                    "end": 1120,
                    "mention": "9",
                    "ref_id": "BIBREF16"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Thus, the findings from BIG-LoVE illustrate a fundamental limitation of pathogen-based diagnosis; namely, that detection of a pathogen does not prove that it is the cause of the patient's illness. While discussions about distinguishing between bacterial colonization and active infection are not new, the recent introduction of sensitive molecular tests for respiratory and gastrointestinal viruses has added a viral dimension to this discussion. In this regard, rhinovirus is especially problematic. This virus is increasingly respected as a legitimate respiratory pathogen that causes lower airway as well as upper airway disease [10] but is also detected frequently in asymptomatic young children [11]. Clinicians receiving reports that their patients are positive for rhinovirus are frequently perplexed about the significance of the finding. Given the high background rate of PCR positivity in asymptomatic individuals, a clinician may not be convinced that rhinovirus is causing the patient's illness and may feel compelled to prescribe an antibiotic in case there is bacterial coinfection.",
            "cite_spans": [
                {
                    "start": 633,
                    "end": 635,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 701,
                    "end": 703,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "An appealing approach to escaping from this conundrum is to query the host response. In an effort to go beyond traditional biomarkers such as C-reactive protein and procalcitonin, several groups have investigated host gene expression in peripheral blood leukocytes and shown differences in response to viral vs bacterial infection or in response to symptomatic vs asymptomatic infection [12\u201316]. Proteomics and metabolomics provide alternative paths for evaluating host response. An approach that combines pathogen detection with characterization of host response may get us closer to the ultimate goal, that is, a test or set of tests that will inform clinicians regarding the need for antibiotic therapy. This capability would empower clinicians to withhold antibiotics when bacterial infection was unlikely and would be the key to lessening the current overuse of antibiotics to treat viral respiratory infections [17].",
            "cite_spans": [
                {
                    "start": 388,
                    "end": 393,
                    "mention": "12\u201316",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 918,
                    "end": 920,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Community surveillance of respiratory viruses among families in the Utah Better Identification of Germs Longitudinal Viral Epidemiology (BIG-LoVE) Study",
            "authors": [
                {
                    "first": "CL",
                    "middle": [],
                    "last": "Byington",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Ampofo",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Stockmann",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Clin Infect Dis",
            "volume": "61",
            "issn": "",
            "pages": "1217-24",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Human rhinoviruses: the cold wars resume",
            "authors": [
                {
                    "first": "IM",
                    "middle": [],
                    "last": "Mackay",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "J Clin Virol",
            "volume": "42",
            "issn": "",
            "pages": "297-320",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Detecting respiratory viruses in asymptomatic children",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Advani",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Sengupta",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Forman",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Valsamakis",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Milstone",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Pediatr Infect Dis J",
            "volume": "31",
            "issn": "",
            "pages": "1221-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Gene expression patterns in blood leukocytes discriminate patients with acute infections",
            "authors": [
                {
                    "first": "O",
                    "middle": [],
                    "last": "Ramilo",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Allman",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Chung",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Blood",
            "volume": "109",
            "issn": "",
            "pages": "2066-77",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Gene expression profiles in febrile children with defined viral and bacterial infection",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "SD",
                    "middle": [],
                    "last": "Crosby",
                    "suffix": ""
                },
                {
                    "first": "GA",
                    "middle": [],
                    "last": "Storch",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "110",
            "issn": "",
            "pages": "12792-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Gene expression signatures diagnose influenza and other symptomatic respiratory viral infections in humans",
            "authors": [
                {
                    "first": "AK",
                    "middle": [],
                    "last": "Zaas",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Varkey",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Cell Host Microbe",
            "volume": "6",
            "issn": "",
            "pages": "207-17",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "A host-based rt-PCR gene expression signature to identify acute respiratory viral infection",
            "authors": [
                {
                    "first": "AK",
                    "middle": [],
                    "last": "Zaas",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Burke",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Sci Transl Med",
            "volume": "5",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Superiority of transcriptional profiling over procalcitonin for distinguishing bacterial from viral lower respiratory tract infections in hospitalized adults",
            "authors": [
                {
                    "first": "NM",
                    "middle": [],
                    "last": "Suarez",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Bunsow",
                    "suffix": ""
                },
                {
                    "first": "AR",
                    "middle": [],
                    "last": "Falsey",
                    "suffix": ""
                },
                {
                    "first": "EE",
                    "middle": [],
                    "last": "Walsh",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mejias",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Ramilo",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Infect Dis",
            "volume": "212",
            "issn": "",
            "pages": "213-22",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Excessive antibiotic use for acute respiratory infections in the United States",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Gonzales",
                    "suffix": ""
                },
                {
                    "first": "DC",
                    "middle": [],
                    "last": "Malone",
                    "suffix": ""
                },
                {
                    "first": "JH",
                    "middle": [],
                    "last": "Maselli",
                    "suffix": ""
                },
                {
                    "first": "MA",
                    "middle": [],
                    "last": "Sande",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Clin Infect Dis",
            "volume": "33",
            "issn": "",
            "pages": "757-62",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "A study of illness in a group of Cleveland families. I. Plan of study and certain general observations",
            "authors": [
                {
                    "first": "JH",
                    "middle": [],
                    "last": "Dingle",
                    "suffix": ""
                },
                {
                    "first": "GF",
                    "middle": [],
                    "last": "Badger",
                    "suffix": ""
                },
                {
                    "first": "AE",
                    "middle": [],
                    "last": "Feller",
                    "suffix": ""
                },
                {
                    "first": "RG",
                    "middle": [],
                    "last": "Hodges",
                    "suffix": ""
                },
                {
                    "first": "WS",
                    "middle": [],
                    "last": "Jordan",
                    "suffix": "Jr"
                },
                {
                    "first": "CH",
                    "middle": [],
                    "last": "Rammelkamp",
                    "suffix": "Jr"
                }
            ],
            "year": 1953,
            "venue": "Am J Hyg",
            "volume": "58",
            "issn": "",
            "pages": "16-30",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "The Tecumseh study of respiratory illness. I. Plan of study and observations on syndromes of acute respiratory disease",
            "authors": [
                {
                    "first": "AS",
                    "middle": [],
                    "last": "Monto",
                    "suffix": ""
                },
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Napier",
                    "suffix": ""
                },
                {
                    "first": "HL",
                    "middle": [],
                    "last": "Metzner",
                    "suffix": ""
                }
            ],
            "year": 1971,
            "venue": "Am J Epidemiol",
            "volume": "94",
            "issn": "",
            "pages": "269-79",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "The Seattle Virus Watch program. I. Infection and illness experience of Virus Watch families during a communitywide epidemic of echovirus type 30 aseptic meningitis",
            "authors": [
                {
                    "first": "CE",
                    "middle": [],
                    "last": "Hall",
                    "suffix": ""
                },
                {
                    "first": "MK",
                    "middle": [],
                    "last": "Cooney",
                    "suffix": ""
                },
                {
                    "first": "JP",
                    "middle": [],
                    "last": "Fox",
                    "suffix": ""
                }
            ],
            "year": 1970,
            "venue": "Am J Public Health Nations Health",
            "volume": "60",
            "issn": "",
            "pages": "1456-65",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "The Virus Watch program: a continuing surveillance of viral infections in metropolitan New York families. II. Laboratory methods and preliminary report on infections revealed by virus isolation",
            "authors": [
                {
                    "first": "I",
                    "middle": [],
                    "last": "Spigland",
                    "suffix": ""
                },
                {
                    "first": "JP",
                    "middle": [],
                    "last": "Fox",
                    "suffix": ""
                },
                {
                    "first": "LR",
                    "middle": [],
                    "last": "Elveback",
                    "suffix": ""
                }
            ],
            "year": 1966,
            "venue": "Am J Epidemiol",
            "volume": "83",
            "issn": "",
            "pages": "413-35",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Community epidemiology of human metapneumovirus, human coronavirus NL63, and other respiratory viruses in healthy preschool-aged children using parent-collected specimens",
            "authors": [
                {
                    "first": "SB",
                    "middle": [],
                    "last": "Lambert",
                    "suffix": ""
                },
                {
                    "first": "KM",
                    "middle": [],
                    "last": "Allen",
                    "suffix": ""
                },
                {
                    "first": "JD",
                    "middle": [],
                    "last": "Druce",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Pediatrics",
            "volume": "120",
            "issn": "",
            "pages": "e929-37",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "FilmArray, an automated nested multiplex PCR system for multi-pathogen detection: development and application to respiratory tract infection",
            "authors": [
                {
                    "first": "MA",
                    "middle": [],
                    "last": "Poritz",
                    "suffix": ""
                },
                {
                    "first": "AJ",
                    "middle": [],
                    "last": "Blaschke",
                    "suffix": ""
                },
                {
                    "first": "CL",
                    "middle": [],
                    "last": "Byington",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "PLoS One",
            "volume": "6",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Symptomatic and asymptomatic respiratory viral infections in the first year of life: association with acute otitis media development",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Chonmaitree",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Alvarez-Fernandez",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Jennings",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Clin Infect Dis",
            "volume": "60",
            "issn": "",
            "pages": "1-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Duration of rhinovirus shedding in the upper respiratory tract in the first year of life",
            "authors": [
                {
                    "first": "MJ",
                    "middle": [],
                    "last": "Loeffelholz",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Trujillo",
                    "suffix": ""
                },
                {
                    "first": "RB",
                    "middle": [],
                    "last": "Pyles",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Pediatrics",
            "volume": "134",
            "issn": "",
            "pages": "1144-50",
            "other_ids": {
                "DOI": []
            }
        }
    }
}