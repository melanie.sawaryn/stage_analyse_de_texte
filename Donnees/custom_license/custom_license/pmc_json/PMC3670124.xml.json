{
    "paper_id": "PMC3670124",
    "metadata": {
        "title": "Design of a set of probes with high potential for influenza virus epidemiological surveillance",
        "authors": [
            {
                "first": "Luis",
                "middle": [
                    "R"
                ],
                "last": "Carre\u00f1o-Dur\u00e1n",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "V",
                "middle": [],
                "last": "Larios-Serrato",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hueman",
                "middle": [],
                "last": "Jaimes-D\u00edaz",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hilda",
                "middle": [],
                "last": "P\u00e9rez-Cervantes",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "H\u00e9ctor",
                "middle": [],
                "last": "Zepeda-L\u00f3pez",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Carlos",
                "middle": [
                    "Javier"
                ],
                "last": "S\u00e1nchez-Vallejo",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Gabriela",
                "middle": [
                    "Edith"
                ],
                "last": "Olgu\u00edn-Ruiz",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rogelio",
                "middle": [],
                "last": "Maldonado-Rodr\u00edguez",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Alfonso",
                "middle": [],
                "last": "M\u00e9ndez-Tenorio",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Influenza viruses are part of Orthomixoviridae Family and\npossess segmented genomes consisting of seven or eight\nseparate RNA molecules, each coding for one or more viral\nproteins. The viruses can exchange segments, leading to\ndiversity of reassortant strains. Together with accumulation of\npoint mutations, segment reassortment is the basis for evolution\nand maintenance of diversity for these viruses. It provides them\nwith the ability to rapidly adapt to the pressure of the host\nimmune system and leads to the continuous emergence of new\nvirus variants that cause seasonal and pandemic outbreaks of\ninfluenza. Because of this ability, segmented viruses can exist in\nnumerous genotypes and serotypes, presenting a challenge to\nthe creation of protective vaccines and detection methods \n[1,\n2].",
            "cite_spans": [
                {
                    "start": 790,
                    "end": 791,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 793,
                    "end": 794,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "Because of these reasons, the early detection and diagnostic\nconfirmation of influenza virus infections is fundamental for an\nappropriate control of the disease. Several molecular biology\ntechniques, most of them based on PCR amplification, have\ncontributed to the diagnostic of the different types and subtypes\nof influenza virus. However, PCR techniques are frequently\nunable to detect new potentially virulent strains. Other\ntechniques such as sequencing are able to perform a precise\nidentification of such strains but still are not so widely available\nfor routinary diagnostic [3,\n4].",
            "cite_spans": [
                {
                    "start": 583,
                    "end": 584,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 586,
                    "end": 587,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "The creation of a microarray is complicated when genomic\nstructures are similar. Probe selection is further complicated\nwhen the number of known sequences is very large. When this\nhappens the probe selection strategy becomes critical \n[5]. There\nare several methods [6\u201312] for the selection of specific probes for\ninfluenza virus detection. Direct search for probes based on\ntraditional computational methods is labor-intensive and often\nrequires plenty of time. The Shannon entropy (H), is a\nbioinformatics technique that has been used to sort the\ninfluenza virus, to analyze the evolution of influenza \n[13], to\nfacilitate the development of an anti-influenza vaccine \n[14], and\nto create a profile of these areas of high variation, observing\ncharacteristic patterns for each subtype [15].",
            "cite_spans": [
                {
                    "start": 236,
                    "end": 237,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 267,
                    "end": 268,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 269,
                    "end": 271,
                    "mention": "12",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 606,
                    "end": 608,
                    "mention": "13",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 672,
                    "end": 674,
                    "mention": "14",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 787,
                    "end": 789,
                    "mention": "15",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "In the present approach we designed and tested in silico, an\nInfluenza Probe Set (IPS) which consists in 1,249 probes with a\nlength of 9mer, extracted from sequence alignment zones with\nmaximum entropy within the full viral genome of over 5,000\nviruses reported, considering almost all viral subtypes of\nInfluenza A. Using Virtual Hybridization (VH) technology, in\nsilico Genomic Fingerprints were generated, which in turn were\ncompared to estimate a phylogeny based on the fingerprint\npairwise distances. Other studies have employed the use of the\nVH technology to create genomic fingerprints for in silico\nclassifying of microorganisms as Human Papillomaviruses [16]\nand bacterial genomes [17].",
            "cite_spans": [
                {
                    "start": 665,
                    "end": 667,
                    "mention": "16",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 692,
                    "end": 694,
                    "mention": "17",
                    "ref_id": "BIBREF16"
                }
            ],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "This program developed in Java, calculates the Shannon\nentropy of aligned sequences. It finds the points having\nmaximum entropy, then, selects 9-mer sequences (the size can\nbe modified by the user), using the point of maximum entropy\nas the 9-mer center.",
            "cite_spans": [],
            "section": "Search Probe: ::: Methodology",
            "ref_spans": []
        },
        {
            "text": "The equation used by SearchProbe to calculate the Shannon\nentropy is: H (n) =-\u03a3 f (i, n) ln f (i, n); Where H (n) is the entropy\nat position n, i represents a residue (in this case there are only\nfour possible options A, C, G and U), f (i, n) is the frequency of\nresidue i in the n position. The information content in position\nn, is defined then as a decrease in uncertainty or entropy in that\nposition. In our particular case, SearchProbe seeks regions with\nmaximum entropy values [18].",
            "cite_spans": [
                {
                    "start": 484,
                    "end": 486,
                    "mention": "18",
                    "ref_id": "BIBREF17"
                }
            ],
            "section": "Search Probe: ::: Methodology",
            "ref_spans": []
        },
        {
            "text": "CalcProbes. This Perl script refines the search of probes using\nthe 9-mer sequences provided by SearchProbe. These sequences\nare subject to the next restrictions: i) Select only sequences\nhaving between 35-65 %G + C (4 or 5), ii) Eliminate 9-mers\nhaving tandem repeats of 2 or 3 nucleotides, iii) Select\nsequences having a minimum of 2 differences between them\nand iv) Chose 9-mer sequences having 34 to 36\u00b0C Tm values.\nTm values were calculated with the thermodynamic Nearest-\nNeighbors (NN) model using SantaLucia parameters [19]. The\nfinal 1,249 9-mer probe set selected by this procedure is the IPS\n(Influenza Probe Set).",
            "cite_spans": [
                {
                    "start": 528,
                    "end": 530,
                    "mention": "19",
                    "ref_id": "BIBREF18"
                }
            ],
            "section": "Search Probe: ::: Methodology",
            "ref_spans": []
        },
        {
            "text": "Virtual Hybridization is a computer program able to predict\nperfect and mismatched target/probe hybridizations under a\nselected Tm cutoff value. The stability of target/probe duplexes\nis calculated with the NN model. This program was used to\ndetermine all the hybridizations occurring between each\nInfluenza virus genome, or control strain, and the IPS. The\ngroup of hybridization signals produced by each viral genome\ncorresponds to its particular fingerprint [20].",
            "cite_spans": [
                {
                    "start": 462,
                    "end": 464,
                    "mention": "20",
                    "ref_id": "BIBREF19"
                }
            ],
            "section": "Virtual Hybridization (VH): ::: Methodology",
            "ref_spans": []
        },
        {
            "text": "Universal Fingerprinting Analysis (UFA) software transforms\ngenomic fingerprints produced by Virtual Hybridization under\nany chosen stringent condition, into images. It also allows\nvisual comparison of any selected pairs of fingerprints,\nproducing spots with specific colors for both distinctive as well\nas for shared hybridization signals. Besides, this tool is able to\ncalculate pairwise distances between pairs of genomic\nfingerprints. From a table of such distances Taxonomic trees\nwere built using the Neighbor -Joining method with the\nprogram MEGA 5 [21].",
            "cite_spans": [
                {
                    "start": 557,
                    "end": 559,
                    "mention": "21",
                    "ref_id": "BIBREF20"
                }
            ],
            "section": "Genomic Fingerprinting Analysis with UFA software: ::: Methodology",
            "ref_spans": []
        },
        {
            "text": "Two types of analysis were performed: I) A Taxonomic tree,\nbased on distances between IPS-Genomic Virtual Hybridization\nfingerprints, comparing several types of Influenza and other\nviruses, was made. II) Overlapped images from selected pairs of\ngenomic fingerprints for strains having: low, medium, or high\ndegree of relatedness, were made. Influenza A /mallard duck/New\nYork/170/1982(H1N2) and Influenza A/Mexico/InDRE4487/200\nwere used as references.",
            "cite_spans": [],
            "section": "Distinction of Influenza strains with the IPS: ::: Methodology",
            "ref_spans": []
        },
        {
            "text": "A database of tested target viral genomes used for the in silico\nexperiments was created. The VH programs conducts a\nrigorous and reliable analysis to find and track all the sites in\neach viral genome where the probe sequences can hybridize\ntaking into account the degree of complementarity between the\nprobe and the recognized site in the target (allowing at least a\nmismatch difference) and the thermodynamic stability between\nthem. The generated information constitutes an in silico\ngenomic fingerprint listing details of the specific sites in each\ntarget DNA where hybridization occurred, the number and\nsequence of the probe that hybridized as well as the free energy\nvalue of the hybridizations and it also provides the sequence of\nthe target site recognized by each probe. A free energy cutoff\nvalue of -9 kcal/mol for 9mer probes was used.",
            "cite_spans": [],
            "section": "Virtual Hybridization: ::: Results & Discussion",
            "ref_spans": []
        },
        {
            "text": "The analysis of the performance of our set of probes to\ndistinguish between several viral sequences the in silico\nevaluation was divided into several steps: A) Viral Family Test:\nTwo kinds of trees were calculated: One derived from the\nalignment of the concatenated fragments calculated with\nClustal X 2.0 and the other resulting from the comparison of\ngenomic fingerprints obtained with the IPS and VH. Both trees\nare derived from 12 different viral genome families including\nParamyxoviridae, Coronaviridae, Picornaviridae, Adenoviridae,\nwhich cause respiratory symptoms similar to influenza and\nseveral family Orthomixoviridae viruses like influenza B,\ninfluenza C, influenza A (H1N1, H1N2, H3N2) Thogotovirus\nand Isavirus are shown in (Figure 1).",
            "cite_spans": [],
            "section": "Evaluation of the genomic fingerprint: ::: Results & Discussion",
            "ref_spans": [
                {
                    "start": 739,
                    "end": 747,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "The comparison of the two trees shows a close correspondence.\nThe tree from genomic fingerprints groups the influenza A\n(H1N1, H1N2 and H3N2) on a branch, influenza B, C and\nIsavirus in another branch and other viral families in other\nclusters. It is noteworthy that Human Respiratory Syncytial\nvirus (which is a Paramyxovirus) was grouped with the\nRhinovirus Human Rhinovirus B, together with Thogotovirus\nin the tree based on genomic fingerprints. Thogotovirus has\nbeen classified as belonging to the Orthomixovirus family,\nalthough other studies make comparisons of Orthomyxoviridae\nPB1 proteins, showing low percentages of amino acid identity\nwhen compared with influenza viruses and Isavirus \n[22,\n23].\nLikewise, influenza virus types A, B and C yielded\ncharacteristic patterns for each virus, so IPS probes allowed\ncreating distinctive fingerprints for each one and create one\nfingerprint characteristic for each virus (Figure 2).",
            "cite_spans": [
                {
                    "start": 699,
                    "end": 701,
                    "mention": "22",
                    "ref_id": "BIBREF21"
                },
                {
                    "start": 703,
                    "end": 705,
                    "mention": "23",
                    "ref_id": "BIBREF22"
                }
            ],
            "section": "Evaluation of the genomic fingerprint: ::: Results & Discussion",
            "ref_spans": [
                {
                    "start": 926,
                    "end": 934,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "B) Hybridization of viral genomes of the same subtype was\ncarried out on two subtypes of Influenza A H1N1, that infect\ndifferent hosts (human and swine), to check if the IPS is able to\ngenerate distinctive genomic fingerprints. The virus A/New\nYork/18/2006/H1N1 causes seasonal influenza whereas the virus\nA/ Swine/Wisconsin/1915/1988/H1N1 infects swine. It is\nhighlighted that the IPS probes generated specific genomic\nfingerprints for each one. This result is very relevant showing\nthat IPS is capable of an appropriated identification when there\nare outbreaks of this disease in humans by strains from animals\nsuch as pigs (Figure 2).",
            "cite_spans": [],
            "section": "Evaluation of the genomic fingerprint: ::: Results & Discussion",
            "ref_spans": [
                {
                    "start": 627,
                    "end": 635,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "C) Comparison of Genomic Fingerprints of two genomes with\nvery high similarity. Overlapping Genomic Fingerprints of\nInfluenza A H1N1 viruses A/M\u00e9xicoindre4487/2009 and\nA/California/04/2009 from the 2009 pandemics are shown in\nFigure 2. It is clear that both viruses are very similar with only\nminor mutations, as expected for viruses from the same\noutbreak. However IPS genomic fingerprints are able to show\nseven differences between then, with five specific probes for\nA/M\u00e9xicoindre4487/2009 H1N1 virus and two for the\nA/California/04/2009. This is very important for molecular\nstudies of influenza because IPS is highly sensitive as to spread\nviruses even those very closer; this will help in the management\nof influenza epidemiology, and not depend on a previous\nsequencing.",
            "cite_spans": [],
            "section": "Evaluation of the genomic fingerprint: ::: Results & Discussion",
            "ref_spans": []
        },
        {
            "text": "Following the established parameters, the set of 1249 highly\nspecific probes (IPS) allowed us to correct typing and subtyping\nof influenza viruses, including human and animal strains, as\nwell as very similar strains. The IPS design based on the\nconstruction of probes from regions of the viral genome with\nmaximum entropy allows a highly sensitive discrimination.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Through an in silico hybridization, the performance of the IPS\nmicroarray was simulated, allows us to know the possible\nbehavior of the probes, and predicting genomic fingerprints of\nthese viruses. Prediction is based in experimentally supported\nthermodynamic models, which suggest that the IPS microarray\nwould be a valuable Influenza diagnosis tool.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Taxonomic trees of 12 viral families including Paramixoviridae, Orthomixoviridae, Coronaviridae, Picornaviridae, Adenoviridae,\nInfluenza A (H1N1, H1N2, H3N2), B and C, and two other Orthomixovirus, Thogotovirus and Isavirus is given (in red). (A)\nFingerprinting Tree, (B) Alignment Tree. It is shown that all the Influenza A virus subtypes were clustered into a single group.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: A) Genomic fingerprints of different influenza viruses and other viral families. Using as reference organism the virus\nInfluenza A A /mallard duck/New York/170/1982(H1N2) (in red) and the Infectious salmon anemia virus(Isavirus), Thogotovirus,\nHuman respiratory syncytial virus(Paramixoviridae), Human rhinovirus B (Picornaviridae), SARS coronavirus (Coronaviridae),\nHuman adenovirus D (Adenoviridae) in green to compare the fingerprints generated , Genomic fingerprints of different viral types\nof influenza virus. Using as reference organism the virus Influenza A A /mallard duck/New York/170/1982(H1N2) (in red) and\nInfluenza B B/Mexico/84/2000 and Influenza C C/Ann Arbor/1/50 (in green) to compare fingerprints B) Genomic fingerprints of\ndifferent viral types of influenza virus. Using as reference organism the virus A/New York/18/2006/H1N1 (in red) and A/\nSwine/Wisconsin/1915/1988/H1N1 (in green) to compare fingerprints; C) Genomic fingerprints of different viral types of influenza\nvirus. Using as reference organism the virus A/Mexico/InDRE4487/2009(H1N1 (in red) and A/California/04/2009 H1N1 (in green) to\ncompare fingerprints.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "AC",
                    "middle": [],
                    "last": "McHardy",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Adams",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "PLoS Pathog",
            "volume": "5",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [
                {
                    "first": "DA",
                    "middle": [],
                    "last": "Steinhauer",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Annu Rev Genet",
            "volume": "46",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2010,
            "venue": "Influenza, N Engl J Med",
            "volume": "362",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [
                {
                    "first": "VA",
                    "middle": [],
                    "last": "Ryabinin",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "PLoS One",
            "volume": "6",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Sengupta",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Clin Microbiol",
            "volume": "41",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "J Clin Microbiol",
            "volume": "39",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Relogio",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Nucleic Acids Res",
            "volume": "30",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [
                {
                    "first": "EE",
                    "middle": [],
                    "last": "Fesenko",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Influenza Other Respi Viruses",
            "volume": "1",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [
                {
                    "first": "ED",
                    "middle": [],
                    "last": "Dawson",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Anal Chem",
            "volume": "78",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [
                {
                    "first": "MB",
                    "middle": [],
                    "last": "Townsend",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Clin Microbiol",
            "volume": "44",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Kessler",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Clin Microbiol",
            "volume": "42",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Clin Microbiol",
            "volume": "45",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pavesi",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Gene",
            "volume": "402",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [
                {
                    "first": "AT",
                    "middle": [],
                    "last": "Heiny",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "PLoS One",
            "volume": "2",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [
                {
                    "first": "WA",
                    "middle": [],
                    "last": "Thompson",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Entropy",
            "volume": "10",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Alfonso",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Rev Latinoam Microbiol",
            "volume": "48",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "",
            "authors": [
                {
                    "first": "JD",
                    "middle": [],
                    "last": "Hueman",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J Microbiol Methods",
            "volume": "87",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "",
            "authors": [
                {
                    "first": "CE",
                    "middle": [],
                    "last": "Shannon",
                    "suffix": ""
                }
            ],
            "year": 1948,
            "venue": "Techn J",
            "volume": "27",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "SantaLucia",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Jr Proc Natl Acad Sci USA",
            "volume": "95",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF19": {
            "title": "",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Casique-Almaz\u00e1n",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Bioinformation",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF20": {
            "title": "",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Tamura",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Kumar",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Mol Biol Evol",
            "volume": "28",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF21": {
            "title": "",
            "authors": [
                {
                    "first": "Bj\u00f8rn",
                    "middle": [],
                    "last": "Kross\u00f8y",
                    "suffix": ""
                },
                {
                    "first": "Curt",
                    "middle": [],
                    "last": "Endresen",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J Virol",
            "volume": "73",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF22": {
            "title": "",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Cottet",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Cortez-San Martin",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "J Virol",
            "volume": "84",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}