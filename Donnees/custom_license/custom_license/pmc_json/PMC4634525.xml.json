{
    "paper_id": "PMC4634525",
    "metadata": {
        "title": "Managing public health crises: the role of models in pandemic preparedness",
        "authors": [
            {
                "first": "Seyed",
                "middle": [
                    "M."
                ],
                "last": "Moghadas",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Nick",
                "middle": [
                    "J."
                ],
                "last": "Pizzi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jianhong",
                "middle": [],
                "last": "Wu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ping",
                "middle": [],
                "last": "Yan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Influenza pandemics have historically been devastating to humanity with significant morbidity, mortality, and socio\u2010economic costs.\n1\n The 1918\u20131919 pandemic, the so\u2010called \u201cmother of all pandemics,\u201d\n2\n was responsible for over 50 million deaths among countless infections worldwide. Today, 40 years after the last pandemic in 1968, the world may be on the brink of another major global pandemic, with a toll that could exceed that of the 1918\u20131919 pandemic.\n3\n While the nature of the next influenza pandemic cannot be predicted with certainty, the identification of strategies to effectively curtail the spread of disease is an unavoidable priority in responding to this global threat. In light of this, the University of Winnipeg hosted a multidisciplinary workshop on the role of models in pandemic preparedness.\n4\n The workshop brought together public health experts, key decision makers, and infectious disease modelers to: (i) identify the strengths and weaknesses of mathematical models, and suggest ways to improve their predictive ability that will ultimately influence policy effectiveness; and (ii) provide an opportunity for the discussion of priority components of a pandemic plan and determine key parameters that affect policy decision making.",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "The first day of this workshop consisted of several outstanding presentations by modelers with the purpose of forging strong links between theory, policy and practice. These included evaluations and model predictions for antiviral strategies and their implications for drug stockpiling; the role of population contact networks in the emergence and spread of drug\u2010resistance; targeting influenza vaccination at specific age groups; optimal control of pandemic outbreaks; and the usefulness of non\u2010pharmaceutical interventions in disease mitigation. Dr. Chris Bowman (Institute for Biodiagnostics, National Research Council Canada) presented the findings of two modeling studies for the management of drug\u2010resistance in the population,\n5\n, \n6\n especially when concerning the scarcity of antiviral supplies. These studies suggest that an adaptive antiviral strategy with conservative initial treatment levels, followed by a timely increase in the scale of drug\u2010use, can minimize the final size of a pandemic while preventing the occurrence of large resistant outbreaks. Dr. Bowman emphasized that the strategic use of drugs may involve decisions for rationing of limited stockpiles and prioritizing high\u2010risk individuals, and therefore ethical considerations should be taken into account for maximum protection of community health. A comparative evaluation of antiviral strategies in homogeneous and heterogeneous population interactions was presented by Dr. Murray Alexander (Institute for Biodiagnostics, National Research Council Canada). He underscored the importance of prolonging the effectiveness of antiviral drugs through an adaptive treatment strategy, in particular for heterogeneous community structure in which the wide\u2010spread of resistance is more likely to take place. These presentations also provided a brief overview of some recent studies carried out by Canadian modelers in the subject of pandemic preparedness.\n7\n, \n8\n, \n9\n, \n10\n, \n11\n\n",
            "cite_spans": [],
            "section": "Model\u2010based policy",
            "ref_spans": []
        },
        {
            "text": "Dr. Babak Pourbohloul (Director, Mathematical Modeling, BC Centre for Disease Control) proposed an important question regarding \u201ca forced marriage\u201d or \u201cnecessity for integration\u201d between mathematical models and public health policy. In his summary of the day, Dr. Pourbohloul acknowledged that the talks were very encouraging and pointed towards integration and development of modeling platforms that could inform policy in Canada. He also highlighted the significant progress evident since the first pandemic meeting in Vancouver, 2005, during which very little could be communicated to policymakers regarding the value of modeling perspectives.",
            "cite_spans": [],
            "section": "Model\u2010based policy",
            "ref_spans": []
        },
        {
            "text": "Dr. Pourbohloul drew attention to various models presented in the workshop, which attest to the fact that we are not lagging behind the current methodology in Canada, but rather are in the forefront.\n5\n, \n6\n, \n7\n, \n8\n, \n9\n, \n10\n, \n11\n However, the central issue is not this, but integration with public health, which is the approach taken by US and UK colleagues for disease modeling and management. A major drawback for Canadian modelers is the lack of appropriate infrastructure, and this calls for investments from healthcare departments and government organizations that could provide modelers with the impetus to continue development of more realistic models. With regard to models used for pandemic planning, we need to critically evaluate their implications for policy implementation. There are two major reasons underlying this evaluation: first, data are limited and prior to the emergence of a novel pandemic strain, it is not possible to study the epidemiological impact of disease or interventions in a real world environment; second, public health authorities would need to be prepared for all the likely scenarios that could influence the outcome of preparedness strategies. Models, by definition, are not supposed to be perfect; approximations are necessary and predictions are made on this understanding. However, a more important question is how much of the knowledge of Canadian modelers has been employed to support policy decision\u2010making? Is it all based upon experience of other countries? Perhaps in Canada, there has not been much communication between modelers and policymakers and therefore modeling results have not been translated into the context of public health. The time has now come to build a pandemic consortium in Canada to have a unified voice from modelers, and close the gaps with infectious disease experts and public health colleagues.",
            "cite_spans": [],
            "section": "Model\u2010based policy",
            "ref_spans": []
        },
        {
            "text": "Dr. Susan Tamblyn (Co\u2010chair, Canadian Pandemic Antivirals Working Group) also emphasized the importance of making progress on linking the modeling with decision making within Canada. These enterprises are still really separate in Canada, whereas the value of modeling groups working very closely with the government and health departments is clearly evident in a few countries. We seem to have this linkage in a couple of provinces in Canada, but it is not elevated to the national level. As planners, they understand that modeling can help formulate pandemic policies; however, the lack of collaboration with Canadian modelers obliged them to turn to outside results from published models. Hopefully, the two groups can work closer together to have beneficial impact with regards to pandemic preparedness. Dr. Tamblyn also expressed her concern about public health questions, which often are not amenable to modeling, and about modeling studies that use unrealistic assumptions and scenarios. Therefore, modelers should also be fully engaged in the process of formulating the questions that policymakers need to address in planning for a pandemic. The point was highlighted by Dr. Ping Yan (Centre for Communicable Disease and Infection Control, Public Health Agency of Canada) that models should be based on realistic assumptions to create fundamental knowledge in all aspects of pandemic research.",
            "cite_spans": [],
            "section": "Model\u2010based policy",
            "ref_spans": []
        },
        {
            "text": "On the second day, the workshop comprised several presentations by participants from the public health domain. These included unanswered questions concerning the emergence of novel infectious diseases; understanding the space\u2010time dynamics of influenza spread; influenza mortality in pandemics and seasonal outbreaks; the impact of global air transportation on the spread of diseases; the role of models in public health planning and decision making; the evolution of pandemic influenza viruses; and the potential for novel means to prevent these pandemics. Dr Julien Arino (University of Manitoba) outlined the objectives of an ongoing data\u2010driven project that aims to draw out the likely patterns of disease spread through the network of all international airports in the world with direct and indirect connections. This investigation can have important implications for heading off a global pandemic, with a particular focus on the optimal allocation of containment resources in the most probable ports of disease introduction and spread in Canada. This presentation was followed by an overview of the Ontario Government\u2019s Pandemic Preparedness Plan (Allison Stuart, Assistant Deputy Minister of the Ontario Ministry of Health and Long\u2010Term Care), which provides the most comprehensive provincial plan in Canada, having undergone five iterations developed over a 5\u2010year period. This plan details guidance to local planners and specific strategies for health sector sub\u2010groups (critical care, pediatrics, laboratories, long\u2010term care, persons with chronic diseases, mental health settings), first responders, faith groups, private sector organizations, and First Nations communities. This presentation also included a list of concerns which modeling should address relating to acute care services (e.g., estimated hospital surge capacity for a given jurisdiction during a pandemic); local implementation (e.g., identification of the tipping point when primary care will not be able to meet the 24\u201348 hour standard of care); and antivirals (e.g., identifying the optimal use of drugs and distribution methods for treatment and prophylaxis to decelerate the spread of a pandemic). Dr. Joanne Langley (Co\u2010chair, Canadian Pandemic Vaccine Working Group) presented a detailed analysis of the potential benefits and uncertainties relating to the standard pillars of pandemic influenza contingency plans, covering antiviral drugs; healthcare delivery planning; vaccines; public health measures; and infection control practices. This included the importance of personal protective equipment such as the N95 mask in the healthcare setting, the need for regular and frequent hand washing, and a risk analysis of potential amantadine resistance. Dr. Langley also stressed the need for \u201creal time\u201d modeling to provide a rapid analysis of alternative tactical decisions following the onset of a pandemic. Dr. Mark Walderhaug (Associate Director, US Center for Biologics Evaluation and Research, FDA) discussed a stock\u2010and\u2010flow model used for simulating the impact of an influenza pandemic on the US blood supply. The model assumes that susceptibility to the pandemic virus will be universal; multiple waves of infection can occur and each wave adversely impacts infected communities for 6\u20138 weeks; and absenteeism may reach as high as 40% during the peak periods. Model simulations for the entire US blood supply were presented, and the need for acquiring detailed data of inter\u2010regional flow of blood was emphasized. These data are essential for projecting various scenarios, including run\u2010out for hospitals despite adequate national supplies and time frames for elective surgery cancellations while the blood supply recovers, which highlight the significant challenges involved in supply distribution.",
            "cite_spans": [],
            "section": "Model\u2010based policy",
            "ref_spans": []
        },
        {
            "text": "Dr. Paul Gully (Senior Advisor, World Health Organization) emphasized the fact that models are essential for guiding public health, but may also raise more questions for policymakers. He expressed growing concerns about being able to fulfill the requirements for pandemic containment that come from modeling studies: \u201cmodels lead to policy but have to confront political reality\u201d. Previous work suggests that a nascent influenza pandemic can be contained at the source if antiviral therapy for a sizable proportion of affected individuals (80\u201390%) is accompanied by a rapid implementation of non\u2010pharmaceutical measures (such as movement restriction) over a very short period of time (days to 3 weeks).\n12\n, \n13\n On serious discussions from a political standpoint, Dr. Gully demonstrated the significant challenges involved in building the capacity for a timely response to meet the condition for averting a global pandemic. Despite these challenges, he acknowledged that models are invaluable tools for making assumptions explicit and for best using limited data, highlighting key factors determining policy needs, and providing quantitative predictions.",
            "cite_spans": [],
            "section": "Model\u2010based policy",
            "ref_spans": []
        },
        {
            "text": "Discussions of the day were then expanded to the implementation of various strategies from a transmission dynamic standpoint. In their capacity, what models offer should be taken along with other health and economic factors to guide sound public health policies. They are not meant to make decisions on managing public health crises, but rather provide recommendations to policymakers. However, for rapid decision making, one would need to consider the interface between simple, interactive, and relatively complex models that may encapsulate population demographics pertaining to the location of a pandemic outbreak.",
            "cite_spans": [],
            "section": "Model\u2010based policy",
            "ref_spans": []
        },
        {
            "text": "Dr. Tamblyn chaired the summary and discussion session of the workshop on day 3, and acknowledged the true interdisciplinary nature of the meeting, enriched discussions, very interesting and relevant presentations, with kudos for planning long health\u2010breaks that allowed for interactions and flow of emerging ideas. She distinguished the meeting as the one that has met its objectives and provided an opportunity for effective communications between modelers and public health authorities on the subject of pandemic preparedness in Canada.",
            "cite_spans": [],
            "section": "Synergies between modelers and public health",
            "ref_spans": []
        },
        {
            "text": "Dr. Ying\u2010Hen Hsieh (China Medical University, Taiwan) offered his perspectives on the workshop with great potential for expanding collaboration with Canadian colleagues in future work. The meeting highlighted important aspects of Canadian public health that will be useful for creating an effective venue to communicate with public health in Taiwan. Dr. Hsieh, as a prominent modeler in Taiwan, shared his experience with SARS (severe acute respiratory syndrome) and exemplified the opportunities missed by public health to engage modelers: \u201cby the time they called me in, it was 2 weeks before the end of SARS outbreaks\u201d. In 2005, there was a cabinet agreement to promote an influenza vaccine R&D program in Taiwan, partly for the economic opportunities it offers; he was brought in after the decision was made with the hope that \u201cmodelling results will be in line with government policy\u201d. He depicted that in public health in Taiwan, a highly challenging task has been to establish collaborative efforts, but the important lesson from this workshop is to understand the process of making decisions, identify its key parameters, and determine effective ways to communicate with policymakers.",
            "cite_spans": [],
            "section": "Synergies between modelers and public health",
            "ref_spans": []
        },
        {
            "text": "Dr. Benjamin Ridenhour (US Center for Disease Control) acknowledged that the workshop had been successful in bringing together the communities involved in pandemic preparedness, to share their various viewpoints and expertise in modeling and public health, in a very congenial and friendly environment. The US Center for Disease Control has made substantial efforts to co\u2010ordinate pandemic activities through synergism between public health officials and modelers, which has led to benefits for planning strategies in the United States. As modelers, we need to strengthen our ties to public health, and exploit our potential for developing models that can inform and optimize health policy decisions. This workshop has demonstrated that strong networking is required to adequately prepare for the pressure of real time crises, and cope with surging demands in a pandemic\u2010related emergency.",
            "cite_spans": [],
            "section": "Synergies between modelers and public health",
            "ref_spans": []
        },
        {
            "text": "In closing the workshop, Dr. Seyed Moghadas (Institute for Biodiagnostics, National Research Council Canada) valued the time and efforts of participants and appreciated their contributions to the success of this event. Key points inferred from presentations and discussions include:",
            "cite_spans": [],
            "section": "Concluding remarks",
            "ref_spans": []
        },
        {
            "text": "\n1In Canada, the pandemic goals are to (i) minimize serious illness and overall deaths; and (ii) minimize social disruption. Pandemic containment has not been a priority to date and may not be feasible.2Development of a pandemic vaccine may take up to 6 months following pandemic detection. However, as novel influenza strains most often emerge in Asia, strong surveillance leading to early detection there can increase our lead time for pandemic vaccine production.3Immunization of children can result in significant changes in contact patterns and attack rates. Age is a surrogate for individual behavior that influences pathogen transmission in the population; vaccine efficacy may also vary in different age groups.4Antiviral therapy is the cornerstone of the pandemic response in Canada until vaccine is available; however, implementation of the strategy is determined by pandemic planners at the provincial level.\n",
            "cite_spans": [],
            "section": "Concluding remarks",
            "ref_spans": []
        },
        {
            "text": "The meeting provided an opportunity for modelers to engage in detailed discussions about modeling strategies that can be employed for gaining new insight into disease processes at the population level and making findings of public health significance. While models serve to synthesize data and suggest optimal scenarios in public health, they can also promote dialogue between modelers and policymakers about alternatives, uncertainties, and assumptions that underlie critical decisions. The workshop revealed that pandemic planning requires involvement of communities across disciplines with firm commitment to the notion that research must ultimately influence policy.",
            "cite_spans": [],
            "section": "Concluding remarks",
            "ref_spans": []
        },
        {
            "text": "The authors declare that they have no competing interests.",
            "cite_spans": [],
            "section": "Competing interests",
            "ref_spans": []
        },
        {
            "text": "SM, NP, JW, and PY proposed and organized the workshop. SM summarized and drafted the preliminary version of this manuscript based on presentations and round\u2010table discussions. All the authors have contributed to this manuscript, and approved its final version.",
            "cite_spans": [],
            "section": "Authors\u2019 contributions",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "A history of influenza",
            "authors": [],
            "year": 2001,
            "venue": "J Appl Microbiol",
            "volume": "91",
            "issn": "",
            "pages": "572-579",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "The impact of prophylaxis of healthcare workers on influenza pandemic burden",
            "authors": [],
            "year": 2007,
            "venue": "J R Soc Interface",
            "volume": "4",
            "issn": "",
            "pages": "727-734",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Management of drug\u2010resistance in the population: influenza as a case study",
            "authors": [],
            "year": 2008,
            "venue": "Proc Biol Sci",
            "volume": "275",
            "issn": "",
            "pages": "1163-1169",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Strategies for containing an emerging influenza pandemic in Southeast Asia",
            "authors": [],
            "year": 2005,
            "venue": "Nature",
            "volume": "437",
            "issn": "",
            "pages": "209-214",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Containing pandemic influenza at the source",
            "authors": [],
            "year": 2005,
            "venue": "Science",
            "volume": "309",
            "issn": "",
            "pages": "1083-1086",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "1918 influenza: the mother of all pandemics",
            "authors": [],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "15-22",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Avian influenza H5N1: is it a cause for concern?",
            "authors": [],
            "year": 2006,
            "venue": "Intern Med J",
            "volume": "36",
            "issn": "",
            "pages": "145-147",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Workshop on managing public health crises",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Population\u2010wide emergence of antiviral resistance during pandemic influenza",
            "authors": [],
            "year": 2008,
            "venue": "PLoS ONE",
            "volume": "3",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Antiviral resistance during pandemic influenza: implications for stockpiling and drug use",
            "authors": [],
            "year": 2009,
            "venue": "BMC Infect Dis",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Emergence of drug\u2010resistance: implications for antiviral control of pandemic influenza",
            "authors": [],
            "year": 2007,
            "venue": "Proc R Soc B",
            "volume": "274",
            "issn": "",
            "pages": "1675-1684",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "A delay differential model for pandemic influenza with antiviral treatment",
            "authors": [],
            "year": 2007,
            "venue": "Bull Math Biol",
            "volume": "70",
            "issn": "",
            "pages": "382-397",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Simple models for containment of a pandemic",
            "authors": [],
            "year": 2006,
            "venue": "J R Soc Interface",
            "volume": "3",
            "issn": "",
            "pages": "453-457",
            "other_ids": {
                "DOI": []
            }
        }
    }
}