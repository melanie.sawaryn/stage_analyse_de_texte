{
    "paper_id": "PMC3373055",
    "metadata": {
        "title": "Coccidioidomycosis as a Common Cause of Community-acquired Pneumonia",
        "authors": [
            {
                "first": "Lisa",
                "middle": [],
                "last": "Valdivia",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [],
                "last": "Nix",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mark",
                "middle": [],
                "last": "Wright",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Elizabeth",
                "middle": [],
                "last": "Lindberg",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Timothy",
                "middle": [],
                "last": "Fagan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Donald",
                "middle": [],
                "last": "Lieberman",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "T'Prien",
                "middle": [],
                "last": "Stoffer",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Neil",
                "middle": [
                    "M."
                ],
                "last": "Ampel",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "John",
                "middle": [
                    "N."
                ],
                "last": "Galgiani",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Patients were recruited from 3 primary care sites in Tucson, Arizona: the Urgent Care Center of University Medical Center and 2 of the medical offices of Arizona Community Physicians. Recruitment was conducted during 2 time periods: from December 1, 2003, through February 21, 2004, and from May 1, 2004, through August 14, 2004. Although we sought to enroll as many eligible persons as possible during these periods, study personnel were not always available to do so.",
            "cite_spans": [],
            "section": "Study Sample ::: Methods",
            "ref_spans": []
        },
        {
            "text": "To be eligible for enrollment, patients had to exhibit a lower respiratory syndrome of <1 month's duration that included >1 of the following: pleuritic chest pain, dyspnea on exertion, having an evaluation by a chest radiograph, multiple visits for the same respiratory problem, or administration of an antibacterial drug for presumed CAP. Patients were excluded from enrollment if they had a previously diagnosed, laboratory-confirmed coccidioidal infection, another laboratory-confirmed diagnosis for inclusion-defining illness, were <18 years of age, or had not had previous exposure >1 week in a disease-endemic area. Fewer than 5% of the patients offered enrollment refused to participate in the study.",
            "cite_spans": [],
            "section": "Study Sample ::: Methods",
            "ref_spans": []
        },
        {
            "text": "This was an observational study, and medical management of each patient's condition remained entirely with the responsible clinician. After informed consent was obtained, persons were interviewed and their clinical records were reviewed to collect information regarding demographics, comorbid conditions, time ranges of exposure in a disease-endemic region, and recent antimicrobial therapy for current respiratory illness. Persons were asked to complete the Medical Outcomes Study 36-Item Short Form Health Survey (SF-36) (9), the Iowa Fatigue Scale (10), and a respiratory infection severity scale (11). Results from chest radiographs and complete blood counts, where obtained as part of routine medical care, were also recorded. A second visit was scheduled for all persons.",
            "cite_spans": [
                {
                    "start": 524,
                    "end": 525,
                    "mention": "9",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 552,
                    "end": 554,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 601,
                    "end": 603,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Study Protocol ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Serum samples were obtained at both visits. They were stored at \u201370\u00b0C until tested at the completion of the study. Persons identified as having a coccidioidal infection were also contacted during or within the next 6 months to determine the status of their illness.",
            "cite_spans": [],
            "section": "Study Protocol ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Anti-coccidioidal antibodies were measured in the laboratory of 1 of the authors (J.N.G.) by several conventional methods. The double immunodiffusion technique was used to measure tube precipitin\u2013type and complement fixing\u2013type anti-coccidioidal antibodies (12,13). Serum samples qualitatively positive for complement fixing\u2013type anti-coccidioidal antibodies were retested quantitatively (14). Anti-coccidioidal immunoglobulin M (IgM) and IgG antibodies were measured by enzyme-linked immunoassay by using a commercial kit according to the manufacturer's instructions (Coccidioides EIA-Gold, Meridian Diagnostics, Cincinnati OH, USA) (15). An optical density >0.20 was considered positive. The relative sensitivity of these tests in patients with coccidioidal pneumonia has been previously analyzed (15).",
            "cite_spans": [
                {
                    "start": 258,
                    "end": 260,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 261,
                    "end": 263,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 389,
                    "end": 391,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 635,
                    "end": 637,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 800,
                    "end": 802,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Serologic Analysis ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Patient and laboratory data were entered into a database (Access 2003, Microsoft Corp., Bellingham WA, USA), and statistical analysis was accomplished with SAS version 8.2 (SAS Institute, Inc., Cary, NC, USA). Differences between groups for categorical variables were compared with the \u03c72 test and those for continuous variables were compared with the Wilcoxon sign-rank test and Wilcoxon rank-sum test as appropriate. Differences with p values <0.05 were considered significant.",
            "cite_spans": [],
            "section": "Statistical Analysis ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Of the 55 persons who provided serum samples at baseline, 19 provided second serum samples 10\u201340 days later (median 18 days). Of these persons, 16 were positive by >1 serologic test (29%, 95% confidence interval [CI] 16%\u201344%). At baseline, 12 (75%) of the 16 were positive by multiple assays, and all but 3 had positive results for multiple serologic tests for both serum samples (Table 2). Of the remaining 36 persons, all had negative results for all tests at baseline, and a second serum sample obtained from 12 persons was also nonreactive.",
            "cite_spans": [],
            "section": "Frequency of Coccidioidomycosis as a Cause of CAP ::: Results",
            "ref_spans": [
                {
                    "start": 381,
                    "end": 388,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Demographic results for persons with and without valley fever are shown in Table 1. Length of exposure in the disease-endemic area was significantly shorter for patients with valley fever than for those who were seronegative (p = 0.043). The odds ratio for developing coccidioidomycosis in persons with exposure of <10 years to a disease-endemic area compared to those with a longer exposure time was 4.11 (95% CI 1.01\u201316.8). No other significant demographic differences were identified between the 2 groups. Twenty, 24, and 11 participants had age ranges of <40 years, 40\u201364 years, and >65 years, respectively. The percentage of participants in each of these age groups with valley fever was 30%, 29%, and 27%, respectively.",
            "cite_spans": [],
            "section": "Comparison of Clinical Characteristics between Groups ::: Results",
            "ref_spans": [
                {
                    "start": 75,
                    "end": 82,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Respiratory, systemic, and musculoskeletal symptoms are shown in Table 3. Only myalgia showed a significant difference between the 2 groups. The SF-36 survey and respiratory infection severity scale did not identify additional differences. However, the Iowa Fatigue Scale survey median productivity domain score (maximum score = 10 indicates greatest reduction) was 7 for persons with valley fever compared with 4.5 for all other persons (p = 0.008).",
            "cite_spans": [],
            "section": "Comparison of Clinical Characteristics between Groups ::: Results",
            "ref_spans": [
                {
                    "start": 65,
                    "end": 72,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The proportion of patients in whom a chest radiograph was obtained was similar between seropositive and seronegative patients (75% vs. 72%). However, for those from whom chest radiographs were obtained (n = 40), abnormalities were significantly more frequent in participants with valley fever (75% vs. 25%, p = 0.005). Radiographic abnormalities associated with coccidioidal infection included pulmonary infiltrates in 88% and hilar adenopathy in 1 participant.",
            "cite_spans": [],
            "section": "Comparison of Clinical Characteristics between Groups ::: Results",
            "ref_spans": []
        },
        {
            "text": "Of the 55 persons who were seen in the primary care setting, 46 (84%) were prescribed antimicrobial medications; 13 received 2 consecutive courses of treatment, and 1 received 3 consecutive courses of treatment. No differences were seen between the seropositive and seronegative groups in either the proportion treated with antimicrobial drugs (81% vs. 85%) or the proportion treated with multiple courses of drugs (31% vs. 26%).",
            "cite_spans": [],
            "section": "Use of Antimicrobial Drugs ::: Results",
            "ref_spans": []
        },
        {
            "text": "All persons improved in the 6 months after enrollment. Only 1 received specific antifungal therapy (oral fluconazole for 1 month), and none required hospitalization. Eleven persons with valley fever repeated the symptom survey and the Iowa Fatigue Scale a median of 22 days after enrollment. Their responses indicated significant improvement for cough (p<0.016), fatigue (p<0.0039), cognition (p = 0.016), energy (p = 0.0015), and productivity (p = 0.062). Similar improvements were noted with the SF-36 survey and the Iowa Fatigue Scale readministered to 10 persons at the 6-month follow-up visit.",
            "cite_spans": [],
            "section": "Follow-up of Persons with Valley Fever ::: Results",
            "ref_spans": []
        },
        {
            "text": "Of the patients enrolled in our prospective study from select ambulatory care settings within the disease-endemic region, 29% were diagnosed serologically as having coccidioidomycosis. Even if one takes into account the wide 95% CI (16%\u201344%), this number demonstrates a high proportion of CAP caused by this infection. Furthermore, although the serologic tests used for diagnosis are highly specific for coccidioidal infection (12,13,16\u201318), another study emphasized that in the first weeks of primary illness these tests frequently show negative results (15). In the current study, 1 of 13 participants not initially serologically positive had coccidioidal antibodies in a second serum sample. Twenty-seven persons serologically negative at enrollment did not return for retesting; thus, additional coccidioidal infections may have been identified in this group. Our results will likely provide an underestimate of the incidence in this group of patients, further strengthening the conclusion that valley fever is a common cause of CAP in persons exposed to Coccidiodes in a disease-endemic area.",
            "cite_spans": [
                {
                    "start": 428,
                    "end": 430,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 431,
                    "end": 433,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 434,
                    "end": 436,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 437,
                    "end": 439,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 556,
                    "end": 558,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The high frequency of valley fever as a cause of CAP found in this study is consistent with previous estimates of coccidioidomycosis as a dominant cause of CAP with exposure in disease-endemic areas. A similar estimate of 25% to 30% has been obtained retrospectively at the Southern Arizona Veterans Administration Health Care System in Tucson, Arizona (7). Conversely, a diagnosis of valley fever requires laboratory testing. That this practice may not be uniform among clinicians was shown in a retrospective analysis of physician-specific diagnoses at primary care clinics in Tucson, Arizona, in which the rate of diagnosing coccidioidomycosis varied between 0% and 25% among physicians within the same group practice (7). Similar differences might also account for the increasing case rate associated with patient age that was reported in a recent analysis of 2001 Arizona state statistics (8). Case rates for persons >44 years of age were nearly twice those for persons 21\u201344 years of age. In our study in which all persons were uniformly evaluated for valley fever, all age groups had similar rates (27.3%\u201330.0%). Furthermore, although not detailed in our results, severity of illness in terms of respiratory symptoms was less in elderly subjects. We interpret the differences between the state statistics and those of our study as indicating that older persons who develop an illness are more likely to have an exact diagnosis determined, underscoring underreporting of illness in some patient groups such as young adults.",
            "cite_spans": [
                {
                    "start": 354,
                    "end": 355,
                    "mention": "7",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 722,
                    "end": 723,
                    "mention": "7",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 895,
                    "end": 896,
                    "mention": "8",
                    "ref_id": "BIBREF17"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "A corollary to the high frequency of coccidioidomycosis seen in this study is that persons anywhere with CAP and a history of recent travel to south-central Arizona or other regions where coccidioidomycosis is highly endemic would be expected to have a similarly high risk. For this reason, obtaining a travel history for any patient with CAP is essential for early and accurate diagnosis of this disease, as well as for other regional problems such as severe acute respiratory syndrome (SARS), hantavirus pneumonia, and avian influenza. Although the Infectious Diseases Society of America practice guidelines for CAP currently recommend obtaining a complete travel history only in patients with refractory pneumonia (19), we recommend that the guidelines be revised to recommend obtaining a travel history at the first evaluation.",
            "cite_spans": [
                {
                    "start": 718,
                    "end": 720,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Analysis of multiple symptoms at baseline showed several characteristics associated with coccidioidal infections. Both a shorter length of exposure in a disease-endemic region and a greater frequency of radiographic abnormalities were seen in persons with valley fever compared with those without valley fever. These associations were also evident in a previous report from a university health center (5). Symptoms of myalgia and reduced productivity were also evident with coccidioidal infection. However, none of these associations, alone or in combination, were of a sufficient magnitude to assist clinicians in the initial diagnosis. Therefore, our findings, as in the previous study (5), emphasize that laboratory testing at the initial physician visit is essential to identify patients with symptoms of CAP that are caused by valley fever.",
            "cite_spans": [
                {
                    "start": 402,
                    "end": 403,
                    "mention": "5",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 689,
                    "end": 690,
                    "mention": "5",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "A high proportion (81%) of persons with valley fever were prescribed an initial course of antimicrobial drugs. Of these, 12 patients, 3 of whom were diagnosed with valley fever, received 2 courses of these drugs. Although diagnosis of valley fever by serologic methods is frequently delayed by 3\u20135 days, use of antimicrobial drugs could still be avoided or stopped earlier in patients whose illness is determined to be caused by Coccidioides species.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The inclusion criteria used in this study were designed both to be broadly inclusive and to select patients with more severe illness. As such, they differ in some respects from commonly used entry criteria for clinical trials of new antimicrobial drugs as treatment for CAP. For example, we chose pleuritic chest pain and dyspnea at rest and fever as an entry requirement. By using these entry criteria, we found that 3 of the 12 patients with valley fever who underwent radiographic examination had normal radiographs, which is consistent with results of a previous study (3), but did not adhere to Infectious Diseases Society of America or American Lung Association definitions of pneumonia (19). When comparing our findings to those of other studies, the way in which patients were selected should be taken into account.",
            "cite_spans": [
                {
                    "start": 574,
                    "end": 575,
                    "mention": "3",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 694,
                    "end": 696,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Several limitations of our methods deserve emphasis. Because our inclusion criteria were not standard, comparison of our results to those of other studies of CAP is difficult. Also, we did not have a diagnosis for patients without valley fever. Since this is a relatively small study, additional expanded studies may be useful, especially to extend observations to other groups such as children, the elderly, those requiring hospitalization, and residents elsewhere within disease-endemic regions. Future studies are also needed to determine best practices for management of primary coccidioidal infection and possible therapy with specific antifungal treatment. Such a high proportion of CAP caused by Coccidioides species should provide further impetus to conduct those studies.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Epidemiology of coccidioidomycosis.",
            "authors": [],
            "year": 1988,
            "venue": "Curr Top Med Mycol",
            "volume": "2",
            "issn": "",
            "pages": "199-238",
            "other_ids": {
                "DOI": [
                    "10.1007/978-1-4612-3730-3_6"
                ]
            }
        },
        "BIBREF1": {
            "title": "The fatigue severity scale. Application to patients with multiple sclerosis and systemic lupus erythematosus.",
            "authors": [],
            "year": 1989,
            "venue": "Arch Neurol",
            "volume": "46",
            "issn": "",
            "pages": "1121-3",
            "other_ids": {
                "DOI": [
                    "10.1001/archneur.1989.00520460115022"
                ]
            }
        },
        "BIBREF2": {
            "title": "Measuring symptomatic and functional recovery in patients with community-acquired pneumonia.",
            "authors": [],
            "year": 1997,
            "venue": "J Gen Intern Med",
            "volume": "12",
            "issn": "",
            "pages": "423-30",
            "other_ids": {
                "DOI": [
                    "10.1046/j.1525-1497.1997.00074.x"
                ]
            }
        },
        "BIBREF3": {
            "title": "The use of immunodiffusion tests in coccidioidomycosis. II. An immunodiffusion test as a substitute for the tube precipitin test.",
            "authors": [],
            "year": 1965,
            "venue": "Tech Bull Regist Med Technol",
            "volume": "35",
            "issn": "",
            "pages": "155-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "The use of immunodiffusion tests in coccidioidomycosis. I. The accuracy and reproducibility of the immunodiffusion test which correlates with complement fixation.",
            "authors": [],
            "year": 1965,
            "venue": "Tech Bull Regist Med Technol",
            "volume": "35",
            "issn": "",
            "pages": "150-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Comparison of immunodiffusion techniques with standard complement fixation assay for quantitation of coccidioidal antibodies.",
            "authors": [],
            "year": 1983,
            "venue": "J Clin Microbiol",
            "volume": "18",
            "issn": "",
            "pages": "529-34",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Detection of coccidioidal antibodies by 33-kDa spherule antigen, Coccidioides EIA, and standard serologic tests in sera from patients evaluated for coccidioidomycosis.",
            "authors": [],
            "year": 1996,
            "venue": "J Infect Dis",
            "volume": "173",
            "issn": "",
            "pages": "1273-7",
            "other_ids": {
                "DOI": [
                    "10.1093/infdis/173.5.1273"
                ]
            }
        },
        "BIBREF7": {
            "title": "Pattern of 39,500 serologic tests in coccidioidomycosis.",
            "authors": [],
            "year": 1956,
            "venue": "JAMA",
            "volume": "160",
            "issn": "",
            "pages": "546-52",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.1956.02960420026008"
                ]
            }
        },
        "BIBREF8": {
            "title": "Serology of coccidioidomycosis.",
            "authors": [],
            "year": 1990,
            "venue": "Clin Microbiol Rev",
            "volume": "3",
            "issn": "",
            "pages": "247-68",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 1958,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Community-acquired pneumonia in adults: guidelines for management. The Infectious Diseases Society of America.",
            "authors": [],
            "year": 1998,
            "venue": "Clin Infect Dis",
            "volume": "26",
            "issn": "",
            "pages": "811-38",
            "other_ids": {
                "DOI": [
                    "10.1086/513953"
                ]
            }
        },
        "BIBREF11": {
            "title": "Coccidioidomycosis: the initial pulmonary infection and beyond.",
            "authors": [],
            "year": 1997,
            "venue": "Semin Respir Crit Care Med",
            "volume": "18",
            "issn": "",
            "pages": "235-47",
            "other_ids": {
                "DOI": [
                    "10.1055/s-2008-1070993"
                ]
            }
        },
        "BIBREF12": {
            "title": "Coccidioidomycosis at a university health service.",
            "authors": [],
            "year": 1985,
            "venue": "Am Rev Respir Dis",
            "volume": "131",
            "issn": "",
            "pages": "100-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [],
            "year": 1985,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Symptoms and routine laboratory abnormalities associated with coccidioidomycosis.",
            "authors": [],
            "year": 1988,
            "venue": "West J Med",
            "volume": "149",
            "issn": "",
            "pages": "419-21",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Estimates of C. immitis infection by skin test reactivity in an endemic community.",
            "authors": [],
            "year": 1985,
            "venue": "Am J Public Health",
            "volume": "75",
            "issn": "",
            "pages": "863-5",
            "other_ids": {
                "DOI": [
                    "10.2105/AJPH.75.8.863"
                ]
            }
        },
        "BIBREF16": {
            "title": "Coccidioidomycosis (valley fever) in older adults: an increasing problem.",
            "authors": [],
            "year": 2003,
            "venue": "Arizona Geriatrics Society Journal.",
            "volume": "8",
            "issn": "",
            "pages": "3-12",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "An epidemic of coccidioidomycosis in Arizona associated with climatic changes, 1998\u20132001.",
            "authors": [],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "191",
            "issn": "",
            "pages": "1981-7",
            "other_ids": {
                "DOI": [
                    "10.1086/430092"
                ]
            }
        },
        "BIBREF18": {
            "title": "Measuring health-related quality of life.",
            "authors": [],
            "year": 1993,
            "venue": "Ann Intern Med",
            "volume": "118",
            "issn": "",
            "pages": "622-9",
            "other_ids": {
                "DOI": []
            }
        }
    }
}