{
    "paper_id": "PMC7120096",
    "metadata": {
        "title": "Respiratory Hygiene and Cough Etiquette",
        "authors": [
            {
                "first": "Louis",
                "middle": [
                    "G."
                ],
                "last": "DePaola",
                "suffix": "",
                "email": "ldepaola@umaryland.edu",
                "affiliation": {}
            },
            {
                "first": "Leslie",
                "middle": [
                    "E."
                ],
                "last": "Grant",
                "suffix": "",
                "email": "legrant@comcast.net",
                "affiliation": {}
            },
            {
                "first": "Sydnee",
                "middle": [],
                "last": "Chavis",
                "suffix": "",
                "email": "schavis@umaryland.edu",
                "affiliation": {}
            },
            {
                "first": "Nisha",
                "middle": [],
                "last": "Ganesh",
                "suffix": "",
                "email": "nganesh@umaryland.edu",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Respiratory infections are quite virulent and easily transmitted throughout populations. Due to the nature of respiratory diseases, pathogens are easily aerosolized and are quite contagious. In the dental office, it is important to recognize signs and symptoms of respiratory illness early\u2014the close proximity in which people work, especially to the airway\u2014can contribute to fast spread of respiratory illnesses between patients, providers, and staff. Prevention of the spread of disease is the most important step in population disease management. It begins with the person afflicted recognizing their symptoms, as well as the awareness of them and those around them to maintain appropriate healthy hygiene. Demonstrating signs or experiencing symptoms of a respiratory infection are often indicative of the disease state [3]. The best method of avoiding the spread of respiratory infections is to avoid contact with others while a person is having symptoms [4]. It is important to inform clinicians, staff, and patients alike that if they have symptoms or show signs of respiratory infections, they should avoid contact with others until they are asymptomatic and noncontagious. There should be policies in place that if an employee or patient is ill with respiratory symptoms, they should stay home (Fig. 7.1).\n",
            "cite_spans": [
                {
                    "start": 824,
                    "end": 825,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 960,
                    "end": 961,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Spread of Disease and Respiratory Hygiene",
            "ref_spans": [
                {
                    "start": 1308,
                    "end": 1311,
                    "mention": "7.1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Maintaining proper respiratory hygiene is critical to maintaining a healthy dental environment. The use of personal protective equipment should be utilized appropriately for all patients and by all clinicians and staff. Masks that provide coverage of the mouth and nose should be worn at every point of providing patient care. Eye coverage that extends over the whole area of the eye and side of the face should be worn as well. Treatment gowns should be utilized and worn when providing care to limit cross contamination between patients. Gloves should be worn at all times when providing patient care and changed for every patient as well. The use of physical, disposable barriers helps to prevent the spread of pathogens among people and from fomites [5].",
            "cite_spans": [
                {
                    "start": 755,
                    "end": 756,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Spread of Disease and Respiratory Hygiene",
            "ref_spans": []
        },
        {
            "text": "Hand washing is another crucial element of proper hygiene. As simple as it sounds, hand washing is an essential step in preventing the spread of any disease. Hands should be washed with soap and water for 30 s, and after washing, one should avoid touching contaminated surfaces with their hands [6]. As an alternative to soap and water, alcohol-based hand sanitizer can be used to disinfect hands before patient care.",
            "cite_spans": [
                {
                    "start": 296,
                    "end": 297,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Spread of Disease and Respiratory Hygiene",
            "ref_spans": []
        },
        {
            "text": "Each patient room should be prepared with protective barriers which are changed after each patient (Fig. 7.2). Hard surfaces should be cleansed of gross, visible debris and disinfected. All materials and instrumentation should be disinfected or sterilized as appropriate [7]. The careful management and cleanliness of the operatory is an important aspect of clinical hygiene.\n",
            "cite_spans": [
                {
                    "start": 272,
                    "end": 273,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Spread of Disease and Respiratory Hygiene",
            "ref_spans": [
                {
                    "start": 105,
                    "end": 108,
                    "mention": "7.2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "Proper hygiene should not be limited to patient rooms and operatories. It is important to disinfect common areas in a dental clinic or office, such as the waiting room, front desk, commonly used items such as pens, and restrooms. Garbage receptacles should be readily available for soiled products in operatories as well as common spaces. Signage around the office is helpful to instruct and remind patients to be mindful and respectful of common areas: cover your mouth and nose when you cough or sneeze with your elbow, wash your hands, and dispose of all garbage in the proper receptacle (Figs. 7.3 and 7.4). These reminders are an important means of making sure patients comply with proper respiratory hygiene as well as staff.\n\n",
            "cite_spans": [],
            "section": "Spread of Disease and Respiratory Hygiene",
            "ref_spans": [
                {
                    "start": 598,
                    "end": 601,
                    "mention": "7.3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 606,
                    "end": 609,
                    "mention": "7.4",
                    "ref_id": "FIGREF3"
                }
            ]
        },
        {
            "text": "Like most disease processes, there is a wide array of pathogenicity of respiratory infections and a broad spectrum of severity of illness. Some infections are minor and self-limiting, and others may be life-threatening. Virulence often depends on the patient population. Young children, the elderly, and people who are medically compromised or immunosuppressed have the highest risk for severe infections [8]. Although vaccines are not available for all respiratory infections, there are several infections that do have vaccines that are very effective at preventing or minimizing disease. Available vaccines for common respiratory infections can be found in Table 7.1. Some of the most common vaccines are for influenza, mumps, measles, rubella (MMR), varicella (chicken pox), diphtheria, pertussis (DTaP), and pneumococcal pneumonia.\n",
            "cite_spans": [
                {
                    "start": 406,
                    "end": 407,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Vaccinations",
            "ref_spans": [
                {
                    "start": 665,
                    "end": 668,
                    "mention": "7.1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "Vaccines are critical to limit the spread of disease and minimize the virulence of these common pathogens. In some countries and jurisdictions, clinicians and staff are required to be up to date on vaccines before they are allowed to practice or work [9]. Those who are not vaccinated may be required to utilize additional personal protective equipment or may be required to wear personal protective equipment, such as a mask, in all areas of a clinic or office (as opposed to just in the operatory). All patients should be informed and encouraged to receive vaccinations for these diseases. The prevention of disease is the best way to manage respiratory infections\u2014vaccinations are a key way to prevent disease.",
            "cite_spans": [
                {
                    "start": 252,
                    "end": 253,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Vaccinations",
            "ref_spans": []
        },
        {
            "text": "Chickenpox (varicella) is a disease caused by varicella-zoster virus. The virus, which is a member of the herpes family, causes a primary respiratory infection with resultant fever and constitutional symptoms, including the characteristic pustulent pox rash. Lesions may present intraorally (Fig. 7.5). Once contracted, the virus remains dormant in cranial nerve and dorsal root ganglia and can reemerge as zoster (shingles) along dermatomal planes. Primary acute varicella is usually self-limiting and lasts about 7\u201310 days but has the potential to cause more severe disease.\n",
            "cite_spans": [],
            "section": "Varicella (Chickenpox) ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": [
                {
                    "start": 297,
                    "end": 300,
                    "mention": "7.5",
                    "ref_id": "FIGREF4"
                }
            ]
        },
        {
            "text": "Varicella is contracted through close contact with infected persons. It can be spread through aerosolized droplets through coughing or sneezing as well as by direct contact with pustulent lesions. The incubation period for disease ranges from 10 to 21 days, with an average incubation period of 2 weeks. Vaccination is available for varicella and widely effective at preventing disease.",
            "cite_spans": [],
            "section": "Varicella (Chickenpox) ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Coronavirus is the pathogen implicated in SARS (severe acute respiratory syndrome) and MERS (Middle East respiratory syndrome). These diseases present as mild to severe acute upper respiratory tract infections with nonspecific symptoms. Most people are infected with coronavirus at some point, and while all people are susceptible, young children are at the highest risk for infection.",
            "cite_spans": [],
            "section": "Coronavirus ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Coronavirus is spread via close contact with infected persons demonstrating symptoms such as coughing or sneezing. It can also be spread via contact with a contaminated object followed by contact with a mucosal surface such as the mouth, nose, or eyes. There are no vaccines or specific treatment for coronavirus infections. Persons with coronavirus should get plenty of rest and stay hydrated to overcome illness.",
            "cite_spans": [],
            "section": "Coronavirus ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Symptoms of coronavirus include fever, cough, sneezing, shortness of breath, headaches, body aches, and can cause severe medical complications such as pneumonia and kidney failure.",
            "cite_spans": [],
            "section": "Coronavirus ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Diphtheria is caused by Corynebacterium diptheriae. Like coronavirus, it is spread by proximity to infected persons and aerosolized secretions through coughing and sneezing, as well as contact with a contaminated object or surface. There is a vaccine available that is very effective for prevention of the disease (DTaP, Tdap, DT, Td). Symptoms may come on gradually, with a usual onset time of 2\u20135 days post exposure.",
            "cite_spans": [],
            "section": "Diphtheria ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Diphtheria is much more rare due to vaccines but still present in developing countries and in areas where vaccines are not standard and routine. Severe disease causes pharyngeal mucous patches that can cause breathing problems, paralysis, heart failure, and in severe cases, death.",
            "cite_spans": [],
            "section": "Diphtheria ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Group A streptococci are a normal and common colonizer of humans. Often found in the throat and on the skin, it is not always pathogenic. However, this pathogen group is implicated in a wide array of pharyngeal and skin diseases. Group A streptococci are implicated in bacterial pharyngitis, scarlet fever, and impetigo.",
            "cite_spans": [],
            "section": "Group A Streptococci ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Disease states of group A strep are contagious via coughing, sneezing, contact with infected person, and contact with skin sores. These diseases are treatable with antibiotics and are often limited due to ease of treatment. However, more severe and invasive infections can lead to sepsis and shock, which requires a multitargeted antibiotic regimen and more invasive treatment.",
            "cite_spans": [],
            "section": "Group A Streptococci ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Mild to moderate infections of group A strep are not contagious after 24 h of treatment with antibiotics. Symptoms include sore throat, fevers, cough, and sneezing. Pharyngitis (sore throat, raspy voice) is commonly the first and most prominent symptom of infection.",
            "cite_spans": [],
            "section": "Group A Streptococci ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "H. influenzae is an upper respiratory tract bacterium that can cause severe and invasive illnesses, such as meningitis, pneumonia, and sepsis, as well as mild illnesses like sinus infections or ear infections. It is spread through respiratory droplets by those colonized and infected with the bacterium. A vaccine is available for Haemophilus influenzae type b, or \u201cHib,\u201d which is the most virulent strain of Haemophilus influenzae. Vaccination should occur at a young age (beginning within the first months, with vaccination by the age of 2). For those not immunized, it is treatable with antibiotics; however, more serious disease often presenting with severe symptoms requires hospitalization for treatment and other care like breathing support.",
            "cite_spans": [],
            "section": "Haemophilus influenzae ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Infection of H. influenzae is contagious via coughing, sneezing, and spread of respiratory droplets from an infected person. It can also be contracted via contact with a contaminated object and contact with mucosal surfaces. Those most at risk for infection are people who are immunosuppressed, young children and older adults.",
            "cite_spans": [],
            "section": "Haemophilus influenzae ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Influenza is a disease caused by influenza virus (not to be confused with H. influenzae bacterium). The flu is usually self-limiting and may present as mild, short-lived illness or enduring moderate illness. It can cause severe illness in children, older adults, and people who are immunocompromised. These populations are not only more susceptible to infection, but also susceptible to increased complications from infection such as pneumonia. With an incubation period of about 1\u20134 days and an average length of infection of about 5 days, the flu has a long period of contagiousness and potential for viral shedding.",
            "cite_spans": [],
            "section": "Influenza ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Symptoms of the flu are akin to symptoms of the previously discussed respiratory infections: cough, fever, sore throat, myalgias, headache, congestion, malaise, chills, and gastrointestinal symptoms. Symptoms usually resolve within a few days but can persist for greater than 2 weeks depending on severity.",
            "cite_spans": [],
            "section": "Influenza ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "A vaccine is available for the flu on an annual basis, accounting for the rapid mutations and virulent strains of the virus. Antiviral medications are also available to help limit and prevent infection with influenza. Medications such as Amantadine, Rimantadine, Zanamivir, and Oseltamivir can help to reduce contagion and duration of illness when taken within the first 2 days of flu onset. Influenza is a very common ailment that can present in epidemic proportions depending on viral virulence and seasonal circumstances.",
            "cite_spans": [],
            "section": "Influenza ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Legionnaires\u2019 disease is a severe type of pneumonia caused by Legionella bacteria. Legionella thrives in damp environments and is spread primarily through human-made water systems like air conditioning units, water fountains, swimming pools, and plumbing units and systems. The bacteria are spread via aerosolized droplets colonized, or less commonly via aspiration of infected water. Infection is not spread from infected persons. People may become infected when they encounter and breathe in contaminated droplets, but not everyone who is exposed develops the disease.",
            "cite_spans": [],
            "section": "Legionnaires\u2019 Disease ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Symptoms of legionnaires include shortness of breath, cough, fever, myalgias, headaches, and generalized constitutional symptoms. It presents with severe disease and high fevers followed by constitutional symptoms. Persons with pulmonary disease, smokers, and with immunosuppression are at highest risk for infection.",
            "cite_spans": [],
            "section": "Legionnaires\u2019 Disease ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Legionnaires\u2019 disease is treatable with antibiotics and can sometimes lead to hospitalization. There is no vaccine available. The best method of prevention of disease is clean, well-circulated water systems without standing water where bacteria can thrive.",
            "cite_spans": [],
            "section": "Legionnaires\u2019 Disease ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Measles (also known as rubeola) is a viral infection that causes respiratory symptoms as well as a secondary disseminated rash of widespread and diffuse red patches. Spread via aerosolized droplets from infected persons, the virus is very contagious and virulent, but preventable by vaccine. MMR vaccine, which inoculates against measles, mumps, and rubella, is administered in two doses. The first dose is administered between 12 and 15 months, and the second delivered between the ages of 4 and 6.",
            "cite_spans": [],
            "section": "Measles ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Symptoms of measles include fever, cough, chills, rhinitis, red and watery eyes (conjunctivitis), as well as Koplik spots (Fig. 7.6), which are punctate white spots on the mucosa in the mouth. Measles is very severe in children and has a high mortality rate for persons under 5 years old. However, due to widespread vaccination, the disease has been close to eradicated.\n",
            "cite_spans": [],
            "section": "Measles ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": [
                {
                    "start": 128,
                    "end": 131,
                    "mention": "7.6",
                    "ref_id": "FIGREF5"
                }
            ]
        },
        {
            "text": "Measles has an incubation period of about 10\u201314 days after exposure. It is contagious for about 4 days before the rash appears and for 4 days after the appearance of the rash. The illness itself begins with mild symptoms which progress to high fever with rash of the acute disease. Vaccination is an important means of controlling outbreaks which are particularly dangerous for children.",
            "cite_spans": [],
            "section": "Measles ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Mumps is a viral disease spread via aerosolized droplets from infected persons and transmission of saliva. Like measles, it is easily prevented by the MMR vaccine and is much less prevalent in recent years due to widespread vaccination. Symptoms include fever, headache, myalgia, fatigue, malaise, coughing, sneezing, and loss of appetite. The telltale sign of mumps infection is swollen cheeks and face, secondary to parotitis and swelling of salivary glands.",
            "cite_spans": [],
            "section": "Mumps ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "The incubation period for mumps is an average of 16\u201318 days, and most symptoms resolve within a few weeks on their own. Mumps presents as a relatively mild disease that is self-limiting. Nonetheless, MMR vaccine provides the best method of prevention.",
            "cite_spans": [],
            "section": "Mumps ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Rubella the third of the viral infections covered by the MMR vaccine. Affecting mostly young children, it presents with mild symptoms and a disseminated rash similar to measles, but with more discrete patches compared to joined patches of rash. Symptoms of rubella are similar to that of measles but much more mild- to low-grade fever, sore throat, lymphadenopathy, and mild constitutional symptoms.",
            "cite_spans": [],
            "section": "Rubella ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Widespread vaccination has led the CDC to declare rubella eradicated within the US. Vaccination for all children remains important to ensure that the disease remains eradicated and that outbreaks do not resume from lack of inoculation.",
            "cite_spans": [],
            "section": "Rubella ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Pneumonia is a disease characterized by inflammation and fluid within the alveoli of the lungs. It is a broad category for several types of respiratory infection that causes lung infiltrate and varying systemic symptoms. Caused by bacteria, viruses, or fungi, pneumonia can be acquired in the community via proximity with infected people but can also be associated with hospitalizations and ventilated patients. Some forms of causative pathogens of pneumonia, like pneumococcal pneumonia caused by Streptococcus pneumonia, are preventable by vaccine, which is generally recommended for older adults. In addition, vaccines for other respiratory pathogens can help prevent or limit some types of pneumonia.",
            "cite_spans": [],
            "section": "Pneumonia ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Pneumonia presents with typical respiratory symptoms of fever, productive cough, malaise, myalgia, fatigue, and headaches. Pneumonia is often a moderate to severe disease and should be treated according to the offending pathogen.",
            "cite_spans": [],
            "section": "Pneumonia ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Mycobacterium tuberculosis is the causative pathogen of this infection of the lungs. Tuberculosis (Tb) is spread by infected persons in close proximity via aerosolized droplets. Not all people who are infected by the bacteria develop the disease; these people have latent Tb without symptoms. However, in those who develop disease, symptoms include a long-lasting productive cough, chest pain, chills and fevers, night sweats, loss of appetite, and weight loss.",
            "cite_spans": [],
            "section": "Tuberculosis ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "A vaccination is available for Tb. It is not widely used within the United States but is administered in other countries where Tb is more common. However, the vaccine does not always prevent people from acquiring Tb. Tb is treated with long-term antibiotics that are targeted toward mycobacterium infections. It can recur from latency, particularly among people who are immunocompromised.",
            "cite_spans": [],
            "section": "Tuberculosis ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Whooping cough, otherwise known as pertussis, is caused by the bacteria Bordetella pertussis. It can affect young children, older adults, and people with immunocompromised more drastically than immune competent people. The predominant symptoms of whooping cough are an uncontrollable, violent cough that lead to shortness of breath and exhaustion. Other symptoms present similarly to other respiratory infections: low-grade fever, rhinitis, and mild constitutional symptoms.",
            "cite_spans": [],
            "section": "Whooping Cough ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Whooping cough is spread via infected aerosolized droplets from coughing or sneezing. It is treatable with antibiotics. It is preventable with the DTaP (TDaP) vaccine, and the prevalence of whooping cough is minimal due to widespread vaccination.",
            "cite_spans": [],
            "section": "Whooping Cough ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "The common cold is arguably the most common respiratory infection. There are over 200 strains of virus that contribute to a cold diagnosis, most of which are rhinoviruses, though many cold strains are coronaviruses. Rhinoviruses contribute to more illness in the spring, summer, and fall, and coronaviruses are more commonly implicated in colder weather due to temperatures at which the virus reproduces and thrives. Other contributing viruses are adenoviruses, coxsackieviruses, echoviruses, and enteroviruses, which are the infecting pathogen associated with more severe illness.",
            "cite_spans": [],
            "section": "Common Cold ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Cold viruses are transmitted via aerosolized droplets and transfer from contaminated objects. They proliferate primarily in the nasal passages, which are the primary point of entry for infection. Symptoms are widespread and include sore throat, nasal discharge and rhinitis, congestion, coughing, sneezing, headaches, head and ear pressure, and myalgia. Symptoms intensify from initial infection though infections are generally self-limiting.",
            "cite_spans": [],
            "section": "Common Cold ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Treatment for colds is usually symptomatic. Rest and fluids are an important aspect of treatment and recovery. Decongestants, analgesics, and cough suppressants help to address specific cold symptoms. Antibiotics are contraindicated for the treatment of colds due to risk for increased bacterial resistance and lack of effect on the insulting virus. The best means of cold prevention is avoidance of people when symptomatic and infected.",
            "cite_spans": [],
            "section": "Common Cold ::: Common and Contagious Respiratory Diseases [#!start#1#!sep#CR1#!sep#bibr#!end#, #!start#2#!sep#CR2#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Respiratory infections are easily spreadable through aerosolized droplets, but they are also preventable via appropriate hygiene in the dental office. Although most respiratory infections are treatable, some infections pose very serious risk to patients. The best method of managing these respiratory diseases is by prevention. By using appropriate barriers and disinfection of the dental operatory, implementing policies to encourage ill patients and staff to stay home, and practicing sound hand hygiene and cough and sneeze etiquette, it is possible to prevent the spread of respiratory infections.",
            "cite_spans": [],
            "section": "Summary",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 7.1: Vaccines, age of vaccine, and treatment for common respiratory pathogens\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig. 7.1: Office signage to inform patients and staff that if they are experiencing symptoms of respiratory illness, they should abstain from presenting to the office. (Photo: The University of Maryland, School of Dentistry)",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig. 7.2: Patient chair with proper disposable barriers and tissues readily available for use. (Photo: Syndee Chavis, DMD, The University of Maryland, School of Dentistry)",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Fig. 7.3: Improper (left) and proper (right) methods of mouth and nose coverage for a cough or sneeze to minimize aerosolized droplets and spread of infection. (Photo: Syndee Chavis, DMD The University of Maryland, School of Dentistry)",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Fig. 7.4: Signage informing patients to cover their cough or sneeze and use proper hand hygiene to avoid spread of infection. (Photo: CDC)",
            "type": "figure"
        },
        "FIGREF4": {
            "text": "Fig. 7.5: Intraoral chicken pox lesions from varicella-zoster virus infection. (Photo courtesy of CDC. http://hardinmd.lib.uiowa.edu/cdc/chickenpox14.html)",
            "type": "figure"
        },
        "FIGREF5": {
            "text": "Fig. 7.6: Koplik spots are an early symptom of measles that present intraorally. (Photo: CDC)",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2017,
            "venue": "The global impact of respiratory disease\u00a0\u2013 second edition",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "The global burden of respiratory disease",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ferkol",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Schraufnagle",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Ann Am Thorac Soc",
            "volume": "11",
            "issn": "",
            "pages": "404-406",
            "other_ids": {
                "DOI": [
                    "10.1513/AnnalsATS.201311-405PS"
                ]
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Antimicrobial resistance and MRSA",
            "authors": [
                {
                    "first": "LG",
                    "middle": [],
                    "last": "DePaola",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Infect Contr Forum",
            "volume": "5",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}