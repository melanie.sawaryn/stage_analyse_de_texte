{
    "paper_id": "PMC7109660",
    "metadata": {
        "title": "A Chain Multinomial Model for Estimating the Real-Time Fatality Rate of a Disease, with an Application to Severe Acute Respiratory Syndrome",
        "authors": [
            {
                "first": "Paul",
                "middle": [
                    "S.",
                    "F."
                ],
                "last": "Yip",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Eric",
                "middle": [
                    "H.",
                    "Y."
                ],
                "last": "Lau",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "K.",
                "middle": [
                    "F."
                ],
                "last": "Lam",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Richard",
                "middle": [
                    "M."
                ],
                "last": "Huggins",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "We take the time periods to be days. Time is measured in the number of days since the first case of the disease was diagnosed or reported. Suppose that we observe the numbers N1t and N2t of the Ht\u22121 inpatients at the start of day t who respectively die from the disease or recover on day t. Note that the number of inpatients Ht\u22121 includes new SARS admissions on day t \u2212 1 as well as existing inpatients who neither die nor recover. On day t, an inpatient dies or recovers with probabilities p1t and p2t, respectively. Using the information on the number of inpatients at the end of day t \u2212 1, N1t, N2t|Ht\u22121 \u223c multinomial (Ht\u22121, p1t, p2t) (see Becker (7) and Yip and Huggins (8)). Consequently, Nit and Njs are conditionally uncorrelated for s \u2260 t, i, j = 1, 2. Simple estimators for p1t and p2t are N1t/Ht\u22121 and N2t/Ht\u22121, respectively. The estimated variances of p\u0302it are given by p\u0302it(1\u2212p\u0302it)/Ht\u22121 for i = 1, 2, and the covariance between p\u03021t and p\u03022t is \u2212p\u03021tp\u03022t/Ht\u22121. A simple estimator for the fatality rate at time t, \u03c0t, is\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{{\\hat{{\\pi}}}}_{t}{=}\\frac{{\\hat{p}}_{1t}}{{\\hat{p}}_{1t}{+}{\\hat{p}}_{2t}}. \\end{document}A plot of these unsmoothed estimates for the Hong Kong data is given in figure 1. These daily estimates are highly variable, and there is no long-term trend in the estimated fatality rates. The daily estimates are generally larger than the WHO estimates, which increase throughout the course of the epidemic. We propose to reduce the variability and hence clarify the long-term trend by computing smooth estimates.",
            "cite_spans": [],
            "section": "THE ESTIMATOR",
            "ref_spans": []
        },
        {
            "text": "For computation of smooth estimates of the recovery and death rates for SARS, a kernel method was applied to p\u03021t and p\u03022t. Let Kb(x) = b\u22121K(x/b) be a kernel function of bandwidth b, with support in [\u22121.1]. The Nadaraya-Watson estimator (9, 10) for pit is given by\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} {\\tilde{p}}_{it}{=}\\frac{{\\sum}_{s{=}1}^{T}K_{b}(t{-}s){\\hat{p}}_{is}}{{\\sum}_{s{=}1}^{T}K_{b}(t{-}s)},{\\,}i{=}1,{\\,}2, \\end{document}where T is the last day of the epidemic. Because p\u0302it and p\u0302is are conditionally uncorrelated for s \u2260 t, the variances of the p\u0303it, i = 1, 2, are given by\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{var}({\\tilde{p}}_{it}){=}\\frac{{\\sum}_{s{=}1}^{T}K_{b}^{2}(t{-}s){\\hat{p}}_{is}(1{-}{\\hat{p}}_{is})/H_{s{-}1}}{\\left({\\sum}_{s{=}1}^{T}K_{b}(t{-}s)\\right)^{2}} \\end{document}and the covariance of p\u03031t and p\u03032t is\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{cov}({\\tilde{p}}_{1t},{\\,}{\\tilde{p}}_{2t}){=}\\frac{{-}{\\sum}_{s{=}1}^{T}K_{b}^{2}(t{-}s){\\hat{p}}_{1s}{\\hat{p}}_{2s}/H_{s{-}1}}{\\left({\\sum}_{s{=}1}^{T}K_{b}(t{-}s)\\right)^{2}}. \\end{document}",
            "cite_spans": [],
            "section": "THE ESTIMATOR",
            "ref_spans": []
        },
        {
            "text": "An estimator for the ratio of the death and recovery transition probabilities is given by \u03b8\u0303t = p\u03031t/p\u03032t, and its variance can be obtained by the standard \u03b4 method (11, p. 388):\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{var}(\\mathrm{{\\tilde{{\\theta}}}}_{t}){=}\\frac{1}{{\\tilde{p}}_{2t}^{2}}\\mathrm{var}({\\tilde{p}}_{1t}){+}\\frac{{\\tilde{p}}_{1t}^{2}}{{\\tilde{p}}_{2t}^{4}}\\mathrm{var}({\\tilde{p}}_{2t}){-}\\frac{2{\\tilde{p}}_{1t}}{{\\tilde{p}}_{2t}^{3}}\\mathrm{cov}({\\tilde{p}}_{1t},{\\tilde{p}}_{2t}). \\end{document}Then, we estimate \u03c0t by\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{{\\tilde{{\\pi}}}}_{t}{=}\\frac{{\\tilde{p}}_{1t}}{{\\tilde{p}}_{1t}{+}{\\tilde{p}}_{2t}}{=}\\frac{\\mathrm{{\\tilde{{\\theta}}}}_{t}}{1{+}\\mathrm{{\\tilde{{\\theta}}}}_{t}}. \\end{document}The \u03b4 method yields a variance estimator for \u03c0\u0303t as\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{var}(\\mathrm{{\\tilde{{\\pi}}}}_{t}){=}\\frac{1}{(1{+}\\mathrm{{\\tilde{{\\theta}}}}_{t})^{4}}\\mathrm{var}(\\mathrm{{\\tilde{{\\theta}}}}_{t}) \\end{document}and approximate (1 \u2212 \u03b1) \u00d7 100 percent pointwise confidence limits for \u03c0\u0303t can be obtained as \u03c0\u0303t \u00b1 z1\u2212\u03b1/2SE(\u03c0\u0303t), where z1\u2212\u03b1/2 is the normal quartile at the significance level \u03b1/2 and SE is the standard error.",
            "cite_spans": [],
            "section": "THE ESTIMATOR",
            "ref_spans": []
        },
        {
            "text": "The first SARS case in Hong Kong was diagnosed on March 12, 2003, and 1,755 Hong Kong patients were hospitalized over the course of the epidemic (13). Of these cases, 1,456 patients were discharged and 299 patients died from SARS by July 31, 2003.",
            "cite_spans": [],
            "section": "Hong Kong ::: APPLICATIONS",
            "ref_spans": []
        },
        {
            "text": "The top panels of figure 2 show real-time transition probabilities and associated 95 percent confidence intervals. The estimated death rate, p\u03031t, peaked in mid-March, decreased for approximately 2 weeks, and then increased again until mid-May. The estimated recovery rate p\u03032t, shown in the top right panel, rose in April, when the majority of SARS patients (mostly health-care workers) from a regional hospital (Prince of Wales Hospital) were discharged, and was then relatively steady until late June. The bottom left panel depicts the ratio of the two estimated transitional probabilities \u03b8\u0303t, and the bottom right panel plots the estimated fatality rate at time t, \u03c0\u0303t, along with the WHO fatality rate over the course of the epidemic for comparison.",
            "cite_spans": [],
            "section": "Hong Kong ::: APPLICATIONS",
            "ref_spans": []
        },
        {
            "text": "The proposed estimate of the fatality rates was high at the beginning of the outbreak. This may reflect that the patients who were initially admitted were quite ill but, as the epidemic progressed, patients were admitted earlier in the progression of their disease. The estimated fatality rate stabilized at the beginning of April, and until mid-June it fluctuated between 15 percent and 25 percent. The stabilization of the estimated fatality rate could reflect a more mature approach in the diagnosis and treatment of SARS, although the efficacy of using high-dose corticosteroid and ribavirin to deal with lung parenchymal inflammation and the coronavirus itself is controversial (14\u201316). Alternatively, it could reflect a stationary distribution of the chain multinomial model. The WHO-adopted estimate increased gradually over the course of the epidemic and converged to the final overall fatality rate, which was approximately 17 percent.",
            "cite_spans": [],
            "section": "Hong Kong ::: APPLICATIONS",
            "ref_spans": []
        },
        {
            "text": "SARS data from Beijing have been available on the WHO website (http://www.who.int/csr) since April 21, 2003. By July 2, 2003, there had been 191 deaths among the 2,521 cases. Our estimates are plotted in the bottom right panel of figure 3. As in Hong Kong, the fatality rate was estimated to be high in the early stage of the epidemic. It then continuously improved over the course of the epidemic, with an overall fatality rate of approximately 8 percent. The reduction in the fatality rate is possibly related to the use of different treatments by the Chinese authorities, who also implemented effective measures to prevent infection in the community and in hospitals during the epidemic. In Beijing, a special hospital was built within 2 weeks of the start of the epidemic to treat SARS patients. Here, the WHO fatality rate overestimates the fatality rate in the latter part of the epidemic. The use of traditional Chinese medicine has also been suggested to have contributed to the low rate of fatality in Beijing, and glycyrrhizin, an active component of licorice root, has been shown to inhibit the replication of SARS-associated coronavirus (17).",
            "cite_spans": [],
            "section": "Beijing ::: APPLICATIONS",
            "ref_spans": []
        },
        {
            "text": "We simulated data under various scenarios to compare the proposed real-time fatality rate with the WHO fatality rate. In the simulation, we used the daily number of inpatients in the Hong Kong SARS epidemic as the basis for simulation for each day so that the infection process was not simulated. The numbers of deaths and recoveries for each day were then simulated according to the assumed multinomial probabilities under different scenarios. We chose bandwidths for the estimator by means of the empirical bias bandwidth selection method.",
            "cite_spans": [],
            "section": "SIMULATED DATA",
            "ref_spans": []
        },
        {
            "text": "Scenario I assumes constant death and recovery rates on each day, with values of 0.01 and 0.1, respectively. This can be the situation if no effective treatment is available and/or the lethality of the disease does not change over time. Scenario II is similar to scenario I except for the appearance of a sudden increase in the death rate from 0.01 to 0.03 at 30 days after the start of the epidemic. This mirrors the Amoy Gardens outbreak in Hong Kong, where the strain of the SARS virus was found to be more lethal. In this scenario, it is assumed that the death rate then drops back to its initial level after a period of time. Scenario III assumes a consistently decreasing death rate and an increasing recovery rate. In this scenario, the fatality rate would decrease monotonically.",
            "cite_spans": [],
            "section": "SIMULATED DATA",
            "ref_spans": []
        },
        {
            "text": "The left panels in figure 4 show the true death and recovery rates under the different scenarios. The right panels show the corresponding true daily fatality rate p1t /(p1t + p2t), the average over 100 simulations of the proposed real-time fatality rate, and the WHO fatality rate. In all cases, the mean of the proposed fatality rate estimated the true fatality rate quite well and captured the trend or sudden changes in the true fatality for the first and third scenarios. In the second scenario, the abrupt change was not totally captured, but this is to be expected.",
            "cite_spans": [],
            "section": "SIMULATED DATA",
            "ref_spans": []
        },
        {
            "text": "In scenario I, the WHO fatality rate increased consistently during the first 60 days and then stabilized, so it was underestimating the true fatality rate in the earlier period of the epidemic. In scenario II, the WHO fatality rate could only gradually reflect the sudden increase in the true fatality rate, but it still underestimated this rate. In addition, the WHO fatality rate did not reflect the later decrease in the fatality rate. In scenario III, after an initial increase, the WHO fatality rate was relatively constant and underestimated the decrease in the fatality rate.",
            "cite_spans": [],
            "section": "SIMULATED DATA",
            "ref_spans": []
        },
        {
            "text": "We have shown that a real-time fatality rate estimated by means of kernel smoothing methods has major advantages over the traditional WHO estimate during the course of an epidemic. An iteratively reweighted kernel estimator\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} {\\tilde{p}}_{it}{=}\\frac{{\\sum}_{s{=}1}^{T}K_{b}(t{-}x){\\hat{p}}_{is}w_{is}}{{\\sum}_{s{=}1}^{T}K_{b}(t{-}x)w_{is}}, \\end{document}where\\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} w_{is}{=}\\frac{H_{s{-}1}}{{\\tilde{p}}_{it}^{{\\ast}}(1{-}{\\tilde{p}}_{it}^{{\\ast}})} \\end{document}is the weight to be iterated until convergence, can also be used. This estimator should be more efficient, but the improvement is expected to be marginal. In general, the weight function, which puts emphasis on the information in different periods or characteristics contributed to the estimator, does not affect the estimator much in comparison with the choice of bandwidth.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Our simulation studies show that variations in clinical outcome measures\u2014resulting from improved treatment modalities, for example\u2014are not accurately detected by the WHO estimator. On the other hand, our method reflects the possible impact of such factors, including variation in clinical management and other exogenous factors over the course of the outbreak. The spread of the SARS epidemic in Hong Kong was similar to that of scenario I, in which the recovery and mortality rates are nearly constant. In addition, the situation in Beijing was similar to that of scenario III, in which constant improvement in rates of recovery and death was observed. However, as was shown in the simulation, the WHO estimate fails to reflect the time-varying fatality rate in all regions.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "The WHO estimates are simply not suitable for monitoring the fatality rate over the course of an emerging epidemic. The present formulation is also different from traditional survival analysis, which adopts chronologic time on the individual level rather than calendar time on the population level and thus requires more detailed data. The proposed chain multinomial model of using the real-time case fatality rate should allow epidemiologists to monitor not only SARS but also other emerging infections and conditions in the future. It can provide a timely and accurate estimate of the number of fatalities inflicted by an epidemic, especially in the earlier period when decisions on health policy and treatment are most critical. It works satisfactorily if the durations of hospital stays for recoveries and deaths are not that different. Yip et al. (18) adopted a competing risk model to provide an estimate of the real-time fatality for SARS using a counting process approach. In the absence of data from controlled clinical trials on SARS treatment, it is interesting to speculate as to whether the different estimated fatality rates between Hong Kong and Beijing could be related to the different clinical treatment protocols used.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "FIGURE 1.: Unsmoothed estimates of the death rate, p\u03021t (top left), the recovery rate, p\u03022t (top right), and the ratio of the death rate to the recovery rate, p\u03021t/p\u03022t (bottom left), for the outbreak of severe acute respiratory syndrome in Hong Kong, China, 2003. Also shown are the estimated fatality rate, p\u03021t/(p\u03021t+p\u03022t), and the World Health Organization estimate (thin line) of the fatality rate (bottom right).",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "FIGURE 2.: Kernel estimates of the death rate, p\u03031t (top left), the recovery rate, p\u03032t (top right), and the ratio of the death rate to the recovery rate, \u03b8\u0303t (bottom left), for the outbreak of severe acute respiratory syndrome in Hong Kong, China, 2003. Also shown are the estimated fatality rate, \u03c0\u0303t, and the World Health Organization estimate (thin line) of the fatality rate (bottom right). Dashed lines, 95% confidence interval.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "FIGURE 3.: Kernel estimates of the death rate, p\u03031t (top left), the recovery rate, p\u03032t (top right), and the ratio of the death rate to the recovery rate, \\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{{\\tilde{{\\theta}}}} \\end{document}t (bottom left), for the outbreak of severe acute respiratory syndrome in Beijing, China, 2003. Also shown are the estimated fatality rate, \\batchmode \\documentclass[fleqn,10pt,legalpaper]{article} \\usepackage{amssymb} \\usepackage{amsfonts} \\usepackage{amsmath} \\pagestyle{empty} \\begin{document} \\mathrm{{\\tilde{{\\pi}}}} \\end{document}t, and the World Health Organization estimate (thin line) of the fatality rate (bottom right). Dashed lines, 95% confidence interval.",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "FIGURE 4.: Simulation results from estimations of the real-time fatality rate for severe acute respiratory syndrome in three different scenarios with constant (top panels), stepwise (middle panels), and decreasing (bottom panels) fatality rates, 2003. True death rates (thick lines) and recovery rates (thin lines) are shown in the left panels. Also shown are the average real-time fatality rates (thick solid lines) and average World Health Organization estimates (thin lines) of the case fatality rate in 100 simulations (right panels). The true fatality rates are shown by the thick dotted lines (right panels).",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 1996,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 1973,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 1997,
            "venue": "J Am Stat Assoc",
            "volume": "92",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "JAMA",
            "volume": "289",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Nature",
            "volume": "423",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 2005,
            "venue": "J R Stat Soc A",
            "volume": "168",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Am J Respir Crit Care Med",
            "volume": "168",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [],
            "year": 2004,
            "venue": "Am J Epidemiol",
            "volume": "159",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": 1995,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "",
            "authors": [],
            "year": 1989,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "",
            "authors": [],
            "year": 1995,
            "venue": "Stoch Anal Appl",
            "volume": "13",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "",
            "authors": [],
            "year": 1964,
            "venue": "Theor Prob Appl",
            "volume": "19",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}