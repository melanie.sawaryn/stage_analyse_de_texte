{
    "paper_id": "PMC7123102",
    "metadata": {
        "title": "Expression and Characterization of Recombinant S2 Subunit of SARS-coronavirus S Fusion Protein",
        "authors": [
            {
                "first": "Susan",
                "middle": [
                    "Del"
                ],
                "last": "Valle",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Emanuel",
                "middle": [],
                "last": "Escher",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "William",
                "middle": [
                    "D."
                ],
                "last": "Lubell",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Zhe",
                "middle": [],
                "last": "Yan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kathryn",
                "middle": [
                    "V."
                ],
                "last": "Holmes",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Robert",
                "middle": [
                    "S."
                ],
                "last": "Hodges",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The SARS-CoV Spike fusion protein subunit S2 plays an important role in viral entry by initiating fusion of the viral and cellular membranes. S2 exists in at least three different conformations within the trimeric glycoprotein complex. In the postfusion conformation (six-helix bundle, 6HB), the three helical heptad repeats of the HRC region pack in an antiparallel fashion into the hydrophobic grooves on the surface of the trimeric coiled-coil formed by the heptad repeat N-terminal (HRN) region. The HRN and HRC regions are separated by a 149 amino acid interhelical domain (IHD), which is thought to provide the flexibility required for the conformational change that occurs during the membrane fusion process. There is no structural information currently available for the interhelical domain and S2 in the prefusion state and fusion intermediate states.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "In this study, we have expressed the recombinant S2 subunit of SARS-CoV S fusion protein including the interhelical domain and studied this protein using biochemical and biophysical methods. This engineered chimeric S2 fusion protein should facilitate structural studies of the prefusion state and also serve as a target to guide future attempts to develop inhibitors that prevent the formation of the postfusion 6HB.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "We have chosen the sequence 901\u20131185 of the S2 subunit of SARS-CoV S protein to design the constructs. Through adding GCN4 trimeric helices and deleting 6 residues in the C-terminus of the HRC region, we proposed to stabilize the trimeric structure of HRC and prevent its binding to the HRN region, and consequently keep the S2 domain in the prefusion state. The proteins were expressed as maltose-binding protein (MBP) fusion proteins. We have designed two constructs as follows,\nthe whole S2: HRN-IHD-HRC-GCN4 (S2);partial HRN and interhelical domain (951-1150): HRN- IHD (L4).\n",
            "cite_spans": [],
            "section": "Construct Design",
            "ref_spans": []
        },
        {
            "text": "Both constructs were expressed in E.coli host strain BL21 cells, and purified by affinity chromatography using amylose resins and size-exclusion chromatography. The results in Fig. 1 show both the constructs have been purified as soluble proteins with high purity. Both S2 and L4 constructs have been cleaved to remove the MBP tag using protease Factor Xa.\n\n",
            "cite_spans": [],
            "section": "Results and Discussion",
            "ref_spans": [
                {
                    "start": 181,
                    "end": 182,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "The purified MBP fusion proteins were analyzed for their oligomerization state by size-exclusion chromatography (SEC). As shown in Fig 2a, MBP-S2 exists in an aggregated state since it is eluted in the exclusion volume. Similarly, recombinant protein MBP-L4 is also aggregated. Following treatment with 8 M urea and 10 mM DTT, analysis by SEC under these denaturing conditions shows a significant concentration of monomer (Fig. 3b). After treatment of the proteins with 2% SDS and 10 mM DTT, analysis by SEC using 0.1% SDS now shows that the proteins are disrupted into the monomeric state (Fig. 3c). These results mean that the recombinant proteins were expressed and purified as soluble high molecular weight aggregates, and this aggregation may be caused by the interhelical domain (IHD).\n\n\n",
            "cite_spans": [],
            "section": "Results and Discussion",
            "ref_spans": [
                {
                    "start": 135,
                    "end": 136,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 428,
                    "end": 429,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 596,
                    "end": 597,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "We have used two antibodies specific for the HRC coiled-coil used to analyze the conformation of the HRC domain in the fusion protein using ELISA. HRC1 antibody could not bind to the prefusion state of HRC but the HRC2 antibody could bind to both the prefusion state and the 6HB conformation of HRC. The results showed that both HRC1 and HRC2 antibodies bind to MBP-S2, suggesting that MPB-S2 is not in a conformation containing the 6HB conformation, i.e., the postfusion state, which is consistent with our design aim.",
            "cite_spans": [],
            "section": "Results and Discussion",
            "ref_spans": []
        },
        {
            "text": "CD spectra in Fig. 3a revealed that after cleavage from MBP, S2 in PBS showed a soluble and largely \u03b1-helical structure. After cleavage from MBP, L4 in 0.1% SDS also showed typical \u03b1-helical structure, as illustrated in Fig. 3b.",
            "cite_spans": [],
            "section": "Results and Discussion",
            "ref_spans": [
                {
                    "start": 19,
                    "end": 20,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 225,
                    "end": 226,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "Our studies show that the recombinant proteins were expressed and purified as soluble high molecular weight aggregate proteins, and the aggregation may be caused by the interhelical domain (IHD). Thus, S2 was expressed as a soluble aggregate in the prefusion state. Further research is in progress to prevent aggregation by mutation or deletion of specific residues in the IHD and structure determination of the IHD.",
            "cite_spans": [],
            "section": "Results and Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 1.: SDS-PAGE analysis of MBP-S2, MPB-L4 and S2, L4",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig. 2.: SEC Analysis of MBP-S2",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Fig. 3: CD Analysis of S2 and L4",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Tripet",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J. Struct. Biol.",
            "volume": "155",
            "issn": "",
            "pages": "176-194",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jsb.2006.03.019"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J. Biol. Chem.",
            "volume": "279",
            "issn": "",
            "pages": "49414-49419",
            "other_ids": {
                "DOI": [
                    "10.1074/jbc.M408782200"
                ]
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "J. Virol.",
            "volume": "74",
            "issn": "",
            "pages": "4746-4754",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.74.10.4746-4754.2000"
                ]
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Lay",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "FEBS",
            "volume": "567",
            "issn": "",
            "pages": "183-188",
            "other_ids": {
                "DOI": [
                    "10.1016/j.febslet.2004.04.054"
                ]
            }
        }
    }
}