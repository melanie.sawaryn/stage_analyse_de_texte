{
    "paper_id": "PMC7121722",
    "metadata": {
        "title": "General Measures of Infection Control",
        "authors": [
            {
                "first": "Rajesh",
                "middle": [],
                "last": "Chawla",
                "suffix": "",
                "email": "drchawla@hotmail.com",
                "affiliation": {}
            },
            {
                "first": "Subhash",
                "middle": [],
                "last": "Todi",
                "suffix": "",
                "email": "drsubhashtodi@gmail.com",
                "affiliation": {}
            },
            {
                "first": "Sheila",
                "middle": [
                    "Nainan"
                ],
                "last": "Myatra",
                "suffix": "",
                "email": "sheila150@hotmail.com",
                "affiliation": {}
            },
            {
                "first": "Raghu",
                "middle": [
                    "S."
                ],
                "last": "Thota",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "A 72-year-old male patient with non-Hodgkin\u2019s lymphoma was neutropenic after chemotherapy and presented to the ICU with breathlessness and hypotension. He was intubated and kept on a ventilator and received broad-spectrum antibiotics. He had a peripheral, central, and arterial line in place. A Foley\u2019s catheter and a nasogastric tube were also placed.",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "Health-care-associated infections are a common cause of increased morbidity, mortality, and cost of care in ICUs. Infection control and judicious antibiotic use are the mainstay of management of these patients. A systematic and multidisciplinary approach to infection control practices goes a long way in minimizing this problem.",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 1: Assess the need for isolation\n\nScreen all ICU patients for the following:\nNeutropenia and immunological disorderDiarrheaSkin rashesKnown communicable diseaseKnown carriers of an epidemic strain of bacterium\n\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 2: Identify the type of isolation needed\nThere are two types of isolation in the ICU:\nProtective isolation for neutropenic or other immunocompromised patients to reduce the chances of acquiring opportunistic infections.Source isolation of colonized or infected patients to minimize potential transmission to other patients or staff.\nIsolation rooms should have tight-fitting doors, glass partitions for observation, and both negative-pressure (for source isolation) and positive-pressure (for protective isolation) ventilations.\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 3: Identify the patient at risk of nosocomial infections\nThere are patient-, therapy-, and environment-related risk factors for the development of nosocomial infection:\nAge more than 70 yearsShockMajor traumaAcute renal failureComaPrior antibioticsMechanical ventilationDrugs affecting the immune system (steroids, chemotherapy)Indwelling cathetersProlonged ICU stay (>3 days)\n\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 4: Observe hand hygiene\nHands are the most common vehicle for transmission of organisms, and \u201chand hygiene\u201d is the single most effective means of preventing the horizontal transmission of infections among hospital patients and health-care personnel.When and why\u2014follow WHO\u2019s five moments for hand hygiene (Fig. 48.1):\nBefore touching a patient\u2014to protect the patient from harmful germs carried on your handsBefore aseptic procedures\u2014to protect the patient against harmful germs, including the patient\u2019s own germsAfter body fluid exposure/risk\u2014to protect yourself and the health-care environment from the harmful patient\u2019s germsAfter touching the patient\u2014to protect yourself and the health-care environment from the harmful patient\u2019s germsAfter touching the patient\u2019s surrounding\u2014to protect yourself and the health-care environment from the harmful patient\u2019s germs(Remember, there are two moments before and three moments after touching the patient)\n\nHow\nWash hands with soap and water when they are soiled or visibly dirty with blood or other body fluids. Wet your hands, apply soap and then scrub them vigorously for at least 15 s. Cover all surfaces of the hands and fingers, wash with water, and then dry thoroughly using a disposable towel (Fig. 48.1).Use an alcohol-based hand rub (e.g., 0.5% chlorhexidine with 70%  w/v ethanol) if hands are not visibly dirty. A combination of chlorhexidine and alcohol is ideal as they cover Gram-positive and Gram-negative organisms, viruses, mycobacteria, and fungi. Chlorhexidine also has residual activity.\nDuring surgical hand preparation, all hand jewelries (e.g., rings, watches, bracelets) must be removed.Finger nails should be trimmed with no nail polish or artificial nails.Avoid wearing long sleeves, ties should be tucked in, house coats are discouraged, and wearing scrubs is encouraged.\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": [
                {
                    "start": 317,
                    "end": 321,
                    "mention": "48.1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1256,
                    "end": 1260,
                    "mention": "48.1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "\nStep 5: Follow standard precautions\nStandard precautions include prudent preventive measures to be used at all times, regardless of a patient\u2019s infection status.\nGloves\nSterile gloves should be worn after hand hygiene procedure while touching mucous membrane and nonintact skin and performing sterile procedures (e.g., arterial, central line, and Foley catheter insertion).Clean, nonsterile gloves are safe for touching blood, other body fluids, \u00adcontaminated items, and any other potentially infectious materials.Change gloves between tasks and procedures in the same patient especially when moving from a contaminated body area to a clean body area.Never wear the same pair of gloves for the care of more than one patient.Remove gloves after caring for a patient.Practice hand hygiene whenever gloves are removed.\n\nGown\nWear a gown to prevent soiling of clothing and skin during procedures that are likely to generate splashes of blood, body fluids, secretions, or excretions.The sterile gown is required only for aseptic procedures, and for the rest, a clean, nonsterile gown is sufficient.Remove the soiled gown as soon as possible, with care to avoid contamination.\n\nMask, eye protection/face shield\nWear a mask and adequate eye protection (eyeglasses are not enough) or a face shield to protect mucous membranes of the eyes, nose, and mouth during procedures and patient-care activities that are likely to generate splashes/sprays of blood, body fluids, etc.Patients, relatives, and health-care workers presenting with respiratory symptoms should also use masks (e.g., cough).\n\nShoe and head coverings\nThey are not required for routine care.\n\nPatient-care equipment\nUsed patient-care equipment soiled with blood, body fluids, secretions, or excretions should be handled carefully to prevent skin and mucous membrane exposures, contamination of clothing, and transfer of microorganisms to health-care workers, other patients, or the environment.Ensure that reusable equipment is not used for the care of another patient until it has been cleaned and sterilized appropriately.Ensure that single-use items and sharps are discarded properly.\n\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 6: Follow transmission-based precautions\nIn addition to standard precautions, the following should be observed in those patients known or suspected to have airborne, contact, or droplet infections:\nAirborne precautions\nDisease-causing microorganisms may be suspended in the air as small particles, aerosols, or dust and remain infective over time and distance, for example, Mycobacterium tuberculosis (pulmonary/laryngeal), varicella zoster virus (chickenpox), herpes zoster (shingles), and rubeola virus (measles).Isolate with negative-pressure ventilation.Respiratory protection must be employed when entering the isolation room.Use the disposable N95 respirator mask, which fits tightly around the nose and mouth to protect against both large and small droplets. This should be worn by all persons entering the room, including visitors.\n\nContact precautions\nInfections can be spread by usual direct or indirect contact with an infected person, the surfaces or patient-care items in the room, for example, parainfluenza virus infection, respiratory syncytial virus infection, varicella (chickenpox), herpes zoster, hepatitis A, and rotavirus infections.\nIsolation is required.Noncritical patient-care equipment should preferably be of single use. If unavoidable, then clean and disinfect them adequately before using to another patient.Limit transport of the patient.\n\nDroplet precautions\nMicroorganisms are also transmitted by droplets (large particles >5 \u03bcm in size) generated during coughing, sneezing, talking, or a short-distance traveling, for example, influenza virus, Bordetella pertussis, Hemophilus influenzae (meningitis, pneumonia), Neisseria meningitidis (meningitis, pneumonia, bacteremia), Mycoplasma pneumoniae, SARS-associated coronavirus (SARS-CoV), group A Streptococcus, adenovirus, and rhinovirus.Isolation is required.Respiratory protection must be employed when entering the isolation room or within 6\u201310 ft of the patient. Use the disposable N95 respirator mask, which fits tightly around the nose and mouth to protect against both large and small droplets. This should be worn by all persons entering the room, including visitors.Limit transport of the patient.\n\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 7: Use specific strategies focused on prevention of specific nosocomial infections\nIn addition to the standard and transmission-based precautions, there are several strategies focused on prevention of specific nosocomial infections in critically ill patients. Of these, ventilator-associated pneumonia (VAP), catheter-related bloodstream infection (CRBSI), and urinary tract infection (UTI) are the most important.\n\nStrategies to reduce VAP\nAvoid intubation whenever possible.Consider noninvasive ventilation whenever possible.Prefer oral intubations to nasal unless contraindicated.Keep head elevated at 30\u201345\u00b0 in the semi-recumbent body position.Consider early extubation.Avoid reintubation whenever possible.Routine change of ventilator circuits is not required.Monitor endotracheal tube cuff pressure (keep it >20 cmH2O) to avoid air leaks around the cuff, which can allow entry of bacterial pathogens into the lower respiratory tract.Prefer endotracheal tubes with a subglottic suction port to prevent pooling of secretions around the cuff leading to microaspiration.The heat moisture exchanger may be better than the heated humidifier.Closed endotracheal suction systems may be better than the open suction.Periodically drain and discard any condensate that collects in the tubing of a mechanical ventilator.\n\nStrategies to reduce CRBSI\nPrefer the upper extremity for catheter insertion. Avoid femoral route for central venous cannulation (CVC).If the catheter is inserted in a lower extremity site, replace to an upper extremity site as soon as possible.Use maximal sterile barrier precautions (cap, mask, sterile gown, sterile gloves) and a sterile full-body drape while inserting CVCs, peripherally inserted central catheters (PICC), or guidewire exchange.Clean skin with more than 0.5% chlorhexidine preparation with alcohol \u00ad(usually 2% chlorhexidine with 70%  w/v ethanol) before CVC, arterial \u00adcatheter insertion, etc.Use chlorhexidine/silver sulfadiazine or minocycline/rifampin-impregnated CVCs when the catheter is expected to remain in place for more than 5 days and only if the bloodstream infection rates are high in the unit.Use ultrasound-guided insertion if technology and expertise are available.Use either sterile gauze or sterile, transparent, semipermeable dressing to cover the catheter site. Replace the catheter site dressing only when the dressing becomes damp, loosened, or visibly soiled.Evaluate the catheter insertion site daily and check if a transparent dressing is present and palpate through the dressing for any tenderness.Insertion date should be put on all vascular access devices.Use 2% chlorhexidine wash daily for skin cleansing to reduce CRBSI.Use needleless intravascular catheter access systems and avoid stopcocks. Closed catheter access systems should be preferred to open systems.Clean injection ports with an appropriate antiseptic (chlorhexidine, povidone-iodine, an iodophor, or 70% alcohol), accessing the port only with sterile devices. Cap stopcocks when not in use.Assess the need for the intravascular catheter daily and remove when not required.Peripheral lines should not be replaced more frequently than 72\u201396 h. Routine replacement of CVCs is not required.Replace administration sets, including secondary sets and add-on devices, every day in patients receiving blood, blood products, or fat emulsions.If other intravenous fluids are used, change no less than 96-h intervals and at least every 7 days.Needleless connectors should be changed frequently (every 72 h).Replace disposable or reusable transducers at 96-h intervals.\n\nStrategies to reduce UTI\nInsert catheters only for appropriate indications.Follow aseptic insertion of the urinary catheter.Maintain a closed drainage system.Maintain unobstructed urine flow. At all times, the urinary catheter should be placed and taped above the thigh and the urinary bag should hang below the level of the bladder.The urinary bag should never have floor contact.Changing indwelling catheters or drainage bags at fixed intervals is not recommended. Change only if there are clinical indications such as infection or obstruction or when the closed system is compromised.Remove the catheter when it is no longer needed.\n\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 8: Consider environmental factors\n\nCleaning and disinfection\nHigh-quality cleaning and disinfection of all patient-care areas is important, especially surfaces close to the patient (e.g., bedrails, bedside tables, doorknobs, and equipment).\nSome pathogens can survive for long periods in the environment, particularly methicillin-resistant Staphylococcus aureus (MRSA), vancomycin-resistant Enterococcus (VRE), Acinetobacter species, Clostridium difficile, and norovirus.\nEPA-registered disinfectants or detergents that best meet the overall needs of the ICU should be used for routine cleaning and disinfection.Frequency of cleaning should be as follows: surface cleaning (walls) twice weekly, floor cleaning two to three times per day, and terminal cleaning (patient bed area) after discharge or death.\n\nArchitecture and layout, especially while designing a new ICU\nThe unit may be situated close to the operating theater or emergency department for easy accessibility but should be away from the main ward areas.Central air-conditioning systems are designed in such a way that recirculated air must pass through appropriate filters.It is recommended that all air should be filtered to 99% efficiency down to 5 \u03bcm.Suitable and safe air quality must be maintained at all times. Air movement should always be from clean to dirty areas.It is recommended to have a minimum of six total air changes per room per hour, with two air changes per hour composed of outside air.Isolation facility should be with both negative- and positive-pressure ventilations.Clearly demarcated routes of traffic flow through the ICU are required.Adequate space around beds is ideally 2.5\u20133 m.Electricity, air, vacuum outlets/connections should not hamper access around the bed.Adequate number of washbasins should be installed.Alcohol gel dispensers are required at the ICU entry, exits, every bed space, and every workstation.There should be separate medication preparation area.There should be separate areas for clean storage and soiled and waste storage and disposal.Adequate toilet facilities should be provided.\n\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nStep 9: Organizational and administrative measures\nWork with hospital administration for better patient-to-nurse ratio in the ICU.Policies for controlling traffic flow to and from the unit to reduce sources of contamination from visitors, staff, and equipment.Waste and sharp disposal policy.Education and training for ICU staff about prevention of nosocomial infections.ICU protocols for prevention of nosocomial infections.Audit and surveillance of infections and infection control practices.Infection control team (multidisciplinary approach).Antibiotic stewardship.Vaccination of health-care personnel.\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nSuggested ReadingMaselli DJ, Restrepo MI. Strategies in the prevention of ventilator-associated pneumonia. Ther Adv Respir Dis. 2011;5(2):131\u201341.\nVAP remains a significant problem in the hospital setting, with very high morbidity, mortality, and cost. This evidence-based review focuses clinically on relevant pharmacological and \u00adnonpharmacological interventions to prevent VAP.\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\n2.Coffin SE, Klompas M, Classen D, Arias KM, Podgorny K, Anderson DJ, Burstin H, Calfee DP, et al. Strategies to prevent ventilator-associated pneumonia in acute care hospitals. Infect Control Hosp Epidemiol. 2008;29(Suppl 1):S31\u201340.\nThis document gives practical recommendations in a concise format designed to assist acute care hospitals in implementing and prioritizing their VAP prevention efforts.\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\n3.Lorente L, Blot S, Rello J. Evidence on measures for the prevention of ventilator-associated pneumonia. Eur Respir J. 2007;30:1193\u2013207.\nEvidence-based guidelines have been issued by the European Task Force on ventilator-\u00adassociated pneumonia, the Centers for Disease Control and Prevention, the Canadian Critical Care Society, and also by the American Thoracic Society and Infectious Diseases Society of America, which have produced a joint set of recommendations. This review article is based on a comparison of these guidelines, together with an update of further publications in the literature.\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\nWebsites\n\nhttp://whqlibdoc.who.int/hq/2009/WHO_IER_PSP_2009.07_eng.pdf\nWHO guidelines on hand hygiene in health care: a summary\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\n2.\nhttp://www.cdc.gov/hicpac/pdf/guidelines/bsi-guidelines-2011.pdf\nGuidelines for the prevention of intravascular catheter-related infections\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\n3.\nhttp://www.cdc.gov/hicpac/pdf/CAUTI/CAUTIguideline2009final.pdf\nGuidelines for prevention of catheter-associated urinary tract infections\n",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        },
        {
            "text": "\n4.\nhttp://www.cdc.gov/hicpac/pdf/isolation/Isolation2007.pdf\nGuideline for isolation precautions: preventing transmission of infectious agents in healthcare settings",
            "cite_spans": [],
            "section": "\u2009",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 48.1: Hand hygiene technique (Adapted from WHO)",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {}
}