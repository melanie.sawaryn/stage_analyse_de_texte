{
    "paper_id": "PMC7091429",
    "metadata": {
        "title": "Neonates Investigated for Influenza-Like Illness During the Outbreak of Pandemic H1N1 2009: Trivial Infections But Major Triage Implications",
        "authors": [
            {
                "first": "Kam-lun",
                "middle": [
                    "Ellis"
                ],
                "last": "Hon",
                "suffix": "",
                "email": "ehon@cuhk.edu.hk",
                "affiliation": {}
            },
            {
                "first": "Kam",
                "middle": [
                    "Lau"
                ],
                "last": "Cheung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "William",
                "middle": [],
                "last": "Wong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Pak",
                "middle": [
                    "Cheung"
                ],
                "last": "Ng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "We reviewed neonates admitted to a teaching hospital for the investigation of possible respiratory viral infections based on fever or respiratory symptoms or contact history during the outbreak of pandemic H1N1 2009 in Hong Kong.",
            "cite_spans": [],
            "section": "Material and Methods",
            "ref_spans": []
        },
        {
            "text": "The Prince of Wales Hospital (PWH) is a university teaching hospital situated in the Eastern part of the New Territories in Hong Kong. The annual deliveries for this cluster were approximately 7000.",
            "cite_spans": [],
            "section": "Material and Methods",
            "ref_spans": []
        },
        {
            "text": "Eight neonates were admitted to the neonatal service from August 2009 through December 2009 for the investigation of possible respiratory viral infections based on fever or respiratory symptoms or contact history (Table 1). There was no pandemic H1N1 case but one case of Influenza A (H3N2).\n\n",
            "cite_spans": [],
            "section": "Material and Methods",
            "ref_spans": [
                {
                    "start": 220,
                    "end": 221,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "This 10-day-old neonate with mild sneezing but no contact history was admitted via the emergency department to the neonatal unit and was cared in an incubator. She was feeding well as usual and there was no cough or running nose. The mother thought that the child was \u201chot\u201d to touch. Physical examination was unremarkable except for a papular erythematous rash over her neck. Temperature was 37.5\u00b0C. Initial complete blood counts, C-reactive protein, cerebrospinal fluid analysis, urinalysis and chest radiograph were normal. She was put on intravenous antibiotics (penicillin and gentamicin) pending blood culture and nasopharyngeal aspirate (NPA) results. As this admission occurred during the HSI pandemic, the attending physician asked for the incubator to be placed in a corner away from the other patients (also in incubators) in the same cubicle. The parents were denied visiting until the NPA results were available, which turned out to be non-HSI influenza A on the same day. The neonate was promptly transferred to an isolation unit. She remained well, cultures were negative, the antibiotics discontinued 3 days later, and the child was discharged home. Serotyping subsequently showed that the virus was H3N2.",
            "cite_spans": [],
            "section": "Material and Methods",
            "ref_spans": []
        },
        {
            "text": "One girl was isolated immediately after delivery because the mother with Group B streptococcus was diagnosed with pandemic H1N1-09 during the peri-natal period. She remained well and had no evidence of H1N1 infection. Respiratory viruses were screened negative in two febrile neonates. Klebsiella pneumoniae was isolated in the urine of one neonate who had a brother with \u201cflu\u201d symptoms. He received a course of intravenous antibiotics for the urinary tract infection. The other neonate became afebrile spontaneously.",
            "cite_spans": [],
            "section": "Material and Methods",
            "ref_spans": []
        },
        {
            "text": "There was no outbreak of respiratory infections in the neonatal service during these admissions. Using an exact delivery rate of 6519 in 2009, the incidences of proven neonatal influenza A, parainfluenza and RSV admissions were 0.015%, 0.046% and 0.015% per annum, respectively.",
            "cite_spans": [],
            "section": "Material and Methods",
            "ref_spans": []
        },
        {
            "text": "In this series, respiratory viral infections occured within the first postnatal month. None of the neonates with proven respiratory viral infections were febrile. The symptoms of influenza or common respiratory viral infection in the neonate may be trivial and nonspecific, making prompt diagnosis difficult. This could create problems with triage at the emergency department. To our knowledge, these were among the youngest neonates with influenza A and respiratory viral infections reported in Hong Kong. The local policy is that an unwell neonate (with or without fever) will be admitted to the neonatal service in an incubator at a designated area pending nasopharyngeal swab for respiratory virus or other investigations if symptoms are non-respiratory or non-specific. The baby will be admitted to a neonatal intensive care service with negative-pressure isolation facility if he/she is critically ill. The patient will be admitted to the pediatric ICU instead if he/she is older than 28 days of age. Other option includes admission to a pediatric infection ward for respiratory viral infection if the child is less ill. Nevertheless, isolation facilities may be overwhelmed during an influenza epidemic. As H1N1 is not airborne, it is appropriate to monitor a neonate with suspected pandemic H1N1-09 in an incubator in designated cubicle pending rapid viral investigation. Interestingly, the pandemic H1N1-09 does not appear to be the prevalent respiratory virus in the neonates.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The initial management of a febrile neonate includes stabilization of vital signs and immediate initiation of broad empirical antimicrobial coverage until potentially serious bacterial infections are excluded [2, 3]. The use of antiviral agents for influenza A infection in the neonate is problematic [3]. Side effects with its usage in children have been reported [4]. Furthermore, there is no standard preparation for a neonate who obviously cannot swallow the capsular form of the medication. Adverse effects in the neonates are not known [4]. Another controversy is with the use of antiviral in parents, patients and staff who have cared for the patient [4]. Currently, oseltamirvir is optionally offered to symptomatic parents and staff.",
            "cite_spans": [
                {
                    "start": 210,
                    "end": 211,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 213,
                    "end": 214,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 302,
                    "end": 303,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 366,
                    "end": 367,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 543,
                    "end": 544,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 659,
                    "end": 660,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Acute respiratory infections and influenza may occasionally be very serious and fatal in children [2]. Pandemic influenza may well be more severe in the neonatal population because of the lack of antibody protection to a novel strain [5, 6]. The most important measures in a neonatal intensive care unit in the event of a pandemic are likely to be preventive ones. Influenza immunization, if available, is recommended only for infants \u22656 months [3].",
            "cite_spans": [
                {
                    "start": 99,
                    "end": 100,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 235,
                    "end": 236,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 238,
                    "end": 239,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 446,
                    "end": 447,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Since the SARS epidemic in 2003 [1, 7\u20139], the citizens of Hong Kong has become phobic to various infections. Outbreaks of infections in any institutions would hit news headlines. During a pandemic, the admission of a \u201cnot-so-ill\u201d neonate with a viral infection is like introducing a wolf covered with a lamb skin among a flock of lambs. Isolation of affected infants and strict adherence to infection-control precautions are critical to control spread within a neonatal unit.",
            "cite_spans": [
                {
                    "start": 33,
                    "end": 34,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 36,
                    "end": 37,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 38,
                    "end": 39,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table\u00a01: Neonates investigated for influenza-like illness in 2009\nGBS Group B streptococcus aUrine yielded Klebsiella sensitive to gentamicin",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Just like SARS",
            "authors": [
                {
                    "first": "KL",
                    "middle": [],
                    "last": "Hon",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Pediatr Pulmonol",
            "volume": "44",
            "issn": "",
            "pages": "1048-1049",
            "other_ids": {
                "DOI": [
                    "10.1002/ppul.21085"
                ]
            }
        },
        "BIBREF1": {
            "title": "Premorbid factors and outcome associated with respiratory virus infections in a pediatric intensive care unit",
            "authors": [
                {
                    "first": "KL",
                    "middle": [],
                    "last": "Hon",
                    "suffix": ""
                },
                {
                    "first": "TF",
                    "middle": [],
                    "last": "Leung",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Tang",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Pediatr Pulmonol",
            "volume": "43",
            "issn": "",
            "pages": "275-280",
            "other_ids": {
                "DOI": [
                    "10.1002/ppul.20768"
                ]
            }
        },
        "BIBREF2": {
            "title": "Influenza in the neonatal intensive care unit",
            "authors": [
                {
                    "first": "DJ",
                    "middle": [],
                    "last": "Wilkinson",
                    "suffix": ""
                },
                {
                    "first": "JP",
                    "middle": [],
                    "last": "Buttery",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Andersen",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Perinatol",
            "volume": "26",
            "issn": "",
            "pages": "772-776",
            "other_ids": {
                "DOI": [
                    "10.1038/sj.jp.7211625"
                ]
            }
        },
        "BIBREF3": {
            "title": "Oseltamivir adherence and side effects among children in three London schools affected by influenza A(H1N1)v, May 2009 - an Internet-based cross-sectional survey",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kitching",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Roche",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Balasegaram",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Heathcock",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Maguire",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Euro Surveill",
            "volume": "14",
            "issn": "30",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "An outbreak of influenza A in a neonatal intensive care unit",
            "authors": [
                {
                    "first": "RJ",
                    "middle": [],
                    "last": "Cunney",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Bialachowski",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Thornley",
                    "suffix": ""
                },
                {
                    "first": "FM",
                    "middle": [],
                    "last": "Smaill",
                    "suffix": ""
                },
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Pennie",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Infect Control Hosp Epidemiol",
            "volume": "21",
            "issn": "",
            "pages": "449-454",
            "other_ids": {
                "DOI": [
                    "10.1086/501786"
                ]
            }
        },
        "BIBREF5": {
            "title": "Influenza virus infections in infants",
            "authors": [
                {
                    "first": "WP",
                    "middle": [],
                    "last": "Glezen",
                    "suffix": ""
                },
                {
                    "first": "LH",
                    "middle": [],
                    "last": "Taber",
                    "suffix": ""
                },
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Frank",
                    "suffix": ""
                },
                {
                    "first": "WC",
                    "middle": [],
                    "last": "Gruber",
                    "suffix": ""
                },
                {
                    "first": "PA",
                    "middle": [],
                    "last": "Piedra",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Pediatr Infect Dis J",
            "volume": "16",
            "issn": "",
            "pages": "1065-1068",
            "other_ids": {
                "DOI": [
                    "10.1097/00006454-199711000-00012"
                ]
            }
        },
        "BIBREF6": {
            "title": "Clinical presentations and outcome of severe acute respiratory syndrome in children",
            "authors": [
                {
                    "first": "KL",
                    "middle": [],
                    "last": "Hon",
                    "suffix": ""
                },
                {
                    "first": "CW",
                    "middle": [],
                    "last": "Leung",
                    "suffix": ""
                },
                {
                    "first": "FT",
                    "middle": [],
                    "last": "Cheng",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1701-1703",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13364-8"
                ]
            }
        },
        "BIBREF7": {
            "title": "Personal view of SARS: confusing definition, confusing diagnoses",
            "authors": [
                {
                    "first": "KL",
                    "middle": [],
                    "last": "Hon",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "FW",
                    "middle": [],
                    "last": "Cheng",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1984-1985",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13556-8"
                ]
            }
        },
        "BIBREF8": {
            "title": "Severe childhood respiratory viral infections",
            "authors": [
                {
                    "first": "KL",
                    "middle": [],
                    "last": "Hon",
                    "suffix": ""
                },
                {
                    "first": "AK",
                    "middle": [],
                    "last": "Leung",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Adv Pediatr",
            "volume": "56",
            "issn": "",
            "pages": "47-73",
            "other_ids": {
                "DOI": [
                    "10.1016/j.yapd.2009.08.019"
                ]
            }
        }
    }
}