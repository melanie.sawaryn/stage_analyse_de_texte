{
    "paper_id": "PMC3901494",
    "metadata": {
        "title": "Genetic Characterization of Coronaviruses from Domestic Ferrets, Japan",
        "authors": [
            {
                "first": "Yutaka",
                "middle": [],
                "last": "Terada",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Shohei",
                "middle": [],
                "last": "Minami",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Keita",
                "middle": [],
                "last": "Noguchi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hassan",
                "middle": [
                    "Y.A.H."
                ],
                "last": "Mahmoud",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hiroshi",
                "middle": [],
                "last": "Shimoda",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Masami",
                "middle": [],
                "last": "Mochizuki",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yumi",
                "middle": [],
                "last": "Une",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ken",
                "middle": [],
                "last": "Maeda",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Fecal samples were collected during August 2012\u2013July 2013 from 79 ferrets from 10 animal hospitals scattered across 5 prefectures in Japan. Most of the ferrets were brought to veterinarians for clinical signs such as diarrhea, abdominal masses, and hypergammaglobulinemia; some had signs unrelated to coronavirus infection or were asymptomatic (Table 1). The diarrhea tended to be mild, unlike with ECE, and was found in coronavirus-negative and -positive animals. ",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 345,
                    "end": 352,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "RNA was extracted from fecal samples by using the QIAamp Viral RNA Mini Kit (QIAGEN, Hilden, Germany), and reverse transcription PCR (RT-PCR) was performed by using the QIAGEN OneStep RT-PCR Kit (QIAGEN) using coronavirus consensus primers IN-6 and IN-7, which amplify the open reading frame (ORF) 1b region, encoding RNA-dependent RNA polymerase (RdRp). This primer pair can amplify nucleic acids from many coronaviruses in the subfamily Coronavirinae (9). Of 79 samples, 33 (41.8%) were positive for coronaviruses by RT-PCR (Table 2). Nucleotide sequences were determined for the amplified fragments and used to construct a phylogenetic tree (Figure 1). The coronaviruses detected in this study belonged to the genus Alphacoronavirus but formed a separate species from those of other species. The identities with feline coronavirus, transmissible gastroenteritis virus, porcine respiratory coronavirus, and mink coronavirus were 73.5%\u201375.9%, 73.5%\u201376.1%, 73.8%\u201376.1%, and 80.2%\u201384.0%, respectively.",
            "cite_spans": [
                {
                    "start": 454,
                    "end": 455,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 645,
                    "end": 653,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 527,
                    "end": 534,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "On the basis of additional sequence data, a new primer pair was designed: forward FRCoV RdRp-F1 (5\u2032-GTT GGT TGC TGC ACA CAT AG-3\u2032) and reverse FRCoV RdRp-R1 (5\u2032-GGA GAA GTG CTT ACG CAA ATA-3\u2032). Results for RT-PCR using this new primer set showed that 44 (55.7%) of 79 samples were positive for coronavirus, which was a higher number than that obtained by using the published coronavirus consensus primers (55.7% vs. 41.8%) (Table 2). Two samples that had positive results by consensus primers had negative results by the new primers: sample 22 had many mutations in the primer binding site (Figure 1), whereas sample 40 had few mutations.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 591,
                    "end": 599,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 424,
                    "end": 431,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "On the basis of the partial sequences of the spike gene, Wise et al. (5) reported that the known ferret coronaviruses could be divided into 2 genotypes: genotype 1, which included the agent of FIP-like disease, and genotype 2, which included the causative agent of ECE. To differentiate between these genotypes in the positive samples from our testing, RT-PCR was carried out by using 2 pairs of genotype-specific primers: forward primer 5\u2032-CTG GTG TTT GTG CAA CAT CTA C-3\u2032 and reverse primer 5\u2032-TCT ATT TGC ACA AAA TCA GAC A-3\u2032 for genotype 1, and forward primer 5\u2032-GGC ATT TGT TTT GAT AAC GTT G-3\u2032 and reverse primer 5\u2032-CTA TTA ATT CGC ACG AAA TCT GC-3\u2032 for genotype 2 (5). Among these ferrets, 30 (38.0%) were infected with genotype 1 and 17 (21.5%) with genotype 2; 8 (10.1%) ferrets were infected with both genotypes of coronaviruses (Figure 2). Samples 27 and 28 were from ferrets that lived in the same house and harbored the same ferret coronavirus but that were born on different farms, indicating that horizontal transmission had occurred. The nucleotide sequences of the amplified genes confirmed that these coronaviruses also fell into genotypes 1 and 2 (Figure 2). ",
            "cite_spans": [
                {
                    "start": 70,
                    "end": 71,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 672,
                    "end": 673,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 840,
                    "end": 848,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 1167,
                    "end": 1175,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "Our results indicate that both genotypes of coronavirus have been spreading within the ferret population in Japan for some time, and some ferrets have been coincidentally infected with both genotypes. Of note, most ferrets that were positive for genotype 1 ferret coronavirus in this study did not show FIP-like disease (Table 1), indicating that infection with genotype 1 ferret coronavirus does not always cause FIP-like disease. Genotype 1 ferret coronavirus has also been detected from asymptomatic ferrets in the Netherlands (11).",
            "cite_spans": [
                {
                    "start": 531,
                    "end": 533,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 321,
                    "end": 328,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "To further investigate virus transmission routes, oral swab specimens were collected from 14 of the 79 ferrets and examined by RT-PCR using primers FRCoV RdRp-F1 and FRCoV RdRp-R1. Five (35.7%) specimens were positive (data not shown), providing a route leading to infection of susceptible animals. Coronaviruses are known to cause both respiratory and intestinal diseases in various animal species; therefore, ferret coronaviruses should be investigated in respiratory disease.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "We established a sensitive RT-PCR method using a new primer pair to detect coronavirus sequences and demonstrated that ferret coronaviruses are widespread among ferrets in Japan. We determined the partial nucleotide sequences of the spike gene of 23 strains and found they were clearly divided into 2 genotypes, 1 and 2 (Figure 2). The reported ferret coronaviruses associated with FIP-like disease, designated as genotype 1 by Wise et al. (5), all fell within genotype 1 phylogenetically, whereas all published ECE-causing strains fell within genotype 2. This finding leads to a possible conclusion that FIP-like disease\u2013causing strains (i.e., FRSCVs) are variants of what has been designated genotype 1 ferret coronaviruses. Because we found no relationship between the 2 genotypes of ferret coronavirus and the type of disease (Table 1), we cannot determine whether FIP-like and ECE-like ferret coronaviruses circulate independently as distinct entities or evolve, like feline coronaviruses, from more ubiquitous and less pathogenic enzootic strains. Nonetheless, the addition of these 23 new isolates to the phylogenetic tree of ferret coronaviruses tends to support the latter conclusion. Without extensive animal passage studies, virus isolation, and coronavirus-free ferrets, this theory may be difficult to confirm. However, additional evidence tends to link virulent pathotypes of ferret coronaviruses to specific mutational events. Nucleotide sequences of the 3c-like protein genes of FRSCV, MSU-1 (DDBJ/EMBL-Bank/GenBank accession no. GU338456), MSU-S (GU459059), and WADL (GU459058), showed that 2, MSU-1 and WADL, possessed a truncated 3c-like protein gene (5), similar to that described for FIP viruses of cats (12\u201314). FIP-causing viruses of cats also contain a second mutation in the spike gene (15), which was not investigated in our study. The existence of 2 major genotypes of Japanese ferret coronaviruses is also reminiscent of the serotype I and II feline coronaviruses. Without ferret coronaviruses that can be grown in cell culture, however, such serologic differentiation will be difficult.",
            "cite_spans": [
                {
                    "start": 441,
                    "end": 442,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1671,
                    "end": 1672,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1726,
                    "end": 1728,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1729,
                    "end": 1731,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1812,
                    "end": 1814,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": [
                {
                    "start": 321,
                    "end": 329,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 831,
                    "end": 838,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Phylogenetic tree constructed on the basis of the nucleotide sequences of the partial RNA-dependent RNA polymerase\u2013encoding regions of ferret coronaviruses (FRCoVs) isolated in Japan (shown in boldface; sample IDs are indicated) compared with other coronaviruses (CoVs). The tree was constructed by the neighbor-joining method in MEGA5.0 software (10); bootstrap values of >90 are shown. DDBJ/EMBL-Bank/GenBank accession numbers for the nucleotide sequences are shown in parentheses. Human CoVs (HCoVs) 229E and NL63, which belong to the Alphacoronavirus genus, were used as the outgroup. CCoV, canine coronavirus; FCoV, feline coronavirus; TGEV, transmissible gastroenteritis virus; PRCoV, porcine respiratory coronavirus. Scale bar indicates nucleotide substitutions per site.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Phylogenetic tree based on the nucleotide sequences of partial S genes of ferret coronaviruses (FRCoVs) isolated in Japan (shown in boldface; sample IDs are indicated) compared with other coronaviruses (CoVs). The tree was constructed by the neighbor-joining method in MEGA5.0 software (10); bootstrap values of >90 are shown. Asterisks indicate samples from ferrets infected with FRCoVs of both genotypes1 and 2. DDBJ/EMBL-Bank/GenBank accession numbers for the nucleotide sequences are shown in parentheses. FRSCV, ferret systemic coronavirus; FRECV, ferret enteric coronavirus. Scale bar indicates nucleotide substitutions per site.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Coronavirus-associated epizootic catarrhal enteritis in ferrets.",
            "authors": [],
            "year": 2000,
            "venue": "J Am Vet Med Assoc",
            "volume": "217",
            "issn": "",
            "pages": "526-30",
            "other_ids": {
                "DOI": [
                    "10.2460/javma.2000.217.526"
                ]
            }
        },
        "BIBREF1": {
            "title": "MEGA5: molecular evolutionary genetics analysis using maximum likelihood, evolutionary distance, and maximum parsimony methods.",
            "authors": [],
            "year": 2011,
            "venue": "Mol Biol Evol",
            "volume": "28",
            "issn": "",
            "pages": "2731-9",
            "other_ids": {
                "DOI": [
                    "10.1093/molbev/msr121"
                ]
            }
        },
        "BIBREF2": {
            "title": "Enteric coronavirus in ferrets, the Netherlands.",
            "authors": [],
            "year": 2011,
            "venue": "Emerg Infect Dis",
            "volume": "17",
            "issn": "",
            "pages": "1570-1",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Significance of coronavirus mutants in feces and diseased tissues of cats suffering from feline infectious peritonitis.",
            "authors": [],
            "year": 2009,
            "venue": "Viruses",
            "volume": "1",
            "issn": "",
            "pages": "166-84",
            "other_ids": {
                "DOI": [
                    "10.3390/v1020166"
                ]
            }
        },
        "BIBREF4": {
            "title": "Feline infectious peritonitis: insights into feline coronavirus pathobiogenesis and epidemiology based on genetic analysis of the viral 3c gene.",
            "authors": [],
            "year": 2010,
            "venue": "J Gen Virol",
            "volume": "91",
            "issn": "",
            "pages": "415-20",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.016485-0"
                ]
            }
        },
        "BIBREF5": {
            "title": "A review of feline infectious peritonitis virus infection: 1963\u20132008.",
            "authors": [],
            "year": 2009,
            "venue": "J Feline Med Surg",
            "volume": "11",
            "issn": "",
            "pages": "225-58",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jfms.2008.09.008"
                ]
            }
        },
        "BIBREF6": {
            "title": "Spike protein fusion peptide and feline coronavirus virulence.",
            "authors": [],
            "year": 2012,
            "venue": "Emerg Infect Dis",
            "volume": "18",
            "issn": "",
            "pages": "1089-95",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1807.120143"
                ]
            }
        },
        "BIBREF7": {
            "title": "Molecular characterization of a novel coronavirus associated with epizootic catarrhal enteritis (ECE) in ferrets.",
            "authors": [],
            "year": 2006,
            "venue": "Virology",
            "volume": "349",
            "issn": "",
            "pages": "164-74",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virol.2006.01.031"
                ]
            }
        },
        "BIBREF8": {
            "title": "Clinicopathologic features of a systemic coronavirus-associated disease resembling feline infectious peritonitis in the domestic ferret (Mustela putorius).",
            "authors": [],
            "year": 2008,
            "venue": "Vet Pathol",
            "volume": "45",
            "issn": "",
            "pages": "236-46",
            "other_ids": {
                "DOI": [
                    "10.1354/vp.45-2-236"
                ]
            }
        },
        "BIBREF9": {
            "title": "Detection of feline infectious peritonitis virus\u2013like antigen in ferrets.",
            "authors": [],
            "year": 2006,
            "venue": "Vet Rec",
            "volume": "158",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1136/vr.158.15.523-b"
                ]
            }
        },
        "BIBREF10": {
            "title": "Comparative sequence analysis of the distal one-third of the genomes of a systemic and an enteric ferret coronavirus.",
            "authors": [],
            "year": 2010,
            "venue": "Virus Res",
            "volume": "149",
            "issn": "",
            "pages": "42-50",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virusres.2009.12.011"
                ]
            }
        },
        "BIBREF11": {
            "title": "Systemic coronavirus-associated disease resembling feline infectious peritonitis in ferrets in the UK.",
            "authors": [],
            "year": 2012,
            "venue": "Vet Rec",
            "volume": "171",
            "issn": "",
            "pages": "200-1",
            "other_ids": {
                "DOI": [
                    "10.1136/vr.e5652"
                ]
            }
        },
        "BIBREF12": {
            "title": "Identification of group 1 coronavirus antigen in multisystemic granulomatous lesions in ferrets (Mustela putorius furo).",
            "authors": [],
            "year": 2008,
            "venue": "J Comp Pathol",
            "volume": "138",
            "issn": "",
            "pages": "54-8",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcpa.2007.10.002"
                ]
            }
        },
        "BIBREF13": {
            "title": "The first case of feline infectious peritonitis\u2013like pyogranuloma in a ferret infected by coronavirus in Japan.",
            "authors": [],
            "year": 2010,
            "venue": "J Toxicol Pathol",
            "volume": "23",
            "issn": "",
            "pages": "99-101",
            "other_ids": {
                "DOI": [
                    "10.1293/tox.23.99"
                ]
            }
        },
        "BIBREF14": {
            "title": "Identification of a novel coronavirus in bats.",
            "authors": [],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "2001-9",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.79.4.2001-2009.2005"
                ]
            }
        }
    }
}