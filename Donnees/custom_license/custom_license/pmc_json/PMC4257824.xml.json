{
    "paper_id": "PMC4257824",
    "metadata": {
        "title": "MERS Coronavirus Neutralizing Antibodies in Camels, Eastern Africa, 1983\u20131997",
        "authors": [
            {
                "first": "Marcel",
                "middle": [
                    "A."
                ],
                "last": "M\u00fcller",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Victor",
                "middle": [
                    "Max"
                ],
                "last": "Corman",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Joerg",
                "middle": [],
                "last": "Jores",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Benjamin",
                "middle": [],
                "last": "Meyer",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mario",
                "middle": [],
                "last": "Younan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anne",
                "middle": [],
                "last": "Liljander",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Berend-Jan",
                "middle": [],
                "last": "Bosch",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Erik",
                "middle": [],
                "last": "Lattwein",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mosaad",
                "middle": [],
                "last": "Hilali",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bakri",
                "middle": [
                    "E."
                ],
                "last": "Musa",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Set",
                "middle": [],
                "last": "Bornstein",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Christian",
                "middle": [],
                "last": "Drosten",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "A serum sample from each of 189 dromedary camels was collected by trained personnel as previously described (14). Blood samples were taken by jugular vein puncture. The blood was allowed to clot and subsequently centrifuged to obtain serum, or serum was separated from the coagulated blood during slaughter. All serum samples were heat-inactivated at 56\u00b0C for 30 min (14). Serum from Somalia was collected during 1983 and 1984; samples from Sudan were collected during June and July 1984, and samples from Egypt were collected during June and July 1997. All camels from Sudan were female (>6 years of age) and belonged to the Anafi breed. They were kept locally and used as a means of transport and a source of milk. The camels from Somalia were sampled at slaughterhouses in Afgoi and Mogadishu. Most camels were adults; however, detailed information about sex and age was not available. The camels from Somalia were bred predominantly for milk and meat. No background information was available for the camels from Egypt. Our study fully complied with national regulations and was approved by the ethics committee of the International Livestock Research Institute accredited by the National Council of Science and Technology in Kenya (approval no. ILRI-IREC2013\u201312).",
            "cite_spans": [
                {
                    "start": 109,
                    "end": 111,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 368,
                    "end": 370,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "We tested all serum samples for MERS-CoV antibodies at a 1:100 dilution by a recombinant MERS-CoV spike protein subunit 1\u2013based ELISA (rELISA) as previously described (3,12). To determine the assay-specific cutoff value, we tested 124 confirmed MERS-CoV antibody\u2013negative and 106 MERS-COV antibody\u2013positive camel serum samples from previous studies (3). For inter-assay calibration, we used the same selected positive serum samples in all applications. The optical density (OD) was measured at 450/605 nm. We determined the OD ratio by dividing the OD of each sample by the OD of the positive serum. The cutoff was defined as the 3-fold mean OD ratio of all tested MERS-CoV antibody\u2013negative serum samples (Technical Appendix Figure 1). To confirm antibody specificity and rule out possible cross-reactivity with other livestock-associated CoVs, we conducted a highly specific MERS-CoV microneutralization test (3,6). All serum samples were tested at a 1:80 dilution and at a 1:800 dilution to identify MERS-CoV neutralizing antibodies. Serum without neutralizing activity at 1:80 was rated MERS-CoV antibody negative.",
            "cite_spans": [
                {
                    "start": 168,
                    "end": 169,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 170,
                    "end": 172,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 350,
                    "end": 351,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 912,
                    "end": 913,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 914,
                    "end": 915,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "A total of 159 (84.1%; range among countries 80.0%\u201386.7%) of 189 dromedary camels were positive for MERS-CoV antibodies in the rELISA (Figure, Table 1). The highly specific neutralization test confirmed that 153 (81.0%; range 68.0%\u201386.9%) of camels had neutralizing activity with reciprocal titers of >80 (Table 1). Whereas most samples (124 [65.6%; range 56.0%\u201373.8%]) had reciprocal neutralizing titers of 80\u2013800, we detected high neutralizing titers of >800 in 29 (15.3%; range 11.6%\u201321.7%) samples (Table 2). Neutralizing titers correlated significantly (p<0.001, Kruskal-Wallis 1-way analysis of variance) with the determined OD ratios of the rELISA. The rELISA was 99.0% specific when correlated with the results of the microneutralization test (Technical Appendix Figure 2).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 135,
                    "end": 141,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 143,
                    "end": 150,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 306,
                    "end": 313,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 503,
                    "end": 510,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "MERS-CoV antibody\u2013carrying dromedaries were present in all 3 countries in 1983, 1984, and 1997 (Figure; Tables 1, 2). The high seropositivity in camels from Egypt (35 [81.4%] of 43), a country that imports camels from Sudan and Somalia, was consistent with previous studies (7,10). Strikingly, camels sampled in Somalia and Sudan >30 years ago were identified as MERS-CoV antibody positive with seropositivity of up to 86.7% in Sudan in 1983.",
            "cite_spans": [
                {
                    "start": 275,
                    "end": 276,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 277,
                    "end": 279,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 96,
                    "end": 102,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 104,
                    "end": 112,
                    "mention": "Tables 1",
                    "ref_id": null
                },
                {
                    "start": 114,
                    "end": 115,
                    "mention": "2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Our study complements and supports the latest findings on long-term and widespread circulation of MERS-CoV or MERS-like CoV in dromedaries in Africa (3,7,9,10). By identifying neutralizing antibodies for MERS-CoV in Somalia dromedaries, we provided data for the country lodging the world's largest camel population and from which many camels are exported to Saudi Arabia (13). The large proportion of adult animals tested in this study explains the high seropositivity (>80%) and agrees with previous observations (3,6,12). Earlier reports provided evidence for seropositive camels in Kenya and Saudi Arabia dating to the early 1990s (3,4). Here we describe the presence of anti-MERS-CoV antibodies in archived serum collected >30 years ago, increasing the timescale for detection by an additional decade. Long-term circulation of MERS-CoV or MERS-like CoV in dromedaries can therefore be hypothesized. As suggested, an important factor possibly contributing to continuous virus maintenance in camels could be a high camel population density combined with nomadic husbandry, including frequent contact among camel herds in the Greater Horn of Africa (3).",
            "cite_spans": [
                {
                    "start": 150,
                    "end": 151,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 152,
                    "end": 153,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 154,
                    "end": 155,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 156,
                    "end": 158,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 372,
                    "end": 374,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 515,
                    "end": 516,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 517,
                    "end": 518,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 519,
                    "end": 521,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 635,
                    "end": 636,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 637,
                    "end": 638,
                    "mention": "4",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1151,
                    "end": 1152,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "MERS-CoV sequences from camels in Saudi Arabia and Qatar were closely related to sequences found in humans and did not show major genetic variability that would support long-term evolution of MERS-CoV in camels (10,11). The MERS-CoV sequence from a camel in Egypt was phylogenetically most distantly related to all other known camel-associated MERS-CoVs but closely related to the early human MERS-CoV isolates (10). An urgent task would be to characterize the diversity of MERS-related CoV in other camels in Africa to elucidate whether the current epidemic MERS-CoV strains have evolved toward more efficient human transmissibility.",
            "cite_spans": [
                {
                    "start": 212,
                    "end": 214,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 215,
                    "end": 217,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 412,
                    "end": 414,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "The existence of unrecognized human infections in African or Arabian countries in the past cannot be ruled out. Resource-limited African countries that have been exposed to civil unrest, such as Somalia and Sudan, are not likely to diagnose and report diagnostically challenging infections resembling other diseases. The lack of MERS-CoV antibodies in a small cohort serosurvey in Saudi Arabia did not suggest the long-term circulation of MERS-CoV in humans on the Arabian Peninsula (15). Large serosurveys in countries where camels are bred and traded, especially in eastern Africa, are needed to explore the general MERS-CoV seroprevalence in camels and humans, particularly humans who have close contact with camels. Such serosurveys could provide the data needed to ascertain whether MERS-CoV has been introduced into, but unrecognized in, the human population on the African continent.",
            "cite_spans": [
                {
                    "start": 484,
                    "end": 486,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure: Arabian Peninsula and neighboring countries of the Greater Horn of Africa in 2014. The study sites Egypt, Sudan (separated into Sudan and South Sudan), and Somalia are in dark orange and labeled with the year the camels were sampled, the number of samples, and the percentage of samples that were reactive in the MERS-CoV ELISA. Countries with previously reported MERS-CoV seropositive dromedaries are in light orange (overlap shown in stripes).",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Isolation of a novel coronavirus from a man with pneumonia in Saudi Arabia.",
            "authors": [],
            "year": 2012,
            "venue": "N Engl J Med",
            "volume": "367",
            "issn": "",
            "pages": "1814-20",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1211721"
                ]
            }
        },
        "BIBREF1": {
            "title": "MERS coronaviruses in dromedary camels, Egypt.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "1049-53",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2006.140299"
                ]
            }
        },
        "BIBREF2": {
            "title": "Middle East respiratory syndrome coronavirus in dromedary camels: an outbreak investigation.",
            "authors": [],
            "year": 2014,
            "venue": "Lancet Infect Dis",
            "volume": "14",
            "issn": "",
            "pages": "140-5",
            "other_ids": {
                "DOI": [
                    "10.1016/S1473-3099(13)70690-X"
                ]
            }
        },
        "BIBREF3": {
            "title": "Human infection with MERS coronavirus after exposure to infected camels, Saudi Arabia, 2013.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "1012-5",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2006.140402"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Prevalence of antibodies to some viral pathogens, Brucella abortus and Toxoplasma gondii in serum from camels (Camelus dromedarius) in Sudan.",
            "authors": [],
            "year": 1987,
            "venue": "Zentralbl Veterinarmed B",
            "volume": "34",
            "issn": "",
            "pages": "364-70",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Investigation of anti\u2013Middle East respiratory syndrome antibodies in blood donors and slaughterhouse workers in Jeddah and Makkah, Saudi Arabia, fall 2012.",
            "authors": [],
            "year": 2014,
            "venue": "J Infect Dis",
            "volume": "209",
            "issn": "",
            "pages": "243-6",
            "other_ids": {
                "DOI": [
                    "10.1093/infdis/jit589"
                ]
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.3201/eid2008.140596"
                ]
            }
        },
        "BIBREF9": {
            "title": "Seroepidemiology of Middle East respiratory syndrome (MERS) coronavirus in Saudi Arabia (1993) and Australia (2014) and characterisation of assay specificity.",
            "authors": [],
            "year": 2014,
            "venue": "Euro Surveill",
            "volume": "19",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "MERS coronavirus in dromedary camel herd, Saudi Arabia.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.3201/eid2007.140571"
                ]
            }
        },
        "BIBREF11": {
            "title": "Antibodies against MERS coronavirus in dromedaries, United Arab Emirates, 2003 and 2013.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "552-9",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2004.131746"
                ]
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Middle East respiratory syndrome coronavirus neutralising serum antibodies in dromedary camels: a comparative serological study.",
            "authors": [],
            "year": 2013,
            "venue": "Lancet Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "859-66",
            "other_ids": {
                "DOI": [
                    "10.1016/S1473-3099(13)70164-6"
                ]
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.3201/eid2008.140590"
                ]
            }
        }
    }
}