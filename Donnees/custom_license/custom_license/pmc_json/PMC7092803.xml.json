{
    "paper_id": "PMC7092803",
    "metadata": {
        "title": "A Novel Coronavirus from Patients with Pneumonia in China, 2019",
        "authors": [
            {
                "first": "Na",
                "middle": [],
                "last": "Zhu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Dingyu",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wenling",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xingwang",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bo",
                "middle": [],
                "last": "Yang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jingdong",
                "middle": [],
                "last": "Song",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xiang",
                "middle": [],
                "last": "Zhao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Baoying",
                "middle": [],
                "last": "Huang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Weifeng",
                "middle": [],
                "last": "Shi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Roujian",
                "middle": [],
                "last": "Lu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peihua",
                "middle": [],
                "last": "Niu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Faxian",
                "middle": [],
                "last": "Zhan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xuejun",
                "middle": [],
                "last": "Ma",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Dayan",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wenbo",
                "middle": [],
                "last": "Xu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Guizhen",
                "middle": [],
                "last": "Wu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "George",
                "middle": [
                    "F."
                ],
                "last": "Gao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wenjie",
                "middle": [],
                "last": "Tan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Four lower respiratory tract samples, including bronchoalveolar-lavage fluid, were collected from patients with pneumonia of unknown cause who were identified in Wuhan on December 21, 2019, or later and who had been present at the Huanan Seafood Market close to the time of their clinical presentation. Seven bronchoalveolar-lavage fluid specimens were collected from patients in Beijing hospitals with pneumonia of known cause to serve as control samples. Extraction of nucleic acids from clinical samples (including uninfected cultures that served as negative controls) was performed with a High Pure Viral Nucleic Acid Kit, as described by the manufacturer (Roche). Extracted nucleic acid samples were tested for viruses and bacteria by polymerase chain reaction (PCR), using the RespiFinderSmart22kit (PathoFinder BV) and the LightCycler 480 real-time PCR system, in accordance with manufacturer instructions.12 Samples were analyzed for 22 pathogens (18 viruses and 4 bacteria) as detailed in the Supplementary Appendix. In addition, unbiased, high-throughput sequencing, described previously,13 was used to discover microbial sequences not identifiable by the means described above. A real-time reverse transcription PCR (RT-PCR) assay was used to detect viral RNA by targeting a consensus RdRp region of pan \u03b2-CoV, as described in the Supplementary Appendix.",
            "cite_spans": [
                {
                    "start": 913,
                    "end": 915,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1098,
                    "end": 1100,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Viral Diagnostic Methods ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Bronchoalveolar-lavage fluid samples were collected in sterile cups to which virus transport medium was added. Samples were then centrifuged to remove cellular debris. The supernatant was inoculated on human airway epithelial cells,13 which had been obtained from airway specimens resected from patients undergoing surgery for lung cancer and were confirmed to be special-pathogen-free by NGS.14",
            "cite_spans": [
                {
                    "start": 232,
                    "end": 234,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 393,
                    "end": 395,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Isolation of Virus ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Human airway epithelial cells were expanded on plastic substrate to generate passage-1 cells and were subsequently plated at a density of 2.5\u00d7105 cells per well on permeable Transwell-COL (12-mm diameter) supports. Human airway epithelial cell cultures were generated in an air\u2013liquid interface for 4 to 6 weeks to form well-differentiated, polarized cultures resembling in vivo pseudostratified mucociliary epithelium.13",
            "cite_spans": [
                {
                    "start": 419,
                    "end": 421,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Isolation of Virus ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Prior to infection, apical surfaces of the human airway epithelial cells were washed three times with phosphate-buffered saline; 150 \u03bcl of supernatant from bronchoalveolar-lavage fluid samples was inoculated onto the apical surface of the cell cultures. After a 2-hour incubation at 37\u00b0C, unbound virus was removed by washing with 500 \u03bcl of phosphate-buffered saline for 10 minutes; human airway epithelial cells were maintained in an air\u2013liquid interface incubated at 37\u00b0C with 5% carbon dioxide. Every 48 hours, 150 \u03bcl of phosphate-buffered saline was applied to the apical surfaces of the human airway epithelial cells, and after 10 minutes of incubation at 37\u00b0C the samples were harvested. Pseudostratified mucociliary epithelium cells were maintained in this environment; apical samples were passaged in a 1:3 diluted vial stock to new cells. The cells were monitored daily with light microscopy, for cytopathic effects, and with RT-PCR, for the presence of viral nucleic acid in the supernatant. After three passages, apical samples and human airway epithelial cells were prepared for transmission electron microscopy.",
            "cite_spans": [],
            "section": "Isolation of Virus ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Supernatant from human airway epithelial cell cultures that showed cytopathic effects was collected, inactivated with 2% paraformaldehyde for at least 2 hours, and ultracentrifuged to sediment virus particles. The enriched supernatant was negatively stained on film-coated grids for examination. Human airway epithelial cells showing cytopathic effects were collected and fixed with 2% paraformaldehyde\u20132.5% glutaraldehyde and were then fixed with 1% osmium tetroxide dehydrated with grade ethanol embedded with PON812 resin. Sections (80 nm) were cut from resin block and stained with uranyl acetate and lead citrate, separately. The negative stained grids and ultrathin sections were observed under transmission electron microscopy.",
            "cite_spans": [],
            "section": "Transmission Electron Microscopy ::: Methods",
            "ref_spans": []
        },
        {
            "text": "RNA extracted from bronchoalveolar-lavage fluid and culture supernatants was used as a template to clone and sequence the genome. We used a combination of Illumina sequencing and nanopore sequencing to characterize the virus genome. Sequence reads were assembled into contig maps (a set of overlapping DNA segments) with the use of CLC Genomics software, version 4.6.1 (CLC Bio). Specific primers were subsequently designed for PCR, and 5\u2032- or 3\u2032-RACE (rapid amplification of cDNA ends) was used to fill genome gaps from conventional Sanger sequencing. These PCR products were purified from gels and sequenced with a BigDye Terminator v3.1 Cycle Sequencing Kit and a 3130XL Genetic Analyzer, in accordance with the manufacturers\u2019 instructions.",
            "cite_spans": [],
            "section": "Viral Genome Sequencing ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Multiple-sequence alignment of the 2019-nCoV and reference sequences was performed with the use of Muscle. Phylogenetic analysis of the complete genomes was performed with RAxML (13) with 1000 bootstrap replicates and a general time-reversible model used as the nucleotide substitution model.",
            "cite_spans": [],
            "section": "Viral Genome Sequencing ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Three adult patients presented with severe pneumonia and were admitted to a hospital in Wuhan on December 27, 2019. Patient 1 was a 49-year-old woman, Patient 2 was a 61-year-old man, and Patient 3 was a 32-year-old man. Clinical profiles were available for Patients 1 and 2. Patient 1 reported having no underlying chronic medical conditions but reported fever (temperature, 37\u00b0C to 38\u00b0C) and cough with chest discomfort on December 23, 2019. Four days after the onset of illness, her cough and chest discomfort worsened, but the fever was reduced; a diagnosis of pneumonia was based on computed tomographic (CT) scan. Her occupation was retailer in the seafood wholesale market. Patient 2 initially reported fever and cough on December 20, 2019; respiratory distress developed 7 days after the onset of illness and worsened over the next 2 days (see chest radiographs, Figure 1), at which time mechanical ventilation was started. He had been a frequent visitor to the seafood wholesale market. Patients 1 and 3 recovered and were discharged from the hospital on January 16, 2020. Patient 2 died on January 9, 2020. No biopsy specimens were obtained.",
            "cite_spans": [],
            "section": "Patients ::: Results",
            "ref_spans": [
                {
                    "start": 871,
                    "end": 879,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Three bronchoalveolar-lavage samples were collected from Wuhan Jinyintan Hospital on December 30, 2019. No specific pathogens (including HCoV-229E, HCoV-NL63, HCoV-OC43, and HCoV-HKU1) were detected in clinical specimens from these patients by the RespiFinderSmart22kit. RNA extracted from bronchoalveolar-lavage fluid from the patients was used as a template to clone and sequence a genome using a combination of Illumina sequencing and nanopore sequencing. More than 20,000 viral reads from individual specimens were obtained, and most contigs matched to the genome from lineage B of the genus betacoronavirus \u2014 showing more than 85% identity with a bat SARS-like CoV (bat-SL-CoVZC45, MG772933.1) genome published previously. Positive results were also obtained with use of a real-time RT-PCR assay for RNA targeting to a consensus RdRp region of pan \u03b2-CoV (although the cycle threshold value was higher than 34 for detected samples). Virus isolation from the clinical specimens was performed with human airway epithelial cells and Vero E6 and Huh-7 cell lines. The isolated virus was named 2019-nCoV.",
            "cite_spans": [],
            "section": "Detection and Isolation of a Novel Coronavirus ::: Results",
            "ref_spans": []
        },
        {
            "text": "To determine whether virus particles could be visualized in 2019-nCoV\u2013infected human airway epithelial cells, mock-infected and 2019-nCoV\u2013infected human airway epithelial cultures were examined with light microscopy daily and with transmission electron microscopy 6 days after inoculation. Cytopathic effects were observed 96 hours after inoculation on surface layers of human airway epithelial cells; a lack of cilium beating was seen with light microcopy in the center of the focus (Figure 2). No specific cytopathic effects were observed in the Vero E6 and Huh-7 cell lines until 6 days after inoculation.",
            "cite_spans": [],
            "section": "Detection and Isolation of a Novel Coronavirus ::: Results",
            "ref_spans": [
                {
                    "start": 485,
                    "end": 493,
                    "mention": "Figure 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Electron micrographs of negative-stained 2019-nCoV particles were generally spherical with some pleomorphism (Figure 3). Diameter varied from about 60 to 140 nm. Virus particles had quite distinctive spikes, about 9 to 12 nm, and gave virions the appearance of a solar corona. Extracellular free virus particles and inclusion bodies filled with virus particles in membrane-bound vesicles in cytoplasm were found in the human airway epithelial ultrathin sections. This observed morphology is consistent with the Coronaviridae family.",
            "cite_spans": [],
            "section": "Detection and Isolation of a Novel Coronavirus ::: Results",
            "ref_spans": [
                {
                    "start": 110,
                    "end": 118,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "To further characterize the virus, de novo sequences of 2019-nCoV genome from clinical specimens (bronchoalveolar-lavage fluid) and human airway epithelial cell virus isolates were obtained by Illumina and nanopore sequencing. The novel coronavirus was identified from all three patients. Two nearly full-length coronavirus sequences were obtained from bronchoalveolar-lavage fluid (BetaCoV/Wuhan/IVDC-HB-04/2020, BetaCoV/Wuhan/IVDC-HB-05/2020|EPI_ISL_402121), and one full-length sequence was obtained from a virus isolated from a patient (BetaCoV/Wuhan/IVDC-HB-01/2020|EPI_ISL_402119). Complete genome sequences of the three novel coronaviruses were submitted to GISAID (BetaCoV/Wuhan/IVDC-HB-01/2019, accession ID: EPI_ISL_402119; BetaCoV/Wuhan/IVDC-HB-04/2020, accession ID: EPI_ISL_402120; BetaCoV/Wuhan/IVDC-HB-05/2019, accession ID: EPI_ISL_402121) and have a 86.9% nucleotide sequence identity to a previously published bat SARS-like CoV (bat-SL-CoVZC45, MG772933.1) genome. The three 2019-nCoV genomes clustered together within the sarbecovirus subgenus, which shows the typical betacoronavirus organization: a 5\u2032 untranslated region (UTR), replicase complex (orf1ab), S gene, E gene, M gene, N gene, 3\u2032 UTR, and several unidentified nonstructural open reading frames.",
            "cite_spans": [],
            "section": "Detection and Isolation of a Novel Coronavirus ::: Results",
            "ref_spans": []
        },
        {
            "text": "Although 2019-nCoV is similar to some betacoronaviruses detected in bats (Figure 4), it is distinct from SARS-CoV and MERS-CoV. The three 2019-nCoV coronaviruses from Wuhan, together with two bat-derived SARS-like strains, ZC45 and ZXC21, form a distinct clade. SARS-CoV strains from humans and genetically similar SARS-like coronaviruses from bats collected from southwestern China formed another clade within the subgenus sarbecovirus. Since the sequence identity in conserved replicase domains (ORF 1ab) is less than 90% between 2019-nCoV and other members of betacoronavirus, the 2019-nCoV \u2014 the likely causative agent of the viral pneumonia in Wuhan \u2014 is a novel betacoronavirus belonging to the sarbecovirus subgenus of Coronaviridae family.",
            "cite_spans": [],
            "section": "Detection and Isolation of a Novel Coronavirus ::: Results",
            "ref_spans": [
                {
                    "start": 74,
                    "end": 82,
                    "mention": "Figure 4",
                    "ref_id": "FIGREF3"
                }
            ]
        },
        {
            "text": "We report a novel CoV (2019-nCoV) that was identified in hospitalized patients in Wuhan, China, in December 2019 and January 2020. Evidence for the presence of this virus includes identification in bronchoalveolar-lavage fluid in three patients by whole-genome sequencing, direct PCR, and culture. The illness likely to have been caused by this CoV was named \u201cnovel coronavirus-infected pneumonia\u201d (NCIP). Complete genomes were submitted to GISAID. Phylogenetic analysis revealed that 2019-nCoV falls into the genus betacoronavirus, which includes coronaviruses (SARS-CoV, bat SARS-like CoV, and others) discovered in humans, bats, and other wild animals.15 We report isolation of the virus and the initial description of its specific cytopathic effects and morphology.",
            "cite_spans": [
                {
                    "start": 655,
                    "end": 657,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Molecular techniques have been used successfully to identify infectious agents for many years. Unbiased, high-throughput sequencing is a powerful tool for the discovery of pathogens.14,16 Next-generation sequencing and bioinformatics are changing the way we can respond to infectious disease outbreaks, improving our understanding of disease occurrence and transmission, accelerating the identification of pathogens, and promoting data sharing. We describe in this report the use of molecular techniques and unbiased DNA sequencing to discover a novel betacoronavirus that is likely to have been the cause of severe pneumonia in three patients in Wuhan, China.",
            "cite_spans": [
                {
                    "start": 182,
                    "end": 184,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 185,
                    "end": 187,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Although establishing human airway epithelial cell cultures is labor intensive, they appear to be a valuable research tool for analysis of human respiratory pathogens.13 Our study showed that initial propagation of human respiratory secretions onto human airway epithelial cell cultures, followed by transmission electron microscopy and whole genome sequencing of culture supernatant, was successfully used for visualization and detection of new human coronavirus that can possibly elude identification by traditional approaches.",
            "cite_spans": [
                {
                    "start": 167,
                    "end": 169,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Further development of accurate and rapid methods to identify unknown respiratory pathogens is still needed. On the basis of analysis of three complete genomes obtained in this study, we designed several specific and sensitive assays targeting ORF1ab, N, and E regions of the 2019-nCoV genome to detect viral RNA in clinical specimens. The primer sets and standard operating procedures have been shared with the World Health Organization and are intended for surveillance and detection of 2019-nCoV infection globally and in China. More recent data show 2019-nCoV detection in 830 persons in China.17",
            "cite_spans": [
                {
                    "start": 598,
                    "end": 600,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Although our study does not fulfill Koch\u2019s postulates, our analyses provide evidence implicating 2019-nCoV in the Wuhan outbreak. Additional evidence to confirm the etiologic significance of 2019-nCoV in the Wuhan outbreak include identification of a 2019-nCoV antigen in the lung tissue of patients by immunohistochemical analysis, detection of IgM and IgG antiviral antibodies in the serum samples from a patient at two time points to demonstrate seroconversion, and animal (monkey) experiments to provide evidence of pathogenicity. Of critical importance are epidemiologic investigations to characterize transmission modes, reproduction interval, and clinical spectrum resulting from infection to inform and refine strategies that can prevent, control, and stop the spread of 2019-nCoV.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Shown are chest radiographs from Patient 2 on days 8 and 11 after the onset of illness. The trachea was intubated and mechanical ventilation instituted in the period between the acquisition of the two images. Bilateral fluffy opacities are present in both images but are increased in density, profusion, and confluence in the second image; these changes are most marked in the lower lung fields. Changes consistent with the accumulation of pleural liquid are also visible in the second image.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3: Negative-stained 2019-nCoV particles are shown in Panel A, and 2019-nCoV particles in the human airway epithelial cell ultrathin sections are shown in Panel B. Arrowheads indicate extracellular virus particles, arrows indicate inclusion bodies formed by virus components, and triangles indicate cilia.",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Figure 4: Shown are a schematic of 2019-nCoV (Panel A) and full-length phylogenetic analysis of 2019-nCoV and other betacoronavirus genomes in the Orthocoronavirinae subfamily (Panel B).",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "From \u201cA\u201dIV to \u201cZ\u201dIKV: attacks from emerging and re-emerging pathogens.",
            "authors": [],
            "year": 2018,
            "venue": "Cell",
            "volume": "172",
            "issn": "",
            "pages": "1157-1159",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "MERS, SARS, and Ebola: the role of super-spreaders in infectious disease.",
            "authors": [],
            "year": 2015,
            "venue": "Cell Host Microbe",
            "volume": "18",
            "issn": "",
            "pages": "398-401",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Viral and bacterial etiology of acute febrile respiratory syndrome among patients in Qinghai, China.",
            "authors": [],
            "year": 2019,
            "venue": "Biomed Environ Sci",
            "volume": "32",
            "issn": "",
            "pages": "438-445",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Coronaviruses and the human airway: a universal system for virus-host interaction studies.",
            "authors": [],
            "year": 2016,
            "venue": "Virol J",
            "volume": "13",
            "issn": "",
            "pages": "24-24",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "A new arenavirus in a cluster of fatal transplant-associated diseases.",
            "authors": [],
            "year": 2008,
            "venue": "N Engl J Med",
            "volume": "358",
            "issn": "",
            "pages": "991-998",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "A novel coronavirus genome identified in a cluster of pneumonia cases \u2014 Wuhan, China 2019\u22122020.",
            "authors": [],
            "year": 2020,
            "venue": "China CDC Weekly",
            "volume": "2",
            "issn": "",
            "pages": "61-62",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Pathogen genomics in public health.",
            "authors": [],
            "year": 2019,
            "venue": "N Engl J Med",
            "volume": "381",
            "issn": "",
            "pages": "2569-2580",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Coronavirus pathogenesis.",
            "authors": [],
            "year": 2011,
            "venue": "Adv Virus Res",
            "volume": "81",
            "issn": "",
            "pages": "85-164",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Epidemiology, genetic recombination, and pathogenesis of coronaviruses.",
            "authors": [],
            "year": 2016,
            "venue": "Trends Microbiol",
            "volume": "24",
            "issn": "",
            "pages": "490-502",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Origin and evolution of pathogenic coronaviruses.",
            "authors": [],
            "year": 2019,
            "venue": "Nat Rev Microbiol",
            "volume": "17",
            "issn": "",
            "pages": "181-192",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Epidemiology and cause of severe acute respiratory syndrome (SARS) in Guangdong, People\u2019s Republic of China, in February, 2003.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "362",
            "issn": "",
            "pages": "1353-1358",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "A novel coronavirus associated with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1953-1966",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Identification of a novel coronavirus in patients with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1967-1976",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Isolation of a novel coronavirus from a man with pneumonia in Saudi Arabia.",
            "authors": [],
            "year": 2012,
            "venue": "N Engl J Med",
            "volume": "367",
            "issn": "",
            "pages": "1814-1820",
            "other_ids": {
                "DOI": []
            }
        }
    }
}