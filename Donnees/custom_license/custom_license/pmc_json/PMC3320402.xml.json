{
    "paper_id": "PMC3320402",
    "metadata": {
        "title": "SARS in Three Categories of Hospital Workers, Hong Kong",
        "authors": [
            {
                "first": "Joseph",
                "middle": [
                    "T.F."
                ],
                "last": "Lau",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xilin",
                "middle": [],
                "last": "Yang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ping-Chung",
                "middle": [],
                "last": "Leung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Louis",
                "middle": [],
                "last": "Chan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Eliza",
                "middle": [],
                "last": "Wong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Carmen",
                "middle": [],
                "last": "Fong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hi-Yi",
                "middle": [],
                "last": "Tsui",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "For all hospitals that had admitted SARS patients in Hong Kong, the numbers of probable SARS patients and of hospital staff in three job categories were obtained. These three categories included nurses (group N), nonmedical support staff (group S; healthcare assistants, ward assistants [cleaning staff], general service assistant [clerical staff]), and other medical or technical staff (group O; physicians, allied health workers, technicians, pharmacists, dieticians, radiologists, radiographers, and medical students, and the like). All were full-time staff. The Hospital Authority and individual hospitals kept lists of infected workers who were hospital staff members. These lists were provided to the authors, with data already grouped into the three categories and the two time periods; no further breakdown of the data was available. Most data were obtained from the Hospital Authority; supplementary data were obtained from a few hospitals. The number of these three types of workers who became probable SARS patients, according to the World Health Organization definition (1), was recorded. These figures were further stratified into two groups: patients whose onset of symptoms occurred 1) before April 17, or 2) on or after April 17, which was approximately the mid-point of the epidemic. (The first patient was admitted on March 4, 2003, and the onset of the last case was on May 31, 2003). Attack rates for the three categories of hospital workers were obtained by dividing the relevant number of hospital care workers contracting SARS by the total number of relevant staff members.",
            "cite_spans": [
                {
                    "start": 1083,
                    "end": 1084,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "Chi-square test and Fisher exact test were used to test the significance of differences in proportions. Spearman correlation analysis was performed to examine the association between the number of SARS patients admitted into a hospital and the number of healthcare workers who contracted SARS in the same hospital. Analysis of variance (ANOVA) and Kruskal-Wallis test were used to compare differences in attack rates among the three types of workers. SPSS for Windows Release 11.0.1 (SPSS Inc., Chicago, IL) was used for the data analysis; p < 0.05 was considered to be significant. Differences in attack rates among the 16 hospitals were tested by using Fisher-Freeman-Halton test (StatXact-4 version 4.0.1, Cytel Software Corporation, Cambridge, MA).",
            "cite_spans": [],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "A total of 1,755 SARS patients were reported in Hong Kong; they were hospitalized in 16 of the 27 hospitals governed by the Hospital Authority. Fourteen of these 16 hospitals had at least one hospital staff member who contracted SARS. In other words, 2 of the 16 hospitals (hospital 2 and hospital 4, which admitted 7 and 17 SARS patients, respectively) had a zero attack rate (Table 1).",
            "cite_spans": [],
            "section": "Infected Staff and Attack Rates ::: Results",
            "ref_spans": [
                {
                    "start": 378,
                    "end": 385,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The total number of affected hospital workers in these 16 hospitals was 339 (i.e., 94.2% of all 360 affected hospital workers in Hong Kong). The other 21 (5.8%) affected hospital staff worked in six other hospitals that had not admitted SARS patients. The distribution of the 339 cases is analyzed in this article.",
            "cite_spans": [],
            "section": "Infected Staff and Attack Rates ::: Results",
            "ref_spans": []
        },
        {
            "text": "The number of affected staff in 2 hospitals (hospitals 15 and 16) accounted for 54.0% of the 339 cases in the 16 Health Authority hospitals (Table 1). The number of affected staff in an individual hospital ranged from 0 to 120 (median = 11, interquartile range = 24.8) (Table 1). The overall attack rates for all three types of hospital staff was 1.20% for the 16 hospitals. These rates ranged from 0% to 4.38%, a significant variation (p < 0.001, Table 1). The overall mean and median of the 16 hospital attack rates for all workers were 1.06% and 0.59%, respectively (Table 2). The overall attack rates for all workers were >2% in three hospitals (hospitals 12, 15, and 16; Table 1). When these three hospitals were removed from the analysis, the overall attack rate was 0.54% and the mean and median of the 13 hospital attack rates were 0.48% and 0.54%, respectively (data not shown in table).",
            "cite_spans": [],
            "section": "Infected Staff and Attack Rates ::: Results",
            "ref_spans": [
                {
                    "start": 141,
                    "end": 148,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 270,
                    "end": 277,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 448,
                    "end": 455,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 570,
                    "end": 577,
                    "mention": "Table 2",
                    "ref_id": null
                },
                {
                    "start": 676,
                    "end": 683,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Attack rates in the three job category groups (group N, S, and O) of hospital workers are listed in Table 1. The ranges of attack rates for the three groups were 0%-4.66% (group N), 0.0%-13.3% (group S), and 0.0%-1.53% (group O). The pooled attack rates for these three groups were 1.21%, 2.73%, and 0.29%, respectively, in the 16 hospitals (Table 1). ORs for comparing the S and N, O and N, and S and O groups were 2.30 (p < 0.001), 0.24 (p < 0.001), and 9.78 (p < 0.001), respectively. The differences in both the mean and median attack rates for the three categories were also significant (p = 0.035, ANOVA test, and p = 0.015 and p = 0.015, Kruskal-Wallis test) (Table 2).",
            "cite_spans": [],
            "section": "Attack Rates by Category of Hospital Worker ::: Results",
            "ref_spans": [
                {
                    "start": 100,
                    "end": 107,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 342,
                    "end": 349,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 667,
                    "end": 674,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The number of affected staff was strongly correlated with the number of admitted SARS patients for all the three groups: group N (Spearman r = 0.883, p < 0.001), group S (Spearman r = 0.928, p < 0.001), and group O (Spearman r = 0.525, p <0.05) (Table 1). Similar significant associations between attack rates and number of admitted SARS patients were observed for groups N and S but not for group O (Spearman r = 0.737, 0.865, and 0.39, respectively) (Table 1).",
            "cite_spans": [],
            "section": "Associations between Numbers of SARS Patients Admitted and Hospital Attack Rates ::: Results",
            "ref_spans": [
                {
                    "start": 246,
                    "end": 253,
                    "mention": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 453,
                    "end": 460,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The overall attack rates for all hospital workers in the first phase of the epidemic (before April 17, 2003: 0.98%) were higher than those for the second phrase (on or after April 17, 2003: 0.22%) (Table 3). This finding was true for all three groups of workers (group N: 0.99% vs. 0.22%; group S: 2.24% vs. 0.50%; group O: 0.21% vs. 0.07%). When data from individual hospitals were examined, however, the trend was not always consistent.",
            "cite_spans": [],
            "section": "Comparison of Attack Rates in First Two Phases of Epidemic ::: Results",
            "ref_spans": [
                {
                    "start": 198,
                    "end": 205,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The overall attack rate for all workers in the 16 hospitals was 1.2%. Staff members working in 14 hospitals contracted SARS, although the attack rates varied significantly among hospitals. The attack rate was >2% in three hospitals, which had 219 (60.8%) of the total 360 cases in Hong Kong. Compared to the overall attack rate (1.2%), the attack rate was much lower (0.54%) for the other 13 hospitals; the pooled overall attack rate for the 16 hospitals was also much lower (0.47%) in the second phase of the epidemic. In other words, nosocomial infection of hospital workers in Hong Kong was concentrated in three hospitals and in the earliest phase of the epidemic (the overall attack rates in the earlier phase were 2.18%, 3.37%, and 3.81% for these three hospitals).",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Attack rates were also associated with the number of SARS patients admitted into the individual hospitals. Five of the 16 hospitals admitted >100 patients. In terms of number of affected workers and attack rates, these five hospitals were also the top five of the 16 studied hospitals (except for hospital 13, which ranked seventh in terms of attack rates). Theoretically, viral load, inadequate manpower, inadequate equipment, and inadequate time for training were possible explanations for the observed associations. If the SARS epidemic resurges on a sizable scale, some consideration should be given to the number of patients to be admitted to a hospital. Yet, without further data, the exact reasons were not clear.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The attack rates also differed significantly among the three studied occupational groups. Support staff, such as healthcare assistants, cleaners, and clerical staff working on the wards (group S), had much higher attack rates, when compared to nurses (group N) and other categories of hospital workers, including physicians (group O). The attack rate of nonmedical support staff (group S) was higher than those of the other two groups in 10 of the 16 studied hospitals. Nonmedical support staff accounted for 134 (39.5%) of the 339 SARS patients among hospital workers, despite the fact that only approximately 17.3% of all Hospital Authority staff belonged to this group. Furthermore, 12 of the 16 hospitals had cases in nonmedical support staff (attack rate 0.83%-13.3% [mean 2.34%]). Even when the later phase of the epidemic was considered, the attack rates of nonmedical support staff were still relatively high (Table 3). Three of the six Hospital Authority staff who died of SARS also belonged to this group. In short, nonmedically trained support staff were exposed to a disproportionately high risk for nosocomial transmission of SARS. Apparently, infection control training was also offered to this group. However, the exact amount of training or assessment of how well the training was understood by this group was not documented.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 918,
                    "end": 925,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Extra protection is required to protect this staff group in the infection control campaign in Hong Kong. Insufficient emphasis may have been given to address the special needs of this group during the first SARS epidemic in Hong Kong, as well as during the post-SARS period. Infection control training and policies may need to be tailored for different occupational groups.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Nonmedical support staff, in general, were not contacting SARS patients as frequently as nurses did. This finding suggests that the fomite theory and the aerosol theory of transmission could not be dismissed. Another study investigating nosocomial infection in Hong Kong (10) reported that breakthrough transmission was likely responsible for nosocomial infection of hospital workers. Inconsistent use of personal protection equipment, perceived inadequate supply of such equipment, inadequate training, and perceived lack of knowledge about infection control were all statistically significant predictors of such breakthrough transmissions.",
            "cite_spans": [
                {
                    "start": 272,
                    "end": 274,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Nonmedical support staff may have been more likely to be lacking infection control knowledge, either as a result of receiving inadequate training or being unable to benefit fully from it. Whether they were adequately trained to use their personal protective equipment correctly is not clear. For instance, some preliminary studies mentioned that some workers were wearing or taking off such equipment in the wrong sequence (Lau et al., unpub. data). In Hong Kong, many support staff were likely to be middle-aged persons, who had a relatively low level of education (many of them did not finish secondary schools). Tailored infection control training and surveillance programs are hence warranted to ensure that this group of workers is sufficiently protected from the occupational hazard of contracting SARS. Practice exercises may also be an effective preventive measure.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "These findings do not mean that nurses were not under a high level of occupational hazard. More than 50% of the affected workers were nurses. In six hospitals, nurses' attack rates were close to or higher than 1%; the maximum was 4.66%. The correlations between number of SARS patients admitted and the number of affected workers were much stronger in the nonmedical support and nursing groups, when compared with that of other medical staff). This finding is understandable as most members of these two groups were working on the frontlines.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Most affected staff (94.2%) were working in hospitals that had been taking care of SARS patients. Further transmission through social contacts had not been a major factor of transmission among hospital workers (10). Nosocomial infection was therefore likely to be responsible for most transmission. Hospital workers in Hong Kong were well supported and appreciated by Hong Kong citizens and the mass media for their professionalism in treating SARS patients. Whether such strong media interest in their experiences and commitment influenced hospital workers in their decision to serve in high-risk environments, at times even when protection might not have been adequate, is of interest. Questions such as how conformity and peer pressure affected the decisions of individual workers who felt that they had to work under suboptimal infection control conditions are worth exploring.",
            "cite_spans": [
                {
                    "start": 211,
                    "end": 213,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The pooled attack rates (for 16 hospitals) were 0.22, 0.50, and 0.07, respectively, for nurses, nonmedical support, and other medical and technical staff when only cases of later onset (on or after April 17, 2003) were considered. When cases with earlier onset (before April 17, 2003) were considered, the rates were higher: 0.99, 2.24, and 0.21, respectively. The respective ratios of the two phases of the epidemic were 4.50:1, 4.48:1, and 3.00:1 for the three types of hospital workers and 4.45:1 for hospital workers overall (Table 3). This may be due to a reduction in the number of patients admitted after April 17 (approximately 18.3% of all cases; the number of patients admitted in the two phases was hence 3.92:1) or to improvement of infection control measures. The overall attack rate ratio (4.45:1, 0.98%/0.22%) was very similar to the overall admission rate for the two phases (4.46:1), although the two ratios were not conceptually equivalent. It, however, gives a clue that the decrease in exposure may have played a relatively important role in the decreased attack rate in the second phase. Improved infection control in the second phase may not be the primary reason for the decrease in the attack rate over time. If proper training, supply of personal protective equipment, infection control procedures, nonexcessive number of patients per hospital, and other measures are ensured, nosocomial infection of hospital workers should be avoidable. On the other hand, hospital workers, especially nonmedical support staff, should be aware that they are facing a certain level of occupational risk.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 530,
                    "end": 537,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The study has some limitations. First, only macro-level data were used. Since no individual data were available, factors associated with nosocomial infection could not be studied. Similarly, no clinical data were reported. Some hospital workers may have been infected in the community. However, another case-control study showed that social contact with other infected colleagues was not a significant factor associated with likelihood of infection among hospital workers (10). The chance of nosocomial infection was therefore very high for these staff who contracted SARS. Another study suggested that asymptomatic transmission among hospital workers was not prevalent (5). Second, different types of workers were included in the three studied job categories, and some heterogeneity across these different types of workers with the categories may exist. The available data do not permit further breakdown. The classification of affected workers into the three categories was arbitrary and may also have affected results. Further, no additional data exist to compare the conditions on infection control and other measures taken within the two analyzed phases of the epidemic, making definite interpretation impossible. The study, however, documents one of the most important scenarios of nosocomial infection among hospital workers. The results should help us to learn from this very costly lesson.",
            "cite_spans": [
                {
                    "start": 473,
                    "end": 475,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 671,
                    "end": 672,
                    "mention": "5",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "This study was funded by the Chinese University of Hong Kong.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "SARS transmission among hospital workers in Hong Kong.",
            "authors": [],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "280-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Ventilation of wards and nosocomial outbreak of severe acute respiratory syndrome among healthcare workers.",
            "authors": [],
            "year": 2003,
            "venue": "Chin Med J (Engl)",
            "volume": "116",
            "issn": "",
            "pages": "1293-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "A major outbreak of severe acute respiratory syndrome in Hong Kong.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1986-94",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030685"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "An outbreak of severe acute respiratory syndrome among hospital workers in a community hospital in Hong Kong.",
            "authors": [],
            "year": 2003,
            "venue": "Ann Intern Med",
            "volume": "139",
            "issn": "",
            "pages": "564-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Investigation of a nosocomial outbreak of severe acute respiratory syndrome (SARS) in Toronto, Canada.",
            "authors": [],
            "year": 2003,
            "venue": "CMAJ",
            "volume": "169",
            "issn": "",
            "pages": "285-92",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Zhonghua Liu Xing Bing Xue Za Zhi",
            "volume": "24",
            "issn": "",
            "pages": "554-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Zhonghua Liu Xing Bing Xue Za Zhi",
            "volume": "24",
            "issn": "",
            "pages": "561-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Zhonghua Liu Xing Bing Xue Za Zhi",
            "volume": "24",
            "issn": "",
            "pages": "350-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Transmission dynamics of the etiological agent of SARS in Hong Kong: impact of public health interventions.",
            "authors": [],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": "1961-6",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1086478"
                ]
            }
        }
    }
}