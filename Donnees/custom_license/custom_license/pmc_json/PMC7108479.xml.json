{
    "paper_id": "PMC7108479",
    "metadata": {
        "title": "Adult cardiac critical care at the King Abdulaziz Cardiac Center",
        "authors": [
            {
                "first": "Arif",
                "middle": [],
                "last": "Hussain",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Nasir",
                "middle": [],
                "last": "Mustafa",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Raed",
                "middle": [],
                "last": "Al Ali",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hind",
                "middle": [],
                "last": "Almodaimegh",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In the recent years, we have witnessed immense progress in medicine in general and cardiac sciences in particular. It was not until recently that we started to recognize a number of distinct disciplines that evolved around the two major activities in medicine related to heart, namely cardiology and cardiac surgery. In addition, it has been observed that most patients have multiple co-morbidities that require multiple specialists.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "A unique phenomenon that has contributed dramatically to this progress has been the overlap of expertise and technology used by different specialties. The merger of such diverse disciplines has required a vision of forward thinking, innovation, and, importantly, giving up the traditional domains held so dearly by the old guards in medicine, in a hope to achieve new dimensions in medicine. Diagnostic and interventional cardiology has exploited the technology developed by radiology and provided an improved service to their patients.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Cardiac critical care, also referred to as acute cardiac care, has emerged as a happy marriage of many of these bodies of knowledge and skills. Adult care coalesces the cardiology, cardiac surgery, intensive care, anaesthesiology, and imaging disciplines into an exciting new field much needed by the critically ill patients in the perioperative or peri-interventional period of their hospitalization. Fragile pediatric patients, in addition, require a sound background and base in pediatric medicine for their optimum care.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Emergence of new specialties is not without new challenges. Defining the role and responsibilities of practitioners of a new specialty or subspecialty, creating well thought out training requirements and standards, establishing governing bodies, boards, examinations, credentialing, privileging, teaching, and research are just a few to mention here. Cardiac critical care is also currently undergoing this phase of development.",
            "cite_spans": [],
            "section": "Challenges for the specialty",
            "ref_spans": []
        },
        {
            "text": "Different professional societies have already accepted the concept of developing a new specialty specifically dedicated to the care of seriously ill cardiac patients and have started working on defining the scope of the specialty along with setting the training goals and even detailed curricula of training. European Society of Cardiology set up a working group1 with a current membership of \u223c1000 members, which has transformed into Acute Cardiovascular Care Association. This group has also started a journal called Journal of Acute Cardiovascular Care. European Society of Cardiology has published a number of documents including a textbook2 on this subject. The British Cardiovascular Society Working Group has also published a similar and comprehensive document with its recommendation on training in acute cardiac care.3 A survey done by the American Heart Association Writing Group on the Evolution of Critical Care Cardiology that looked at the organization and staffing practices in US cardiac intensive care units has revealed a growing trend of involvement of cardiologists trained in the critical care at least in the academic centers.4 These publications address the non-surgical patients only. It is important to include the post cardiac surgical care in the same context for future discussions and planning of a more comprehensive specialty.",
            "cite_spans": [
                {
                    "start": 362,
                    "end": 363,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 644,
                    "end": 645,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 826,
                    "end": 827,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1148,
                    "end": 1149,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Challenges for the specialty",
            "ref_spans": []
        },
        {
            "text": "We should view the new developments with excitement and anticipation and should not feel threatened that our territory is being invaded by alien specialties. It is only natural that practitioners looking after the sick cardiac patients will use a limited but focused application of echocardiography and other ultrasound modalities for their optimum care at the bedside. Intensivists would be engaged in performing tasks such as bronchoscopies, bedside tracheostomies, mechanical management of complicated myocardial infarction, renal failure, and management of septic syndromes. These skills are being developed with the help of those who traditionally practised them and also through establishment of properly constructed training programs. This trend, however, is not without certain risks, but they can be mitigated if limitations of acquired skills are recognized and higher levels of competencies are unabashedly sought and encouraged.",
            "cite_spans": [],
            "section": "New technologies and skill sets",
            "ref_spans": []
        },
        {
            "text": "Embracing these concepts, the King Abdulaziz Cardiac Center (KACC) established a new division called Cardiac Critical Care 3 years ago. The biggest challenge has been recruiting the right mix of staff that has solid background in different overlapping specialties with the common denominator of critical care training and experience.",
            "cite_spans": [],
            "section": "Institutional challenges",
            "ref_spans": []
        },
        {
            "text": "Creating a smooth transition from traditional surgeon-led and cardiologist-led management of intensive care units (ICUs) to critical care has been another challenge. This changeover has been slow, but it has been necessary for team building and active participation.",
            "cite_spans": [],
            "section": "Institutional challenges",
            "ref_spans": []
        },
        {
            "text": "A two-year fellowship in Critical Care (Major in Cardiac Sciences) was conceived and developed in collaboration with the General Intensive Care Fellowship Program and has been approved by Saudi Commission for Health Specialties. Currently, there are two fellows under training, both board-certified cardiologists, who are getting wider training in general intensive care, surgical intensive care, pulmonary medicine, infectious disease, and cardiac anaesthesia in a well-rounded training program with organized didactic sessions and rotations. They are also encouraged to be involved in teaching and research. Many learning opportunities are available within the department and the hospital in the form of lectures, seminars, courses, and workshops. Hands-on training and skill building are constantly provided and encouraged. Equipment related to these skills such as bedside ultrasound and echocardiography is readily available. Internationally accredited courses are offered here. All cardiac staff is certified in Advanced Cardiac Life Support.",
            "cite_spans": [],
            "section": "Training and education",
            "ref_spans": []
        },
        {
            "text": "There are two ICUs, with a total of 17 beds: Medical Cardiac Intensive Care Unit (MCICU) has 9 beds, and Adult Cardiac Surgical Intensive Care Unit (ACICU) has 8 beds.",
            "cite_spans": [],
            "section": "The units",
            "ref_spans": []
        },
        {
            "text": "At present, we have three consultants and four junior staff physicians in addition to two fellows in the Division of Adult Cardiac Critical Care who work hand-in-hand with the existing cardiac surgical and cardiology doctors complementing their roles. There is a comfortable 1:1 patient-to-nurse ratio in our units. This service is provided by a complement of \u223c75 nurses that include two nurse managers, several charge nurses, clinical nurse coordinators, clinical nurses, and patient care technologists. There are a number of nursing interns and residents in training at a given time. Paramedical staff includes respiratory therapists, diabetic educator, nutritionists, and social workers.",
            "cite_spans": [],
            "section": "Medical and paramedical staff",
            "ref_spans": []
        },
        {
            "text": "Regular daily multidisciplinary rounds are conducted 7 days a week that include medical staff of cardiology or cardiac surgery along with the critical care physicians, doctors in training, a clinical pharmacist, nurses, a dietician, and a respiratory therapist. Pulmonologists, infectious disease specialists, and nephrologists regularly follow their patients.",
            "cite_spans": [],
            "section": "Daily rounds",
            "ref_spans": []
        },
        {
            "text": "Many cardiac, pharmaceutical, and critical care societies have recognized the role of the acute care clinical pharmacist in optimizing the patient care. The American Society of Health Pharmacists and the Society of Hospital Pharmacists of Australia have set the minimum standards of the clinical pharmacist as being a part of a multidisciplinary team caring for critically ill patients including cardiac patients.5 These specialized pharmacists must have a high level of training and have clinical, management, and research skills. In addition to providing drug information, other major contributions made by the clinical pharmacist participating in rounds in the MCICU and ACICU include assisting physicians in pharmacotherapy decision-making based on available guidelines, assisting in establishing protocols, providing pharmacokinetic consultations and therapeutic drug monitoring, providing total parenteral nutrition, monitoring patients for drug efficacy and safety, and offering medical education to physicians, nurses, and patients.",
            "cite_spans": [
                {
                    "start": 413,
                    "end": 414,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Role of clinical pharmacologists in our units",
            "ref_spans": []
        },
        {
            "text": "Although not totally paperless, patient records are maintained with the help of computerized narratives from initial admission history and physical examination (Apollo) to critical care, anaesthesia, cardiac catheterization, imaging, surgical data and flow-sheets intellivue-clinical-information-portfolio Philips well integrated with the hospital-wide information system (Quadramed) that is linked to all laboratory data. Computerized physician order entry (CPOE) is also fully electronic.",
            "cite_spans": [],
            "section": "Electronic records and computerized physician orders",
            "ref_spans": []
        },
        {
            "text": "The hospital is a Joint Commission International (JCI)-accredited institution that has gone through a rigorous re-accreditation process twice and is currently in the process of becoming a high reliability organization. The ICUs in which this division has been involved have always ranked among the best in the hospital. Several of the staff including AH have served on many hospital committees that have implemented JCI standards across the hospital, and these units have championed the global patient safety initiatives such as hand hygiene, reduction or elimination of central line-associated bloodstream infection, ventilator-associated pneumonias, falls risk, etc. using different \u2018bundles\u2019 embedded within the electronic data keeping. The relevant departments provide hospital-wide surveillance of these indicators.",
            "cite_spans": [],
            "section": "Standards and quality initiatives",
            "ref_spans": []
        },
        {
            "text": "Recent challenges of dealing with Middle East respiratory syndrome caused by coronavirus have also been faced by our ICUs and our staff managed several patients affected by this virus.",
            "cite_spans": [],
            "section": "Standards and quality initiatives",
            "ref_spans": []
        },
        {
            "text": "A number of internationally accepted and recommended bundles, protocols, and guidelines are applied in these units. This is a work in progress and more standardized practices are continuously being introduced and applied.",
            "cite_spans": [],
            "section": "Bundles and protocols applied",
            "ref_spans": []
        },
        {
            "text": "Having a sophisticated electronic record keeping and continuous flow of patient data both administrative and clinical allows prospective and retrospective review of many parameters that are helpful in patient management and possible research projects. The informatics has just begun to yield reliable data for research, quality management, and benchmarking. The potentials for research activities are enormous. Data are available for demographics of the patients, bed occupancy rates, length of stay, whether discharged alive, mechanical ventilation, renal replacement therapy, intra-aortic balloon pumps, extra-corporeal membrane oxygenator, International Statistical Classification of Diseases and Related Health Problems coded diagnoses, etc. The work in progress includes morbidity data, quality indicators dashboard, and institution of risk assessment tools for outcomes. All the laboratory and imaging data are accessible from the hospital informatics systems and data warehouse. All this activity is supported by the dedicated experts in informatics and data management employed by the Cardiac Center.",
            "cite_spans": [],
            "section": "Data management",
            "ref_spans": []
        },
        {
            "text": "In the MCICU, the majority of patients are admitted either directly from the emergency department (>100 beds) or from other cardiac units namely coronary care unit (CCU), Stepdown, or Cathlab.",
            "cite_spans": [],
            "section": "Case mix and sources of admission to intensive care units",
            "ref_spans": []
        },
        {
            "text": "The ACICU, on the other hand, receives practically all of its admissions from the operating rooms although some patients may be transferred from the emergency ward, hospital wards, CCU and other ICUs within the hospital.",
            "cite_spans": [],
            "section": "Case mix and sources of admission to intensive care units",
            "ref_spans": []
        },
        {
            "text": "The Cardiac Critical Care division is still in its infancy but has accepted the challenges of developing this evolving specialty with the entire modern infrastructure at its disposal for state-of-the-art delivery of healthcare to the most critically ill adult cardiac patients at the KACC.",
            "cite_spans": [],
            "section": "Conclusion",
            "ref_spans": []
        },
        {
            "text": "\nConflict of interest: none declared.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "The ESC Working Group on Acute Cardiac Care",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Vrints",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Eur Heart J",
            "volume": "32",
            "issn": "",
            "pages": "2465-2472",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Tubaro",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "The ESC Textbook of Intensive and Acute Cardiac Care",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Recommendations of the British Cardiovascular Society Working Group on Acute Cardiac Care: from coronary care unit to acute cardiac care unit\u2014the evolving role of specialist cardiac care",
            "authors": [
                {
                    "first": "DM",
                    "middle": [],
                    "last": "Walker",
                    "suffix": ""
                },
                {
                    "first": "NE",
                    "middle": [],
                    "last": "West",
                    "suffix": ""
                },
                {
                    "first": "SG",
                    "middle": [],
                    "last": "Ray",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Heart",
            "volume": "98",
            "issn": "",
            "pages": "350-352",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Organization and staffing practices in US cardiac intensive care units: a survey on behalf of the American Heart Association Writing Group on the Evolution of Critical Care Cardiology",
            "authors": [
                {
                    "first": "RG",
                    "middle": [],
                    "last": "O'Malley",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Olenchock",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Bohula-May",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Barnett",
                    "suffix": ""
                },
                {
                    "first": "DJ",
                    "middle": [],
                    "last": "Fintel",
                    "suffix": ""
                },
                {
                    "first": "CB",
                    "middle": [],
                    "last": "Granger",
                    "suffix": ""
                },
                {
                    "first": "JN",
                    "middle": [],
                    "last": "Katz",
                    "suffix": ""
                },
                {
                    "first": "MC",
                    "middle": [],
                    "last": "Kontos",
                    "suffix": ""
                },
                {
                    "first": "JT",
                    "middle": [],
                    "last": "Kuvin",
                    "suffix": ""
                },
                {
                    "first": "SA",
                    "middle": [],
                    "last": "Murphy",
                    "suffix": ""
                },
                {
                    "first": "JE",
                    "middle": [],
                    "last": "Parrillo",
                    "suffix": ""
                },
                {
                    "first": "DA",
                    "middle": [],
                    "last": "Morrow",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Eur Heart J; Acute Cardiovascular Care",
            "volume": "2",
            "issn": "",
            "pages": "3-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "SHAP standards for practice for critical care pharmacy practice",
            "authors": [],
            "year": 2008,
            "venue": "J Pharm Practice Res",
            "volume": "38",
            "issn": "",
            "pages": "58-60",
            "other_ids": {
                "DOI": []
            }
        }
    }
}