{
    "paper_id": "PMC7095066",
    "metadata": {
        "title": "Pandemics: avoiding the mistakes of 1918",
        "authors": [
            {
                "first": "John",
                "middle": [
                    "M."
                ],
                "last": "Barry",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In the next influenza pandemic, be it now or in the future, be the virus mild or virulent, the single most important weapon against the disease will be a vaccine. The second most important will be communication. History has shown that to cut vaccine production time, minimize economic and social disruption, deliver health care and even food, governments need to communicate well \u2014 both between themselves and with the public.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The US response to the 1918 flu offers a case study of a communication strategy to avoid. The world response to the threat of an emerging flu in recent weeks shows that we have learned from the past. And there is much to learn.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The communication strategy of either reassurance or silence had its effect. Its effect was terror.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The pandemic that began in January 1918 and ended in June 1920 killed an estimated 35 million\u2013100 million people worldwide, or 1.9\u20135.5% of the entire population1. Although an estimated 2% of people died in Western countries, some large subgroups were affected disproportionately. The Metropolitan Life Insurance Company, based in New York, found that the disease killed 3.26% of its insured US industrial workers aged 25\u201345. Given that 25\u201340% of the population contracted the disease, case mortality would have been 8\u201313% in that population2.",
            "cite_spans": [
                {
                    "start": 160,
                    "end": 161,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 540,
                    "end": 541,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The flu started slowly. In the United States, a small wave of the disease sputtered across the country in the spring of 1918, but went largely unnoticed except in military training camps. The effects were more noticeable in Europe, where many soldiers in the armies of the First World War fell ill. By the end of summer, a more lethal wave had surfaced in Switzerland. On 3 August, the US military received an intelligence report comparing the Swiss epidemic to the Black Death.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": " The US government used the same strategy for communicating about the disease that it had developed to disseminate war news. The essence of that strategy was described by its main architect, writer Arthur Bullard: \u201cTruth and falsehood are arbitrary terms ... There is nothing in experience to tell us one is always preferable to the other ... The force of an idea lies in its inspirational value. It matters very little if is true or false.\u201d Fellow adviser Walter Lippman, another architect of this strategy, sent President Woodrow Wilson a memo saying that most citizens were \u201cmentally children\u201d and advising that \u201cself-determination\u201d had to be subordinated to \u201corder\u201d and \u201cprosperity\u201d. In 1917, the day after receiving Lippman's memo, Wilson issued an executive order to control all government communication strategy during the war that was premised on keeping up morale.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": " As a result, when the full-blown and lethal pandemic wave arrived in the United States in September 1918, Wilson never made a single statement about it, and lesser public figures provided only reassurance. US surgeon general Rupert Blue declared: \u201cThere is no cause for alarm if proper precautions are observed.\u201d Local health officials echoed this message. Chicago's director of public health, for instance, decided not to \u201cinterfere with the morale of the community\u201d, explaining: \u201cIt is our job to keep people from fear. Worry kills more than the disease.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "That last phrase became a mantra repeated in hundreds of newspapers. Paid advertising carried a comparable message: every day, press advertisements for Vicks VapoRub appeared with the line: \u201cSimply the Old-Fashioned grip masquerading under A New Name.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Yet it was not ordinary influenza by another name. The disease was unusual enough to be misdiagnosed initially as cholera, typhoid and dengue. Some people died within 24 hours of the first symptom. The most horrific feature was bleeding, not just from the nose and mouth but also from the ears and eyes.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Nonetheless, the government and newspapers continued to reassure. Although physicians fully understood the explosive nature of the pandemic, they routinely misled people, covered up the truth and lied. In Philadelphia, for example, public-health director Wilmer Krusen promised \u2014 before a single civilian had died \u2014 to \u201cconfine this disease to its present limits. In this we are sure to be successful.\u201d As the death toll grew, he repeatedly reassured the public that \u201cthe disease has about reached its crest. The situation is well in hand.\u201d When the number of daily deaths broke 200, he again promised: \u201cThe peak of the epidemic has been reached.\u201d When 300 died in a day, he said: \u201cThese deaths mark the high-water mark.\u201d Ultimately, daily deaths reached 759. The press never questioned him.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Meanwhile, the bodies piled up. In many cities, they lay uncollected in homes for days. In some places, including Philadelphia, they were buried in mass graves dug by steam shovels.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nSame old fever?\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Unfortunately, Philadelphia's communication strategy was the rule, not the exception. Local officials and newspapers across the country were either deceptive or said nothing. Many papers did not print lists of the dead. Even as 8,000 soldiers were hospitalized in Camp Pike, Arkansas, over four days, the Arkansas Gazette in Little Rock, just a few miles away, maintained, \u201cSpanish influenza is plain la grippe \u2014 same old fever and chills.\u201d This communication strategy of either reassurance or silence had its effect. Its effect was terror.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Lies and silence cost authority figures credibility and trust. With no public official to believe in, people believed rumours and their most horrific imaginings. A man living in Washington described the result: \u201cPeople were afraid to kiss one another, people were afraid to eat with one another ... It destroyed those contacts and destroyed the intimacy that existed amongst people ... there was an aura of a constant fear that you lived through from getting up in the morning to going to bed at night.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Under that pressure, society first drifted, then threatened to fall apart. The health-care system, already drained of physicians and nurses by the military, collapsed first. Elsewhere, fear, not illness, kept people at home. Absenteeism reached extraordinary levels. Shipyard workers were told that their duties were as important as a soldier's; they were paid only if they worked; and, unlike elsewhere, physicians were available to them on site. Yet absentee rates in the shipyards \u2014 one of the few industries for which there are good data \u2014 still ranged from 45% to 58% (ref. 3). Absenteeism crippled the railroad system, which transported nearly all freight, bringing it to the point of collapse. It shut down telephone exchanges, closing off communication, and further isolating and alienating people. Grocers refused to open. Coal sellers closed. In cities and rural communities, the Red Cross reported that people \u201cwere starving to death not for lack of food but because the well were too panic stricken to bring food to the sick\u201d.",
            "cite_spans": [
                {
                    "start": 579,
                    "end": 580,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Victor Vaughan, a sober, serious scientist, for years dean of the medical school at the University of Michigan in Ann Arbor, worried that if this trend accelerated \u201cfor a few more weeks ... civilization could easily disappear from the face of the Earth\u201d.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Better communication led to better results. In San Francisco, for example, despite a slow reaction to the initial onslaught of flu, in October 1918 the mayor, health officials and business and union leaders all signed a full-page newspaper advert in huge type reading: \u201cWear A Mask and Save Your Life!\u201d It was a rare, bold statement. In this city, society, although reeling, functioned. Food was delivered, and the sick were cared for. Where people had accurate information and knew what they faced, they often performed heroically. Red Cross professionals, physicians and nurses routinely risked their lives. When Philadelphia's city police \u2014 who knew the facts even if the papers weren't printing them \u2014 were asked to supply four volunteers to \u201cremove bodies from beds ... and load them in vehicles\u201d, 118 officers responded4.",
            "cite_spans": [
                {
                    "start": 825,
                    "end": 826,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nTruth telling\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Of course, the world is different today to how it was in 1918. But communication remains paramount.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Until we develop a vaccine that is effective against all influenza viruses and available globally, the world will be vulnerable to influenza pandemics. H1N1 is the most immediate danger, including the possibility that a more deadly wave will strike later this year, but H5N1 and other viruses remain pandemic threats.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "First and foremost, authorities must tell each other the truth. This provides crucial lead time for vaccine production. Models even suggest that in a few circumstances, surveillance and transparency may allow a new virus to be contained and extirpated.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The world has performed well in the past few weeks in this regard, but this is a lesson that has not been entirely taken on board. In 2003, China initially covered up SARS, putting the world at risk and contributing to near-panic in Beijing, where people felt they could trust nothing coming from the government. In 2004, Thailand and Indonesia withheld information during the first outbreaks of H5N1 bird flu. There continue to be both political and bureaucratic problems in ensuring that H5N1 isolates are shared \u2014 especially by Indonesia \u2014 thereby increasing the risk to the world.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Telling the public the truth is also paramount. Before a vaccine becomes available during a severe pandemic, at some point the government will ask citizens to adhere to a series of public-health guidelines for non-pharmaceutical interventions, such as staying at home if they become ill. Large-scale, sustained compliance will be essential if those measures are to succeed. Compliance requires trust, and that depends on truth-telling.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In the United States, former health and human services secretaries Tommy Thompson and Michael Leavitt deserve credit for institutionalizing real transparency in the current US pandemic plan. And the administration of President Barack Obama has performed admirably so far. Obama himself has addressed the issue several times, making perhaps only one mistake, when he said the threat was \u201ccause for concern, but not alarm\u201d. Had things deteriorated quickly, he ran the risk of suddenly having to reverse his position.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In Mexico, the problem was not reticence but candour, releasing inaccurate information that overstated the problem. Mexico should be congratulated for this, not condemned.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Although a false alarm can be damaging, it is not nearly as damaging as silence \u2014 the type of silence that makes people believe the truth is being withheld. That is how trust disintegrates and how rumours \u2014 passed in the streets in 1918, today passed over Internet blogs \u2014 take hold and grow.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "I don't much care for the term 'risk communication'. It implies that the truth is being managed. The truth should not be managed, it should be told. Only by knowing the truth can imaginary horrors be transformed into concrete realities. And only then can people start to deal with those realities, and do so without panic.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nFURTHER READING\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Barry, J. M., Viboud, C. & Simonsen, L. Cross-protection between successive waves of the 1918-1919 influenza pandemic: epidemiological evidence from US Army camps and from Britain. J. Infect. Dis. 198, 1427-1434 (2008).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Barry, J. M. Comments on the nonpharmaceutical interventions in New York City and Chicago during the 1918 flu pandemic. J. Transl. Med. 5, 65 (2007).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Canetti, E. Crowds and Power (Farrar, Straus and Giroux, 1984).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Osterholm, M. T. Unprepared for a Pandemic. Foreign Affairs 86, 47-57 (2007).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Vaughn, S. Holding Fast the Inner Lines: Democracy, Nationalism, and the Committee on Public Information (University of North Carolina Press, 1980).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "NPAS",
                    "middle": [],
                    "last": "Johnson",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Mueller",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Bull. Hist. Med.",
            "volume": "76",
            "issn": "",
            "pages": "105-115",
            "other_ids": {
                "DOI": [
                    "10.1353/bhm.2002.0022"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [
                {
                    "first": "EO",
                    "middle": [],
                    "last": "Jordan",
                    "suffix": ""
                }
            ],
            "year": 1927,
            "venue": "Epidemic Influenza: A Survey",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}