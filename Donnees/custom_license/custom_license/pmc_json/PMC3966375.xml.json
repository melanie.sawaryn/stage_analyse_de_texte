{
    "paper_id": "PMC3966375",
    "metadata": {
        "title": "Truth in the Details ",
        "authors": [
            {
                "first": "Sharon",
                "middle": [],
                "last": "Bloom",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Emily",
                "middle": [
                    "M."
                ],
                "last": "Weeks",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "John Frederick Lewis (1804\u20131876) was a British painter who specialized in exquisitely detailed paintings of Oriental (Middle Eastern) subject matter, inspired by his ten-year residency in Egypt. Having become an exhibiting member of London\u2019s Old Watercolor Society by 1827, Lewis would later turn almost exclusively to oil painting. His early works favored animals and hunting scenes, but his compositions became increasingly diverse after he had traveled to Spain and Europe. In 1841 his travels took him to Cairo, where he remained until 1851, making numerous drawings and gathering the materials that would confirm his growing reputation in London as an \u201cartist-ethnographer.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "On the Banks of the Nile, Upper Egypt, painted shortly before Lewis\u2019s death in 1876, depicts a springtime scene in rural Egypt, in which a group of traveling Bedouin and their camels pause to meet the local fellaheen (peasant farmers). One Bedouin man and his saluki hunting dog rest at the edge of the river, beneath the towering form of a camel, silhouetted against the sky. True to Lewis\u2019s realistic style, the details of the man\u2019s clothing are rendered with nearly photographic accuracy; he wears the traditional kufiyeh (head scarf) and a heavy outer garment of wool and cotton, its voluminous folds showing off a distinctive pattern of broad brown and cream panels. Also typical of the artist\u2019s work is the easy parlance between man and beast: the camels wait contentedly for their riders to return, while the dog and waterfowl adopt similarly relaxed demeanors. In the distance are female peasants, who traverse a field of orange and white wildflowers on their way to fetch water from the Nile. They are as much a part of the landscape as are the flora, and they provide a gentle reminder of Lewis\u2019s fascination with beauty of all kinds. The viewpoint that Lewis offers\u2014that of an unacknowledged observer upon an idling boat\u2014seems to suggest that we too belong in his composition, as unobtrusive additions to this delicately balanced natural world.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Several Orientalist motifs of Lewis\u2019s art are mirrored in this month\u2019s issue of Emerging Infectious Diseases. Lewis\u2019s juxtaposition of travelers, wild birds, and domesticated animals illustrates ideal opportunities for disease transmission. For example, since 2012, Middle East respiratory syndrome coronavirus (MERS-CoV) has caused an ongoing outbreak of severe acute respiratory tract infection in humans; new findings add to the growing evidence that MERS-CoV, or a closely related virus, infected dromedary camels in the United Arab Emirates long before the first human case of MERS-CoV. While the specific role of camels in MERS-CoV transmission remains unclear, the camel mystery deepens with a report of a novel coronavirus of camels, related to but distinct from MERS-CoV. Wild waterfowl similar to those vividly portrayed by Lewis play a key role in the transmission of avian influenza viruses; active surveillance of avian influenza viruses among domestic poultry in Egypt found that subtype H5N1 still circulates in a widespread manner and that subtype H9N2 is emerging.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The words of art critic John Ruskin, quoted above, allude to Lewis\u2019s artistic mission and provide an unexpected link between the disciplines of art and science: In Lewis\u2019s meticulous attention to detail and in the careful research of this month\u2019s scientists, we witness the tireless pursuit of truth.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Active surveillance for avian influenza virus, Egypt, 2010\u20132012.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "542-51",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2004.131295"
                ]
            }
        },
        "BIBREF2": {
            "title": "Antibodies against MERS coronavirus in dromedaries, United Arab Emirates, 2003 and 2013.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "552-9",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2004.131746"
                ]
            }
        },
        "BIBREF3": {
            "title": "Novel betacoronavirus in dromedaries of the Middle East, 2013.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "560-72",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2004.131769"
                ]
            }
        }
    }
}