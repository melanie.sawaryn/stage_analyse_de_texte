{
    "paper_id": "PMC7108161",
    "metadata": {
        "title": "Quantitative Analysis and Prognostic Implication of SARS Coronavirus RNA in the Plasma and Serum of Patients with Severe Acute Respiratory Syndrome",
        "authors": [
            {
                "first": "Enders",
                "middle": [
                    "K",
                    "O"
                ],
                "last": "Ng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [
                    "S"
                ],
                "last": "Hui",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "K",
                "middle": [
                    "C",
                    "Allen"
                ],
                "last": "Chan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Emily",
                "middle": [
                    "C",
                    "W"
                ],
                "last": "Hung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rossa",
                "middle": [
                    "W",
                    "K"
                ],
                "last": "Chiu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Nelson",
                "middle": [],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Alan",
                "middle": [],
                "last": "Wu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Stephen",
                "middle": [
                    "S",
                    "C"
                ],
                "last": "Chim",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yu",
                "middle": [
                    "K"
                ],
                "last": "Tong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Joseph",
                "middle": [
                    "J",
                    "Y"
                ],
                "last": "Sung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "John",
                "middle": [
                    "S"
                ],
                "last": "Tam",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Y",
                "middle": [
                    "M",
                    "Dennis"
                ],
                "last": "Lo",
                "suffix": "",
                "email": "loym@cuhk.edu.hk",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Peripheral blood samples were obtained from SARS patients admitted to the New Territories East Cluster of Hospital Authority Hospitals in Hong Kong. Samples were recruited between March and May 2003.",
            "cite_spans": [],
            "section": "patients ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "In the first part of this study, blood samples were collected from 12 SARS patients on the day of hospital admission, as well as on days 7 and 14 after fever onset. Informed consent was obtained from the patients, and ethics approval was obtained from the Institutional Review Board. In the second part of this study, blood samples were obtained from 23 SARS patients on the day of hospital admission. All studied patients had subsequent serologic evidence of antibodies to SARS-CoV. For the prognostic part of the study, the previously mentioned 23 SARS patients were subdivided into two patient groups: (a) 11 patients who required admission to the intensive care unit (ICU) and (b) 12 patients who did not require ICU admission (non-ICU) during the duration of their hospitalization.",
            "cite_spans": [],
            "section": "patients ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "Blood samples were collected in EDTA-containing or plain tubes. The plain bottles were allowed to stand at room temperature for 2 h for complete clotting of blood. The blood samples were then centrifuged at 1600g for 10 min at 4 \u00b0C. Plasma or serum was then carefully transferred to plain polypropylene tubes. The plasma samples were recentrifuged at 16 000g for 10 min at 4 \u00b0C, and the supernatants were collected in fresh polypropylene tubes.",
            "cite_spans": [],
            "section": "processing of blood samples ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "Viral RNA was extracted from 0.28 mL of plasma/serum with use of a QIAamp viral RNA mini reagent set (Qiagen) according to the manufacturer\u2019s recommendations. RNA was eluted with 50 \u03bcL of AVE buffer (included in the reagent set) and stored at \u221280 \u00b0C.",
            "cite_spans": [],
            "section": "rna extraction ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "One-step, real-time quantitative RT-PCR was used for SARS-CoV RNA quantification. We used the publicly released full genomic sequences of SARS-CoV (http://www.ncbi.nlm.nih.gov) to design two RT-PCR systems specifically targeting the SARS-CoV genome. The sequences of the primers and probes showed perfect alignment with all publicly available complete genome sequences of SARS-CoV on GenBank as of July 23, 2003 (accession nos. AY338175, AY338174, NC_004718, AY321118, AY323977, AY283798, AY283797, AY283796, AY283795, AY283794, AY291315, AY279354, AY278490, AY278487, AY278489, AY278554, AY297028, AY274119, AY291451, AY282752, AY278488, AY278741, and AY278491). The SARSPol1 system targeted the polymerase gene (orf1ab polyprotein; nucleotides 15327\u201315398; accession no. AY278554), and the SARSN system targeted the nucleocapsid gene (N; nucleotides 28758\u201328823; accession no. AY278554) of the SARS-CoV genome. The SARSPol1 primer sequences were 5\u2032-GAGTGTGCGCAAGTATTAAGTGA-3\u2032 (forward) and 5\u2032-TGATGTTCCACCTGGTTTAACA-3\u2032 (reverse), and the dual-labeled fluorescent probe was 5\u2032-(FAM)ATGGTCATGTGTGGCGGCTCACTA(TAMRA)-3\u2032, where FAM is 6-carboxyfluorescein and TAMRA is 6-carboxytetramethylrhodamine. The SARSN primer sequences were 5\u2032-TGCCCTCGCGCTATTG-3\u2032 (forward) and 5\u2032-GGCCTTTACCAGAAACTTTGC-3\u2032 (reverse), and the dual-labeled fluorescent probe was 5\u2032-(FAM)TGCTAGACAGATTGAACCAGCTTG(TAMRA)-3\u2032. Calibration curves for SARS-CoV RNA quantification were prepared with serial dilutions of a HPLC-purified single-stranded synthetic DNA oligonucleotide (PROLIGO), with concentrations ranging from 1 \u00d7 107 to 1 \u00d7 100 copies. The initial concentration of the synthetic oligonucleotide was determined by measuring the absorbance at 260 and 280 nm. The assay was able to detect 5 copies of the calibrator target in the reaction mixture, as indicated in Figs. 1 and 2 in the Data Supplement that accompanies the online version of this article athttp://www.clinchem.org/content/vol49/issue12/. Concentrations of SARS-CoV were expressed as copies/mL of plasma/serum.",
            "cite_spans": [],
            "section": "real-time quantitative rt-pcr ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "Because no recovery experiments had been done, the reported concentrations (copies/mL) were minimum estimates. The sequences of the synthetic DNA oligonucleotides for SARSPol1 and SARSN calibrations were 5\u2032-AACGAGTGTGCGCAAGTATTAAGTGAGATGGTCATGTGTGGCGGCTCACTATATGTTAAACCAGGTGGAACATCATCCGG-3\u2032 and 5\u2032-GAAACTGCCCTCGCGCTATTGCTGCTAGACAGATTGAACCAGCTTGAGAGCAAAGTTTCTGGTAAAGGCCAACAA-3\u2032, respectively.",
            "cite_spans": [],
            "section": "real-time quantitative rt-pcr ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "The RT-PCR reactions were set up according to the manufacturer\u2019s instructions (EZ rTth RNA PCR reagent set; Applied Biosystems) in a reaction volume of 25 \u03bcL. The primers and fluorescent probes were used at concentrations of 300 and 100 nM, respectively, and 12 \u03bcL of extracted plasma/serum RNA was used for amplification. The thermal profile used for the analysis was as follows: the reaction was initiated at 50 \u00b0C for 2 min for the included uracil N-glycosylase to act, followed by reverse transcription at 60 \u00b0C for 30 min. After a 5-min denaturation at 95 \u00b0C, 40 cycles of PCR were carried out with denaturation at 94 \u00b0C for 20 s and annealing/extension for 1 min at 56 \u00b0C. Each sample was analyzed in duplicate, and the calibration curve was run in parallel for each analysis. Multiple negative water blanks were also included in every analysis.",
            "cite_spans": [],
            "section": "real-time quantitative rt-pcr ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "Statistical analysis was performed with SigmaStat 2.03 software (SPSS). The Student t-test was used for the comparison of the ages of the ICU and non-ICU groups, and the Mann\u2013Whitney test was used for the comparison of serum SARS-CoV RNA concentrations between the ICU and non-ICU groups. Pearson correlation analysis was used to assess the correlation of SARS-CoV RNA concentrations between the SARSPol1 and SARSN RT-PCR systems.",
            "cite_spans": [],
            "section": "statistical analysis ::: Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "To determine the quantitative performance of the SARS-CoV RT-PCR assays, we used these assays to amplify serially diluted calibrators, which were synthetic DNA oligonucleotides based on the SARS-CoV genomic sequence. The calibration curves for the SARSPol1 (polymerase) and SARSN (nucleocapsid) amplification systems demonstrated a dynamic range from 5 copies to 1 \u00d7 107 copies. A semilogarithmic plot of different calibrator concentrations against the threshold cycles yielded correlation coefficients of 0.987 for the SARSPol1 system and 0.993 for the SARSN system. The amplification steps of these assays were sufficient to allow detection of 5 copies of the targets in the reaction mixtures, which corresponded to a detection limit of 74 copies/mL of plasma/serum. The assay consistently demonstrated this low detection limit, as indicated by the amplification of calibrators. In 11 of 15 separate analyses, we were able to detect down to 5 copies of the synthetic oligonucleotide in the reaction mixture, as indicated in Fig. 3 of the online Data Supplement.",
            "cite_spans": [],
            "section": "development of real-time quantitative rt-pcr ::: Results",
            "ref_spans": []
        },
        {
            "text": "To determine the precision of the whole analytical procedure, including RNA extraction, reverse transcription, and amplification, we performed 10 replicate RNA extractions from a sample pooled from plasma obtained from 5 SARS patients and subjected these extracted RNA samples to RT-PCR assays. The CV of the copy number of these replicate analyses for the SARSPol1 and SARSN amplification systems were 16% at 280 copies/mL and 15% at 320 copies/mL, respectively. Because we had not carried out recovery experiments, the actual virus concentrations in the tested samples would be expected to be higher than the actual concentrations measured by real-time RT-PCR. However, this would not affect the interpretation of our data because the reproducibility of the RNA extraction and quantitative RT-PCR steps had been tested as detailed above and our system would serve as a reproducible and valid comparison of the virus concentrations within a patient at different times and between different patients.",
            "cite_spans": [],
            "section": "development of real-time quantitative rt-pcr ::: Results",
            "ref_spans": []
        },
        {
            "text": "To investigate whether SARS-CoV RNA could be detected in plasma and to evaluate the relative usefulness of plasma SARS-CoV measurements at different stages of the disease, we studied 12 serologically confirmed SARS patients. Plasma samples were taken on admission, representing a mean of 3.6 days after fever onset (range, 1\u20136 days). Additional samples were taken from each of these patients at days 7 and 14 after fever onset. The SARSPol1 real-time RT-PCR system detected plasma SARS-CoV RNA above the detection limit, as determined by the serially diluted calibrator, in six patients on admission (50%). The detection rate remained at 50% (6 of 12) at day 7 and fell to 25% (3 of 12) at day 14 after fever onset (Table 1 ). As negative controls, SARS-CoV RNA was not detected in the plasma samples obtained from 40 healthy individuals.",
            "cite_spans": [],
            "section": "detectability of plasma sars-CoVrna at different stages of the disease ::: Results",
            "ref_spans": [
                {
                    "start": 716,
                    "end": 723,
                    "mention": "Table 1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "To test the detectability of SARS-CoV RNA in serum, instead of plasma, during the early stage of SARS, we analyzed 23 serum samples obtained from SARS patients on the day of hospital admission, using the SARSPol1 real-time RT-PCR system. For 22 patients, the serum samples were taken at a mean of 2.6 days (range, 1\u20136 days) after the onset of fever. One patient did not have fever during his illness. The SARSPol1 system was able to detect SARS-CoV RNA in 18 of the 23 samples (78%), including the patient who did not have fever. The detection rate essentially confirmed the plasma-based results from our first cohort as described above. The median serum SARS-CoV RNA concentration was 752 copies/mL. As negative controls, SARS-CoV RNA was not detected in serum samples obtained from 30 healthy individuals.",
            "cite_spans": [],
            "section": "quantitative analysis of sars-CoVrna in sera of sars patients ::: Results",
            "ref_spans": []
        },
        {
            "text": "To confirm the data generated by the SARSPol1 RT-PCR system, we used another real-time RT-PCR system (SARSN) targeting the nucleocapsid gene of the SARS-CoV genome to repeat the serum analysis. The SARSN system was able to detect SARS-CoV RNA in 20 of the 23 samples (87%). The median serum SARS-CoV RNA concentration was 3900 copies/mL. The correlation coefficient for SARS-CoV RNA concentrations between the SARSPol1 and SARSN RT-PCR systems was 0.998 (Pearson correlation analysis; P <0.001; Fig. 1 ). As negative controls, SARS-CoV RNA was not detected by the SARSN RT-PCR system in serum samples obtained from 30 healthy individuals.",
            "cite_spans": [],
            "section": "corroborative data from a second rt-pcr system ::: Results",
            "ref_spans": [
                {
                    "start": 495,
                    "end": 501,
                    "mention": "Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "To determine the potential prognostic implications of the admission serum SARS-CoV concentration, the previously mentioned group of 23 patients was stratified into those who required admission to the ICU (n = 11) and those who did not (n = 12) during the duration of their hospitalization. The mean ages of the two groups of patients were 56 and 47 years, respectively (Student t-test, P = 0.188). The male-to-female ratios were 5:6 for the ICU group and 6:6 for the non-ICU group. For the SARSPol1 RT-PCR assay, the median serum SARS-CoV concentrations in the ICU and non-ICU groups were 5800 and 140 copies/mL, respectively (see Fig. 4 in the online Data Supplement). The difference in the serum SARS-CoV concentrations between these groups was statistically significant (Mann\u2013Whitney test, P <0.005). For the SARSN RT-PCR assay, the median serum SARS-CoV concentrations in the ICU and non-ICU groups were 23 000 and 870 copies/mL, respectively (see Fig. 4 in the online Data Supplement). The difference in serum SARS-CoV concentrations between these groups was also statistically significant (Mann\u2013Whitney test, P <0.007).",
            "cite_spans": [],
            "section": "prognostic implications ::: Results",
            "ref_spans": []
        },
        {
            "text": "In this report we have demonstrated that SARS-CoV RNA is detectable in the plasma and serum of patients during the early stages of SARS, with detection rates up to 78% for the SARSPol1 system and 87% for the SARSN system. The findings demonstrate that plasma/serum SARS-CoV measurement is a sensitive method for detecting SARS-CoV infection during the first week after fever onset. This sensitivity, especially for serum, is much higher than the sensitivities reported for other clinical specimen types at a similar stage of infection. For example, RT-PCR analysis of nasopharyngeal aspirates had been reported to have a sensitivity of 32% in the first week after symptom onset (6).",
            "cite_spans": [
                {
                    "start": 679,
                    "end": 680,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The data presented here also demonstrated that the median concentrations of serum SARS-CoV RNA in patients who required ICU admission during the course of hospitalization were 30- and 26-fold higher than the median concentrations in those who did not require intensive care, as measured by the SARSPol1 and SARSN RT-PCR systems, respectively. Our results showed that there is a strong correlation between the serum SARS-CoV RNA concentrations obtained by the SARSPol1 and those obtained by the SARSN RT-PCR system, although the median serum SARS-CoV concentrations differed. The differences observed in the quantitative values may be a result of the coexistence of subgenomic fragments of the N gene with the full virus genome in serum. This could be a subject of further investigation.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "We envision that plasma/serum SARS-CoV measurements can function in a synergistic manner with existing diagnostic strategies for SARS. Thus, plasma/serum RT-PCR can be performed with high sensitivity during the first week of the disease, RT-PCR analysis of stool and respiratory samples can be performed during the second week, and serologic testing for antibodies against SARS-CoV can be used from day 21 onward (6). The availability of a diagnostic test for the early identification of SARS patients could potentially be useful in the public health control of SARS. Furthermore, our data show that serum SARS-CoV measurement is a prognostic marker that can be used even on the first day of hospital admission. Apart from its obvious clinical significance, this observation also suggests that a high systemic viral load may lead to more severe tissue damage either directly or indirectly through the activation of a potentially damaging immune reaction. Elucidation of the latter possibility would be fertile ground for future research.",
            "cite_spans": [
                {
                    "start": 414,
                    "end": 415,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1.: Serial analysis of plasma SARS-CoV RNA concentrations in SARS patients by the RT-PCR system for the polymerase region.1\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Figure 1.: Correlation of serum SARS-CoV RNA concentrations between the SARSPol1 and SARSN RT-PCR systems in SARS patients requiring and not requiring ICU admission. \u25b4, results for SARS patients requiring ICU admission. \u25cb, results for SARS patients not requiring ICU admission. The double slashes indicate the detection limit (74 copies/mL).",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Nature",
            "volume": "423",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Clin Chem",
            "volume": "49",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "J Clin Microbiol",
            "volume": "39",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 2002,
            "venue": "J Clin Microbiol",
            "volume": "40",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}