{
    "paper_id": "PMC4111206",
    "metadata": {
        "title": "Isolation of MERS Coronavirus from a Dromedary Camel, Qatar, 2014",
        "authors": [
            {
                "first": "V.",
                "middle": [
                    "Stalin"
                ],
                "last": "Raj",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Elmoubasher",
                "middle": [
                    "A.B.A."
                ],
                "last": "Farag",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Chantal",
                "middle": [
                    "B.E.M."
                ],
                "last": "Reusken",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mart",
                "middle": [
                    "M."
                ],
                "last": "Lamers",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Suzan",
                "middle": [
                    "D."
                ],
                "last": "Pas",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jolanda",
                "middle": [],
                "last": "Voermans",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Saskia",
                "middle": [
                    "L."
                ],
                "last": "Smits",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Albert",
                "middle": [
                    "D.M.E."
                ],
                "last": "Osterhaus",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Naema",
                "middle": [],
                "last": "Al-Mawlawi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hamad",
                "middle": [
                    "E."
                ],
                "last": "Al-Romaihi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mohd",
                "middle": [
                    "M."
                ],
                "last": "AlHajri",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ahmed",
                "middle": [
                    "M."
                ],
                "last": "El-Sayed",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Khaled",
                "middle": [
                    "A."
                ],
                "last": "Mohran",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hazem",
                "middle": [],
                "last": "Ghobashy",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Farhoud",
                "middle": [],
                "last": "Alhajri",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mohamed",
                "middle": [],
                "last": "Al-Thani",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Salih",
                "middle": [
                    "A."
                ],
                "last": "Al-Marri",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mamdouh",
                "middle": [
                    "M."
                ],
                "last": "El-Maghraby",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marion",
                "middle": [
                    "P.G."
                ],
                "last": "Koopmans",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bart",
                "middle": [
                    "L."
                ],
                "last": "Haagmans",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In February 2014, nasal swab samples were collected from 53 healthy dromedary camels in Doha, Qatar. After sampling, swabs were put into tubes containing viral transport medium and stored at \u221280\u00b0C until shipment to the Netherlands on dry ice, as described (9).",
            "cite_spans": [
                {
                    "start": 257,
                    "end": 258,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Total nucleic acids from nasal swabs were isolated by using the MagnaPure 96 total nucleic acid isolation kit (Roche, Mannheim, Germany), and samples were tested for MERS-CoV by using 2 TaqMan assays: 1 for the envelope (upE) and 1 for the nucleocapsid gene (N), as described previously (9,13). In each assay we detected MERS-CoV RNA in a sample from an 8-month-old camel. The cycle threshold of the positive sample was 12.9 in the upE assay and 11.3 in the N assay.",
            "cite_spans": [
                {
                    "start": 288,
                    "end": 289,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 290,
                    "end": 292,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "For further genomic characterization, RNA was isolated from 50 \u03bcL of 1 swab sample with the QIAamp Viral RNA Mini Kit (QIAGEN, Hilden, Germany), eluted in 60 \u03bcL water, and reverse transcribed with the Superscript III First-Strand Synthesis System (Life Technologies (Bleiswijk, the Netherlands) with random hexamers. The MERS-CoV genome was amplified by using MERS-CoV\u2013specific overlapping primer sets as described previously (3). Amplified MERS-CoV fragments were sequenced directly on both strands by using the BigDye Terminator version 3.1 Cycle Sequencing kit on an ABI PRISM 3100 genetic analyzer (Applied Biosystems, Bleiswijk, the Netherlands). To obtain the 5\u2032 and 3\u2032 ends, we used the FirstChoice RLM-RACE kit (Ambion, Bleiswijk, the Netherlands) according to the manufacturer\u2019s protocols. Using overlapping sequence fragments, we assembled the complete MERS-CoV genome, except for 1 nt potentially missing at the 5\u2032 end.",
            "cite_spans": [
                {
                    "start": 427,
                    "end": 428,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The genome was 30,117 nt long, including 12 nt at the 3\u2032 poly A tail (MERS-CoV camel/Qatar_2_2014, GenBank accession no. KJ650098). Similar to the genome of human MERS-CoV isolates, the genome of camel MERS-CoV isolates contains 10 complete open reading frames (ORFs) (ORF 1ab, spike, ORF3, ORF4a, ORF4b, ORF5, envelope, membrane, nucleocapsid, and ORF8b), 8 transcription-regulatory sequences, and 2 terminal untranslated regions. The alignment of the camel MERS-CoV with known human MERS-CoVs, including 1 near-complete camel MERS-CoV (NRCE_HKU205) sequence, showed overall nucleotide identities of 99.5%\u201399.9% between camel and human MERS-CoV isolates from different geographic regions.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Phylogenetic analysis of the complete genome clearly showed that MERS-CoV camel/Qatar_2_2014 is highly similar to human MERS-CoV; the closest relative to camel MERS-CoV was England/Qatar1 2012 (99.9% identity) (Figure 1), and it was clearly distinct from the camel MERS-CoV (99.5% identity) isolated from camels at a different location in Qatar and in Egypt (9,11). Comparison of spike protein amino acid sequences from various human and camel isolates showed that this protein is highly conserved between this camel virus and other human isolates (online Technical Appendix Table, wwwnc.cdc.gov/EID/article/20/8/14-0663-Techapp1.pdf).",
            "cite_spans": [
                {
                    "start": 359,
                    "end": 360,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 361,
                    "end": 363,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 211,
                    "end": 219,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "In addition, most amino acid residues critical for receptor binding (14) are identical in human and camel isolates, except for L506F in England/Qatar1. The biologic relevance of this mutation has not been investigated. The presence of arginine at position 1020 in the camel virus isolate might indicate that selective pressure at this site has probably not taken place as previously postulated. The fact that a MERS-CoV from a camel is highly similar to that from a human patient who probably became infected >1 year earlier in the same region suggests that this virus is maintained within camel populations and further supports the hypothesis that MERS-CoV can be transmitted from camels to humans.",
            "cite_spans": [
                {
                    "start": 69,
                    "end": 71,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "To test for the presence of infectious virus, we titrated the swab sample on Vero cells (ATCC no. CCL-81). After 48 hours, we observed cytopathic changes in cells (320 50% tissue culture infectious dose/mL). After isolation, the passage-3 virus stock was used for all subsequent experiments.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "To check for adaptive mutations obtained during cell culture, we used 454 deep-sequencing technology (Roche, Indianapolis, IN, USA) to analyze the full-genome sequence as described elsewhere (3). A total of 57,655 sequence reads were obtained, of which 17,056 were specific for MERS-CoV, revealing \u224899.77% of the virus genome. Genome coverage ranged from 1 to 2,082 reads at single nucleotide positions. Gaps or regions with coverage of <4 reads were confirmed by Sanger sequencing. When the genome of the passaged virus was aligned with the genome of the initial clinical isolate, we did not observe any mutations acquired during passaging.",
            "cite_spans": [
                {
                    "start": 192,
                    "end": 193,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "To further functionally characterize this virus isolate, we subsequently inoculated human hepatoma (Huh-7) cells with MERS-CoV camel/Qatar_2_2014. After 2 days, virus-induced cytopathic effects were observed in the inoculated cell cultures (Technical Appendix Figure). In addition, a strong increase in virus titer was measured in the cell supernatant (Figure 2, panel A); produced virus could be passaged (not shown). Virus production in Huh-7 cells was blocked by preincubating camel MERS-CoV with a 1:200 dilution of serum from MERS-CoV antibody\u2013positive camels (9) but not with seronegative camel serum (4) (Figure 2, panel A). Infection of Huh-7 cells could also be blocked by preincubation of cells with polyclonal antiserum against human DPP4 but not with control serum (Figure 2, panel A). Furthermore, transfection of nonsusceptible MDCK cells with human DPP4 (Figure 2, panel B), but not with empty vector, conferred susceptibility to infection with camel MERS-CoV (Figure 2, panel C). These data demonstrate that the MERS-CoV obtained from a dromedary camel is able to replicate in human cells and uses DPP4 as entry receptor, similar to MERS-CoV isolates obtained from human patients (15).",
            "cite_spans": [
                {
                    "start": 566,
                    "end": 567,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 608,
                    "end": 609,
                    "mention": "4",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1197,
                    "end": 1199,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 353,
                    "end": 361,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 612,
                    "end": 620,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 778,
                    "end": 786,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 870,
                    "end": 878,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 976,
                    "end": 984,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "We isolated MERS-CoV from the nasal cavity of 1 dromedary camel and demonstrated its infectiousness. Further studies are needed to test whether camels infected at a young age are more likely than adult dromedary camels to excrete infectious virus, possibly because of the MERS-CoV seronegative status of the younger camels. In addition, our results add to recent findings that MERS-CoVs from camels and humans are nearly identical (9\u201311). As might be expected from the high level of conservation in the critical interacting amino acids in the receptor-binding domain of the camel and human MERS-CoV isolates (online Technical Appendix Table), we show that camel MERS-CoV can infect human Huh-7 cells by using the same entry receptor as the human MERS-CoV isolates (15). Collectively, combined with the observation that the sequence of this virus was most closely related to that of a virus from a human patient who acquired MERS-CoV in Qatar a year earlier, these data support the hypothesis that dromedary camels are a reservoir for MERS-CoV and can transmit the infection to humans. However, whether exposure of humans to camels directly can lead to human infection cannot be concluded from our data. We are not aware of a connection between the camel population sampled in this study and the patient infected with MERS-CoV England/Qatar 1. Future epidemiologic studies are needed to investigate whether contact with camels or camel products constitutes a risk factor for MERS-CoV infection.",
            "cite_spans": [
                {
                    "start": 432,
                    "end": 433,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 434,
                    "end": 436,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 765,
                    "end": 767,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Phylogenetic analysis of Middle East respiratory syndrome coronaviruses (MERS-CoVs). Genome sequences of representative isolates were aligned by using ClustalW, and a phylogenetic tree was constructed by using the PhyML method in Seaview 4 (all 3 software packages can be found at http://pbil.univ-lyon1.fr/software/seaview) and was visualized in FigTree version 1.3.1 (http://tree.bio.ed.ac.uk/software/figtree/). Values at branches show the result of the approximate likelihood ratio; values <0.70 are not shown. The MERS-CoV isolated from a dromedary camel in Qatar in 2014 is depicted in a rectangle. Scale bar indicates nucleotide substitutions per site.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Middle East respiratory syndrome coronavirus (MERS-CoV) from camel replicates in human hepatoma (Huh-7) cells and uses human DPP4 as entry receptor. Huh-7 cells were inoculated with camel MERS-CoV and left for 1 h. Next, cells were washed twice, and supernatant was collected at 2 h (open bars) and 20 h (closed bars) before being tested for MERS-CoV RNA by using a TaqMan assay. We analyzed control camel MERS-CoV\u2013infected cells, cells inoculated with camel MERS-CoV in the presence of normal camel serum (NCS), MERS-CoV\u2013antibody positive camel serum (Ab-positive CS), normal goat serum (NGS), and anti-DPP4 polyclonal antibody\u2013treated cells. Results are expressed as genome equivalents (GE), 50% tissue culture infective dose (TCID50/mL) (A). MDCK cells transfected with plasmid-encoding human DPP4 or a control plasmid (pcDNA) were stained with polyclonal antibody against human DPP4 (B) or inoculated with camel MERS-CoV and fixed 20 h after inoculation (p.i.) and stained for viral antigen (C).",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Isolation of a novel coronavirus from a man with pneumonia in Saudi Arabia.",
            "authors": [],
            "year": 2012,
            "venue": "N Engl J Med",
            "volume": "367",
            "issn": "",
            "pages": "1814-20",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1211721"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.3201/eid2006.140299"
                ]
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Detection of a novel human coronavirus by real-time reverse-transcription polymerase chain reaction.",
            "authors": [],
            "year": 2012,
            "venue": "Euro Surveill",
            "volume": "17",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Molecular basis of binding between novel human coronavirus MERS-CoV and its receptor CD26.",
            "authors": [],
            "year": 2013,
            "venue": "Nature",
            "volume": "500",
            "issn": "",
            "pages": "227-31",
            "other_ids": {
                "DOI": [
                    "10.1038/nature12328"
                ]
            }
        },
        "BIBREF6": {
            "title": "Dipeptidyl peptidase 4 is a functional receptor for the emerging human coronavirus-EMC.",
            "authors": [],
            "year": 2013,
            "venue": "Nature",
            "volume": "495",
            "issn": "",
            "pages": "251-4",
            "other_ids": {
                "DOI": [
                    "10.1038/nature12005"
                ]
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Genomic characterization of a newly discovered coronavirus associated with acute respiratory distress syndrome in humans.",
            "authors": [],
            "year": 2012,
            "venue": "MBiol",
            "volume": "3",
            "issn": "",
            "pages": "e00473-12",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Middle East respiratory syndrome coronavirus neutralising serum antibodies in dromedary camels: a comparative serological study.",
            "authors": [],
            "year": 2013,
            "venue": "Lancet Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "859-66",
            "other_ids": {
                "DOI": [
                    "10.1016/S1473-3099(13)70164-6"
                ]
            }
        },
        "BIBREF10": {
            "title": "Middle East respiratory syndrome (MERS) coronavirus seroprevalence in domestic livestock in Saudi Arabia, 2010 to 2013.",
            "authors": [],
            "year": 2013,
            "venue": "Euro Surveill",
            "volume": "18",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Antibodies against MERS coronavirus in dromedary camels, United Arab Emirates, 2003 and 2013.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "552-9",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2004.131746"
                ]
            }
        },
        "BIBREF12": {
            "title": "Middle East respiratory syndrome coronavirus infection in dromedary camels in Saudi Arabia.",
            "authors": [],
            "year": 2014,
            "venue": "MBio",
            "volume": "5",
            "issn": "",
            "pages": "e00884-14",
            "other_ids": {
                "DOI": [
                    "10.1128/mBio.01002-14"
                ]
            }
        },
        "BIBREF13": {
            "title": "Seroepidemiology for MERS coronavirus using microneutralisation and pseudoparticle virus neutralisation assays reveal a high prevalence of antibody in dromedary camels in Egypt, June 2013.",
            "authors": [],
            "year": 2013,
            "venue": "Euro Surveill",
            "volume": "18",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Middle East respiratory syndrome coronavirus in dromedary camels: an outbreak investigation.",
            "authors": [],
            "year": 2014,
            "venue": "Lancet Infect Dis",
            "volume": "14",
            "issn": "",
            "pages": "140-5",
            "other_ids": {
                "DOI": [
                    "10.1016/S1473-3099(13)70690-X"
                ]
            }
        }
    }
}