{
    "paper_id": "PMC3414035",
    "metadata": {
        "title": "New Variants of Porcine Epidemic Diarrhea Virus, China, 2011",
        "authors": [
            {
                "first": "Wentao",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Heng",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yunbo",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yongfei",
                "middle": [],
                "last": "Pan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Feng",
                "middle": [],
                "last": "Deng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yanhua",
                "middle": [],
                "last": "Song",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xibiao",
                "middle": [],
                "last": "Tang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Qigai",
                "middle": [],
                "last": "He",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "From January 2011 through October 2011, a total of 455 samples (fecal, intestine, and milk) were collected from 57 farms in 12 provinces of China. All samples were evaluated by reverse transcription PCR (RT-PCR), by using previously described primers (6). Forty-five (78.95%) of the farms had at least 1 PEDV-positive sample. A total of 278 (61.11%) samples were PEDV positive, including 253 (of 402; 62.94%) fecal samples, 20 (of 31; 64.52%) intestine samples, and 5 (of 22; 22.73%) milk samples. The representative detection of PEDV in fecal samples of PED-affected farms is shown in Technical Appendix Figure 1.",
            "cite_spans": [
                {
                    "start": 252,
                    "end": 253,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Nine diarrhea samples were collected from pigs at 9 farms (where animals had severe diarrhea and mortality rate was high) for sequencing analysis of the full-length S gene (Technical Appendix Table 1). RT-PCR gene-specific primers were designed on the basis of the sequence of PEDV-CV777 strain (GenBank accession no. AF353511.1) (Table 1) and used to amplify 3 overlapping cDNA fragments spanning the entire S gene. The amplicons were sequenced in both directions (GenScript Co., Nanjing, PRC).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 331,
                    "end": 338,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The 9 PEDV S gene sequences were aligned with the sequences of 24 previously published PEDV S genes (Table 2) by using the ClustalX (version 1.82), Bioedit (version 7.0.9.0) and MegAlign version 5.0 (DNAStar Inc., Madison, WI, USA) software packages (14). The full-length S gene sequences of the 9 isolates from our study showed overall high conservation with the reference strains, up to 94.9%\u201399.6% homology (Technical Appendix Table 2). By phylogenetic analysis, 4 of the field isolates (CH2, CH5, CH6, CH7) clustered with the previously described strain JS-2004\u20132 from China. Three field isolates (CH1, CH8, CHGD-01) formed a unique cluster with the sequence-confirmed variant strain CH-FJND-3, which had been isolated from China in 2011 (7). CH1 and CH8 were isolated from 2 farms, where all sucking piglets had died from diarrhea, even though all of the sows had been vaccinated with the PEDV-CV777 strain\u2013based inactivated vaccine. The isolated variant strains, CHGD-01 and CH1, were tested in experimental infection studies and found to cause illness in 100% of sucking piglets (data not shown).",
            "cite_spans": [
                {
                    "start": 251,
                    "end": 253,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 743,
                    "end": 744,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 101,
                    "end": 108,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The phylogenetic analysis of the S gene nucleotide sequences revealed 3 major clusters (Figure 2). Clade 1 comprised 6 strains from our study (CH2, CH3, CH4, CH5, CH6, CH7), the vaccine strain CV777 from China, the attenuated strain DR13 from South Korea, and 2 strains (CHFJND-1, CHFJND-2) that had been isolated in China in 2011. Clade 2 consisted of 4 variant strains (CH1, CH8, CHFJND-3, CHGD-01) that were identified from China in 2011. Clade 3 was composed of 9 isolates from South Korea and 2 strains from Japan (NK and Kawahira). The deduced amino acids of the 4 variant strains in clade 2 had 93% homology to CV777. Furthermore, the 4 variant strains from China (CH1, CH8, CHGD-01, CH-FJND-3) and 9 PEDV isolates from South Korea shared a 5-aa insertion (at positions 56\u201360 of the S protein) with CV777. One amino acid insertion at position 141 was shared among all variant strains and 6 isolates from South Korea (Technical Appendix Figure 2). In the S genes, 132 point mutations were found that accounted for genetic diversity among the isolates.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 88,
                    "end": 96,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The recent 4 isolates from China (CH2, CH5, CH6, CH7) were closely related to the previously identified isolates from China (JS-2004\u20132, LJB03, DX) and another 4 variant strains. Three of the new isolates (CH1, CH8, CHGD-01) were highly pathogenic in piglets. All strains were obtained from farms that used the CV777-based inactivated vaccine but had 100% prevalence of diarrhea in pigs (Technical Appendix Table 1). Another 2 field isolates (CH3, CH4) from 2 farms with pigs with severe diarrhea shared the highest sequence identity with attenuated strain DR13 from South Korea (99.2% and 99.1%, respectively), which has been in routine use as an oral vaccine against PEDV in South Korea since 2004 (15). The appearance of strains in China similar to those from South Korea and their role in the recent PEDV outbreak should be further investigated.",
            "cite_spans": [
                {
                    "start": 700,
                    "end": 702,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "RT-PCR amplification and sequencing analysis of the full-length PEDV spike genes were used to investigate isolates from diarrhea samples from local pig farms with severe diarrhea in piglets. Both classical and variant strains were detected, implying a diverse distribution profile for PEDV on pig farms in China. The sequence insertions and mutations found in the variant strains may have imparted a stronger pathogenicity to the new PEDV variants that influenced the effectiveness of the CV777-based vaccine, ultimately causing the 2011 outbreak of severe diarrhea on China\u2019s pig farms. Future studies should investigate the biologic role of these particular insertions and mutations. Furthermore, our study of the full-length S gene revealed a more comprehensive distribution profile that reflects the current PEDV status in pig farms in China, including the presence of a strain similar to strain DR13, isolated in South Korea. Collectively, these data indicate the urgent need to develop novel variant strain\u2013based vaccines to treat the current outbreak in China.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Clinical features of pigs infected with porcine epidemic diarrhea virus from pig farms in the People\u2019s Republic of China, 2011. A) Litter of pigs infected with this virus, showing watery diarrhea and emaciated bodies. B) A representative emaciated piglet with yellow, water-like feces. C) Yellow and white vomitus from a representative sucking piglet. D) Thin-walled intestinal structure with light yellow water-like content. E) Congestion in the small intestinal wall and intestinal villi; desquamated epithelial cells from the intestinal villus (original magnification \u00d7100). F) Congestion in the lamina propria of intestinal mucosa, and degeneration, necrosis, and desquamation of epithelial cells of the intestinal villi (original magnification \u00d7400).",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Phylogenetic trees of porcine epidemic diarrhea virus (PEDV) strains generated by the neighbor-joining method with nucleotide sequences of the full-length spike genes. Bootstrapping with 1,000 replicates was performed to determine the percentage reliability for each internal node. Horizontal branch lengths are proportional to genetic distances between PEDV strains. Black circles indicate PEDV field isolates from the 2011 outbreak in China. Scale bar indicates nucleotide substitutions per site.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "A new coronavirus-like particle associated with diarrhea in swine.",
            "authors": [],
            "year": 1978,
            "venue": "Arch Virol",
            "volume": "58",
            "issn": "",
            "pages": "243-7",
            "other_ids": {
                "DOI": [
                    "10.1007/BF01317606"
                ]
            }
        },
        "BIBREF1": {
            "title": "Cloning and sequence analysis of the Korean strain of spike gene of porcine epidemic diarrhea virus and expression of its neutralizing epitope in plants.",
            "authors": [],
            "year": 2005,
            "venue": "Protein Expr Purif",
            "volume": "41",
            "issn": "",
            "pages": "378-83",
            "other_ids": {
                "DOI": [
                    "10.1016/j.pep.2005.02.018"
                ]
            }
        },
        "BIBREF2": {
            "title": "Cloning and further sequence analysis of the spike gene of attenuated porcine epidemic diarrhea virus DR13.",
            "authors": [],
            "year": 2007,
            "venue": "Virus Genes",
            "volume": "35",
            "issn": "",
            "pages": "55-64",
            "other_ids": {
                "DOI": [
                    "10.1007/s11262-006-0036-1"
                ]
            }
        },
        "BIBREF3": {
            "title": "Sequence of the spike protein of the porcine epidemic diarrhoea virus.",
            "authors": [],
            "year": 1994,
            "venue": "J Gen Virol",
            "volume": "75",
            "issn": "",
            "pages": "1195-200",
            "other_ids": {
                "DOI": [
                    "10.1099/0022-1317-75-5-1195"
                ]
            }
        },
        "BIBREF4": {
            "title": "Completion of the porcine epidemic diarrhoea coronavirus (PEDV) genome sequence.",
            "authors": [],
            "year": 2001,
            "venue": "Virus Genes",
            "volume": "23",
            "issn": "",
            "pages": "137-44",
            "other_ids": {
                "DOI": [
                    "10.1023/A:1011831902219"
                ]
            }
        },
        "BIBREF5": {
            "title": "MEGA4: Molecular Evolutionary Genetics Analysis (MEGA) software version 4.0.",
            "authors": [],
            "year": 2007,
            "venue": "Mol Biol Evol",
            "volume": "24",
            "issn": "",
            "pages": "1596-9",
            "other_ids": {
                "DOI": [
                    "10.1093/molbev/msm092"
                ]
            }
        },
        "BIBREF6": {
            "title": "Porcine epidemic diarrhoea virus: a comprehensive review of molecular epidemiology, diagnosis, and vaccines.",
            "authors": [],
            "year": 2012,
            "venue": "Virus Genes",
            "volume": "44",
            "issn": "",
            "pages": "167-75",
            "other_ids": {
                "DOI": [
                    "10.1007/s11262-012-0713-1"
                ]
            }
        },
        "BIBREF7": {
            "title": "Heterogeneity in spike protein genes of porcine epidemic diarrhea viruses isolated in Korea.",
            "authors": [],
            "year": 2010,
            "venue": "Virus Res",
            "volume": "149",
            "issn": "",
            "pages": "175-82",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virusres.2010.01.015"
                ]
            }
        },
        "BIBREF8": {
            "title": "Mutations in the spike gene of porcine epidemic diarrhea virus associated with growth adaptation in vitro and attenuation of virulence in vivo.",
            "authors": [],
            "year": 2011,
            "venue": "Virus Genes",
            "volume": "43",
            "issn": "",
            "pages": "72-8",
            "other_ids": {
                "DOI": [
                    "10.1007/s11262-011-0617-5"
                ]
            }
        },
        "BIBREF9": {
            "title": "The N-terminal region of the porcine epidemic diarrhea virus spike protein is important for the receptor binding.",
            "authors": [],
            "year": 2011,
            "venue": "Korean Journal of Microbiology and Biotechnology.",
            "volume": "39",
            "issn": "",
            "pages": "140-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Isolation of porcine epidemic diarrhea virus in porcine cell cultures and experimental infection of pigs of different ages.",
            "authors": [],
            "year": 2000,
            "venue": "Vet Microbiol",
            "volume": "72",
            "issn": "",
            "pages": "173-82",
            "other_ids": {
                "DOI": [
                    "10.1016/S0378-1135(99)00199-6"
                ]
            }
        },
        "BIBREF11": {
            "title": "Establishment and clinical application of a multiplex reverse transcription PCR for detection of porcine epidemic diarrhea virus, porcine transmissible gastroenteritis virus and porcine group A rotavirus.",
            "authors": [],
            "year": 2010,
            "venue": "Chinese Journal of Animal and Veterinary Sciences.",
            "volume": "41",
            "issn": "",
            "pages": "1001-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Complete genome sequence of a porcine epidemic diarrhea virus variant.",
            "authors": [],
            "year": 2012,
            "venue": "J Virol",
            "volume": "86",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.07150-11"
                ]
            }
        },
        "BIBREF13": {
            "title": "Complete genome sequence of a Chinese virulent porcine epidemic diarrhea virus strain.",
            "authors": [],
            "year": 2011,
            "venue": "J Virol",
            "volume": "85",
            "issn": "",
            "pages": "11538-9",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.06024-11"
                ]
            }
        },
        "BIBREF14": {
            "title": "Cloning and sequence analysis of the spike gene of porcine epidemic diarrhea virus Chinju99.",
            "authors": [],
            "year": 2003,
            "venue": "Virus Genes",
            "volume": "26",
            "issn": "",
            "pages": "239-46",
            "other_ids": {
                "DOI": [
                    "10.1023/A:1024443112717"
                ]
            }
        }
    }
}