{
    "paper_id": "PMC5443424",
    "metadata": {
        "title": "Hospital Outbreaks of Middle East Respiratory Syndrome, Daejeon, South Korea, 2015",
        "authors": [
            {
                "first": "Jung",
                "middle": [
                    "Wan"
                ],
                "last": "Park",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Keon",
                "middle": [
                    "Joo"
                ],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kang",
                "middle": [
                    "Hyoung"
                ],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sang",
                "middle": [
                    "Hyup"
                ],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jung",
                "middle": [
                    "Rae"
                ],
                "last": "Cho",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jin",
                "middle": [
                    "Won"
                ],
                "last": "Mo",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Soo",
                "middle": [
                    "Young"
                ],
                "last": "Choi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Geun",
                "middle": [
                    "Yong"
                ],
                "last": "Kwon",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ji-Yeon",
                "middle": [],
                "last": "Shin",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jee",
                "middle": [
                    "Young"
                ],
                "last": "Hong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jin",
                "middle": [],
                "last": "Kim",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mi-Yeon",
                "middle": [],
                "last": "Yeon",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jong",
                "middle": [
                    "Seok"
                ],
                "last": "Oh",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hae-Sung",
                "middle": [],
                "last": "Nam",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Hospital A is a 300-bed general hospital in Daejeon. The outbreak occurred in ward 51 on the fifth floor, where 13 rooms (5 with 7 beds, 6 with 4 beds, 1 with 2 beds, and 1 with 1 bed) are located. Hospital B is an 800-bed university hospital in Daejeon. The main outbreak occurred in ward 101 on the ninth floor, where 16 rooms (7 with 6 beds, 1 with 4 beds, 1 with 2 beds, and 7 with 1 bed) are located.",
            "cite_spans": [],
            "section": "Setting ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Epidemiologic investigators of the Korea Centers for Disease Control and Prevention started their outbreak investigation with face-to-face interviews of the index case-patient in Daejeon and the 25 additional case-patients with confirmed MERS-CoV infection. We collected data on the demographic characteristics and the clinical, contact, and MERS-CoV exposure histories and thoroughly reviewed the medical records of the case-patients to identify symptoms, underlying concurrent medical conditions, laboratory findings, and clinical courses of illness. Clinical outcome was classified as recovery or death, and the ambulation status of the inpatients at the time of admission was clarified.",
            "cite_spans": [],
            "section": "Data Collection and Exposure Assessment ::: Methods",
            "ref_spans": []
        },
        {
            "text": "We collected the names of inpatients, their room numbers, medical staff, and caregivers (family members or professionals hired by the family or hospital) exposed to MERS-CoV in each hospital. The duration and route of exposure were further determined by reviewing recordings from closed-circuit televisions placed in the hospitals. Moreover, we used the floor plan of each hospital to estimate the spatial distributions and transmission routes of the virus within the hospitals. These estimates enabled us to identify a possible location of exposure and a transmission route for each confirmed case. When a patient with a confirmed case had experienced several possible exposures, we determined the most probable exposure by author consensus. Persons who had had face-to-face contact with patients with confirmed cases were considered the closest contacts. When the data were ambiguous, the following were reviewed independently by the Korea Centers for Disease Control and Prevention and the Daejeon In-Depth team: all potential exposures by symptom onset; disease duration; physical distance from a patient with a confirmed case; and infector factors including ambulation status, symptoms (including a productive cough), and sharing of caregivers. When a patient with a confirmed case had been subjected to several potential exposures, the most probable exposure was determined by consensus of the 2 teams. An expert member of the Korean Society of Epidemiology reviewed all decisions. The process was repeated until a final consensus was obtained.",
            "cite_spans": [],
            "section": "Data Collection and Exposure Assessment ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Sputum samples from the persons suspected of having MERS were collected in sterile cups and sent to qualified local or national laboratories for confirmation. As a confirmatory test, a real-time reverse transcription PCR of nucleic acid extracted from sputum specimens was performed (5). Cycle threshold values were also measured to quantify viral loads. For each patient with a confirmed case, the Korea Centers for Disease Control and Prevention assigned a case number according to the order of confirmation during the 2015 MERS outbreak in South Korea. For example, the case number of the index case-patient in Daejeon was 16.",
            "cite_spans": [
                {
                    "start": 284,
                    "end": 285,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Laboratory Diagnoses ::: Methods",
            "ref_spans": []
        },
        {
            "text": "The cases were described in case-series form. Attack rates were calculated as the number of cases per number of exposed persons (defined as persons who had experienced face-to-face contact with a symptomatic MERS case-patient in either hospital or as persons who had been in the same hospital ward as the symptomatic case-patients). Such persons were identified from the outbreak investigation reports and the lists of those undergoing cohort or home quarantine. To assess differences in attack rates and case-fatality rates according to independent variables, we performed \u03c72 and Fisher exact tests by using SAS software version 9.3 (SAS Institute, Inc., Cary, NC, USA). Comparisons were considered significant at p<0.05 and marginally significant at p<0.1 (both p values were 2-tailed).",
            "cite_spans": [],
            "section": "Data and Statistical Analyses ::: Methods",
            "ref_spans": []
        },
        {
            "text": "We defined the incubation period as the time from exposure to onset of MERS-associated symptoms, including nonspecific signs and symptoms such as fever, chills, cough, sore throat, sputum production, dyspnea, myalgia, headache, nausea, vomiting, diarrhea, and abdominal discomfort. If the exposure period was >2 days, a single interval-censored estimate of the incubation period was computed by using the earliest and latest dates of exposure and the date of symptom onset for each case-patient (coarseDataTools package in R statistical software version 3.2.2) (7). To construct cumulative fraction curves of all cases by incubation period, we calculated the log-normal density function by fitting the interval-censored data on incubation periods. To do this, we used the maximum-likelihood method and calculated the medians and 5th and 95th percentiles of the incubation periods.",
            "cite_spans": [
                {
                    "start": 562,
                    "end": 563,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Data and Statistical Analyses ::: Methods",
            "ref_spans": []
        },
        {
            "text": "The Daejeon index case-patient (case-patient 16), a 41-year-old man, lived in Daejeon and was a former smoker (10 to 20 pack-years). He had undergone colon surgery in August 2014 at hospital P. The index case-patient of the MERS outbreak in South Korea (case-patient 1) was in hospital P during May 15\u201317, 2015. The Daejeon index case-patient was admitted to hospital P at the same time (May 15\u201318, 2015) for a follow-up colonoscopy. After discharge on May 20, the Daejeon index case-patient felt feverish and had chills, cough, general weakness, and diarrhea. Because of these symptoms, he was hospitalized in hospital A in Daejeon during May 22\u201328; the room was shared by 3 inpatients and 1 caregiver. Because his symptoms did not improve, he was transferred to the emergency department at hospital B. After hospitalization in ward 101 in hospital B, he was suspected of having MERS and was isolated in a negative-pressure room on May 30. Ultimately, he became the 16th confirmed MERS patient of 186 total case-patients during the 2015 outbreak.",
            "cite_spans": [],
            "section": "Description of the Daejeon Index Case-Patient  ::: Results",
            "ref_spans": []
        },
        {
            "text": "Before the Daejeon case-patient was isolated, those around him did not use protective equipment. Therefore, virus was spread from him during his first 10 days of illness, before MERS diagnosis and isolation. When we checked the closed-circuit television recordings from hospital A to estimate how many persons could have been in contact with the Daejeon index case-patient, we found that he had been in several sections of the hospital ward, in particular those located on the left side of the nurse station. These sections included his admission room, a restroom, the nurse station, the foyer, and the hall in front of the elevators. The Daejeon index case-patient had potentially contacted every inpatient in the same hospital ward. Therefore, we classified all patients and caregivers in that ward as possible contacts.",
            "cite_spans": [],
            "section": "Description of the Daejeon Index Case-Patient  ::: Results",
            "ref_spans": []
        },
        {
            "text": "A total of 26 cases (including the index case) were confirmed in the 2 hospitals, and 11 case-patients died of MERS (4 in hospital A and 7 in hospital B) (Technical Appendix). Other than the index case, 14 cases occurred in hospital A and 11 in hospital B. Case-patients 30, 38, and 128 were admitted to the same room in hospital A as the Daejeon index case-patient. Case-patient 85 was a caregiver hired by case-patient 128, so she was in the same hospital room in hospital A during May 22\u201328. Case-patients 23, 24, 31, 36, and 95 were admitted to the same room in hospital B as the Daejeon index case-patient. Case-patient 82 was the wife of case-patient 36; case-patient 106 was a caregiver hired by case-patient 36; and case-patient 127 was the wife of case-patient 24. Therefore, when caregiving, they were in the same room as the Daejeon index case-patient.",
            "cite_spans": [],
            "section": "Description of Patients with Confirmed Cases ::: Results",
            "ref_spans": []
        },
        {
            "text": "The median age of the case-patients was 71 (interquartile range 38\u201386) years; 13 (52.0%) were male; 6 (24.0%) were commercial caregivers; and 3 (12.0%) were family caregivers. A total of 18 (72.0%) case-patients had underlying diseases; 7 (28.0%) had pulmonary diseases, such as asthma, chronic obstructive pulmonary disease, idiopathic pulmonary disease, lung cancer, and pulmonary tuberculosis.",
            "cite_spans": [],
            "section": "Description of Patients with Confirmed Cases ::: Results",
            "ref_spans": []
        },
        {
            "text": "All patients reported fever. Other signs and symptoms included chills (10 patients, 38.5%), cough (8, 30.8%), sputum (6, 23.1%), myalgia (9, 34.6%), headache (4, 15.4%), dyspnea (6, 23.1%), nausea (3, 11.5%), diarrhea (6, 23.1%), sore throat (3, 11.5%), rhinorrhea (2, 7.7%), hemoptysis (1, 3.8%), and abdominal discomfort (1, 3.8%)",
            "cite_spans": [],
            "section": "Description of Patients with Confirmed Cases ::: Results",
            "ref_spans": []
        },
        {
            "text": "To prevent the spread of MERS-CoV to the local community, on June 1, 2015, the government of South Korea ordered cohort quarantine, which hospitals A and B followed (Table 1). Persons with a history of exposure to patients with confirmed MERS were isolated in the same hospital ward. ",
            "cite_spans": [],
            "section": "Quarantine ::: Results",
            "ref_spans": [
                {
                    "start": 166,
                    "end": 173,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "After the index case-patient in Daejeon spread MERS-CoV in Daejeon, the first case occurred on May 30, 2015, and the last on June 15, 2015 (total outbreak duration 17 days) (Figure 1). The epidemic curve for hospital A suggested a relatively sporadic pattern compared with that for hospital B. The peak in hospital B comprised mostly patients who shared a hospital room with the index case-patient. Most MERS cases appeared later in professional or family caregivers rather than in inpatients.",
            "cite_spans": [],
            "section": "Epidemic Curve  ::: Results",
            "ref_spans": [
                {
                    "start": 174,
                    "end": 182,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "The estimated median incubation period for confirmed cases was 6.1 (95% CI 4.7\u20137.5) days (Figure 2). Incubation periods were 8.8 (95% CI 7.2\u201310.4) days for hospital A and 4.6 (95% CI 2.9\u20136.2) days for hospital B.",
            "cite_spans": [],
            "section": "Epidemic Curve  ::: Results",
            "ref_spans": [
                {
                    "start": 90,
                    "end": 98,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "In hospital A, the index case-patient was admitted to room 5101, in sector A (Figure 3). Thereafter, 12 case-patients were in sector A, and 1 was in sector B. However, the case-patient in sector B had a history of contact with case-patient 85, who was transferred to sector B from sector A for quarantine. Most case-patients were presumed to have been infected by the Daejeon index case-patient (case-patient 16). However, 3 instances of other transmission were noted: case-patient 85 to case-patient 130, case-patient 54 to case-patient 172, and several case-patients to case-patient 129. For this last instance of transmission, we could not identify the most probable source, because many possible exposures were evident (case-patients 54, 84, 86, 87, 107, and 149).",
            "cite_spans": [],
            "section": "Epidemic Curve  ::: Results",
            "ref_spans": [
                {
                    "start": 78,
                    "end": 86,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "In hospital B, the index case-patient was admitted to room 1007, located on the upper side of ward 101 (sector C). Eight case-patients were in sector C. Case-patient 83 was in room 1013 on the opposite side of ward 101 (sector D). Case-patient 45 was in the emergency room and ward 101 with the index case-patient. Case-patient 148 was presumed to have been infected by case-patient 36 during performance of cardiopulmonary resuscitation in the intensive care unit.",
            "cite_spans": [],
            "section": "Epidemic Curve  ::: Results",
            "ref_spans": []
        },
        {
            "text": "A total of 14 cases developed among 301 persons exposed in hospital A (attack rate 4.7%) and 11 among 371 persons exposed in hospital B (attack rate 3.0%) (Table 2). The attack rates for the sectors hosting the index case-patients (sector A of hospital A, sector C of hospital B; Figure 3) were higher than those for other sectors (sector B of hospital A, sector D of hospital B; Figure 3) (31.6% vs. 2.4% in hospital A, p<0.05; 18.2% vs. 6.5% in hospital B; Table 3). The probability of infection for a person admitted to the same rooms as the index case-patient was 75.0%. In both hospitals, attack rates were somewhat higher for caregivers (22.5%) than for inpatients (12.3%), although statistical significance was not attained.",
            "cite_spans": [],
            "section": "Attack Rate and Case-Fatality Rate ::: Results",
            "ref_spans": [
                {
                    "start": 280,
                    "end": 288,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 380,
                    "end": 388,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 156,
                    "end": 163,
                    "mention": "Table 2",
                    "ref_id": null
                },
                {
                    "start": 459,
                    "end": 466,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The overall case-fatality rate was 44% (Table 4). This rate was higher for patients in hospital B (63.6%) than for those in hospital A (28.6%), although statistical significance was not attained.",
            "cite_spans": [],
            "section": "Attack Rate and Case-Fatality Rate ::: Results",
            "ref_spans": [
                {
                    "start": 40,
                    "end": 47,
                    "mention": "Table 4",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "During the MERS outbreak in South Korea, 25 confirmed cases (including 11 deaths) occurred in Daejeon, all associated with the same index case-patient. Two hospitals were affected. The incubation periods and case-fatality rates for the 2 hospitals differed.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Under the South Korea healthcare system, patients can visit secondary hospitals and the emergency rooms of tertiary hospitals without limitation (5), which probably facilitated nosocomial transmission of MERS-CoV. Indeed, the outbreak in Daejeon accounted for 1 of the 3 major MERS-CoV outbreaks in South Korea. These observations underscored the importance of the outbreak in Daejeon, leading the South Korea government to focus resources on controlling transmission of the virus.",
            "cite_spans": [
                {
                    "start": 146,
                    "end": 147,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The estimated median incubation period for MERS during the outbreak we report was similar to that for outbreaks in the eastern province of Saudi Arabia in 2013 (1). Incubation period estimates may differ, depending on the method used to select exposure: the most probable exposure versus overlapping exposures. In our study, the incubation periods estimated by using both methods were similar. The incubation period estimated by using the most probable exposure method was 6.1 (95% CI 4.7\u20137.5) days, and that estimated by using the overlapping exposures method was 5.6 (95% CI 4.2\u20136.9) days. The overall attack rate among all exposed persons in Daejeon was similar to that for Pyeongtaek (5). The case-fatality rate of the outbreak in Daejeon was lower than that in the eastern province of Saudi Arabia in 2013 (1) but similar to that in Jeddah, Saudi Arabia, in 2014 (3).",
            "cite_spans": [
                {
                    "start": 161,
                    "end": 162,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 689,
                    "end": 690,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 812,
                    "end": 813,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 869,
                    "end": 870,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Our results indicate various epidemiologic characteristics of MERS-CoV. All persons acquired infection in a hospital setting, which is consistent with the previous outbreak in Saudi Arabia, in which nosocomial spread was a major route of MERS-CoV transmission (1). The characteristics of the specific hospital seemed to affect attack rates and case-fatality rates. The index case-patient in Daejeon was consecutively admitted to 2 hospitals. The fifth floor of hospital A specializes in senile patients, most of whom have chronic illnesses, including Parkinson\u2019s disease, paraplegia attributable to old infarctions, or amyotrophic lateral sclerosis. Most beds on the fifth floor are occupied by bedridden patients. The attack rate among caregivers was higher in hospital A than in hospital B. Because immobile patients require personal caregiving, their caregivers were required to be in prolonged close contact with patients, which might have resulted in a higher attack rate. Hospital B is a university hospital and thus contained more severely ill patients than hospital A. Ward 101, to which the Daejeon index case-patient was admitted, is the main ward of the pulmonary medicine department. We presumed that the case-fatality rate was higher for hospital B than hospital A because of underlying pulmonary disease, which has been reported to be a risk factor for development of more severe diseases (8).",
            "cite_spans": [
                {
                    "start": 261,
                    "end": 262,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1404,
                    "end": 1405,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Generally, cohort quarantine may be useful as an infection-control tool to limit virus transmission in hospitals in which susceptible inpatients are gathered or to more effectively detect infected patients (9). Hospitals A and B applied cohort quarantine. In this situation, cohort quarantine had several advantages and disadvantages. The primary purpose was to prevent the spread of MERS-CoV to the local community. After applying cohort quarantine, no further spread of MERS-CoV occurred other than within hospitals A and B. This result may have been achieved by quarantining all persons who had been in contact with MERS-CoV\u2013infected patients and by refusing hospital entry to all susceptible persons. In addition, more cases were diagnosed promptly by active surveillance of the cohort. However, this policy had a limitation. One cohort accommodated inpatients and caregivers in the same hospital room; thus, if 1 person in the cohort was infected by MERS-CoV, others were exposed, increasing the probability of MERS-CoV transmission. This practice raises an ethical issue in terms of whether letting persons stay in the same room with potential MERS patients is justified by the purpose of preventing transmission of the virus to the community. Some caregivers at hospital A may have had difficulty complying with the quarantine policy (the protector and contact rules) because they cared for immobile patients. Thus, this practice may have increased transmission within the hospital.",
            "cite_spans": [
                {
                    "start": 207,
                    "end": 208,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "We identified several cases with uncommon routes of transmission. Case-patient 148 was the head nurse of the intensive care unit to which case-patient 36 was admitted. When case-patient 36 experienced cardiac arrest, that nurse performed cardiopulmonary resuscitation while wearing a level D protector. However, afterward, she may have been exposed to MERS-CoV when she wiped sweat with her bare arm. Case-patient 143 was an employee of hospital A; he worked in information technology. He was employed by hospital A during January\u2013May 30, 2015, and his bedroom was located on the seventh floor. His routine work routes, shown on closed-circuit television, did not reveal any close contact with case-patients; thus, the transmission route was unclear. We presume that he was infected by fomites in an elevator or exposed to a patient in a place lacking closed-circuit television coverage.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The transmission route for case-patient 83 was also unidentified. It is possible that some medical staff and caregiver, contaminated with MERS-CoV after visiting room 1007, may have visited case-patient 83 in room 1013. Of note, when case-patient 83 was exposed to MERS-CoV, the outbreak in Daejeon had not yet been recognized and hospital B had not yet implemented infection control strategies (e.g., handwashing; wearing gloves, masks, and vinyl gowns).",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "This study had several limitations. First, we cannot be certain that all chains of infection between case-patients have been identified. We did not perform serologic analyses to seek cases that were potentially missed; such missed cases may have affected the incubation period estimates and case-fatality rate. Second, closed-circuit television may not have captured all relevant movements.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In conclusion, in 2015, Daejeon experienced a hospital-associated outbreak of MERS-CoV. Two hospitals experienced nosocomial outbreaks, and virus transmission was evident among mostly inpatients and caregivers. To prevent the spread of the virus to the local community, we developed a unique and successful cohort quarantine policy. However, ethical issues associated with this policy require thorough discussion by policy makers. ",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Epidemic curves for the Middle East respiratory syndrome outbreak in Daejeon, South Korea, 2015. The cases are numbered in the order in which they were confirmed in the context of all cases reported during the outbreak. A) Hospitals A and B; B) Hospital A; C) Hospital B. Case-patient 38 is not included because date of illness onset is unknown. Black, weekday; blue, Saturday; red, Sunday or holiday. ",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Estimated incubation periods for the Middle East respiratory syndrome outbreak in Daejeon, South Korea, 2015. Curves indicate estimated cumulative fractions of cases corresponding to the incubation periods, estimated by creating log-normal density functions fitting the observed data. Horizontal lines indicate 95% CIs for the 5th, 50th, and 95th percentiles of the estimated incubation periods. A) Total; estimated median incubation period was 6.1 (95% CI 4.7\u20137.5) days. B) Hospital A; estimated median incubation period was 8.8 (95% CI 7.2\u201310.4) days. C) Hospital B; estimated median incubation period was 4.6 (95% CI 2.9\u20136.2) days.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3: Locations of Middle East respiratory syndrome case-patients in hospitals A and B, Daejeon, South Korea, 2015, showing where case-patients were exposed to presumed infectors. Not shown are case-patient 143, an engineer working in hospital A, because the location of his exposure is unclear; case-patient 45, a family caregiver in either the emergency department or room 1015; and case-patient 148, a nurse in the intensive care unit.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Hospital outbreak of Middle East respiratory syndrome coronavirus.",
            "authors": [],
            "year": 2013,
            "venue": "N Engl J Med",
            "volume": "369",
            "issn": "",
            "pages": "407-16",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1306742"
                ]
            }
        },
        "BIBREF1": {
            "title": "Middle East respiratory syndrome coronavirus: epidemiology and disease control measures.",
            "authors": [],
            "year": 2014,
            "venue": "Infect Drug Resist",
            "volume": "7",
            "issn": "",
            "pages": "281-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "2014 MERS-CoV outbreak in Jeddah\u2014a link to health care facilities.",
            "authors": [],
            "year": 2015,
            "venue": "N Engl J Med",
            "volume": "372",
            "issn": "",
            "pages": "846-54",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1408636"
                ]
            }
        },
        "BIBREF3": {
            "title": "Middle East respiratory syndrome coronavirus outbreak in the Republic of Korea.",
            "authors": [],
            "year": 2015,
            "venue": "Osong Public Health Res Perspect",
            "volume": "6",
            "issn": "",
            "pages": "269-78",
            "other_ids": {
                "DOI": [
                    "10.1016/j.phrp.2015.08.006"
                ]
            }
        },
        "BIBREF4": {
            "title": "2015 MERS outbreak in Korea: hospital-to-hospital transmission.",
            "authors": [],
            "year": 2015,
            "venue": "Epidemiol Health",
            "volume": "37",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.4178/epih/e2015033"
                ]
            }
        },
        "BIBREF5": {
            "title": "Epidemiologic features of the first MERS outbreak in Korea: focus on Pyeongtaek St.",
            "authors": [],
            "year": 2015,
            "venue": "Epidemiol Health",
            "volume": "37",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.4178/epih/e2015041"
                ]
            }
        },
        "BIBREF6": {
            "title": "Estimating incubation period distributions with coarse data.",
            "authors": [],
            "year": 2009,
            "venue": "Stat Med",
            "volume": "28",
            "issn": "",
            "pages": "2769-84",
            "other_ids": {
                "DOI": [
                    "10.1002/sim.3659"
                ]
            }
        },
        "BIBREF7": {
            "title": "Epidemiological, demographic, and clinical characteristics of 47 cases of Middle East respiratory syndrome coronavirus disease from Saudi Arabia: a descriptive study.",
            "authors": [],
            "year": 2013,
            "venue": "Lancet Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "752-61",
            "other_ids": {
                "DOI": [
                    "10.1016/S1473-3099(13)70204-4"
                ]
            }
        },
        "BIBREF8": {
            "title": "Understanding, compliance and psychological impact of the SARS quarantine experience.",
            "authors": [],
            "year": 2008,
            "venue": "Epidemiol Infect",
            "volume": "136",
            "issn": "",
            "pages": "997-1007",
            "other_ids": {
                "DOI": [
                    "10.1017/S0950268807009156"
                ]
            }
        }
    }
}