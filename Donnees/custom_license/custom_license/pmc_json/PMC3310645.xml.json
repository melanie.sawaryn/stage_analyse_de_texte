{
    "paper_id": "PMC3310645",
    "metadata": {
        "title": "Pandemic (H1N1) 2009 among Quarantined Close Contacts, Beijing, People\u2019s Republic of China",
        "authors": [
            {
                "first": "Xinghuo",
                "middle": [],
                "last": "Pang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peng",
                "middle": [],
                "last": "Yang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Shuang",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Li",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Lili",
                "middle": [],
                "last": "Tian",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yang",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bo",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yi",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Baiwei",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ruogang",
                "middle": [],
                "last": "Huang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xinyu",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Quanyi",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In 2009, under the guidance of the Beijing Center for Disease Prevention and Control (Beijing CDC), a network of 55 collaborating laboratories was established to perform reverse transcription PCR testing to confirm cases of pandemic (H1N1) 2009 (15). The confirmed cases included symptomatic and asymptomatic cases, and these cases were detected mainly by border entry screening, ILI screening in hospitals, health follow-up of travelers from overseas, and quarantine and testing of close contacts. Once confirmed, index case-patients were immediately quarantined in designated hospitals to receive treatment while in isolation. All the confirmed cases were required by law to be reported to Beijing and local CDCs. From May through October 2009, a detailed epidemiologic investigation was conducted for each confirmed case of pandemic (H1N1) 2009 (including symptomatic and asymptomatic cases) by Beijing and local CDCs within 6 hours after confirmation of infection. Patients with confirmed cases were interviewed about demographic characteristics, course of illness, travel and contact history, and information about close contacts. Patients with confirmed cases were categorized as having imported cases (travelers) and locally acquired cases (no travel history) on the basis of where the infection was acquired.",
            "cite_spans": [
                {
                    "start": 246,
                    "end": 248,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Confirmation of Index Cases ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Close contacts were defined as anyone who ever came within 2 meters of an index case-patient without the use of effective personal protective equipment (PPE) (including masks and gloves, with or without gowns or goggles) during the presumed infectious period. Trained staff from local CDCs made the determinations on the basis of field investigation. The relationships of close contacts to index case-patients were categorized as 1) spouses, 2) other household members, 3) nonrelated roommates, 4) contacts at workplace or school, 5) nonhousehold relatives, 6) passengers on the same flight, 7) friends, and 8) service persons met at public places. A close contact on an aircraft was defined as a passenger sitting within 3 rows in front and 3 rows behind the index case-patient.",
            "cite_spans": [],
            "section": "Definition of Close Contacts ::: Methods",
            "ref_spans": []
        },
        {
            "text": "All close contacts were traced and quarantined for 7 days after the most recent exposure to the index case-patient. All index case-patients detected between May 16 (the first case, the date of confirmation) and September 15, 2009 (before widespread transmission in Beijing), and their close contacts were included in this study. We excluded cluster or outbreak cases for which close contacts could not be determined clearly by epidemiologic investigation (the transmission chain was obscure).",
            "cite_spans": [],
            "section": "Definition of Close Contacts ::: Methods",
            "ref_spans": []
        },
        {
            "text": "For each close contact, before quarantine, a pharyngeal swab specimen was collected for reverse transcription PCR testing, regardless of symptoms. A second pharyngeal swab specimen was collected for testing for pandemic (H1N1) 2009 virus if any of the following symptoms developed in a close contact during quarantine: axillary temperature >37.3\u00b0C, cough, sore throat, nasal congestion, or rhinorrhea.",
            "cite_spans": [],
            "section": "Laboratory Screening ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Data were analyzed by using SPSS version 11.5 (SPSS Inc., Chicago, IL, USA). Median and range values were calculated for continuous variables, and percentages were calculated for categorical variables. Differences in attack rates were compared between subgroups of close contacts by using the \u03c72 test. For the significant difference found in multiple subgroups, this test does not enable identification of which multiple subgroups are significantly different, only that across all the subgroups there are differences. The variables with p<0.10 in \u03c72 test were included in multivariate analysis. Multivariate unconditional logistic regression analysis was conducted to determine risk factors associated with infection in close contacts. Backward logistic regression was conducted by removing variables with p>0.10. Odds ratios (ORs) and 95% confidence intervals were calculated for potential risk factors of infection. The Hosmer-Lemeshow goodness-of-fit test was used to assess the model fit for logistic regression. All statistical tests were 2-sided, and significance was defined as p<0.05.",
            "cite_spans": [],
            "section": "Statistical Analysis ::: Methods",
            "ref_spans": []
        },
        {
            "text": "A total of 613 eligible index case-patients, detected from May 16 through September 15, 2009, were included in this study. Through field epidemiologic investigations, 7,099 close contacts were traced and quarantined in Beijing. The median number of close contacts per index case per day was 7.0 persons (range 2.0\u201395.0 persons); the median number for an imported index case was 7.0 persons (range 1.7\u201395.0 persons) and for a locally acquired index case was 5.3 persons (range 1.0\u201325.0 persons). For the 601 symptomatic index case-patients, the median interval between illness onset and sample collection was 1.0 days (range \u22121.9 to 7.0 days).",
            "cite_spans": [],
            "section": "Timeliness and Intensity of Index Case Detection and Contact Tracing ::: Results",
            "ref_spans": []
        },
        {
            "text": "Among close contacts with symptomatic infection, the median interval between illness onset and sample collection was 0.5 days. More than 85% of close contacts were quarantined within 72 hours after interview of the index case-patients. The median interval between first exposure and quarantine was 3.4 days for the close contacts, and it was shorter, on average, for flight passenger contacts than nonpassenger contacts (1.7 days vs. 3.8 days). For symptomatic close contacts infected with pandemic (H1N1) 2009, the median of generation time (i.e., the time from illness onset in an index case to illness onset in a secondary case) were 2.4 days; it was shorter for flight passenger contacts than nonpassenger contacts (1.6 days vs. 2.5 days) (Table 1).",
            "cite_spans": [],
            "section": "Timeliness and Intensity of Index Case Detection and Contact Tracing ::: Results",
            "ref_spans": [
                {
                    "start": 744,
                    "end": 751,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Approximately 43% of the index case-patients were women; the median age was 20 years, and 38% likely contracted pandemic (H1N1) 2009 virus locally because they had not traveled recently. Among the index case-patientss, 2% had subclinical infection. Only 18% of index case-patients had close contacts with confirmed pandemic (H1N1) 2009 (Table 2), and the total number of close contacts who were infected by the virus from 110 index case-patients was 167.",
            "cite_spans": [],
            "section": "Characteristics of Index Case-Patients and Close Contacts ::: Results",
            "ref_spans": [
                {
                    "start": 337,
                    "end": 344,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Fifty percent (3,514 of 7,032) of close contacts were women, and the median age was 27 years. Approximately 12% of close contacts were household member of index case-patients (spouse or other household member), and aircraft passengers accounted for 44% of close contacts. Approximately 61% of close contacts were exposed to symptomatic index case-patients during their symptomatic phase. About 70% were quarantined in a quarantine station (Table 2).",
            "cite_spans": [],
            "section": "Characteristics of Index Case-Patients and Close Contacts ::: Results",
            "ref_spans": [
                {
                    "start": 440,
                    "end": 447,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The overall attack rate for infection among close contacts (positive test result) was 2.4% (167 of 7,099), indicating that 1 index case-patient transmitted infection to 0.27 close contacts (167 of 613) on average (reproduction number = 0.27). Among those close contacts with a positive test result, 14.4% (24 of 167) had subclinical infection; among the close contacts with positive test results at the start of quarantine, 17.2% (20 of 116) had subclinical infection.",
            "cite_spans": [],
            "section": "Attack Rate ::: Results",
            "ref_spans": []
        },
        {
            "text": "Attack rates did not differ by index case-patient\u2019s sex (p = 0.225). However, attack rates differed significantly by index case-patient\u2019s age (p = 0.022), and the lower attack rate was found for older index case-patients. There was no significant difference in attack rates between close contacts of patients with imported cases and those with locally acquired cases (p = 0.282). No infection was found in close contacts exposed to index case-patients with subclinical infection, and the attack rate observed in close contacts exposed to symptomatic index case-patients during their symptomatic phase was higher (p<0.001). Almost identical attack rates were found among male and female close contacts (p = 0.808). However, attack rates were significantly different among different age groups of close contacts (p<0.001), and the lowest attack rate was found for those >50 years of age. The attack rates were significantly different across 8 contact types (p<0.001). The attack rate was 5.3% among spouses and 6.6% among other family members in the household, and was lower among other types of close contacts (Table 3).The attack rate among passengers on the same flight was low, 0.9% overall, and 1 index case-patient transmitted infection to 0.19 close contacts on a flight on average (28 of 147), and the attack rate was higher among the passengers with longer flight times (>12 hours, p = 0.001). The attack rate among close contacts of service persons at public places was 0.2%, and 1 index case-patient transmitted infection to 0.01 close contacts of service persons on average (1 of 113). Nonpassenger close contacts with longer exposure duration (>12 hours), compared with those with shorter duration (>12 hours), recorded the higher attack rate (p<0.001) (Table 3).",
            "cite_spans": [],
            "section": "Attack Rate ::: Results",
            "ref_spans": [
                {
                    "start": 1110,
                    "end": 1117,
                    "mention": "Table 3",
                    "ref_id": null
                },
                {
                    "start": 1765,
                    "end": 1772,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "By multivariate analysis, age and type of contact were the major predictors of infection (Table 4). Compared with close contacts >50 years of age, those 20\u201350 years (OR 3.42; p = 0.002) and 0\u201319 years of age (OR 7.76; p< 0.001) were at higher risk for infection. Other significant independent risk factors associated with infection included being a household member of a person with an index case (OR 3.83; p<0.001), being exposed to index case-patients during their symptomatic phase (OR 1.86; p = 0.003), and exposure duration >12 hours (OR 1.83; p = 0.002). Similar risk factors were observed among aircraft passengers.",
            "cite_spans": [],
            "section": "Risk Factors ::: Results",
            "ref_spans": [
                {
                    "start": 90,
                    "end": 97,
                    "mention": "Table 4",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "We estimated that pandemic (H1N1) 2009 virus was transmitted by 18% of index case-patients to their close contacts and that 2.4% (167 of 7,099) of close contacts we traced were infected. Our data indicate that pandemic (H1N1) 2009 virus has low transmissibility in nonoutbreak settings.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "We found that 1 index case-patient transmitted infection to 0.27 close contacts on average, i.e., reproduction number = 0.27. This finding suggests that among those quarantined index case-patients, the number of persons with secondary cases who could be traced through rigorous field investigation was small and far less than the number needed for the sustainable transmission of infectious disease in the population (reproduction number >1). However, the fact that the pandemic eventually spread in Beijing indicates that contact and case tracing were far from complete, especially later in the summer and early fall of 2009. The strict control measures may have worked to some extent at the beginning but were outpaced by local transmission (16); the percentage of locally acquired infections ranged from <10% in June 2009 to >80% in September 2009 (data not shown).",
            "cite_spans": [
                {
                    "start": 744,
                    "end": 746,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In this study, the median number of close contacts per index case-patient per day was 7.0 persons. Although locating and quarantining these close contacts was done quickly, and stringent quarantine measures were used, which hindered implementation of control measures, the real number of close contacts was unknown and probably exceeded this number. Many close contacts were persons met in public places, including public transportation, theaters or cinemas, and shopping malls, and it is nearly impossible to trace all of the contacts. In addition, some persons who had worn PPE during contact with index case-patients were excluded from close contacts management (i.e., they were not quarantined), but because wearing PPE might not protect (or fully protect) against infection, some persons excluded might have become infected. In addition, many persons with mild and asymptomatic cases cannot be detected, but they may transmit the virus. Furthermore, the short generation time of pandemic (H1N1) 2009 shown in this study and in a previous study (13) could lead to the rapid accumulation of infection sources and close contacts. This rapid compounding could overwhelm response capacity and would have resulted in compromised effectiveness of containment measures. It should also be mentioned that we did not include persons with cluster or outbreak cases for whom close contacts could not be determined clearly by epidemiologic investigation to examine the basic feature of pandemic (H1N1) 2009 (e.g., generation time), and the reproduction number obtained from our data is an underestimate.",
            "cite_spans": [
                {
                    "start": 1050,
                    "end": 1052,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Attack rates of infection differed significantly by contact type. Among household members of index case-patients, the attack rate was the highest, as shown in the multivariate analysis after controlling for age and other factors. The most likely reason for this finding is that household members are more likely to have come into closer contact with index case-patients for a longer period with shorter distance and longer duration. Another possible reason is that household members may have some certain linkage with index cases in genetic susceptibility or living habits that would cause higher predisposition in household members than in other close contacts. This finding is similar to findings in other investigations of respiratory infectious disease (17).",
            "cite_spans": [
                {
                    "start": 758,
                    "end": 760,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Close contacts on flights accounted for the highest proportion of all the close contacts, in part because of how the index cases were detected and the broad definition we used for close contacts. However, the attack rate was much lower than that for other close contacts; 1 index case infected only 0.19 close contacts on flights on average. This finding indicated that the possibility of transmission of pandemic (H1N1) 2009 virus on flights was low, and the yield of tracing and quarantining of close contacts on flights was limited. Tracing contacts of service persons at public places was more difficult than tracing other categories of contacts, and the lowest attack rate (0.2%) was recorded in this category. Despite extensive measures, on average, only 0.01 infected close contacts per index case-patient were identified among service persons. Tracing the contacts of service persons at public places seems far less cost-effective. Criteria for close contacts on flights and those of service persons should be refined with respect to exposure duration and age of those exposed.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Exposure to index case-patients for >12 hours was a significant independent risk factor for infection in flight passenger contacts. This finding suggests that limiting the time of contact with persons with ILI on aircraft can reduce risk for transmission, and a long duration of exposure may be necessary for transmission to occur on aircraft.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Younger close contacts were at higher risk for infection than older ones. The possible reason was that younger persons had much closer contact with index case-patients than did older persons; another reason may be that younger persons were more susceptible to infection with pandemic (H1N1) 2009 virus (18). This finding was consistent with findings reported in other studies (5,6).",
            "cite_spans": [
                {
                    "start": 303,
                    "end": 305,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 377,
                    "end": 378,
                    "mention": "5",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 379,
                    "end": 380,
                    "mention": "6",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "No secondary cases were found among close contacts exposed to index case-patients with subclinical infection. The attack rate among close contacts who were exposed to symptomatic index case-patients during their symptomatic phase was much higher than that among those exposed to these case-patients before their illness onset. Exposure to index case-patients during the symptomatic phase was a significant independent risk factor for infection among close contacts. These findings indicate that the infectivity of pandemic (H1N1) 2009 virus was higher after illness onset, and that the infectivity of symptomatic pandemic (H1N1) 2009 case-patients before illness onset was higher than that of persons with subclinical cases, although persons in each group were asymptomatic when in contact with other persons.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In general, the earliest infectious time for pandemic (H1N1) 2009 was considered as 1 day before illness onset (19). We found that index case-patients and infected close contacts shed pandemic (H1N1) 2009 virus <1 day before illness onset, which suggests that the infectious period of symptomatic persons with pandemic (H1N1) 2009 might be <1 day before illness onset.",
            "cite_spans": [
                {
                    "start": 112,
                    "end": 114,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Among close contacts with pandemic (H1N1) 2009, \u224814.4% were asymptomatic. It is noteworthy that specimens from some close contacts tested negative for pandemic (H1N1) 2009 virus before quarantine, but those persons could shed the virus during quarantine without symptoms. Such infection could not be detected, and the proportion of subclinical infection was underestimated. Therefore, we calculated the proportion of subclinical infection by cross-sectional analysis of the subclinical infection of close contacts before quarantine, and we found that \u224817% of case-patients with pandemic (H1N1) 2009 were asymptomatic.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "This study has several limitations. We could not find all close contacts of persons with pandemic (H1N1) 2009 and did not know their infection status, so the infection parameters of pandemic (H1N1) 2009 that we found in this study might not be precise, especially for reproduction number, which may be underestimated to some extent. Furthermore, we could not exclude the possibility that the infected close contacts had been infected from another unknown source before quarantine started, which might influence our conclusion to some extent.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In summary, the attack rate among close contacts was low, even among household contacts. Household member and younger age were the major risk factors for infection with pandemic (H1N1) 2009 virus among close contacts. Approximately 17% of cases of pandemic (H1N1) 2009 were asymptomatic.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Pneumonia and respiratory failure from swine-origin influenza A (H1N1) in Mexico.",
            "authors": [],
            "year": 2009,
            "venue": "N Engl J Med",
            "volume": "361",
            "issn": "",
            "pages": "680-9",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa0904252"
                ]
            }
        },
        "BIBREF1": {
            "title": "Subclinical infection with the novel influenza A (H1N1) virus.",
            "authors": [],
            "year": 2009,
            "venue": "Clin Infect Dis",
            "volume": "49",
            "issn": "",
            "pages": "1622-3",
            "other_ids": {
                "DOI": [
                    "10.1086/644775"
                ]
            }
        },
        "BIBREF2": {
            "title": "Shedding and transmission of novel influenza virus A/H1N1 infection in households\u2014Germany, 2009.",
            "authors": [],
            "year": 2010,
            "venue": "Am J Epidemiol",
            "volume": "171",
            "issn": "",
            "pages": "1157-64",
            "other_ids": {
                "DOI": [
                    "10.1093/aje/kwq071"
                ]
            }
        },
        "BIBREF3": {
            "title": "Clinical and epidemiologic characteristics of 3 early cases of influenza A pandemic (H1N1) 2009 virus infection, People\u2019s Republic of China, 2009.",
            "authors": [],
            "year": 2009,
            "venue": "Emerg Infect Dis",
            "volume": "15",
            "issn": "",
            "pages": "1418-22",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1509.090794"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2009,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Outbreak of swine-origin influenza A (H1N1) virus infection\u2014Mexico, March\u2013April 2009.",
            "authors": [],
            "year": 2009,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "58",
            "issn": "",
            "pages": "467-70",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Severe, critical and fatal cases of 2009 H1N1 influenza in China.",
            "authors": [],
            "year": 2010,
            "venue": "J Infect",
            "volume": "61",
            "issn": "",
            "pages": "277-83",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jinf.2010.07.010"
                ]
            }
        },
        "BIBREF7": {
            "title": "Alternative epidemic of different types of influenza in 2009\u20132010 influenza season, China.",
            "authors": [],
            "year": 2010,
            "venue": "Clin Infect Dis",
            "volume": "51",
            "issn": "",
            "pages": "631-2",
            "other_ids": {
                "DOI": [
                    "10.1086/655766"
                ]
            }
        },
        "BIBREF8": {
            "title": "Evaluation of control measures implemented in the severe acute respiratory syndrome outbreak in Beijing, 2003.",
            "authors": [],
            "year": 2003,
            "venue": "JAMA",
            "volume": "290",
            "issn": "",
            "pages": "3215-21",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.290.24.3215"
                ]
            }
        },
        "BIBREF9": {
            "title": "Incidence of 2009 pandemic influenza A H1N1 infection in England: a cross-sectional serological study.",
            "authors": [],
            "year": 2010,
            "venue": "Lancet",
            "volume": "375",
            "issn": "",
            "pages": "1100-8",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(09)62126-7"
                ]
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": 2009,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Swine influenza A (H1N1) infection in two children\u2014Southern California, March\u2013April 2009.",
            "authors": [],
            "year": 2009,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "58",
            "issn": "",
            "pages": "400-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Pandemic potential of a strain of influenza A (H1N1): early findings.",
            "authors": [],
            "year": 2009,
            "venue": "Science",
            "volume": "324",
            "issn": "",
            "pages": "1557-61",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1176062"
                ]
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [],
            "year": 2009,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Household transmission of 2009 influenza A (H1N1) virus after a school-based outbreak in New York City, April\u2013May 2009.",
            "authors": [],
            "year": 2010,
            "venue": "J Infect Dis",
            "volume": "201",
            "issn": "",
            "pages": "984-92",
            "other_ids": {
                "DOI": [
                    "10.1086/651145"
                ]
            }
        },
        "BIBREF15": {
            "title": "Household transmission of 2009 pandemic influenza A (H1N1) virus in the United States.",
            "authors": [],
            "year": 2009,
            "venue": "N Engl J Med",
            "volume": "361",
            "issn": "",
            "pages": "2619-27",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa0905498"
                ]
            }
        },
        "BIBREF16": {
            "title": "Transmission of pandemic A/H1N1 2009 influenza on passenger aircraft: retrospective cohort study.",
            "authors": [],
            "year": 2010,
            "venue": "BMJ",
            "volume": "340",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1136/bmj.c2424"
                ]
            }
        },
        "BIBREF17": {
            "title": "Emergence of a novel swine-origin influenza A (H1N1) virus in humans.",
            "authors": [],
            "year": 2009,
            "venue": "N Engl J Med",
            "volume": "360",
            "issn": "",
            "pages": "2605-15",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa0903810"
                ]
            }
        },
        "BIBREF18": {
            "title": "Clinical features of the initial cases of 2009 pandemic influenza A (H1N1) virus infection in China.",
            "authors": [],
            "year": 2009,
            "venue": "N Engl J Med",
            "volume": "361",
            "issn": "",
            "pages": "2507-17",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa0906612"
                ]
            }
        }
    }
}