{
    "paper_id": "PMC7087974",
    "metadata": {
        "title": "Human metapneumovirus in adults: a short case series",
        "authors": [
            {
                "first": "C.",
                "middle": [],
                "last": "O\u2019Gorman",
                "suffix": "",
                "email": "ciaran.ogorman@bll.n-i.nhs.uk",
                "affiliation": {}
            },
            {
                "first": "E.",
                "middle": [],
                "last": "McHenry",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "P.",
                "middle": [
                    "V."
                ],
                "last": "Coyle",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Acute respiratory tract infections are a major cause of morbidity and mortality worldwide, and they account for a high percentage of hospital admissions. However, up to 50% of cases of community-acquired pneumonia in adults and 15\u201335% of cases of bronchiolitis and pneumonia in children are of uncertain aetiology [1\u20133]. It is clear that respiratory viruses are the principal pathogens in a proportion of such cases [4].",
            "cite_spans": [
                {
                    "start": 315,
                    "end": 316,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 317,
                    "end": 318,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 417,
                    "end": 418,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "hMPV is a recently described respiratory pathogen of the subfamily Pneumovirinae, which is divided into the Pneumovirus genus, containing respiratory syncytial virus, and the Metapneumovirus genus, containing hMPV. In young children and elderly patients hMPV is most commonly associated with a clinical diagnosis of bronchiolitis or bronchitis, respectively, whereas in middle-aged adults, it may produce an influenza-like illness, which can be complicated by pneumonitis in the presence of immunocompromising factors [5]. hMPV has been detected as the sole pathogen in the nasopharyngeal aspirate of a haematopoietic stem cell transplant recipient who subsequently died of respiratory failure following an upper respiratory prodrome [6].",
            "cite_spans": [
                {
                    "start": 519,
                    "end": 520,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 735,
                    "end": 736,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The Regional Virus Laboratory in Belfast carries out viral diagnostics for Northern Ireland, a region with a population of 1,700,000. It began routine use of a multiplex reverse-transcriptase polymerase chain reaction (RT-PCR) method for virus detection from respiratory samples in July of 2003 [7]. The samples were from three sources: adult hospital inpatients (25.57% of total processed), paediatric hospital inpatients (64.48%) and general practitioners participating in a sentinel influenza surveillance program (9.96%). The laboratory tested any specimens of respiratory tract material including bronchoalveolar lavage, nasopharyngeal and tracheal aspirates, and sputum, nose and throat swabs.",
            "cite_spans": [
                {
                    "start": 296,
                    "end": 297,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "This retrospective observational study reviewed all cases of hMPV detected in patients over 18 years of age, from the time the RT-PCR method was adopted in July 2003 through to January 2005. We selected adult cases for review as there is less data in the literature pertaining to hMPV infection in this age group.",
            "cite_spans": [],
            "section": "Patients and methods",
            "ref_spans": []
        },
        {
            "text": "The routine assay used to detect respiratory viruses in the regional virus laboratory is a multiplex, nested RT-PCR for 12 common respiratory pathogens: influenza A (H3 and H1) and B; parainfluenza virus types 1, 2 and 3; respiratory syncytial virus A and B; adenovirus; human rhinovirus; coronavirus 229E; and hMPV. The PCR target for hMPV was the fusion gene. Sensitivity was determined at 3\u00d7103 copies per ml using an hMPV amplicon cloned from a clinical specimen during the preliminary development of the molecular strip assay. Direct comparison of the assay with an alternative was not possible because of the lack of specimens containing hMPV or alternative tests; virus identification was confirmed by selected amplicon sequencing [7].",
            "cite_spans": [
                {
                    "start": 739,
                    "end": 740,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Patients and methods",
            "ref_spans": []
        },
        {
            "text": "Following ethics committee approval, we obtained written consent from the living patients in whom hMPV was detected. We reviewed their clinical case notes, noting presenting symptoms, significant medical and drug histories, results of other infectious investigations, chest radiograph findings, and clinical outcomes.",
            "cite_spans": [],
            "section": "Patients and methods",
            "ref_spans": []
        },
        {
            "text": "A total of 741 respiratory samples from adults were tested over the winters of 2003/04 and 2004/05. Six were positive for hMPV, accounting for a period prevalence in the population assayed of 0.81%. In all cases, hMPV was the only virus detected. Unless otherwise stated, bacterial culture and serology for atypical respiratory pathogens were negative. The clinical characteristics of these six patients are summarised in Table 1. \n\n",
            "cite_spans": [],
            "section": "Results and discussion",
            "ref_spans": [
                {
                    "start": 428,
                    "end": 429,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "Of the six cases, five were female and one male. Their ages ranged from 51 to 83 years (mean 71.5 years). Two cases presented to sentinel general practitioners with self-limiting influenza-like illness (cases 1 and 2). Neither had a background respiratory disease or any other significant aspect of their medical history. Two cases were diagnosed while admitted for acute exacerbations of chronic obstructive pulmonary disease (cases 3 and 4). They presented with dyspnoea, decreased exercise tolerance, cough and low-grade pyrexia with a background of respiratory disease requiring home oxygen and nebulised corticosteroids. Both had been smokers, had received prednisolone as inpatients, and were discharged on maintenance oral steroids. Sputum culture of case 3 revealed Serratia liquefaciens, which was treated with intravenous ertapenem; for case 4, hMPV was the sole pathogen isolated. Two cases (5 and 6) were fatal.",
            "cite_spans": [],
            "section": "Results and discussion",
            "ref_spans": []
        },
        {
            "text": "The fatalities occurred in individuals receiving immunosuppressive therapy for rheumatological conditions. Case 5 had rheumatoid arthritis for which she received prednisolone and methotrexate. She presented in a collapsed and unresponsive condition following a 1-week history of cough, nausea and increasing dyspnoea. Within 6 h of admission she was intubated, ventilated and admitted to the intensive care unit (ICU). hMPV was the sole pathogen isolated. She died with multiorgan failure 4 days after admission.",
            "cite_spans": [],
            "section": "Results and discussion",
            "ref_spans": []
        },
        {
            "text": "Case six was admitted in an acute confusional state attributed to the oral steroids she was taking for polymyalgia rheumatica. She had been an inpatient for 14 days when she became acutely dyspnoeic with hypoxia, hypothermia and hypotension requiring intubation and admission to the ICU. She was found to have bacteremia caused by Escherichia coli and was treated with piperacillin-tazobactam, fluconazole and pulsed methylprednisolone. On the first day of her ICU stay, having been an inpatient for 15 days, she was found to have hMPV. This patient made no significant progress while receiving maximal inotropic and ventilatory support and she died after 2 weeks in the ICU.",
            "cite_spans": [],
            "section": "Results and discussion",
            "ref_spans": []
        },
        {
            "text": "Whilst our detection of hMPV in symptomatic patients does not establish causation, it is possible that the virus may be associated with serious sequelae in high-risk adults as reported previously [8] and supported by the fact that two of our six cases required ICU admission and ventilation. We cannot be sure that case 6 represented nosocomial acquisition as our assay may detect hMPV up to 2 weeks after clinical infection. The clinical syndrome associated with this virus is probably too indistinct to be of diagnostic value. Treatment is principally supportive; at present there are no antiviral agents active against hMPV, nor any available vaccine.",
            "cite_spans": [
                {
                    "start": 197,
                    "end": 198,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Results and discussion",
            "ref_spans": []
        },
        {
            "text": "Effective diagnosis of viral respiratory pathogens is necessary to elucidate their epidemiology and assess their clinical impact. Moreover, such data is crucial for effective infection control in hospitals and may ultimately allow more rational prescribing of antibiotics and corticosteroids [9]. Defining the assay sensitivity by comparison with a gold-standard detection system is a growing problem for viruses such as hMPV or human rhinovirus, for which PCR is becoming adopted as the diagnostic test of choice [8]. We recommend the routine use of RT-PCR or other nucleic acid-based techniques for adults presenting with respiratory symptoms and a background of pre-existing respiratory disease or immunocompromise.",
            "cite_spans": [
                {
                    "start": 293,
                    "end": 294,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 515,
                    "end": 516,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Results and discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table\u00a01: Clinical characteristics of six adult cases of human metapneumovirus\n\nF female, M male, ICU intensive care unit, COPD chronic obstructive pulmonary disease, RR respiratory rate",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "The Tucson Children\u2019s Respiratory Study II. Lower respiratory tract illness in the first year of life",
            "authors": [
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Wright",
                    "suffix": ""
                },
                {
                    "first": "LM",
                    "middle": [],
                    "last": "Taussig",
                    "suffix": ""
                },
                {
                    "first": "CG",
                    "middle": [],
                    "last": "Ray",
                    "suffix": ""
                },
                {
                    "first": "HR",
                    "middle": [],
                    "last": "Harrison",
                    "suffix": ""
                },
                {
                    "first": "CJ",
                    "middle": [],
                    "last": "Holberg",
                    "suffix": ""
                }
            ],
            "year": 1989,
            "venue": "Am J Epidemiol",
            "volume": "129",
            "issn": "",
            "pages": "1232-1246",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Etiology of community-acquired pneumonia: impact of age, comorbidity, and severity",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ruiz",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Ewig",
                    "suffix": ""
                },
                {
                    "first": "MA",
                    "middle": [],
                    "last": "Marcos",
                    "suffix": ""
                },
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Martinez",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Arancibia",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Mensa",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Torres",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "Am J Respir Crit Care Med",
            "volume": "160",
            "issn": "",
            "pages": "397-405",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Prospective comparative study of viral, bacterial and atypical organisms identified in pneumonia and bronchiolitis in hospitalized Canadian infants",
            "authors": [
                {
                    "first": "HD",
                    "middle": [],
                    "last": "Davies",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Matlow",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Petric",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Glazier",
                    "suffix": ""
                },
                {
                    "first": "EE",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 1996,
            "venue": "Pediatr Infect Dis J",
            "volume": "15",
            "issn": "",
            "pages": "371-375",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Virological features and clinical manifestations associated with human metapneumovirus: a new paramyxovirus responsible for acute respiratory-tract infections in all age groups",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Boivin",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Abed",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Pelletier",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Ruel",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Moisan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Cote",
                    "suffix": ""
                },
                {
                    "first": "TC",
                    "middle": [],
                    "last": "Peret",
                    "suffix": ""
                },
                {
                    "first": "DD",
                    "middle": [],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "LJ",
                    "middle": [],
                    "last": "Anderson",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J Infect Dis",
            "volume": "186",
            "issn": "",
            "pages": "1330-1334",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "A newly discovered human pneumovirus isolated from young children with respiratory tract disease",
            "authors": [
                {
                    "first": "BG",
                    "middle": [],
                    "last": "van den Hoogen",
                    "suffix": ""
                },
                {
                    "first": "JC",
                    "middle": [],
                    "last": "de Jong",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Groen",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kuiken",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "de Groot",
                    "suffix": ""
                },
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Fouchier",
                    "suffix": ""
                },
                {
                    "first": "AD",
                    "middle": [],
                    "last": "Osterhaus",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Nat Med",
            "volume": "7",
            "issn": "",
            "pages": "719-724",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Human metapneumovirus in a haematopoietic stem cell transplant recipient with fatal lower respiratory tract disease",
            "authors": [
                {
                    "first": "PA",
                    "middle": [],
                    "last": "Cane",
                    "suffix": ""
                },
                {
                    "first": "BG",
                    "middle": [],
                    "last": "van den Hoogen",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Chakrabarti",
                    "suffix": ""
                },
                {
                    "first": "CD",
                    "middle": [],
                    "last": "Fegan",
                    "suffix": ""
                },
                {
                    "first": "AD",
                    "middle": [],
                    "last": "Osterhaus",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Bone Marrow Transplant",
            "volume": "31",
            "issn": "",
            "pages": "309-310",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "A touchdown nucleic acid amplification protocol as an alternative to culture backup for immunofluorescence in the routine diagnosis of acute viral respiratory tract infections",
            "authors": [
                {
                    "first": "PV",
                    "middle": [],
                    "last": "Coyle",
                    "suffix": ""
                },
                {
                    "first": "GM",
                    "middle": [],
                    "last": "Ong",
                    "suffix": ""
                },
                {
                    "first": "HJ",
                    "middle": [],
                    "last": "O\u2019Neill",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "McCaughey",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "De Ornellas",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Mitchell",
                    "suffix": ""
                },
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "Mitchell",
                    "suffix": ""
                },
                {
                    "first": "SA",
                    "middle": [],
                    "last": "Feeney",
                    "suffix": ""
                },
                {
                    "first": "DE",
                    "middle": [],
                    "last": "Wyatt",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Forde",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Stockton",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "BMC Microbiol",
            "volume": "4",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Human metapneumovirus\u2014an important new respiratory virus",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "McIntosh",
                    "suffix": ""
                },
                {
                    "first": "AJ",
                    "middle": [],
                    "last": "McAdam",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "N Engl J Med",
            "volume": "350",
            "issn": "",
            "pages": "431-433",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Prevalence and clinical symptoms of human metapneumovirus infection in hospitalized patients",
            "authors": [
                {
                    "first": "BG",
                    "middle": [],
                    "last": "van den Hoogen",
                    "suffix": ""
                },
                {
                    "first": "GJ",
                    "middle": [],
                    "last": "van Doornum",
                    "suffix": ""
                },
                {
                    "first": "JC",
                    "middle": [],
                    "last": "Fockens",
                    "suffix": ""
                },
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "Cornelissen",
                    "suffix": ""
                },
                {
                    "first": "WE",
                    "middle": [],
                    "last": "Beyer",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "de Groot",
                    "suffix": ""
                },
                {
                    "first": "AD",
                    "middle": [],
                    "last": "Osterhaus",
                    "suffix": ""
                },
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Fouchier",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Infect Dis",
            "volume": "188",
            "issn": "",
            "pages": "1571-1577",
            "other_ids": {
                "DOI": []
            }
        }
    }
}