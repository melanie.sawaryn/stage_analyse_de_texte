{
    "paper_id": "PMC7096019",
    "metadata": {
        "title": "China\u2019s response to a novel coronavirus stands in stark contrast to the 2002 SARS outbreak response",
        "authors": [
            {
                "first": "John",
                "middle": [],
                "last": "Nkengasong",
                "suffix": "",
                "email": "nkengasongj@africa-union.org",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "On 8 December 2019, the Chinese government identified the death of 1 patient and 41 others hospitalized with unknown etiology in Wuhan, a city in central China2 that has an estimated population of about 10 million and is a major hub of transportation3.",
            "cite_spans": [
                {
                    "start": 159,
                    "end": 160,
                    "mention": "2",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 250,
                    "end": 251,
                    "mention": "3",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "2019-nCoV",
            "ref_spans": []
        },
        {
            "text": "Less than a month after the first patients were identified, on 1 January 2020, the government shut down a seafood market known to sell live exotic small animals for food, with which most of the 41 laboratory-confirmed cases were found to have links4.",
            "cite_spans": [
                {
                    "start": 248,
                    "end": 249,
                    "mention": "4",
                    "ref_id": "BIBREF12"
                }
            ],
            "section": "2019-nCoV",
            "ref_spans": []
        },
        {
            "text": "In a massive effort of national and international coordination, a consortium led by Chinese scientist and international researchers was rapidly brought together. This included the Shanghai Public Health Clinical Center and School of Public Health, the Central Hospital of Wuhan, the Chinese Center for Disease Control and Prevention (China CDC), the Huazhong University of Science and Technology, the Wuhan Center for Disease Control and Prevention, the National Institute for Communicable Disease Control and Prevention, and the University of Sydney, Australia.",
            "cite_spans": [],
            "section": "2019-nCoV",
            "ref_spans": []
        },
        {
            "text": "By 10 January 2020, the consortium had sequenced and publicly released partial sequences obtained from a patient in Wuhan that demonstrated at least 70% similarity in genetic material to severe acute respiratory syndrome (SARS)5. Such transparency in sharing sequence information is critical for the development of diagnostic tests and potential therapy and vaccines to help control the outbreak, should it spread widely. The sequences were deposited in GenBank (accession code MN908947).",
            "cite_spans": [
                {
                    "start": 227,
                    "end": 228,
                    "mention": "5",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "2019-nCoV",
            "ref_spans": []
        },
        {
            "text": "Early observations seem to suggest that the fatality (2 deaths out of 48 laboratory-confirmed cases, or 4.1%) of 2019-nCoV may be less than that of SARS. The animal reservoir, pathogenesis, epidemiology and clinical spectrum of 2019-nCoV still need to be better understood.",
            "cite_spans": [],
            "section": "2019-nCoV",
            "ref_spans": []
        },
        {
            "text": "The Chinese government\u2019s response to this new outbreak has been swift and decisive. It shows a marked departure from public health policies that, during the SARS outbreak in 2002, contributed to the deaths of 774 people, spread of the disease to 37 countries and an economic loss of over US$40 billion over a period of 6 months6,7.",
            "cite_spans": [
                {
                    "start": 327,
                    "end": 328,
                    "mention": "6",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 329,
                    "end": 330,
                    "mention": "7",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "From SARS to 2019-nCoV: China\u2019s rapid progress in outbreak response",
            "ref_spans": []
        },
        {
            "text": "It is clear that in just under two decades, China has made remarkable progress in responding effectively to disease outbreaks. First, a notable change in public-health political posture has allowed China to admit, very early on, the existence of a novel coronavirus. In response to the 2002 SARS outbreak, the Chinese government did not report the abnormal new infection to the World Health Organization until 4 months after the first case was reported8.",
            "cite_spans": [
                {
                    "start": 452,
                    "end": 453,
                    "mention": "8",
                    "ref_id": "BIBREF16"
                }
            ],
            "section": "From SARS to 2019-nCoV: China\u2019s rapid progress in outbreak response",
            "ref_spans": []
        },
        {
            "text": "Second, the government was decisive in closing the sea food market in Wuhan, borrowing lessons from the 2002 SARS outbreak, which was brought to an end only when the palm civet that is eaten in China was subsequently identified as the reservoir and was removed from markets.",
            "cite_spans": [],
            "section": "From SARS to 2019-nCoV: China\u2019s rapid progress in outbreak response",
            "ref_spans": []
        },
        {
            "text": "Third, the rapid development of a national and international consortium helped in the swift analysis of the virus and in making the sequences publicly available within a few days. In contrast, the first laboratory sequences suggesting that a new coronavirus was the cause of SARS were reported on 24 March 2003, 5 months after the first cases were recognized in November 2002.",
            "cite_spans": [],
            "section": "From SARS to 2019-nCoV: China\u2019s rapid progress in outbreak response",
            "ref_spans": []
        },
        {
            "text": "The SARS outbreak in 2002 clearly highlighted the weaknesses of China CDC\u2019s system, their premier public health agency. However, once the outbreak ended, the government prioritized the strengthening of the CDC systems, improving public-health surveillance and laboratory systems, as well as the workforce-development program, through the Field Epidemiology Training Program. This investment in core public-health systems and infrastructure will no doubt be critical in the response to the current 2019-nCoV outbreak. In fact, an evaluation conducted in 2012 showed the remarkable progress that China CDC has made since 2002, which has resulted in quicker responses to emergent epidemics, with an overall completeness of public-health services significantly increasing from 47.4% to 76.6% (ref. 9).",
            "cite_spans": [
                {
                    "start": 794,
                    "end": 795,
                    "mention": "9",
                    "ref_id": "BIBREF17"
                }
            ],
            "section": "From SARS to 2019-nCoV: China\u2019s rapid progress in outbreak response",
            "ref_spans": []
        },
        {
            "text": "China CDC has also played a critical role in strengthening a tiered national public-health system in the country. The agency has also recognized that global disease threats can affect China; thus, they are now actively exporting their expertise to assist other developing countries to better prepare for and respond to emerging pathogen outbreaks, including active support for the West Africa Ebola virus disease epidemic in 2013.",
            "cite_spans": [],
            "section": "From SARS to 2019-nCoV: China\u2019s rapid progress in outbreak response",
            "ref_spans": []
        },
        {
            "text": "As of 20 January 2020, the Chinese government reported 136 new cases of infection with this virus over the weekend that were spreading to other cities in the country, bringing the total cumulative cases worldwide to over 200 (ref. 10). Cases of infection with 2019-nCoV have now been reported in several other countries in the region: on 8 January 2020, Thailand reported a case of infection by 2019-nCoV of a 61-year-old Chinese tourist from Wuhan in Bangkok11. On 10 January 2020, the Japanese Ministry of Health reported a male patient who was hospitalized and tested positive for the virus12,13. Suspected cases have also been reported in Hong Kong and Singapore14,15.",
            "cite_spans": [
                {
                    "start": 231,
                    "end": 233,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 459,
                    "end": 461,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 593,
                    "end": 595,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 596,
                    "end": 598,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 666,
                    "end": 668,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 669,
                    "end": 671,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Why Africa should be prepared",
            "ref_spans": []
        },
        {
            "text": "A modeling analysis has estimated that over 1,700 people may have been infected16. The analysis, which had a large 95% confident interval ranging from 427 to 4,471, made assumptions based on the vast international air traffic from Wuhan, which is a travel hub, and from the incubation periods of the patients in Thailand and Japan.",
            "cite_spans": [
                {
                    "start": 79,
                    "end": 81,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Why Africa should be prepared",
            "ref_spans": []
        },
        {
            "text": "The rapid spread of 2019-nCoV in Asia, as a result of air traffic and vast population movements, should matter in Africa. The SARS outbreak in 2002 largely spared Africa, as only one case was reported in South Africa\u2014a businessman who traveled to Hong Kong17. But air traffic between China and Africa has risen by over 600% in the past decade as a result of the rapid expansion of Chinese investment in Africa18. For instance, Ethiopian Airlines, the largest carrier in Africa, today operates almost half of the 2,616 annual flights from Africa to China18.",
            "cite_spans": [
                {
                    "start": 256,
                    "end": 258,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 409,
                    "end": 411,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 553,
                    "end": 555,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Why Africa should be prepared",
            "ref_spans": []
        },
        {
            "text": "Thus, African countries need to be on the alert and strengthen their public-health surveillance and laboratory systems, coordinated by functional national public-health institutions, in order to better prepare to prevent, rapidly detect and control any eventual spread of the novel virus on the continent. There is also a need for closer coordination of efforts between the Africa Centres for Disease Control and Prevention, based in Addis Ababa, Ethiopia, and China CDC for sharing information on potential people suspected of being infected who are traveling from China to Africa.",
            "cite_spans": [],
            "section": "Why Africa should be prepared",
            "ref_spans": []
        },
        {
            "text": "China\u2019s political openness to reporting in a timely manner and the emergence of the novel virus 2019-nCoV, coupled with the rapid sequencing and public sharing of the sequences, represents a new dawn for global health security and international health diplomacy. In addition, there is hope that the much-strengthened networks of China CDC will provide a huge return on public-health investment in tackling the outbreak, should it spread more broadly in China. It will also ensure the world\u2019s health security, as the global health chain is only as strong as its weakest link, so a disease threat anywhere can quickly become a threat everywhere.",
            "cite_spans": [],
            "section": "Why Africa should be prepared",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Mukherjee",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Indian J. Dermatol.",
            "volume": "62",
            "issn": "",
            "pages": "459-467",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Cohen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Normile",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Science",
            "volume": "367",
            "issn": "",
            "pages": "234-235",
            "other_ids": {
                "DOI": [
                    "10.1126/science.367.6475.234"
                ]
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [
                {
                    "first": "RD",
                    "middle": [],
                    "last": "Smith",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Soc. Sci. Med.",
            "volume": "63",
            "issn": "",
            "pages": "3113-3123",
            "other_ids": {
                "DOI": [
                    "10.1016/j.socscimed.2006.08.004"
                ]
            }
        },
        "BIBREF15": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Am. J. Public Health",
            "volume": "106",
            "issn": "",
            "pages": "2093-2102",
            "other_ids": {
                "DOI": [
                    "10.2105/AJPH.2016.303508"
                ]
            }
        }
    }
}