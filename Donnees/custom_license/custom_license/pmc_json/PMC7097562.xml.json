{
    "paper_id": "PMC7097562",
    "metadata": {
        "title": "In the news",
        "authors": []
    },
    "body_text": [
        {
            "text": "The transmissible agent of spongiform encephalopathies, the prion, has now been found in the inflamed mammary glands of sheep with scrapie. Although prion proteins were initially thought to be confined to the nervous system and immune tissues of infected hosts, recent research in mice has shown that immune cells can transport prion proteins to inflamed organs such as the kidney, liver and pancreas. A team of scientists led by Adriano Aguzzi now shows in Nature Medicine that this is also true for farm animals. The group analysed 261 sheep that were genetically susceptible to prion disease and found seven animals with clinical scrapie. Four sheep had both scrapie and mastitis, and the inflamed mammary glands of these four animals contained infectious prion particles. By contrast, the mammary glands of the scrapie-sick sheep without mastitis were prion free. The presence of prions in mammary glands raises fears about their transmission to animals and humans through milk. Although prions have not as yet been detected in milk, this is primarily because of technical difficulties, and Aguzzi predicts that they will eventually be found. Nature Med.",
            "cite_spans": [],
            "section": "Keeping abreast of scrapie",
            "ref_spans": []
        },
        {
            "text": "Two papers published in Nature have revealed insights about the protein-interaction networks of the malaria parasite Plasmodium falciparum. Despite complete genome sequencing and large-scale gene-expression and proteomic analyses, most P. falciparum proteins have not been functionally characterized. LaCount et al. used high-throughput two-hybrid assays to map 2,846 unique protein\u2013protein interactions in P. falciparum. Suthram et al. compared the LaCount data set with interactomes for other model organisms, including Saccharomyces cerevisiae, Caenorhabditis elegans, Drosophila melanogaster and Helicobacter pylori. The authors found that the Plasmodium protein network diverged from those of other model organisms, which reflects the unique lifestyle of this parasite. It is hoped that the Plasmodium proteome will provide unique targets for therapeutic and preventive interventions. Nature",
            "cite_spans": [],
            "section": "Malaria interactome stands alone",
            "ref_spans": []
        },
        {
            "text": "Although combination antiretroviral therapy has dramatically improved the prognosis of patients infected with HIV, eradication of HIV in treated patients has not been achieved owing to the persistence of residual virus in T cells. A recent study in the Journal of Clinical Investigation characterizes the nature and source of persisting HIV virus in patients receiving long-term antiretroviral therapy. Although it has long been believed that the resting CD4+ T-cell compartment is the main source of persistent HIV virus, Tae-Wook Chun et al. show that it is activated CD4+ T cells that are the main reservoir of HIV proviral DNA. The authors propose that latently infected, resting CD4+ T cells are reactivated as part of the normal immune response, which results in the spread of virions to neighbouring resting and activated CD4+ T cells. In this way, the CD4+ T-cell viral reservoir is constantly replenished. These new insights into the source of persistent HIV virus will allow the development of new strategies aimed at eradication of the virus from infected patients. J. Clin. Invest.",
            "cite_spans": [],
            "section": "Activated CD4@!start@+@!end@ T cells harbour HIV",
            "ref_spans": []
        },
        {
            "text": "An epidemic of poliomyelitis that began in Nigeria in 2003 and spread through Africa into the Middle East and Indonesia has been brought to a halt in ten African countries. The epidemic had resulted in over 200 cases of paralytic poliomyelitis, but since the implementation of a successful mass immunization campaign, no cases have been reported in ten central and west African countries since June 2005. A maintenance programme that hopes to immunize more than 100 million children across 28 African countries has now been launched. It is hoped that commitment of this sort will irradicate poliomyelitis worldwide. However, the obstacles to achieving this goal were illustrated by the recent documentation of five cases of poliomyelitis in an Amish community in central Minnesota, USA. Although the source of the infection is unknown, the first reported case was that of an unimmunized child with an immunodeficiency syndrome. The other infected children had also not been vaccinated. Poliovirus is highly transmissible, and this outbreak highlights problems faced by those trying to eradicate poliomyelitis worldwide. WHO, NYT",
            "cite_spans": [],
            "section": "Polio cases rise and fall",
            "ref_spans": []
        },
        {
            "text": "New research has shown that wrapping up warmly does indeed prevent the symptoms of the common cold. Researchers in Wales put the old maxim to the test in a trial involving 180 people. Half of the volunteers were subjected to a 'foot chill' by immersing their feet in cold water, whereas the other half sat with their feet in an empty bowl. 29% of the chilled subjects reported common-cold symptoms within the next five days compared with 9% of the controls. It is thought that exposure to cold conditions causes constriction of the blood vessels in the nose, reducing the supply of immune cells and allowing dormant viruses to flourish. CNN",
            "cite_spans": [],
            "section": "The common cold \u2014 chilling truths",
            "ref_spans": []
        },
        {
            "text": "The reputation of bats has been further tarnished by a recent study in Nature which argues that fruit bats might be the reservoir for Ebola virus. Three fruit-bat species captured during Ebola outbreaks in Gabon and the Republic of Congo were found to have asymptomatic Ebola virus infection. The infected animals harboured Ebola virus RNA or showed evidence of an immune response to the virus. This follows closely on the finding that bats are the most likely source of the SARS coronavirus. Nature",
            "cite_spans": [],
            "section": "Bats might be Ebola reservoir",
            "ref_spans": []
        },
        {
            "text": "Scientists have found a way of preventing bilharzia infection by destroying infective schistosome larvae. After leaving aquatic snail hosts, schistosome larvae are attracted to their human hosts by the presence of skin lipids in the water. They subsequently shed their outer coat and work their way through the skin into the bloodstream. If they are unable to penetrate the skin, the unprotected larvae swell and burst. Researchers at the John Hopkins Bloomberg School of Public Health, Baltimore, USA, have now developed a formulation made of red cedarwood oil and surfactant. This mix spreads evenly on the water surface, and the unsaturated fatty acids it contains mimics skin lipids and tricks schistosome larvae into uncoating. In the absence of a human host, the unprotected parasites swell and burst. Bilharzia infects 200 million people in Africa, the Middle East and Asia, and therefore it is hoped that this compound can be developed for use in the field. ScienceNOW",
            "cite_spans": [],
            "section": "Pop goes the schistosome!",
            "ref_spans": []
        },
        {
            "text": "The fourth MIM Pan-African malaria conference took place in Yaound\u00e9 in the Cameroon in November, attracting more than 1,500 participants from 65 countries. Themes encompassed molecular and clinical aspects of malaria, vector biology, bioethics and capacity building. Coinciding with the conference, several studies have emerged that cover important issues related to malaria. A study published in the Lancet has shown that a promising vaccine candidate, RTS,S/AS02A \u2014 a recombinant protein that consists of a Plasmodium falciparum protein fused to the hepatitis B surface antigen \u2014 reduced clinical malaria episodes by 35% for the 18-month period of follow-up. These results were also presented at the Yaound\u00e9 conference. A letter to Nature used mathematical modelling to show that certain people in malarious areas are more likely to be bitten by mosquitoes than others. It is not only obvious factors, such as the use of nets and the proximity to stagnant water, that are responsible for this selective biting; factors such as the chemicals in sweat or breath could also be important. A team of UK scientists have identified an enzyme that is crucial for the successful entry of Plasmodium falciparum into red blood cells, and that is therefore a promising drug target. According to the study in PLoS Pathogens, the protease PSUB2 (also known as a sheddase) allows the parasite to shed sticky surface proteins and to detach from the surface of the red cell. Another group of researchers has focused on mosquito immunity to find possible ways of combating the spread of the disease. Reporting in PNAS, the authors identified a gene known as SRPN6 that is expressed in mosquito cells only when they are infected with the malaria parasite. Knockdown of SRPN6 markedly increased parasite numbers, and the authors hope that modulating the activity of SRPN6 might control the spread of the parasite. BBC, Nature, PLoS Pathog., PNAS",
            "cite_spans": [],
            "section": "Some more malaria news",
            "ref_spans": []
        },
        {
            "text": "Avian influenza has now reached the Gulf states \u2014 a wild flamingo found on a southern Kuwaiti beach has tested positive for the H5N1 virus, making this the first confirmed case of avian flu in the Middle East. The first three human cases (two fatal) of the the H5N1 avian influenza virus have been confirmed in China, which is racing to vaccinate 14 billion chickens and other poultry. There are now regular reports of outbreaks of avian flu in poultry in China, and international health experts have voiced concerns that the problem of human disease in China is underestimated. Meanwhile, Thailand announced its twenty-first case of human avian flu, the first case of the disease in Bangkok. Three more human fatalities were reported in Indonesia in November and, in a worrying development, hundreds of chickens have died from avian flu in the tsunami-hit Aceh province in Indonesia, where people are living in overcrowded refugee camps. Indonesia has now secured permission from Roche Holding AG to mass-produce Tamiflu for consumption in the country. Birds afflicted with the H5N1 strain have also been found in Croatia and Romania. Researchers continue to search for clues as to the highly pathogenic nature of the virus. A study published in Respiratory Research postulates that H5N1 might cause damage by overstimulating the immune response. Effective treatment could therefore encompass immunosuppressive therapy. WHO, Resp. Res.",
            "cite_spans": [],
            "section": "Avian influenza update",
            "ref_spans": []
        },
        {
            "text": "World AIDS Day was observed on December 1st with a call by the United Nations for an \u201cexceptional response\u201d to the threat of HIV. There are now more people than ever living with the virus \u2014 40.3 million in 2005. Initiatives were launched to halt the spread of the disease in countries throughout the globe. The British government contributed $48 million to fight the disease, and the tiny mountain kingdom of Lesotho launched a campaign for all its citizens to know their HIV status by the end of 2007. Strategies will include free and confidential door-to-door HIV testing and counselling, and the establishment of independent peoples committees, to ensure that testing is voluntary and confidential. WHO, WP",
            "cite_spans": [],
            "section": "World AIDS Day",
            "ref_spans": []
        },
        {
            "text": "Yellow fever. An outbreak of yellow fever has been reported in Mali. As of November 22, there have been 53 suspected cases and 23 deaths. A mass vaccination campaign in the district has been completed, and six surrounding high-risk areas will also be targeted. Sudan has also reported an outbreak of yellow fever in the South Kordofan State. A vaccination campaign is underway.",
            "cite_spans": [],
            "section": "Outbreak news",
            "ref_spans": []
        },
        {
            "text": "Dengue fever. The first case of dengue haemorrhagic fever acquired from a mosquito in the United States has been diagnosed in Brownsville, Texas. The young woman has been treated successfully and is among 15 cases in Brownsville currently being investigated for dengue fever.",
            "cite_spans": [],
            "section": "Outbreak news",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {}
}