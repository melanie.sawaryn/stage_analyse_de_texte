{
    "paper_id": "PMC7099968",
    "metadata": {
        "title": "Antibody-mediated targeting of viral vectors to the Fc receptor expressed on acute myeloid leukemia cells",
        "authors": [
            {
                "first": "T",
                "middle": [],
                "last": "W\u00fcrdinger",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "M",
                "middle": [
                    "H"
                ],
                "last": "Verheije",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "L",
                "middle": [
                    "M"
                ],
                "last": "van der Aa",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "B",
                "middle": [
                    "J"
                ],
                "last": "Bosch",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "C",
                "middle": [
                    "A",
                    "M"
                ],
                "last": "de Haan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "V",
                "middle": [
                    "W"
                ],
                "last": "van Beusechem",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "W",
                "middle": [
                    "R"
                ],
                "last": "Gerritsen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "P",
                "middle": [
                    "J",
                    "M"
                ],
                "last": "Rottier",
                "suffix": "",
                "email": "p.rottier@vet.uu.nl",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Acute myeloid leukemia (AML) is a cancer of the blood and bone marrow. Despite advances in chemotherapy and radiotherapy, first-line therapy of AML results in long-term disease-free survival in only 20\u201330% of patients. Selective approaches targeting cytotoxic agents to these cells might offer a promising avenue for the specific elimination of residual disease following classical therapy. One of the currently explored experimental approaches involves the viral vector-mediated transduction of such residual acute myeloid leukemic cells with immunomodulatory genes.1 This has been described for a number of viral vector systems including lentiviruses, retroviruses, adenoviruses, adeno-associated virus and HSV-1 amplicons. One of the major advantages in the context of immunomodulatory gene transfer is the preclinical observation of antileukemic bystander effects, suggesting that not every cell needs to be transduced, as was shown for instance for B7.1, interleukin-12, tumor necrosis factor-\u03b1 and granulocyte\u2013macrophage colony-stimulating factor.1 In addition, many viral vectors may themselves have immunostimulatory properties, which upon infection of the cancer cell may lead to increased antigen presentation and stimulation of an anticancer immune response. However, specific targeting of viral vectors to leukemic cells remains a significant obstacle. In addition, circulating virus-specific antibodies normally act to neutralize virus infections and are thereby likely to counteract viral vector-based therapies. However, in addition to using the common receptor-dependent virus entry mechanisms, some viruses can, under certain \u2013 usually pathological \u2013 conditions, also use antiviral antibodies for their entry into target cells. This mechanism, known as Fc-receptor-mediated antibody-dependent enhancement of viral infection, was proposed by Halstead and Porterfield and colleagues (for review, see Takada and Kawaoka2). The concept is that virus\u2013antibody complexes can bind to cells expressing Fc receptors (such as cells of the immune system, including macrophages, monocytes, B-cells, neutrophils and granulocytes) through interaction between the Fc portion of the antibody and the Fc receptor on the cell surface, providing a bridge that mediates the attachment of viruses to those cells. Several human Fc receptors exist, each exhibiting a different immunoglobulin (Ig) Fc tail specificity. The high affinity receptor Fc\u03b3RI efficiently binds to human IgG1; in addition, it crossreacts with mouse IgG2a.3 Fc\u03b3RI is highly expressed on AML cells of type M4 and M5 morphology (according to the French\u2013American\u2013British classification system) but is absent on pluripotent stem cells and on CD34+ hematopoietic progenitor cells. Thus, the Fc\u03b3RI may represent an appropriate AML target receptor for cytotoxic agents considering that the normal Fc\u03b3RI-positive immune effector cells will repopulate after termination of the Fc\u03b3RI-targeted therapy.",
            "cite_spans": [
                {
                    "start": 567,
                    "end": 568,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1053,
                    "end": 1054,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1933,
                    "end": 1934,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 2523,
                    "end": 2524,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We have shown recently that coronaviruses can be specifically retargeted to selected cells using various types of bispecific adapters including single-chain antibodies.4, 5, 6 This prompted us to explore the use of normal virus-specific antibodies as adapters for targeting viral vectors to Fc\u03b3RI molecules as expressed on cells of certain types of AML. As a model vector, we used a mouse hepatitis coronavirus (MHV) encoding the firefly luciferase (MHV-EFLM).7 This virus normally infects only murine cells through the specific interaction of its spike (S) protein with the cellular receptor murine CEACAM1a. We first checked whether the virus could establish infection of human cells via the Fc\u03b3RI by using a chimeric protein composed of the S-binding N-domain of mCEACAM1a fused to a human IgG1 Fc tail (N-CEACAM-Fc).8 We inoculated THP-1 cells, which are human AML cells lacking mCEACAM1a expression but expressing high amounts of Fc\u03b3RI, with MHV-EFLM at different multiplicities of infection (MOIs) in the absence or presence of different concentrations of N-CEACAM-Fc protein. After 20 h, the cells were lysed and luciferase activity was measured. Figure 1 shows that N-CEACAM-Fc was able to mediate infection of the otherwise refractory THP-1 cells in a manner that depended both on the dose of the virus and on the presence and concentration of the adapter. At higher concentrations, N-CEACAM-Fc becomes increasingly virus neutralizing.",
            "cite_spans": [
                {
                    "start": 168,
                    "end": 169,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 171,
                    "end": 172,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 174,
                    "end": 175,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 460,
                    "end": 461,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 820,
                    "end": 821,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 1154,
                    "end": 1162,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Next, we analyzed a number of anti-MHV monoclonal antibodies (MAbs) for their ability to redirect MHV-EFLM infection to the Fc\u03b3RI-positive THP-1 cells. We used a set of available antibodies with known MHV protein specificity and IgG subclass9 (Figure 2a). Serial dilutions of the different MAbs were mixed with MHV-EFLM and inoculated onto THP-1 cells. At 16 h after inoculation, luciferase expression was measured. Interestingly, of all the antibodies tested only MAb A3.10 was able to efficiently target MHV-EFLM to THP-1 cells; none of the others was effective (Figure 2b).",
            "cite_spans": [
                {
                    "start": 241,
                    "end": 242,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 244,
                    "end": 253,
                    "mention": "Figure 2a",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 565,
                    "end": 574,
                    "mention": "Figure 2b",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The N-CEACAM-Fc fusion protein contains a human IgG1 Fc tail, that of MAb A3.10 is of mouse IgG2a origin. Both Fc tails have been described to bind to human Fc\u03b3RI. To further confirm their binding to this receptor, we analyzed whether the targeted infections mediated by N-CEACAM-Fc and MAb A3.10 could be blocked by preincubating the THP-1 cells with MAb 10.1, an antibody directed against CD64/Fc\u03b3RI. Thus, cells were treated or mock-treated with MAb 10.1 and subsequently inoculated with MHV-EFLM in the presence or absence of MAb A3.10 or N-CEACAM-Fc. They were incubated for 24 h, after which the cells were lysed and luciferase expression was measured. As Figure 3a shows, preincubation of the cells with the anti-Fc\u03b3RI antibody reduced the N-CEACAM-Fc and MAb A3.10-mediated luciferase expression by 100\u20131000 times, indicating that these infections are, solely or primarily, targeted towards the Fc\u03b3RI. To further confirm the Fc\u03b3RI specificity, BHK-21 cells were transfected with an Fc\u03b3RI expression construct or, as a control, without plasmid DNA. The expression of the Fc\u03b3RI significantly enhanced the susceptibility of BHK-21 cells to N-CEACAM-Fc and MAb A3.10-mediated infection (Figure 3b). To determine the efficacy of the Fc\u03b3RI-mediated infections, we determined the number of targeted cells. THP-1 cells were inoculated with MHV strain A59 at an MOI of 10 in the presence or absence of N-CEACAM-Fc or MAb A3.10. After 24 h of incubation, the cells were analyzed for infection by immunofluorescence staining using MHV-specific antibodies. As shown in Figure 4, infected cells (>20%) were observed only in the presence of N-CEACAM-Fc protein or MAb A3.10.",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 662,
                    "end": 671,
                    "mention": "Figure 3a",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 1191,
                    "end": 1200,
                    "mention": "Figure 3b",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 1565,
                    "end": 1573,
                    "mention": "Figure 4",
                    "ref_id": "FIGREF3"
                }
            ]
        },
        {
            "text": "The combined results demonstrate that antibodies can direct a viral vector, in this case MHV-EFLM, specifically to the Fc\u03b3RI on human leukemic cells. The efficiency of targeting obviously depends on the specific features of the antibodies, particularly on their Ig-subclass (human IgG1 or murine IgG2a). How the efficiency depends on the viral epitope recognized by a particular antibody is unclear and will be specific for any given virus. Its importance is illustrated nicely by the anti-MHV MAbs A1.4 and A3.10, which are both of IgG2a subclass and which both recognize a (different) epitope in the S1 part of the MHV S protein, but of which only the latter effects Fc\u03b3RI-mediated MHV infection (Figure 2).",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 699,
                    "end": 707,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "Besides coronaviruses, various other viruses have been described to use Fc receptors as an alternative entry pathway; examples include HIV, HSV-1, poliovirus, yellow fever virus and influenza A virus.8 Therefore, the use of antibodies to target viral vectors to AML cells may also be applicable to other viral vector systems. Clearly, not every MAb will be able to target a particular virus to the Fc\u03b3RI. However, for several of these viruses specific Fc receptor targeting antibodies have already been identified. Compared to other viral vector targeting strategies, the use of IgG antibodies may offer easy of-the-shelf targeting potential for the systemic delivery of these viral vectors. Therefore, further studies on the antibody-mediated targeting of viral vectors to Fc receptors expressed on cells of certain types of AML are warranted.",
            "cite_spans": [
                {
                    "start": 200,
                    "end": 201,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: N-CEACAM-Fc protein mediated targeting of MHV to THP-1 cells. THP-1 cells were inoculated with MHV-EFLM at different MOIs in the absence or presence of various concentrations of N-CEACAM-Fc protein. After 20 h of incubation, the luciferase expression was determined. Shown is the relative luciferase expression, calculated as the increase in luciferase activity relative to inoculations carried out in the absence of N-CEACAM-Fc protein. All data shown represent the average and s.d. of an experiment performed in triplicate.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Antibody-mediated targeting of MHV to THP-1 cells. (a) Features of the MAbs used: viral protein specificity, murine IgG subclass and MHV-EFLM neutralization capacity as determined on murine LR7 cells (+: >50% decrease in luciferase expression relative to inoculations in the absence of antibody). MHV-M, -S1 and -S2 refer to the viral membrane (M) protein and to the S1 and S2 part of the spike (S) protein, respectively. (b) MHV-EFLM (1 \u00d7 105 TCID50 units) was inoculated onto 1 \u00d7 105 human THP-1 cells in the presence of the different anti-MHV MAbs at various dilutions. At 16 h after inoculation, luciferase lysis buffer was added to the cell suspension and total luciferase expression was measured. The data are from a representative experiment.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3: Analysis of antibody-mediated infection specificity. (a) MHV-EFLM was mixed with N-CEACAM-Fc or MAb A3.10 and inoculated onto THP-1 cells that had been preincubated for 30 min at 4\u00b0C in the presence or absence of anti-Fc\u03b3RI MAb 10.1. At 24 h after inoculation, luciferase lysis buffer was added and total luciferase expression measured. The data represent the average luciferase expression of an experiment performed in triplicate. The error bars show the standard deviations. (b) BHK-21 cells were transfected with an Fc\u03b3RI expression plasmid or, as a control, without plasmid DNA. After 48 h, the transfected BHK-21 cells were inoculated with MHV-EFLM mixed either with N-CEACAM-Fc or with MAb A3.10 and incubated further for 24 h, after which the cells were lysed and intracellular luciferase expression measured. The data represent the average luciferase expression of an experiment performed in triplicate. The error bars show the s.d.",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Figure 4: Visualization of the Fc\u03b3RI-mediated MHV infection of THP-1 cells. An amount of 1 \u00d7 106 TCID50 units MHV-A59 was mixed with N-CEACAM-Fc or MAb A3.10 and inoculated onto 1 \u00d7 105 THP-1 cells. After 24 h, infected cells were immunofluorescently stained using MHV-specific antibodies.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Transduction of acute myeloid leukemia cells with third generation self-inactivating lentiviral vectors expressing CD80 and GM-CSF: effects on proliferation, differentiation, and stimulation of allogeneic and autologous anti-leukemia immune responses",
            "authors": [
                {
                    "first": "RC",
                    "middle": [],
                    "last": "Koya",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Kasahara",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Pullarkat",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Levine",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Stripecke",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Leukemia",
            "volume": "16",
            "issn": "",
            "pages": "1645-1654",
            "other_ids": {
                "DOI": [
                    "10.1038/sj.leu.2402582"
                ]
            }
        },
        "BIBREF1": {
            "title": "Antibody-dependent enhancement of viral infection: molecular mechanisms and in vivo implications",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Takada",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Kawaoka",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Rev Med Virol",
            "volume": "13",
            "issn": "",
            "pages": "387-398",
            "other_ids": {
                "DOI": [
                    "10.1002/rmv.405"
                ]
            }
        },
        "BIBREF2": {
            "title": "Role of Fc gamma receptors in cancer and infectious disease",
            "authors": [
                {
                    "first": "PK",
                    "middle": [],
                    "last": "Wallace",
                    "suffix": ""
                },
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Howell",
                    "suffix": ""
                },
                {
                    "first": "MW",
                    "middle": [],
                    "last": "Fanger",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "J Leukoc Biol",
            "volume": "55",
            "issn": "",
            "pages": "816-826",
            "other_ids": {
                "DOI": [
                    "10.1002/jlb.55.6.816"
                ]
            }
        },
        "BIBREF3": {
            "title": "Targeting non-human coronaviruses to human cancer cells using a bispecific single-chain antibody",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "W\u00fcrdinger",
                    "suffix": ""
                },
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Verheije",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Raaben",
                    "suffix": ""
                },
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Bosch",
                    "suffix": ""
                },
                {
                    "first": "CAM",
                    "middle": [],
                    "last": "de Haan",
                    "suffix": ""
                },
                {
                    "first": "VW",
                    "middle": [],
                    "last": "van Beusechem",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Gene Therapy",
            "volume": "12",
            "issn": "",
            "pages": "1394-1404",
            "other_ids": {
                "DOI": [
                    "10.1038/sj.gt.3302535"
                ]
            }
        },
        "BIBREF4": {
            "title": "Soluble receptor-mediated targeting of mouse hepatitis coronavirus to the human epidermal growth factor receptor",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Wurdinger",
                    "suffix": ""
                },
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Verheije",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Broen",
                    "suffix": ""
                },
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Bosch",
                    "suffix": ""
                },
                {
                    "first": "CAM",
                    "middle": [],
                    "last": "de Haan",
                    "suffix": ""
                },
                {
                    "first": "VW",
                    "middle": [],
                    "last": "van Beusechem",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "15314-15322",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.79.24.15314-15322.2005"
                ]
            }
        },
        "BIBREF5": {
            "title": "Redirecting coronavirus to a nonnative receptor through a virus-encoded targeting adapter",
            "authors": [
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Verheije",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Wurdinger",
                    "suffix": ""
                },
                {
                    "first": "VW",
                    "middle": [],
                    "last": "van Beusechem",
                    "suffix": ""
                },
                {
                    "first": "CAM",
                    "middle": [],
                    "last": "de Haan",
                    "suffix": ""
                },
                {
                    "first": "WR",
                    "middle": [],
                    "last": "Gerritsen",
                    "suffix": ""
                },
                {
                    "first": "PJM",
                    "middle": [],
                    "last": "Rottier",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Virol",
            "volume": "80",
            "issn": "",
            "pages": "1250-1260",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.80.3.1250-1260.2006"
                ]
            }
        },
        "BIBREF6": {
            "title": "Coronaviruses as vectors: position dependence of foreign gene expression",
            "authors": [
                {
                    "first": "CAM",
                    "middle": [],
                    "last": "de Haan",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "van Genne",
                    "suffix": ""
                },
                {
                    "first": "JN",
                    "middle": [],
                    "last": "Stoop",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Volders",
                    "suffix": ""
                },
                {
                    "first": "PJM",
                    "middle": [],
                    "last": "Rottier",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Virol",
            "volume": "77",
            "issn": "",
            "pages": "11123-11312",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.77.21.11312-11323.2003"
                ]
            }
        },
        "BIBREF7": {
            "title": "A role for naturally occurring variation of the murine coronavirus spike protein in stabilizing association with the cellular receptor",
            "authors": [
                {
                    "first": "TM",
                    "middle": [],
                    "last": "Gallagher",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "J Virol",
            "volume": "71",
            "issn": "",
            "pages": "3129-3137",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Characterization of the structural proteins of the murine coronavirus strain A59 using monoclonal antibodies",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Gilmore",
                    "suffix": ""
                },
                {
                    "first": "JO",
                    "middle": [],
                    "last": "Fleming",
                    "suffix": ""
                },
                {
                    "first": "SA",
                    "middle": [],
                    "last": "Stohlman",
                    "suffix": ""
                },
                {
                    "first": "LP",
                    "middle": [],
                    "last": "Weiner",
                    "suffix": ""
                }
            ],
            "year": 1987,
            "venue": "Proc Soc Exp Biol Med",
            "volume": "185",
            "issn": "",
            "pages": "177-186",
            "other_ids": {
                "DOI": [
                    "10.3181/00379727-185-42532"
                ]
            }
        }
    }
}