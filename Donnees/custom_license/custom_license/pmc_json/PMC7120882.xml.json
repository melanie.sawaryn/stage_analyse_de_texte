{
    "paper_id": "PMC7120882",
    "metadata": {
        "title": "Construction and Building Applications",
        "authors": [
            {
                "first": "Mohab",
                "middle": [],
                "last": "Anis",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ghada",
                "middle": [],
                "last": "AlTaher",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wesam",
                "middle": [],
                "last": "Sarhan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mona",
                "middle": [],
                "last": "Elsemary",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Construction and building are major contributors to the rising carbon emissions and the global climate change. In addition, the construction industry is a rapidly growing industry. The nature of the construction industry involves the application and use of a diversity of building materials. This leads to nanotechnology being rapidly adopted by the construction market in several aspects like coatings, insulation materials, and building materials (steel, cement, asphalt, glass, polymers, etc.). Nanomaterials are currently employed in cements, steel, and even windows to render buildings greener, more cost effective and safer.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Green construction has become the dominant trend in the construction industry, which means that buildings should meet certain specifications in the materials used, the processes employed, and in the behavior of the residents. These specifications aim at reducing CO2 emissions and reducing the negative impact of the construction industry on the environment. Green architectural design and construction has shifted dramatically since the evolution of nanotechnology. Many people are now seeking healthy lifestyles and environmentally friendly neighborhoods. Green building is currently experiencing a growing demand by governments, architects, and people, especially after many cities have adopted strict regulations regarding green construction. Globally buildings consume around one third of the worldwide electricity [1], with cement alone contributing to the global CO2 emissions by around 5 % [2].",
            "cite_spans": [
                {
                    "start": 821,
                    "end": 822,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 899,
                    "end": 900,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "One way nanotechnology is used in construction is the concept of nanoreinforcements which involves reinforcing the body by adding dispersions that are in the nano-size scale. For example, nanoreinforcements in steel have rendered steel stronger and lighter. Nanoreinforcements in cement have rendered it more durable and cost effective. Another application of nanotechnology in the market involves making Ultra Violet (UV) absorbing, self-cleaning, and depolluting coatings for windows. Moreover, nanotechnology has been rapidly adopted in air and water purification systems. The next generation of nanotechnology applications in construction has begun to involve Building Integrated Photovoltaics (BIPV), which are solar cells that can be integrated within buildings in smart designs without affecting the buildings\u2019 aesthetics, as well as being a clean source of energy and electricity. Even electronics and sensors are being developed to be integrated with buildings. Smart curtains or windows are expected to change the way people are experiencing the way they live (imagine walls that change colors with just a click!) [1].",
            "cite_spans": [
                {
                    "start": 1125,
                    "end": 1126,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "It is important to note that there are still a lot of barriers to the adoption of nanotechnology in building, however, there are certain drivers and enablers that if emphasized and executed will overcome such barriers. Nano-enhanced materials are more expensive than conventional materials. They yield high performances using small amounts of the materials. Thus, it is expected that with the continuous R&D (Research and Development) efforts to improve nanomaterials\u2019 performances at lower costs, the construction market will continue to increasingly adopt the nanomaterials. Proper education of the construction industry about the potential benefits associated with nanotechnology is the starting point towards the rapid adoption of nanotechnology in building materials. Although nano-based construction materials are not economically competitive compared to conventional materials, continuous efforts are undertaken to render them cost effective. In addition, strict regulations have proved to be an important surge to the adoption of nanotech in green construction. One significant example is the US LEED (Leadership in Energy and Environmental Development) which has developed strict minimum standards to simultaneously force and encourage builders to conserve energy and reduce carbon emissions.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "\nHong Kong fights germs in subways.\n",
            "cite_spans": [],
            "section": "Hong Kong Subway ::: Cases",
            "ref_spans": []
        },
        {
            "text": "A Japanese antimicrobial nanocoating that had long lasting efficiency offered maximum protection and elevated hygiene levels in Hong Kong subway stations.\nDrivers\nSARS outbreak. In the spring of 2003, a large outbreak of Severe Acute Respiratory Syndrome (SARS) occurred in Hong Kong and other Asian countries. The contagion rapidly spread throughout the world. Its rapid transmission and high mortality rate made SARS a global threat for which no efficient therapy was available and proactive empirical strategies had to be developed to treat the patients and protect healthy individuals.\nHigh risk zones. Around 2.5 Million commuters use Hong Kong railway and subway stations daily, and they are highly susceptible to dangerous microbes adhering on floors, handrails and buttons.\nMore cost and lower sustainability. Regular disinfection of railway stations uses harsh cleansers, which are expensive and some of which are not environmentally friendly.\nBarriers\nPublic misconception. The US National Institute for Occupational Safety and Health (NIOSH) has classified TiO2 as a \u201cPotential Occupational Carcinogen\u201d, which produces a public misconception regarding its safety\u2014workers need to wear protective clothes while spraying NSTDC, but once dried it is completely safe.\nSilver resistance. There are recent investigations that are addressing issues related to microbes that can develop resistance against silver nanoparticles, which can compromise its long term effectiveness.\nNo international standardized testing. Lack of standardized tests to assess the long term potential environmental impact of nanoparticles (because there are different formulations of different compositions using different synthetic techniques).\nEnablers\nEfficiency on a broad spectrum. NSTDC is certified to be effective in killing a wide range of bacteria, viruses, and mold including the H1N1 influenza virus.\nHuge investment. MTR Corporation attempted to invest 1.5 million dollars in nanotechnology products to enhance the hygiene levels of Hong Kong stations, especially that nanoparticles maximize effectiveness.\nRegulated materials. NSTDC\u2019s main component, titanium dioxide (TiO2), has been approved for use in foods by the FDA and under the Public Health and Municipal Services Ordinance in Hong Kong.\nImpact\nPromising results. Preliminary tests on Hong Kong subway cars coated by NSTDC showed 60 % reduced bacterial infections.\nDurability. The NSTDC nano formula is stable and does not require frequent replacement; therefore it is sprayed every 3 years and is checked regularly every 8 months.\nWider adoption. NSTDC, promising antimicrobial efficiency, makes it suitable to be applied in MTR-managed shopping malls, staff offices and recreational facilities to ensure the highest cleanliness standards for passengers, customers and staff.\nWhat\u2019s Next?MTR plans to monitor the coatings in Hong Kong in attempts to apply them in its two new UK rail franchises\u2014London Rail and West Midlands.\n",
            "cite_spans": [],
            "section": "Hong Kong Subway ::: Cases",
            "ref_spans": []
        },
        {
            "text": "\nPalais Royale: India\u2019s first green super-tall residential building.\n",
            "cite_spans": [],
            "section": "Hycrete ::: Cases",
            "ref_spans": []
        },
        {
            "text": "Hydrophobic concrete plays an important role in the rapidly growing building industry in India.\nDrivers\nRapidly emerging market. The construction boom and the damp environment in India make it an ideal customer for Hycrete. It is worth mentioning that in 2008 alone, around 30 projects made use of Hycrete.\nCost of corrosion. The US spends 276 billion dollars annually on corrosion related problems and the Federal Republic of Germany spends 4 % of its gross national product on corrosion [1].\nLEED motif. Builders and architects all over the world seek to earn LEED credentials. India has become an advanced entrant in the LEED race.\nEnvironmental impact. External protective coatings on concrete are usually petroleum based and thus compromise the ability to recycle concrete. In addition, they release VOCs (Volatile Organic Compounds) into the surrounding environment.\nBarriers\nStill needs to be greener. Hycrete technology does not play too impactful of a role in reducing carbon emissions associated with making concrete.\nVery slow adoption rate. The construction market is somewhat conservative and diffuse, especially in India: high quality construction is not yet fully appreciated on a large scale.\nRisky investment. The construction market in developed countries is well established. Consequently, developing countries are the main target. However, the concepts of green nanotechnology innovations are not easily implemented or understood in most developing countries.\nUncertain feature. Some studies on Hycrete suggested that its compressive strength is reduced by 10\u201320 % yet is still suitable for most construction applications [3].\nEnablers\nIndia is getting rich. The construction boom in India was mainly triggered by the rapid growth in the middle-income class as well as the increasing wealth of the country.\nHigh level testing. Hycrete went through over 10 years of independent and sponsored tests, mostly funded by US Federal Highway administration. All of these tests showed promising results. According to the British Standard Absorption Test BSI 1881-122, Hycrete had less than 1 % water absorption [4].\nGold certificate. Hycrete is the first material certified by Cradle-to-Cradle gold certification because it is easy to recycle and it adds LEED points to projects.\nCost and time saving. The admixture is built within the concrete that is batched at the plant not at the job site. Thus it saves costs of extra containers and materials compared to the case of applying external coatings at the job site.\nImpact\nBusiness boom. The Palais Royale project greatly boosted the Hycrete business. The project required 100,000 gal of Hycrete and the company gained more than one million dollars.\nCost efficiency. The installation process of Hycrete in the \u201cPalais Royale\u201d project was fast and cost effective because the amount of Hycrete needed per square feet was 40 % less than the amount needed for the external waterproofing membrane solutions.\nOperational impact. Implementation of nanostructured admixtures in Hycrete decreases the amount dosed in concrete (1 gal per cubic yard of concrete) reducing the time, cost, and waste of operations [4].\nInternal impact. The application of Hycrete is expected to enhance the longevity and durability of the skyscraper\u2019s infrastructure.\nWhat\u2019s Next?The underdeveloped construction market in India along with Hycrete\u2019s success in Indian projects is driving private equity firms to joint venture with Hycrete, Inc. Hycrete\u2019s achievement in India is setting it as a role model for the emerging construction markets in the Middle East.\n",
            "cite_spans": [
                {
                    "start": 490,
                    "end": 491,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1643,
                    "end": 1644,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 2123,
                    "end": 2124,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 3164,
                    "end": 3165,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Hycrete ::: Cases",
            "ref_spans": []
        },
        {
            "text": "\nBioni paints Dubai Discovery Gardens.\n",
            "cite_spans": [],
            "section": "Bioni, Inc. ::: Cases",
            "ref_spans": []
        },
        {
            "text": "A 40,000 square meter residential community in Dubai \u201cDiscovery Gardens\u201d decided to use the antimicrobial Bioni paints.\nDrivers\nGreener long lasting alternative. Traditional antimicrobial coatings containing biocides have short life spans and slowly diffuse into the environment posing human and environmental risks.\nMetropolitan growth. In Dubai, there is a continuously growing construction activity with buildings of truly immense proportions, which makes it a valuable customer for Bioni antimicrobial paints.\nDesert kingdom. Although Dubai employs high quality construction work, the hot climate and high humidity create an ideal medium for the growth and propagation of mold and microbes inside buildings.\nSBS. The SBS (Sick Building Syndrome) is strongly related to contaminants and pollutants released from indoor sources [1].\nBarriers\nAesthetics and efficiency. The difficulty of maintaining a high enough concentration of nanosilver to efficiently exert an antimicrobial effect yet low enough to prevent yellowish discoloration.\nHigh cost. Although Bioni antimicrobial paints in hospitals are 25 % more cost effective because they decreased the frequent use of biocides, they are more expensive in residential buildings.\nSuperbugs. Even though tests have shown that Bioni antimicrobial paints killed more than 99 % of Staphylococcus aureus on the paint surface, still there were no appropriate field tests carried out to test activity against superbugs (microbes resistant to multiple antibiotics).\nEnablers\nSuccessful research collaboration. Researchers at the Fraunhofer Institute for Manufacturing Engineering and Applied Materials Research in Bremen) and in Bioni CS have spent more than 10 years developing processes for manufacturing the antibacterial nanosilver particles and for incorporating them successfully into paint solutions.\nBroad microbicidal effect. Bioni antimicrobial paints are volatile organic compounds, free of antiallergic, antiviral and antibacterial effects.\nNanosafety. The nanotechnology innovation in the paint ensures nanosafety by trapping the nanoparticles in a polymeric matrix, thus there is no fear of release into the surroundings. Furthermore, the coating is certified by T\u00dcV Rheinland Signet ensuring that it is emission free and does not release any substances into the environment.\nDubai municipality testing. The national building authority of Dubai\u2014the Dubai Municipality\u2014conducted its tests on Bioni products yielding promising results.\nImpact\nMaintain building integrity. The innovative paints proved to be the best fit with building materials because they are resistant to abrasion (wet abrasion resistance class 2) and disinfection chemicals. Additionally, they are not flammable (building material class A2).\nExtra cost efficiency. According to Dubai municipality testing, Bioni\u2019s nanosilver technology has low thermal conductivity and can reflect up to 93 % of the incident light, thus lowering air conditioning bills.\nGrowing market in Dubai. On July 13th 2009, Bioni was officially recorded in the Emirate\u2019s commercial register marking an excellent opportunity for Bioni to expand its activities in the Gulf region.\nWhat\u2019s Next?Although Bioni antimicrobial paints were designed to target medical facilities, Bioni they can find applications in other sensitive buildings such as children\u2019s bedrooms, schools, kindergartens, bathrooms, showers and toilets, the food industry, and retirement homes for the elderly.\n",
            "cite_spans": [
                {
                    "start": 831,
                    "end": 832,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Bioni, Inc. ::: Cases",
            "ref_spans": []
        },
        {
            "text": "\nCan windows save energy?\n",
            "cite_spans": [],
            "section": "Soci\u00e9t\u00e9 G\u00e9n\u00e9rale ::: Cases",
            "ref_spans": []
        },
        {
            "text": "Soci\u00e9t\u00e9 G\u00e9n\u00e9rale bank in Switzerland utilized 3 M nano-based UV absorbing window films to solve the problems of its overheated indoor working environment.\nDrivers\nNowadays\u2019 impractical alternatives. Conventional reflective films are either metal based, which interfere with electronic signals and corrode in coastal regions changing the window color, or dye-based, which fade over time.\nSustainability. Energy costs are increasing, and there is a great need for sustainable energy efficient solutions especially from the building sector which is one of the main detrimental influences on the environment.\nPromising technology. The US Department of Energy (DOE) conducted a study on the top 50 commercially available energy conservation technologies. The results show that window films were ranked as a top tier technology with the highest probability of success and the fastest Return on Investment (ROI).\nBarriers\nLandmark status. Soci\u00e9t\u00e9 G\u00e9n\u00e9rale bank is based in a building that is considered to be Zurich\u2019s very first high rise building. Therefore, any solution to the bank\u2019s overheated indoor environment shouldn\u2019t interfere with the historical building\u2019s external features and aesthetics.\nMarket coverage. UV absorbing window films are not one fit for all types of windows or buildings, thus they are not equally effective in all cases.\nSkeptical customer. Although the building industry serves a huge market, it is very hesitant in adopting new and uncertain technologies.\nSkeptical consumer. Casual residents may underestimate the cost efficiency associated with installing UV absorbing window films until they see real cost savings.\nEnablers\nNANO. 3 M window films are made of nanoscaled ceramics. At this scale, materials are invisible, therefore, the films keep windows clear and less reflective compared to glass.\nFirst patency. 3 M is the first company to earn the world\u2019s first patency in window films over four decades ago. Since then, the company has been producing reliable solutions to lower customers\u2019 energy costs and to provide an accelerated return on investment.\nMarketing strategy. 3 M supports its window films with a 15 year commercial warranty, encouraging its rapid commercialization and adoption.\nLEED. 3 M window films are components of LEED sustainable design where buildings can earn 2\u201311 points [6].\nImpact\nIndoor environment. Up to 99 % of total UV energy and more than 80 % of the heat producing IR energy were rejected. The bank had better and sustainable indoor temperature control. Furthermore, fabrics and furnishings were protected against UV induced damage.\nCost effectiveness. Soci\u00e9t\u00e9 G\u00e9n\u00e9rale bank enjoyed a unique and green approach to reduce energy consumption with a valuable return on investment.\nMission accomplished. The bank managed to improve the indoor working environments without compromising the specific historic and aesthetic features of the old building.\nGreen impact. The building industry is a top contributor to worldwide carbon emissions. Building insulations could save around 42 % of energy consumption and consequently reduce carbon emissions [1].\nWhat\u2019s Next?The growing regulations on the building sector to save energy drive the growth for solar control window films, which is forecast to be an 863 million dollars market by 2018. First technology providers like 3 M are likely to be market leaders, and early adopters in the construction market will enjoy a unique position [7].\n",
            "cite_spans": [
                {
                    "start": 2329,
                    "end": 2330,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 3109,
                    "end": 3110,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 3444,
                    "end": 3445,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Soci\u00e9t\u00e9 G\u00e9n\u00e9rale ::: Cases",
            "ref_spans": []
        },
        {
            "text": "\nCleaning using forces of nature.\n",
            "cite_spans": [],
            "section": "Pilkington Active Glass ::: Cases",
            "ref_spans": []
        },
        {
            "text": "An innovative glass makes use of rain and sun to self-clean and resist dirt.\nDrivers\nEnvironment friendliness. Pilkington self-cleaning windows eliminate the need for noxious chemicals that are washed off into the environment after the cleaning process.\nConsumer convenience. Self-cleaning windows are forecasted to be more rapidly adopted in construction compared to other types of smart windows: consumers can rapidly see the cost efficiency associated with the decreased frequency of cleaning windows compared to that associated with energy savings in other types of smart windows.\nEmerging market. The market for self-cleaning windows is not yet mature, with few players, thus, there is a great opportunity for growth and expansion for early entrants.\nBarriers\nRegional limitation. The photocatalytic material used will not function properly in the presence of inorganic dirt, long spells, coastal areas and internal windows because it needs daylight.\nLack of standards. Since self-cleaning technologies in windows are new features and no quality standards are there to assess their efficiency, a consumer\u2019s decision will be reliant only on the manufacturer\u2019s claims.\nHigh cost. Pilkington self-cleaning windows are 15\u201320 % more expensive than conventional glass, which can negatively influence the buyer\u2019s purchasing decision.\nMarket upset. Glass cleaning companies are an important component of the glass windows \u201cvalue chain\u201d. Thus, the introduction of self-cleaning windows will affect their businesses negatively.\nEnablers\nR&D. Pilkington\u2019s expertise through 5\u20137 years of extensive R&D on thin film technologies enabled it to successfully transform them from laboratory samples to the market.\nSafe material. Titanium dioxide is publicly and governmentally accepted because it has been widely used in food related materials, cosmetics, and toothpastes.\nImpact\nAttraction of unique customers. Saint Pancras Station in the United Kingdom is one of the customers that has applied the self-cleaning glass. It installed the glass on its roof and building structure to reduce maintenance costs.\nTechnology appreciation. Pilkington was one of the four finalists for The Royal Academy of Engineering\u2019s MacRobert prize that rewards technological and engineering innovation.\nNew added benefit. Pilkington photocatalytic effect could naturally also depollute the surrounding air by decomposing some pollutants such as formaldehyde and ground level ozone.\nWhat\u2019s Next?Pilkington is working with other glass manufacturers, universities, and standards\u2019 institutes to develop standards that assess and measure the self-cleaning properties of nanocoatings in an attempt to increase the public trust (so as not to be only reliant on the manufacturer claims) and the commercial adoption of these products. Moreover, self-cleaning glass can be integrated with solar panels to reduce their frequent cleaning costs, and to make use of the same solar radiations.\n",
            "cite_spans": [],
            "section": "Pilkington Active Glass ::: Cases",
            "ref_spans": []
        },
        {
            "text": "\nInvisible shutter windows.\n",
            "cite_spans": [],
            "section": "Lumotone, Inc. NanoShutters ::: Cases",
            "ref_spans": []
        },
        {
            "text": "Lumotone invented new transparent shutter films that are stuck on window made opaque by the click of a button.",
            "cite_spans": [],
            "section": "Lumotone, Inc. NanoShutters ::: Cases",
            "ref_spans": []
        },
        {
            "text": "\n\n\nDrivers\nThe need for privacy. Window blinds are important house amenities to prevent people from prying into other people\u2019s lives.\nNew d\u00e9cor trends. The new futuristic d\u00e9cor trends constitute less furnishings, wide spaces and invisible blinds instead of curtains that can be controlled by the press of a button.\nThe need to save energy. By controlling the amount of light entering the room, you can ensure the room stays cool in the summer and warm in the winter thereby reducing the energy needed to maintain a room\u2019s temperature.\nThe need for automation. Since the advent of remote controls, computers and mobile phones people have come to rely more and more on automation. This is further increased by the modern fast pace of life. Numerous people nowadays install home automation systems to make their life more comfortable and easy.\nUV blockade. NanoShutters block 99 % of UV light entering through the glass. This in turn is healthier as well as crucial to preventing the fading of colors of the interior d\u00e9cor and fabrics of furnishings inside the house.\nBarriers\nInstallation. The difficulty of installation of the NanoShutter will definitely affect its success. However, Lumotone, Inc. has made the NanoShutters easy to install\u2014as easy as putting up a poster on the wall. They also provide certified professionals who can install them for their customers.\nAutomation. How to make these smart windows automated and be controlled by your mobile device remotely when you are away from home.\nDurability. The durability of the NanoShutter film and whether it can withstand cleaning by household solvents is a limitation. This explains why such films are protected by an upper layer to prevent them from being scratched or destroyed. In addition, the company provides directions as how to clean such smart windows; which solvents are allowed or not allowed.\nEnablers\nResearch. The founders of the company were originally researchers and engineers in the field of nanotechnology in the University of Waterloo, Canada, giving them credible backgrounds to design such products.\nNanoShutters that can be integrated with home automation systems. Lumotone developed their nanoshutters to be compatible with any home automation system.\nMobile applications for the NanoShutters. Lumotone has also designed mobile applications and computer software that enable the users to control their shutters in or away from their homes.\nAuto-scheduled NanoShutters. Lumotone has made its shutters to be schedulable by computer or mobile application so a user can sleep in shaded windows and wake up in bright sunlight early in the morning to go to work.\nImpactThe efficiency of NanoShutters made them invade other market sectors including automotive and aircrafts. These smart windows help to control the inside cabin temperature as well as prevent screen glare when watching videos.\nWhat\u2019s Next?Lumotone is further developing its technology to produce pixels and transparent electronic displays.\n",
            "cite_spans": [],
            "section": "Lumotone, Inc. NanoShutters ::: Cases",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 2.1: Polymer dispersed liquid crystals used in NanoShutters",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}