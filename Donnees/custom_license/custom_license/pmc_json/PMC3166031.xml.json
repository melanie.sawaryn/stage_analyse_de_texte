{
    "paper_id": "PMC3166031",
    "metadata": {
        "title": "Tuberculosis among Foreign-born Persons, Singapore, 2000\u20132009",
        "authors": [
            {
                "first": "Khin",
                "middle": [
                    "Mar",
                    "Kyi"
                ],
                "last": "Win",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Cynthia",
                "middle": [
                    "B.E."
                ],
                "last": "Chee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Liang",
                "middle": [],
                "last": "Shen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yee",
                "middle": [
                    "T."
                ],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jeffery",
                "middle": [],
                "last": "Cutter",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Notification of TB cases to the Singapore TB Elimination Programme (STEP) Registry is mandated by law. Since 1998, the notification report has included disease characteristics, sociodemographic information, country of origin, immigration status, and year of arrival in Singapore of TB patients (10). All notifications during 2000\u20132009 were obtained from the STEP Registry database. When data on country of birth were not available for persons not born in Singapore (0.5% of case-patients), the nationality of these persons was assumed to represent their country of birth. Missing information about country of birth for persons with a Singaporean nationality (17%) and arrival date (6.5%) were matched against a database provided by the Ministry of Home Affairs.",
            "cite_spans": [
                {
                    "start": 295,
                    "end": 297,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Persons not born in Singapore were considered foreign born. Foreign-born case-patients comprised naturalized citizens, PRs, and LTPHs. Singapore issues long-term passes to skilled and unskilled workers and others (students and foreign-born family members of citizens or PRs) to enable a stay >6 months in Singapore. Persons applying for permanent residency or long-term stay and renewal of long-term passes are given medical examinations that include a general physical examination, chest radiograph, and testing for HIV (11). The Department of Singapore Statistics provided 2000\u20132010 population data. We estimated the number of persons born in Singapore during 2000\u20132010 by using linear interpolation based on 2000 and 2010 population census exercises. Consistent with the country classification used in the Singapore 2000 population census, People\u2019s Republic of China, Hong Kong, and Taiwan were classified as 1 group of country of birth.",
            "cite_spans": [
                {
                    "start": 522,
                    "end": 524,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "There were 23,164 cases of TB (new and re-treated) reported to the registry during 2000\u20132009. After we excluded 4,033 short-term pass holders, persons whose TB diagnosis had changed (n = 97), and persons with missing information about country of origin (n = 34), a total of 19,000 persons were eligible for the study. Of these persons, 13,048 (68.7%) were born in Singapore and 5,952 (31.3%) were foreign born. The number and proportion of foreign-born cases decreased in the first half of the study period from 675 (33.6%) in 2000 to 444 (25.5%) in 2004. This trend reversed during 2005, and the number and proportion of foreign-born persons with TB increased to 788 (37.6%) in 2009 (Figure 1). Of foreign-born persons with TB, 3,386 (56.9%) were LTPHs, 1,820 (30.6%) were PRs, and 746 (12.5%) were naturalized citizens.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 685,
                    "end": 693,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "LTPHs with TB comprised 2,562 (75.7%) unskilled workers, 371 (10.9%) professional and skilled workers, and 162 (4.8%) students. The remaining 291(8.6%) persons were foreign-born family members of Singapore citizens or PRs who were granted long-term stay in Singapore. TB among LTPHs increased from 220 (49.5%) cases in 2004 to 532 (67.5%) in 2009 and that among PRs increased from 45 (6.7%) in 2000 to 124 (15.7%); cases among foreign-born citizens decreased from 342 (50.7%) in 2000 to 132 (16.8%) in 2009 (Figure 2, panel A).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 508,
                    "end": 516,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The number of persons with TB from Indonesia, the Philippines, and Myanmar increased from 2000\u20132004 to 2005\u20132009 (Figure 2, panel B). Among native-born persons, TB occurred predominantly in men >40 years of age (Table). TB in foreign-born persons was distributed equally in both sexes and occurred mostly in persons 20\u201339 years of age (Table). Drug resistance and extrapulmonary involvement were higher among foreign-born persons than among native-born persons.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 114,
                    "end": 122,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 212,
                    "end": 217,
                    "mention": "Table",
                    "ref_id": null
                },
                {
                    "start": 336,
                    "end": 341,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The decreasing trend of foreign-born persons with TB in the first half of the study period could be attributed to fewer transient migrant workers entering Singapore during the economic crisis in Asia and the severe acute respiratory syndrome outbreak in 2003. After economic recovery and liberalization of the immigration policy in Singapore in 2005, there was an influx of migrant workers and immigrants from countries with high incidences of TB and a corresponding increase in TB notifications among this population. Of these persons with TB, >75% came from 5 of the 7 countries (India, China, Indonesia, Bangladesh, and the Philippines) with highest incidences of TB (12,13).",
            "cite_spans": [
                {
                    "start": 671,
                    "end": 673,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 674,
                    "end": 676,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "The large increase in the number of TB case-patients from Indonesia, the Philippines, and Myanmar reflects the influx of migrant workers from these countries. This finding is consistent with the predominance of cases reported in foreign-born persons 20\u201339 years of age. The lower incidence of diabetes among these younger persons than in native-born persons is not surprising. The preponderance of lymph node involvement among the foreign-born persons merits further study.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "The estimated TB rate in persons born in Singapore decreased from 49.7 cases/100,000 population to 41.8/100,000 during 2000\u20132007 but increased to 46.5/100,000 in 2008 and 45.2/100,000 in 2009. This increase followed the increase in cases among foreign-born persons in 2005. Transmission from foreign-born to native-born persons may have occurred, especially in increasingly crowded urban settings of Singapore. However, this proposal cannot be verified without DNA fingerprinting studies.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Currently, persons with untreated inactive lesions or scarring on their screening chest radiographs are given permission for a long-term stay in Singapore without further examination or follow-up. To improve screening, mandating sputum TB cultures may be prudent for these persons. If active disease is excluded, these persons can then receive prophylactic treatment.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "We do not favor a policy of screening for latent TB infection because of the high probability of false-positive tuberculin skin test results in persons vaccinated with Mycobacterium bovis BCG and the unfavorable risk-benefit ratio of preventive therapy for persons from countries with high TB incidences, who may be more likely to have acquired latent TB in the past than recently (14). Although interferon-\u03b3 release assays will overcome the problem of false-positive tuberculin skin test results in persons vaccinated with M. bovis BCG, the cost of these assays is prohibitively high, and their positive predictive value for progression to active TB remains to be determined (15).",
            "cite_spans": [
                {
                    "start": 382,
                    "end": 384,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 677,
                    "end": 679,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "A strength of this study was its nationally representative data of all TB cases reported to the STEP Registry. Electronic linkage of the Registry to the 2 mycobacterial culture laboratories in Singapore also enabled inclusion of all bacteriologically positive cases. A limitation of this study was the unavailability of data for native-born and foreign-born persons by sex and age to compare age- and sex-specific TB case rates between these 2 groups. In addition, because of the lack of population data for country of birth, we were unable to determine the TB rate by country of origin. Also, information bias is inherent in retrospective studies, and most country of birth data were based on information submitted by the notifying physician. We were also unable to analyze the association of HIV and TB because access to HIV status in our study population was limited. Our study highlights the need for measures to address TB among foreign-born persons in Singapore.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Tuberculosis (TB) cases and proportion of native-born versus foreign-born persons, Singapore, 2000\u20132009. Numbers along data lines indicate percentage of native-born persons with TB versus foreign-born persons with TB.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Tuberculosis (TB) cases, Singapore, 2000\u20132009. A) No. notified cases among foreign-born subgroups, by year of notification. Citizens, naturalized citizens; PRs, permanent residents; LTPHs, long-term pass holders. B) No. notified cases by country of origin.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "The Singapore Tuberculosis Elimination Programme: the first five years.",
            "authors": [],
            "year": 2003,
            "venue": "Bull World Health Organ",
            "volume": "81",
            "issn": "",
            "pages": "217-21",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "WHO Global Surveillance and Monitoring Project. Global burden of tuberculosis: estimated incidence, prevalence and mortality by country.",
            "authors": [],
            "year": 1999,
            "venue": "JAMA",
            "volume": "282",
            "issn": "",
            "pages": "677-86",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.282.7.677"
                ]
            }
        },
        "BIBREF4": {
            "title": "Rethinking the socioeconomics and geography of tuberculosis among foreign-born residents of New Jersey, 1994\u20131999.",
            "authors": [],
            "year": 2003,
            "venue": "Am J Public Health",
            "volume": "93",
            "issn": "",
            "pages": "1007-12",
            "other_ids": {
                "DOI": [
                    "10.2105/AJPH.93.6.1007"
                ]
            }
        },
        "BIBREF5": {
            "title": "Screening of immigrants to Canada for tuberculosis: chest radiography or tuberculin skin testing?",
            "authors": [],
            "year": 2003,
            "venue": "CMAJ",
            "volume": "169",
            "issn": "",
            "pages": "1035-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Updated guidelines for using interferon gamma release assays to detect Mycobacterium tuberculosis infection\u2014United States, 2010.",
            "authors": [],
            "year": 2010,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "59",
            "issn": "",
            "pages": "1-25",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "The epidemiology of tuberculosis among foreign-born persons in the United States, 1986 to 1993.",
            "authors": [],
            "year": 1995,
            "venue": "N Engl J Med",
            "volume": "332",
            "issn": "",
            "pages": "1071-6",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJM199504203321606"
                ]
            }
        },
        "BIBREF8": {
            "title": "Tuberculosis among foreign-born persons in the United States, 1993\u20131998.",
            "authors": [],
            "year": 2000,
            "venue": "JAMA",
            "volume": "284",
            "issn": "",
            "pages": "2894-900",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.284.22.2894"
                ]
            }
        },
        "BIBREF9": {
            "title": "Migrants and tuberculosis: analysing epidemiological data with ethnography.",
            "authors": [],
            "year": 2008,
            "venue": "Aust N Z J Public Health",
            "volume": "32",
            "issn": "",
            "pages": "142-9",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1753-6405.2008.00191.x"
                ]
            }
        },
        "BIBREF10": {
            "title": "Persistent high incidence of tuberculosis in immigrants in a low-incidence country.",
            "authors": [],
            "year": 2002,
            "venue": "Emerg Infect Dis",
            "volume": "8",
            "issn": "",
            "pages": "679-84",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Epidemiology and clinical features of tuberculosis in immigrants at an infectious diseases department in Madrid.",
            "authors": [],
            "year": 2007,
            "venue": "Int J Tuberc Lung Dis",
            "volume": "11",
            "issn": "",
            "pages": "769-74",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Tuberculosis among foreign-born persons in United States: achieving tuberculosis elimination.",
            "authors": [],
            "year": 2007,
            "venue": "Am J Respir Crit Care Med",
            "volume": "175",
            "issn": "",
            "pages": "75-9",
            "other_ids": {
                "DOI": [
                    "10.1164/rccm.200608-1178OC"
                ]
            }
        },
        "BIBREF13": {
            "title": "The epidemiology of tuberculosis in Ottawa, Canada, 1995\u20132004.",
            "authors": [],
            "year": 2008,
            "venue": "Int J Tuberc Lung Dis",
            "volume": "12",
            "issn": "",
            "pages": "1128-33",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Tuberculosis among foreign-born persons in the United States.",
            "authors": [],
            "year": 2008,
            "venue": "JAMA",
            "volume": "300",
            "issn": "",
            "pages": "405-12",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.300.4.405"
                ]
            }
        }
    }
}