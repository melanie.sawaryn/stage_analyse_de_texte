{
    "paper_id": "PMC7109661",
    "metadata": {
        "title": "Airborne Severe Acute Respiratory Syndrome Coronavirus and Its Implications",
        "authors": [
            {
                "first": "Tommy",
                "middle": [
                    "R."
                ],
                "last": "Tong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Airborne transmission of the severe acute respiratory syndrome (SARS) coronavirus (CoV) has been the favored explanation for its transmission on an aircraft [1] and appeared to explain a large community outbreak of SARS in the Amoy Gardens in Hong Kong [2]. The article by Booth et al. in this issue of the Journal of Infectious Diseases [3] suggests that airborne dissemination of SARS-CoV may also occur in the health-care setting. A patient with SARS who was breathing quietly but coughing occasionally in a hospital room contaminated the surrounding air with SARS-CoV, as shown by experiments conducted during the SARS outbreak in Canada in early 2003",
            "cite_spans": [
                {
                    "start": 158,
                    "end": 159,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 254,
                    "end": 255,
                    "mention": "2",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 339,
                    "end": 340,
                    "mention": "3",
                    "ref_id": "BIBREF12"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Several viruses and other pathogens, such as Mycobacterium tuberculosis have been shown to be transmitted by airborne dissemination [4\u20138]. However, the possibility of airborne dissemination of SARS-CoV has been controversial. The important work by Booth et al. has shown beyond doubt that SARS-CoV aerosol generation can occur from a patient with SARS. The study was well conceived and designed and employed nucleic acid amplification and state-of-the-art air slit-sampling technology. To ensure the accuracy of their results, the authors followed stringent control measures in their studies. For example, empty specimen containers made the same trip from outside to the hospital ward and then to the laboratory, in the same way as the real specimen containers. All samples were tested similarly, and the technologists were blinded to their true nature. These procedures helped to control for possible contamination of the outside of the specimen containers, a little-thought-of possible cause of false-positive test results. Another, even more stringent measure was the dedication of special rooms for these experiments. These researchers anticipated laboratory contamination as a possible cause of false-positive results long before news broke of SARS-CoV escaping microbiology laboratories through infection of workers [9, 10]. Other measures, such as use of dummy controls (with water only), confirming the identity of SARS-CoV by testing more than one region of the viral genome, and sequencing the amplified products, add to the credibility of their results. The authors use their findings to make several valid recommendations regarding proper ventilation, air filtration, and aerosol prevention",
            "cite_spans": [
                {
                    "start": 133,
                    "end": 136,
                    "mention": "4\u20138",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 1323,
                    "end": 1324,
                    "mention": "9",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 1326,
                    "end": 1328,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Because none of the SARS-CoV cultures were found to be positive and host infection was not involved, the authors rightly avoided drawing a conclusion of airborne transmission of SARS-CoV. Definitive proof of transmission will need to come from experiments similar to those performed by Riley et al. in the 1950s, which involved exposure of guinea pigs to air shared by patients with active pulmonary tuberculosis [11]. In vitro viral culture tests may not be sensitive enough for this purpose. However, if SARS-CoV is naturally airborne (produced by breathing and coughing), as was shown by Booth et al., then there is sufficient concern that it can be transmitted successfully by air. A number of factors may affect the ability of a virus to establish infection after successful transmission. The lack of proofreading [12, 13] during SARS-CoV replication suggests that some assembled viral genomes are defective and not packaged within viral capsids to form infectious viral particles. Viability may also be compromised, even for nondefective viral particles, after release into the environment. Considerable airborne viral dilution may also occur, adding another challenge to a pathogen that employs air for dissemination. Finally, the number of viral particles needed to cause an infection differs among viral pathogens, with influenza virus requiring as few as 3 particles to cause infection [14]. It is not clear how many SARS-CoV particles are required to cause infection",
            "cite_spans": [
                {
                    "start": 414,
                    "end": 416,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 820,
                    "end": 822,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 824,
                    "end": 826,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1397,
                    "end": 1399,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Circumstances limited the Booth et al. study\u2014the few confirmed SARS cases were scattered over 4 Toronto hospitals, and hardware appeared to be limited, as reflected by the small numbers of slit samples (all collected in hospital Z). Air in several rooms was sampled with polytetrafluoroethylene filters only and yielded negative results\u20142 rooms had intervals between air sampling and onset of disease similar to that in the room that tested positive by use of slit-sampling technology. Had Booth et al. not employed the newer slit-sampling technology in harvesting virus from the air, their results would likely have been falsely negative. The small number of positive results must not beget complacency. Recent work showing that certain individuals produce larger numbers of exhaled particles during breathing than do other individuals might help to explain \u201csuperspreading events\u201d during the SARS outbreaks, further underscoring the importance of this research [15]",
            "cite_spans": [
                {
                    "start": 964,
                    "end": 966,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The other important part of the work by Booth et al. concerns experimental proof of SARS-CoV contamination of fomites. Their detection of the virus on frequently touched surfaces, including a bed table, a television remote control, and even a medication refrigerator at a nurses\u2019 station, emphasizes the need for even stricter infection control precautions than are usually applied. As the authors point out, electronic equipment, because of its moisture sensitivity, may need particular attention",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "This work by Booth et al. can be looked at from multiple perspectives. The first is from that of patients: the study\u2019s results justify their concern about health-care facilities as places in which infectious organisms may be encountered. However, with knowledge of transmission mechanisms should come a better understanding of how to prevent transmission. Improving the indoor air quality of health-care facilities, including not just isolation wards but also common areas, will help to prevent the notion of them being potential \u201ccenters of contagion.\u201d",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The second perspective follows from the first\u2014namely, that of the caregivers, clinical microbiologists, and health-care policy makers. Acknowledgment of the fact that SARS-CoV can be aerosolized justifies the actions of those who have already committed resources for providing a safer environment in terms of preventing airborne transmission of infectious diseases and might provide the needed pressure for others to follow suit. Public health officials will also be more likely to recommend \u201csmart\u201d quarantine [16] and to provide point-of-care diagnostics. Avoiding crowding in the clinic is important in the prevention of nosocomial transmission of any infectious diseases, especially those spread by air",
            "cite_spans": [
                {
                    "start": 512,
                    "end": 514,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Engineers and architects interested in designing safer institutional and other public environments should read the article by Booth et al. with interest and be provided with additional momentum to advance novel concepts [17\u201319]. Architectural advances in the design of safer hospital facilities, particularly isolation rooms for patients with airborne communicable diseases, are needed. Hopefully, the work of Booth et al. will spur these efforts",
            "cite_spans": [
                {
                    "start": 221,
                    "end": 226,
                    "mention": "17\u201319",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Transmission of the severe acute respiratory syndrome on aircraft",
            "authors": [
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "Olsen",
                    "suffix": ""
                },
                {
                    "first": "HL",
                    "middle": [],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "TY",
                    "middle": [],
                    "last": "Cheung",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": "2416-22",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "Western Pacific Region. Summary of China\u2019s investigation into the April outbreak",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Aerial dissemination of pulmonary tuberculosis: a two-year study of contagion in a tuberculosis ward, 1959",
            "authors": [
                {
                    "first": "RL",
                    "middle": [],
                    "last": "Riley",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Mills",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Nyka",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Am J Epidemiol",
            "volume": "142",
            "issn": "",
            "pages": "3-14",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Lack of evidence for proofreading mechanisms associated with an RNA virus polymerase",
            "authors": [
                {
                    "first": "DA",
                    "middle": [],
                    "last": "Steinhauer",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Domingo",
                    "suffix": ""
                },
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "Holland",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "Gene",
            "volume": "122",
            "issn": "",
            "pages": "281-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "The population genetics and evolutionary epidemiology of RNA viruses",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Moya",
                    "suffix": ""
                },
                {
                    "first": "EC",
                    "middle": [],
                    "last": "Holmes",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Gonzalez-Candelas",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat Rev Microbiol",
            "volume": "2",
            "issn": "",
            "pages": "279-88",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "How contagious are common respiratory tract infections",
            "authors": [
                {
                    "first": "JD",
                    "middle": [],
                    "last": "Hartzell",
                    "suffix": ""
                },
                {
                    "first": "CN",
                    "middle": [],
                    "last": "Oster",
                    "suffix": ""
                },
                {
                    "first": "JC",
                    "middle": [],
                    "last": "Gaydos",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Inhaling to mitigate exhaled bioaerosols",
            "authors": [
                {
                    "first": "DA",
                    "middle": [],
                    "last": "Edwards",
                    "suffix": ""
                },
                {
                    "first": "JC",
                    "middle": [],
                    "last": "Man",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Brand",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Proc Natl Acad Sci USA",
            "volume": "101",
            "issn": "",
            "pages": "17383-88",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Efficiency of quarantine during an epidemic of severe acute respiratory syndrome\u2014Beijing, China",
            "authors": [],
            "year": 2003,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "52",
            "issn": "",
            "pages": "1037-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Ventilation of wards and nosocomial outbreak of severe acute respiratory syndrome among healthcare workers",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Jiang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Chin Med J",
            "volume": "116",
            "issn": "",
            "pages": "1293-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Hospital preparedness and SARS",
            "authors": [
                {
                    "first": "MR",
                    "middle": [],
                    "last": "Loutfy",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Wallington",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Rutledge",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "771-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Severe acute respiratory syndrome and biology, air quality, physics, and mechanical engineering",
            "authors": [
                {
                    "first": "CY",
                    "middle": [],
                    "last": "Wong",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Hong Kong Med J",
            "volume": "9",
            "issn": "",
            "pages": "304-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Evidence of airborne transmission of the severe acute respiratory syndrome virus",
            "authors": [
                {
                    "first": "IT",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "TW",
                    "middle": [],
                    "last": "Wong",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "N Engl J Med",
            "volume": "350",
            "issn": "",
            "pages": "1731-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Detection of airborne severe acute respiratory system (SARS) coronavirus and environmental contamination in SARS outbreak units",
            "authors": [
                {
                    "first": "TF",
                    "middle": [],
                    "last": "Booth",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Kournikakis",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Bastien",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "191",
            "issn": "",
            "pages": "1472-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Airborne transmission of communicable infection\u2014the elusive pathway",
            "authors": [
                {
                    "first": "CJ",
                    "middle": [],
                    "last": "Roy",
                    "suffix": ""
                },
                {
                    "first": "DK",
                    "middle": [],
                    "last": "Milton",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "N Engl J Med",
            "volume": "350",
            "issn": "",
            "pages": "1710-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Detection of varicella-zoster virus DNA in air samples from hospital rooms",
            "authors": [
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Sawyer",
                    "suffix": ""
                },
                {
                    "first": "CJ",
                    "middle": [],
                    "last": "Chamberlin",
                    "suffix": ""
                },
                {
                    "first": "YN",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Aintablian",
                    "suffix": ""
                },
                {
                    "first": "MR",
                    "middle": [],
                    "last": "Wallace",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "J Infect Dis",
            "volume": "169",
            "issn": "",
            "pages": "91-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "A school outbreak of Norwalk-like virus: evidence for airborne transmission",
            "authors": [
                {
                    "first": "PJ",
                    "middle": [],
                    "last": "Marks",
                    "suffix": ""
                },
                {
                    "first": "IB",
                    "middle": [],
                    "last": "Vipond",
                    "suffix": ""
                },
                {
                    "first": "FM",
                    "middle": [],
                    "last": "Regan",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Wedgwood",
                    "suffix": ""
                },
                {
                    "first": "RE",
                    "middle": [],
                    "last": "Fey",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Epidemiol Infect",
            "volume": "131",
            "issn": "",
            "pages": "727-36",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Airborne dispersal as a novel transmission route of coagulase-negative staphylococci: interaction between coagulase-negative staphylococci and rhinovirus infection",
            "authors": [
                {
                    "first": "WE",
                    "middle": [],
                    "last": "Bischoff",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Bassetti",
                    "suffix": ""
                },
                {
                    "first": "BA",
                    "middle": [],
                    "last": "Bassetti-Wyss",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Infect Control Hosp Epidemiol",
            "volume": "25",
            "issn": "",
            "pages": "504-11",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "Experimental airborne transmission of PRRS virus",
            "authors": [
                {
                    "first": "CS",
                    "middle": [],
                    "last": "Kristensen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Botner",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Takai",
                    "suffix": ""
                },
                {
                    "first": "JP",
                    "middle": [],
                    "last": "Nielsen",
                    "suffix": ""
                },
                {
                    "first": "SE",
                    "middle": [],
                    "last": "Jorsal",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Vet Microbiol",
            "volume": "99",
            "issn": "",
            "pages": "197-202",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "Laboratory-acquired SARS raises worries on biosafety",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Orellana",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Lancet Infect Dis",
            "volume": "4",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}