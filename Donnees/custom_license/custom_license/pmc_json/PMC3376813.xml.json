{
    "paper_id": "PMC3376813",
    "metadata": {
        "title": "Spike Protein Fusion Peptide and Feline Coronavirus Virulence",
        "authors": [
            {
                "first": "Hui-Wen",
                "middle": [],
                "last": "Chang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Herman",
                "middle": [
                    "F."
                ],
                "last": "Egberink",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rebecca",
                "middle": [],
                "last": "Halpin",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [
                    "J."
                ],
                "last": "Spiro",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peter",
                "middle": [
                    "J.M."
                ],
                "last": "Rottier",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "FECV strain RM and FECV strain UCD (FECV UU2) were propagated in specific pathogen\u2013free cats. FIPV UU3 was obtained from a lymph node of a cat infected with FECV UCD; the presence of feline infectious peritonitis in the cat was pathologically confirmed. During 2006\u20132011, with the assistance of veterinarians in the Netherlands, we randomly obtained field cats with suspected feline infectious peritonitis and feces samples from apparently healthy cats from all geographic areas of the Netherlands; we did not use any selection criteria, such as age, sex, or breed of cat. Cats originated from 144 different catteries and single- and multicat households. Cats were pathologically diagnosed with feline infectious peritonitis by postmortem examination at the Veterinary Pathology Department, Utrecht University; findings confirmed that the cats had feline infectious peritonitis. Ascites samples and lesions from affected organs were obtained for RNA isolation. Fecal material from apparently healthy cats was obtained from the rectum by using a cotton swab.",
            "cite_spans": [],
            "section": "Viruses and Clinical Specimens ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "We suspended fecal specimens to a final concentration of 10% (wt/vol) in phosphate-buffered saline by vigorously vortexing the specimens. The supernatant was cleared (centrifugation for 10 min at 3,000 \u00d7 g) and then used for RNA extraction. Following the manufacturer\u2019s protocols, we used the QIAamp Viral RNA Mini Kit (QIAGEN, Valencia, CA, USA) to extract viral RNA from 140 \u03bcL of fecal supernatants or ascites and the QIAamp RNeasy Mini Kit to extract viral RNA from 30 mg of organ tissue homogenate.",
            "cite_spans": [],
            "section": "RNA Preparation ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "We tested RNA isolated from organs or ascites of cats with feline infectious peritonitis and from feces of apparently healthy cats for the presence of FCoV RNA by using a reverse transcription nested PCR (RT-nPCR) targeting the highly conserved 3\u2032 untranslated region (23). Samples with results positive for FCoV were checked for the virus serotype by using an RT-nPCR targeting the S gene (24). Only samples positive for serotype I FCoV were included in this study.",
            "cite_spans": [
                {
                    "start": 269,
                    "end": 271,
                    "mention": "23",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 391,
                    "end": 393,
                    "mention": "24",
                    "ref_id": "BIBREF16"
                }
            ],
            "section": "FCoV Detection and Serotyping ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "We determined the sequence in the S gene region of interest by using an RT-nPCR. In brief, we synthesized complementary DNA by using an antisense primer (5\u2032-CCCTCGAGTCCCGCAGAAACCATACCTA-3\u2032) and superscript II reverse transcriptase (Promega, Madison, WI, USA) at 50\u00b0\u0421 for 1 h. We then performed the PCR by using Taq DNA polymerase (Promega) and specific primers (sense 5\u2032-CAATATTACAATGGCATAATGG-3\u2032, antisense 5\u2032-CCCTCGAGTCCCGCAGAAACCATACCTA-3\u2032) for the first reaction and specific primers (sense 5\u2032-GGCATAATGGTTTTACCTGGTG-3\u2032, antisense 5\u2032-TAATTAAGCCTCGCCTGCACTT-3\u2032) for the second reaction. PCR cycling conditions were 30 cycles at 94\u00b0\u0421 for 60 s, at 50\u00b0\u0421 for 30 s, and at 72\u00b0\u0421 for 1 min plus a 7-min extension at 72\u00b0\u0421 at the end of the reaction. All enzymes were used according to the manufacturer\u2019s instructions. Primer pairs were expected to generate a 598-bp product for the first PCR run and a 142-bp product for the second run.",
            "cite_spans": [],
            "section": "FCoV Detection and Serotyping ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Representative PCR products were purified by electrophoresis in 2% agarose gel followed by extraction from the gel by using a gel extraction kit (QIAGEN) according to the manufacturer\u2019s recommended instructions. Macrogen Inc. (http://dna.macrogen.com/eng/) sequenced the gel-purified DNA.",
            "cite_spans": [],
            "section": "FCoV Detection and Serotyping ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Two 96-well plates of degenerate primers (Technical Appendix) were designed from aligned reference genomes by using a computational PCR primer design pipeline. The pipeline was developed at the J. Craig Venter Institute (JCVI) to produce tiled amplicons with an optimal length of 550 bp, with 100-bp overlap to provide 6-fold sequence coverage of the genome. An M13 sequence tag was added to the 5\u2032 end of each degenerate primer and was used for sequencing. Primers were arranged in a 96-well plate format, and all PCRs for each sample were performed in 2 plates. The primers used in this study are listed in the Technical Appendix.",
            "cite_spans": [],
            "section": "Full Genome Sequencing ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Sequencing reactions were performed by using Big Dye Terminator (Applied Biosystems, Foster City, CA, USA) chemistry. Each amplicon was sequenced from both ends by using M13 primers, and sequencing reactions were analyzed by using a 3730 ABI sequencer (Applied Biosystems). Raw sequence data were trimmed to remove any primer-derived and low-quality sequence; gene sequences were assembled by using a viral assembly tool (www.jcvi.org/cms/research/software). Assemblies were edited computationally and manually. When insufficient underlying sequence information was obtained, the sample was entered into the secondary sequencing pipeline and reamplified by using existing primers or primers designed from the problematic sequence assembly itself. The reamplified sample was then sequenced again.",
            "cite_spans": [],
            "section": "Full Genome Sequencing ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "An RNA virus genome prediction program called VIGOR (Viral Genome ORF Reader, JCVI (www.jcvi.org/vigor) can decode many classes of viruses, taking into account virus-specific features, such as alternative splicing, internal open reading frames, and ribosomal slippage. This program was used to annotate de novo assemblies of coronaviruses sequenced at JCVI and also to validate newly assembled genomes during the finishing process. Last, we performed a quality control assessment and manually inspected the gene predictions before loading them into the annotation database at JCVI, from which they were exported in formats acceptable to the National Center for Biotechnology Information.",
            "cite_spans": [],
            "section": "Full Genome Sequencing ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "The full-length and partial FCoV genomic nucleotide sequences we obtained were deposited in the National Center for Biotechnology Information database (www.ncbi.nlm.nih.gov). The sequence accession numbers of the full-length FCoV sequences used in this study are listed in Table 1. The GenBank accession numbers for the partial S gene sequences are JQ304323\u2013JQ304518. Multiple-sequence alignments were constructed by using Clustal W (www.ebi.ac.uk/clustalw) with the Lasergene MegAlign (DNASTAR, www.dnastar.com/t-sub-products-lasergene-megalign.aspx) and MEGA4 (www.megasoftware.net) software programs. To identify key differences between FIPV and FECV, we analyzed their genomes and proteomes; for each nucleotide or amino acid position, we determined the rate at which FIPVs differed from all FECVs at that position. Phylogenetic analysis was performed by using features of the MEGA4 suite of programs. Phylogenetic trees of these sequences were obtained by using the neighbor-joining method. The bootstrap consensus tree, inferred from 1,000 replicates, was prepared; positions containing gaps and missing data were eliminated from the dataset.",
            "cite_spans": [],
            "section": "Multiple-Sequence Alignment ::: Materials and Methods",
            "ref_spans": [
                {
                    "start": 273,
                    "end": 280,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "To identify the distinguishing difference(s) between the FCoV pathotypes, we initiated a full genome sequencing program of FECVs found in the feces of apparently healthy cats and of FIPVs found in organs or ascites of cats with pathologically confirmed feline infectious peritonitis. To obtain a more extensive analysis of the coronavirus genome, this sequencing program is still ongoing; however, after the sequences of 11 genomes of each pathotype were completed, we performed a comparative FECV\u2013FIPV analysis, screening the genomes for nucleotide differences (Table 1). This was done by counting, for every nucleotide position, the number of FIPV genomes for which the identity at that position differed from that in all FECV genomes.",
            "cite_spans": [],
            "section": "Full Genome Sequencing ::: Results",
            "ref_spans": [
                {
                    "start": 563,
                    "end": 570,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Our results showed that differences were scattered along the entire genome (Figure 1). At 2,963 (10%) of the 29,277 genome positions, the nucleotide identity in at least 1 of the 11 FIPVs did not occur in any of the 11 FECVs. Of these 2,963 positions, 1,187 occurred in gene 1ab, 1,246 in the S gene, 248 in gene cluster 3abc, 22 in the envelope protein gene, 42 in the membrane protein gene, 113 in the nucleocapsid protein gene, and 106 in gene cluster 7ab, showing the disproportionally large genetic variation in the S gene. The frequency with which differences occurred at different nucleotide positions across the genome showed the following distribution: a difference was detected 1\u00d7 at 1,914 positions, 2\u00d7 at 945 positions, 3\u00d7 at 87 positions, 4\u00d7 at 15 positions, and 5\u00d7 at 1 position. At 1 position (23531), the nucleotide identity in 9 of the FIPVs was not found in any of the FECVs. No position(s) uniquely distinguished the 2 FCoV pathotypes.",
            "cite_spans": [],
            "section": "Full Genome Sequencing ::: Results",
            "ref_spans": [
                {
                    "start": 76,
                    "end": 84,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Nucleotide identity differed the most at position 23531: it was highly conserved (100% A) in all FECV genomes, and it was C or T in 9 of the 11 FIPV genomes. This difference occurs in the S gene and results in an amino acid difference in the predicted S protein. Thus, although all FECV S proteins have a methionine at position 1058, a leucine is encoded in the 9 FIPVs, irrespective of the identity of the genetic difference (C or T).",
            "cite_spans": [],
            "section": "Full Genome Sequencing ::: Results",
            "ref_spans": []
        },
        {
            "text": "To further investigate the single most prominent region of difference between FECVs and FIPVs, we established an RT-nPCR method to amplify and analyze the genomic region covering nucleotides 23442\u201324040 for the first PCR run and nucleotides 23451\u201323593 for the second run, which includes deviant position 23531. Altogether, 183 FECV and 118 FIPV RNAs isolated from different cats were sequenced in this specific region. Results for the 11 entirely sequenced FECVs and FIPVs are shown in Figure 2. The A at nucleotide 23531 was 100% conserved in all 183 FECVs in our collection. Of the 118 FIPVs, 96 (81.4%) had a T and 12 (10.2%) a C at this position; in both cases, this changes the methionine occurring at position 1058 in the FECV S protein into a leucine in FIPV (i.e., mutation M1058L).",
            "cite_spans": [],
            "section": "Sequencing of the S gene ::: Results",
            "ref_spans": [
                {
                    "start": 487,
                    "end": 495,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "Assuming that the difference observed in 108 of the 118 sequenced FIPVs may be responsible for the virulent phenotype, the remaining 10 viruses should be expected to carry alternative differences. In search of those differences, we performed a phylogenetic analysis of the partial nucleotide sequences accumulated by the RT-nPCR procedure, but the results did not enable further differentiation (data not shown). When carrying out a phylogenetic analysis of the translated partial amino acid sequences, we observed a small but distinct second cluster B in addition to the major cluster A constituted by the FIPVs having leucine at position 1058 in their S protein (Figure 3). This smaller cluster, formed by 5 (4.2%) of the sequenced FIPVs, appeared to be characterized by the occurrence of an alanine at position 1060 (i.e., mutation S1060A), just 2 residues downstream of the M1058 that is changed in most FIPVs. All other FIPVs and all sequenced FECVs consistently had a serine at this position. The difference is brought about in all 5 cases by a T\u2192G change at nucleotide 23537. Overall, we have detected characteristic differences with FECV for 113 (95.8%) of the 118 sequenced FIPVs. These differences were observed in both pathologic forms (i.e., wet and dry forms) of feline infectious peritonitis (Table 2).",
            "cite_spans": [],
            "section": "Phylogenetic Analysis ::: Results",
            "ref_spans": [
                {
                    "start": 665,
                    "end": 673,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 1307,
                    "end": 1314,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Our findings show differences in 2 alternative codons of the FCoV S gene that correlate with the feline infectious peritonitis disease phenotype in >95% of cases. Besides providing a realistic basis for diagnostic discrimination of the 2 FCoV pathotypes, our findings also support the mutation hypothesis. Thus, we propose that alternative mutations in the S protein of FECV give rise to a tropism change that allows the virus to escape from the intestine into body tissues, where it causes feline infectious peritonitis. Proof of this hypothesis will require introduction of these mutations into the FECV genome and demonstration of the virulence switch by infection of cats. However, this is a formidable challenge in the absence of a reverse genetics system and a proper cell culture system to generate and propagate these viruses.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Our findings relating the S protein to FCoV pathogenicity are not surprising, given earlier explorations into the involvement of various genes (7a, 7b, M, and 3c) (5\u20137,10,11,19\u201322,24,25). One of the most notable consequences of the presumed mutation in FECV is the acquisition of monocyte/macrophage tropism by the resulting virus (26). Thus, whereas replication of FECV is restricted to the epithelial cells lining the gut, the virulence mutation enables FIPV to efficiently infect and replicate in macrophages and spread the infection systemically (26). Such tropism change corresponds most logically with a modification in the S protein. An earlier study, using serotype II FCoVs, indicated a virulence role for the S protein (21); however, identification of the mutation(s) was not pursued because of the controversial nature of the FECV strain used in the study (14).",
            "cite_spans": [
                {
                    "start": 164,
                    "end": 165,
                    "mention": "5",
                    "ref_id": "BIBREF23"
                },
                {
                    "start": 166,
                    "end": 167,
                    "mention": "7",
                    "ref_id": "BIBREF25"
                },
                {
                    "start": 168,
                    "end": 170,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 171,
                    "end": 173,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 174,
                    "end": 176,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 177,
                    "end": 179,
                    "mention": "22",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 180,
                    "end": 182,
                    "mention": "24",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 183,
                    "end": 185,
                    "mention": "25",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 332,
                    "end": 334,
                    "mention": "26",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 551,
                    "end": 553,
                    "mention": "26",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 730,
                    "end": 732,
                    "mention": "21",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 868,
                    "end": 870,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "As for the serotype II viruses, the putative virulence mutations detected in the serotype I FCoV spike occur in the membrane-proximal domain of the protein. In coronaviruses, the S protein functions in cell entry; it is responsible for receptor attachment and membrane fusion. While the receptor binding site is located in the N terminal part of the protein, fusion is mediated by its membrane-proximal part. Coronavirus S proteins are class I fusion proteins, which typically contain domains instrumental for this process: 2 heptad repeat regions and a fusion peptide (27). The fusion peptide is located just upstream of the membrane-distal heptad repeat region, but it remains to be proven that it functions as a fusion peptide. The 2 putative virulence mutations identified in our study, M1058L and S1060A, map to this characteristic hydrophobic domain. Both changes are subtle and do not give clues as to their functional consequences. We assume, however, that these alternative mutations have a similar effect, and we speculate that the mutations in the remaining 4% of cases might also occur in the fusion peptide of the S protein.",
            "cite_spans": [
                {
                    "start": 570,
                    "end": 572,
                    "mention": "27",
                    "ref_id": "BIBREF19"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "If these mutations are all that is needed to convert a nonvirulent FECV into a lethal FIPV, the question arises as to why feline infectious peritonitis occurs so infrequently. For example, simple calculations based on a 10\u22124 frequency and a stochastic occurrence of RNA polymerase errors across the genome (28) predict that the M1058L mutation, for which 2 alternative substitutions of A23531 (to T or G) occur, would statistically arise once in every 1.5 \u00d7 104 genomes produced. In experimental FECV infection of kittens, we showed that up to 108 genome equivalents of the virus are shed per microliter of feces (18); thus, typical FECV infections would be expected to generate thousands of progeny carrying 1 of the critical mutations. However, the virulence phenotype supposedly associated with the mutation is not observed to any proportional extent. We can only speculate as to the reasons.",
            "cite_spans": [
                {
                    "start": 307,
                    "end": 309,
                    "mention": "28",
                    "ref_id": "BIBREF20"
                },
                {
                    "start": 614,
                    "end": 616,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "One likely possibility is that additional mutations (1 or more, perhaps alternative mutations) are required to generate the virulent pathotype. Such mutations would most probably involve the accessory gene 3c, which is intact in FECVs but severely affected in about two thirds of FIPVs (7,10\u201312). The 3c protein apparently is essential for replication of FECV in the gut but becomes nonessential once virulence mutation(s) elsewhere in the genome (e.g., in the S gene) enable the virus to infect macrophages and spread systemically. As we suggested earlier, loss of 3c function may not only be tolerated, it may even enhance the fitness of the mutant virus in its new biotope and, as a consequence, hamper its return to the gut. If the mutant virus is absent in the gut, it will not be shed in feces, providing an explanation for the seemingly rare incidence of feline infectious peritonitis outbreaks. Our discoveries of the critical differences between FECVs and FIPVs are clearly only a small step toward understanding the pathogenetic phenomena of feline coronavirus infections.",
            "cite_spans": [
                {
                    "start": 287,
                    "end": 288,
                    "mention": "7",
                    "ref_id": "BIBREF25"
                },
                {
                    "start": 289,
                    "end": 291,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 292,
                    "end": 294,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Comparison of full genomes of 11 lethal feline infectious peritonitis viruses (FIPVs) with full genomes of 11 nonvirulent feline enteric coronaviruses (FECVs). Nucleotide (nt) positions are shown on the x-axis; y-axis indicates number of FIPV genomes for which the identity at the nt position differed from identity at same position in all FECV genomes. FIPV strain C1Je (GenBank accession no. DQ848678) was used as the reference for nt numbering. *Highest difference score: 9 FIPVs had identities at nt position 23531 that differed from those at the same position in all FECVs. 1a, gene 1a; 1b, gene 1b; S, spike protein gene; 3abc, gene cluster 3abc; E, envelope protein gene; M, membrane protein gene; N, nucleocapsid protein gene; 7ab, gene cluster 7ab.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Alignment of partial nucleotide sequences and translated amino acid sequences in the spike protein of 11 strains each of 2 feline coronavirus pathotypes: FIPVs (lethal) and FECVs (nonvirulent). The viruses were sequenced in a study to distinguish virulent from nonvirulent feline coronaviruses (see Table 1). FIPV strain C1Je (GenBank accession no. DQ848678) was used as the reference for numbering. Sequence positions are shown along the top; virus strains are shown on the right. Specific differences between the pathotypes are boxed. FIPVs, feline infectious peritonitis viruses; FECVs, feline enteric coronaviruses.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3: Phylogenetic tree based on partial amino acid sequences (aa 1056\u20131069) of the spike proteins of 118 feline infectious peritonitis viruses (FIPVs) and 183 feline enteric coronaviruses (FECVs) obtained by using reverse transcription nested PCR and sequencing of the distinguishing genomic region. A circular rooted neighbor-joining tree was constructed by using the bootstrap method and applying 1,000 replicates. Black dots indicate FIPVs. Clade A comprises FIPVs containing the M1058L mutation; clade B comprises FIPVs containing the S1060A mutation.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Coronaviruses post-SARS: update on replication and pathogenesis.",
            "authors": [],
            "year": 2009,
            "venue": "Nat Rev Microbiol",
            "volume": "7",
            "issn": "",
            "pages": "439-50",
            "other_ids": {
                "DOI": [
                    "10.1038/nrmicro2147"
                ]
            }
        },
        "BIBREF1": {
            "title": "Feline infectious peritonitis viruses arise by mutation from endemic feline enteric coronaviruses.",
            "authors": [],
            "year": 1998,
            "venue": "Virology",
            "volume": "243",
            "issn": "",
            "pages": "150-7",
            "other_ids": {
                "DOI": [
                    "10.1006/viro.1998.9045"
                ]
            }
        },
        "BIBREF2": {
            "title": "Significance of coronavirus mutants in diseased tissues of cats suffering from feline infectious peritonitis.",
            "authors": [],
            "year": 2009,
            "venue": "Viruses",
            "volume": "1",
            "issn": "",
            "pages": "166-84",
            "other_ids": {
                "DOI": [
                    "10.3390/v1020166"
                ]
            }
        },
        "BIBREF3": {
            "title": "Feline infectious peritonitis: role of the feline coronavirus 3c gene in intestinal tropism and pathogenicity based upon isolates from resident and adopted shelter cats.",
            "authors": [],
            "year": 2012,
            "venue": "Virus Res",
            "volume": "165",
            "issn": "",
            "pages": "17-28",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virusres.2011.12.020"
                ]
            }
        },
        "BIBREF4": {
            "title": "Common virus infections in cats, before and after being placed in shelters, with emphasis on feline enteric coronavirus.",
            "authors": [],
            "year": 2004,
            "venue": "J Feline Med Surg",
            "volume": "6",
            "issn": "",
            "pages": "83-8",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jfms.2003.08.008"
                ]
            }
        },
        "BIBREF5": {
            "title": "Pathogenesis of feline enteric coronavirus infection.",
            "authors": [],
            "year": 2008,
            "venue": "J Feline Med Surg",
            "volume": "10",
            "issn": "",
            "pages": "529-41",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jfms.2008.02.006"
                ]
            }
        },
        "BIBREF6": {
            "title": "A review of feline infectious peritonitis virus infection: 1963\u20132008.",
            "authors": [],
            "year": 2009,
            "venue": "J Feline Med Surg",
            "volume": "11",
            "issn": "",
            "pages": "225-58",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jfms.2008.09.008"
                ]
            }
        },
        "BIBREF7": {
            "title": "Sites of feline coronavirus persistence in healthy cats.",
            "authors": [],
            "year": 2010,
            "venue": "J Gen Virol",
            "volume": "91",
            "issn": "",
            "pages": "1698-707",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.020214-0"
                ]
            }
        },
        "BIBREF8": {
            "title": "Persistence and evolution of feline coronavirus in a closed cat-breeding colony.",
            "authors": [],
            "year": 1997,
            "venue": "Virology",
            "volume": "234",
            "issn": "",
            "pages": "349-63",
            "other_ids": {
                "DOI": [
                    "10.1006/viro.1997.8663"
                ]
            }
        },
        "BIBREF9": {
            "title": "Pathogenic characteristics of persistent feline enteric coronavirus infection in cats.",
            "authors": [],
            "year": 2010,
            "venue": "Vet Res",
            "volume": "41",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1051/vetres/2010043"
                ]
            }
        },
        "BIBREF10": {
            "title": "Diagnostic methods for feline coronavirus: a review.",
            "authors": [],
            "year": 2010,
            "venue": "Vet Med Int",
            "volume": "2010",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Coronavirus pathogenesis.",
            "authors": [],
            "year": 2011,
            "venue": "Adv Virus Res",
            "volume": "81",
            "issn": "",
            "pages": "85-164",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "The molecular genetics of feline coronaviruses: comparative sequence analysis of the ORF7a/7b transcription unit of different biotypes.",
            "authors": [],
            "year": 1995,
            "venue": "Virology",
            "volume": "212",
            "issn": "",
            "pages": "622-31",
            "other_ids": {
                "DOI": [
                    "10.1006/viro.1995.1520"
                ]
            }
        },
        "BIBREF13": {
            "title": "Acquisition of macrophage tropism during the pathogenesis of feline infectious peritonitis is determined by mutations in the feline coronavirus spike protein.",
            "authors": [],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "14122-30",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.79.22.14122-14130.2005"
                ]
            }
        },
        "BIBREF14": {
            "title": "Mutation of neutralizing/antibody-dependent enhancing epitope on spike protein and 7b gene of feline infectious peritonitis virus: influences of viral replication in monocytes/macrophages and virulence in cats.",
            "authors": [],
            "year": 2011,
            "venue": "Virus Res",
            "volume": "156",
            "issn": "",
            "pages": "72-80",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virusres.2010.12.020"
                ]
            }
        },
        "BIBREF15": {
            "title": "Detection of feline coronavirus RNA in feces, tissues, and body fluids of naturally infected cats by reverse transcriptase PCR.",
            "authors": [],
            "year": 1995,
            "venue": "J Clin Microbiol",
            "volume": "33",
            "issn": "",
            "pages": "684-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Persistence and transmission of natural type I feline coronavirus infection.",
            "authors": [],
            "year": 2003,
            "venue": "J Gen Virol",
            "volume": "84",
            "issn": "",
            "pages": "2735-44",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.19129-0"
                ]
            }
        },
        "BIBREF17": {
            "title": "The paradox of feline coronavirus pathogenesis: a review.",
            "authors": [],
            "year": 2011,
            "venue": "Adv Virol",
            "volume": "2011",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1155/2011/109849"
                ]
            }
        },
        "BIBREF18": {
            "title": "Intrinsic resistance of feline peritoneal macrophages to coronavirus infection correlates with in vivo virulence.",
            "authors": [],
            "year": 1989,
            "venue": "J Virol",
            "volume": "63",
            "issn": "",
            "pages": "436-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF19": {
            "title": "The coronavirus spike protein is a class I virus fusion protein: structural and functional characterization of the fusion core complex.",
            "authors": [],
            "year": 2003,
            "venue": "J Virol",
            "volume": "77",
            "issn": "",
            "pages": "8801-11",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.77.16.8801-8811.2003"
                ]
            }
        },
        "BIBREF20": {
            "title": "Map locations of mouse hepatitis virus temperature-sensitive mutants: confirmation of variable rates of recombination.",
            "authors": [],
            "year": 1994,
            "venue": "J Virol",
            "volume": "68",
            "issn": "",
            "pages": "7458-66",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF21": {
            "title": "Porcine respiratory coronavirus: molecular features and virus\u2013host interactions.",
            "authors": [],
            "year": 1993,
            "venue": "Vet Res",
            "volume": "24",
            "issn": "",
            "pages": "125-50",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF22": {
            "title": "Sequence comparison of porcine respiratory coronavirus isolates reveals heterogeneity in the S, 3, and 3\u20131 genes.",
            "authors": [],
            "year": 1995,
            "venue": "J Virol",
            "volume": "69",
            "issn": "",
            "pages": "3176-84",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF23": {
            "title": "Genetics and pathogenesis of feline infectious peritonitis virus.",
            "authors": [],
            "year": 2009,
            "venue": "Emerg Infect Dis",
            "volume": "15",
            "issn": "",
            "pages": "1445-52",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1509.081573"
                ]
            }
        },
        "BIBREF24": {
            "title": "Genetic determinants of pathogenesis by feline infectious peritonitis virus.",
            "authors": [],
            "year": 2011,
            "venue": "Vet Immunol Immunopathol",
            "volume": "143",
            "issn": "",
            "pages": "265-8",
            "other_ids": {
                "DOI": [
                    "10.1016/j.vetimm.2011.06.021"
                ]
            }
        },
        "BIBREF25": {
            "title": "Feline infectious peritonitis: insights into feline coronavirus pathobiogenesis and epidemiology based on genetic analysis of the viral 3c gene.",
            "authors": [],
            "year": 2010,
            "venue": "J Gen Virol",
            "volume": "91",
            "issn": "",
            "pages": "415-20",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.016485-0"
                ]
            }
        },
        "BIBREF26": {
            "title": "Sequence analysis of feline coronaviruses and the circulating virulent/avirulent theory.",
            "authors": [],
            "year": 2011,
            "venue": "Emerg Infect Dis",
            "volume": "17",
            "issn": "",
            "pages": "744-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF27": {
            "title": "Two related strains of feline infectious peritonitis virus isolated from immunocompromised cats infected with a feline enteric coronavirus.",
            "authors": [],
            "year": 1996,
            "venue": "J Clin Microbiol",
            "volume": "34",
            "issn": "",
            "pages": "3180-4",
            "other_ids": {
                "DOI": []
            }
        }
    }
}