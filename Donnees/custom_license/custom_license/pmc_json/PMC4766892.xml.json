{
    "paper_id": "PMC4766892",
    "metadata": {
        "title": "Absence of Middle East Respiratory Syndrome Coronavirus in Camelids, Kazakhstan, 2015",
        "authors": [
            {
                "first": "Eve",
                "middle": [],
                "last": "Miguel",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ranawaka",
                "middle": [
                    "A.P.M."
                ],
                "last": "Perera",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Almagul",
                "middle": [],
                "last": "Baubekova",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "V\u00e9ronique",
                "middle": [],
                "last": "Chevalier",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bernard",
                "middle": [],
                "last": "Faye",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Nurlan",
                "middle": [],
                "last": "Akhmetsadykov",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Chun",
                "middle": [
                    "Yin"
                ],
                "last": "Ng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Fran\u00e7ois",
                "middle": [],
                "last": "Roger",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Malik",
                "middle": [],
                "last": "Peiris",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "To the Editor: Middle East respiratory syndrome coronavirus (MERS-CoV) acquired from animals causes severe pneumonia in humans, with some chains of human-to-human transmission, leading to large outbreaks. MERS-CoV is a cause of concern for global public health. The only natural host of MERS-CoV identified so far is the dromedary camel (Camel dromedarius) (1,2), and transmission from camels to humans has been documented (3). The geographic distribution of MERS-CoV in dromedaries extends beyond the Arabian Peninsula (where human cases have been reported) to North and East Africa (where human cases have not been reported) (2,4). However, MERS-CoV from a camel in Egypt and MERS-CoV from a human were phenotypically similar in tropism and replication competence in ex vivo cultures of the human respiratory tract (5).",
            "cite_spans": [
                {
                    "start": 358,
                    "end": 359,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 360,
                    "end": 361,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 424,
                    "end": 425,
                    "mention": "3",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 628,
                    "end": 629,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 630,
                    "end": 631,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 818,
                    "end": 819,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Our previous study demonstrated no evidence of MERS-CoV infection in Bactrian camels in Mongolia (6). The question whether MERS-CoV is endemic in camelids in Central Asia remains unanswered. MERS-CoV RNA was detected in swab samples from camels in Iran, which had been imported from Pakistan; however, where the infection was acquired is unclear (7).",
            "cite_spans": [
                {
                    "start": 98,
                    "end": 99,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 347,
                    "end": 348,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In Asia, Kazakhstan is of particular interest because large populations of 2 major camelid species overlap: 90% Bactrian (Kazakh breed including 3 ecotypes) and \u224810% dromedary (Arvana breed from Turkmenistan) and their hybrids (8). To determine whether MERS-CoV is present in camelids in Kazakhstan, we conducted a seroepidemiologic survey. ",
            "cite_spans": [
                {
                    "start": 228,
                    "end": 229,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "During February\u2013March 2015, blood was collected from 550 female camels (455 dromedary, 95 Bactrian) (Figure) in 2 regions, Almaty and Shymkent, which differ in camelid density (0.034 and 0.20 camels/km2, respectively; http://www.stat.gov.kz). Dromedaries were sampled in the cities/villages of Kyzylorda (105 animals from 2 herds), Zanakorgan (35 animals from 1 herd), Sholakkorgan (110 animals from 2 herds), and Akshiy (205 animals from 4 herds). Bactrian camels were sampled in Sholakkorgan (40 animals from 1 herd) and Kanshengel (55 animals from 1 herd) (Figure). For dromedary camels, mean age was 6.1 years (SD 3\u20137 years) and mean herd size was 53.6 animals (SD 31\u201370); for Bactrian camels, mean age was 6.5 years (SD 5\u20138 years) and mean herd size was 48.6 animals (SD 40\u201355). Serum samples were tested for MERS-CoV antibodies at a screening dilution of 1:20 by using a validated MERS-CoV (strain EMC) spike pseudoparticle neutralization test (9). Positive and negative controls were included in each run. Absence of positivity for any sample indicated a lack of recent or past MERS-CoV infection.",
            "cite_spans": [
                {
                    "start": 951,
                    "end": 952,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 101,
                    "end": 107,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 560,
                    "end": 566,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Two randomly selected samples each from dromedaries from Kyzlorda, Zanakorgan, and Akshiy and Bactrians from Sholakkorgan and Kanshenegel were tested for neutralizing antibody to bovine coronavirus (9). All 10 samples were seropositive, as has been reported for Bactrian camels in Mongolia and the Middle East (6,9).",
            "cite_spans": [
                {
                    "start": 199,
                    "end": 200,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 311,
                    "end": 312,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 313,
                    "end": 314,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Given the uniformly high seroprevalence of MERS-CoV infection among dromedaries in Africa and the Arabian Peninsula, the lack of infection in dromedaries in southern Kazakhstan was surprising. Because genetically diverse MERS-CoV from Africa remains antigenically conserved with viruses from the Arabian Peninsula, the lack of antibodies is probably not explained by antigenically divergent strains (9). Feral dromedaries in Australia, which originated from animals imported from Afghanistan or Pakistan during 1840\u20131907, are also seronegative for MERS-CoV (10). In contrast, bovine-like coronavirus seems to be present in dromedaries everywhere (including Kazakhstan and Australia). ",
            "cite_spans": [
                {
                    "start": 400,
                    "end": 401,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 558,
                    "end": 560,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Our study was limited by sample size and by geographic coverage. Of the \u2248180,000 camels in Kazakhstan, we studied camelids from only 2 of the 13 provinces. No samples were collected from the western part of the country near Turkmenistan, where dromedaries are also common.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Dromedaries are clearly a natural host of MERS-CoV. However, the finding that MERS-CoV is not endemic in dromedaries in all geographic regions suggests the possibility that dromedaries may not be the ultimate natural reservoir (i.e., the long-term host of a pathogen of an infectious disease). Topography (i.e., mountain chains) may limit camel movements from the Middle East or Africa to Central Asia, although such interchange certainly occurred centuries ago as a consequence of the silk-trade routes through southern Kazakhstan. The only known recent imports to Kazakhstan are dromedaries (Arvana breed), brought from Turkmenistan for cross-breeding with Bactrians to improve milk production (8). The findings that MERS-CoV is not universally endemic in dromedaries raises the hypothesis that certain species of bats or some other animal, the environment, or both, may constitute a maintenance community and be the true natural reservoir of MERS-CoV and that the virus spills over to camels and is maintained within camels for varying periods of time. Further studies on the epidemiology of MERS-CoV infection among camelids from central Asia are warranted.",
            "cite_spans": [
                {
                    "start": 697,
                    "end": 698,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure: Density of camelids in Kazakhstan (extracted from the Ministry of National Economy of the Republic of Kazakhstan Committee on Statistics, Department of Statistics; http://www.stat.gov.kz) and specimen collection for detection of Middle East respiratory syndrome virus, by species and region, 2015.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Absence of MERS-CoV antibodies in feral camels in Australia: implications for the pathogen\u2019s origin and spread.",
            "authors": [],
            "year": 2015,
            "venue": "One Health.",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1016/j.onehlt.2015.10.003"
                ]
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2012,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}