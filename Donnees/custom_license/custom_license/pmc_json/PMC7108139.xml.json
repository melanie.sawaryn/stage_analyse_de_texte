{
    "paper_id": "PMC7108139",
    "metadata": {
        "title": "Consistent Detection of 2019 Novel Coronavirus in Saliva",
        "authors": [
            {
                "first": "Kelvin",
                "middle": [
                    "Kai-Wang"
                ],
                "last": "To",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Owen",
                "middle": [
                    "Tak-Yin"
                ],
                "last": "Tsang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Cyril",
                "middle": [
                    "Chik-Yan"
                ],
                "last": "Yip",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok-Hung",
                "middle": [],
                "last": "Chan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tak-Chiu",
                "middle": [],
                "last": "Wu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jacky",
                "middle": [
                    "Man-Chun"
                ],
                "last": "Chan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wai-Shing",
                "middle": [],
                "last": "Leung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Thomas",
                "middle": [
                    "Shiu-Hong"
                ],
                "last": "Chik",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Chris",
                "middle": [
                    "Yau-Chung"
                ],
                "last": "Choi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Darshana",
                "middle": [
                    "H"
                ],
                "last": "Kandamby",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [
                    "Christopher"
                ],
                "last": "Lung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anthony",
                "middle": [
                    "Raymond"
                ],
                "last": "Tam",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rosana",
                "middle": [
                    "Wing-Shan"
                ],
                "last": "Poon",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Agnes",
                "middle": [
                    "Yim-Fong"
                ],
                "last": "Fung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ivan",
                "middle": [
                    "Fan-Ngai"
                ],
                "last": "Hung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Vincent",
                "middle": [
                    "Chi-Chung"
                ],
                "last": "Cheng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jasper",
                "middle": [
                    "Fuk-Woo"
                ],
                "last": "Chan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok-Yung",
                "middle": [],
                "last": "Yuen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In Hong Kong, 2019-nCoV testing was performed by Public Health Laboratory Services Branch in Hong Kong for patients who fulfilled the reporting criteria or enhanced surveillance criteria [9]. A patient is considered to have laboratory-confirmed infection if 2019-nCoV was detected in their nasopharyngeal or sputum specimens.",
            "cite_spans": [
                {
                    "start": 188,
                    "end": 189,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Patient Specimens ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "Saliva was collected by asking the patient to cough out saliva from their throat into a sterile container, and 2 mL of viral transport medium was added as we described previously [5, 6]. This study was approved by the Institutional Review Board of the University of Hong Kong/Hospital Authority Hong Kong West Cluster (UW 13-372).",
            "cite_spans": [
                {
                    "start": 180,
                    "end": 181,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 183,
                    "end": 184,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Patient Specimens ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "Saliva specimens were subjected to total nucleic acid extraction by NucliSENS easyMAG (BioMerieux) as we described previously [5]. Each specimen was mixed with lysis buffer. After extraction, the total nucleic acid was recovered using 55 \u03bcL of elution buffer. In-house 1-step real-time reverse transcription\u2013quantitative polymerase chain reaction (RT-qPCR) assay targeting the S gene of 2019-nCoV was performed using QuantiNova SYBR Green RT-PCR Kit (Qiagen) in a LightCycler 480 Real-Time PCR System (Roche), as we described previously [2].",
            "cite_spans": [
                {
                    "start": 127,
                    "end": 128,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 538,
                    "end": 539,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Nucleic Acid Extraction and Real-time Reverse Transcription\u2013Quantitative Polymerase Chain Reaction for 2019-nCoV ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "Viral culture of 2019-nCoV was conducted in a biosafety level-3 facility. Vero E6 cells were seeded with 1 mL of minimum essential medium (MEM) at 2 \u00d7 105 cells/mL in culture tubes and incubated at 37\u00b0C in a carbon dioxide incubator for 1\u20132 days until confluence for inoculation. Each saliva specimen was inoculated in duplicate; 1 tube contained tosylsulfonyl phenylalanyl chloromethyl ketone\u2013treated trypsin (0.5 \u03bcg/mL) in serum-free MEM and the other tube contained MEM with 1% fetal calf serum. Each tube was inoculated with 0.2 mL of saliva and was incubated in a slanted position so that the inoculum covered the monolayer for 60 minutes at 37\u00b0C. Then 1 mL of either MEM or trypsin MEM was added and incubated in a roller apparatus at a speed 12 to 15 revolutions per hour. Virus-induced cytopathic effect was examined daily for up to 7 days.",
            "cite_spans": [],
            "section": "Viral Culture ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "A total of 12 patients with laboratory-confirmed 2019-nCoV infection in Hong Kong were included. The median age was 62.5 years, ranging from 37 to 75 years. There were 5 female and 7 male patients. At the time of writing, all patients were still hospitalized. Saliva specimens were collected at a median of 2 days after hospitalization (range, 0\u20137 days) (Figure 1). The 2019-nCoV was detected in the initial saliva specimens of 11 patients (91.7%). For patient K, the first saliva specimen collected on the day of hospital admission tested negative. The median viral load of the first available saliva specimens was 3.3 \u00d7 106 copies/mL (range, 9.9 \u00d7 102 to 1.2 \u00d7 108 copies/mL).",
            "cite_spans": [],
            "section": "RESULTS",
            "ref_spans": [
                {
                    "start": 355,
                    "end": 363,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Serial saliva specimens were available for 6 patients. The viral load was highest in the earliest available specimens for 5 patients (83.3%). For patient H, the viral load was slightly higher on day 1 after hospitalization (6.8 \u00d7 107 copies/mL) than on the day of hospital admission (5.7 \u00d7 107 copies/mL). For patient B, viral shedding in saliva was still detected on day 11 after hospitalization. In 33 patients whose nasopharyngeal specimens tested negative for 2019-nCoV, all saliva specimens also tested negative. At the time of writing, viral cultures were positive for 3 patients and negative for 2 patients.",
            "cite_spans": [],
            "section": "RESULTS",
            "ref_spans": []
        },
        {
            "text": "In this study, we have demonstrated that 2019-nCoV could be detected in the saliva specimens of 11 of the 12 patients studied. Serial saliva specimens showed declines in salivary 2019-nCoV RNA levels after hospitalization. Viral culture demonstrated that live viruses were present in the saliva of 3 patients.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "There are several advantages in using saliva specimens for the diagnosis of 2019-nCoV. First, saliva specimens can be provided by the patient easily without any invasive procedures. Therefore, the use of saliva specimens could reduce the risk of nosocomial 2019-nCoV transmission. Cases of 2019-nCoV infection among healthcare workers have been found, with at least 1 reported death [10]. Second, the use of saliva will allow specimen collection outside the hospitals where airborne-infection isolation rooms are not available, such as in outpatient clinics or in the community. In the setting where a large number of individuals require screening, saliva would represent a practical noninvasive specimen type. Third, since healthcare workers are not required to collect saliva specimens, the use of saliva specimens will eliminate the waiting time for specimen collection, and hence the results would be available much sooner. This is especially important in busy clinical settings where the number of available staff is limited.",
            "cite_spans": [
                {
                    "start": 384,
                    "end": 386,
                    "mention": "10",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Among patients with serial saliva specimens available, there was a general decline in viral load for most patients, but 1 patient had viral shedding in the saliva for at least 11 days after hospitalization. The use of saliva is preferred over nasopharyngeal or oropharyngeal specimens for serial viral load monitoring because this would reduce the discomfort to the patient and reduce the health hazards to healthcare workers during repeated sampling. Our experience with SARS in 2003 showed that viral load often peaked at day 10 after symptom onset. Thus, early detection and isolation of cases was strategic for infection control and provides the window of opportunity for antiviral therapy to decrease the peak viral load.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "The positive viral culture indicates that saliva contains live viruses that may allow transmission. Respiratory viruses are considered to be transmitted from person to person through direct or indirect contact, or via coarse or fine droplets. Saliva can be emitted through cough, and respiratory droplets containing influenza virus can be found even during normal breathing [11]. Therefore, 2019-nCoV may be transmitted via saliva directly or indirectly even among patients without coughing or other respiratory symptoms. Our findings reinforce the use of surgical masks as a control measure.",
            "cite_spans": [
                {
                    "start": 375,
                    "end": 377,
                    "mention": "11",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "SARS-CoV has been shown to infect epithelial cells in salivary gland ducts in rhesus macaques [12]. The presence of 2019-nCoV in patients\u2019 saliva suggests the possibility of salivary gland infection. However, it should be noted that saliva specimens not only contain saliva secreted from major or minor salivary glands but also contain secretions coming down from the nasopharynx or coming up from the lung via the action of cilia lining the airway. Further studies are required to delineate the sources of 2019-nCoV in saliva.",
            "cite_spans": [
                {
                    "start": 95,
                    "end": 97,
                    "mention": "12",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Our results have demonstrated the potential for saliva to be a noninvasive specimen type for the diagnosis and viral load monitoring of 2019-nCoV. Because saliva can be provided by patients without any invasive procedures, the use of saliva specimens will reduce the risk of nosocomial transmission of 2019-nCoV and is ideal for situations in which nasopharyngeal specimen collection may be contraindicated.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1.: Saliva viral load in patients with 2019 novel coronavirus infection. For this figure, specimens with undetected viral load were assigned a value of 101.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Coronavirus as a possible cause of severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Peiris",
                    "suffix": ""
                },
                {
                    "first": "ST",
                    "middle": [],
                    "last": "Lai",
                    "suffix": ""
                },
                {
                    "first": "LL",
                    "middle": [],
                    "last": "Poon",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1319-25",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "A familial cluster of pneumonia associated with the 2019 novel coronavirus indicating person-to-person transmission: a study of a family cluster",
            "authors": [
                {
                    "first": "JF",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Yuan",
                    "suffix": ""
                },
                {
                    "first": "KH",
                    "middle": [],
                    "last": "Kok",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan, China",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Genomic characterization of the 2019 novel human-pathogenic coronavirus isolated from a patient with atypical pneumonia after visiting Wuhan",
            "authors": [
                {
                    "first": "JF",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "KH",
                    "middle": [],
                    "last": "Kok",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Emerg Microbes Infect",
            "volume": "9",
            "issn": "",
            "pages": "221-36",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Additional molecular testing of saliva specimens improves the detection of respiratory viruses",
            "authors": [
                {
                    "first": "KK",
                    "middle": [],
                    "last": "To",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Yip",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Emerg Microbes Infect",
            "volume": "6",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Saliva as a diagnostic specimen for testing respiratory virus by a point-of-care molecular assay: a diagnostic validity study",
            "authors": [
                {
                    "first": "KKW",
                    "middle": [],
                    "last": "To",
                    "suffix": ""
                },
                {
                    "first": "CCY",
                    "middle": [],
                    "last": "Yip",
                    "suffix": ""
                },
                {
                    "first": "CYW",
                    "middle": [],
                    "last": "Lai",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Clin Microbiol Infect",
            "volume": "25",
            "issn": "",
            "pages": "372-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Respiratory virus infection among hospitalized adult patients with or without clinically apparent respiratory infection: a prospective cohort study",
            "authors": [
                {
                    "first": "KKW",
                    "middle": [],
                    "last": "To",
                    "suffix": ""
                },
                {
                    "first": "KH",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Ho",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Clin Microbiol Infect",
            "volume": "25",
            "issn": "",
            "pages": "1539-45",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Detection of SARS-associated coronavirus in throat wash and saliva in early diagnosis",
            "authors": [
                {
                    "first": "WK",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "IJ",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "1213-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Infectious virus in exhaled breath of symptomatic seasonal influenza cases from a college community",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Yan",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Grantham",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Pantelic",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Proc Natl Acad Sci USA",
            "volume": "115",
            "issn": "",
            "pages": "1081-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Epithelial cells lining salivary gland ducts are early target cells of severe acute respiratory syndrome coronavirus infection in the upper respiratory tracts of rhesus macaques",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Alvarez",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J Virol",
            "volume": "85",
            "issn": "",
            "pages": "4025-30",
            "other_ids": {
                "DOI": []
            }
        }
    }
}