{
    "paper_id": "PMC7120397",
    "metadata": {
        "title": "Manipulation of the Coronavirus Genome Using Targeted RNA Recombination with Interspecies Chimeric Coronaviruses",
        "authors": [
            {
                "first": "Dave",
                "middle": [],
                "last": "Cavanagh",
                "suffix": "",
                "email": "dave.cavanagh@bbsrc.ac.uk",
                "affiliation": {}
            },
            {
                "first": "Cornelis",
                "middle": [
                    "A.M."
                ],
                "last": "de Haan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Bert",
                "middle": [
                    "Jan"
                ],
                "last": "Haijema",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Paul",
                "middle": [
                    "S."
                ],
                "last": "Masters",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peter",
                "middle": [
                    "J.M."
                ],
                "last": "Rottier",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Targeted RNA recombination was the first reverse genetics method developed to introduce mutations into the coronavirus genome [for a recent review, see (1)]. Actually, its first demonstration involved the repair of an 87-nucleotide deletion in the N gene of a temperature-sensitive mutant of the mouse hepatitis virus (MHV) by recombination with a synthetic donor RNA equivalent to the subgenomic mRNA for the wild-type N gene (2). Recombinant virus was selected by its increased temperature stability and efficient growth at the nonpermissive temperature, a tedious process. The frequency of RNA recombination and hence the ease with which recombinant viruses could be isolated was subsequently improved by the use of defective interfering (DI) RNAs as donors, i.e., RNA constructs consisting of sequences derived from the 5\u2032 and 3\u2032 terminal parts of the MHV genome (3,4). Yet, selection of mutant viruses remained the Achilles\u2019 heel of the method, particularly when one was seeking to construct temperature-sensitive or otherwise crippled mutants. Hence, the approach did not become common practice.",
            "cite_spans": [
                {
                    "start": 153,
                    "end": 154,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 428,
                    "end": 429,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 868,
                    "end": 869,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 870,
                    "end": 871,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "A decisive improvement emerged from studies on the assembly of the spike into the coronavirion. With the use of virus-like particles it was shown that this process is governed by the spike (S) protein\u2019s carboxy-terminal region, comprising the transmembrane and cytoplasmic domains (5). Spikes were still incorporated into virus-like particles after replacement of their ectodomain by that of another coronavirus. Such chimeric spikes also retained their biological functions\u2014receptor binding and membrane fusion\u2014as judged by their ability to cause cell-cell fusion. Given the generally strict species-specific nature of coronaviruses in tissue culture these features enabled the design of a powerful positive selection strategy based on interspecies chimeric coronaviruses, as was first described for MHV (6).",
            "cite_spans": [
                {
                    "start": 282,
                    "end": 283,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 806,
                    "end": 807,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Here we describe the detailed procedures for the method of targeted RNA recombination using this strategy of host cell switching. Protocols are presented for MHV and FIPV (feline infectious peritonitis virus) as examples. For any given virus the procedure starts with the generation of an interspecies chimeric coronavirus. Thus, a donor DI RNA is prepared containing a chimeric S gene. Cells are infected with the virus in question and additionally transfected with the donor RNA. In such cells homologous recombination of the donor RNA with viral genomic RNA can give rise to the formation and release of chimeric virus. This chimeric virus is subsequently selected by passing the culture supernatant onto cells that are infectable only by the virus from which the chimeric S protein ectodomain was derived. Felinized MHV (fMHV) and murinized FIPV (mFIPV) are thus the chimeric viruses described in the examples; they are isolated and grown in feline and murine cells, respectively. The chimeric viruses subsequently function as the recipients for the second round of homologous recombination designed to regenerate the original virus or to obtain mutants thereof. Donor DI RNA is again prepared that now contains the original wild-type S gene, plus any additional intended mutations. In cells infected with the chimeric virus, recombination with this RNA can lead to formation of (mutants of) the original virus, which can now, in turn, be isolated by passing the collection of progeny viruses onto its natural cells. A schematic outline of the latter recombination process is depicted in Fig.\n1\n\n",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": [
                {
                    "start": 1597,
                    "end": 1598,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Owing to restrictions inherent in the selection principle, the targeted RNA recombination system described here will have its main value in the study and manipulation of functions specified by the genomic regions downstream of the polymerase gene. For a survey of important contributions that the targeted recombinant approach has made in the different areas of its application, the reader is referred to the review by Masters and Rottier (1).",
            "cite_spans": [
                {
                    "start": 440,
                    "end": 441,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "\nPlasmids pMH54 (6) and pBRDI1 (7) are used for the generation of MHV and FIPV recombinants, respectively.Restriction enzymes Pac I and Not I (New England Biolabs) are used in combination with the supplied reaction buffer.3 M ammonium acetate solution (see\nNote 1).Ethanol 70% (v/v).Ethanol 96% (v/v).T7 RNA polymerase kit (Ambion).Agarose (Invitrogen).1X Tris-borate electrophoresis buffer (TBE): 0.09 M Tris-borate, 0.002 M EDTA. 0.5X Tris-acetate electrophoresis buffer (TAE): 0.02 M Tris-acetate, 0.0005 EDTA.\n",
            "cite_spans": [
                {
                    "start": 17,
                    "end": 18,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 32,
                    "end": 33,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Plasmids and RNA Synthesis ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nDulbecco\u2019s Modified Eagle\u2019s Medium (DMEM; Biowhittaker) supplemented with 10% fetal bovine serum (FBS; HyClone), 100 IU of penicillin/ml and 100 \u03bcg of streptomycin/ml (p/s) (DMEM++).Both murine LR7 cells (5) and feline FCWF cells (ATCC) are used for the generation of MHV and FIPV recombinants (see\nNote 2).Solutions of trypsin (500 mg/liter trypsin 1:250) and EDTA (200 mg/liter) (Trypsin EDTA solution; Biowhittaker).Dulbecco\u2019s phosphate buffered saline (DPBS) without Ca and Mg.Select agar (Invitrogen).2X Minimal Essential Medium Eagle (EMEM; Biowhittaker) supplemented with 20% FBS and 2X p/s.\n",
            "cite_spans": [
                {
                    "start": 206,
                    "end": 207,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Cell Culture and Plaque Assay ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nRecipient viruses fMHV and mFIPV are used for the generation of MHV and 2. FIPV recombinants, respectively.0.4-cm Gene Pulser cuvette (Biorad).Biorad Gene Pulser II apparatus with Capacitance extender plus.\n",
            "cite_spans": [],
            "section": "Infection and Electroporation ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nMaintain the LR7 and FCWF cells by regular passaging, using Trypsin EDTA solution when the cells approach confluency, to provide new maintenance cultures as well as cultures for the generation of the recipient virus stocks.A 1:15 split of LR7 cells and a 1:3 split of FCWF cells will usually provide cultures that approach confluence after 72 h.Wash LR7 or FCWF cells once with DMEM before inoculating with mFIPV or fMHV, respectively, in DMEM at a multiplicity of infection (MOI) of 0.05\u20130.1 TCID50 (50% tissue culture infectious doses) per cell (see\nNote 2).After 1 h incubation at 37\u00b0C, remove the inoculum and wash the cells once with DMEM, and then overlay them with DMEM++.Harvest the culture medium containing the viruses typically after 24 h incubation at 37\u00b0C, when the monolayers exhibit extensive cytopathic effects and detach from the plastic.Transfer the medium to 50-ml polypropylene tubes and clarify by centrifugation for 10 min at 2500 rpm at room temperature using a Labofuge GL (Heraeus) or a comparable centrifuge.Store the supernatant in aliquots at \u201380\u00b0C until use. Typical titers obtained for fMHV and mFIPV are around 106 TCID50/ml.\n",
            "cite_spans": [],
            "section": "Preparation of Recipient Virus Stocks ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nLinearize 10 \u03bcg of pMH54 or pBRDI1 with 30 U Pac I or Not I restriction enzyme, respectively, using the reaction buffers supplied by New England Biolabs in a reaction volume of 100 \u03bcl for 2 h at 37\u00b0C.Check the linearization of the plasmids by analyzing an aliquot of the reaction mixture using 1% (w/v) agarose gel electrophoresis in 0.5X TAE.Precipitate the linearized plasmid DNA by addition of one-tenth volume of 3 M ammonium acetate and 2.5 volumes of 96% (v/v) ethanol, followed by incubation at \u201320\u00b0C for 30 min.Pellet the DNA precipitates using an Eppendorf centrifuge (13,000 rpm, 15 min, 4\u00b0C).Wash the pellet with 70% (v/v) ethanol, dry it, and take it up in 10 \u03bcl of water (see\nNote 3).Transcribe synthetic RNA in a 20-\u03bcl reaction volume, using the T7 Message Machine kit (Ambion) according to the manufacturer\u2019s instructions, with the following modifications.\nInstead of 1 \u03bcg of DNA, 3 \u03bcl of the concentrated, linearized plasmid DNA is used.In addition, 3 \u03bcl of 30 mM GTP (present in the kit) is added, to enhance transcription of large RNAs (see\nNote 4).\nCheck RNA synthesis by analyzing one-tenth of the reaction volume by electrophoresis in a 1% (w/v) agarose gel in 1X TBE.Store the RNA at \u201380\u00b0C until use.\n",
            "cite_spans": [],
            "section": "Preparation of Synthetic Donor RNA ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nTypically, one 80-cm2 flask containing a culture of cells reaching confluence is sufficient for two RNA electroporations.Infect FCWF or LR7 cells with the recipient viruses fMHV or mFIPV, respectively, at an MOI of 0.5\u20131 TCID50/cell in DMEM++.After 4 h of incubation at 37\u00b0C, detach the cells from the plate using Trypsin EDTA solution (1.5 ml per 80 cm2).Add approximately 10 ml DMEM++ to the cell suspension.Pellet the cells by centrifugation in 50-ml polypropylene tubes for 10 min at 500 rpm at room temperature.Remove the supernatant and gently take up the pellet in DPBS without Ca and Mg.Pellet the cells by centrifugation in 50-ml polypropylene tubes for 10 min at 500 rpm at room temperature.Remove the supernatant and gently take up the cell pellet in DPBS without Ca and Mg (1 ml/80 cm2 original monolayer). Gently pipette the cell suspension carefully up and down until all cell clumps have disappeared.Transfer 800 \u03bcl of the cell suspension to a 0.4-cm Gene Pulser cuvette containing 9 \u03bcl of the synthetic donor RNA.Mix the cells and RNA by pipetting gently.Subject the RNA/cell mixture to two consecutive pulses using the Biorad Gene Pulser II with capacitance extender plus (0.3 kV, 950 \u03bcF).Add the electroporated cell suspension to 8 ml DMEM++ and mix gently.Transfer the DMEM++ to two 25-cm2 flasks containing subconfluent monolayer cultures of LR7 or FCWF cells, in order to allow propagation of the respective recombinant viruses.\n",
            "cite_spans": [],
            "section": "Infection and Electroporation ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Recombinant MHV or FIPV are selected by growth on LR7 or FCWF cells, respectively. After 24 h incubation of these cells\u2014overlaid with the infected/tra- nsfected cells\u2014at 37\u00b0C, cytopathic effects are visible when recombinant wild-type MHV or FIPV is generated. The appearance of cytopathic effects may take more time when preparing viruses with crippling mutations.\nWhen cytopathic effects are clearly visible, collect the culture medium and clarify by centrifugation as described above.Store the supernatants at \u201380\u00b0C until the recombinant viruses are purified by two consecutive plaque assays.To this end, autoclave a 3% (w/v) agar solution in water and cool it to 43\u00b0C.At the same time, warm the 2X EMEM concentrate containing FBS and p/s at 37\u00b0C.Inoculate cells reaching confluence (LR7 or FCWF cells for MHV or FIPV, respectively) with increasing dilutions in DMEM of the cleared cell culture supernatant.After 1 h at 37\u00b0C, remove the inoculum and overlay the cells with a 1:1 mixture of the agar solution and the 2X EMEM concentrate (2 ml/10 cm2 monolayer) (see\nNote 5).After 5 min incubation at room temperature, transfer the cells to 37\u00b0C. Typically, plaques are visible after a 24- h incubation.Pick plaques using disposable tips of a 200-\u03bcl pipette. First, remove the extreme end of the tip using scissors.Subsequently, push the tip through the agar overlay, at the position of a free plaque.Transfer the agar in the tip to 0.5 ml DMEM.Store this at \u201380\u00b0C until it is used in a second round of plaque purification.After the second round of plaque purification, generate a stock of recombinant virus by inoculating a 25-cm2 flask containing either LR7 or FCWF cells, using one-half of the purified plaque.Harvest stocks (designated as passage 1) when extensive cytopathic effects are visible.Clear the harvest by centrifugation and store in aliquots at \u201380\u00b0C.\n",
            "cite_spans": [],
            "section": "Selection and Purification of Recombinant Viruses ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nIn general, generate at least two independent recombinants to verify that the observed phenotypic characteristics are the result of the intended genomic modifications.Purify the genomic RNA from the passage 1 stock using the QIAamp Viral RNA mini kit (Qiagen).Genetically analyze the recombinant viruses by reverse transcription-PCR using genomic RNA as a template.Analyze the RT-PCR products using size or restriction fragment analysis and direct sequencing.\n",
            "cite_spans": [],
            "section": "Verification of Recombinant Phenotype ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nUnless otherwise stated, prepare all solutions in MilliQ water.Instead of LR7 cells, 17 Cl1, L2, DBT, or Sac(\u2013) cells can be used.Wear gloves and use RNase-free water and materials.Capped synthetic RNA transcripts are used. It is recommended that the synthetic RNA be used within 48 h.Mix by pipetting once up and down; then use immediately. If the temperature of the agar solution used is too high, the cells will be killed. If the temperature is not high enough, the solution will solidify prematurely.\n",
            "cite_spans": [],
            "section": "Notes",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 1.: Second round of homologous recombination to regenerate the original virus or to obtain mutants thereof. Donor DI RNA with the original wild-type S gene plus any additional intended mutations are introduced into cells that have been infected with the chimeric virus. Recombination with this RNA can lead to formation of (mutants of) the original virus. These are isolated by passing the collection of progeny viruses onto its natural cells.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Coronavirus reverse genetics by targeted RNA recombination",
            "authors": [
                {
                    "first": "P.",
                    "middle": [
                        "S."
                    ],
                    "last": "Masters",
                    "suffix": ""
                },
                {
                    "first": "P.",
                    "middle": [
                        "J."
                    ],
                    "last": "Rottier",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Curr. Top. Microbiol. Immunol",
            "volume": "287",
            "issn": "",
            "pages": "133-159",
            "other_ids": {
                "DOI": [
                    "10.1007/3-540-26765-4_5"
                ]
            }
        },
        "BIBREF1": {
            "title": "Repair and mutagenesis of the genome of a deletion mutant of the coronavirus mouse hepatitis virus by targeted RNA recombination.",
            "authors": [
                {
                    "first": "C.",
                    "middle": [
                        "A."
                    ],
                    "last": "Koetzner",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "J. Virol.",
            "volume": "66",
            "issn": "",
            "pages": "1841-1848",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Homologous RNA recombination allows efficient introduction of site-specific mutations into the genome of coronavirus MHV-A59 via synthetic co-replicating RNAs.",
            "authors": [
                {
                    "first": "R.",
                    "middle": [
                        "G."
                    ],
                    "last": "van der Most",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "Nucleic Acids Res.",
            "volume": "20",
            "issn": "",
            "pages": "3375-3381",
            "other_ids": {
                "DOI": [
                    "10.1093/nar/20.13.3375"
                ]
            }
        },
        "BIBREF3": {
            "title": "Optimization of targeted RNA recombination and mapping of a novel nucleocapsid gene mutation in the coronavirus mouse hepatitis virus.",
            "authors": [
                {
                    "first": "P.",
                    "middle": [
                        "S."
                    ],
                    "last": "Masters",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "J. Virol.",
            "volume": "68",
            "issn": "",
            "pages": "328-337",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Assembly of spikes into coronavirus particles is mediated by the carboxy-terminal domain of the spike protein",
            "authors": [
                {
                    "first": "G.",
                    "middle": [
                        "J."
                    ],
                    "last": "Godeke",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "J. Virol.",
            "volume": "74",
            "issn": "",
            "pages": "1566-1571",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.74.3.1566-1571.2000"
                ]
            }
        },
        "BIBREF5": {
            "title": "Retargeting of coronavirus by substitution of the spike glycoprotein ectodomain: crossing the host cell species barrier.",
            "authors": [
                {
                    "first": "L.",
                    "middle": [],
                    "last": "Kuo",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "J. Virol",
            "volume": "74",
            "issn": "",
            "pages": "1393-1406",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.74.3.1393-1406.2000"
                ]
            }
        },
        "BIBREF6": {
            "title": "Switching species tropism: an effective way to manipulate the feline coronavirus genome.",
            "authors": [
                {
                    "first": "B.",
                    "middle": [
                        "J."
                    ],
                    "last": "Haijema",
                    "suffix": ""
                },
                {
                    "first": "H.",
                    "middle": [],
                    "last": "Volders",
                    "suffix": ""
                },
                {
                    "first": "P.",
                    "middle": [
                        "J."
                    ],
                    "last": "Rottier",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J. Virol",
            "volume": "77",
            "issn": "",
            "pages": "4528-38",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.77.8.4528-4538.2003"
                ]
            }
        }
    }
}