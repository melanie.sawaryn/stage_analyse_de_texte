{
    "paper_id": "PMC7104423",
    "metadata": {
        "title": "Zo\u00f6nosen, voorkomen in Nederland en dreiging voor de toekomst",
        "authors": [
            {
                "first": "Jim",
                "middle": [],
                "last": "van Steenbergen",
                "suffix": "",
                "email": "redactieBijblijven@bsl.nl",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Zo\u00f6nosen zijn infectieziekten die worden overgedragen van dier op mens. Zo\u00f6nosen veroorzaken een breed scala aan ziekteverschijnselen. Op basis van de klachten in de spreekkamer zijn er voor een huisarts weinig aanknopingspunten om al dan niet aan een zo\u00f6nose te denken. Zonder de anamnese uit te diepen, kun je zo\u00f6nosen makkelijk over het hoofd zien. ",
            "cite_spans": [],
            "section": "Samenvatting",
            "ref_spans": []
        },
        {
            "text": "In deze bijdrage wordt het voorkomen van de verschillende zo\u00f6nosen beschreven met hun transmissieroutes en er wordt uitgelegd en ingegaan op hoe het beleid rond zo\u00f6nosen in Nederland is ingericht. Ten slotte gaat de auteur in op de betekenis van zo\u00f6nosen voor de huisarts.",
            "cite_spans": [],
            "section": "Samenvatting",
            "ref_spans": []
        },
        {
            "text": "Zo\u00f6nosen zijn infectieziekten die worden overgedragen van dier op mens. Zo\u00f6nosen veroorzaken een breed scala aan ziekteverschijnselen die op de gebruikelijke wijze in te delen zijn op basis van aangedane orgaansysteem: longen, maag- darm, urinewegen, huid, zenuwstelsel. De klachten door een zo\u00f6nose verschillen niet wezenlijk van de klachten door andere infecties in hetzelfde orgaansysteem die niet van dieren, maar van andere mensen afkomstig zijn. ",
            "cite_spans": [],
            "section": "Wat zijn zo\u00f6nosen?  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "Op basis van de klachten in de spreekkamer zijn er voor een huisarts weinig aanknopingspunten om al dan niet aan een zo\u00f6nose te denken: zelfs de kenmerkende langzaam uitbreidende rode ring met centrale opklaring, het erythema migrans van de ziekte van Lyme is soms niet veel meer dan een rode vlek. ",
            "cite_spans": [],
            "section": "Wat zijn zo\u00f6nosen?  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "De trigger om aan een zo\u00f6nose te denken moet daarom van de anamnese komen. Iemand met een infectieus ziektebeeld in de spreekkamer heeft altijd een context. De meest voorkomende infecties zijn door onze vele menselijke contacten, vooral overgedragen door andere mensen (bijvoorbeeld Pneumokokken, Haemophilus influenzae, Streptokokken, Stafylokokken, adenovirus, rhinovirus, oxyuren), maar zoals bij alle ziektebeelden zal een huisarts ook altijd rekening moeten houden met bijzondere omstandigheden, met ongewone blootstelling en bij infectieuze beelden dus met ongewone verwekkers zoals andere verwekkers in verre landen (reisgedrag: unde venis?, waar ben je geweest?). Verder ook met andere verwekkers door bijzondere arbeidsomstandigheden (waar werk je, wat voor werk doe je?), bijzonder voedsel (wat eet je of heb je gegeten?), bijzondere hobby's, vrijwilligerswerk en huisdieren (waar ga je mee om?). Zonder deze anamnese zo uit te diepen, kun je zo\u00f6nosen gemakkelijk over het hoofd zien. Is dat erg? Meestal niet, want de meeste ziektebeelden gaan vanzelf (en dus ook bij verkeerde medicatie) over. Soms is het uiteraard wel van belang: een ge\u00efnfecteerde muggenbeet vergt immers een andere behandeling dan ziekte van Lyme. ",
            "cite_spans": [],
            "section": "Wat zijn zo\u00f6nosen?  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "\nInfectie onderste luchtwegen (NHG-Standaard Acuut hoesten)\n",
            "cite_spans": [],
            "section": "Voorbeeld  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "Als een onderste luchtweginfectie gecompliceerd verloopt, wordt gesproken over pneumonie. Meestal zal een pneumonie veroorzaakt worden door S. pneumoniae, H. influenzae of een virus overgedragen door een ander mens. Een pneumonie kan ook veroorzaakt worden door C. psittaci of C. burnetii (resp. ornithose en Q-koorts), beide overgedragen door dieren, respectievelijk vogels en geiten. Op klinische gronden is geen onderscheid in verwekker te maken.1\n",
            "cite_spans": [],
            "section": "Voorbeeld  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "Voor een pneumonie geldt amoxicilline als middel van eerste keus. Voor psittacose en Q-koorts is dit doxycycline. Als de a-priori kans op psittacose (contact met vogels) of Q-koorts (contact met geiten) hoog is, verdient het dus voorkeur om te starten met doxycycline.",
            "cite_spans": [],
            "section": "Voorbeeld  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "In de zomermaanden blijkt dat meer dan twee derde van de pneumonie\u00ebn van mensen onder de 60 jaar die in het ziekenhuis belanden met een CAP (community-acquired pneumonia), veroorzaakt worden door atypische verwekkers waaronder Coxiella burnetii en Chlamydia psittacosis.2",
            "cite_spans": [],
            "section": "Voorbeeld  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "Voor een huisarts kan het dus belangrijk zijn aan de pati\u00ebnt te vragen welke bijzondere contacten de pati\u00ebnt in de periode voor aanvang van de klachten heeft gehad in verband met een mogelijke zo\u00f6nose (bijvoorbeeld door geiten of vogels) maar ook voor andere niet-zo\u00f6notische verwekkers (contacten in verre landen verhoogt de kans op antibioticumresistentie, vele hotelovernachtingen verhoogt kans op legionellose). Nog belangrijker is het de keuze voor een antibioticum mede te laten leiden door de ernst van het ziektebeeld.",
            "cite_spans": [],
            "section": "Voorbeeld  ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "Het is huisartsgeneeskundig gebruikelijk en verstandig in alle gevallen ook aan ongewone situaties te blijven denken, dus bij infectieuze aandoeningen ook aan zo\u00f6nosen. Dit kan van belang zijn vanwege de soms andere behandeling (Q-koorts, MRSA), het soms grotere gevaar op ernstig beloop (rabi\u00ebs, ebola), de mogelijkheid bronnen te vinden en deze uit te schakelen (Q-koorts) en daarmee blootstelling en ziekte bij anderen te voorkomen. Het is belangrijk niet alleen te denken aan een zo\u00f6nose, maar als er bijzondere omstandigheden zijn, ook te handelen door een mogelijke verwekker te zoeken en de pati\u00ebnt en situatie zonodig te melden aan de GGD-afdeling Infectieziekten. Zo\u00f6nosen zijn meestal niet de meest voorkomende mogelijkheid bij een ziektebeeld, maar toch ook niet altijd zeldzaam (rabi\u00ebs, ebola, SARS/MERS), want ook de hoogfrequente buikgriep in de zomer is meestal een zo\u00f6nose door besmet voedsel (campylobacter, salmonella: zie ook artikel Wildgerelateerde zo\u00f6nosen van Miriam Maas et al). Ook Lyme komt veel voor, meer dan 21.000 gevallen per jaar, waarvan drie kwart erythema migrans en 10% gedissemineerde infectie, de overige persisterende klachten na behandeling. De meeste overige zo\u00f6nosen ziet een huisarts zelden zoals leptospirose, hantavirusinfectie of psittacose (ornithose). ",
            "cite_spans": [],
            "section": "Moet huisarts dan toch iets weten over zo\u00f6nosen? ::: Inleiding",
            "ref_spans": []
        },
        {
            "text": "Het werkelijk aantal nieuwe zo\u00f6notische infecties per jaar is onbekend, omdat veel infecties zonder (ziekte)verschijnselen verlopen. Het jaarlijks aantal mensen met klachten is eveneens onbekend omdat er veel klachten zijn waarvoor mensen niet naar de (huis)arts gaan en die dus niet ergens geregistreerd worden. Zelfs de mensen die zich met een klacht tot een (huis)arts wenden, kunnen niet geteld worden omdat bij de meeste infectieuze ziektebeelden de microbiologische verwekker niet wordt gezocht en een microbiologische diagnose dus ook niet wordt gesteld (onderdiagnostiek). Veel van de vastgestelde zo\u00f6nosen behoeven niet gemeld te worden (bijvoorbeeld Lyme), en wat wel gemeld moet worden, wordt nog wel eens vergeten (onderrapportage). Verschillende onderzoekers doen hun best betrouwbare schattingen te maken over het werkelijk aantal zieken. Zo berekende het RIVM in de jaarlijks verschijnende Staat van zo\u00f6nosen dat zich in 2018, op basis van de 3091 in het laboratorium bevestigde gevallen van Campylobacteriose, 71.246 ziekte-episoden moeten hebben voorgedaan veroorzaakt door Campylobacter.",
            "cite_spans": [],
            "section": "Voorkomen in Nederland ",
            "ref_spans": []
        },
        {
            "text": "Zo\u00f6nosen (infectieziekten met dier als bron) hebben als subgroep van alle infectieziekten dezelfde overdrachtsmechanismen als alle anderen. Overdracht van infectieziekten is simpel: (1) vanuit een bron, (2) is er transmissie (3) naar een gastheer. Overigens is de omgekeerde route ook mogelijk waarbij onze verwekkers dieren ziek maken (bijvoorbeeld influenza van mens naar varken).",
            "cite_spans": [],
            "section": "Transmissieroutes",
            "ref_spans": []
        },
        {
            "text": "Deze Corona-virussen van vleermuizen zijn (voor zover bekend) nooit van vleermuis naar mens overgedragen. Maar na adaptatie aan de civetkat paste het SARS Co-virus wel bij de mens en kon daarna leiden tot een wereldwijde epidemie (pandemie) toen mens-op-mensoverdracht mogelijk was. MERS had de dromedaris als tussenstap nodig om bij de mens te komen. Overdracht van mens-op-mens is vastgesteld, maar voortgaande transmissie is (nog) niet mogelijk geworden.",
            "cite_spans": [],
            "section": "Voorbeeld SARS/MERS  ::: Bron",
            "ref_spans": []
        },
        {
            "text": "Dieren kunnen niet alleen bron, maar ook het reservoir zijn van de verwekker. Het reservoir is de gebruikelijke plek waar de verwekker leeft. SARS-CoV en MERS-CoV gedijen in vleermuizen.",
            "cite_spans": [],
            "section": "Reservoir ::: Bron",
            "ref_spans": []
        },
        {
            "text": "De verwekker van Lyme, Borrelia burgdorferi, leeft in knaagdieren en vogels. Pas door de beet van de larve van een teek kan de bacterie worden overgedragen op andere dieren, waaronder de mens. Als de ge\u00efnfecteerde tekenlarve volwassen is geworden raken ree\u00ebn, vossen en mensen besmet door de beet van een teek. Omdat het enkele uren duurt voordat de teek zich goed heeft aangehecht en pas daarna ge\u00efnfecteerd materiaal inspuit, heeft het zin teken snel te verwijderen: dat verlaagt de kans op infectie en dus de kans op de ziekte van Lyme.",
            "cite_spans": [],
            "section": "Voorbeeld Lyme ::: Bron",
            "ref_spans": []
        },
        {
            "text": "\nHuidschimmel\n",
            "cite_spans": [],
            "section": "Voorbeeld directe transmissie ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "Door intiem contact met een lievelingsdier kunnen dierlijke (zo\u00f6fiele) schimmels overgedragen worden van paard en hond op de aaiende mens (Trichophyton equinum, Microsporum canis) en zo tinea corporis veroorzaken (ringworm). Tinea capitis wordt overigens vrijwel altijd veroorzaakt door antropofiele dermatofyen, zoals Trichophyton tonsurans, en is dus geen zo\u00f6nose.",
            "cite_spans": [],
            "section": "Voorbeeld directe transmissie ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "\nQ-koorts\n",
            "cite_spans": [],
            "section": "Voorbeeld aerogene transmissie ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "In het artikel Parasitaire zo\u00f6nosen van Titia Kortbeek komt Q-koorts uitgebreid aan bod. De bacterie Coxiella burnetii gedijt in de placenta van kleine herkauwers, zoals geiten en schapen. Bij een abortus of voldragen geboorte komen grote hoeveelheden bacteri\u00ebn in de lucht vrij en kunnen directe omstanders besmetten (aerogene transmissie) en daarmee gold Q-koorts lange tijd als beroepsziekte van geiten- of schapenhouders, schapenscheerders en dierenartsen. ",
            "cite_spans": [],
            "section": "Voorbeeld aerogene transmissie ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "Helaas kan de bacterie langer in de lucht infectieus blijven en als er zeer grote aantallen in bacteri\u00ebn in de lucht vrijkomen (bij een zogenaamde abortusstorm in een geitenkudde) kunnen omwonenden tot op grote afstand ge\u00efnfecteerd raken. Theoretisch is ook indirecte overdracht door geitenkaas of -melk mogelijk, maar dit is Nederland niet overtuigend aangetoond, mede omdat de tankmelk van geiten altijd op Coxiella getest wordt. In andere landen spelen teken ook een rol in de (mechanische) overdracht van Coxiella.",
            "cite_spans": [],
            "section": "Voorbeeld aerogene transmissie ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "\nZwemmersjeuk\n",
            "cite_spans": [],
            "section": "Voorbeeld transmissie via biologische vector  ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "Zwemmersjeuk is in Nederland een veel voorkomende gezondheidsklacht na recreatie in zoet oppervlaktewater: een uur durende prikkelende sensatie tijdens het binnendringen van larven van vogelschistosomen enkele uren later gevolgd door heftig jeukende papels.4\n",
            "cite_spans": [],
            "section": "Voorbeeld transmissie via biologische vector  ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "De vogelschistosomen leven in de darmvaten van watervogels (en soms neusholte van eenden). De watervogels poepen de eieren uit en als die in water komen, verlaat het miracidium het ei en zoekt een geschikte zoetwaterslak op. Na een paar weken komen bij warm en zonnig weer duizenden vrijzwemmende larven vrij. Deze larven penetreren de intacte huid van de zwemmer, maar migreren verder niet in de mens (dead-end host, of 'incidentele gastheer', in tegenstelling tot bij de tropische tegenstelling tot de tropische schistosomiasis of bilharzia), maar migreren wel, na succesvollepenetratie van de huid, in een watervogel naar de darmvaten en maken zo de transmissiecyclus rond. Bij de mens verdwijnen na een eerste contact de symptomen binnen twaalf uur. Bij volgende contacten ontstaat bij iedere nieuwe penetratie een heftiger reactie. Symptomen kunnen dan lang, tot drie weken, aanhouden en gepaard gaan met hoofdpijn en koorts.",
            "cite_spans": [],
            "section": "Voorbeeld transmissie via biologische vector  ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "\nE. coli O157\n",
            "cite_spans": [],
            "section": "Voorbeeld indirecte transmissie via water en voedsel ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "E coli is een darmbacterie bij mens en dier. Sommige typen produceren toxinen waar mensen ziek van kunnen worden. Deze toxinen verschillen weinig van het shigella-toxine dat door Shigella wordt uitgescheiden en wat de bacteri\u00eble dysenterie veroorzaakt. Infectie met een bekende shiga-toxine-producerende E. coli (O157) kan milde tot ernstige bloederige diarree veroorzaken, maar is vooral bekend als veroorzaker van heamolytisch ureamisch syndroom (HUS). ",
            "cite_spans": [],
            "section": "Voorbeeld indirecte transmissie via water en voedsel ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "E coli O157 leeft in de darm van runderen en komt via koeienvlaaien op het land. Als het hard regent stroomt de E. coli naar de sloot of rivier. Zwemmers lopen de kans zo E. coli O157 binnen te krijgen en kinderen HUS te bezorgen: indirecte overdracht via water. ",
            "cite_spans": [],
            "section": "Voorbeeld indirecte transmissie via water en voedsel ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "Als tijdens de slacht van het rund per abuis darminhoud op andere eetbare delen (de spieren) terecht komt, kan vlees besmet raken en zo mensen ziek maken (Hamburger Disease): indirecte overdracht via voedsel.",
            "cite_spans": [],
            "section": "Voorbeeld indirecte transmissie via water en voedsel ::: Transmissie",
            "ref_spans": []
        },
        {
            "text": "Als het over ziekten bij de mens gaat, is de gastheer uiteraard altijd de mens. De plek waar de verwekker binnenkomt, is van belang voor de mogelijke verschijnselen en eventuele preventie. De conditie van de gastheer is eveneens van belang, niet alleen om de ernst van het beloop te begrijpen en in te schatten, maar ook om bescherming door immuniteit in te schatten, zoals verworven door eerdere infecties, maar bij voorkeur natuurlijk ook door vaccinatie. ",
            "cite_spans": [],
            "section": "De Gastheer",
            "ref_spans": []
        },
        {
            "text": "Nederland is een land waar zo\u00f6nosen gemakkelijk voor kunnen komen. Wij hebben veel mensen en dieren dicht op elkaar wonen (hoge bevolkingsdichtheid en hoge landbouwhuisdierendichtheid). In Nederland is het voller met dieren dan vrijwel overal elders in Europa. ",
            "cite_spans": [],
            "section": "Waarom zijn zo\u00f6nosen nog belangrijk in Nederland?",
            "ref_spans": []
        },
        {
            "text": "De absolute aantallen landbouwhuisdieren zijn indrukwekkend: 95 miljoen kippen, 6 miljoen varkens, 4 miljoen runderen (tabel 1). We slachten meer dan een half miljard kippen per jaar (tabel 2). ",
            "cite_spans": [],
            "section": "Waarom zijn zo\u00f6nosen nog belangrijk in Nederland?",
            "ref_spans": [
                {
                    "start": 119,
                    "end": 126,
                    "mention": "tabel 1",
                    "ref_id": null
                },
                {
                    "start": 184,
                    "end": 191,
                    "mention": "tabel 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Landbouwhuisdieren leven al duizenden jaren bij de mens en de door hen overgedragen zo\u00f6nosen zijn derhalve over het algemeen wel bekend. Nieuwe ziekten komen eerder uit bronnen van in het wild levende dieren. Nieuwe ziekten ontstaan niet alleen ver weg in landen met minder welvaart. Als we kijken naar weder opduikende zo\u00f6nosen en/of nieuwe zo\u00f6nosen zijn de Nederlandse omstandigheden daarvoor ook gunstig. Naast de miljoenen landbouwhuisdieren hebben we immers ook nog veel knaagdieren (muizen, ratten, woelmuizen, vleermuizen) die als (tijdelijke) gastheer voor verwekkers op kunnen treden en overdracht van dier naar mens kunnen bewerkstelligen (al dan niet via landbouwhuisdieren). ",
            "cite_spans": [],
            "section": "Waarom zijn zo\u00f6nosen nog belangrijk in Nederland?",
            "ref_spans": []
        },
        {
            "text": "Bekende verwekkers met nieuwe antibiotica-resistentiepatronen vergen nieuwe of tenminste andere therapie en scharen we vaak ook onder nieuwe infectieziekten. Vanwege het antibioticumgebruik in de veehouderij, de spill over van dit gebruik naar het milieu en de omvang van de veehouderij is deze subgroep van nieuwe infectieziekten voor Nederland ook zeer relevant. ",
            "cite_spans": [],
            "section": "Waarom zijn zo\u00f6nosen nog belangrijk in Nederland?",
            "ref_spans": []
        },
        {
            "text": "In 2008 verschenen de resultaten van een analyse van alle nieuwe infectieziekten in de wereld die in de periode 1940-2004 zijn ontdekt (n=335). Drie kwart waren zo\u00f6nosen. Op de wereldkaart waarop het ontstaan van de nieuwe infectieziekten is weergegeven, is Europa beslist geen witte vlek: alleen in de categorie vector-overdraagbare pathogenen staat Nederland niet in het rood (Kaart 1).",
            "cite_spans": [],
            "section": "Waarom zijn zo\u00f6nosen nog belangrijk in Nederland?",
            "ref_spans": []
        },
        {
            "text": "In 2010 heeft het RIVM de resultaten gepresenteerd van een grootschalig project waarin geprobeerd is een prioritering aan te brengen op welke zo\u00f6nosen Nederland zich goed zou moeten voorbereiden (RIVM/WUR/GD/UU EmZoo: Emerging zoonoses: early warning and surveillance in the Netherlands).7 Op basis van harde feiten (epidemiologie van bronnen, verwekkers, transmissie, gastheren) en kwalitatieve beoordeling door experts en door leken van de ernst van mogelijke situaties is voor 84 zo\u00f6nosen een ingewikkelde maar wetenschappelijk verantwoorde prioritering aangebracht. Het was geen poging te voorspellen welke zo\u00f6nosen binnenkort zullen opspelen, maar wel voor welke zo\u00f6nosen we diagnostiek, signalering en surveillance in ieder geval in orde zouden moeten hebben en welke diagnostiek we vooralsnog ook elders zouden kunnen laten doen. ",
            "cite_spans": [],
            "section": "Waarom zijn zo\u00f6nosen nog belangrijk in Nederland?",
            "ref_spans": []
        },
        {
            "text": "In de 70'er en 80'er jaren veroorzaakte Salmonella in Nederland meer dan 140.000 ziekte-episoden per jaar. Door een gemeenschappelijk hygi\u00ebneprogramma in de veehouderij- en voedselsector neemt het aantal infecties jaarlijks af en is sedert begin deze eeuw gehalveerd.8 Dankzij dit programma is de huidige (geschatte) ziekte-incidentie door salmonella bij 27.000 gekomen. ",
            "cite_spans": [],
            "section": "Voorbeeld salmonella-reductieprogramma. ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "Preventie kan wellicht beter. Vooral bij voedselproductie (de belangrijkste reden voor het houden van landbouwhuisdieren) wordt een uitgebalanceerde risicoanalyse van het gehele proces gemaakt en worden bij alle stappen maatregelen genomen om de risico's te verminderen of uit te sluiten (zie ook artikel Wildgerelateerde zo\u00f6nosen van Miriam Maas et al p. 54). ",
            "cite_spans": [],
            "section": "Voorbeeld salmonella-reductieprogramma. ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "Voor een deel zullen algemeen hygi\u00ebnische maatregelen op basis van een grondige risicoanalyse (hazard analysis critical control points of HACCP) ook nieuwe transmissie of introductie van nieuwe verwekkers beperken of voorkomen, maar echt nieuwe ziekten zijn onvoorspelbaar. ",
            "cite_spans": [],
            "section": "Voorbeeld salmonella-reductieprogramma. ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "In veel multidisciplinair onderzoek onder co\u00f6rdinatie van multilaterale organisaties, zoals de WHO (wereld gezondheidszorgorganisatie), FAO (wereld voedselorganisatie), OIE (wereld dierziekteorganisatie) proberen onderzoekers terugkijkend te zoeken of er voorspellende factoren zijn bij introductie van nieuwe zieken (drivers of change). Klimaatverandering en reizen spelen zonder twijfel een grote rol. Bij een retrospectief onderzoek van 116 Europese incidenten door infectieuze ziekten in de periode 2008-2013 bleek dat het vele en snelle reizen van mensen (inclusief toerisme), dieren en voedsel (handel), en voedsel- en waterkwaliteit de belangrijkste drivers of change waren.9 Pas daarna kwamen factoren zoals klimaatverandering, diergezondheid en migratie. Bij 27 van de incidenten was sprake van een door vectoren overgedragen infecties, waarvan het merendeel door West Nile Virus veroorzaakt was.",
            "cite_spans": [],
            "section": "Voorbeeld salmonella-reductieprogramma. ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "Zoals de naam doet vermoeden komt dit virus voor in het stroomgebied van de Nijl. Het virus wordt door gewone steekmuggen tussen vogels en van vogels naar mensen of paarden overgedragen. Daarna is ook iatrogene mens-op-mensoverdracht mogelijk door bloedtransfusie en transplantatie. Paarden kunnen ziek worden en kunnen zo fungeren als kanarie in de kolenmijn (verklikker).",
            "cite_spans": [],
            "section": "Voorbeeld westnijlvirus15 ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "De meeste infecties bij de mens verlopen zonder symptomen (80%) of met koorts (20%). Ernstige ziektebeelden ontstaan in 1% van de infecties door meningitis en meningo-encephalitis, wat vooral gezien wordt bij mensen boven de 50 jaar en mensen met een immuunstoornis. ",
            "cite_spans": [],
            "section": "Voorbeeld westnijlvirus15 ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "In de maanden dat muggen actief zijn, komt westnijlvirus in Europa voor in landen rond de Middellandse Zee (juni-november). Voor het handhaven van de transmissiecyclus moeten er genoeg muggen zijn, de buitentemperatuur hoog genoeg zijn om in \u00e9\u00e9n (kort) muggenleven genoeg virus op te zuigen bij het brondier en het virus de gelegenheid te geven te migreren naar de speekselklieren en dan weer een nieuwe gastheer te steken. Dat zijn tot op heden nog uitzonderlijke omstandigheden, maar ze komen steeds vaker voor.",
            "cite_spans": [],
            "section": "Voorbeeld westnijlvirus15 ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "In 1996 is de eerste grote epidemie van westnijlvirus in Europa gesignaleerd (in Roemeni\u00eb), vermoedelijk ge\u00efntroduceerd door overvliegende trekvogels. In de lange warme zomer waren er door ernstige verwaarlozing van muggenbestrijding genoeg muggen om 393 bevestigde ziektegevallen te veroorzaken, waarvan 17 mensen overleden.10 Sedertdien wordt westnijlvirus vaker in Europa gezien. In 2018 zijn de meeste gevallen van westnijlkoorts in Europa gerapporteerd sinds de start van Europese surveillance.11 We zullen ook in Nederland met westnijlvirus te maken krijgen, maar wanneer is moeilijk te voorspellen.",
            "cite_spans": [],
            "section": "Voorbeeld westnijlvirus15 ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "De toegenomen verspreiding kan samenhangen met klimaatverandering, maar dat is nog niet bewezen. Het klassieke ziektebeeld 'koorts terug uit de tropen' is aan herziening toe: koorts na een Europese reis kan ook gevaarlijk zijn. ",
            "cite_spans": [],
            "section": "Voorbeeld westnijlvirus15 ::: Preventie en bewaking ",
            "ref_spans": []
        },
        {
            "text": "In Nederland is in 2019 door NWO geld vrij gemaakt voor innovatief multidisciplinair onderzoek om kennis te verzamelen waarmee nieuwe uitbraken van vector-overdraagbare ziekten voorspeld kunnen worden. Samenwerking in onderzoek en bestrijding op dit gebied wordt tegenwoordig vervat in de term One health: dieren, mens en milieu zitten samen in \u00e9\u00e9n ecosysteem en moeten zo gezond mogelijk blijven en vergt dus samenwerking van meerdere vakgebieden. Het NWO-onderzoek staat dan ook onder co\u00f6rdinatie van het Nederlands Centrum voor One Health (NCOH, wat weer een samenwerkingsverband is van negen universitaire onderzoeksinstituten). De wens is om uit alle mogelijke be\u00efnvloedende factoren een model te maken dat op basis van veranderende omstandigheden een nieuwe ziekte aangekondigd kan worden. ",
            "cite_spans": [],
            "section": "Surveillance: een rol voor de huisarts",
            "ref_spans": []
        },
        {
            "text": "Omdat voorspellen vooralsnog onmogelijk is, zijn we in een land met re\u00ebel risico op het ontstaan of introductie van nieuwe ziekten afhankelijk van goed opletten (waakzaamheid, vigilance) en zowel clusters van ziekten vroeg onderkennen en onderzoeken (early warning), als ook bij ongewone individuele (ernstige) ziektebeelden steeds goed te zoeken naar (mogelijke) verwekkers. Dat is ook de reden dat in de meldingsplicht voor (huis)artsen is opgenomen de GGD te bellen 'als u te maken hebt met een ziektebeeld met een volgens de stand van de wetenschap onbekende oorzaak, waarbij gegrond vermoeden bestaat van besmettelijkheid en ernstig gevaar voor de volksgezondheid'.12 Daarnaast wordt van de (huis)arts ook verwacht te melden als hij of zij te maken heeft met een ongewoon aantal pati\u00ebnten met een infectieziekte die niet vermeld staat in de wet. Het is een ruime formulering, waarvan de meeste artsen zich vermoedelijk niet bewust zijn. Van alle artsen wordt gevraagd goed op te letten op mogelijke 'clusters' en zo nieuwe en wederopduikende infecties snel op het spoor te komen en snel maatregelen tegen verspreiding te kunnen nemen. ",
            "cite_spans": [],
            "section": "Surveillance: een rol voor de huisarts",
            "ref_spans": []
        },
        {
            "text": "Bijzondere clusters moet een (huis)arts melden, maar ook directeuren van instellingen waar kwetsbare personen verblijven worden geacht ongewone clusters te melden: Heeft u in uw instelling te maken met een ongewoon aantal zieken met maag- en darmaandoeningen, geelzucht huidaandoeningen; of heeft u in uw instelling te maken met een andere ernstige aandoening van vermoedelijk infectieuze aard? Vermoedelijk en hopelijk zal de directeur bij een ongewoon aantal zieken eerst een arts raadplegen en dan is het goed de directeur er aan te herinneren dat melden aan de GGD verstandig, gewenst en zelfs verplicht is. ",
            "cite_spans": [],
            "section": "Surveillance: een rol voor de huisarts",
            "ref_spans": []
        },
        {
            "text": "De afdeling infectieziektebestrijding van de GGD zal bij dergelijke meldingen adequate microbiologische en epidemiologische diagnostiek inzetten op zoek naar (nieuwe) bronnen of verwekkers. ",
            "cite_spans": [],
            "section": "Surveillance: een rol voor de huisarts",
            "ref_spans": []
        },
        {
            "text": "Het Centrum Monitoring Vectoren is onderdeel van de Nederlandse Voedsel en Warenautoriteit (NVWA) waar enthousiaste entomologen de verspreiding van (mogelijke) vectoren in de gaten houden. Omdat bekend is dat uit het buitenland ingevoerde oude rubberbanden larven van exotische muggen kunnen bevatten heeft het CMV muggenvallen bij tweedehands bandenbedrijven geplaatst. Iedere twee weken wordt gekeken wat voor soort muggen er in de val terecht zijn gekomen. Als er ongewenste muggen zijn gevonden (zoals al meerdere keren is gebeurd met Aedes albopictus en Aedes egyptii) worden op het bedrijf maatregelen genomen (larvicide middelen verspreid) en in ruimere omgeving naar muggen gezocht. Zonodig, wordt dan ook buiten het bedrijf aan muggenbestrijding gedaan.",
            "cite_spans": [],
            "section": "Voorbeeld Centrum Monitoring Vectoren ::: Op de achtergrond",
            "ref_spans": []
        },
        {
            "text": "Na de gebrekkige samenwerking bij de bestrijding van Q-koorts is er in Nederland een sterk samenwerkingsverband opgericht van instellingen en hun professionals dat er op gericht is nieuwe gebeurtenissen op het gebied van zo\u00f6nosen zo snel mogelijk adequaat aan te pakken en de juiste maatregelen snel te nemen, de zo\u00f6nosenstructuur.13 In een Vademecum Zo\u00f6nosen staat beschreven hoe de samenwerking en de verantwoordelijkheden zijn afgesproken.14 Deelnemers zijn de Gemeenschappelijke Gezondheidsdiensten (GGD'en), de Gezondheidsdienst voor Dieren (GDDeventer), de Nederlandse Voedsel- en Warenautoriteit (NVWA), het Centrum Infectieziektebestrijding van het Rijksinstituut voor Volksgezondheid en het Milieu (CIb/RIVM), het Centraal Veteriniair Instituut (CVI Lelystad, nu onderdeel van Wageningen Bioveterinary Institute Universiteit en Research), Faculteit Diergeneeskunde van de Universiteit Utrecht (FD-UU) en Dutch Wildlife Health Centre (DWHC ondergebracht bij de FD-UU). De afspraken zijn er vooral op gericht om zo snel mogelijk informatie uit te wisselen en wederzijdse deskundigheid in te schakelen. In deze structuur is ook de verantwoordelijkheidstoedeling en besluitvorming op 'hoger niveau' (ministerieel) vastgelegd.",
            "cite_spans": [],
            "section": "Voorbeeld 'zo\u00f6nosestructuur' ::: Op de achtergrond",
            "ref_spans": []
        },
        {
            "text": "Als een huisarts op tijd denkt aan de mogelijkheid van een zo\u00f6nose kan dat belangrijk zijn voor de pati\u00ebnt. Na microbiologische diagnostiek kan dan gepaste medicatie worden voorgeschreven. Voor de bescherming van anderen (volksgezondheid) is het prettig om op tijd de GGD in te schakelen. Een telefoontje naar de afdeling infectieziektebestrijding is genoeg. Het heeft zin om bij te blijven voor wat betreft nieuwe ontwikkelingen, niet alleen in diagnostiek en therapie, maar ook wat er om ons heen gebeurt (ecologie) om te weten welke dreigingen er zijn voor het geval de eerste pati\u00ebnt met iets nieuws in Nederland in de eigen spreekkamer zit. ",
            "cite_spans": [],
            "section": "De huisarts als belangrijke schakel ::: Op de achtergrond",
            "ref_spans": []
        },
        {
            "text": "\nhttps://www.nhg.org/standaarden/volledig/nhg-standaard-acuut-hoesten#idp87392.Raeven et al. BMC Infectious Diseases (2016) 16:299.Infectieziektebestrijding. Hans van den Kerkhof, Jim van Steenbergen red. Boom Lemma Uitgevers, Den Haag, 2013 ISBN 978-90-5931-637-9.https://lci.rivm.nl/sites/default/files/201706/Bijlage%20I%20Cercari%C3%ABn%20dermatitis.pdf.https://ec.europa.eu/eurostat/statisticsexplained/images/9/9e/Livestock_density_at_regional_ level%2C_2007.png.Kate Jones et. Al. Global trends in Emerging Diseases Nature 2008; 451: 990-4.https://www.rivm.nl/bibliotheek/rapporten/330214002.pdf.https://www.rivm.nl/trends-in-salmonella-bij-mens-landbouwhuisdieren-en-in-voedsel.Semenza JC, Lindgren E, Balkanyi L, Espinosa L, Almqvist MS, Penttinen P, Rockl\u00f6v J. Determinants and Drivers of Infectious Disease Threat Events in Europe. Emerg Infect Dis. 2016 Apr;22(4):581-9. doi: 10.3201/eid2204. PubMed PMID: 26982104; PubMed Central PMCID: PMC4806948.Tsai TF, Popovici F, Cernescu C, Campbell GL, Nedelcu NI. West Nile encephalitis epidemic in southeastern Romania. Lancet. 1998;352:767-71.https://ecdc.europa.eu/en/publications-data/west-nile-virus-infections-affected-areas-eueea-member-states-and-eu-neighbouring.https://www.rivm.nl/sites/default/files/2019-06/011440_FS%20Meldingsplichtige%20ziekten_V1_TG%20%28mei%202019%29.pdf.Nationale evaluatiecommissie Q-koorts. Van verwerping tot verheffing. Uitgave Evaluatiecommissie Q-koorts, Den Haag, november 2010.https://www.rivm.nl/documenten/vademecum-Zo\u00f6nosen-2014.Bron LCI richtlijn: https://lci.rivm.nl/richtlijnen/westnilevirusinfectie). \n",
            "cite_spans": [],
            "section": "Referenties",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {}
}