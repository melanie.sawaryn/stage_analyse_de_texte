{
    "paper_id": "PMC7107609",
    "metadata": {
        "title": "Rash and Elevated Creatine Kinase in a Deployed Soldier",
        "authors": [
            {
                "first": "Thomas",
                "middle": [
                    "W"
                ],
                "last": "Schmidt",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mark",
                "middle": [],
                "last": "Garfinkle",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Daniel",
                "middle": [
                    "F"
                ],
                "last": "Battafarano",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "A 24-year-old active duty African-American male was admitted to the hospital with myalgias, weakness, and rash. The patient presented 1 month before to a clinic while deployed in Afghanistan with fevers, sore throat, and odynophagia. Initial laboratory screenings revealed a negative monospot and negative throat cultures. He was treated for a viral pharyngitis, but the sore throat worsened with swelling at the base of his tongue. There was concern for Ludwig's angina, and he was treated with ampicillin 3 g IV every 6 hours. Despite 5 days of antibiotic therapy his symptoms persisted. Subsequently, his antibiotic regimen was switched to clindamycin 600 mg IV, and he was evacuated to the United States. Computed tomography of the neck showed diffuse lymphadenopathy without evidence of an abscess. Upon arrival to the United States, soft-tissue infection surrounding the neck was still suspected, and he was treated with trimethoprim\u2013sulfamethoxazole (TMP-SMX). Four days later, he developed a rash around his neck and chest with severe myalgias. Laboratory data were significant for creatine kinase (CK) of 22,717 U/L (normal range: 24\u2013170 U/L), normal urinalysis, creatinine of 1.6 mg/dL (baseline 1.0 mg/dL), aspartate aminotransferase (AST) elevated to 684 U/L (normal range: 4\u201340 U/L), and alanine aminotransferase (ALT) of 116 U/L (4\u201341 U/L); complete blood count (CBC) was significant for an elevated white blood cells (WBC) of 11.6 \u00d7 103/\u03bcL with a neutrophil predominance (85.1%), lymphocytes 5%, eosinophils 4%, monocytes 5%, hemoglobin of 16.3 g/dL, and platelets of 247 \u00d7 103/\u03bcL. Patient was diagnosed with rhabdomyolysis and possible drug reaction. He received aggressive intravenous fluids and TMP-SMX was discontinued. The creatinine level returned to baseline and the CK downtrended below 10,000 U/L, so he was discharged home. One week later, the patient had persistent muscle pain and weakness with a rash. He was transferred to San Antonio Military Medical Center for possible dermatomyositis (DM).",
            "cite_spans": [],
            "section": "CASE REPORT",
            "ref_spans": []
        },
        {
            "text": "Before his systemic illness, the patient had no medical problems and did not take any medications. He had no family history of rheumatic, autoimmune, or hereditary diseases.",
            "cite_spans": [],
            "section": "CASE REPORT",
            "ref_spans": []
        },
        {
            "text": "Upon arrival to our facility, his temperature was 98.3 F, heart rate was 90 beats per minute, blood pressure was 133/79 mmHg, and his oxygen saturation was 95% on room air. The patient had a muscular physique, but he was in moderate distress lying in bed. Physical examination showed normal breath sounds, normal cardiac exam, and a normal abdominal exam. His oropharynx was clear, and he had non-tender lymphadenopathy along his cervical chain. His skin showed an exfoliative rash over chest and back that spared mucous membranes, face, and hands. Musculoskeletal examination was remarkable for diffuse muscle tenderness without arthritis. Appearance of the patient's shoulders and thighs was consistent with muscle edema. His neurological examination was significant for 4 out of 5 strength in shoulders and thighs, and the patient had difficulty in standing and walking secondary to pain. He had normal reflexes and sensation was intact.",
            "cite_spans": [],
            "section": "CASE REPORT",
            "ref_spans": []
        },
        {
            "text": "Laboratory results revealed WBC of 8,000 \u00d7 103/\u03bcL with neutrophil predominance (73%), lymphocytes 15.4%, eosinophils 1.7%, monocytes 9.4%, hemoglobin of 11.7 g/dL, mean corpuscular volume 92.3 fL, and platelets of 350 \u00d7 103/\u03bcL. Renal function panel was normal (creatinine 1.0 mg/dL). AST and ALT remained elevated at 273 U/L and 107 U/L, and his CK was 6,700 U/L with aldolase elevated at 34.4 U/L (normal range 0.5\u20138.5 U/L). Thyroid stimulating hormone and free T4 were normal (0.96 \u03bcIU/mL and 1.25 ng/dL). All blood cultures were negative, and rapid strep testing was negative as was the throat culture. Heterophile antibody was negative, but EBV IgM and IgG titers were positive. The patient had a negative ANA, and the antibodies to SSA, SSB, Smith, JO-1, PL-7, PL-12, Mi-2, KU, EJ, OJ and signal recognition protein were negative. He had negative serologies for human immunodeficiency virus (HIV); influenza A and B; respiratory syncytial virus; rhinovirus; adenovirus; cytomegalovirus; echovirus, coxsackie A and B; hepatitis A, B, and C; mycoplasma; and a negative rapid plasma reagin. T2-weighted magnetic resonance imaging of his thighs showed diffuse intramuscular edema and enhancement, compatible with myositis. A skin biopsy was most consistent with erythema multiforme. The muscle biopsy was normal without evidence of inflammatory infiltrates or immune complex deposition in vessels.",
            "cite_spans": [],
            "section": "CASE REPORT",
            "ref_spans": []
        },
        {
            "text": "During hospitalization, the patient was treated with 1,000 mg IV Solu-Medrol for 3 days for presumed inflammatory myositis with transition to 1 mg/kg oral prednisone with taper after biopsy results returned. His weakness and pain improved, and he was discharged home with the diagnosis of acute mononucleosis with possible TMP-SMX drug reaction. On outpatient follow-up, the rash and weakness resolved and the CK was normalized to 160 U/L.",
            "cite_spans": [],
            "section": "CASE REPORT",
            "ref_spans": []
        },
        {
            "text": "DM should be considered in a patient presenting with a characteristic rash, proximal muscle weakness, and elevated CK. Although this patient had an elevated CK, he did not have the typical rash of DM, had muscle pain greater than weakness, and had a normal muscle biopsy. Given his dramatic muscle pain, infection and drug reaction likely played a role in this clinical presentation.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "DM is one of the idiopathic inflammatory myopathies (IIM), and classically presents with elevated serum muscle enzymes, proximal muscle weakness, and a characteristic rash. Muscle weakness is typically progressive and symmetrical with mild pain. Rashes highly specific for DM include the heliotrope rash, the shawl sign, and Gottron's sign.1,2 Skin involvement often includes sun-exposed areas of the face, scalp, neck, upper back, and extensor surfaces. Other skin lesions include periungual telangiectasias, alopecia, vesiculobullous lesions, panniculitis, urticaria, and mechanic's hands. A classic presentation may not require further testing; however, electromyography and muscle biopsy may assist in diagnosis.1 Muscle biopsy for DM classically includes a perivascular infiltrate consisting predominantly of CD4+ T lymphocytes. Muscle involvement may be patchy and a magnetic resonance imaging can guide an optimal biopsy site. During the workup for an IIM, a broad differential including metabolic myopathies (acquired and genetic), motor neuron diseases, muscular dystrophies, and exposures to medications or infections should be considered. When weakness is accompanied by severe muscle pain, this is often suggestive of an infectious or a drug-related process.1",
            "cite_spans": [
                {
                    "start": 340,
                    "end": 341,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 342,
                    "end": 343,
                    "mention": "2",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 716,
                    "end": 717,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1270,
                    "end": 1271,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Infections associated with myositis include viruses, bacteria, parasites, and fungi (Table I). Viruses and parasites commonly present with diffuse myositis although bacteria are more often associated with focal muscle symptoms. Fungal muscle invasion is uncommon and described in case reports. Parasitic infections are rare in the United States, but should be considered in a returning traveler. Viruses are the most common cause of myositis. Viral myositis should be considered when a patient presents with a typical viral prodrome of fevers, headache, sore throat, or rhinorrhea. The myalgias are often diffuse and may be a presenting symptom.3 Influenza is the most common viral cause. Other viruses frequently associated with myositis include enterovirus, HIV, and hepatitis B and C.3",
            "cite_spans": [
                {
                    "start": 645,
                    "end": 646,
                    "mention": "3",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 787,
                    "end": 788,
                    "mention": "3",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": [
                {
                    "start": 85,
                    "end": 92,
                    "mention": "Table I",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "EBV infection mimicking myositis is rare and when reported has been associated with chronic active EBV (CAEBV) infection.4 Uchiyama et al4 presented a case series of CAEBV patients with weakness of the extremities and elevated CKs. Six of the eight CAEBV patients also developed lingual or orbital muscle involvement, which is rare in IIM and may help in distinguishing the two diagnoses. Acute EBV infection associated with elevated CKs and muscle pain has been described in cases of rhabdomyolysis,5\u20138 but is uncommon. Myoglobinuria and elevated CK are the hallmarks of rhabdomyolysis, and in the reported cases of EBV-associated rhabdomyolysis both were documented.",
            "cite_spans": [
                {
                    "start": 121,
                    "end": 122,
                    "mention": "4",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 137,
                    "end": 138,
                    "mention": "4",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 500,
                    "end": 501,
                    "mention": "5",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 502,
                    "end": 503,
                    "mention": "8",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Medications may be associated with musculoskeletal complaints ranging from mild myalgias to severe myositis or rhabdomyolysis.9,10 Lipid-lowering medications, such as statins and fibrates, are often associated with myopathies, in part because of the high frequency in which they are prescribed. Rheumatological medications that cause myositis or myopathy include antitumor necrosis factor therapy, corticosteroids, colchicine, and chloroquines. Other classes of medications implicated include antiarrhythmic and chemotherapeutic agents, psychiatric medications, antiviral agents, and antibiotics including antimalarial medications that are often prescribed to deployed soldiers10 (Table II). Realistically, all medications should be considered if the onset of symptoms is temporally related to initiation of a medication.",
            "cite_spans": [
                {
                    "start": 126,
                    "end": 127,
                    "mention": "9",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 128,
                    "end": 130,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 677,
                    "end": 679,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": [
                {
                    "start": 681,
                    "end": 689,
                    "mention": "Table II",
                    "ref_id": "TABREF1"
                }
            ]
        },
        {
            "text": "TMP-SMX is commonly prescribed, but there are only a few reported cases of rhabdomyolysis associated with its use. When TMP-SMX has been linked with rhabdomyolysis, it has been in patients who are immunocompromised and most commonly have HIV or AIDS.11\u201313 The onset of symptoms can present up to 14 days after initiation. Muscle pain and elevated CKs are often accompanied by fevers and rash. Symptoms resolve after withdrawal of TMP-SMX.12",
            "cite_spans": [
                {
                    "start": 250,
                    "end": 252,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 253,
                    "end": 255,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 438,
                    "end": 440,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "In this case, it is plausible that an acute viral infection and administration of TMP-SMX resulted in myositis. Drug reactions associated with administration of amoxicillin in patients with acute EBV infection are well established, and underlying viral infections are associated with an increased risk of drug reactions.14,15 A possible mechanism of TMP-SMX hypersensitivity in HIV patients was recently described by a polymorphism in glutamate cysteine ligase catalytic subunit.16 A similar mechanism could be responsible for the findings in our patient.",
            "cite_spans": [
                {
                    "start": 320,
                    "end": 322,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 323,
                    "end": 325,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 479,
                    "end": 481,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "We described an active duty male that developed an upper respiratory tract infection while deployed to Afghanistan. His course was complicated by an exfoliative rash, diffuse myalgias, and elevated CK. The etiopathogenesis for this syndrome with EBV infection and TMP-SMX exposure is unclear. This case highlights the extensive differential diagnosis for a rash, inflammatory muscle disease, and an elevated CK in a deployed soldier.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "TABLE I: Infections Associated With Myositis1,3\n",
            "type": "table"
        },
        "TABREF1": {
            "text": "TABLE II: Medications Associated With Myositis1,10\n",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Classification, diagnosis, and management of idiopathic inflammatory myopathies",
            "authors": [
                {
                    "first": "IN",
                    "middle": [],
                    "last": "Lazarou",
                    "suffix": ""
                },
                {
                    "first": "PA",
                    "middle": [],
                    "last": "Guerne",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Rheumatol",
            "volume": "40",
            "issn": "5",
            "pages": "550-64",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Drugs causing muscle disease",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mor",
                    "suffix": ""
                },
                {
                    "first": "RL",
                    "middle": [],
                    "last": "Wortmann",
                    "suffix": ""
                },
                {
                    "first": "HJ",
                    "middle": [],
                    "last": "Mitnick",
                    "suffix": ""
                },
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Pillinger",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Rheum Dis Clin North Am",
            "volume": "37",
            "issn": "2",
            "pages": "219-31",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Trimethoprim\u2013sulfamethoxazole associated rhabdomyolysis in a patient with AIDS: case report and review of the literature",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Walker",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Norwood",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Thornton",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Schaberg",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Am J Med Sci",
            "volume": "331",
            "issn": "6",
            "pages": "339-41",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Trimethoprim\u2013sulphamethoxazole-associated rhabdomyolysis in an HIV-infected patient",
            "authors": [
                {
                    "first": "SP",
                    "middle": [],
                    "last": "Jen",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Sharma",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Int J STD AIDS",
            "volume": "22",
            "issn": "7",
            "pages": "411-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Trimethoprim\u2013sulfamethoxazole-induced rhabdomyolysis in an allogeneic stem cell transplant patient",
            "authors": [
                {
                    "first": "PJ",
                    "middle": [],
                    "last": "Kiel",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Dickmeyer",
                    "suffix": ""
                },
                {
                    "first": "JE",
                    "middle": [],
                    "last": "Schwartz",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Transpl Infect Dis",
            "volume": "12",
            "issn": "5",
            "pages": "451-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Amoxicillin-induced exanthema in young adults with infectious mononucleosis: demonstration of drug-specific lymphocyte reactivity",
            "authors": [
                {
                    "first": "CN",
                    "middle": [],
                    "last": "Renn",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Straff",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Dorfmuller",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Al-Masaoudi",
                    "suffix": ""
                },
                {
                    "first": "HF",
                    "middle": [],
                    "last": "Merk",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Sachs",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Br J Dermatol",
            "volume": "147",
            "issn": "6",
            "pages": "1166-70",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Incidence of rash after amoxicillin treatment in children with infectious mononucleosis",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Chovel-Sella",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ben Tov",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Lahav",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Pediatrics",
            "volume": "131",
            "issn": "5",
            "pages": "e1424-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Polymorphism in glutamate cysteine ligase catalytic subunit (GCLC) is associated with sulfamethoxazole-induced hypersensitivity in HIV/AIDS patients",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Curtis",
                    "suffix": ""
                },
                {
                    "first": "AC",
                    "middle": [],
                    "last": "Papp",
                    "suffix": ""
                },
                {
                    "first": "SL",
                    "middle": [],
                    "last": "Koletar",
                    "suffix": ""
                },
                {
                    "first": "MF",
                    "middle": [],
                    "last": "Para",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "BMC Med Genomics",
            "volume": "5",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Dermatomyositis",
            "authors": [
                {
                    "first": "JP",
                    "middle": [],
                    "last": "Callen",
                    "suffix": ""
                },
                {
                    "first": "RL",
                    "middle": [],
                    "last": "Wortmann",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Clin Dermatol",
            "volume": "24",
            "issn": "5",
            "pages": "363-73",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Bacterial, fungal, parasitic, and viral myositis",
            "authors": [
                {
                    "first": "NF",
                    "middle": [],
                    "last": "Crum-Cianflone",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Clin Microbiol Rev",
            "volume": "21",
            "issn": "3",
            "pages": "473-94",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Generalized myositis mimicking polymyositis associated with chronic active Epstein\u2013Barr virus infection",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Uchiyama",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Arai",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Yamamoto-Tabata",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Neurol",
            "volume": "252",
            "issn": "5",
            "pages": "519-25",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Rhabdomyolysis complicating acute Epstein\u2013Barr virus infection",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Osamah",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Finkelstein",
                    "suffix": ""
                },
                {
                    "first": "JG",
                    "middle": [],
                    "last": "Brook",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Infection",
            "volume": "23",
            "issn": "2",
            "pages": "119-20",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "A rare cause of rhabdomyolysis",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Roychowdhury",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Shivakumar",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Mustafa",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Patil",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Salahuddin",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "South Med J",
            "volume": "100",
            "issn": "3",
            "pages": "333-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Unusual causes of rhabdomyolysis",
            "authors": [
                {
                    "first": "EE",
                    "middle": [],
                    "last": "Mazokopakis",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Intern Med J",
            "volume": "38",
            "issn": "5",
            "pages": "364-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Acute renal failure and rhabdomyolysis in a patient with infectious mononucleosis: a case report",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Aloizos",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gourgiotis",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Oikonomou",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Stakia",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Cases J",
            "volume": "1",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Drug-induced arthritic and connective tissue disorders",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mor",
                    "suffix": ""
                },
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Pillinger",
                    "suffix": ""
                },
                {
                    "first": "RL",
                    "middle": [],
                    "last": "Wortmann",
                    "suffix": ""
                },
                {
                    "first": "HJ",
                    "middle": [],
                    "last": "Mitnick",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Semin Arthritis Rheum",
            "volume": "38",
            "issn": "3",
            "pages": "249-64",
            "other_ids": {
                "DOI": []
            }
        }
    }
}