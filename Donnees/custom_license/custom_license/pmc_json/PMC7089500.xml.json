{
    "paper_id": "PMC7089500",
    "metadata": {
        "title": "The effect of porcine epidemic diarrhea (PED) on ovarian function and reproductive performance after weaning in Berkshire sows",
        "authors": [
            {
                "first": "Yosuke",
                "middle": [],
                "last": "Sasaki",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tadahiro",
                "middle": [],
                "last": "Kawabata",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Michiko",
                "middle": [],
                "last": "Noguchi",
                "suffix": "",
                "email": "m-noguchi@azabu-u.ac.jp",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Porcine epidemic diarrhea (PED) is caused by a porcine coronavirus in the Coronaviridae family, a group of single-stranded, positive-sense RNA viruses (Saif et al. 2012). A global outbreak of PED began in 2013, and the disease has been reported in many countries around world. In Japan, the first case of PED to occur in Japan in 7 years was reported in October 2013 and PED then spread across the country. During the acute phase of a PED outbreak, the mortality rate of infected piglets is very high and may reach 100%. Reduction and/or loss of suckling stimulation from piglets during lactation strongly influence sow ovarian follicular development, which is associated with changes in reproductive hormone secretion (Langendijk et al. 2007). There has been only one published report investigating the relationship between PED and reproductive performance (Olanratmanee et al. 2010), and to our knowledge there have been no other investigations into the influence of piglet loss due to PED on the ovaries of the sow at weaning and her subsequent reproductive performance.",
            "cite_spans": [
                {
                    "start": 164,
                    "end": 168,
                    "mention": "2012",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 738,
                    "end": 742,
                    "mention": "2007",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 879,
                    "end": 883,
                    "mention": "2010",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Kagoshima Prefecture, located on the southeastern coast of Kyushu Island, Japan, has extratropical climates and produces large amounts of pork in Japan. The traditional purebred Kagoshima Berkshire pig, which is a famous Japanese pork breed (sold under the trademarked name Kagoshima Kurobuta), was developed in Kagoshima from animals that originated in Berkshire county, England. Berkshire sows have a lower litter size and reduced fertility relative to the F1 crossbred animals that predominate on Japanese pig farms (Sasaki et al. 2014); thus, the loss of piglets due to the PED outbreak severely impacted the profitability of purebred Kagoshima Berkshire herds.",
            "cite_spans": [
                {
                    "start": 534,
                    "end": 538,
                    "mention": "2014",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Thus, the aim of this study was to investigate the ovarian condition and subsequent reproductive performance of Kagoshima Berkshire sows following an outbreak of PED.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "All animal procedures were reviewed and approved by the Animal Experiment Care and Use Committee of the Kagoshima University, Kagoshima, Japan.",
            "cite_spans": [],
            "section": "Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "This study was conducted on an approximately 500-sow commercial farrow-to-finish farm having purebred Kagoshima Berkshire pigs in Kagoshima Prefecture, Japan. We conducted two separate studies: study 1 investigated the effect of the PED outbreak on sow ovarian condition at weaning, and study 2 investigated the effect of the PED outbreak on reproductive performance by comparing production variables before and after the outbreak. On the studied farm, the PED outbreak began in the gestation barn on January 6, 2014, and spread to the farrowing barn in January 9. Herd immunization was immediately performed using natural exposure methods; the intestines of infected piglets were obtained, ground, mixed well, and fed to the pregnant gilts and sows, weaned sows, and replacement gilts.",
            "cite_spans": [],
            "section": "Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "In study 1, 19 to 20 sows were randomly selected in each month from July 2013 to July 2014, which included the 6 months before and after the start of the PED outbreak, to collect blood samples at weaning (on average 27.3 \u00b1 0.2 days after parturition) from jugular venous. Serum concentrations of progesterone (secreted from functional corpora lutea (CL)) and estradiol-17\u03b2 (secreted from large mature follicles) were measured by time-resolved fluoroimmunoassay (Tr-FIA) using a Tr-FIA kit (DELFIA estradiol and progesterone kits; PerkinElmer Japan, Yokohama, Japan) as previously reported (Noguchi et al. 2007). The intra- and inter-assay CVs were 7.1 and 12.6% for estradiol-17\u03b2 and 10.0 and 9.1% for progesterone, respectively. Functional CL or mature follicles were considered to be present if progesterone concentrations were greater than 5 ng/ml or estradiol-17\u03b2 concentrations were greater than 10 pg/ml, respectively (Noguchi et al. 2010, 2011).",
            "cite_spans": [
                {
                    "start": 605,
                    "end": 609,
                    "mention": "2007",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 940,
                    "end": 944,
                    "mention": "2010",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 946,
                    "end": 950,
                    "mention": "2011",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "In study 2, data from after the PED outbreak began were collected from sows farrowed from January 6 until March 2014. To minimize potentially confounding seasonal effects on reproductive performance, the before-PED outbreak data were collected from January until March 2013 and compared for each month. In total, this study used reproductive data from 521 farrowing records and 658 mating records.",
            "cite_spans": [],
            "section": "Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "As statistical analysis, continuous data were analyzed using general linear mixed model and binomial data were analyzed using a mixed effects logistic regression model (SAS Institute Inc., Cary, NC, USA). In study 1, ovarian condition (progesterone and estradiol-17\u03b2 concentrations), lactation length, and number of piglets weaned were compared by the month. In study 2, reproductive performance was compared by the month (January, February, or March), year (2013 or 2014), and the interaction between the month and year. In both studies, parity, lactation length, and number of piglets weaned were accordingly included in the model as a covariate and so was included as a random effect.",
            "cite_spans": [],
            "section": "Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "In study 1, the mean serum concentrations (\u00b1SEM) of progesterone and estradiol-17\u03b2 were 0.81 \u00b1 0.32 ng/ml and 2.04 \u00b1 0.43 pg/ml, respectively. The percentages of sows with functional CL were 5% (1/20) in November and December 2013, 10% (2/20) in February 2014, and 15% (3/20) in January 2014, the first month of the PED outbreak. The percentages of sows with mature follicles were 5% (1/20) in February 2014 and 10% (2/20) in November and December 2013 and January 2014. The mean progesterone concentration during January 2014 was numerically higher than during the other months, but this difference was not significant (Table 1). In contrast, the estradiol-17\u03b2 concentration during January 2014 was significantly higher than during July and October 2013 (P < 0.05). There was no significant difference in lactation length between the months. The number of pigs weaned during January 2014 was significantly lower than during any of the other months studied (P < 0.05).\n",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 627,
                    "end": 628,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "In study 2, there were significant differences between years in preweaning mortality, number of piglets weaned, and farrowing rate (P < 0.05; Table 2). In addition, the interaction between the year and month significantly affected preweaning mortality, number of piglets weaned, weaning-to-first mating interval, and farrowing rate (P < 0.05; Table 2). In January, preweaning mortality was higher during 2014 than 2013 (P < 0.05), but there were no between-year differences in February or March. The number of piglets weaned during January and February 2013 were higher than during the same months in 2014 (P < 0.05). For each month from January to March, the farrowing rate in 2013 was higher than in 2014 (P < 0.05).\n",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 148,
                    "end": 149,
                    "mention": "2",
                    "ref_id": "TABREF1"
                },
                {
                    "start": 349,
                    "end": 350,
                    "mention": "2",
                    "ref_id": "TABREF1"
                }
            ]
        },
        {
            "text": "Preweaning mortality was high during the month that PED occurred, reaching approximately 40%. This is consistent with values reported by Stevenson et al. (2013) and Carvajal et al. (2015), although those reports were based on F1 crossbred pigs. After the initial month of the PED outbreak, preweaning mortality returned to levels not significantly higher than before the outbreak. This result suggests that the response to the PED outbreak, including herd immunization, intensive cleaning, and disinfection, and early weaning programs effectively controlled the outbreak so that during the next few months the sows and their litters experienced low effects of PED. In contrast, monthly differences in farrowing rates before and after the PED outbreak were significant in January, February, and March. These results indicate that the PED outbreak negatively influenced sow fertility, an effect likely to last for some time. Thus, it is necessary to monitor the sows that were infected with PED around the time of weaning and mating to avoid an increase in nonproductive days.",
            "cite_spans": [
                {
                    "start": 155,
                    "end": 159,
                    "mention": "2013",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 182,
                    "end": 186,
                    "mention": "2015",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Our results showed that a higher proportion of sows had functional CL at weaning after the PED outbreak than before it. In addition, during January 2014, the month that the PED outbreak started, the percentages of sows that had ovulated (15%) or had mature follicles on their ovaries (10%) at weaning were both high compared to during the other months studied. Stimulation from suckling piglets suppresses follicular development, onset of estrus, and ovulation during lactation (Quesnel and Prunier 1995). Our results suggest that a decrease in and/or lack of stimulation from suckling piglets, due the PED infection, increased the occurrence of large mature follicles and preweaning ovulation. In general, follicular development and maturation is associated with gonadotrophin (LH and FSH) stimulation. Estradiol-17\u03b2 is secreted from mature follicles greater than 6 mm in diameter; follicles of this size are close to ovulation (Noguchi et al. 2010). In addition, low suckling stimulation may result in lactational estrus and extend the weaning-to-first mating interval; indeed, this period was longer in sows exposed to PED than in sows not exposed to PED. Normally, sows do not experience lactational ovulation because during lactation, the peripheral concentration and pulsatility of LH are suppressed due to suckling-induced inhibition of the GnRH-pulse generator (De Rensis et al. 1993) and also to the negative energy balance of the sow during lactation (van den Brand et al. 2000), both of which inhibit follicle growth and ovulation. However, when suckling intensity is reduced, follicle growth and ovulation can occur during lactation (Soede et al. 2011).",
            "cite_spans": [
                {
                    "start": 499,
                    "end": 503,
                    "mention": "1995",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 945,
                    "end": 949,
                    "mention": "2010",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1387,
                    "end": 1391,
                    "mention": "1993",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1659,
                    "end": 1663,
                    "mention": "2011",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In conclusion, our results show that a PED outbreak led to poorer reproductive performance in Berkshire sows. These findings help to quantify the impact of PED infection in Japan and, ultimately, contribute to our knowledge of the cost-effectiveness of disease prevention and control measures.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1: Change of concentrations of progesterone and estradiol-17\u03b2, lactation length, and number of pigs weaned before and after porcine epidemic diarrhea (PED) outbreak\nValues within a column without a common letter (a\u2013c) were significantly different (P < 0.05)",
            "type": "table"
        },
        "TABREF1": {
            "text": "Table 2: Comparisons of reproductive performance between 2013 (before PED outbreak) and 2014 (after PED outbreak) in January, February, and March\nMeans within year and month without a common letter (a\u2013b) are significantly different (P < 0.05)",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Porcine epidemic diarrhoea: new insights into an old disease",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Carvajal",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Arg\u00fcello",
                    "suffix": ""
                },
                {
                    "first": "FJ",
                    "middle": [],
                    "last": "Mart\u00ednez-Lobo",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Costillas",
                    "suffix": ""
                },
                {
                    "first": "RG",
                    "middle": [],
                    "last": "Miranda",
                    "suffix": ""
                },
                {
                    "first": "PJ",
                    "middle": [],
                    "last": "de Nova",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Rubio",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Porcine Health Management",
            "volume": "1",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/s40813-015-0007-9"
                ]
            }
        },
        "BIBREF1": {
            "title": "An assessment of reproductive and lifetime performances of Kagoshima Berkshire gilts and sows",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Sasaki",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Togunaga",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Uemura",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Sueyoshi",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Animal Science Journal",
            "volume": "85",
            "issn": "",
            "pages": "213-218",
            "other_ids": {
                "DOI": [
                    "10.1111/asj.12140"
                ]
            }
        },
        "BIBREF2": {
            "title": "Reproductive cycles in pigs",
            "authors": [
                {
                    "first": "NM",
                    "middle": [],
                    "last": "Soede",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Langendijk",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Kemp",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Animal Reproduction Science",
            "volume": "124",
            "issn": "",
            "pages": "251-258",
            "other_ids": {
                "DOI": [
                    "10.1016/j.anireprosci.2011.02.025"
                ]
            }
        },
        "BIBREF3": {
            "title": "Emergence of Porcine epidemic diarrhea virus in the United States: clinical signs, lesions, and viral genomic sequences",
            "authors": [
                {
                    "first": "GW",
                    "middle": [],
                    "last": "Stevenson",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Hoang",
                    "suffix": ""
                },
                {
                    "first": "KJ",
                    "middle": [],
                    "last": "Schwartz",
                    "suffix": ""
                },
                {
                    "first": "ER",
                    "middle": [],
                    "last": "Burrough",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Sun",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Madson",
                    "suffix": ""
                },
                {
                    "first": "VL",
                    "middle": [],
                    "last": "Cooper",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pillatzki",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Gauger",
                    "suffix": ""
                },
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Schmitt",
                    "suffix": ""
                },
                {
                    "first": "LG",
                    "middle": [],
                    "last": "Koster",
                    "suffix": ""
                },
                {
                    "first": "ML",
                    "middle": [],
                    "last": "Killian",
                    "suffix": ""
                },
                {
                    "first": "KJ",
                    "middle": [],
                    "last": "Yoon",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Journal of Veterinary Diagnostic Investigation",
            "volume": "25",
            "issn": "",
            "pages": "649-654",
            "other_ids": {
                "DOI": [
                    "10.1177/1040638713501675"
                ]
            }
        },
        "BIBREF4": {
            "title": "Luteinizing hormone and prolactin responses to naloxone vary with stage of lactation in the sow",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "De Rensis",
                    "suffix": ""
                },
                {
                    "first": "JR",
                    "middle": [],
                    "last": "Cosgrove",
                    "suffix": ""
                },
                {
                    "first": "GR",
                    "middle": [],
                    "last": "Foxcroft",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Biology of Reproduction",
            "volume": "48",
            "issn": "",
            "pages": "970-976",
            "other_ids": {
                "DOI": [
                    "10.1095/biolreprod48.5.970"
                ]
            }
        },
        "BIBREF5": {
            "title": "LH pulsatile release patterns, follicular growth and function during repetitive periods of suckling and non-suckling in sows",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Langendijk",
                    "suffix": ""
                },
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "Dieleman",
                    "suffix": ""
                },
                {
                    "first": "CM",
                    "middle": [],
                    "last": "van den Ham",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Hazeleger",
                    "suffix": ""
                },
                {
                    "first": "NM",
                    "middle": [],
                    "last": "Soede",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Kemp",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Theriogenology",
            "volume": "67",
            "issn": "",
            "pages": "1076-1086",
            "other_ids": {
                "DOI": [
                    "10.1016/j.theriogenology.2006.12.008"
                ]
            }
        },
        "BIBREF6": {
            "title": "Measurement of porcine luteinizing hormone concentration in blood by time-resolved fluoroimmunoassay",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Noguchi",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Yoshioka",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kaneko",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Iwamura",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Takahashi",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Suzuki",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Arai",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wada",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Itoh",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Journal of Veterinary Medical Science",
            "volume": "70",
            "issn": "",
            "pages": "1291-1294",
            "other_ids": {
                "DOI": [
                    "10.1292/jvms.69.1291"
                ]
            }
        },
        "BIBREF7": {
            "title": "Peripheral concentrations of inhibin A, ovarian steroids, and gonadotropins associated with follicular development throughout the estrous cycle of the sow",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Noguchi",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Yoshioka",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Itoh",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Suzuki",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Arai",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wada",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Hasegawa",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kaneko",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Reproduction",
            "volume": "139",
            "issn": "",
            "pages": "153-161",
            "other_ids": {
                "DOI": [
                    "10.1530/REP-09-0018"
                ]
            }
        },
        "BIBREF8": {
            "title": "An efficient protocol for inducing pseudopregnancy using estradiol dipropionate and follicular development associated with changes in reproductive hormones after prostaglandin F2alpha treatment in pseudopregnant sows",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Noguchi",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Yoshioka",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Suzuki",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Itoh",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kaneko",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Reproductive Biology and Endocrinology",
            "volume": "9",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/1477-7827-9-157"
                ]
            }
        },
        "BIBREF9": {
            "title": "Impact of porcine epidemic diarrhea virus infection at different periods of pregnancy on subsequent reproductive performance in gilts and sows",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Olanratmanee",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kunavongkrit",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Tummaruk",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Animal Reproduction Science",
            "volume": "122",
            "issn": "",
            "pages": "42-51",
            "other_ids": {
                "DOI": [
                    "10.1016/j.anireprosci.2010.07.004"
                ]
            }
        },
        "BIBREF10": {
            "title": "Endocrine bases of lactational anestrus in the sow",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Quesnel",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Prunier",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Reproduction Nutrition Development",
            "volume": "35",
            "issn": "",
            "pages": "395-414",
            "other_ids": {
                "DOI": [
                    "10.1051/rnd:19950405"
                ]
            }
        },
        "BIBREF11": {
            "title": "Coronaviruses",
            "authors": [
                {
                    "first": "LJ",
                    "middle": [],
                    "last": "Saif",
                    "suffix": ""
                },
                {
                    "first": "MB",
                    "middle": [],
                    "last": "Pensaert",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Sestak",
                    "suffix": ""
                },
                {
                    "first": "S-G",
                    "middle": [],
                    "last": "Yeo",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Jung",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Diseases of Swine",
            "volume": "",
            "issn": "",
            "pages": "501-524",
            "other_ids": {
                "DOI": []
            }
        }
    }
}