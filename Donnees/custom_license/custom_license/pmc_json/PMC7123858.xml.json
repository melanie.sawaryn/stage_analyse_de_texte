{
    "paper_id": "PMC7123858",
    "metadata": {
        "title": "Fighting the Coronavirus Outbreak",
        "authors": [
            {
                "first": "Laura",
                "middle": [],
                "last": "Kiessling",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peng",
                "middle": [],
                "last": "Chen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jie",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jie",
                "middle": [
                    "P."
                ],
                "last": "Li",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Laura Kiessling, ACS Chemical Biology Editor-in-Chief, and Peng\nChen, ACS Chemical Biology Associate Editor: The recent global\noutbreak of the novel coronavirus COVID-19 is changing the research focus of scientists and\nthe ways in which we practice science. To limit the virus spread, those in the USA are\ncanceling both smaller (\u223c100 people) and large meetings, researchers are limiting\ntheir travel both overseas and within the country, and student college and graduate school\nvisit weekends are being postponed or stopped altogether. Still, there is much uncertainty\nabout how to proceed and what other measures should be taken. Since the outbreak began in\nChina, Chinese scientists have already faced many of the questions that those of us in other\nlocations are now asking. We therefore asked several Chinese scientists to discuss the\nstrategies that they are pursuing to limit the impact of the virus both on their research\nand on society. Some of the actions taken by the scientists and the government under this\nextreme situation may have a long-term influence and shift our future research and funding\nparadigm. We anticipate that their perspectives will be valuable to the community.",
            "cite_spans": [],
            "section": "COVID-19 and the Chemical Biology Community",
            "ref_spans": []
        },
        {
            "text": "Jie Wang, Synthetic and Functional Biomolecules Center (SFBC), College of Chemistry\nand Molecular Engineering, Peking University, Beijing 100871, China, and Jie P. Li, State\nKey Laboratory of Coordination Chemistry, Chemistry and Biomedicine Innovation Center\n(ChemBIC), School of Chemistry and Chemical Engineering, Nanjing University, Nanjing,\n210023, China: The outbreak of Coronavirus 2019 (COVID-19) has become a global\nlife-threatening concern in recent days. The war against COVID-19 there has now reached the\nmost critical moment, as confirmed COVID-19 cases have peaked for several days, and almost\nall the people in Wuhan City have been screened for suspected case identification (as of\nlate February). We cannot imagine how people there are sacrificing to control the spread of\nCOVID-19. As alumni of Wuhan University that is located in the center of Wuhan city, we\nbecame deeply saddened and worried as we watched the entire city cope with an unprecedented\nemergency situation. We wanted to provide a glimpse of what chemical biologists have been\nworking on, from the perspective of scientists in China. In the past 40 days, we have heard\nmore stories of hope and resilience than of worry and grief in China, where 1.4 billion\npeople are doing their best to bring this serious outbreak under control. Right now, the\nepidemic situation outside Hubei is gradually improving, and the situation inside Hubei has\nshown signs of stabilization. It is noteworthy that although it is mandated that most people\nhave to stay at home as a quarantine procedure, scientists in China have been increasingly\ndevoted to combating COVID-19, with several clinical trials currently underway. As for\nscientists throughout the world, researchers in China are making extraordinary efforts\ntoward rapid advancement of diagnostic and treatment approaches, and are doing so under\nhighly challenging personal circumstances with extensive restrictions on not only travel,\nbut day-to-day activities. As mentioned by WHO officers, Chinese scientists have been\nextremely professional in the face of this epidemic and have made their findings rapidly\navailable to researchers around the world to coordinate global efforts to characterize,\ntreat, and mitigate the spread of COVID-19. One team led by Yongzhen Zhang from Fudan\nUniversity completed the deep sequencing of COVID-19 and made its whole genome sequence\npublicly available on January 10, 2020.1 Medical experts, virologists,\nstructural biologists, and medicinal chemists have made significant contributions in areas\nsuch as epidemic prevention and control, patient treatment, virus classification and\ntraceability, structure analysis, and clinical drug development. The current progress relied\non the rapid development of science and technology, as well as the dedication of scientific\nresearchers. During the epidemic, almost all industries not related to any antiepidemic\nefforts were halted due to the lock down of transportation and lack of manpower. It has been\nextremely difficult to complete high-level scientific research under such an extreme\nsituation.",
            "cite_spans": [
                {
                    "start": 2432,
                    "end": 2433,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Fighting the COVID-19 Outbreak: Perspectives from Chemical Biologists in China",
            "ref_spans": []
        },
        {
            "text": "Here, we hope to provide a glimpse into current efforts and progress made by Chinese\nscientists during the outbreak, especially those who work at the interface of chemistry and\nbiology, and to discuss how the community can work together to prevent and control\ncoronavirus infection now, and in the future.",
            "cite_spans": [],
            "section": "Fighting the COVID-19 Outbreak: Perspectives from Chemical Biologists in China",
            "ref_spans": []
        },
        {
            "text": "On the basis of the crystal structure and computational chemistry of 2019-nCoV hydrolase\n(Mpro), a research team led by Prof. Hualiang Jiang at the Shanghai Institute of Materia\nMedica of the Chinese Academy of Sciences performed virtual screening of marketed drugs and\nthe self-built compound library with \u201chigh druggability.\u201d The hit compounds\nare currently subject to further experimental tests for identifying potential drug\ncandidates.2 Another expert in chemical biology and structural\nbiology\u2014Prof. Wenshe Liu from Texas A&M University\u2014performed a docking\nstudy and suggested the use of remdesivir, a clinical trial III antiviral drug developed by\nGilead, as a safe and potentially effective drug against 2019-nCoV3,4 (Figure 1). He posted his independent findings as a preprint on January 27,\nwhich was supported by the \u201csympathetic use\u201d of the first patient in the\nUnited States around the same time.5 Gilead is now working with hospitals\nin China to carry out large-scale clinical trials.6 The anti-malarial\ndrug chloroquine is also in clinical trials and showing promising preliminary results. Other\npotential therapeutics, such as a plant microRNA (MIR2911), found by Prof. Chenyu Zhang from\nNanjing University, are under preclinical or clinical evaluations. In order to speed up the\ndiscovery process of the neutralizing antibody, a team led by Prof. Sunney Xie at Peking\nUniversity adopted a method for high-throughput single-cell sequencing of B cells (only VDJ\nregion) from the blood samples in patients during rehabilitation.7 In\ncollaboration with the You\u2019an Hospital of Beijing Medical University, they collected\nblood samples from 12 convalescent patients, performed B-cell single cell sorting and\nlibrary construction, and detected 15 000 IgG antibody sequences in 143 000 B\ncells. A total of 138 IgG antibodies with the highest enrichment were selected, and the\nELISA-based affinity screening confirmed the nanomolar level of binding to the surface\nprotein S and RBD of the virus.7 The ongoing neutralizing antibody\nscreening is expected to facilitate virus prevention and treatment, as well as\ndiagnostics.",
            "cite_spans": [
                {
                    "start": 440,
                    "end": 441,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 721,
                    "end": 722,
                    "mention": "3",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 723,
                    "end": 724,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 909,
                    "end": 910,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 998,
                    "end": 999,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1542,
                    "end": 1543,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 2002,
                    "end": 2003,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Fighting the COVID-19 Outbreak: Perspectives from Chemical Biologists in China",
            "ref_spans": [
                {
                    "start": 726,
                    "end": 734,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "As a key component in epidemic prevention and control, the diagnosis of COVID-19 determines\nthe classification and treatment of patients. In the early stage of the epidemic, clinicians\nrelied on the traditional nucleic acid detection kit as the most precise method for\ndiagnosis; however, it is time-consuming with an inherent low positive detection rate. As an\nalternative approach, a team led by Prof. Xueji Zhang and Prof. Xinchun Chen at Shenzhen\nUniversity developed a chemiluminescence-based antibody detection kit, which was\nsuccessfully tested and put into use on February 4, 2020.8 The kit is\nbased on the enrichment and detection of IgM in serum and is suitable for clinical samples\nfrom patients with fevers in the past 7\u201314 days. It can complete the rapid diagnosis\nof coronavirus infection in as fast as 22 min, which greatly reduced the diagnosis time.\nMany other scientists in China have also been dedicated to the development of diagnostic\nmethods. For example, a joint team led by Prof. Weihong Tan from Renji Hospital of Shanghai\nJiaotong University and Hunan University has developed a new diagnostic method\nvia portable RT-PCR-based molecular diagnostics.9 At\nthe center of the outbreak, a joint team led by Prof. Tiangang Liu at Wuhan University and\nProf. Yan Li from the Renmin Hospital of Wuhan University developed a nanopore target\nsequencing (NTS) method to detect COVID-19 and other potential respiratory viruses\nsimultaneously. By combining the advantages of long-read, real-time nanopore sequencing and\ntarget amplification, this approach allowed more sensitive detection than qPCR kits and\ncould also monitor for mutation or other respiratory virus infections in test\nsamples.10 In addition to the academic community, scientists from\nindustry with a background in chemical biology are also actively participating in the war\nagainst the epidemic. On January 25th, the first day of the Chinese New Year, an R&D\nteam from a biotech company announced successful development of a rapid and sensitive\ndetection system that only takes 9 min for pretreatment of 32 samples (Figure 2). With the combination of an automatic nucleic acid\nextraction instrument, the daily test capacity for a single technician can reach 1000\nsamples. The diagnosis kits have been utilized in over 10 Chinese provinces and used to test\nmore than 100 000 clinical samples in the first 2 weeks of February alone. Dr. Lu\nDong and Dr. Haiping Song are two leading scientists in this team, who previously graduated\nfrom the Department of Chemical Biology, College of Chemistry at Peking University.\nScientists in Hong Kong, one of the busiest international transportation hubs in China, are\nalso dedicated to protecting their city through everything they can do. For example, Prof.\nChi-Ming Che and Vivian Yam, two of the most famous chemists in Hong Kong, even provided\nself-made disinfectants to Hong Kong citizens according to the protocol of the WHO.",
            "cite_spans": [
                {
                    "start": 589,
                    "end": 590,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1175,
                    "end": 1176,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1706,
                    "end": 1708,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Fighting the COVID-19 Outbreak: Perspectives from Chemical Biologists in China",
            "ref_spans": [
                {
                    "start": 2096,
                    "end": 2104,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "It should be noted that the aforementioned work is only a part of the growing list of\nexamples in fighting the virus on an invisible battlefield, and the global medical and\nscientific communities have been working tirelessly to put forth solutions to bring the\ncurrent outbreak under control. Facing such a sudden and serious epidemic outbreak, we feel\nthat scientific research is difficult to accomplish overnight, and many scientific studies\ncannot keep up with the fast spread of the virus. Global efforts from research institutions\nand pharmaceutical companies with a \u201crecord breaking\u201d speed are highly desired\nin the arms race against the virus. To our delight, it only took a few days for remdesivir\nto enter clinical trials. A coherent effort from every section of the drug development chain\nmade this fast track possible, which we hope can eventually turn into a life-saving\ntherapy.",
            "cite_spans": [],
            "section": "Fighting the COVID-19 Outbreak: Perspectives from Chemical Biologists in China",
            "ref_spans": []
        },
        {
            "text": "After this epidemic, we have further realized that human beings need more technical\nreserves when facing major infectious diseases, and many directions need to be urgently\nexplored. From the chemical biology perspective, below are some directions that need the\nattention from scientists working at the interface of chemistry, life sciences, and\nmedicine:(i)Development of rapid,\naccurate, safe, and convenient detection and diagnosis technologies. Early stage\nscreening is to quickly isolate confirmed patients. Intermediate stage screening can\nprovide basis for precise treatment, and later screening is to establish reliable\nstandards for discharge and desegregation. Specific diagnostic methods should be\ndeveloped for each of the different\nstages.(ii)Fast screening of safe and\neffective drugs. In the face of sudden public health safety incidents, the fastest way\nis to establish treatments to identify drugs already available in drug libraries since\ntheir safety has been proved in clinical trials. At the same time, drug efficacy needs\nto be screened for different stages of the disease. For example, for advanced\npatients, reducing the cytokine storm may be a major\ndirection.(iii)Development of\nneutralizing antibodies and vaccines. Interdisciplinary participation, including\nchemical biology, is expected to accelerate the research and development process, for\nexample, how to speed up the adaptive immune response to develop specific IgG\nantibodies, how to efficiently enrich antibodies from serum, and how to quickly and\ncost-effectively isolate human B cells and perform single BCR sequencing to facilitate\nthe identification of neutralizing antibodies.",
            "cite_spans": [],
            "section": "Fighting the COVID-19 Outbreak: Perspectives from Chemical Biologists in China",
            "ref_spans": []
        },
        {
            "text": "The outbreak of COVID-19 may trigger many scientists to reconsider and reshape their\nresearch interests toward infectious-disease-related fields and encourage research funders\nto commit more long-term resources to these efforts, even after the current outbreak has\nbeen contained. Indeed, we should speed up our scientific advancement so that we can build\nthe defense line more quickly when combating a mysterious and rapidly evolving virus in the\nfuture. Coherent and interdisciplinary efforts among the entire scientific community will\nplay important roles to equip ourselves with a more powerful arsenal in the war against\nvirus outbreak.",
            "cite_spans": [],
            "section": "Fighting the COVID-19 Outbreak: Perspectives from Chemical Biologists in China",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Remdesivir can be metabolized into its active form GS-441524 and target RdRp of\ncoronavirus. (The picture of RdRp is regenerated from the structure of SARS-CoV nsp12\npolymerase, PDB: 6NUS.)",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Scientists were developing the COVID-19 diagnostic kit during the Chinese New Year.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "A new coronavirus associated with human respiratory disease in\nChina",
            "authors": [],
            "year": 2020,
            "venue": "Nature",
            "volume": "579",
            "issn": "",
            "pages": "265-269",
            "other_ids": {
                "DOI": [
                    "10.1038/s41586-020-2008-3"
                ]
            }
        },
        "BIBREF1": {
            "title": "Nanopore target sequencing for accurate and comprehensive detection of\nSARS-CoV-2 and other respiratory viruses",
            "authors": [],
            "year": 2020,
            "venue": "medRvix",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "The Chinese Academy of Sciences and\nthe University of Science and Technology recently found that these old drugs may be\neffective for new pneumonia",
            "authors": [],
            "year": 2020,
            "venue": "Small Tech News",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Learning from the Past: Possible Urgent Prevention and Treatment Options\nfor Severe Acute Respiratory Infections Caused by 2019-nCoV",
            "authors": [],
            "year": 2020,
            "venue": "ChemBioChem",
            "volume": "21",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1002/cbic.202000047"
                ]
            }
        },
        "BIBREF4": {
            "title": "Learning from the Past: Possible Urgent Prevention and Treatment Options\nfor Severe Acute Respiratory Infections Caused by 2019-nCoV",
            "authors": [],
            "year": 2020,
            "venue": "ChemRxiv",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.26434/chemrxiv.11728983.v1"
                ]
            }
        },
        "BIBREF5": {
            "title": "First Case of 2019 Novel Coronavirus in the United States",
            "authors": [],
            "year": 2020,
            "venue": "N. Engl. J. Med.",
            "volume": "382",
            "issn": "",
            "pages": "929-936",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001191"
                ]
            }
        },
        "BIBREF6": {
            "title": "Coronavirus drug to begin clinical trials",
            "authors": [],
            "year": 2020,
            "venue": "China\nDaily",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Resource: from the website of\nBiomedical Pioneering Innovation Center (BIOPIC), Peking University",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Resource: from the website of\nschool of basic medical science, Shenzhen University Health Science\nCenter",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Quick virus detection kits may be suitable for home use",
            "authors": [],
            "year": 2020,
            "venue": "Shine",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}