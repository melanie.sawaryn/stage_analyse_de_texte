{
    "paper_id": "PMC7095925",
    "metadata": {
        "title": "Ebola: a call to action",
        "authors": []
    },
    "body_text": [
        {
            "text": "At the time of this writing, more than 2,200 people are estimated to have been infected by a new strain of Zaire ebolavirus in four West African nations, and more than 1,200 have died. Infection can cause fever, vomiting, diarrhea and internal and external hemorrhaging that can lead to death. Neighboring as well as non-neighboring countries are at risk because of porous borders and air travel of presymptomatic infected individuals, the latter having resulted in the spread of infection to Nigeria. And while the death rate\u2014estimated at 55%\u2014is lower than that of many previous Ebola outbreaks, the total number of cases exceeds all ebolavirus infections since 1976. We don't know when the outbreak will end, or how far it will spread, but its control is expected to take months and may involve extraordinary measures.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Ebola virus first emerged in the Democratic Republic of the Congo (DRC) and in South Sudan in 1976 and reappeared in South Sudan in 1979, but it caused no further outbreaks until 1994. Since then, there have been several outbreaks in Africa, but none approached the magnitude of the current outbreak. The natural reservoir of the virus remains unclear, but it is suspected to be the fruit bat. However, Ebola virus also infects nonhuman primates, a species of antelope and porcupines, all of which could be sources of human transmission.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The unusually rapid and far-reaching spread of the virus during the current outbreak has been facilitated by insufficient treatment and containment facilities in West African nations that had no prior experience with Ebola; a distrust of Western medical practices; the stigma associated with infection, causing failure to seek early treatment; as well as the long asymptomatic incubation period of the virus (up to 21 days), which enables dissemination through travel.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Similar to the situation with severe acute respiratory syndrome (SARS), caused by the SARS coronavirus SARS-CoV and that killed more than 700 people in 29 countries during the 2003 epidemic, there is no approved vaccine or cure for Ebola virus infection. For both pathogens, vaccine development is hampered by the fact that the diseases are not endemic, resulting in a lack of identifiable at-risk populations in which to test vaccine candidates. Moreover, there have been no recorded cases of SARS since 2004, and the current Ebola outbreak began more than 2,000 miles from the previous Zaire ebolavirus outbreak in 2008\u20132009 in the DRC. These circumstances lessen the urgency in preparing for these threats.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "What's more, unlike with malaria, drugs or vaccines for SARS and Ebola cannot be tested in the setting of experimental human infection. Demonstrating their efficacy and safety is restricted to animal models, which themselves have limitations. For example, preclinical testing of treatments against ebolavirus is generally initiated within hours or a few days after infection, whereas in humans the virus may be first identified weeks after initial infection, and the viral load\u2014and its sequelae\u2014may or may not be comparable to those in animal models.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Financial challenges also slow development of vaccines and treatments for these infections. The market for drugs against SARS or Ebola is likely to be small and sporadic. Lacking market-driven forces, financial investment in their development is therefore dependent on governments of wealthy nations. However, the citizens of these nations may have limited exposure to the specific pathogens, and, as such, governments may not prioritize the development of drugs to fight them. For example, in 2012, around the time of the US 'fiscal cliff' scenario, the US Department of Defense (DoD), citing budget constraints, issued separate stop-work orders to Sarepta Therapeutics and Tekmira Pharmaceuticals on their programs aimed at developing morpholinos and RNAi therapeutics, respectively, against Ebola virus. The DoD ultimately reinstated Ebola research funding to Tekmira but not Sarepta.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In view of the unprecedented severity of the current Ebola outbreak, the World Health Organization (WHO) has stated that it would be ethical to use experimental medicines to combat the disease. Sarepta has said it would mobilize its stock of candidate drug, which was tested for safety in humans, if requested. The Tekmira drug might also be used in Ebola patients, although owing to concerns about cytokine-associated side effects, the US Food and Drug Administration placed a clinical hold\u2014recently revised to a 'partial hold'\u2014on testing in healthy volunteers. Two Americans, a Spanish priest and three Liberian doctors infected with Ebola have received ZMapp, a cocktail of monoclonal antibodies produced by Mapp Biopharmaceutical that was shown to neutralize the virus in monkeys but had not been tested for safety in humans. But the supply of ZMapp is now exhausted, and the company estimates it will take several months before more is available. The WHO is also weighing the possibility of the use of serum from individuals who recovered from Ebola infection. And Canada has committed up to 1,000 doses of an experimental Ebola vaccine\u2014VSV-EBOV\u2014to the WHO. Other companies and governments also have drugs and vaccines in various stages of development.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "These actions are laudable, but piecemeal. When this outbreak has run its course, what will become of these candidate drugs and vaccines? Which ones will be rigorously tested and stockpiled, and by which nations? Which countries will continue to invest in cures for Ebola, SARS and other emerging infectious diseases, such as Marburg hemorrhagic fever or the Middle East respiratory syndrome, once they cease to command global attention? Encouragingly, on 21 August the Wellcome Trust announced two initiatives: rapid funding for research proposals targeting the current and future Ebola outbreaks and a five-year \u00a340 million commitment to fund research focusing on health challenges facing Africa, including emerging and endemic infections. The latter initiative, which takes a more long-term view, is a step in the right direction. The ability to survive the next outbreak requires continued investment by all nations in detection, prevention, containment, treatment and education. Anything less would be unethical.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {}
}