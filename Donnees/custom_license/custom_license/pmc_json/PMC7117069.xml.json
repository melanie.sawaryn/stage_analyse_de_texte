{
    "paper_id": "PMC7117069",
    "metadata": {
        "title": "Suggestions for infection prevention and control in digestive endoscopy during current 2019-nCoV pneumonia outbreak in Wuhan, Hubei province, China",
        "authors": [
            {
                "first": "Yafei",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xiaodan",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Lan",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hongling",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Qiu",
                "middle": [],
                "last": "Zhao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "\nIn outbreak areas such as Hubei province, only emergency endoscopy should be performed to treat patients with diseases such as acute GI bleeding, foreign bodies in the GI tract, and acute suppurative cholangitis. In the epidemic area, after the screening process, the basic protection requirements of the medical staff in the endoscopy center should achieve biosafety level 2 for all GI endoscopic procedures\n5\n6\n. Protection at biosafety level 3 is required for all endoscopic procedures in patients with confirmed or suspected 2019-nCoV infection, and for those with very high risk of potential exposure to 2019-nCoV, such as during tracheal intubation, airway care, and sputum suction in noninfected patients\n5\n6\n.\n",
            "cite_spans": [
                {
                    "start": 410,
                    "end": 411,
                    "mention": "5",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 412,
                    "end": 413,
                    "mention": "6",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 713,
                    "end": 714,
                    "mention": "5",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 715,
                    "end": 716,
                    "mention": "6",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Endoscopy in outbreak areas",
            "ref_spans": []
        },
        {
            "text": "In other areas of China, routine endoscopy should be performed with extra precautions to avoid hospital transmission from those patients with 2019-nCoV infection. Identification of patients with potential 2019-nCoV infection is the first critical step in prevention and infection control. Chest computed tomography (CT) is the most reliable approach based on our experiences, as some patients have no clinical manifestations or display only slight digestive tract symptoms; CT scans can reveal interstitial pneumonia in the outer zone of one or both lungs. Moreover, we found that CT manifestations may appear earlier than nucleic acid detection, and in a few patients with epidemiological history, typical chest CT manifestations, and typical clinical outcome of 2019-nCov pneumonia, the viral pneumonia etiologies in the local region \u2013 including 2019-nCov etiology detection \u2013 are all negative. Therefore, we believe that CT examination of the lung is faster and more effective than etiological examination for screening of 2019 nCov pneumonia. Compared with other kinds of viral pneumonia, the family aggregation of 2019-nCov pneumonia is more obvious.",
            "cite_spans": [],
            "section": "Endoscopy in other areas of China",
            "ref_spans": []
        },
        {
            "text": "The following workflow is recommended for all endoscopy centers in China based on the current 2019-nCoV pneumonia outbreak in Wuhan, Hubei province, China.",
            "cite_spans": [],
            "section": "Endoscopy in other areas of China",
            "ref_spans": []
        },
        {
            "text": "Ensuring that staff are working in a safe and clean environment is a critical step. The following approaches are recommended to prevent staff from transmitting 2019-nCoV in the endoscopy center.",
            "cite_spans": [],
            "section": "Staff protection",
            "ref_spans": []
        },
        {
            "text": "\nAs coronavirus, including 2019-nCoV, is inactivated by many commonly used disinfectants, no additional measures are necessary for endoscope cleaning and disinfection\n7\n8\n9\n10\n. For the endoscopy center environment, UV irradiation and ozone treatment are recommended for the cleaning and sterilization of air and all surfaces, such as endoscopic equipment, office tables, and walls of the examination room. Chlorine-containing detergent is recommended for daily floor cleaning\n8\n9\n.\n",
            "cite_spans": [
                {
                    "start": 167,
                    "end": 168,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 169,
                    "end": 170,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 171,
                    "end": 172,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 173,
                    "end": 175,
                    "mention": "10",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 477,
                    "end": 478,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 479,
                    "end": 480,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Disinfection management",
            "ref_spans": []
        },
        {
            "text": "In general, we suggest that endoscopy examination and procedures should be strictly limited in all areas of China during the current outbreak in order to combat against 2019-nCoV. For essential endoscopy procedures, pre-screening of patients and protection of staff are critical to avoid hospital transmission.",
            "cite_spans": [],
            "section": "Disinfection management",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig.\u200a1: \n Diagnosis and treatment workflow in a gastrointestinal endoscopy center during the 2019-nCoV outbreak. CT, computed tomography; GI, gastrointestinal.\n",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Biosafety in microbiological and biomedical laboratories",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "Y"
                    ],
                    "last": "Richmond",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "W"
                    ],
                    "last": "McKinney",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Biosafety guidelines. StatPearls [Internet]",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Bayot",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Limaiem",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan, China",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "497-506",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Laboratory containment of SARS virus",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Lim",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "N"
                    ],
                    "last": "Tsang",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Ann Acad Med Singapore",
            "volume": "35",
            "issn": "",
            "pages": "354-360",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Transmission of 2019-nCoV infection from an asymptomatic contact in Germany",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Rothe",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Schunk",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Sothmann",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMc2001468"
                ]
            }
        },
        "BIBREF5": {
            "title": "First case of 2019 novel coronavirus in the United States",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Holshue",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "DeBolt",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Lindquist",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001191"
                ]
            }
        },
        "BIBREF6": {
            "title": "ASGE guideline for infection control during GI endoscopy",
            "authors": [],
            "year": 2018,
            "venue": "Gastrointest Endosc",
            "volume": "87",
            "issn": "",
            "pages": "1167-1179",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Human coronaviruses: insights into environmental resistance and its influence on the development of new antiseptic strategies",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Geller",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Varbanov",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "E"
                    ],
                    "last": "Duval",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Viruses",
            "volume": "4",
            "issn": "",
            "pages": "3044-3068",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Estimating the effective reproduction number of the 2019-nCoV in China",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Cao",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "National Health and Health Commission releases new coronary pneumonia diagnosis and treatment plan",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}