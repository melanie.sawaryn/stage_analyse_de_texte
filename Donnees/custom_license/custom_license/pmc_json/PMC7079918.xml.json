{
    "paper_id": "PMC7079918",
    "metadata": {
        "title": "An overview on severe infections in Europe",
        "authors": [
            {
                "first": "George",
                "middle": [],
                "last": "Dimopoulos",
                "suffix": "",
                "email": "gdimop@med.uoa.gr",
                "affiliation": {}
            },
            {
                "first": "Murat",
                "middle": [],
                "last": "Akova",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The latest developments on severe infections in the ICU could be summarized as the changed epidemiology of infections and pathogens, the emerging appearance of new resistant strains especially in war-ridden conditions, the upcoming new antibiotics, the use of alternative routes for antibiotics administration and the prolonged infusion of antibiotics.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A significant change of patterns in acute infections has emerged in Europe recently as a result of various factors. In the large number of refugees from the Middle East, Africa and elsewhere, multidrug-resistant (MDR) tuberculosis, MDR Gram-negative bacterial infections including those with Acinetobacter, enterics and MRSA have been reported in surveillance studies throughout Europe [1]. War-ridden conditions led to re-emerge of some vaccine-preventable diseases such as measles and poliomyelitis, as well [1]. Crimean Congo haemorrhagic fever, a tick-borne viral infection with a case-fatality ratio of 5\u201330% is widespread in the Balkan region and autochthonous cases were recently reported from Spain [2]. Only a few Middle East respiratory syndrome-coronavirus (MERS CoV) infections have been imported to Europe, but mortality could be up to 36% [3]. Travel-associated Zika virus infections were detected in 19 EU countries and in 92 pregnant women [3]. Severe influenza has been reported as a risk factor for invasive pulmonary aspergillosis even in non-immunocompromised patients [4]. Although Candida albicans is still the single most frequent cause of fungaemia in the ICU, non-albicans Candida are responsible for remarkable epidemiological differences in Europe with C. parapsilosis being more frequent in Southern Europe and C. glabrata in Northern Europe; in the latter, acquired echinocandin resistance is emerging [5]. Among MDR bacterial infections, Escherichia coli has the highest resistance rates (up to 38.1%) against broad-spectrum cephalosporins in Southern and Eastern Europe via extended-spectrum beta-lactamases [6]. For Klebsiella pneumoniae, the highest figures come from Greece (70.1%) and Serbia (88%). Carbapenem resistance in E. coli is rare in Europe, but has become prevalent in K. pneumoniae (59.4% in Greece, 34.3% in Italy, 20.5% in Romania, and less than 2% in other EU countries) [6]. Three main carbapenemases are responsible for this resistance (Fig. 1) [7]. These isolates are usually sensitive to colistin, fosfomycin and tigecycline although resistance to these antibiotics is also rapidly emerging [7]. The recently described plasmid-mediated colistin resistance gene (mcr-1) has been rarely found in human isolates in Europe and its clinical significance is currently unknown [8]. Carbapenem resistance was reported in more than 50% of isolates of Acinetobacter baumannii in Portugal, Greece, Italy, Cyprus, Romania and Bulgaria. These isolates are usually co-resistant to aminoglycosides and quinolones. Colistin resistance in A. baumannii is rare. Tigecycline resistance may occur after a brief exposure to the drug during therapy [6]. Rapid detection of resistant strains is essential for effective treatment, and several phenotypic and genotypic tests have recently been described. Among these are multiplex and/or real-time PCR assays, DNA microarray, whole genome sequencing, MALDI-TOF mass spectrometry and a batch of biochemical tests which can provide results within minutes to a couple of hours with high sensitivity and specificity. Biochemical tests would require bacteria to be cultured first from clinical specimens [7, 9]. Genotypic methods may be even more rapid and one can use them both on clinical specimens and cultures and not only for identifying bacteria but also detecting the genes of antibiotic resistance.\n",
            "cite_spans": [
                {
                    "start": 387,
                    "end": 388,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 511,
                    "end": 512,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 708,
                    "end": 709,
                    "mention": "2",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 854,
                    "end": 855,
                    "mention": "3",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 957,
                    "end": 958,
                    "mention": "3",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1090,
                    "end": 1091,
                    "mention": "4",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1432,
                    "end": 1433,
                    "mention": "5",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1640,
                    "end": 1641,
                    "mention": "6",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1921,
                    "end": 1922,
                    "mention": "6",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1997,
                    "end": 1998,
                    "mention": "7",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 2145,
                    "end": 2146,
                    "mention": "7",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 2324,
                    "end": 2325,
                    "mention": "8",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 2681,
                    "end": 2682,
                    "mention": "6",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 3178,
                    "end": 3179,
                    "mention": "7",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 3181,
                    "end": 3182,
                    "mention": "9",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 1993,
                    "end": 1994,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Several new antibiotics are available or under development including \u03b2-lactamase inhibitor combinations (ceftolozane/tazobactam, ceftazidime/avibactam, ceftaroline/avibactam, aztreonam/avibactam, imipenem/relebactam), eravacycline (a novel broad-spectrum fluorocycline with broad-spectrum activity), plazomicin (a next-generation aminoglycoside with activity against many MDR bacteria), new quinolones (finafloxacin, avarofloxacin, zabofloxacin, nemonoxacin and delafloxacin), new lipo-glycopeptides (dalbavancin and oritavancin) and a new oxazolidinone (tedizolid) with more potent in vitro activity than linezolid [10]. The revival of old antibiotics (fosfomycin, colistin, trimethoprim/sulfamethoxazole combination, tetracycline) against extended-spectrum \u03b2-lactamase (ESBL)-producing and MDR Gram-negative pathogens, MRSA and VRE is already a reality in daily clinical practice (Table 1) [11]. Nebulized antibiotics are used as an adjunctive treatment to IV route in MDR/ventilator-associated pneumonia (VAP) or ventilator-associated tracheobronchitis (VAT) aiming to increase the antibiotic efficacy by delivering high doses to the lung, to reduce drug toxicity and to minimize the risk of resistance [12]. Currently available devices (jet, ultrasonic and vibrating mesh nebulizers) deliver to the lung parenchyma 15\u201360% of the antibiotic dose placed in the chamber. Several other factors (physicochemical/pharmacologic antibiotic properties, patient\u2019s parameters and variability, ventilator and circuit parameters) are important in order to maximize antibiotic delivery and reduce residual loss of the drug [13]. Currently, two randomised clinical trials on MDR/VAP treatment with nebulized antibiotics are ongoing: the amikacin inhale program (BAYER\u00ae) and the IASIS program (Cardeas Pharma\u00ae). The former program in the USA and Europe includes two identical, superiority phase III, prospective, randomized, double blind, placebo-controlled safety and efficacy trials. A specially formulated amikacin inhalation solution is used in combination with IV therapy, powered to demonstrate superiority over standard IV therapy through an on-ventilator or hand-held pulmonary drug delivery system (PDDS). The IASIS program (Cardeas Pharma\u00ae) includes a randomized placebo-controlled study adjunctive to IV antibiotics using a specially formulated amikacin (300 mg)\u2013fosfomycin (120 mg) inhalation solution through an eFlow Inline Nebulizer system (PARI eFlow techology) aiming to improve the outcome of MDR/VAP and to shorten ventilator days [14]. However, in this study the adjunctive aerosol therapy compared to standard of care IV antibiotics in patients with Gram-negative VAP was ineffective to improve clinical outcomes despite reducing bacterial burden. Today, a modern approach of treatment with antibiotics in critically ill patients is based on adequate exposure according to PK/PD properties aimed at a more personalized dosing and on prolonged or continuous infusion mainly of various beta-lactams. This treatment approach aims to reduce the mortality of infected patients, to increase the efficacy of antibiotics, to overcome the potential resistance and to minimize the toxicity associated with antibiotics [15, 16].\n",
            "cite_spans": [
                {
                    "start": 617,
                    "end": 619,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 893,
                    "end": 895,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1207,
                    "end": 1209,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1614,
                    "end": 1616,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 2539,
                    "end": 2541,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 3218,
                    "end": 3220,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 3222,
                    "end": 3224,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 889,
                    "end": 890,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "In conclusion, although several new, emerging and re-emerging infections pose threats to ICU patients, there are new diagnostic and therapeutic options available to counter them. However, we are yet to see their impact and are eagerly awaiting the results of ongoing trials.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table\u00a01: Old antibiotics for the future: fighting antimicrobial resistance in the ICU\nModified table from Ref. [12] \nCoNS coagulase-negative staphylococci, CA community-associated, MDR multidrug-resistant, MRSA methicillin-resistant Staphylococcus aureus, VAP ventilator-associated pneumonia, HA hospital-acquired, UTI urinary tract infection, BJI bone and joint infection, PJI prosthetic joint infection, IAI intra-abdominal infection, ESBL extended-spectrum \u03b2-lactamases, IV intravenous, VRE vancomycin-resistant Enterococcus sp., IE infective endocarditis, IAI intra-abdominal infection",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig.\u00a01: Epidemiological characteristics of carbapenemases produced by Klebsiella pneumoniae in Europe (the data produced in part from Ref. [7])",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Refugees of the Syrian civil war: impact on reemerging infection, health services, and biosecurity in Turkey",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Doganay",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Demiraslan",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Health Sec",
            "volume": "14",
            "issn": "",
            "pages": "220-225",
            "other_ids": {
                "DOI": [
                    "10.1089/hs.2016.0054"
                ]
            }
        },
        "BIBREF1": {
            "title": "Advances in antibiotic therapy in the critically ill",
            "authors": [
                {
                    "first": "JL",
                    "middle": [],
                    "last": "Vincent",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bassetti",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Fran\u00e7ois",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Karam",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Chastre",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Torres",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Roberts",
                    "suffix": ""
                },
                {
                    "first": "FS",
                    "middle": [],
                    "last": "Taccone",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rello",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Calandra",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "De Backer",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Welte",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Antonelli",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Crit Care",
            "volume": "20",
            "issn": "",
            "pages": "133-146",
            "other_ids": {
                "DOI": [
                    "10.1186/s13054-016-1285-6"
                ]
            }
        },
        "BIBREF2": {
            "title": "A new strategy to fight antimicrobial resistance: the revival of old antibiotics",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Cassir",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Rolain",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Brouqui",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Front Microbiol",
            "volume": "20",
            "issn": "5",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Can we improve clinical outcomes in patients with pneumonia treated with antibiotics in the intensive care unit?",
            "authors": [
                {
                    "first": "DP",
                    "middle": [],
                    "last": "Nicolau",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Dimopoulos",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Welte",
                    "suffix": ""
                },
                {
                    "first": "CE",
                    "middle": [],
                    "last": "Luyt",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Expert Rev Respir Med",
            "volume": "10",
            "issn": "8",
            "pages": "907-918",
            "other_ids": {
                "DOI": [
                    "10.1080/17476348.2016.1190277"
                ]
            }
        },
        "BIBREF4": {
            "title": "Influence of inspiratory flow pattern and nebulizer position on aerosol delivery with a vibrating-mesh nebulizer during invasive mechanical ventilation: an in vitro analysis",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Dugernier",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wittebole",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Roeseler",
                    "suffix": ""
                },
                {
                    "first": "JB",
                    "middle": [],
                    "last": "Michotte",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Sottiaux",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Dugernier",
                    "suffix": ""
                },
                {
                    "first": "PF",
                    "middle": [],
                    "last": "Laterre",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Reychler",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Aerosol Med Pulm Drug Deliv",
            "volume": "28",
            "issn": "3",
            "pages": "229-236",
            "other_ids": {
                "DOI": [
                    "10.1089/jamp.2014.1131"
                ]
            }
        },
        "BIBREF5": {
            "title": "A randomized trial of the amikacin fosfomycin inhalation system for the adjunctive therapy of Gram-negative ventilator-associated pneumonia: IASIS Trial",
            "authors": [
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Kollef",
                    "suffix": ""
                },
                {
                    "first": "JD",
                    "middle": [],
                    "last": "Ricard",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Roux",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Francois",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Ischaki",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Rozgonyi",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Boulain",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Ivanyi",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "J\u00e1nos",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Garot",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Koura",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Zakynthinos",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Dimopoulos",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Torres",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Danker",
                    "suffix": ""
                },
                {
                    "first": "AB",
                    "middle": [],
                    "last": "Montgomery",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Chest",
            "volume": "16",
            "issn": "",
            "pages": "62463-62467",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "DALI Study Group. Is prolonged infusion of piperacillin/tazobactam and meropenem in critically ill patients associated with improved PK/PD and patient outcomes? An observation from the Defining Antibiotic Levels in Intensive care unit patients (DALI) cohort",
            "authors": [
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Abdul-Aziz",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Lipman",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Akova",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bassetti",
                    "suffix": ""
                },
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "De Waele",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Dimopoulos",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "J Antimicrob Chemother",
            "volume": "71",
            "issn": "1",
            "pages": "196-207",
            "other_ids": {
                "DOI": [
                    "10.1093/jac/dkv288"
                ]
            }
        },
        "BIBREF7": {
            "title": "DALI: defining antibiotic levels in intensive care unit patients: are current \u03b2-lactam antibiotic doses sufficient for critically ill patients?",
            "authors": [
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Roberts",
                    "suffix": ""
                },
                {
                    "first": "SK",
                    "middle": [],
                    "last": "Paul",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Akova",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bassetti",
                    "suffix": ""
                },
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "De Waele",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Dimopoulos",
                    "suffix": ""
                },
                {
                    "first": "KM",
                    "middle": [],
                    "last": "Kaukonen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Koulenti",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Martin",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Montravers",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rello",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Rhodes",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Starr",
                    "suffix": ""
                },
                {
                    "first": "SC",
                    "middle": [],
                    "last": "Wallis",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Lipman",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Clin Infect Dis",
            "volume": "58",
            "issn": "8",
            "pages": "1072-1083",
            "other_ids": {
                "DOI": [
                    "10.1093/cid/ciu027"
                ]
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Invasive aspergillosis associated with severe influenza infections",
            "authors": [
                {
                    "first": "NF",
                    "middle": [],
                    "last": "Crum-Cianflone",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Open Forum Infect Dis",
            "volume": "3",
            "issn": "3",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1093/ofid/ofw171"
                ]
            }
        },
        "BIBREF11": {
            "title": "Candidemia in intensive care unit: a nationwide prospective observational survey (GISIA-3 study) and review of European literature from 2000 through 2013",
            "authors": [
                {
                    "first": "MT",
                    "middle": [],
                    "last": "Montagna",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Lovero",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Borghi",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Eur Rev Med Pharma Sci",
            "volume": "18",
            "issn": "",
            "pages": "661-674",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Epidemiology of antimicrobial resistance in bloodstream infections",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Akova",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Virulence",
            "volume": "7",
            "issn": "",
            "pages": "252-266",
            "other_ids": {
                "DOI": [
                    "10.1080/21505594.2016.1159366"
                ]
            }
        },
        "BIBREF13": {
            "title": "Global dissemination of carbapenemase-producing Klebsiella pneumoniae: epidemiology, genetic context, treatment options, and detection methods",
            "authors": [
                {
                    "first": "CR",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "JH",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "KS",
                    "middle": [],
                    "last": "Park",
                    "suffix": ""
                },
                {
                    "first": "YB",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "BC",
                    "middle": [],
                    "last": "Jeong",
                    "suffix": ""
                },
                {
                    "first": "SH",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Front Microbiol",
            "volume": "7",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Antimicrobial resistance",
            "authors": [
                {
                    "first": "HD",
                    "middle": [],
                    "last": "Marston",
                    "suffix": ""
                },
                {
                    "first": "DM",
                    "middle": [],
                    "last": "Dixon",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Knisley",
                    "suffix": ""
                },
                {
                    "first": "TN",
                    "middle": [],
                    "last": "Palmore",
                    "suffix": ""
                },
                {
                    "first": "AS",
                    "middle": [],
                    "last": "Fauci",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "JAMA",
            "volume": "316",
            "issn": "",
            "pages": "1193-1204",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2016.11764"
                ]
            }
        },
        "BIBREF15": {
            "title": "The changing role of the clinical microbiology laboratory in defining resistance in Gram-negatives",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Endimiani",
                    "suffix": ""
                },
                {
                    "first": "MR",
                    "middle": [],
                    "last": "Jacobs",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Infect Dis Clin N Am",
            "volume": "30",
            "issn": "",
            "pages": "323-345",
            "other_ids": {
                "DOI": [
                    "10.1016/j.idc.2016.02.002"
                ]
            }
        }
    }
}