{
    "paper_id": "PMC7153046",
    "metadata": {
        "title": "Letter to the Editor Regarding the Viewpoint \u201cEvidence\nof the COVID-19 Virus Targeting the CNS: Tissue Distribution, Host\u2013Virus\nInteraction, and Proposed Neurotropic Mechanism\u201d",
        "authors": [
            {
                "first": "Karlo",
                "middle": [],
                "last": "Toljan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Besides\nthe heart, kidneys, and testes having been found as initial\nsites of angiotensin-converting enzyme 2 (ACE2) expression, endothelial\nand neuronal presence was confirmed, with ultimate consensus stating\nthe receptor is almost ubiquitous.2,3 Although mRNA\nexpression showed a clear presence of ACE2 receptor in various human\nneuronal regions, immunohistochemistry for ACE2 receptor of central\nnervous system (CNS) tissue, though with limited description, failed\nto show neuronal or glial positivity but did confirm it in the brain\nvasculature.4 Translating the known about\nthe severe acute respiratory syndrome coronavirus (SARS-CoV), it has\nbeen shown that full interaction of the virus with the ACE2 receptor\nis enabled once the viral spike protein is cleaved by surface proteases,\nnamely, transmembrane serine protease 2 (TMPRSS2),5 although some findings argue against the strict necessity\nof the step.6 Moreover, lysosomal related\ncomponents, namely, cathepsin L, 1-phosphatidylinositol 3-phosphate\n5-kinase, and two pore channel-2 also have a role in initial viral\ninteraction with the host cell.6,7 An additional receptor\nbinding the spike protein has been possibly recognized in CD147 by\nan in vitro experiment.8 Both cathepsin\nL and CD147 are widely present in the CNS.9,10 TMPRSS2\nis only scantly present in the brain (brainstem, globus pallidus,\ninsula, temporal lobe, occipital lobe, and postcentral gyrus).11 A detailed study of nasal epithelium did not\nshow TMPRSS2 presence in the neuronal component but did on the respiratory\nepithelium.12",
            "cite_spans": [
                {
                    "start": 243,
                    "end": 244,
                    "mention": "2",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 245,
                    "end": 246,
                    "mention": "3",
                    "ref_id": "BIBREF22"
                },
                {
                    "start": 548,
                    "end": 549,
                    "mention": "4",
                    "ref_id": "BIBREF25"
                },
                {
                    "start": 839,
                    "end": 840,
                    "mention": "5",
                    "ref_id": "BIBREF26"
                },
                {
                    "start": 911,
                    "end": 912,
                    "mention": "6",
                    "ref_id": "BIBREF27"
                },
                {
                    "start": 1107,
                    "end": 1108,
                    "mention": "6",
                    "ref_id": "BIBREF27"
                },
                {
                    "start": 1109,
                    "end": 1110,
                    "mention": "7",
                    "ref_id": "BIBREF28"
                },
                {
                    "start": 1224,
                    "end": 1225,
                    "mention": "8",
                    "ref_id": "BIBREF29"
                },
                {
                    "start": 1283,
                    "end": 1284,
                    "mention": "9",
                    "ref_id": "BIBREF30"
                },
                {
                    "start": 1285,
                    "end": 1287,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1424,
                    "end": 1426,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1558,
                    "end": 1560,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Receptors for Viral Entry and Their Distribution",
            "ref_spans": []
        },
        {
            "text": "Notably, SARS-CoV was\nconfirmed in postmortem neurons and glial\ncells of human patients with fatal systemic manifestations.13 A non-peer-reviewed report claims there was\na case of symptomatic encephalitis with detected SARS-CoV-2 in cerebrospinal\nfluid.14",
            "cite_spans": [
                {
                    "start": 123,
                    "end": 125,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 253,
                    "end": 255,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Receptors for Viral Entry and Their Distribution",
            "ref_spans": []
        },
        {
            "text": "The spread of the virus and the neuroinvasive\npotential have been\nproposed according to the known routes of SARS-CoV15 and a growing body of findings specific for SARS-CoV-2.16 Although hematologic spread is a known route\nfor systemic viral dissemination, it has been postulated that the\nvirus could also advance from the periphery to the CNS via retrograde\nneuronal transport and synaptic connections, notably vagal nerve afferents\nfrom the lung.16 The concept of the pentapartite\nsynapse as a nexus of endothelial, glial, neuronal, and immune cells\nopens a possibility for this mechanism.17 However, with the growing findings of SARS CoV-2 infecting cells\nin the gastrointestinal tract,18 the neuroinvasive\npotential could encompass the enteric nervous system and subsequent\nvagal and sympathetic afferents to the CNS. Previous experimental\nwork on coronaviruses has shown retrograde neuronal transport as a\nviable route for viral invasion,19 but\nit remains to be established for SARS-CoV-2 in particular. Exosomal\ncellular transport has also been shown as a mode of systemic viral\ndissemination, and it could include SARS-CoV-2.20 Following SARS-CoV-2 infection and immune activation, CD4+ T-cells produce granulocyte-macrophage colony-stimulating\nfactor which further induces macrophage lines to secrete interleukin-6\n(IL-6), occasionally causing a vicious cycle of cytokine storm, a\nmost concerning clinical presentation. However, lymphatic spread of\nthe virus via immune cells has not been postulated as no experimental\ndata confirmed viral presence in these cells or the presence of ACE2\nreceptor4,21",
            "cite_spans": [
                {
                    "start": 116,
                    "end": 118,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 174,
                    "end": 176,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 447,
                    "end": 449,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 590,
                    "end": 592,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 688,
                    "end": 690,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 942,
                    "end": 944,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1131,
                    "end": 1133,
                    "mention": "20",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1603,
                    "end": 1604,
                    "mention": "4",
                    "ref_id": "BIBREF25"
                },
                {
                    "start": 1605,
                    "end": 1607,
                    "mention": "21",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "Host\u2013Virus\nInteraction Routes",
            "ref_spans": []
        },
        {
            "text": "Secondary neuroinflammation related to systemic\nimmune activation\ncould be mediated by lymphatic routes22 which could contribute to encephalopathy, a common neurological\nmanifestation of SARS-CoV-2 infection.23,24 Per experimental\nexperience with SARS-CoV, aside from this secondary neuroinflammation,\nprimary neuronal infection results in increased secretion of IL-6,15 an already recognized salient molecule implicated\nin cytokine storm. The aforementioned case of encephalitis may corroborate\nsuch a notion.14 Additionally, systemic\ninflammation related metabolic and homeostatic derangements contributes\nto encephalopathy, but it may also predispose one to stroke, which\nhas been noted to occur more commonly in severe clinical presentations.25 Besides the acute neurological manifestations\nof SARS CoV-2 infection, further monitoring for long-term sequelae\nmay reveal viral contribution in pathophysiology or increased risk\nfor neuroinflammatory and neurodegenerative diseases. It has been\nshown in animal and human studies that coronaviruses could possibly\nbe implicated in the pathogenesis of Parkinson\u2019s disease,26 acute disseminated encephalomyelitis,27 or multiple sclerosis.23,28 In already established neurologic patients and even more so, those\nunder active immunomodulating therapies, noticing trends in acute\nand chronic disease presentation or course may provide valuable insights\nin guiding acute management and determining the neuropathologic aspects\nof SARS-CoV-2. Exemplary neurologic features of SARS CoV-2 include\nanosmia and dysgeusia.29 For the former,\nit could be argued it is more, or even exclusively, related to the\nrespiratory epithelium infection and subsequent inflammation, but\nfor the latter it still remains an open question. High expression\nof ACE2 was found on tongue epithelium,30 but animal studies show ACE2 expression in the nucleus of the solitary\ntract,31 which could point to central cause\nof dysgeusia and a possible neuroinvasive route by continuous local\nor retrograde vagal axonal transport. Further research is warranted,\nand this letter should supplement the original publication to expand\nthe scope and understanding of pathogenesis, and posit some reasonable\nhypotheses that could be scientifically scrutinized.",
            "cite_spans": [
                {
                    "start": 103,
                    "end": 105,
                    "mention": "22",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 208,
                    "end": 210,
                    "mention": "23",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 211,
                    "end": 213,
                    "mention": "24",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 368,
                    "end": 370,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 510,
                    "end": 512,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 746,
                    "end": 748,
                    "mention": "25",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 1120,
                    "end": 1122,
                    "mention": "26",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 1160,
                    "end": 1162,
                    "mention": "27",
                    "ref_id": "BIBREF19"
                },
                {
                    "start": 1185,
                    "end": 1187,
                    "mention": "23",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 1188,
                    "end": 1190,
                    "mention": "28",
                    "ref_id": "BIBREF20"
                },
                {
                    "start": 1558,
                    "end": 1560,
                    "mention": "29",
                    "ref_id": "BIBREF21"
                },
                {
                    "start": 1815,
                    "end": 1817,
                    "mention": "30",
                    "ref_id": "BIBREF23"
                },
                {
                    "start": 1895,
                    "end": 1897,
                    "mention": "31",
                    "ref_id": "BIBREF24"
                }
            ],
            "section": "Clinical Pathophysiological\nImplications",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Evidence of the COVID-19\nVirus Targeting the CNS: Tissue Distribution, Host\u2013Virus Interaction,\nand Proposed Neurotropic Mechanisms",
            "authors": [],
            "year": 2020,
            "venue": "ACS Chem.\nNeurosci.",
            "volume": "11",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1021/acschemneuro.0c00122"
                ]
            }
        },
        "BIBREF1": {
            "title": "Catepsin L",
            "authors": [],
            "year": null,
            "venue": "The Human Protein\nAtlas",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "TMPRSS2",
            "authors": [],
            "year": null,
            "venue": "The Human Protein Atlas",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Non-neural expression of SARS-CoV-2 entry genes\nin the olfactory epithelium suggests mechanisms underlying anosmia\nin COVID-19 patients",
            "authors": [],
            "year": 2020,
            "venue": "bioRxiv",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1101/2020.03.25.009084"
                ]
            }
        },
        "BIBREF4": {
            "title": "Detection of severe\nacute respiratory syndrome coronavirus in the brain: potential role\nof the chemokine mig in pathogenesis",
            "authors": [],
            "year": 2005,
            "venue": "Clin.\nInfect. Dis.",
            "volume": "41",
            "issn": "8",
            "pages": "1089-1096",
            "other_ids": {
                "DOI": [
                    "10.1086/444461"
                ]
            }
        },
        "BIBREF5": {
            "title": "Beijing hospital confirms nervous system infections by novel coronavirus",
            "authors": [],
            "year": 2020,
            "venue": "XinhuaNet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Severe Acute\nRespiratory Syndrome Coronavirus Infection Causes Neuronal Death in\nthe Absence of Encephalitis in Mice Transgenic for Human ACE2",
            "authors": [],
            "year": 2008,
            "venue": "J. Virol.",
            "volume": "82",
            "issn": "15",
            "pages": "7264-7275",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.00737-08"
                ]
            }
        },
        "BIBREF7": {
            "title": "The neuroinvasive\npotential of SARS-CoV2 may be at\nleast partially responsible for the respiratory failure of COVID-19\npatients",
            "authors": [],
            "year": 2020,
            "venue": "J. Med. Virol.",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1002/jmv.25728"
                ]
            }
        },
        "BIBREF8": {
            "title": "Neurons, Glia, Extracellular Matrix and Neurovascular Unit: A Systems\nBiology Approach to the Complexity of Synaptic Plasticity in Health\nand Disease",
            "authors": [],
            "year": 2020,
            "venue": "Int. J. Mol. Sci.",
            "volume": "21",
            "issn": "4",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.3390/ijms21041539"
                ]
            }
        },
        "BIBREF9": {
            "title": "Covid-19\nand the Digestive System",
            "authors": [],
            "year": 2020,
            "venue": "J. Gastroenterol.\nHepatol.",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1111/jgh.15047"
                ]
            }
        },
        "BIBREF10": {
            "title": "Axonal Transport Enables Neuron-to-Neuron\nPropagation of Human Coronavirus OC43",
            "authors": [],
            "year": 2018,
            "venue": "J. Virol.",
            "volume": "92",
            "issn": "17",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.00404-18"
                ]
            }
        },
        "BIBREF11": {
            "title": "Angiotensin-converting\nenzyme 2 in the brain: properties\nand future directions",
            "authors": [],
            "year": 2008,
            "venue": "J. Neurochem.",
            "volume": "107",
            "issn": "6",
            "pages": "1482-1494",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1471-4159.2008.05723.x"
                ]
            }
        },
        "BIBREF12": {
            "title": "Exosome Biogenesis, Regulation, and Function in Viral\nInfection",
            "authors": [],
            "year": 2015,
            "venue": "Viruses",
            "volume": "7",
            "issn": "9",
            "pages": "5066-5083",
            "other_ids": {
                "DOI": [
                    "10.3390/v7092862"
                ]
            }
        },
        "BIBREF13": {
            "title": "ACE2",
            "authors": [],
            "year": null,
            "venue": "The Human\nProtein Atlas",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "CNS lymphatic drainage\nand neuroinflammation are regulated by meningeal lymphatic vasculature",
            "authors": [],
            "year": 2018,
            "venue": "Nat. Neurosci.",
            "volume": "21",
            "issn": "10",
            "pages": "1380-1391",
            "other_ids": {
                "DOI": [
                    "10.1038/s41593-018-0227-9"
                ]
            }
        },
        "BIBREF15": {
            "title": "Viral-induced suppression\nof self-reactive T cells:\nLessons from neurotropic coronavirus-induced demyelination",
            "authors": [],
            "year": 2017,
            "venue": "J. Neuroimmunol.",
            "volume": "308",
            "issn": "",
            "pages": "12-16",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jneuroim.2017.01.003"
                ]
            }
        },
        "BIBREF16": {
            "title": "Neurological Complications\nof Coronavirus Disease (COVID-19): Encephalopathy",
            "authors": [],
            "year": 2020,
            "venue": "Cureus",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.7759/cureus.7352"
                ]
            }
        },
        "BIBREF17": {
            "title": "Acute Cerebrovascular Disease Following\nCOVID-19: A Single Center, Retrospective, Observational Study",
            "authors": [],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.2139/ssrn.3550025"
                ]
            }
        },
        "BIBREF18": {
            "title": "Cerebrospinal\nfluid antibodies to\ncoronavirus in patients with Parkinson\u2019s disease",
            "authors": [],
            "year": 1992,
            "venue": "Mov. Disord.",
            "volume": "7",
            "issn": "2",
            "pages": "153-158",
            "other_ids": {
                "DOI": [
                    "10.1002/mds.870070210"
                ]
            }
        },
        "BIBREF19": {
            "title": "Detection\nof coronavirus in the central nervous system of a child with acute\ndisseminated encephalomyelitis",
            "authors": [],
            "year": 2004,
            "venue": "Pediatrics",
            "volume": "113",
            "issn": "1",
            "pages": "e73-6",
            "other_ids": {
                "DOI": [
                    "10.1542/peds.113.1.e73"
                ]
            }
        },
        "BIBREF20": {
            "title": "Coronaviruses in brain tissue from\npatients with multiple sclerosis",
            "authors": [],
            "year": 2001,
            "venue": "Acta Neuropathol.",
            "volume": "101",
            "issn": "6",
            "pages": "601-604",
            "other_ids": {
                "DOI": [
                    "10.1007/s004010000331"
                ]
            }
        },
        "BIBREF21": {
            "title": "AAO-HNS: Anosmia,\nHyposmia, and Dysgeusia Symptoms of Coronavirus Disease",
            "authors": [],
            "year": null,
            "venue": "ENTConnect",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF22": {
            "title": "Quantitative mRNA\nexpression profiling of ACE 2, a novel homologue of angiotensin converting\nenzyme",
            "authors": [],
            "year": 2002,
            "venue": "FEBS Lett.",
            "volume": "532",
            "issn": "1\u20132",
            "pages": "107-110",
            "other_ids": {
                "DOI": [
                    "10.1016/S0014-5793(02)03640-2"
                ]
            }
        },
        "BIBREF23": {
            "title": "High expression of ACE2\nreceptor of 2019-nCoV on the epithelial cells of oral mucosa",
            "authors": [],
            "year": 2020,
            "venue": "Int. J. Oral Sci.",
            "volume": "12",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1038/s41368-020-0074-x"
                ]
            }
        },
        "BIBREF24": {
            "title": "Gene transfer\nof angiotensin-converting\nenzyme 2 in the nucleus tractus solitarius improves baroreceptor heart\nrate reflex in spontaneously hypertensive rats",
            "authors": [],
            "year": 2011,
            "venue": "JRAAS",
            "volume": "12",
            "issn": "4",
            "pages": "456-461",
            "other_ids": {
                "DOI": [
                    "10.1177/1470320311412809"
                ]
            }
        },
        "BIBREF25": {
            "title": "Tissue distribution\nof ACE2 protein, the functional\nreceptor for SARS coronavirus. A first step in understanding SARS\npathogenesis",
            "authors": [],
            "year": 2004,
            "venue": "J. Pathol.",
            "volume": "203",
            "issn": "2",
            "pages": "631-637",
            "other_ids": {
                "DOI": [
                    "10.1002/path.1570"
                ]
            }
        },
        "BIBREF26": {
            "title": "SARS-CoV-2 Cell Entry\nDepends on ACE2 and TMPRSS2 and Is Blocked by a Clinically Proven\nProtease Inhibitor",
            "authors": [],
            "year": 2020,
            "venue": "Cell",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1016/j.cell.2020.02.052"
                ]
            }
        },
        "BIBREF27": {
            "title": "Characterization of spike glycoprotein of SARS-CoV-2\non virus entry and its immune cross-reactivity with SARS-CoV",
            "authors": [],
            "year": 2020,
            "venue": "Nat. Commun.",
            "volume": "11",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1038/s41467-020-15562-9"
                ]
            }
        },
        "BIBREF28": {
            "title": "TMPRSS2 Contributes\nto Virus Spread\nand Immunopathology in the Airways of Murine Models after Coronavirus\nInfection",
            "authors": [],
            "year": 2019,
            "venue": "J. Virol.",
            "volume": "93",
            "issn": "6",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.01815-18"
                ]
            }
        },
        "BIBREF29": {
            "title": "SARS-CoV-2 invades\nhost cells via a novel route:\nCD147-spike protein",
            "authors": [],
            "year": 2020,
            "venue": "bioRxiv",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1101/2020.03.14.988345"
                ]
            }
        },
        "BIBREF30": {
            "title": "Human brain gene expression profiles\nof the cathepsin V and cathepsin L cysteine proteases, with the PC1/3\nand PC2 serine proteases, involved in neuropeptide production",
            "authors": [],
            "year": 2018,
            "venue": "Heliyon.",
            "volume": "4",
            "issn": "7",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1016/j.heliyon.2018.e00673"
                ]
            }
        }
    }
}