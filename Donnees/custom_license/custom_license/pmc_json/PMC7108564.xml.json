{
    "paper_id": "PMC7108564",
    "metadata": {
        "title": "Commentary: Vaccines and unexpected events",
        "authors": [
            {
                "first": "Andrew",
                "middle": [],
                "last": "Hall",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Exposure to antigenic stimulation of our immune system begins in utero. It intensifies from the moment of birth with multiple infectious stimuli. These infections occur with a range of intensity and in a vast array of sequence. Thus a child may meet respiratory syncytial virus before or after influenza or their first coronavirus. Not only the sequence but the age at which they meet each infection may be critical to how they respond. This is a topic of increasing interest in relation to the Th1/2 paradigm, immunological programming and the epidemic of atopic disorders seen in high and middle income countries. Dissecting out the various influences presents a major challenge to modern epidemiology, not least because of the difficulties of measuring the timing and sequence of infections using current tools.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In contrast vaccines are delivered with precise timing and in a known dose. The sequence and age at delivery is frequently recorded. This information can be statistically analysed and related to any outcome. Since vaccines, unlike infections, are deliberately given there is concern that they do not do harm. The public regards them as a much more likely cause of adverse events than the more common infections of childhood.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "On this background Peter Aaby and his group have been examining the effects of vaccination on mortality over the last two decades. His initial observation of an unexpected effect of high dose Edmondson Zagreb measles vaccine on mortality in Guinea Bissau1 has undoubtedly been the major source of this interest. That observation was remarkable in that the vaccine given in the first 6 months of life only exerted an effect on mortality in the second year of life\u2014and then much more on girls than boys. The findings were replicated by analysis of trials carried out in Senegal, Haiti, and Sudan. The underlying biological mechanism is still not understood although Aaby has recently published a re-analysis proposing a relationship to the sequence of vaccination.2",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The great strength of that original observation was that it was a randomized trial, and there were other trials in which the finding was replicated.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "However, since then the group has produced studies that are observational.3,4 These studies are much more difficult to evaluate, as has been pointed out by Paul Fine,5 in particular the reasons for some children to get vaccinated in these societies at that period of time may introduce bias. The justification for the choice of time intervals (why 6 months after diphtheria-tetanus-pertussis?) and analytical methods is not always clear. An example of the latter is the use of the last antigen delivered as the determinant of the sex ratio of mortality. It is quite unclear whether this is an artifact of the modelling process or an interesting biological effect. Unfortunately the basis for doing it forms part of a PhD which is not readily accessible.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The quality of data on vaccination, as discussed above, compared with that on other variables, may also lead to skew in the analysis. It would be of interest to see the application of a conceptual framework to this data.6 This would allow a better evaluation of the strength of different aspects of the data.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "All of this makes it difficult for the reader to assess potential bias and uncontrolled confounding. This situation is made more difficult because there is no independent replication of the findings. Are they likely to be globally applicable? West Africa is unusual in that it has had very high case fatality rates from measles until quite recent times. Peter Aaby made the important observation that this was largely related to where you become infected\u2014the high fatality is associated with transmission occurring at home.7 West Africa stands out in the world because of the extremely high density of children per dwelling. Could this also be related to these surprising findings?",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "What is the way forward? WHO had an independent assessment made of the data by consultants sent to Guinea Bissau. They have funded studies in other parts of the world including Bangladesh. These should soon be in print and should resolve at least whether this is a global problem. In West Africa there needs to be analysis of other demographic surveillance systems that can address this issue, such exist in Ghana and The Gambia, and this needs to be done independently of Peter Aaby and his group.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The ultimate answer is to reduce mortality to the point at which these effects are not an issue\u2014sadly that seems unlikely\u2014particularly in the war torn countries of sub-Saharan Africa. Ironically the best hope of doing so is with vaccines!",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 1993,
            "venue": "J Pediatr",
            "volume": "122",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 1995,
            "venue": "BMJ",
            "volume": "311",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2000,
            "venue": "BMJ",
            "volume": "321",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2000,
            "venue": "BMJ",
            "volume": "321",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 1997,
            "venue": "Int J Epidemiol",
            "volume": "26",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 1990,
            "venue": "Int J Epidemiol",
            "volume": "19",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}