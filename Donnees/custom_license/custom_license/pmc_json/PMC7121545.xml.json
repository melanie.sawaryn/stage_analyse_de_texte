{
    "paper_id": "PMC7121545",
    "metadata": {
        "title": "Sore Throat",
        "authors": [
            {
                "first": "Elana",
                "middle": [],
                "last": "Sydney",
                "suffix": "",
                "email": "elana.sydney@nbbn.net",
                "affiliation": {}
            },
            {
                "first": "Eleanor",
                "middle": [],
                "last": "Weinstein",
                "suffix": "",
                "email": "eleanor.weinstein@nbhn.net",
                "affiliation": {}
            },
            {
                "first": "Lisa",
                "middle": [
                    "M."
                ],
                "last": "Rucker",
                "suffix": "",
                "email": "lisa.rucker@nbhn.net",
                "affiliation": {}
            },
            {
                "first": "Lori",
                "middle": [],
                "last": "Ciuffo",
                "suffix": "",
                "email": "lori.ciuffo@nbhn.net",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Although the most common cause of pharyngitis is viral, Group A streptococcal (GAS) pharyngitis is a significant cause of community infections [1]. A majority of patients with pharyngitis receive presumptive antibiotic therapy. One report estimates about 60% of adults seen in the USA in 2010 for a complaint of sore throat received an antibiotic prescription [2]. Group A streptococcus typically presents with abrupt onset of symptoms including sore throat, fever, and anterior neck pain related to lymphadenopathy. These symptoms may occur in association with headache or malaise. Late winter and early spring are peak GAS seasons. The infection is transmitted via respiratory secretions, and the incubation period is 2\u20134 days. The goal of therapy is to reduce complications including acute rheumatic fever\nand glomerulonephritis [3, 4]. Many patients with viral pharyngitis also have signs and symptoms associated with a viral upper respiratory infection including nasal congestion, coryza, hoarseness, sinus discomfort, ear pain, or cough. Coinfections with streptococci and viruses may occur [2].",
            "cite_spans": [
                {
                    "start": 144,
                    "end": 145,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 361,
                    "end": 362,
                    "mention": "2",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 833,
                    "end": 834,
                    "mention": "3",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 836,
                    "end": 837,
                    "mention": "4",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1098,
                    "end": 1099,
                    "mention": "2",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Viruses including adenovirus, influenza virus, parainfluenza virus, rhinovirus, and respiratory syncytial virus are frequent causes of acute pharyngitis. Other viral agents include coxsackievirus, echovirus, coronavirus, enterovirus, cytomegalovirus (CMV), human immunodeficiency virus (HIV), and herpes simplex virus. Epstein-Barr virus is a frequent cause of acute pharyngitis accompanied by other clinical features of infectious mononucleosis such as generalized fatigue, lymphadenopathy, and splenomegaly. Systemic infections with rubella virus or measles virus can be associated with acute pharyngitis [5, 6]. Other bacterial causes of acute pharyngitis include groups C and G beta-hemolytic streptococci, Corynebacterium diphtheria, Arcanobacterium haemolyticum, Neisseria gonorrhoeae, Chlamydia pneumoniae, Francisella tularensis, Fusobacterium necrophorum, and Mycoplasma pneumoniae [1]. Noninfectious causes include seasonal or environmental allergies, smoking or secondhand smoking, poorly humidified air, and gastroesophageal reflux disease (GERD) [3].",
            "cite_spans": [
                {
                    "start": 608,
                    "end": 609,
                    "mention": "5",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 611,
                    "end": 612,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 892,
                    "end": 893,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1060,
                    "end": 1061,
                    "mention": "3",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Decision-Making/Differential Diagnosis ",
            "ref_spans": []
        },
        {
            "text": "The goal of the evaluation of adults with acute pharyngitis is to exclude potentially dangerous causes, to identify treatable causes, and to prevent complications including acute rheumatic fever. History of exposure to strep pharyngitis with exam findings including pharyngeal erythema, fever, tonsillar exudates with hypertrophy, tender and enlarged anterior cervical lymph nodes, and palatal petechiae is highly suspicious of GAS. Lymphadenopathy in any area other than the anterior cervical chain is not typical of GAS, but is common in mononucleosis. The presence of rash should be noted. The appearance of a whitish exudate in the mouth and pharynx (thrush) suggests fungal infection, seen in immunocompromised patients. Viral symptoms may include conjunctivitis, coryza, cough, diarrhea, coarseness, ulcerative stomatitis, or viral exanthem. Patients who present with unusually severe signs and symptoms, including secretions, drooling, dysphonia, muffled voice, or neck swelling particularly if they have difficulty swallowing, should be evaluated for rare but serious throat infections/local abscesses [2, 4\u20136]. College-aged patients should be asked about sexual practices and risks, as they have an increased incidence of oral chlamydia and gonorrhea infections.",
            "cite_spans": [
                {
                    "start": 1111,
                    "end": 1112,
                    "mention": "2",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1114,
                    "end": 1115,
                    "mention": "4",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1116,
                    "end": 1117,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Key History and Physical Exam ",
            "ref_spans": []
        },
        {
            "text": "The modified Centor score (also known as McIsaac score) is a validated decision-making instrument that utilizes patient age and four specific signs and symptoms to determine the likelihood of having GAS [1, 7].",
            "cite_spans": [
                {
                    "start": 204,
                    "end": 205,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 207,
                    "end": 208,
                    "mention": "7",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Diagnosis of Bacterial Pharyngitis ",
            "ref_spans": []
        },
        {
            "text": "\n\n",
            "cite_spans": [],
            "section": "Modified Centor score [#!start#7#!sep#CR7#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "The likelihood of GAS increases with the score; however, the score is equally useful for identifying patients for whom neither microbiologic tests nor antimicrobial therapy are necessary. Patients with a score of 1 or less are unlikely to have GAS and should not undergo further RADT testing or receive antibiotic treatment. Patients with a score of 4 or more should not be tested and antibiotics should be started. Patients with scores of 2 or 3 should receive RADT and, if indicated based on test results, given antibiotics. Other factors linked to increased likelihood of GAS infection are recent contact with a person with documented streptococcal infection and residence in a dormitory or group home.",
            "cite_spans": [],
            "section": "Modified Centor score [#!start#7#!sep#CR7#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Throat culture has been considered the gold standard to establish the microbial cause of acute pharyngitis. However, compared with RADT, culture results are not available for 24\u201348 and can cause a delay in diagnosis. Throat culture is primarily used as a backup test in patients with negative RADT where clinical concern for GAS or bacterial pharyngitis is high [1, 7].",
            "cite_spans": [
                {
                    "start": 363,
                    "end": 364,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 366,
                    "end": 367,
                    "mention": "7",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Modified Centor score [#!start#7#!sep#CR7#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Rapid antigen detection testing is 70\u201390% sensitive and 90\u2013100% specific. The use of antibiotics should usually be based on RADT results. Since the RADT is not 100% sensitive, if there is such a high level of suspicion of GAS infection, warranting empiric antibiotic treatment and then testing are not economically sensible.",
            "cite_spans": [],
            "section": "Modified Centor score [#!start#7#!sep#CR7#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Throat culture is 90\u201395% sensitive and 95\u201399% specific. For patients with a modified Centor score of 1, but who are high risk (e.g., poorly controlled diabetics, immunocompromised patients, chronic steroid users, patients with a history of rheumatic fever, childcare workers, and the elderly), consider doing throat culture or RADT.",
            "cite_spans": [],
            "section": "Modified Centor score [#!start#7#!sep#CR7#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "Culture results should guide therapy [1, 3].",
            "cite_spans": [
                {
                    "start": 38,
                    "end": 39,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 41,
                    "end": 42,
                    "mention": "3",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Modified Centor score [#!start#7#!sep#CR7#!sep#bibr#!end#]",
            "ref_spans": []
        },
        {
            "text": "See Fig. 13.1 for the modified Centor decision tree.\n",
            "cite_spans": [],
            "section": "Modified Centor score [#!start#7#!sep#CR7#!sep#bibr#!end#]",
            "ref_spans": [
                {
                    "start": 9,
                    "end": 13,
                    "mention": "13.1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "\nUse the modified Centor score to guide testing and treatment of GAS.If there is high suspicion of GAS, start empiric treatment.The goal of treatment is to prevent complications and transmission. Treatment does not significantly shorten the duration of symptoms.\n",
            "cite_spans": [],
            "section": "Clinical Pearls ::: Treatment ",
            "ref_spans": []
        },
        {
            "text": "\nEvaluate for serious complications (abscess/local space infection) in patients presenting with sore throat and any of the following concerning symptoms: secretions, drooling, dysphonia, muffled voice, or neck swelling.Consider underlying immunodeficiencies in patients presenting with thrush.\n",
            "cite_spans": [],
            "section": "Don\u2019t Miss This! ::: Treatment ",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 13.1: Modified Centor score and management options using clinical decision rule. Other factors should be considered (e.g., a score of 1, but recent family contact with documented streptococcal infection). GAS Group A beta-hemolytic streptococcus, RADT rapid antigen detection testing. Adapted from McIsaac WJ, White D, Tannenbaum D, Low DE. A clinical score to reduce unnecessary antibiotic use in patients with sore throat. CMAJ. 1998;158(1):79 [11]",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Diagnosis and treatment of streptococcal pharyngitis",
            "authors": [
                {
                    "first": "BA",
                    "middle": [],
                    "last": "Choby",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Am Fam Phys",
            "volume": "79",
            "issn": "5",
            "pages": "383-390",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Antibiotic treatment of adults with sore throat by community primary care physicians: a national survey, 1989-19999",
            "authors": [
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Linder",
                    "suffix": ""
                },
                {
                    "first": "RS",
                    "middle": [],
                    "last": "Statford",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "JAMA",
            "volume": "286",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1001/jama.286.10.1181"
                ]
            }
        },
        "BIBREF2": {
            "title": "A clinical score to reduce unnecessary antibiotic use in patients with sore throat",
            "authors": [
                {
                    "first": "WJ",
                    "middle": [],
                    "last": "McIsaac",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "White",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Tannenbaum",
                    "suffix": ""
                },
                {
                    "first": "DE",
                    "middle": [],
                    "last": "Low",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "CMAJ",
            "volume": "158",
            "issn": "1",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Clinical practice guidelines for the diagnosis and management of Group A Streptococcal pharyngitis: 2012 update by the Infection Disease Society of America",
            "authors": [
                {
                    "first": "ST",
                    "middle": [],
                    "last": "Shulman",
                    "suffix": ""
                },
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Bisno",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Clin Infect Dis",
            "volume": "55",
            "issn": "10",
            "pages": "e86-e102",
            "other_ids": {
                "DOI": [
                    "10.1093/cid/cis629"
                ]
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Pharyngitis in adults: the presence and coexistence of viruses and bacterial organisms",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Huovinen",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Lahtonen",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ziegler",
                    "suffix": ""
                }
            ],
            "year": 1989,
            "venue": "Ann Intern Med",
            "volume": "110",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.7326/0003-4819-110-8-612"
                ]
            }
        },
        "BIBREF7": {
            "title": "Group A streptococci, mycoplasmas and viruses associated with acute pharyngitis",
            "authors": [
                {
                    "first": "WP",
                    "middle": [],
                    "last": "Glezen",
                    "suffix": ""
                },
                {
                    "first": "WA",
                    "middle": [],
                    "last": "Clyde",
                    "suffix": "Jr"
                },
                {
                    "first": "RJ",
                    "middle": [],
                    "last": "Senior",
                    "suffix": ""
                }
            ],
            "year": 1967,
            "venue": "JAMA",
            "volume": "201",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1001/jama.1967.03130190061007"
                ]
            }
        },
        "BIBREF8": {
            "title": "The diagnosis of strep throat in adults in the emergency room",
            "authors": [
                {
                    "first": "RM",
                    "middle": [],
                    "last": "Centor",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Witherspoon",
                    "suffix": ""
                },
                {
                    "first": "HP",
                    "middle": [],
                    "last": "Dalton",
                    "suffix": ""
                }
            ],
            "year": 1981,
            "venue": "Med Decis Making",
            "volume": "1",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1177/0272989X8100100304"
                ]
            }
        },
        "BIBREF9": {
            "title": "Principles of appropriate antibiotic use for acute pharyngitis in adults",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Snow",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Mottur-Pilson",
                    "suffix": ""
                },
                {
                    "first": "RJ",
                    "middle": [],
                    "last": "Cooper",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Ann Intern Med",
            "volume": "134",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.7326/0003-4819-134-6-200103200-00018"
                ]
            }
        },
        "BIBREF10": {
            "title": "Antibiotic prescribing to adults with sore throat in the United States, 1997-2010",
            "authors": [
                {
                    "first": "ML",
                    "middle": [],
                    "last": "Barett",
                    "suffix": ""
                },
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Linder",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "JAMA Intern Med",
            "volume": "174",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1001/jamainternmed.2013.11673"
                ]
            }
        }
    }
}