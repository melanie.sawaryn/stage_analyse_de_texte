{
    "paper_id": "PMC7111883",
    "metadata": {
        "title": "Detection of Human Bocavirus in Children Hospitalized because of Acute Gastroenteritis",
        "authors": [
            {
                "first": "Jae",
                "middle": [
                    "In"
                ],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ju-Youn",
                "middle": [],
                "last": "Chung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tae",
                "middle": [
                    "Hee"
                ],
                "last": "Han",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mi-Ok",
                "middle": [],
                "last": "Song",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Eung-Soo",
                "middle": [],
                "last": "Hwang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "\nStudy population and sample collection. Between January of 2005 and December of 2006, a total of 1063 stool samples were prospectively collected from 962 children \u2a7e5 years old who were hospitalized, because of acute gastroenteritis, at Sanggyepaik Hospital, in Seoul, Korea. After exclusion of duplicate samples, a total of 962 stool samples were included in the study. Stool samples were collected from each child \u2a7e48 h after hospitalization. Acute diarrhea was defined as 12 episodes of loose or watery stools within a 24-h period. The study protocol was approved by the ethics committee of Sanggyepaik Hospital, Inje University.",
            "cite_spans": [],
            "section": "Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "\nBacteriology. All fresh stool specimens were plated on MacConkey agar, salmonella-shigella agar, charcoalcefoperazone-desoxycholate agar, thiosulfate-citrate-bile salts-sucrose agar, cefsulodin-irgasan-novobiocin agar, and tryptose-sulfite-cycloserine agar, to isolate Escherichia coli and Salmonella, Shigella, Campylobacter, Vibrio, Yersinia, and Clostridium species, respectively. Polymerase chain reaction (PCR) assays were performed to identify enterovirulent E. coli and Clostridium species, respectively.",
            "cite_spans": [],
            "section": "Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "\nDetection of viruses. The stool samples were diluted 1:10 in PBS and then were vortexed and centrifuged. Supernatants were collected and were frozen at -70\u00b0C until use. The identification of rotavirus and adenoviruses 40 and 41 was performed by use of ELISA kits and Rotaclone and Adenoclone 40/41 (Meridian Bioscience). Total RNA was extracted from 200 \u00b5L of the 10% stool suspensions by use Tri-reagent (Molecular Research Center), as described in the manufacturer's protocol. The final RNA pellets were dissolved in 5 \u00b5L of diethyl pyrocarbonate-treated distilled water and then were used in the reverse transcription (RT) reaction.",
            "cite_spans": [],
            "section": "Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "The seminested PCR for norovirus was performed with primers based on the capsid region, as described elsewhere [10]. NV-GIF1 (5\u2032-CTG CCC GAA TTY GTA AAT GAT GAT-3\u2032) and NV-GIR1 (5\u2032-CCA ACC CAR CCA TTR TAC ATY TG-3\u2032) were used for RT and outer PCR, for genogroup 1 (GI). NV-GIIF1 (5\u2032-GGG AGG GCG ATC GCA ATC T-3\u2032) and NV-GIIR1 (5\u2032-CCR CCI GCA TRI CCR TTR TAC AT-3\u2032) were used for RT and outer PCR, for genotype 2 (GII). For the nested PCR, NVGIF2 (5\u2032-ATG ATG ATG GCG TCT AAG 5\u2032-TTG TGA ATG AAG ATG GCG TCG ART-3\u2032) and NV-GIIF2 (5\u2032-TTG TGA ATG AAG ATG GCG TCG ART-3\u2032) were used.",
            "cite_spans": [
                {
                    "start": 112,
                    "end": 114,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "RT-PCR for human astrovirus was performed with the following primers based on open reading frame-1a: Mon 340 (5\u2032-CGT CAT TAT TTG TTG TCA TAC T-3\u2032) and Mon 348 (5\u2032-AC ATG TGC TGC TGT TAC T ATG-3\u2032). A 5-\u00b5L portion of each RNA extract was mixed with RT mix containing 8 \u00b5L of each dNTP, 5 x 5 \u00b5L of enzyme buffer, 2.5 \u00b5L of Mon 348 primer, 0.5 \u00b5L of RNase inhibitor, and 0.5 \u00b5L of reverse transcriptase. RT reactions were incubated for 60 min at 42\u00b0C and then for 5 min at 95\u00b0C and were soaked at 4\u00b0C. For PCR, 5 \u00b5L of cDNA was used, with 0.5 \u00b5L of Taq DNA polymerase (Takara), and 2.5 \u00b5L of each primer, under the following conditions: 3 min at 94\u00b0C, 30 s at 94\u00b0C, 20 s at 50\u00b0C, 30 s at 72\u00b0C, and 5 min at 72\u00b0C.",
            "cite_spans": [],
            "section": "Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "\nDetection of hBoV. DNA was extracted from the stool samples by use of the QIAamp DNA stool Kit (Qiagen). Two PCR assays using different primer sets were performed on each sample, to detect the NP1 gene and NS1 gene of hBoV, as described elsewhere [2, 4]. Positivity was defined as being definitely established when the results of both PCR assays were positive. Another PCR assay, for the VP1/VP2 gene of hBoV, was performed in positive samples, to compare the diversity of sequences [8]. The amplified DNA fragments for the NP1 gene, NS1 gene, and VP1/VP2 gene were 354, 291, and 819 bp, respectively. To validate the amplification process and to exclude the presence of carryover contamination, positive and negative controls were run for each PCR. The PCR product was sizeseparated on a 2% agarose gel with ethidium bromide and was visualized by use of UV light. Amplicon was purified by use of QIAqucik (Qiagen) and was sequenced in both directions by use of the BigDye Terminator Cycle Sequencing kit (version 3.1; Applied Biosystems). Sequencing products were resolved by use of an ABI 3730 XL autoanalyzer (Applied Biosystems).",
            "cite_spans": [
                {
                    "start": 249,
                    "end": 250,
                    "mention": "2",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 252,
                    "end": 253,
                    "mention": "4",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 485,
                    "end": 486,
                    "mention": "8",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "Patients and Methods",
            "ref_spans": []
        },
        {
            "text": "The median age of the 962 children was 17 months (range, 1\u201360 months), and the sex ratio (male:female) was 1.4:1 (560: 402). The age distribution of the study population was as follows: 437 children <12 months old (45.4% of the study population), 387 children 12\u201335 months old (40.2% of the study population), and 138 children 36\u201360 months old (14.3% of the study population).",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": []
        },
        {
            "text": "Among the 962 stool samples from children with acute gastroenteritis, enteropathogenic bacteria were found in 8.0% (77 patients), viral agents in 44.4% (427 patients). Typical bacterial enteropathogens such as enterohemorraghic E. coli, Campylobacter jejuni, and Yersinia, Shigella, and Vibrio species were not detected in the present study, but Salmonella species were found in 11 patients. The bacteria most frequently found were enteropathogenic E. coli (EPEC), enteroaggregative E. coli (EAEC), and C. perfringens (table 1). Virus and pathogenic bacteria were codetected in 3.3% (32 patients) of the study population.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 519,
                    "end": 526,
                    "mention": "table 1",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "A single viral agent was detected in 39.7% (382 patients) of the study population. Rotavirus and norovirus were the viral agents most frequently detected in the study population, occurring in 25.7% (247 patients) and 13.7% (132 patients) of the study population, respectively; adenovirus, astrovirus, and hBoV were detected in 3.0% (29 patients), 1.1% (11 patients), and 0.8% (8 patients) of the study population, respectively (table 1). More than one viral agent was found in 2.4% (23 patients) of the study population. In addition to being the viral agents that were most frequent, rotavirus and norovirus also were those most frequently codetected, co-occurring in 1.1% (11 patients) of the study population; norovirus and adenovirus were codetected in 0.5% (5 patients), rotavirus and hBoV in 0.3% (3 patients), astrovirus and hBoV in 0.1% (1 patient), norovirus and hBoV in 0.1% (1 patient), rotavirus and astrovirus in 0.1% (1 patient), and norovirus and astrovirus in 0.1% (1 patient). The seasonal distribution of the viral agents associated with acute gastroenteritis is shown in figure 1 and demonstrates an outbreak of norovirus infection between October and December of 2006.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 428,
                    "end": 435,
                    "mention": "table 1",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "DNA sequences of hBoV were found in stool samples from 8 (0.8%) of the 962 children with acute gastroenteritis; most of these hBoV-positive samples were detected during May and June of 2006 (3 samples in May and 3 in June), the others (2 samples) in January of 2005. The age range of the hBoV-positive patients was 7\u201322 months. All 8 of them had diarrhea; other symptoms present were fever, in 5 patients (maximum body temperature, 37.5\u00b0C-39\u00b0C; duration of fever, 1\u20137 days); vomiting, in 3 patients; and rhinorrhea and cough, in 2 patients. Nasopharyngeal specimens were not collected from 2 children with mild respiratory symptoms. All PCR products found to be positive for hBoV were confirmed by sequencing, and the 8 partial sequences of the NP1 gene and the 6 partial sequences of the VP1/VP2 gene were deposited in GenBank (EF441532, EF441534, EF441536-EF441541, and EF441546-EF441550). Direct sequencing of the 8 hBoV-positive PCR products showed that the 4 sequences of the partial NPI gene were completely identical to either ST1 or ST2, the original isolates identified by Allander et al. [2]",
            "cite_spans": [
                {
                    "start": 1099,
                    "end": 1100,
                    "mention": "2",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Results",
            "ref_spans": []
        },
        {
            "text": "In 2005, hBoV was identified in children with LRTI and is now suspected as a likely respiratory pathogen despite the high rate of being codetected with other pathogens [2\u20136]. However, the exact clinical role that newly identified hBoV plays in various clinical diseases, especially gastroenteritis, remains unclear. Bovine parvovirus and canine minute virus, which are included in the genus of bocavirus-like hBoV, are important etiologic agents of diarrhea in calves and dogs and have been shown to destroy small intestinal crypt cells, resulting in dilation of crypts and stunting of villi, which results in the infected cells showing prominent intranuclear inclusion bodies [11]. In recent studies [7\u20139], it has been suggested that hBoV possibly plays a role in acute gastroenteritis in children. In the present study, microbial testing for enteropathogenic bacteria and other common viral etiologic agents was performed to delineate the clinical role played by hBoV infection in children hospitalized with gastroenteritis. The bacterial pathogens most commonly found were EAEC, Clostridium species, and EPEC, which are known to have less pathogenicity in young children; and typical bacterial pathogens were found in only 1.1% of the study population. Codetection of viruses was found in 2.4% of the study population, a rate similar to those reported by previous studies [12]. The results of the present study showed that rotavirus and norovirus were the most common agents, and the coinfection detected most frequently was rotavirus and norovirus, which coincided with the outbreak of norovirus infection during the study period. The fact that PCR was used for detection of norovirus and that the less-sensitive ELISA was used for detection of adenovirus and rotavirus may have affected the results. In the present study, the prevalence of astrovirus was lower than that found by previous studies [13], which may be due not to the sensitivity of primers used but, rather, to the low-prevalence season encompassed by the study period, because we detected astrovirus in 4.1% (33/812) of children hospitalized with gastroenteritis during 2004 [14].",
            "cite_spans": [
                {
                    "start": 169,
                    "end": 172,
                    "mention": "2\u20136",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 678,
                    "end": 680,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 702,
                    "end": 705,
                    "mention": "7\u20139",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 1376,
                    "end": 1378,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1903,
                    "end": 1905,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 2146,
                    "end": 2148,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In nasopharyngeal aspirates of patients with acute respiratory-tract infection, the rate at which hBoV has been codetected with other respiratory viruses is 17.6%\u201355.6% [3, 4, 5, 15]. In the present study, hBoV was codetected with another enteropathogenic virus in 62.5% (5/8) of the hBoV-positive patients, a rate that is consistent with that found in a previous study [9]; the other 37.5% (3/8) of the hBoV-positive patients did not show any coinfection with a bacterial or viral etiologic agent, which suggests that hBoV possibly plays a role in gastroenteritis. In another study, performed during the same period as that of the present study, we detected hBoV in nasopharyngeal specimens from 7.3% (45/613) of children with acute LRTI, and most (44.4% [20/45]) of these detections occurred during May and June of 2006 [5, 16]. The results of the present study showed that, during the same period, the rate of detection of hBoV in acute gastroenteritis was lower than that in acute LRTI and therefore suggest that hBoV may play a minor role in gastroenteritis. These findings are different from those of a Spanish study [9], which showed a high (9.1%) prevalence of hBoV in children with gastroenteritis during a winter period, a prevalence that was similar to that (7.7%) which the study found in children with respiratory illness. Although it is still difficult to explain these studies' differences regarding the role that hBoV plays in gastroenteritis, it should be remembered (1) that respiratory bocavirus infection, even in the absence of gastrointestinal bocavirus infection, may cause gastrointestinal disturbance with diarrhea and (2) that, after systemic infection, global excretion of the virus into various compartments is possible, as has been suggested by Allander et al.'s study of children with acute wheezing [17]. Further studies, including asymptomatic control and respiratory specimens from children with gastroenteritis, are warranted to define the role that hBoV plays in gastroenteritis.",
            "cite_spans": [
                {
                    "start": 170,
                    "end": 171,
                    "mention": "3",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 173,
                    "end": 174,
                    "mention": "4",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 176,
                    "end": 177,
                    "mention": "5",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 179,
                    "end": 180,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 371,
                    "end": 372,
                    "mention": "9",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 823,
                    "end": 824,
                    "mention": "5",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 826,
                    "end": 828,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1124,
                    "end": 1125,
                    "mention": "9",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 1831,
                    "end": 1833,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In conclusion, the present study identified a viral agent in 44.4% of children hospitalized with gastroenteritis. Rotavirus and norovirus were the most frequently detected agents. hBoV was detected in 0.8% of children with gastroenteritis and frequently was codetected with other enteropathogens, which suggests that it might play a minor role in gastroenteritis.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1.: Seasonal distribution of viral agents in children <5 years old who were hospitalized, because of gastroenteritis, at Sanggyepaik Hospital in Seoul, Korea, during 2005\u20132006. The no. in parentheses after each virus in the key indicates the total no. of positive cases; the no. in parentheses after each month indicates the no. of samples tested.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Table 1.: Enteropathogenic agents identified in children with acute gastroenteritis.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Epidemiology of acute viral gastroenteritis in children hospitalized in Rouen, France",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Marie-Cardine",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Gouarlain",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Mouterde",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Clin Infect Dis",
            "volume": "34",
            "issn": "",
            "pages": "1170-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Genogroup-specific PCR primers for detection of Norwalk-like viruses",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Kojima",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kageyama",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Fukushi",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J Virol Methods",
            "volume": "100",
            "issn": "",
            "pages": "107-14",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Comparative pathology of infection by novel diarrhea viruses",
            "authors": [
                {
                    "first": "GA",
                    "middle": [],
                    "last": "Hall",
                    "suffix": ""
                }
            ],
            "year": 1987,
            "venue": "Ciba Found Symp",
            "volume": "128",
            "issn": "",
            "pages": "192-217",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Acute viral gastroenteritis: proportion and clinical relevance of multiple infections in Spanish children",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Roman",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wilhelmi",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Colomina",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Med Microbiol",
            "volume": "52",
            "issn": "",
            "pages": "435-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Astrovirus infection in children",
            "authors": [
                {
                    "first": "JE",
                    "middle": [],
                    "last": "Walter",
                    "suffix": ""
                },
                {
                    "first": "DK",
                    "middle": [],
                    "last": "Mitchell",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Curr Opin Infect Dis",
            "volume": "16",
            "issn": "",
            "pages": "247-53",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Molecular epidemiology of human astrovirus infection in hospitalized children with acute gastroenteritis [in Korean]",
            "authors": [
                {
                    "first": "JY",
                    "middle": [],
                    "last": "Chung",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Huh",
                    "suffix": ""
                },
                {
                    "first": "SW",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Korean J Pediatr Gastroenterol Nutr",
            "volume": "9",
            "issn": "",
            "pages": "139-46",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Frequent detection of bocavirus DNA in German children with respiratory tract infections",
            "authors": [
                {
                    "first": "BB",
                    "middle": [],
                    "last": "Weissbrich",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Neske",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Schubert",
                    "suffix": ""
                },
                {
                    "first": "al",
                    "middle": [],
                    "last": "et",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "BMC Infect Dis",
            "volume": "6",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Detection of viruses identified recently in children with acute wheezing",
            "authors": [
                {
                    "first": "JY",
                    "middle": [],
                    "last": "Chung",
                    "suffix": ""
                },
                {
                    "first": "TH",
                    "middle": [],
                    "last": "Han",
                    "suffix": ""
                },
                {
                    "first": "SW",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "CK",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "ES",
                    "middle": [],
                    "last": "Hwang",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Med Virol",
            "volume": "79",
            "issn": "",
            "pages": "1238-43",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Human bocavirus and acute wheezing in children",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Allander",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Jartti",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gupta",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Clin Infect Dis",
            "volume": "44",
            "issn": "",
            "pages": "904-10",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Cloning of a human parvovirus by molecular screening of respiratory tract samples",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Allander",
                    "suffix": ""
                },
                {
                    "first": "MT",
                    "middle": [],
                    "last": "Tammi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Eriksson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Bjerkner",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Tiveljung-Lindell",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Andersson",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Proc Natl Acad Sci USA",
            "volume": "102",
            "issn": "",
            "pages": "12891-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Frequent detection of human rhinoviruses, paramyxoviruses, coronaviruses, and bocavirus during acute respiratory tract infections",
            "authors": [
                {
                    "first": "KE",
                    "middle": [],
                    "last": "Arden",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "McErlean",
                    "suffix": ""
                },
                {
                    "first": "MD",
                    "middle": [],
                    "last": "Nissen",
                    "suffix": ""
                },
                {
                    "first": "TP",
                    "middle": [],
                    "last": "Sloots",
                    "suffix": ""
                },
                {
                    "first": "IM",
                    "middle": [],
                    "last": "Mackay",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Med Virol",
            "volume": "78",
            "issn": "",
            "pages": "1232-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Evidence of human coronavirus HKU1 and human bocavirus in Australian children",
            "authors": [
                {
                    "first": "TP",
                    "middle": [],
                    "last": "Sloots",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "McErlean",
                    "suffix": ""
                },
                {
                    "first": "DJ",
                    "middle": [],
                    "last": "Speicher",
                    "suffix": ""
                },
                {
                    "first": "KE",
                    "middle": [],
                    "last": "Arden",
                    "suffix": ""
                },
                {
                    "first": "MD",
                    "middle": [],
                    "last": "Nissen",
                    "suffix": ""
                },
                {
                    "first": "IM",
                    "middle": [],
                    "last": "Mackay",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Clin Virol",
            "volume": "35",
            "issn": "",
            "pages": "99-102",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Bocavirus infection in hospitalized children, South Korea",
            "authors": [
                {
                    "first": "JY",
                    "middle": [],
                    "last": "Chung",
                    "suffix": ""
                },
                {
                    "first": "TH",
                    "middle": [],
                    "last": "Han",
                    "suffix": ""
                },
                {
                    "first": "CK",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "SW",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "1254-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Human bocavirus: prevalence and clinical spectrum at a children's hospital",
            "authors": [
                {
                    "first": "JC",
                    "middle": [],
                    "last": "Arnold",
                    "suffix": ""
                },
                {
                    "first": "KK",
                    "middle": [],
                    "last": "Singh",
                    "suffix": ""
                },
                {
                    "first": "AA",
                    "middle": [],
                    "last": "Spector",
                    "suffix": ""
                },
                {
                    "first": "MH",
                    "middle": [],
                    "last": "Sawyer",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Clin Infect Dis",
            "volume": "43",
            "issn": "",
            "pages": "283-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Human bocavirus infection, People's Republic of China",
            "authors": [
                {
                    "first": "XW",
                    "middle": [],
                    "last": "Qu",
                    "suffix": ""
                },
                {
                    "first": "ZJ",
                    "middle": [],
                    "last": "Duan",
                    "suffix": ""
                },
                {
                    "first": "ZY",
                    "middle": [],
                    "last": "Qi",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Emerg Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "165-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Human bocavirus infection in young children in the United States: molecular epidemiological profile and clinical characteristics of a newly emerging respiratory virus",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Kesebir",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Vazquez",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Weibel",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Infect Dis",
            "volume": "194",
            "issn": "",
            "pages": "1276-82",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Human bocavirus, a respiratory and enteric virus",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Vincente",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Cilla",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Montes",
                    "suffix": ""
                },
                {
                    "first": "EG",
                    "middle": [],
                    "last": "Perez-Yarza",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Perez-Trallero",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Emerg Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "636-7",
            "other_ids": {
                "DOI": []
            }
        }
    }
}