{
    "paper_id": "PMC7122433",
    "metadata": {
        "title": "Recombinant Turkey Coronavirus Nucleocapsid Protein Expressed in Escherichia coli",
        "authors": [
            {
                "first": "Leyi",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": "Leyi.Wang@Agri.ohio.gov",
                "affiliation": {}
            },
            {
                "first": "Chien",
                "middle": [
                    "Chang"
                ],
                "last": "Loa",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ching",
                "middle": [
                    "Ching"
                ],
                "last": "Wu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tsang",
                "middle": [
                    "Long"
                ],
                "last": "Lin",
                "suffix": "",
                "email": "tllin@purdue.edu",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "\nTurkey coronavirus (TCoV) is the cause of an acute and highly contagious enteric disease affecting turkeys of all ages. The disease is severe in 1- to 4-week-old turkey poults [1]. Turkey flocks that recover from natural or experimental coronaviral enteritis may develop lifelong immunity [2]. TCoV has been recognized as an important pathogen of young turkeys. TCoV infection causes significant economic losses in the turkey industry due to poor feed conversion and uneven growth. Outbreaks of TCoV enteritis in turkey poults remain as a threat to the turkey industry.",
            "cite_spans": [
                {
                    "start": 178,
                    "end": 179,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 291,
                    "end": 292,
                    "mention": "2",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "In order to rapidly diagnose and effectively control turkey coronaviral enteritis, development of an antibody-capture enzyme-linked immunosorbent assay ( ELISA) for detecting antibodies to TCoV is essential. Development of ELISA for detection of TCoV infection requires large amounts of TCoV antigen. Molecular cloning and expression of TCoV N protein were carried out for preparation of large quantities of highly purified viral proteins.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Coronavirus is an enveloped and positive-stranded RNA virus that possesses three major structural proteins including a predominant phosphorylated nucleocapsid (N) protein, peplomeric glycoprotein, and spike (S) protein that makes up the large surface projections of the virion, and membrane protein (M) [3, 4]. The N protein is abundantly produced in coronavirus-infected cells and is highly immunogenic. The N protein binds to the viral genomic RNA and composes the structural feature of helical nucleocapsid.",
            "cite_spans": [
                {
                    "start": 304,
                    "end": 305,
                    "mention": "3",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 307,
                    "end": 308,
                    "mention": "4",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The N protein is a preferred choice for developing a group-specific serologic assay because of highly conserved sequence and antigenicity. The nucleocapsid proteins of various RNA viruses, such as mumps, rabies, vesicular stomatitis, measles, Newcastle disease, and infectious bronchitis (IBV) viruses, have been used as the coating antigens in diagnostic ELISA [5\u201310]. Prokaryotic expression is an economic and convenient system to prepare large amount of pure recombinant protein. In addition, the antigenic integrity of N protein expressed in prokaryotic system is expected to be maintained due to the lack of glycosylation. This chapter describes expression and purification of TCoV N protein with a prokaryotic system for preparation of a large quantity of highly purified viral protein, which can be used as coating antigen for antibody-capture ELISA for serologic diagnosis of TCoV infection [11, 12].",
            "cite_spans": [
                {
                    "start": 363,
                    "end": 364,
                    "mention": "5",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 365,
                    "end": 367,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 900,
                    "end": 902,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 904,
                    "end": 906,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "\nVirus source (see\nNote\n1).RNApure reagent (GenHunter, Nashville, TN, USA).Chloroform.Isopropanol.70 % Ethanol.Diethyl-pyrocarbonate (DEPC)-treated sterile double-distilled water (DEPC-H2O).SuperScript III first-strand synthesis system for RT-PCR kit (Life Technologies/Invitrogen, Carlsbad, CA, USA).RNaseOUT, a recombinant RNase inhibitor (Life Technologies/Invitrogen).RNase H enzyme (Life Technologies/Invitrogen).Random hexamers (Life Technologies/Invitrogen).N gene primers (see\nNote\n2): NF, 5\u2032-TCTTTTGCCATGGCAAGC-3\u2032; NR, 5\u2032-TTGGGTACCTAAAAGTTCATTCTC-3\u2032.Taq polymerase (Promega Corp, Madison, WI, USA).Pfu polymerase (Stratagene, La Jolla, CA, USA).Deoxynucleotide triphosphates (dNTP) (Promega).Sterile distilled water.pTriEx 1.1 cloning kit (Novagen, Madison, WI, USA; EMD Millipore Corp., Billerica, MA, USA).Vector primer for clone screening: TriExUP primer (Novagen; EMD Millipore Corp.).QIAquick PCR product purification kit (Qiagen, Valencia, CA, USA).Restriction enzymes: Nco I and Kpn I (New England Biolabs, Inc., Ipswich, MA, USA) (see\nNote\n3).Shrimp alkaline phosphatase (SAP) (Promega Corp).Zymoclean gel DNA recovery kit (Zymo Research, Irvine, CA, USA).\nEscherichia coli (E. coli) NovaBlue competent cell (Novagen; EMD Millipore Corp.).Growth medium: LB medium containing 50 \u03bcg/ml carbenicillin (Life Technologies, Grand Island, NY, USA).QIAquick mini-prep kit (Qiagen).\nE. coli Tuner (DE3) pLacI competent cell (Novagen; EMD Millipore Corp.).\n",
            "cite_spans": [],
            "section": "Construction of TCoV N Gene in the Expression Vector pTriEx ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nExpression host E. coli cell, strain Tuner (DE3) pLacI transformant with recombinant plasmid pTri-N containing N protein gene (prepared from Sects. 2.1 and 3.1).Expression medium: LB medium containing 50 \u03bcg/ml carbenicillin, 34 \u03bcg/ml chloramphenicol, and 1 % glucose (Life Technologies) (see\nNote\n4).Isopropyl \u03b2-d-1-thiogalactopyranoside (IPTG), 100 mM (Life Technologies).Incubator shaker (New Brunswick Scientific Co., Inc., Edison, NJ, USA; Eppendorf North America, Hauppauge, NY, USA).\n",
            "cite_spans": [],
            "section": "Expression of Recombinant TCoV Protein ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nBugBuster (Novagen; EMD Millipore Corp.).Benzonase (Novagen; EMD Millipore Corp.).His-Bind column, pre-packed column with 1.25 ml Ni2+ charged His-Bind resin (Novagen; EMD Millipore Corp.) (see\nNote\n5).Binding buffer, 5 mM imidazole, 0.5 M NaCl, 20 mM Tris\u2013HCl (pH 7.9) (Novagen; EMD Millipore Corp.) containing 6 M urea (see\nNote\n6).Wash buffer, 20 mM imidazole, 0.5 M NaCl, 20 mM Tris\u2013HCl (pH 7.9) (Novagen; EMD Millipore Corp.) containing 6 M urea.Elute buffer, 1 M imidazole, 0.5 M NaCl, 20 mM Tris\u2013HCl (pH 7.9) (Novagen; EMD Millipore Corp.) containing 6 M urea.Protein assay reagent (Bio-Rad, Hercules, CA, USA).SDS-polyacrylamide gel electrophoresis and Western blotting apparatuses (Mini-Protean\u00ae electrophoresis chamber and wet/tank blotting system, Bio-Rad).\n",
            "cite_spans": [],
            "section": " Purification of Recombinant TCoV N Protein ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nTotal RNA was extracted from TCoV virus source by RNApure reagent. Two hundred microliters of virus solution are mixed with 1 ml of RNApure reagent and incubated on ice for 10 min (see\nNote\n7).Add 180 \u03bcl of chloroform, mix the mixture, and vortex vigorously for 10 s (see\nNote\n8).Centrifuge at 13,000 \u00d7 g for 10 min at 4 \u00b0C. Carefully take the upper aqueous phase into a clean microcentrifuge tube and mix with equal volume of cold isopropanol by vortexing vigorously for 30 s. Incubate on ice for 10 min.Centrifuge at 13,000 \u00d7 g for 10 min at 4 \u00b0C. Carefully discard the supernatant without disturbing the RNA pellet.Wash RNA pellet with 1 ml of cold 70 % ethanol. Incubate on ice for 5 min.Centrifuge at 13,000 \u00d7 g for 2 min at 4 \u00b0C. Remove ethanol. Spin briefly and remove the residual liquid with pipette (see\nNote\n9).Dissolve RNA pellet in 50 \u03bcl of DEPC-H2O and a portion of it is quantified by spectrophotometry (GeneQuant Pro Spectrophotometer, Amersham Pharmacia Biotech, Inc., Piscataway, NJ, USA; GeneQuant 1300 Spectrophotometer, GE Healthcare Bio-Sciences, Piscataway, NJ, USA) at 260 nm wavelength (see\nNote\n10).Mix 8 \u03bcl (1 pg to 5 \u03bcg) of RNA with 1 \u03bcl (50 ng/\u03bcl) random hexamer and 1 \u03bcl (10 mM) dNTP in a total volume of 10 \u03bcl.Incubate at 65 \u00b0C for 5 min and sit on ice for 1 min.Add 10 \u03bcl of SuperScript III cDNA Synthesis Mix (containing 2 \u03bcl 10\u00d7 RT buffer, 4 \u03bcl (25 mM) MgCl2, 2 \u03bcl (0.1 M) DTT, 1 \u03bcl (40 U/\u03bcl) RNaseOUT, 1 \u03bcl (200 U/\u03bcl) SuperScript III RT enzyme) to each RNA/primer mixture.Incubate at 25 \u00b0C for 10 min followed by 50 \u00b0C for 50 min.Terminate the reverse transcription (RT) reaction at 85 \u00b0C for 5 min and chill on ice (see\nNote\n11).Add 2 \u03bcl of the above RT mixture to the PCR amplification reaction (100 \u03bcl) with primers NF and NR. A mix of Taq and Pfu at 10:1 is recommended to maintain PCR fidelity (Table 1).\n\nPCR cyclic parameters: 94 \u00b0C for 10 s for denaturation, 58 \u00b0C for 30 s for annealing, 72 \u00b0C for 2 min for extension for 35 cycles followed by 72 \u00b0C for 10-min final extension.\nPCR product is purified by QIAquick PCR purification kit and recovered in 16 \u03bcl of sterile distilled water (pH 7.0\u20138.5).Add all 16 \u03bcl of purified PCR product to the restriction enzyme (RE) digestion with 1 \u03bcl (10 units/\u03bcl) Nco I, 1 \u03bcl (10 units/\u03bcl) Kpn I, and 2 \u03bcl 10\u00d7 buffer in one reaction of 20 \u03bcl. Incubate at 37 \u00b0C for 4 h.Set up the same RE digestion for 1 \u03bcg of vector pTriEx 1.1 vector plasmid.For dephosphorylation of digested vector, add 1 \u03bcl (1 unit) of SAP to the 20 \u03bcl of digestion reaction mixture. Incubate at 37 \u00b0C for 30 min.Gel purify both digestion reactions for PCR product and plasmid pTriEx by Zymoclean gel DNA recovery kit. Recover each digested DNA prep in 10 \u03bcl of sterile distilled water (see\nNote\n12).Set up two ligation reactions (pTriEx 1.1 cloning kit) with 1 \u03bcl of pTriEx vector and 1 or 4 \u03bcl of PCR product (insert) DNA in a reaction of 10 \u03bcl as demonstrated in Table 2. Incubate at 16 \u00b0C for 15 min (see\nNote\n13).\nTake 1 \u03bcl of ligation reaction for transformation to NovaBlue competent cell. Plate 20 and 100 \u03bcl of the transformation mix on growth medium plates (see\nNote\n14).Colony screening by PCR with primers TriExUp and NR. The PCR cycling parameters are the same as above for N gene amplification (step 14). The PCR product of correct clone is about 1500 bp.The selected clone is grown in growth medium. Plasmids are purified by QIAquick mini-prep kit and sequenced to confirm that the inserted TCoV N gene is in frame with the vector-defined open reading frame at both its N- and C-termini.The recombinant plasmid containing the entire TCoV N protein gene (pTri-N) is transformed to competent E. coli strain Tuner (DE3) pLacI. Transformants are grown in expression medium.\n",
            "cite_spans": [],
            "section": "Construction of TCoV N Gene in the Expression Vector pTriEx ::: Methods",
            "ref_spans": [
                {
                    "start": 1842,
                    "end": 1843,
                    "mention": "1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 2924,
                    "end": 2925,
                    "mention": "2",
                    "ref_id": "TABREF1"
                }
            ]
        },
        {
            "text": "\nA starter culture of expression host bacteria cells is prepared in 3 ml of expression medium. Cells are incubated at 37 \u00b0C with shaking at 250 rpm in an incubator shaker overnight, to an OD of 600 nm approximately 0.5 (see\nNote\n15).The entire 3 ml culture is added to 100 ml of fresh expression medium and incubated at 37 \u00b0C with shaking at 250 rpm in an incubator shaker until the OD reached 0.5\u20131.0 at 600 nm wavelength (see\nNote\n16).The cultures are induced by addition of IPTG to a final concentration of 1 mM (add 1 ml of sterile 100 mM IPTG solution). The induced cultures are incubated at 37 \u00b0C with shaking at 250 rpm in an incubator shaker for another 4 h.The bacterial cultures are harvested by centrifugation at 10,000 \u00d7 g for 10 min at 4 \u00b0C. The cell pellet can be immediately processed or frozen stored at \u221220 or \u221280 \u00b0C until processed (see\nNote\n17).\n",
            "cite_spans": [],
            "section": "Expression of Recombinant TCoV N Protein ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nResuspend the cell pellet completely in 5 ml of BugBuster reagent per gram of wet cell weight (see\nNote\n18).The nuclease reagent Benzonase is added with 1 \u03bcl (25 units) for every ml of BugBuster reagent used.Incubate at room temperature with slow rotation for 20 min.Centrifuge at 16,000 \u00d7 g for 20 min at 4 \u00b0C. The soluble supernatant is discarded and the insoluble fraction (containing inclusion bodies) is dissolved in 10 ml of binding buffer (see\nNote\n19).Equilibrate His-Bind column with 10 ml of binding buffer. Allow the entire buffer volume to flow through.Apply the protein solution (dissolved inclusion bodies) to the column.Wash the column with 10 ml of binding buffer.Wash the column with 10 ml of wash buffer.Elute the recombinant N protein from the column with 5 ml of elute buffer (see\nNote\n20).The concentration of purified N protein is determined by protein assay reagent. The yield of purified N protein is about 10 mg from a 100 ml of culture (see\nNote\n21).The purified N protein is further confirmed by SDS-polyacrylamide gel electrophoresis and Western blotting as a single protein band with a molecular mass of about 57 kDa.\n",
            "cite_spans": [],
            "section": " Purification of Recombinant TCoV N Protein ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nHomogenates of infected intestines can be the virus source. For better results, further purification of intestinal homogenates containing TCoV with 40\u201360 % sucrose gradient is recommended. TCoV/IN/94 (GenBank accession number EU022525) has been used as the stock virus for expression of recombinant TCoV N protein in our laboratory.Primers NF and NR containing restriction sites Nco I and Kpn I, respectively. The amplified product containing the entire open reading frame of TCoV N protein gene (1230 bp).Nco I and Kpn I are typical restriction enzymes. The digestions by both enzymes can be conducted in a single reaction with compatible buffer. For example, the two enzymes from New England Biolabs (Ipswich, MA) can be performed in NE Buffer 1.1.Ampicillin antibiotic marker is on the expression vector pTriEx and chloramphenicol antibiotic marker is on plasmid pLacI in the expression host strain Tuner cells. Carbenicillin is recommended to be in place of ampicillin for better stability for pH changes throughout the bacterial cultures.The binding capacity of 1.25 ml of His-Bind resin is 10 mg of target protein per column. As for any affinity chromatography, the best purity of target protein is achieved when the amount of protein extract is near the binding capacity.The purpose of 6 M urea is to improve resolution of the sticky inclusion bodies. The presence of 6 M urea does not affect binding of His-Bind resin to target N protein.The suggested ratio of RNApure reagent to sample is 10:1. Excess amount of RNApure reagent has no negative impact. The lower ratio (5:1) in this step is intended to obtain higher concentration of viral RNA in the final supernatants. If the upper aqueous phase after centrifugation at step 3 is more than half of the total volume, there is not enough RNApure reagent added. The appropriate reagent amount may be adjusted. Chloroform is applied at 150 \u03bcl for every milliliter of lysate.The sample mixture with chloroform at this step can be stored at \u221270 \u00b0C or even lower temperature before proceeding to the next step.Optional: Inverting the tube for 5\u201310 min for air-drying of RNA pellet is a helpful tip to completely remove any residual ethanol that may interfere the following RT reaction.It is critical to make sure that the jellylike RNA pellet is completely dissolved into solution by repeat pipetting. The volume (50 \u03bcl) of DEPC-H2O may be adjusted according to RNA pellet size to achieve appropriate concentrations. Concentration can be estimated by taking 1 \u03bcl of the RNA solution into 1 ml of water. Read at 260 nm. 1 OD260 = 40 \u03bcg. The RNA quality can be further examined by OD 260/280 and 260/230 ratio. The ratio of 260/280 about 2.0 is considered as pure for RNA, while 1.8 is considered pure for DNA. The expected 260/230 ratio is around 2.0\u20132.2 for pure nucleic acid. If the ratio is appreciably lower, it may indicate the presence of protein, phenol, or other contaminants with strong absorption near 280 or 230 nm. RNA should be stored at \u221270 \u00b0C or even lower temperature.The synthesized cDNA in the RT reaction can be stored at \u221220 \u00b0C or even lower temperature until used.\nPCR product may be purified. The vector must be gel purified due to the long digested fragment size above 30 bp.The molar ratio between vector and insert is suggested at 1:2 to 1:5. The volumes in this step are illustrated for initial exploration. The concentration of digested vector and insert can be estimated by OD 260 or agarose gel electrophoresis with known amount of DNA of similar size in adjacent wells. The ligation reaction mixture can be stored at 4 \u00b0C until used for transformation or at \u221220 \u00b0C for longer term.After plating, the leftover transformation mix can be stored at 4 \u00b0C for further plating in the following days at different amount if needed.The starter culture can be prepared from a fresh colony on a plate or directly from a glycerol storage stock. An OD around 0.5 represents a culture at log phase when the cells are at the best condition to expand and for protein expression.This usually takes about 2\u20133 h to reach the OD range. The higher the OD of starter culture in the previous step, the shorter the time to reach this OD range.The centrifuge tubes should be weighed before and after collection of cell pellet for estimation of wet pellet amount and the volume of BugBuster to be applied in the next step. Frozen storage of cell pellets may improve the extraction efficiency of BugBuster reagents through the freeze/thaw cycle.It is important to completely resuspend the cell pellets for the best results of BugBuster extraction. Higher volume of BugBuster reagent does not have adverse effect. Roughly 10\u201320 ml of BugBuster reagent should be enough for cell pellets collected from a 100 ml of culture. BugBuster reagent can be added directly to frozen cell pellets. There is no need to wait for the temperature to return to room temperature. Protease inhibitors may be added at this step but usually not necessary.It is critical but somewhat difficult to completely dissolve the sticky inclusion bodies. Repeat pipetting up and down until the protein solution is homogeneous. Any undissolved particles will clot the His-Bind column and affect the purification process. It is advisable to centrifuge the dissolved inclusion body protein solution at 5000\u201310,000 \u00d7 g for 10\u201315 min at 4 \u00b0C for clarification before application to the column.Eluate may be collected in fractions such as 0.5 or 1 ml each fraction.The presence of 6 M urea is compatible with the protein assay reagent. The assay range can be adjusted for protein concentrations from low \u03bcg/ml to 1 mg/ml with different assay format. The protein concentration of the target N protein eluate as obtained following this process is about 1\u20132 mg/ml. The presence of 6 M urea has no adverse effect on plate coating for ELISA performance. Given the coating concentration of N protein at 20 \u03bcg/ml, the eluate is usually diluted in coating buffer for at least 1:10 to reduce the urea content to less than 600 mM and, subsequently, further diminish any possible effect on ELISA performance. Accordingly, the purified N protein eluate can be directly applied to the ELISA assay for detection of antibodies to TCoV.\n",
            "cite_spans": [],
            "section": "Notes",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1: Preparation of reaction mixture for turkey coronavirus nucleocapsid protein gene polymerase chain reaction\n",
            "type": "table"
        },
        "TABREF1": {
            "text": "Table 2: Preparation of ligation reaction mixture for recombinant plasmid carrying turkey coronavirus nucleocapsid protein gene\n(x + y + z = 5 \u03bcl)",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Coronaviral enteritis of turkeys (blue comb disease)",
            "authors": [
                {
                    "first": "KV",
                    "middle": [],
                    "last": "Nagaraja",
                    "suffix": ""
                },
                {
                    "first": "BS",
                    "middle": [],
                    "last": "Pomeroy",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Diseases of poultry",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Recombinant nucleocapsid protein is potentially an inexpensive, effective serodiagnostic reagent for infectious bronchitis virus",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ndifuna",
                    "suffix": ""
                },
                {
                    "first": "AK",
                    "middle": [],
                    "last": "Waters",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "EW",
                    "middle": [],
                    "last": "Collisson",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "J Virol Methods",
            "volume": "70",
            "issn": "",
            "pages": "37-44",
            "other_ids": {
                "DOI": [
                    "10.1016/S0166-0934(97)00170-5"
                ]
            }
        },
        "BIBREF2": {
            "title": "Expression and purification of turkey coronavirus nucleocapsid protein in Escheria coli",
            "authors": [
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Loa",
                    "suffix": ""
                },
                {
                    "first": "TL",
                    "middle": [],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "TA",
                    "middle": [],
                    "last": "Bryan",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Hooper",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Schrader",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Virol Methods",
            "volume": "116",
            "issn": "",
            "pages": "161-167",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jviromet.2003.11.006"
                ]
            }
        },
        "BIBREF3": {
            "title": "Recombinant nucleocapsid protein-based enzyme-linked immunosorbent assay for detection of antibody to turkey coronavirus",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Abdelwahab",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Loa",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "TL",
                    "middle": [],
                    "last": "Lin",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Virol Methods",
            "volume": "217",
            "issn": "",
            "pages": "36-41",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jviromet.2015.02.024"
                ]
            }
        },
        "BIBREF4": {
            "title": "Immunity to transmissible coronaviral enteritis of turkeys (Blue comb)",
            "authors": [
                {
                    "first": "BS",
                    "middle": [],
                    "last": "Pomeroy",
                    "suffix": ""
                },
                {
                    "first": "TC",
                    "middle": [],
                    "last": "Larsen",
                    "suffix": ""
                },
                {
                    "first": "RD",
                    "middle": [],
                    "last": "Deshmukh",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Patel",
                    "suffix": ""
                }
            ],
            "year": 1975,
            "venue": "Am J Vet Res",
            "volume": "36",
            "issn": "",
            "pages": "553-555",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Identification of the structural proteins of turkey enteric coronavirus",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Dea",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Tijssen",
                    "suffix": ""
                }
            ],
            "year": 1988,
            "venue": "Arch Virol",
            "volume": "99",
            "issn": "",
            "pages": "173-186",
            "other_ids": {
                "DOI": [
                    "10.1007/BF01311068"
                ]
            }
        },
        "BIBREF6": {
            "title": "Coronavirus immunogens",
            "authors": [
                {
                    "first": "LJ",
                    "middle": [],
                    "last": "Saif",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Vet Microbiol",
            "volume": "37",
            "issn": "",
            "pages": "285-297",
            "other_ids": {
                "DOI": [
                    "10.1016/0378-1135(93)90030-B"
                ]
            }
        },
        "BIBREF7": {
            "title": "Immunoglobulin class and immunoglobulin G subclass enzyme-linked immunosorbent assays compared with microneutralisation assay for sero-diagnosis of mumps infection and determination of immunity",
            "authors": [
                {
                    "first": "GA",
                    "middle": [],
                    "last": "Linde",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Granstrom",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Orvell",
                    "suffix": ""
                }
            ],
            "year": 1987,
            "venue": "J Clin Microbiol",
            "volume": "25",
            "issn": "",
            "pages": "1653-1658",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Rabies diagnostic reagents prepared from a rabies N gene recombinant expressed in baculovirus",
            "authors": [
                {
                    "first": "FL",
                    "middle": [],
                    "last": "Reid-Sanden",
                    "suffix": ""
                },
                {
                    "first": "JW",
                    "middle": [],
                    "last": "Sumner",
                    "suffix": ""
                },
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Fekadu",
                    "suffix": ""
                },
                {
                    "first": "JH",
                    "middle": [],
                    "last": "Shaddock",
                    "suffix": ""
                },
                {
                    "first": "WJ",
                    "middle": [],
                    "last": "Bellini",
                    "suffix": ""
                }
            ],
            "year": 1990,
            "venue": "J Clin Microbiol",
            "volume": "28",
            "issn": "",
            "pages": "858-863",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Baculovirus expression of the nucleocapsid gene of measles virus and utility of the recombinant protein in diagnostic enzyme immunoassays",
            "authors": [
                {
                    "first": "KB",
                    "middle": [],
                    "last": "Hummel",
                    "suffix": ""
                },
                {
                    "first": "DD",
                    "middle": [],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Heath",
                    "suffix": ""
                },
                {
                    "first": "WJ",
                    "middle": [],
                    "last": "Bellini",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "J Clin Microbiol",
            "volume": "30",
            "issn": "",
            "pages": "2874-2880",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Immunological characterization of the VSV nucleocapsid (N) protein expressed by recombinant baculovirus in Spodoptera exigua larva: use in differential diagnosis between vaccinated and infected animals",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Ahmad",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bassiri",
                    "suffix": ""
                },
                {
                    "first": "AK",
                    "middle": [],
                    "last": "Banerjee",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Yilma",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Virology",
            "volume": "192",
            "issn": "",
            "pages": "207-216",
            "other_ids": {
                "DOI": [
                    "10.1006/viro.1993.1023"
                ]
            }
        },
        "BIBREF11": {
            "title": "A diagnostic immunoassay for Newcastle disease virus based on the nucleocapsid protein expressed by a recombinant baculovirus",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Errington",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Steward",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Emmerson",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "J Virol Methods",
            "volume": "55",
            "issn": "",
            "pages": "357-365",
            "other_ids": {
                "DOI": [
                    "10.1016/0166-0934(95)00074-7"
                ]
            }
        }
    }
}