{
    "paper_id": "PMC7108127",
    "metadata": {
        "title": "Rapid Diagnosis of a Coronavirus Associated with Severe Acute Respiratory Syndrome (SARS)",
        "authors": [
            {
                "first": "Leo",
                "middle": [
                    "L",
                    "M"
                ],
                "last": "Poon",
                "suffix": "",
                "email": "llmpoon@hkucc.hku.hk",
                "affiliation": {}
            },
            {
                "first": "On",
                "middle": [
                    "Kei"
                ],
                "last": "Wong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Winsie",
                "middle": [],
                "last": "Luk",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok",
                "middle": [
                    "Yung"
                ],
                "last": "Yuen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Joseph",
                "middle": [
                    "S",
                    "M"
                ],
                "last": "Peiris",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yi",
                "middle": [],
                "last": "Guan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Severe acute respiratory syndrome (SARS) is a recently emerged disease associated with pneumonia in infected patients (1). The disease is unusual in its severity, and patients suffering from this disease do not respond to empirical antimicrobial treatment for acute community-acquired typical or atypical pneumonia (2). By the end of March 2003, a cumulative total of 1622 cases and 58 deaths had been reported from 13 countries (3). The disease is highly infectious, and attach rates >56% have been reported in healthcare workers caring for SARS patients (2).",
            "cite_spans": [
                {
                    "start": 119,
                    "end": 120,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 316,
                    "end": 317,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 430,
                    "end": 431,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 557,
                    "end": 558,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Recently, we identified a novel virus in the family Coronaviridae in SARS patients (4). Of patients from whom paired acute and convalescent sera were available, all had seroconverted or had a greater than fourfold increase in antibody titer to this novel virus (4), suggesting that it plays an important role in the etiology of SARS. Thus, the establishment of a rapid noninvasive test for this virus is a high priority for monitoring and control of this disease. Here, we report a real-time quantitative PCR assay to detect this virus in clinical specimens.",
            "cite_spans": [
                {
                    "start": 84,
                    "end": 85,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 262,
                    "end": 263,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Patients with a clinical diagnosis of SARS and admitted in two hospitals in Hong Kong between February 26, 2003, and March 26, 2003, were considered for this study. Inclusion criteria were a fever of 38 \u00b0C or higher, cough or shortness of breath, new pulmonary infiltrate(s) on chest radiographs, and either a history of exposure to a patient with SARS or absence of response to empirical antimicrobial coverage for typical and atypical pneumonia. Samples were collected with informed consent. In total, 29 SARS patients with paired sera and nasopharyngeal aspirate (NPA) samples were available for the study. The diagnosis of SARS was confirmed in all patients by the presence of antibodies against the novel coronavirus in the serum (4). The age of these patients ranged from 24 to 71 years [mean (SD), 45.2 (13.9) years], and the male-to-female ratio was 11:18. At the time of submitting this manuscript, one of the patients had died.",
            "cite_spans": [
                {
                    "start": 736,
                    "end": 737,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "NPA samples were collected by suction into a disposable aspiration trap containing 3 mL of viral transport medium (containing, per liter, 2 g of sodium bicarbonate, 5 g of bovine serum albumin, 200 \u03bcg of vancomycin, 18 \u03bcg of amikacin, and 160 U of nystatin in Earle\u2019s balanced salt solution). Total RNA from 140 \u03bcL of each NPA sample was extracted with use of a QIAamp Virus RNA Mini Kit (Qiagen), as instructed by the manufacturer, and eluted in 50 \u03bcL of buffer. For isolating RNA from stool specimens, 140 \u03bcL of viral transport medium containing suspended stool was subjected to the RNA extraction method described above.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Ten microliters of eluted RNA was reverse-transcribed by use of 200 U of Superscript II reverse transcriptase (Invitrogen) in a 20-\u03bcL reaction containing 0.15 \u03bcg of random hexamers, 10 mM dithiothreitol, and 0.5 mM deoxynucleotide triphosphates. FastStart DNA Master SYBR Green I fluorescence reaction (Roche) was used in the PCR assay. Briefly, 2 \u03bcL of cDNA was amplified in 20 \u03bcL containing, per liter, 3.5 mmol of MgCl2, 0.25 \u03bcmol of forward primer (coro3: 5\u2032-TACACACCTCAGCGTTG-3\u2032), and 0.25 \u03bcmol of reverse primer (coro4: 5\u2032-CACGAACGTGACGAAT-3\u2032). Reactions were performed in a LightCycler (Roche) with the following conditions: 10 min at 95 \u00b0C, followed by 50 cycles of 95 \u00b0C for 10 s, 57 \u00b0C for 5 s, and 72 \u00b0C for 9 s. Plasmids containing the target sequence were used as positive controls. Fluorescence signals from these reactions were captured at the end of the extension step in each cycle. To determine the specificity of the assay, PCR products were subjected to melting curve analysis at the end of the assay (65 to 95 \u00b0C; 0.1 \u00b0C/s).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "On the basis of our identified sequences of this virus, a pair of primers expected to amplify a 182-bp region within the RNA-dependent RNA polymerase-encoding sequence of the virus (accession no. AY268070) was used in the assay. The identity of the product was confirmed by sequencing.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "To determine the dynamic range of real-time quantitative PCR, serial dilutions of plasmid DNA containing the target sequence were made and subjected to the real-time quantitative PCR assay. The assay was able to distinguish 10-fold differences in concentration over a range from 10 to 107, and no signal was observed in the water control (Fig. 1a ).",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 339,
                    "end": 346,
                    "mention": "Fig. 1a",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "A positive signal was observed in 23 of 29 serologically confirmed SARS patients. In all of these positive cases, a unique PCR product (Tm = 82 \u00b0C) corresponding to the signal from the positive control was observed (Fig. 1b , and data not shown). These results indicated that this assay is highly specific for the target. The copy numbers of the target sequence in these reactions were as high as 4.5 \u00d7 103. Thus, as many as 2.5 \u00d7 105 copies of this viral sequence were present in 140 \u03bcL of NPA sample in transport medium. Among these 23 positive cases, 6 had copy numbers less than 10, i.e., below that of the low calibrator. The identities of positive signals from these six cases were confirmed by the melting curve analysis and were further confirmed by gel electrophoresis in three cases.",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 216,
                    "end": 223,
                    "mention": "Fig. 1b",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "In five of the above positive cases, NPA samples were collected before seroconversion. Viral RNA was detected in three of these samples, indicating that this assay can detect the virus even at an early stage of infection.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "To further validate the specificity of this assay, NPA samples from healthy individuals (n = 11) and patients suffering from adenovirus (n = 11), respiratory syncytial virus (n = 11), human metapneumovirus (n = 11), influenza A virus (n = 13), or influenza B virus (n = 1) infection were recruited as negative controls. All but one of these samples were negative in the assay. The false-positive case was negative in a subsequent test. Taken together, including the initial false-positive case, the real-time quantitative PCR assay was positive in 79% of SARS cases and negative in 98% of controls with and without viral infections.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Epidemiologic data suggest that droplet transmission is one of the major routes of transmission of this virus. The detection of live virus (4) and the detection of high copy numbers of viral sequence from NPA samples in the current study clearly support that the concept that cough and sneeze droplets from SARS patients are a major route of spread of this infectious agent. At the time of the writing of this manuscript, disposable surgical masks, gloves, and gown are worn by healthcare workers who care for the SARS patients. Interestingly, two of four available stool samples from the SARS patients in this study were positive in the assay (data not shown). It is thus relevant to note that many animal coronaviruses are spread via the fecal-oral route (5). Further studies are required to test whether the virus in feces is infectious.",
            "cite_spans": [
                {
                    "start": 140,
                    "end": 141,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 758,
                    "end": 759,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In addition to this novel human coronavirus, there are two known serogroups of human coronaviruses, 229E and OC43 (6). The primer set used in our assay does not have sequence homology to 229E. As a corresponding OC43 sequence is not available in GenBank, it is not known whether these primers would cross-react with this strain. However, sequence analyses of available sequences in other regions of the OC43 polymerase gene indicate that the human virus associated with SARS is genetically distinct from OC43. Furthermore, the primers used in this study do not have homology to any sequences from known coronaviruses. Thus, it is very unlikely that these primers would hybridize with sequences from strain OC43 under the conditions of this assay.",
            "cite_spans": [
                {
                    "start": 115,
                    "end": 116,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In addition to the novel coronavirus, metapneumovirus was reported in some SARS patients (7). RNA samples from this study were subjected to nested reverse transcription-PCR (4), and no evidence of metapneumovirus infection was detected in any of the patients in this study (data not shown), suggesting that the novel coronavirus is the key player in the pathogenesis of SARS. Further work is required, however, to determine whether other pathogens contribute to the progression of the disease.",
            "cite_spans": [
                {
                    "start": 90,
                    "end": 91,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 174,
                    "end": 175,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In this work, we used SYBR Green detection, which allows melting curve analysis (8) and, thus, recognition of samples with sequence variations (mutations) in the virus. This may be important for viruses as the low fidelity of RNA-dependent RNA polymerase produces high mutation rates in some RNA viruses. The PCR products from all 23 positive cases in this study had the same melting point, strongly suggesting that there was no viral sequence variation in the target region of samples collected at the two Hong Kong hospitals during the 1-month period of patient accrual.",
            "cite_spans": [
                {
                    "start": 81,
                    "end": 82,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "While this manuscript was being revised, a quantitative TaqMan-format assay for SARS-associated coronavirus was published (9) and shown to be positive in two confirmed cases of SARS. The TaqMan method and ours are complementary as they target different regions of the virus. Moreover, our demonstration of negative results in patients with a variety of other respiratory virus infections complements the negative findings by the TaqMan assay in stools from patients with gastrointestinal symptoms, further supporting the concept that this coronavirus is specific for SARS. Our assay also adds information about sequence variation of the virus (which cannot by assessed by TaqMan assays) and about the range of viral copy numbers seen in a relatively large number of patients.",
            "cite_spans": [
                {
                    "start": 123,
                    "end": 124,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In conclusion, we describe a simple and rapid noninvasive diagnostic test for a novel coronavirus associated with SARS. The assay is positive early in the disease. For modest numbers of specimens, the procedure (e.g., RNA extraction, reverse transcription, and real-time PCR) can be completed in 3\u20134 h. As testing of hundreds of suspected SARS cases can be needed during each day of an outbreak, we are also adapting this protocol to a high-throughput format. It is expected that the current rapid method of prompt identification of this pathogen will allow prompt identification of this virus and thus facilitate control of the disease and provision of prompt and appropriate treatment to patients.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "\nFigure 1.\n: Detection of a novel human coronavirus associated with SARS by real-time quantitative PCR (a), and melting curve analysis of PCR products from clinical samples (b). (a), amplification plot of fluorescence intensity against the PCR cycle. The copy numbers of input plasmid DNA in the reactions are indicated. The x axis denotes the cycle number of a quantitative PCR assay, and the y axis denotes the fluorescence intensity (F1) over the background. (b), melting curve analysis of PCR products from clinical samples. Signals from positive (+ve) samples, negative (\u2212ve) samples, and a water control (water) are indicated. The x axis denotes the temperature (\u00b0C), and the y axis denotes the fluorescence intensity (F1) over the background.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Wkly Epidemiol Rec",
            "volume": "78",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Wkly Epidemiol Rec",
            "volume": "78",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 1974,
            "venue": "Curr Top Microbiol Immunol",
            "volume": "63",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 1990,
            "venue": "Acta Virol",
            "volume": "34",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "MMWR Morbid Mortal Wkly Rep",
            "volume": "52",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "Clin Chem",
            "volume": "47",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}