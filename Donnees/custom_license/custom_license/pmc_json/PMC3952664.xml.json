{
    "paper_id": "PMC3952664",
    "metadata": {
        "title": "\nIFITM3 and Susceptibility to Respiratory Viral Infections in the Community",
        "authors": [
            {
                "first": "Tara",
                "middle": [
                    "C."
                ],
                "last": "Mills",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anna",
                "middle": [],
                "last": "Rautanen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Katherine",
                "middle": [
                    "S."
                ],
                "last": "Elliott",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tom",
                "middle": [],
                "last": "Parks",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Vivek",
                "middle": [],
                "last": "Naranbhai",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Margareta",
                "middle": [
                    "M."
                ],
                "last": "Ieven",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Christopher",
                "middle": [
                    "C."
                ],
                "last": "Butler",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Paul",
                "middle": [],
                "last": "Little",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Theo",
                "middle": [],
                "last": "Verheij",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Chris",
                "middle": [
                    "S."
                ],
                "last": "Garrard",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Charles",
                "middle": [],
                "last": "Hinds",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Herman",
                "middle": [],
                "last": "Goossens",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Stephen",
                "middle": [],
                "last": "Chapman",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Adrian",
                "middle": [
                    "V.",
                    "S."
                ],
                "last": "Hill",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Genotyping of rs12252 was performed by restriction fragment length polymorphism using primers CAGGAAAAGGAAACTGTTGAGAACC(F) and CTCCTGGAGCCTCCTCCTCA(R) in standard polymerase chain reaction (PCR) conditions. Primers had 3\u2032 penultimate base mismatches to IFITM3 to ensure specific amplification of IFITM3 rather than IFITM2. MScI (New England Biolabs) was used to cut the PCR product in the presence of the wild-type T allele. Fragments with lengths of 572, 426, and/or 146 base pairs were visualized on agarose gels, according to the genotype of the sample. All minor homozygotes and heterozygotes were Sanger sequenced in the reverse direction to confirm their genotype, using the same primers.",
            "cite_spans": [],
            "section": "Genotyping Methods ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "The GAinS study recruited adult patients admitted to ICUs in the United Kingdom with community-acquired pneumonia or fecal peritonitis [8]. Exclusion criteria for this study were inclusion in an interventional study of a novel intervention, immunosuppression (known regular systemic corticosteroid therapy, known regular therapy with other immunosuppressive agent, known presence of human immunodeficiency virus (HIV) positive or AIDS, neutrophil count <1000 mm3 owing to any cause; details reported elsewhere [8]), presence of a directive to withhold or withdraw life-sustaining treatment, or admission to ICU for palliative care only. Individuals with community-acquired pneumonia, H1N1 infection and self-reported Caucasian ancestry were included in the analysis presented here. All of these patients had chest radiographic evidence of pneumonia. None had malignant disease or liver cirrhosis, was receiving systemic steroids, or had significant premorbid exercise restriction. In contrast to the cohort studied by Everitt et al [3], none of the patients in the GAinS study was pregnant. Only 2 of them had chronic obstructive pulmonary disease, and 5 had asthma. Superadded or concurrent infections were identified in 7 patients (3 with Staphylococcus aureus and 1 each with Staphylococcus hominis, Haemophilus influenzae, Mycoplasma pneumoniae, and respiratory syncytial virus). All patients were mechanically ventilated except for 1.",
            "cite_spans": [
                {
                    "start": 136,
                    "end": 137,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 511,
                    "end": 512,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 1033,
                    "end": 1034,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Sample Collections ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "GRACE [9] is a Europe-wide study of LRTI in primary care. Patients and controls used in this study were collected from 14 primary healthcare networks in 11 European countries, with patients matched to local controls by age, sex, and time of sampling. Subjects of non-European origin were excluded from the analysis. Eligible patients were aged \u226518 years, consulting for the first time with an acute cough (duration, \u226428 days) as the main symptom or acute LRTI as the primary diagnosis. None of the CC genotype patients with LRTI in this study were hospitalized. In house real-time PCR was performed to diagnose viral infection in 3 collaborating laboratories based on previous validation of the different nucleic acid amplification methods used in the respective laboratories [10].",
            "cite_spans": [
                {
                    "start": 7,
                    "end": 8,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 777,
                    "end": 779,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Sample Collections ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "All analyses included 2623 Caucasian European controls from GRACE. Two-tailed Fisher's exact test was used for the H1N1 analyses (SPSS software; version 18). GRACE patients and controls from across Europe were analyzed using logistic regression in PLINK software [11, 12], with country as a covariate. All Hardy-Weinberg equilibrium P values were calculated using PLINK software [11, 12].",
            "cite_spans": [
                {
                    "start": 264,
                    "end": 266,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 268,
                    "end": 270,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 380,
                    "end": 382,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 384,
                    "end": 386,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Statistical Methods ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "Association analysis results between our cohort of patients with severe H1N1 and a large collection of controls, the largest available collection of directly genotyped Caucasians for rs12252 that we know of, showed no significant association (Table 1). We observed no CC homozygotes among our patients with severe H1N1 infection and nearly identical frequencies of TC heterozygotes in our patients and controls. However, association analysis between these controls and the combined patients with severe influenza (data from our study and Everitt et al [3]) suggests a recessive model of association (Table 1), rather than the additive model originally described [3]. Rare homozygotes are driving this association and show an odds ratio of 23.38, with no association for heterozygotes compared with major homozygotes (odds ratio, 1.05; 95% confidence interval, .48\u20132.30).\n",
            "cite_spans": [
                {
                    "start": 553,
                    "end": 554,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 663,
                    "end": 664,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "RESULTS",
            "ref_spans": [
                {
                    "start": 249,
                    "end": 250,
                    "mention": "1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 606,
                    "end": 607,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "We extended our study to assess the possible role of IFITM3 rs12252 in susceptibility to mild viral LRTI diagnosed in primary care (n = 1248), including patients with rhinovirus (n = 498), influenza (n = 240), coronavirus (n = 169), respiratory syncytial virus (n = 116), human metapneumovirus (n = 110), parainfluenza virus (n = 61), or coinfection with 2 of these viruses (n = 54). Comparing the IFITM3 rs12252 genotypes of the combined virally infected primary care patients with those of the controls demonstrated an association for the recessive test (P = .049; Table 1) that was more significant in the subgroup of patients with disease due to influenza virus (P = .025; Table 1). As with severe H1N1, the best fitting model of association was recessive. When patients with noninfluenza viral infection were studied, no association was found (Table 1). No association was seen in rhinovirus or coronavirus specific analyses either (data not shown). We found no evidence of association between rs12252 and bacterial LRTI (n = 379; data not shown).",
            "cite_spans": [],
            "section": "RESULTS",
            "ref_spans": [
                {
                    "start": 573,
                    "end": 574,
                    "mention": "1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 683,
                    "end": 684,
                    "mention": "1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 855,
                    "end": 856,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "Without directly genotyped controls and large sample sizes, it is not possible to fully define genetic associations. We did not find an association between rs12252 and our patients with H1N1 influenza, probably owing to small sample size. However, by extending the case series of influenza patients reported by Everitt et al [3] and using our large collection of directly genotyped controls, we find a distinct genetic model for severe influenza susceptibility in humans. However, even with the combination of our genotyped patients with H1N1 influenza and the patients with influenza studied by Everitt et al [3] (85% of whom had H1N1 influenza), the number of patients in this study is small and the association relies on only 3 homozygotes genotyped by Everitt et al [3]. Not all of these 3 individuals have undergone population stratification analysis, and given the large differences in the allele frequency of this SNP between populations, population outliers could easily have biased this result. More patients with H1N1 influenza would be needed to better define this association in Europe.",
            "cite_spans": [
                {
                    "start": 326,
                    "end": 327,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 611,
                    "end": 612,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 771,
                    "end": 772,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "Moreover, by analyzing large numbers of patients with community-acquired LRTIs and matched controls, we show the same recessive association for mild viral infections. We find the most significant association with susceptibility to mild influenza infection, in contrast to Zhang et al [7], who found an association with severe but not mild influenza. The interferon-pathway gene IFITM3 therefore seems to act as a susceptibility locus for influenza infection in humans, encompassing both very mild and life-threatening disease. We found no evidence that this SNP is associated with mild bacterial infection, which is unsurprising because IFITM3 has not been implicated in the control of bacterial infection. Although IFITM3 has been shown to mediate infection by multiple viruses, including influenza A, HIV-1, yellow fever virus, and West Nile virus [1, 2, 13\u201315], of the pathogens we have studied, the association seemed to be specific for influenza susceptibility. Therefore future studies with other large viral sample sets are necessary to further define this genetic association.",
            "cite_spans": [
                {
                    "start": 285,
                    "end": 286,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 851,
                    "end": 852,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 854,
                    "end": 855,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 857,
                    "end": 862,
                    "mention": "13\u201315",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "One limitation of our study is that population stratification analysis has not been performed for our samples. Whole-genome genotyping has not been performed to make this possible. Self-reported ancestry has been used to remove non-Caucasian subjects from the analysis.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        },
        {
            "text": "There have been many case-control studies of severe viral and bacterial infectious disease, which are almost always recruited in a hospital setting. However, there have been very few studies of the genetics of susceptibility to common mild infections that are generally managed in primary care. This is the first large genetic study, of which we are aware, assessing susceptibility to mild LRTIs in a primary care (general practice) setting. Our data suggest that larger studies of mild infection phenotypes in addition to severe cases could help distinguish between genetic determinants of initial infection as opposed to severe consequences of infection. Moreover, the large variety of microbial causes of common infections in primary care may allow more efficient identification of genetic loci that affect the risk of infection by multiple pathogens.",
            "cite_spans": [],
            "section": "DISCUSSION",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1.: Association Results for IFITM3 SNP rs12252a\n",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "The IFITM proteins mediate cellular resistance to influenza A H1N1 virus, West Nile virus, and dengue virus",
            "authors": [
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Brass",
                    "suffix": ""
                },
                {
                    "first": "IC",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Benita",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Cell",
            "volume": "139",
            "issn": "",
            "pages": "1243-54",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Performance of different mono- and multiplex nucleic acid amplification tests on a multipathogen external quality assessment panel",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Loens",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "van Loon",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Coenjaerts",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "J Clin Microbiol",
            "volume": "50",
            "issn": "",
            "pages": "977-87",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Purcell",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "PLINK v1.07",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "PLINK: a tool set for whole-genome association and population-based linkage analyses",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Purcell",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Neale",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Todd-Brown",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Am J Hum Genet",
            "volume": "81",
            "issn": "",
            "pages": "559-75",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Identification of five interferon-induced cellular proteins that inhibit West Nile virus and dengue virus infections",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Jiang",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Weidner",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Qing",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "J Virol",
            "volume": "84",
            "issn": "",
            "pages": "8332-41",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Distinct patterns of IFITM-mediated restriction of filoviruses, SARS coronavirus, and influenza A virus",
            "authors": [
                {
                    "first": "IC",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Bailey",
                    "suffix": ""
                },
                {
                    "first": "JL",
                    "middle": [],
                    "last": "Weyer",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "PLoS Pathog",
            "volume": "7",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "A diverse range of gene products are effectors of the type I interferon antiviral response",
            "authors": [
                {
                    "first": "JW",
                    "middle": [],
                    "last": "Schoggins",
                    "suffix": ""
                },
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "Wilson",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Panis",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Nature",
            "volume": "472",
            "issn": "",
            "pages": "481-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "IFITM proteins restrict viral membrane hemifusion",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "RM",
                    "middle": [],
                    "last": "Markosyan",
                    "suffix": ""
                },
                {
                    "first": "Y-M",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "PLoS Pathog",
            "volume": "9",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "IFITM3 restricts the morbidity and mortality associated with influenza",
            "authors": [
                {
                    "first": "AR",
                    "middle": [],
                    "last": "Everitt",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Clare",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Pertel",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Nature",
            "volume": "484",
            "issn": "",
            "pages": "519-23",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "An integrated map of genetic variation from 1,092 human genomes",
            "authors": [],
            "year": 2012,
            "venue": "Nature",
            "volume": "491",
            "issn": "",
            "pages": "56-65",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [
                {
                    "first": "LA",
                    "middle": [],
                    "last": "Hindorff",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "MacArthur",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Morales",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "A catalog of published genome-wide association studies",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Genotype imputation for genome-wide association studies",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Marchini",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Howie",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Nat Rev Genet",
            "volume": "11",
            "issn": "",
            "pages": "499-511",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Interferon-induced transmembrane protein-3 genetic variant rs12252-C is associated with severe influenza in Chinese individuals",
            "authors": [
                {
                    "first": "Y-H",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Nature Comms",
            "volume": "4",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [
                {
                    "first": "CJ",
                    "middle": [],
                    "last": "Hinds",
                    "suffix": ""
                },
                {
                    "first": "CS",
                    "middle": [],
                    "last": "Garrard",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "UK Critical Care Genomics (UKCCG) group",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Amoxicillin for acute lower-respiratory-tract infection in primary care when pneumonia is not suspected: a 12-country, randomised, placebo-controlled trial",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Little",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Stuart",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Moore",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Lancet Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "123-9",
            "other_ids": {
                "DOI": []
            }
        }
    }
}