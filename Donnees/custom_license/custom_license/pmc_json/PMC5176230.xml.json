{
    "paper_id": "PMC5176230",
    "metadata": {
        "title": "Norovirus Infection in Harbor Porpoises",
        "authors": [
            {
                "first": "Miranda",
                "middle": [],
                "last": "de Graaf",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rogier",
                "middle": [],
                "last": "Bodewes",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Cornelis",
                "middle": [
                    "E."
                ],
                "last": "van Elk",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marco",
                "middle": [],
                "last": "van de Bildt",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sarah",
                "middle": [],
                "last": "Getu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Georgina",
                "middle": [
                    "I."
                ],
                "last": "Aron",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Georges",
                "middle": [
                    "M.G.M."
                ],
                "last": "Verjans",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Albert",
                "middle": [
                    "D.M.E."
                ],
                "last": "Osterhaus",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Judith",
                "middle": [
                    "M.A."
                ],
                "last": "van den Brand",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Thijs",
                "middle": [],
                "last": "Kuiken",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marion",
                "middle": [
                    "P.G."
                ],
                "last": "Koopmans",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "A juvenile male harbor porpoise (Phocoena phocoena) \u224810.5 months of age was found alive on the coast of the Netherlands on May 10, 2012, and was transported to the SOS Dolphin Foundation (Harderwijk, the Netherlands) for rehabilitation (Technical Appendix). Important clinical signs at the rehabilitation center were anorexia, labored breathing, and disorientation. The animal showed no evidence of gastrointestinal disease, such as vomiting or diarrhea. Eight days after arrival in the rehabilitation center, the animal was euthanized because of the severity of clinical signs, and necropsy was performed according to standard procedures (8). The main pathology findings were bronchopneumonia associated with lungworm infection and encephalitis and hepatitis of unknown cause. The intestine did not show significant lesions macroscopically; microscopically, the enterocytes at the luminal surface of the intestine had sloughed into the lumen as a result of freeze\u2013thaw artifact. The cells lining the intestinal crypts consisted of a mixture of enterocytes and mucus cells. The proportion of mucus cells increased progressively toward the end of the intestine. The lamina propria was infiltrated diffusely with a moderate number of lymphocytes, plasma cells, and eosinophils. This infiltrate was considered normal for this species.",
            "cite_spans": [
                {
                    "start": 640,
                    "end": 641,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "In the frame of a research program focusing on the identification of new viruses in possible reservoir hosts, we collected fecal material and performed random PCR in combination with 454-sequencing as described previously (9). This analysis resulted in 5,774 reads, of which 88 reads were most closely related to the norovirus genus, as determined by blastn and blastx analysis (10). Other reads that were most similar to viral genomes were most closely related to a coronavirus (16 reads [35%\u201394% nt identity]), salmon calicivirus (3 reads [11%\u201369% nt identity]), and porcine anello virus (1 read [91% nt identity], 2 reads [36% amino acid identity]). The harbor porpoise norovirus (HPNV) sequence was confirmed by Sanger sequencing (6,293 nt; GenBank accession no. KP987888) by using specific primers and comprising 3 ORFs, the partial ORF1 encoding the putative polyprotein, ORF2 encoding viral protein (VP) 1, and a partial ORF3 encoding VP2 (Figure 1, panel A). Phylogenetic analysis revealed that the HPNV RNA-dependent RNA-polymerase encoded by ORF1 clustered together with human genogroup I(GI) sequences (Figure 1, panel B), whereas VP1 clustered near strains belonging to human GI and bovine GIII (Figure 1, panel C).",
            "cite_spans": [
                {
                    "start": 223,
                    "end": 224,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 379,
                    "end": 381,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 947,
                    "end": 955,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1114,
                    "end": 1122,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1208,
                    "end": 1216,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "To determine the tissue tropism of HPNV, we extracted RNA from formalin-fixed paraffin-embedded (FFPE) tissues collected from the HPNV-positive animal for histopathology. Tissues from all main organs and lymph nodes were tested for HPNV RNA by real-time PCR (Technical Appendix). Only the intestinal tissue and the FFPE material for immunohistochemical analysis (containing a mixture of tissues) were positive for norovirus with cycle threshold values of 30.5 and 37.4, respectively.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "We conducted in situ hybridization to determine the cellular tropism of HPNV, as described previously (Figure 2) (12). HPNV-specific transcripts were detected in the cells in the intestine, indicating that this virus replicates in the intestinal tract. Sequential slides stained with hematoxylin and eosin or pankeratin showed that positive cells corresponded with enterocytes that had sloughed into the intestinal lumen because of freeze\u2013thaw artifact, although we cannot exclude the possibility that other cell types were present (Figure 2).",
            "cite_spans": [
                {
                    "start": 114,
                    "end": 116,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 103,
                    "end": 111,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 533,
                    "end": 541,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "To estimate the percentage of norovirus-infected harbor porpoises in our dataset, we extracted RNA from FFPE porpoise intestinal tissues collected from 48 animals during a 10-year period. Including the animal in which we detected HPNV, 10% (5/49) of the animals were positive for norovirus (cycle threshold <37) (Table 1), with primers designed to detect HPNV. Macroscopic and microscopic examination of the intestine of these animals showed no pathologic differences from harbor porpoises without norovirus infection.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 313,
                    "end": 320,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "To detect norovirus-specific antibodies in porpoise serum, we developed an ELISA based on HPNV VP1 and subsequently screened 34 harbor porpoise serum samples collected during 2006\u20132015 in the Netherlands (Technical Appendix). Samples from 8 (24%) harbor porpoises were positive for norovirus antibodies (Table 2). This dataset included samples from 2 harbor porpoises that were positive for norovirus RNA; however, their serum samples were negative for norovirus-specific antibodies.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 304,
                    "end": 311,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Similar to human noroviruses, HPNV replicates in the intestine. B cells and enterocytes support human norovirus replication in vitro (13,14). We detected HPNV in cells corresponding to enterocytes, and it will be interesting to determine whether these viruses share receptor use with other noroviruses. In humans, norovirus infections are self-limiting in healthy persons but can result in illness and death in high-risk groups (4). Because the harbor porpoise in which we detected HPNV did not exhibit clinical signs of gastrointestinal disease, norovirus infection probably was not a major factor in the death of this animal.",
            "cite_spans": [
                {
                    "start": 134,
                    "end": 136,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 137,
                    "end": 139,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 429,
                    "end": 430,
                    "mention": "4",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Remarkably, the HPNV displayed 99% sequence homology to a short (300-nt) norovirus VP1 sequence detected in oysters (15). These oysters had been sampled because they were associated with a foodborne gastroenteritis outbreak in Ireland in 2012 (15). Oyster samples were collected from the restaurant where the outbreak occurred and from their harvesting area. Strains belonging to genotypes GI.1, GI.4, GII.4, GII.3, GII.1, GII.6, GI.2, GII.7, GI.11, and the strain that was homologous to HPNV were detected, although the HPNV-like strain was detected only in oysters from the harvesting area. In the patients, only GI.4, GI.2, GI.6, GII.1, and GII.7 strains were detected, but the fact that noroviruses infecting marine mammals closely related to human noroviruses have been found infecting harbor porpoises and contaminating oysters raises the question of whether HPNV could infect humans through contamination of oysters or other shellfish.",
            "cite_spans": [
                {
                    "start": 117,
                    "end": 119,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 244,
                    "end": 246,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "On the basis of our findings that norovirus infections might be a common infection in harbor porpoises from the southern North Sea and the detection of a norovirus in a sea lion (3), it is not unlikely that noroviruses are common in other marine mammals as well. The high genetic diversity within this genus complicates detection of new noroviruses. The discovery of HPNV and the recent discovery of noroviruses in bats highlight that much still remains to be discovered about animal reservoirs of noroviruses and triggers questions about the zoonotic potential of these viruses.",
            "cite_spans": [
                {
                    "start": 179,
                    "end": 180,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Genetic characterization of harbor porpoise norovirus. A) Genome organization of harbor porpoise norovirus. The putative cleavage sites are shown with arrowheads. B, C) Maximum-likelihood trees of the RNA-dependent RNA-polymerase (B) and ORF 2 (C) were inferred by PhyML 3.0 software (http://www.atgc-montpellier.fr/phyml/) by using the general time reversible nucleotide substitution model. Selected bootstrap values >70 are depicted. Scale bars indicate nucleotide substitutions per site. NS, nonstructural; NTPase, nucleoside triphosphatase; ORF, open reading frame; P, protein; Pol, polymerase; Pro, protease; VP, viral protein. ",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Detection of harbor porpoise norovirus transcripts in intestinal tissue of a harbor porpoise (Phocoena phocoena) using in situ hybridization with probes designed by Advanced Cell Diagnostics (Hayward, CA, USA), based on the 6,293 nt of harbor porpoise norovirus (A, C, E; original magnification \u00d740, \u00d7100, \u00d7100, respectively). Consecutive slides were stained with hematoxylin and eosin (B, D; original magnification \u00d740, \u00d7100, respectively) and pankeratin (F, original magnification \u00d7100), as described previously (11).",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Advances in laboratory methods for detection and typing of norovirus.",
            "authors": [],
            "year": 2015,
            "venue": "J Clin Microbiol",
            "volume": "53",
            "issn": "",
            "pages": "373-81",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.01535-14"
                ]
            }
        },
        "BIBREF1": {
            "title": "Metagenomic survey for viruses in Western Arctic caribou, Alaska, through iterative assembly of taxonomic units.",
            "authors": [],
            "year": 2014,
            "venue": "PLoS One",
            "volume": "9",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.pone.0105227"
                ]
            }
        },
        "BIBREF2": {
            "title": "Pandemic 2009 H1N1 influenza virus causes diffuse alveolar damage in cynomolgus macaques.",
            "authors": [],
            "year": 2010,
            "venue": "Vet Pathol",
            "volume": "47",
            "issn": "",
            "pages": "1040-7",
            "other_ids": {
                "DOI": [
                    "10.1177/0300985810374836"
                ]
            }
        },
        "BIBREF3": {
            "title": "Central nervous system disease and genital disease in harbor porpoises (Phocoena phocoena) are associated with different herpesviruses.",
            "authors": [],
            "year": 2016,
            "venue": "Vet Res (Faisalabad)",
            "volume": "47",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/s13567-016-0310-8"
                ]
            }
        },
        "BIBREF4": {
            "title": "Replication of human noroviruses in stem cell-derived human enteroids.",
            "authors": [],
            "year": 2016,
            "venue": "Science",
            "volume": "353",
            "issn": "",
            "pages": "1387-93",
            "other_ids": {
                "DOI": [
                    "10.1126/science.aaf5211"
                ]
            }
        },
        "BIBREF5": {
            "title": "Enteric bacteria promote human and mouse norovirus infection of B cells.",
            "authors": [],
            "year": 2014,
            "venue": "Science",
            "volume": "346",
            "issn": "",
            "pages": "755-9",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1257147"
                ]
            }
        },
        "BIBREF6": {
            "title": "Norovirus genotypes implicated in two oyster-related illness outbreaks in Ireland.",
            "authors": [],
            "year": 2014,
            "venue": "Epidemiol Infect",
            "volume": "142",
            "issn": "",
            "pages": "2096-104",
            "other_ids": {
                "DOI": [
                    "10.1017/S0950268813003014"
                ]
            }
        },
        "BIBREF7": {
            "title": "Deciphering the bat virome catalog to better understand the ecological diversity of bat viruses and the bat origin of emerging infectious diseases.",
            "authors": [],
            "year": 2016,
            "venue": "ISME J",
            "volume": "10",
            "issn": "",
            "pages": "609-20",
            "other_ids": {
                "DOI": [
                    "10.1038/ismej.2015.138"
                ]
            }
        },
        "BIBREF8": {
            "title": "The fecal viral flora of California sea lions.",
            "authors": [],
            "year": 2011,
            "venue": "J Virol",
            "volume": "85",
            "issn": "",
            "pages": "9909-17",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.05026-11"
                ]
            }
        },
        "BIBREF9": {
            "title": "Human norovirus transmission and evolution in a changing world.",
            "authors": [],
            "year": 2016,
            "venue": "Nat Rev Microbiol",
            "volume": "14",
            "issn": "",
            "pages": "421-33",
            "other_ids": {
                "DOI": [
                    "10.1038/nrmicro.2016.48"
                ]
            }
        },
        "BIBREF10": {
            "title": "Norovirus recombination in ORF1/ORF2 overlap.",
            "authors": [],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "1079-85",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1107.041273"
                ]
            }
        },
        "BIBREF11": {
            "title": "Strain-dependent norovirus bioaccumulation in oysters.",
            "authors": [],
            "year": 2011,
            "venue": "Appl Environ Microbiol",
            "volume": "77",
            "issn": "",
            "pages": "3189-96",
            "other_ids": {
                "DOI": [
                    "10.1128/AEM.03010-10"
                ]
            }
        },
        "BIBREF12": {
            "title": "The European Union summary report on trends and sources of zoonoses, zoonotic agents and food-borne outbreaks in 2012.",
            "authors": [],
            "year": 2014,
            "venue": "EFSA J",
            "volume": "12",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.2903/j.efsa.2014.3547"
                ]
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [],
            "year": 1994,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Novel B19-like parvovirus in the brain of a harbor seal.",
            "authors": [],
            "year": 2013,
            "venue": "PLoS One",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.pone.0079259"
                ]
            }
        }
    }
}