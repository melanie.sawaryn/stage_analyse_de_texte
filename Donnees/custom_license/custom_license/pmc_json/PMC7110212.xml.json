{
    "paper_id": "PMC7110212",
    "metadata": {
        "title": "Hantavirus Pulmonary Syndrome: The Sound of a Mouse Roaring",
        "authors": [
            {
                "first": "Joel",
                "middle": [
                    "M."
                ],
                "last": "Montgomery",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Thomas",
                "middle": [
                    "G."
                ],
                "last": "Ksiazek",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ali",
                "middle": [
                    "S."
                ],
                "last": "Khan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In the spring of 1993, a mouse roared. It was an unusual sound that took months to be registered by astute clinicians and the public health system [1]. Today, hantavirus pulmonary syndrome (HPS) is widely recognized as a distinctive clinical entity; it is associated with a precipitous cardiorespiratory decomposition, thrombocytopenia, and atypical lymphocytes on a peripheral blood smear and is transmitted by rodents throughout the Americas [2]. The deer mouse (Peromyscus maniculatus) and Sin Nombre virus were quickly identified as the primary reservoir and etiological agent of disease, respectively, in the originally recognized outbreak in the southwestern United States [3] and, subsequently, in most of North America.",
            "cite_spans": [
                {
                    "start": 148,
                    "end": 149,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 445,
                    "end": 446,
                    "mention": "2",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 680,
                    "end": 681,
                    "mention": "3",
                    "ref_id": "BIBREF19"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The initial recognition of HPS in the United States hinged on the combined employment of some classically trained virologists and the relatively new skills of molecular biologists at the Department of Health and Human Services (DHHS)/ Centers for Disease Control and Prevention (CDC) and the Department of Defense (DoD) laboratories who had studied and refined diagnostic tests and treatments for hantaviruses of little relevance to the United States associated with Korean hemorrhagic fever and other hemorrhagic fevers with renal syndrome in Eurasia [4]. Their pioneering work led to a reclassification of hantaviruses into 2 distinct groups: (1) Old World hantaviruses, which are associated with renal syndrome, and (2) New World hantaviruses, which typically cause cardiopulmonary disease. The initial recognition in North America rapidly led to the description of numerous newly recognized viruses\u2014in excess of 30 species\u2014found throughout the Americas. New World hantaviruses are associated with a plethora of rodent host species in the sigmodontine subfamily, each with a unique rodent-virus species pairing. These viruses cause a spectrum of clinical illnesses designated as \u201cnew hantavirusassociated American hemorrhagic fever\u201d and are thought to represent a coevolutionary relationship between New World rodents and their viruses [2, 3, 5, 6].",
            "cite_spans": [
                {
                    "start": 553,
                    "end": 554,
                    "mention": "4",
                    "ref_id": "BIBREF20"
                },
                {
                    "start": 1340,
                    "end": 1341,
                    "mention": "2",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1343,
                    "end": 1344,
                    "mention": "3",
                    "ref_id": "BIBREF19"
                },
                {
                    "start": 1346,
                    "end": 1347,
                    "mention": "5",
                    "ref_id": "BIBREF21"
                },
                {
                    "start": 1349,
                    "end": 1350,
                    "mention": "6",
                    "ref_id": "BIBREF22"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The recognition of HPS coincided with an Institute of Medicine report on emerging infections that warned about complacency toward infectious diseases in the era of improved sanitation and immunizations [7]. Similar international recognition of these microbial threats led to a global response that was accelerated after an anthrax bioterrorism incident in the United States and that served the world well for the response to severe acute respiratory syndrome [8].",
            "cite_spans": [
                {
                    "start": 203,
                    "end": 204,
                    "mention": "7",
                    "ref_id": "BIBREF23"
                },
                {
                    "start": 460,
                    "end": 461,
                    "mention": "8",
                    "ref_id": "BIBREF24"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Andes virus (ANDV)\u2013associated HPS in Argentina and Chile has evidenced a unique predilection for limited person-toperson transmission. To date, there have been at least 6 reports supported by epidemiological and/or molecular data suggesting a potential for person-to-person transmission of ANDV [9, 10, 11, 12, 13, 14]. The latest is the elegant study by Ferr\u00e9s et al. [14] presented in this issue of the Journal; it prospectively followed 421 household contacts of patients with laboratory-confirmed ANDV infection to test the hypothesis that ANDV retains the ability to be transmitted from person to person. The authors determined that sex partners of patients with laboratory-confirmed cases were at the greatest risk of infection, with an estimated secondary attack rate of 2.5% and detectable viremia 5\u201315 days before the onset of symptoms. The biological basis for person-to-person transmission of ANDV requires definitive characterization; however, ANDV is the only hantavirus that has been isolated from human serum and that consistently kills Syrian hamsters [15]. This evidence suggests that infection with ANDV results in a higher viral load [16] and perhaps that humans are a much more permissive host for this particular strain than for other hantaviruses. The data suggest that person-toperson transmission is the result of direct contact during the preclinical phase of infection (which is consistent with the timing of viremia) and that the disease is most likely to have an incubation period of 2\u2013 4 weeks and to occur after contact with subsequently severely ill patients. It also explains the paucity of nosocomial transmission, because contact with health care workers is likely to occur much later, during the clinical phase of infection.",
            "cite_spans": [
                {
                    "start": 296,
                    "end": 297,
                    "mention": "9",
                    "ref_id": "BIBREF25"
                },
                {
                    "start": 299,
                    "end": 301,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 303,
                    "end": 305,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 307,
                    "end": 309,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 311,
                    "end": 313,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 315,
                    "end": 317,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 370,
                    "end": 372,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1069,
                    "end": 1071,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1154,
                    "end": 1156,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We are becoming ever more cognizant that the vast majority of emerging pathogens have zoonotic origins [17]. Through limited person-to-person transmission events, some of these pathogens may be able to establish themselves in a new permissive host or vector. HIV, influenza virus, measles virus, Plasmodium falciparum, and smallpox virus\u2014all pathogens hypothesized to be of zoonotic origin\u2014are but a few examples of human pathogens arising from evolutionary events across millennia. [18, 19, 20, 21, 22]. Human dengue and urban yellow fever may be examples of a new reservoir allowing sustained person-to-person transmission [23]. Limited person-to-person transmission also characterizes the current panzoonotic H5N1 strain of influenza virus, for which there has been intense scrutiny of the biological mechanism that enables human transmission.",
            "cite_spans": [
                {
                    "start": 104,
                    "end": 106,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 484,
                    "end": 486,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 488,
                    "end": 490,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 492,
                    "end": 494,
                    "mention": "20",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 496,
                    "end": 498,
                    "mention": "21",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 500,
                    "end": 502,
                    "mention": "22",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 626,
                    "end": 628,
                    "mention": "23",
                    "ref_id": "BIBREF15"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Global pandemic influenza preparedness illustrates a unique challenge for the public health system: responding to diseases that result from the intersection of animals, humans, and the environment. Success will require coordination between the animal and human health sectors [24, 25]. The ongoing H5N1 epizootic, the Rift Valley fever outbreaks in eastern Africa, and the monkeypox outbreak in the United States have illustrated that need. This is an area that has been woefully neglected to date. As the world's population continues to grow, interactions between humans and novel pathogens are likely to increase, resulting in greater numbers of emerging and reemerging diseases [26]. To help understand the ecologies of infectious diseases, the DHHS/CDC, in cooperation with the World Health Organization (WHO) and numerous ministries of health, is currently establishing Global Disease Detection (GDD) centers in 5 of the 6 WHO regions across the globe. These GDD centers will work in concert with the DoD\u2013Global Emerging Infections Surveillance (GEIS) centers as partners within the WHO\u2013Global Outbreak and Alert Response Network (GOARN). The interactions between the DHHS/CDC-GDD centers, the DoD-GEIS centers, and WHOGOARN will undoubtedly enhance global disease surveillance and host country capacity, reducing the overall time from outbreak recognition to response and disease prevention.",
            "cite_spans": [
                {
                    "start": 277,
                    "end": 279,
                    "mention": "24",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 281,
                    "end": 283,
                    "mention": "25",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 682,
                    "end": 684,
                    "mention": "26",
                    "ref_id": "BIBREF18"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "These efforts need to be reinforced with coordinated surveillance and rapid-response activities through interactions between ministries of health, US government agencies (DHHS, the Department of Agriculture, the Department of State, and the DoD), United Nations agencies (WHO and the Food and Agriculture Organization), the World Organization for Animal Health, and academia and other nongovernmental organizations (e.g., M\u00e9decins sans Fronti\u00e8res). We also need increasing emphasis on building local capacities and conducting integrative studies across the animal, human, and environmental domains; on discovery research; and on strategic partnerships. Recognizing and preventing the next pandemic\u2014be it of H5N1 influenza, an efficient human-to-human hantavirus, or an as-yet unidentified pathogen\u2014can be achieved only through strategic animal/human health partnerships and enhanced global disease surveillance.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Outbreak of acute illness\u2014southwestern United States, 1993",
            "authors": [],
            "year": 1993,
            "venue": "MMWR",
            "volume": "42",
            "issn": "",
            "pages": "421-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "An outbreak of hantavirus pulmonary syndrome, Chile, 1997",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Toro",
                    "suffix": ""
                },
                {
                    "first": "JD",
                    "middle": [],
                    "last": "Vega",
                    "suffix": ""
                },
                {
                    "first": "AS",
                    "middle": [],
                    "last": "Khan",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Emerg Infect Dis",
            "volume": "4",
            "issn": "",
            "pages": "687-94",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Hantavirus pulmonary syndrome outbreak in Argentina:molecular evidence of person-to-person transmission of Andes virus",
            "authors": [
                {
                    "first": "PJ",
                    "middle": [],
                    "last": "Padula",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Edelstein",
                    "suffix": ""
                },
                {
                    "first": "SD",
                    "middle": [],
                    "last": "Miguel",
                    "suffix": ""
                },
                {
                    "first": "NM",
                    "middle": [],
                    "last": "Lo\u00f3pez",
                    "suffix": ""
                },
                {
                    "first": "CM",
                    "middle": [],
                    "last": "Rossi",
                    "suffix": ""
                },
                {
                    "first": "RD",
                    "middle": [],
                    "last": "Rabinovich",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Virology",
            "volume": "241",
            "issn": "",
            "pages": "323-30",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Person-to-person transmission of Andes virus",
            "authors": [
                {
                    "first": "VP",
                    "middle": [],
                    "last": "Martinez",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Bellomo",
                    "suffix": ""
                },
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Juan",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "1848-53",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Clusters of hantavirus infection, southern Argentina",
            "authors": [
                {
                    "first": "ME",
                    "middle": [],
                    "last": "L\u00e1zaro",
                    "suffix": ""
                },
                {
                    "first": "GE",
                    "middle": [],
                    "last": "Cantoni",
                    "suffix": ""
                },
                {
                    "first": "LM",
                    "middle": [],
                    "last": "Calanni",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Emerg Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "104-10",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Prospective evaluation of household contacts of persons with hantavirus cardiopulmonary syndrome in Chile",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ferr\u00e9s",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Vial",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Marco",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Infect Dis",
            "volume": "195",
            "issn": "",
            "pages": "1563-71",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "A lethal disease model for hantavirus pulmonary syndrome",
            "authors": [
                {
                    "first": "JW",
                    "middle": [],
                    "last": "Hooper",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Larsen",
                    "suffix": ""
                },
                {
                    "first": "DM",
                    "middle": [],
                    "last": "Custer",
                    "suffix": ""
                },
                {
                    "first": "CS",
                    "middle": [],
                    "last": "Schmaljohn",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Virology",
            "volume": "289",
            "issn": "",
            "pages": "6-14",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "First human isolate of hantavirus (Andes virus) in the Americas",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Galeno",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Mora",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Villagra",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Emerg Infect Dis",
            "volume": "8",
            "issn": "",
            "pages": "657-61",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Risk factors for human disease emergence",
            "authors": [
                {
                    "first": "LH",
                    "middle": [],
                    "last": "Taylor",
                    "suffix": ""
                },
                {
                    "first": "SM",
                    "middle": [],
                    "last": "Lathan",
                    "suffix": ""
                },
                {
                    "first": "ME",
                    "middle": [],
                    "last": "Woolhouse",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Philos Trans R Soc Lond B Biol Sci",
            "volume": "356",
            "issn": "",
            "pages": "983-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "AIDS as a zoonosis: scientific and public health implications",
            "authors": [
                {
                    "first": "BH",
                    "middle": [],
                    "last": "Hahn",
                    "suffix": ""
                },
                {
                    "first": "GM",
                    "middle": [],
                    "last": "Shaw",
                    "suffix": ""
                },
                {
                    "first": "KM",
                    "middle": [],
                    "last": "De Cock",
                    "suffix": ""
                },
                {
                    "first": "PM",
                    "middle": [],
                    "last": "Sharp",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Science",
            "volume": "287",
            "issn": "",
            "pages": "607-14",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Characterization of the 1918 influenza virus polymerase genes",
            "authors": [
                {
                    "first": "JK",
                    "middle": [],
                    "last": "Taubenberger",
                    "suffix": ""
                },
                {
                    "first": "AH",
                    "middle": [],
                    "last": "Reid",
                    "suffix": ""
                },
                {
                    "first": "RM",
                    "middle": [],
                    "last": "Lourens",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Jin",
                    "suffix": ""
                },
                {
                    "first": "TG",
                    "middle": [],
                    "last": "Fanning",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Nature",
            "volume": "437",
            "issn": "",
            "pages": "889-93",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Hantavirus pulmonary syndrome: the new American hemorrhagic fever",
            "authors": [
                {
                    "first": "CJ",
                    "middle": [],
                    "last": "Peters",
                    "suffix": ""
                },
                {
                    "first": "AS",
                    "middle": [],
                    "last": "Khan",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Clin Infect Dis",
            "volume": "34",
            "issn": "",
            "pages": "1224-31",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Progress in malaria research: the case for phylogenetics",
            "authors": [
                {
                    "first": "SM",
                    "middle": [],
                    "last": "Rich",
                    "suffix": ""
                },
                {
                    "first": "FJ",
                    "middle": [],
                    "last": "Ayala",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Adv Parasitol",
            "volume": "54",
            "issn": "",
            "pages": "255-80",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Genome sequence diversity and clues to the evolution of variola (smallpox) virus",
            "authors": [
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "Esposito",
                    "suffix": ""
                },
                {
                    "first": "SA",
                    "middle": [],
                    "last": "Sammons",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Frace",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Science",
            "volume": "313",
            "issn": "",
            "pages": "807-12",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "The origin of human pathogens: evaluating the role of agriculture and domestic animals in the evolution of human disease",
            "authors": [
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Pearce-Duvet",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Biol Rev Camb Philos Soc",
            "volume": "81",
            "issn": "",
            "pages": "369-82",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Host range, amplification and arboviral disease emergence",
            "authors": [
                {
                    "first": "SC",
                    "middle": [],
                    "last": "Weaver",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Arch Virol Suppl",
            "volume": "19",
            "issn": "",
            "pages": "33-44",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Microbial threats to\nhealth: emergence, detection, and response",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "Public health: pathogen surveillance in animals",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kuiken",
                    "suffix": ""
                },
                {
                    "first": "FA",
                    "middle": [],
                    "last": "Leighton",
                    "suffix": ""
                },
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Fouchier",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Science",
            "volume": "309",
            "issn": "",
            "pages": "1680-1",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "Wildlife, exotic pets, and emerging zoonoses",
            "authors": [
                {
                    "first": "BB",
                    "middle": [],
                    "last": "Chomel",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Belotto",
                    "suffix": ""
                },
                {
                    "first": "F-X",
                    "middle": [],
                    "last": "Meslin",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Emerg Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "6-11",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF19": {
            "title": "Genetic identification of a hantavirus associated with an outbreak of acute respiratory illness",
            "authors": [
                {
                    "first": "ST",
                    "middle": [],
                    "last": "Nichol",
                    "suffix": ""
                },
                {
                    "first": "CF",
                    "middle": [],
                    "last": "Spiropoulou",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Morzunov",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Science",
            "volume": "262",
            "issn": "",
            "pages": "914-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF20": {
            "title": "Epidemiology of hemorrhagic fever viruses",
            "authors": [
                {
                    "first": "JW",
                    "middle": [],
                    "last": "LeDuc",
                    "suffix": ""
                }
            ],
            "year": 1989,
            "venue": "Rev Infect Dis",
            "volume": "11",
            "issn": "Suppl 4",
            "pages": "S730-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF21": {
            "title": "Hantaviruses: etiologic agents of rare, but potentially life-threatening zoonotic diseases",
            "authors": [
                {
                    "first": "CH",
                    "middle": [],
                    "last": "Calisher",
                    "suffix": ""
                },
                {
                    "first": "JN",
                    "middle": [],
                    "last": "Mills",
                    "suffix": ""
                },
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "Root",
                    "suffix": ""
                },
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Beaty",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Am Vet Med Assoc",
            "volume": "222",
            "issn": "",
            "pages": "163-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF22": {
            "title": "Hantaviruses: A global disease problem",
            "authors": [
                {
                    "first": "CS",
                    "middle": [],
                    "last": "Schmaljohn",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Hjelle",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Emerg Infect Dis",
            "volume": "3",
            "issn": "",
            "pages": "95-104",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF23": {
            "title": "Emerging infections: microbial threats to health in the United States",
            "authors": [],
            "year": 1992,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF24": {
            "title": "A novel coronavirus associated with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "TG",
                    "middle": [],
                    "last": "Ksiazek",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "CS",
                    "middle": [],
                    "last": "Goldsmith",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1953-66",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF25": {
            "title": "An unusual hantavirus outbreak in southern Argentina: person-to-person transmission?Hantavirus Pulmonary Syndrome Study Groupfor Patagonia",
            "authors": [
                {
                    "first": "RM",
                    "middle": [],
                    "last": "Wells",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Sosa Estani",
                    "suffix": ""
                },
                {
                    "first": "ZE",
                    "middle": [],
                    "last": "Yadon",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Emerg Infect Dis",
            "volume": "3",
            "issn": "",
            "pages": "171-4",
            "other_ids": {
                "DOI": []
            }
        }
    }
}