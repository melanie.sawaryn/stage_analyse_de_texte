{
    "paper_id": "PMC6154147",
    "metadata": {
        "title": "Severe Respiratory Illness Outbreak Associated with Human Coronavirus NL63 in a Long-Term Care Facility",
        "authors": [
            {
                "first": "Julie",
                "middle": [],
                "last": "Hand",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Erica",
                "middle": [
                    "Billig"
                ],
                "last": "Rose",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Andrea",
                "middle": [],
                "last": "Salinas",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Xiaoyan",
                "middle": [],
                "last": "Lu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Senthilkumar",
                "middle": [
                    "K."
                ],
                "last": "Sakthivel",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Eileen",
                "middle": [],
                "last": "Schneider",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "John",
                "middle": [
                    "T."
                ],
                "last": "Watson",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Human coronaviruses (HCoVs) OC43, 229E, NL63, and HKU1 are frequently associated with upper respiratory tract infection but can also cause lower respiratory tract infections (LRTIs), such as pneumonia or bronchitis. Transmission of these viruses primarily occurs through respiratory droplets and indirect contact with secretions from infected persons. Signs and symptoms of illness often include runny nose, headache, cough, sore throat, and fever. LRTI occurs less frequently, but young children, older adults, and persons who are immunosuppressed appear to be at higher risk for these types of infections (1\u20133).",
            "cite_spans": [
                {
                    "start": 608,
                    "end": 609,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 610,
                    "end": 611,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "A wide range of respiratory viruses are known to circulate in long-term care facilities (LTCFs) and contribute to respiratory illness in the residents who live in them (4). Although outbreaks of HCoV-OC43 have been described among elderly populations in long-term care settings (5), outbreaks of severe respiratory illness associated with HCoV-NL63 have not, to our knowledge, been documented in LTCF settings.",
            "cite_spans": [
                {
                    "start": 169,
                    "end": 170,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 279,
                    "end": 280,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "On November 15, 2017, the Louisiana Department of Health (Baton Rouge, Louisiana, USA) was notified of a possible outbreak of severe respiratory illness by a representative of an LTCF that provides nursing home care and short-term rehabilitation services to 130 residents. At the time of notification, the facility reported 11 residents with chest radiograph\u2013confirmed pneumonia. For this investigation, we defined a case-patient as any LTCF resident with respiratory tract symptoms of new onset in November 2017, and we considered LRTI diagnoses that were based on clinical or radiologic evidence. During November 1\u201318, a total of 20 case-patients (60% male) of a median age of 82 (range 66\u201396) years were identified. The number of cases of respiratory illness peaked in mid-November. The most common symptoms were cough (95%) and chest congestion (65%). Shortness of breath, wheezing, fever, and altered mental status were also reported (Table). Sixteen (80%) case-patients had abnormal findings on chest radiograph; pneumonia was noted in 14. All case-patients had concurrent medical conditions; the most common were heart disease (70%, 14/20), dementia (65%, 13/20), hypertension (40%, 8/20), diabetes (35%, 7/20), and lung disease (35%, 7/20). Six (30%) case-patients required hospitalization; all had chest radiograph\u2013confirmed pneumonia. Hospitalized LRTI case-patients demonstrated shortness of breath (50% vs. 10%), wheezing (50% vs. 0%), and altered mental status (33% vs. 0%) more frequently than did nonhospitalized LRTI case-patients (Table).",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 940,
                    "end": 945,
                    "mention": "Table",
                    "ref_id": null
                },
                {
                    "start": 1548,
                    "end": 1553,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "We performed molecular diagnostic viral testing on nasopharyngeal specimens from 13 case-patients by real-time PCR at the Louisiana State Public Health Laboratory (Baton Rouge, Louisiana, USA) and the Centers for Disease Control and Prevention (Atlanta, Georgia, USA). Of the 13 available specimens, HCoV-NL63 was detected in 7 (54%); rhinovirus was co-detected in 2 specimens. Of the 6 specimens negative for HCoV-NL63, 1 was positive for rhinovirus, and 1 was positive for parainfluenza virus 1. For the 6 case-patients hospitalized with pneumonia, median length of hospital stay was 4 (range 1\u20135) days; none of these case-patients were mechanically ventilated or admitted to the intensive care unit. Nasopharyngeal specimens were available from 2 of the hospitalized case-patients, and HCoV-NL63 was detected in both. HCoV-NL63 respiratory infection was considered a contributory cause in the deaths of 3 case-patients (1 hospitalized and 2 nonhospitalized) that occurred 10\u201336 days after illness onset. The concurrent medical conditions of those who died included dementia, cardiovascular disease, cancer, multiple sclerosis, Parkinson disease, diabetes mellitus, hypertension, asthma, and chronic kidney disease.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Infection control measures, including adherence to standard and droplet precautions for symptomatic residents, reviewing hand and personal hygiene policies, and enhanced environmental cleaning, were implemented in the LTCF on November 15. All residents were monitored daily for the onset of respiratory symptoms. The case-patients resided in rooms throughout the facility. However, residents often shared rooms, walked throughout the facility, and spent much of their time in shared areas (e.g., gym, dining rooms, and recreational rooms). Because all case-patients had visited the gym at the facility for recreation or physical therapy before becoming ill, environmental cleaning of this area was performed. No new cases among residents were identified after November 18, 2017, and no facility staff members reported respiratory symptoms during this outbreak.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The use of molecular diagnostics and respiratory virus panels has become more common, enabling specific HCoVs to be more easily identified. In the United States, HCoV respiratory infections commonly occur in the fall and winter, and annual variations are noted in patterns of circulation of individual HCoVs (6). At the time of this outbreak, national surveillance data from the National Respiratory and Enteric Virus Surveillance System indicated that HCoV-NL63 was the predominant circulating HCoV type.",
            "cite_spans": [
                {
                    "start": 309,
                    "end": 310,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "This outbreak demonstrates that HCoV-NL63 can be associated with severe respiratory illness in LTCF residents. Clinicians and public health practitioners should consider HCoV-NL63 in patients with similar clinical presentations in these settings.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Characterization of human coronavirus OC43 and human coronavirus NL63 infections among hospitalized children <5 years of age.",
            "authors": [],
            "year": 2014,
            "venue": "Pediatr Infect Dis J",
            "volume": "33",
            "issn": "",
            "pages": "814-20",
            "other_ids": {
                "DOI": [
                    "10.1097/INF.0000000000000292"
                ]
            }
        },
        "BIBREF1": {
            "title": "Clinical impact of human coronaviruses 229E and OC43 infection in diverse adult populations.",
            "authors": [],
            "year": 2013,
            "venue": "J Infect Dis",
            "volume": "208",
            "issn": "",
            "pages": "1634-42",
            "other_ids": {
                "DOI": [
                    "10.1093/infdis/jit393"
                ]
            }
        },
        "BIBREF2": {
            "title": "Viral infections in immunocompromised patients.",
            "authors": [],
            "year": 2011,
            "venue": "Biol Blood Marrow Transplant",
            "volume": "17",
            "issn": "Suppl",
            "pages": "S2-5",
            "other_ids": {
                "DOI": [
                    "10.1016/j.bbmt.2010.11.008"
                ]
            }
        },
        "BIBREF3": {
            "title": "Long-term care facilities: a cornucopia of viral pathogens.",
            "authors": [],
            "year": 2008,
            "venue": "J Am Geriatr Soc",
            "volume": "56",
            "issn": "",
            "pages": "1281-5",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1532-5415.2008.01775.x"
                ]
            }
        },
        "BIBREF4": {
            "title": "Human coronavirus OC43 causes influenza-like illness in residents and staff of aged-care facilities in Melbourne, Australia.",
            "authors": [],
            "year": 2005,
            "venue": "Epidemiol Infect",
            "volume": "133",
            "issn": "",
            "pages": "273-7",
            "other_ids": {
                "DOI": [
                    "10.1017/S0950268804003346"
                ]
            }
        },
        "BIBREF5": {
            "title": "Human coronavirus circulation in the United States 2014-2017.",
            "authors": [],
            "year": 2018,
            "venue": "J Clin Virol",
            "volume": "101",
            "issn": "",
            "pages": "52-6",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcv.2018.01.019"
                ]
            }
        }
    }
}