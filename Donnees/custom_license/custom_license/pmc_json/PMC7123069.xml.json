{
    "paper_id": "PMC7123069",
    "metadata": {
        "title": "Generation of MERS-CoV Pseudotyped Viral Particles for the Evaluation of Neutralizing Antibodies in Mammalian Sera",
        "authors": [
            {
                "first": "Rahul",
                "middle": [],
                "last": "Vijay",
                "suffix": "",
                "email": "rahul-vijay@uiowa.edu",
                "affiliation": {}
            },
            {
                "first": "Abdulrahman",
                "middle": [],
                "last": "Almasaud",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Naif",
                "middle": [
                    "Khalaf"
                ],
                "last": "Alharbi",
                "suffix": "",
                "email": "harbina2@ngha.med.sa",
                "affiliation": {}
            },
            {
                "first": "Anwar",
                "middle": [
                    "M."
                ],
                "last": "Hashem",
                "suffix": "",
                "email": "amhashem@kau.edu.sa",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Pseudotype viruses (also known as pseudoviruses, pseudoparticles, or pseudotype viral particles) were discovered in 1911 [1]. They are chimeric viral particles expressing recombinant glycoproteins from one virus on the surface of another replication-deficient virus (viral vector), generating a single round chimeric viral particles. Pseudotype viral particles have been developed for many viruses especially those requiring high containment facilities such as SARS-CoV, MERS-CoV, Ebola, and highly pathogenic influenza A viruses without the need to handle wild-type viruses [2]. Pseudotype-based assays allow for accurate, specific, and sensitive detection of neutralizing antibodies (nAbs) and screening for viral entry inhibitors.",
            "cite_spans": [
                {
                    "start": 122,
                    "end": 123,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 576,
                    "end": 577,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The most efficient and common viral vectors for the production of pseudotype viral particles are lentiviruses. Lentiviral vectors are retroviruses, which are enveloped single-stranded RNA viruses, derived, for example, from human immunodeficiency virus type 1 (HIV-1). They have been used to develop pseudotype viral particles for many pathogenic viruses [3\u20136]. These replication-deficient vectors offer a number of advantages including that they do not replicate in mammalian cells, they infect dividing and nondividing cells, they can incorporate large transgenes derived from other pathogenic viruses as large as 9 kb, and they induce no or weak immune response [7\u20139]. Several studies have utilized lentiviral vectors to generate MERS-CoV pseudotype viral particles ( MERSpp) to evaluate nAbs in humans and animals. Here, we describe detailed protocol for the generation and utilization of MERSpp. This protocol is based on lentiviral system and use of luciferase enzyme as the main readout reporter of the system.",
            "cite_spans": [
                {
                    "start": 356,
                    "end": 357,
                    "mention": "3",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 358,
                    "end": 359,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 666,
                    "end": 667,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 668,
                    "end": 669,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "\nBiosafety cabinet.Inverted microscope.Low-speed centrifuge.37 \u00b0C incubator with 5% CO2.37 \u00b0C water bath.Sterile tissue culture 75 cm2 flasks.Sterile serological pipettes.Sterile 15 mL falcon tubes.Pipettes.Multichannel pipette.Sterile disposable aerosol-resistant filtered tips.Complete cell growth media: Filter-sterilized Dulbecco\u2019s modification of Eagle medium (DMEM) supplemented with 10% heat-inactivated fetal bovine serum (FBS), 1% l-glutamine, and 1% penicillin/streptomycin.70% Ethanol for decontamination of laminar flow biosafety cabinet and objects brought into the hood.Sterile DPBS without calcium or magnesium.Sterile 1\u00d7 trypsin-EDTA in DPBS without calcium or magnesium.\n",
            "cite_spans": [],
            "section": "General Materials ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nHEK 293T cells.Envelope DNA plasmid: Glycoprotein expression plasmid: pCAGGS-MERS-CoV spike (see\nNote 1).Lentiviral plasmid expressing firefly luciferase: pCSFLW [9] (see\nNote 1).Lentiviral packaging plasmid: second-generation packaging vector expressing HIV-Gag-Pol: p8.91 [10] (see\nNote 1).Branched polyethylenimine solution (PEI) (1 mg/mL) (see\nNote 2).Opti-MEM reduced serum media.1 M HEPES.Sterile 1.5 mL microcentrifuge tubes (two tubes per transfection).Sterile 0.22 \u03bcm, \u03b3-irradiated syringe filters.Sterile 0.45 \u03bcm \u03b3-irradiated syringe filters.Sterile 10 mL syringes.\n",
            "cite_spans": [
                {
                    "start": 164,
                    "end": 165,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 276,
                    "end": 278,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Making MERSpp ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\n96-Well white opaque culture plate: Sterile luminomer IsoPlate-96 B & W tissue culture plate with lid.Supernatant containing MERSpp pseudotype virus, VSV-G or \u0394Env pseudotype viruses.Confluent Huh7 cells in a tissue culture 75 cm2 flask, cultured in complete cell growth media (preferentially subcultured at 1:4 ratio 48 h before use).Cell Counter or hemocytometer.Bright Glo\u2122 kits (Promega).Luminometer.\n",
            "cite_spans": [],
            "section": "Titration Step ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\n96-Well white opaque culture plate: Sterile luminomer IsoPlate-96 B & W tissue culture plate with lid.Supernatant containing titrated pseudotyped viruses.Mammalian serum samples to be tested.5 mL Huh7 cells suspension at 2 \u00d7 105cells/mL in complete cell growth media.Bright Glo\u2122 kits (Promega).Luminometer.\n",
            "cite_spans": [],
            "section": "Neutralization Step ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nPlate 293T cells 24 h before transfection in a 75 cm2 tissue culture flask and incubate the flask in 37 \u00b0C incubator with 5% CO2 to be 70% confluent at the time of transfection (next day) (see\nNote 3).On the day of transfection, prepare and label two sterile 1.5 mL microcentrifuge tubes (tube 1 and tube 2) per transfection.Add 200 \u03bcL pre-warmed Opti-MEM to tube 1.Add the DNA plasmids to tube 1 at the ratio 0.9:1:1.5 (pCAGGS-MERS-CoV spike:p8.91:pCSFLW) (see\nNote 4).Add 200 \u03bcL Opti-MEM and 35 \u03bcL of 1 mg/mL PEI to tube 2 (see\nNote 5).Mix both tubes by gentle mixing and incubate for 5 min at room temperature.Transfer the content of tube 2 into tube 1 and incubate the tube at room temperature for 20 min. Mix the tube by gentle rocking every 3\u20134 min during incubation period.During incubation, remove the media from the 293 T cell flask and add 7 mL of fresh pre-warmed complete cell growth media (see\nNote 6).After the 20 min incubation, pipette the mixture from the tube onto 293T cells dropwise over the complete area of the flask. Swirl the flask gently to ensure even dispersal.Incubate the flask at 37 \u00b0C, 5% CO2 for overnight for 12\u201316 h.After incubation, change the media by removing the old media and adding 7 mL of fresh pre-warmed complete cell growth media (see\nNote 6).Incubate the flask at 37 \u00b0C, 5% CO2 for additional 32\u201336 h.Collect the supernatant, which contains the viral pseudotype particles, using sterile 10 mL sterile syringe.Filter the collected supernatant through a Sterile 0.45 \u03bcm filter into a sterile 15 mL tube.Store the filtered supernatant at \u221280 \u00b0C (see\nNote 7).\n",
            "cite_spans": [],
            "section": "Generation of Pseudotype Viruses ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nIn a 96-well white opaque culture plate, add 50 \u03bcL of pre-warmed complete cell growth media to all wells in column 12 \u201ccell only control\u201d (CC) as a negative control.Add 50 \u03bcL of pre-warmed complete cell growth media to all wells in rows B to H, columns 1 to 11 (Fig. 1).Add 100 \u03bcL of supernatant containing MERSpp, VSV-G, or \u0394Env pseudotype viruses to wells of row A as shown in Fig. 1 excluding CC column (i.e., column 12) (see\nNote 8).Remove 50 \u03bcL from virus-containing wells in row A (A1-A11) and perform 1:2 serial dilutions downward to all wells below (Fig. 1).During each dilution step mix well by pipetting eight times up and down.Continue the dilution until row H and discard the final 50 \u03bcL from the last wells in row H (Fig. 1) (see\nNote 9).Harvest Huh7 cells from the 75 cm2 tissue culture flask using standard trypsinization procedure (see\nNote 10).Count the cells and prepare a 5 mL cell suspension at 2 \u00d7 105cells/mL in pre-warmed complete cell growth media (i.e., every 50 \u03bcL should contain 1 \u00d7 104cells in total).Add 50 \u03bcL of the Huh7 cell suspension to all well in the 96-well white opaque culture plate (see\nNote 11).Centrifuge the 96-well opaque culture plate for 1 min at 500 \u00d7 g to settle down any droplets on the inner sides of the wells.Incubate the plate for 48 h at 37 \u00b0C, 5% CO2 (see\nNote 12).In 15 mL falcon tube, prepare 1:1 Bright Glo\u2122 luciferase substrate by adding equal amount of the substrate and pre-warmed complete cell growth media (for one 96-well plate: add 2.5 mL of substrate and 2.5 mL pre-warmed complete cell growth media to make 5 mL total volume of prepared substrate).After incubation, pipette out and discard supernatant from all wells and add 50 \u03bcL of the prepared substrate into each well of the 96-well plate (see\nNotes 13 and 14).Wait 5 min and then read the plate using a luminometer and save the results. Figure 2 shows example readout of a titration plate (see\nNote 15).\n\n\n",
            "cite_spans": [],
            "section": "MERSpp Titration ::: Methods",
            "ref_spans": [
                {
                    "start": 268,
                    "end": 269,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 385,
                    "end": 386,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 564,
                    "end": 565,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 736,
                    "end": 737,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1866,
                    "end": 1867,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "\nAdd 100 \u03bcL of pre-warmed complete cell growth media to all wells in column 12 \u201ccell only control\u201d (CC) of the 96-well opaque white plate (Fig. 3).Add 50 \u03bcL of pre-warmed complete cell growth media to all wells in column 11 \u201ccells + virus control\u201d (VC) (Fig. 3).Add 50 \u03bcL of pre-warmed complete cell growth media to all wells in rows B to H in columns 1 to 10 (Fig. 3).Add 95 \u03bcL of pre-warmed complete cell growth media in wells in row A (A1\u2013A10) (Fig. 3).Add 5 \u03bcL of serum samples in duplicate (two wells per sample) in wells in row A (A1\u2013A10) (Fig. 3) to have 1:20 dilution (see\nNote 16).Remove 25 \u03bcL from wells in row A (A1\u2013A10) and perform 1:3 serial dilutions downward to all wells below (Fig. 3).During each dilution step mix well by pipetting five times up and down.Continue the dilution until row H; discard the final 25 \u03bcL from the last wells in row H (Fig. 3).Also discard 25 \u03bcL from wells in row A (A1\u2013A10).Based on MERSpp titration, prepare a MERSpp suspension with a concentration of 200,000 RLU per 50 \u03bcL, a total of 5 mL are needed for one 96-well plate (see\nNote 15).Add 50 \u03bcL of MERSpp suspension into each well in the plate except column 12 (CC).Incubate the plate for 1 h at 37 \u00b0C, 5% CO2.After incubation, add 50 \u03bcL of the Huh7 cell suspension to all wells (1 \u00d7 104cells in total). So, each well in the plate will have 150 \u03bcL total volume.Incubate the plate for 48 h at 37 \u00b0C, 5% CO2 (see\nNote 12).After incubation, discard the supernatant from all wells and measure luciferase activity on a luminometer as indicated in the titration step and save the results (see\nNote 13). Figure 4 shows an example of the levels of background neutralization activity from different species.\n\n\n",
            "cite_spans": [],
            "section": "MERSpp Neutralization (MN) Assay ::: Methods",
            "ref_spans": [
                {
                    "start": 144,
                    "end": 145,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 259,
                    "end": 260,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 366,
                    "end": 367,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 453,
                    "end": 454,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 551,
                    "end": 552,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 699,
                    "end": 700,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 867,
                    "end": 868,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 1602,
                    "end": 1603,
                    "mention": "4",
                    "ref_id": "FIGREF3"
                }
            ]
        },
        {
            "text": "\nOther mammalian vectors such as pcDNA3.1 could be used. Use codon-optimized transgene for mammalian cell expression. All plasmids need to be transformed into DH5\u03b1 cells or similar cells using ampicillin as a selection antibiotic. Plasmids can be purified using routine protocols.To prepare branched polyethylenimine (PEI) at 1 mg/mL, dissolve PEI in endotoxin-free water (pre-warmed to 80 \u00b0C). Let it cool down at room temperature, and then neutralize the pH (pH 7.0) using 1 M HEPES buffer to a final concentration of 15 mM. Sterilize the solution by filtration using Sterile 0.22 \u03bcm filters. Filtration is important not only for sterility but also to remove undissolved PEI, which could reduce the transfection efficiency. Store aliquots of 0.2\u20131 mL at \u221280 \u00b0C for long-term storage. Thawed and working solutions could be stored at 4 \u00b0C for up to 2 months.Alternatively, 100 mm petri dishes could be used. Cells could be cultured without antibiotics to reduce toxicity and cell death.A plasmid encoding vesicular stomatitis virus G protein (VSV-G-pcDNA3.1) or an empty pcDNA3.1 vector could be used instead of pCAGGS-MERS-CoV spike to generate VSV-G pseudovirus or pseudovirus without Env (\u0394Env) as controls.Alternatively, Lipofectamine 2000 could be used. To tube 2, add 15 \u03bcL of Lipofectamine 2000 into 200 \u03bcL pre-warmed Opti-MEM. It is recommended to use a range of 0.5\u20135 \u03bcL of Lipofectamine 2000 per \u03bcg of DNA.Make sure to add media slowly to one side of the flask to avoid detaching adherent cells. Avoid cell drying by adding the media right after removing the old media.It is recommended to aliquot the collected supernatant to avoid multiple freezing and thawing. Collected supernatant could be stored at 4 \u00b0C for up to one week without loss in MERSpp titer.Use of VSV-G or \u0394Env pseudotype viruses is optional as controls.After completing the serial dilutions, the final volume per well should be 50 \u03bcL of mixed cell growth media and supernatant containing viral pseudotype particles.To harvest or maintain Huh7 cells, remove media from the flask, wash the cell monolayer gently with 3\u20135 mL of sterile pre-warmed DPBS without calcium or magnesium, and discard the used washing solution. Add 3 mL pre-warmed 1\u00d7 trypsin-EDTA in DPBS without calcium or magnesium to the cell monolayer and incubate for 5 min at 37 \u00b0C, 5% CO2 to detach cells (incubation may vary, so check the cells every 2\u20133 min). After cells are detached, add 3 mL pre-warmed complete cell growth media to the flask to quench trypsin activity, and collect detached cells in 15 mL sterile falcon tube. Make sure to centrifuge the collected cells and discard the supernatant. Then, add new 6 mL pre-warmed complete cell growth media and re-suspend the cells by pipetting up and down to make a homogenous cell suspension.It is recommended to seed a few wells of a regular 96-well tissue culture plate with 50 \u03bcL of the Huh7 cell suspension and 50 \u03bcL pre-warmed complete cell growth media to be able to check cells under the microscope to get a sense of how confluent and viable the cells are.Ensure the incubator is H2O saturated to avoid cell drying.Make sure to avoid detaching and removing adherent cells when aspirating off medium from wells. Therefore, it is recommended to only remove 90 \u03bcL and 140 \u03bcL from each well in the titration and the neutralization plates, respectively.Alternatively, aspirate off medium and add 30 \u03bcL of 1\u00d7 passive lysis buffer (25 mM Tris-phosphate (pH 7.8), 2 mM DTT, 2 mM 1,2-diaminocyclohexaneN,N,N\u2032,N\u2032-tetraacetic acid, 10% glycerol, 1% Triton X-100) to each well. Then, freeze and thaw the cells once on dry ice. After thawing, leave the plate at room temperature for 30 min before reading luciferase activity on Luminometer supplied with two injectors for both luciferase buffer (15 mM MgSO4, 15 mM KPO4 (pH 7.8), 1 mM ATP, 1 mM DTT in d H2O; 20 mL is needed per plate to add 100 \u03bcL per well) and luciferase substrates (d-luciferin potassium salt dissolve in H2O at 0.3 mg/mL, 10 mL is needed per plate to add 50 \u03bcL per well). Measure light produced from the reaction ~8 s after adding the substrate using an integration time of 5\u201330 s.Alternatively, the HIV-1 p24 content in the generated pseudotyped viruses could be quantified by HIV-1 p24CA capture ELISA kit and pseudotyped viruses equivalent to ~500 \u03b7g of p24 could be used in neutralization assay.Use heat-inactivated serum samples at 56 \u00b0C for 30 min.\n",
            "cite_spans": [],
            "section": "Notes",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 1: Plate layout for MERSpp titration. The sequential steps of plate preparation are indicated by numbered boxes. The color intensity indicates the expected values of luciferase readout",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig. 2: An example readout of MERSpp titration plate",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Fig. 3: Plate preparation for MERSpp neutralization assay. The preparation steps for MERSpp neutralization assay plate are indicated by numbered boxes. The color intensity indicates the expected values of luciferase readout",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Fig. 4: MERSpp neutralization assay for na\u00efve serum samples from different species. Serum samples from different species, including humans, camels, rats, and mice, were tested in a MERSpp NA with two different concentrations of pseudotyped viruses (200,000 and 550,000 RLU per well). All samples were negative for Anti-MERS-CoV antibodies by standard commercial ELISA before conducting the MERSpp NA. The results showed levels of background neutralization that varied between species. This background neutralization is expected based on our previous observation (unpublished data)",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "A transmissible avian neoplasm. (Sarcoma of the common fowl)",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Rous",
                    "suffix": ""
                }
            ],
            "year": 1910,
            "venue": "J Exp Med",
            "volume": "12",
            "issn": "",
            "pages": "696-705",
            "other_ids": {
                "DOI": [
                    "10.1084/jem.12.5.696"
                ]
            }
        },
        "BIBREF1": {
            "title": "The mitochondrial genome of Arabidopsis thaliana contains 57 genes in 366,924 nucleotides",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Unseld",
                    "suffix": ""
                },
                {
                    "first": "JR",
                    "middle": [],
                    "last": "Marienfeld",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Brandt",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Nat Genet",
            "volume": "15",
            "issn": "",
            "pages": "57-61",
            "other_ids": {
                "DOI": [
                    "10.1038/ng0197-57"
                ]
            }
        },
        "BIBREF2": {
            "title": "Pseudotype-based neutralization assays for influenza: a systematic analysis",
            "authors": [
                {
                    "first": "GW",
                    "middle": [],
                    "last": "Carnell",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Ferrara",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Grehan",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Front Immunol",
            "volume": "6",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.3389/fimmu.2015.00161"
                ]
            }
        },
        "BIBREF3": {
            "title": "Chimeric influenza haemagglutinins: generation and use in pseudotype neutralization assays",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Ferrara",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Temperton",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Methods",
            "volume": "4",
            "issn": "",
            "pages": "11-24",
            "other_ids": {
                "DOI": [
                    "10.1016/j.mex.2016.12.001"
                ]
            }
        },
        "BIBREF4": {
            "title": "Novel functional hepatitis C virus glycoprotein isolates identified using an optimized viral pseudotype entry assay",
            "authors": [
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Urbanowicz",
                    "suffix": ""
                },
                {
                    "first": "CP",
                    "middle": [],
                    "last": "McClure",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "King",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "J Gen Virol",
            "volume": "97",
            "issn": "",
            "pages": "2265-2279",
            "other_ids": {
                "DOI": [
                    "10.1099/jgv.0.000537"
                ]
            }
        },
        "BIBREF5": {
            "title": "Pseudoparticle neutralization assay for detecting Ebola-neutralizing antibodies in biosafety level 2 settings",
            "authors": [
                {
                    "first": "AWH",
                    "middle": [],
                    "last": "Chin",
                    "suffix": ""
                },
                {
                    "first": "RAPM",
                    "middle": [],
                    "last": "Perera",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Clin Chem",
            "volume": "61",
            "issn": "",
            "pages": "885-886",
            "other_ids": {
                "DOI": [
                    "10.1373/clinchem.2015.238204"
                ]
            }
        },
        "BIBREF6": {
            "title": "An optimised method for the production of MERS-CoV spike expressing viral pseudotypes",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Grehan",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Ferrara",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Temperton",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Methods",
            "volume": "2",
            "issn": "",
            "pages": "379-384",
            "other_ids": {
                "DOI": [
                    "10.1016/j.mex.2015.09.003"
                ]
            }
        },
        "BIBREF7": {
            "title": "Altering the tropism of lentiviral vectors through pseudotyping",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Cronin",
                    "suffix": ""
                },
                {
                    "first": "X-Y",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Reiser",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Curr Gene Ther",
            "volume": "5",
            "issn": "",
            "pages": "387-398",
            "other_ids": {
                "DOI": [
                    "10.2174/1566523054546224"
                ]
            }
        },
        "BIBREF8": {
            "title": "Pseudotyping of murine leukemia virus with the envelope glycoproteins of HIV generates a retroviral vector with specificity of infection for CD4-expressing cells",
            "authors": [
                {
                    "first": "BS",
                    "middle": [],
                    "last": "Schnierle",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Stitz",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Bosch",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "94",
            "issn": "",
            "pages": "8640-8645",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.94.16.8640"
                ]
            }
        },
        "BIBREF9": {
            "title": "Self-inactivating lentivirus vector for safe and efficient in vivo gene delivery",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Zufferey",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Dull",
                    "suffix": ""
                },
                {
                    "first": "RJ",
                    "middle": [],
                    "last": "Mandel",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "J Virol",
            "volume": "72",
            "issn": "",
            "pages": "9873-9880",
            "other_ids": {
                "DOI": []
            }
        }
    }
}