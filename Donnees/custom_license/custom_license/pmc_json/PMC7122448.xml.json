{
    "paper_id": "PMC7122448",
    "metadata": {
        "title": "Guidelines for Health Organizations: European Perspectives and Experience in Pandemics",
        "authors": [
            {
                "first": "Antonio",
                "middle": [
                    "M."
                ],
                "last": "Esquinas",
                "suffix": "",
                "email": "antmesquinas@gmail.com",
                "affiliation": {}
            },
            {
                "first": "Emilio",
                "middle": [],
                "last": "Curiel-Balsera",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Elena",
                "middle": [],
                "last": "Garc\u00eda-Trujillo",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In Europe, the rate of noninvasive ventilation (NIV) use in intensive care units (ICUs) is about 35 % for ventilated patients and higher (roughly 60 %) in respiratory ICUs or emergency departments. In North America, this form of ventilation is begun most often in emergency departments (EDs), most patients being transferred to the ICU or step-down units in hospitals with such facilities. This low rate of use in some hospitals is related to scarce knowledge on or experience with this technique, insufficient technical equipment, and inadequate funding. Despite these limitations, NIV is increasingly being used outside traditional and respiratory ICUs, including EDs, postsurgical recovery rooms, cardiology, neurology, and oncology wards, and palliative care units.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Approximately 10\u201330 % of hospitalized patients with H1N1 virus infection require admission to the ICU (where available). Critically ill patients include those suffering from rapidly progressive lower respiratory tract disease, respiratory failure, and acute respiratory distress syndrome (ARDS) with refractory hypoxemia [1].",
            "cite_spans": [
                {
                    "start": 322,
                    "end": 323,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "Before the severe acute respiratory syndrome (SARS) outbreak in 2003, there was no evidence to support the idea that the use of NIV might increase the risk of infectious disease transmission. Despite the paucity of epidemiological data, the idea that NIV leads to increased occupational risk has gained currency. In fact, some organizations such as the Canadian Diseases Advisory Committee have published recommendations to avoid NIV in patients with febrile respiratory illness [2]. Other studies show that NIV can be used effectively and safely in such situations by applying strict infection-control procedures [3\u20135].",
            "cite_spans": [
                {
                    "start": 480,
                    "end": 481,
                    "mention": "2",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 615,
                    "end": 616,
                    "mention": "3",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 617,
                    "end": 618,
                    "mention": "5",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "The European Society of Intensive Medicine and the European Respiratory Society guidelines recommend when NIV should be considered (or not) after reviewing studies following the last H1N1 pandemics in Europe [6]: NIV must not be considered in patients with severe hypoxemic acute respiratory failure (ARF), rapid development of ARDS, or multiorgan failure. Invasive ventilation is recommended for these patients. NIV may be considered to prevent further deterioration and intubation needs in patients with mild-to-moderate hypercapnic ARF due to cardiogenic pulmonary edema or exacerbation of a chronic respiratory disease secondary to H1N1 infection in the absence of pneumonia, multi-organ failure (MOF), or refractory hypoxemia. It can also be used to prevent postextubation respiratory failure in patients with resolving ARDS secondary to H1N1 infection, preferably when patients are no longer contaminated.",
            "cite_spans": [
                {
                    "start": 209,
                    "end": 210,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "There is growing concern about droplet dispersion during NIV, but it is important to note that similar exposures may occur during routine oxygen therapy by mask, coughing or sneezing, or procedures such as bronchoscopy and aerosol delivery.",
            "cite_spans": [],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "Recommendations for droplets include patient isolation with protective measures for health care providers and other patients, use of double-circuit tubes and special filters for nonrebreathing devices, minimization of leaks, preferably full-face mask or helmet interfaces, avoidance of heated humidifiers, and disposing of mask and tubes after use according to routine infection control procedures [7].",
            "cite_spans": [
                {
                    "start": 399,
                    "end": 400,
                    "mention": "7",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "The Spanish Society of Intensive Care Medicine, after collecting data from its hospital network, developed a document with recommendations for the management of severe complications in the H1N1 flu pandemic [8]. The document states that:\n\u2026noninvasive mechanical ventilation cannot be considered a technique of choice in patients with acute respiratory distress syndrome, but could be useful in experienced centers and in cases of respiratory failure associated with exacerbation of chronic obstructive pulmonary disease or heart failure. It can be used in highly experienced centers, with appropriate helmet-type interfaces and patients who have reported very good results, although only in a few cases.\n",
            "cite_spans": [
                {
                    "start": 208,
                    "end": 209,
                    "mention": "8",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "The use of NIV and its risks have been discussed in many documents. In 2009, the Scottish government published a guide on NIV in pandemic flu patients. The recommendations on NIV are somewhat complex, but they ultimately suggested that NIV could be used effectively and safely in such situations under strict infection-control procedures. These conclusions were reached in the United Kingdom are shown in Table 41.1 [9]. The recommended equipment and materials are shown in Table 41.2.\n\n",
            "cite_spans": [
                {
                    "start": 417,
                    "end": 418,
                    "mention": "9",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Analysis",
            "ref_spans": [
                {
                    "start": 411,
                    "end": 415,
                    "mention": "41.1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 480,
                    "end": 484,
                    "mention": "41.2",
                    "ref_id": "TABREF1"
                }
            ]
        },
        {
            "text": "In some circumstances, a continual leak of unfiltered gas from the exhalatory circuit may be anticipated, and consideration should be given to adopting a policy for the staff working close to the patient of wearing FFP3 respirators and eye protection for extended periods throughout a shift. Examples of leaks of unfiltered gas include: (1) situations where no bacterial/viral filters are available and therefore ventilator circuits have to be used unfiltered and (2) when high-frequency oscillatory ventilators are used.",
            "cite_spans": [],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "The World Health Organization recommends special considerations in NIV-treated patients, including additional precautions in EDs and ICUs [10].Noninvasive ventilation [bilevel positive airway pressure (BiPAP), continuous positive airway pressure (CPAP)]: standard and droplet precautions unless indicated otherwise by new evidence of increased transmission risk.Nebulization: standard and droplet precautions. Nebulizer treatment should be performed in an area that is physically separated from other patients (e.g., treatment room, screened enclosure).\n",
            "cite_spans": [
                {
                    "start": 139,
                    "end": 141,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "In relation to supportive therapies for hypoxemia treatment, oxygen support is recommended but with no distinction between invasive and noninvasive ventilation, except in lung-protective ventilation strategies [1].",
            "cite_spans": [
                {
                    "start": 211,
                    "end": 212,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "Regarding NIV in pandemics, Italian and French guidelines refer to the World Health Organization or the Centers for Disease Control and Prevention for its implementation, without further information.",
            "cite_spans": [],
            "section": "Analysis",
            "ref_spans": []
        },
        {
            "text": "\nNoninvasive mechanical ventilation cannot be considered a technique of choice in patients with ARDS but could be useful in experienced centers and in cases of respiratory failure associated with exacerbation of chronic obstructive pulmonary disease or heart failure.It is preferable to perform NIV using an appropriate helmet-type interface.Concerning droplet dispersion during NIV, similar exposures may occur during routine oxygen therapy by mask or procedures such as bronchoscopy and aerosol delivery.Water humidification should be avoided.All respiratory equipment used on patients, including transport ventilator circuits and manual resuscitation aids, should include a high-efficiency bacterial/viral breathing system filter (BS EN 13328-1).\n",
            "cite_spans": [],
            "section": "Key Major Recommendations ::: Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 41.1: Conclusions of guidance on infection control for critical care and NIV of Scottish Government and Health Protection Scotland\n",
            "type": "table"
        },
        "TABREF1": {
            "text": "Table 41.2: Recommended equipment and material for infection control in critical care patients\n",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Surge capacity and infrastructure considerations for mass critical care",
            "authors": [
                {
                    "first": "JL",
                    "middle": [],
                    "last": "Hick",
                    "suffix": ""
                },
                {
                    "first": "MD",
                    "middle": [],
                    "last": "Christian",
                    "suffix": ""
                },
                {
                    "first": "CL",
                    "middle": [],
                    "last": "Srpung",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Intensive Care Med",
            "volume": "36",
            "issn": "Suppl 1",
            "pages": "S11-20",
            "other_ids": {
                "DOI": [
                    "10.1007/s00134-010-1761-4"
                ]
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Effectiveness of noninvasive positive pressure ventilation in the treatment of acute respiratory failure in severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "TMT",
                    "middle": [],
                    "last": "Cheung",
                    "suffix": ""
                },
                {
                    "first": "LYC",
                    "middle": [],
                    "last": "Yam",
                    "suffix": ""
                },
                {
                    "first": "LKY",
                    "middle": [],
                    "last": "So",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Chest",
            "volume": "126",
            "issn": "",
            "pages": "845-50",
            "other_ids": {
                "DOI": [
                    "10.1378/chest.126.3.845"
                ]
            }
        },
        "BIBREF5": {
            "title": "Non invasive versus invasive mechanical ventilation for respiratory failure in severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "LYC",
                    "middle": [],
                    "last": "Yam",
                    "suffix": ""
                },
                {
                    "first": "AYF",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "TMT",
                    "middle": [],
                    "last": "Cheung",
                    "suffix": ""
                },
                {
                    "first": "ELH",
                    "middle": [],
                    "last": "Tsui",
                    "suffix": ""
                },
                {
                    "first": "JCK",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "VCW",
                    "middle": [],
                    "last": "Wong",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Chin Med J",
            "volume": "118",
            "issn": "",
            "pages": "1413-21",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "High success and low mortality rates with noninvasive ventilation in influenza A H1N1 patients in a tertiary hospital",
            "authors": [
                {
                    "first": "KT",
                    "middle": [],
                    "last": "Timenetsky",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Aquino",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Saghabi",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Taniguchi",
                    "suffix": ""
                },
                {
                    "first": "CV",
                    "middle": [],
                    "last": "Silvia",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Correa",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "BMC Res Notes",
            "volume": "4",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/1756-0500-4-375"
                ]
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Exhaled air dispersion distances during noninvasive ventilation via different respironics face masks",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Hui",
                    "suffix": ""
                },
                {
                    "first": "BK",
                    "middle": [],
                    "last": "Chow",
                    "suffix": ""
                },
                {
                    "first": "SS",
                    "middle": [],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "LCY",
                    "middle": [],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "SD",
                    "middle": [],
                    "last": "Hall",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Gin",
                    "suffix": ""
                },
                {
                    "first": "JJY",
                    "middle": [],
                    "last": "Sung",
                    "suffix": ""
                },
                {
                    "first": "MTV",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Chest",
            "volume": "136",
            "issn": "",
            "pages": "998-1005",
            "other_ids": {
                "DOI": [
                    "10.1378/chest.09-0434"
                ]
            }
        },
        "BIBREF9": {
            "title": "Recommendations of the Infectious Diseases Work Group (GTEI) of the Spanish. Society of Intensive and Critical Care Medicine and Coronary Units (SEMICYUC) and the Infections in Critically Ill Patients Study Group (GEIPC) of the Spanish Society of Infectious Diseases and Clinical Microbiology (SEIMC) for the diagnosis and treatment of influenza A/H1N1 in seriously ill adults admitted to the Intensive Care Unit",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Rodr\u00edguez",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "\u00c1lvarez-Rocha",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Sirvent",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Zaragoza",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Nieto",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Arenzana",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Med Intensiva",
            "volume": "36",
            "issn": "2",
            "pages": "103-37",
            "other_ids": {
                "DOI": [
                    "10.1016/j.medin.2011.11.020"
                ]
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}