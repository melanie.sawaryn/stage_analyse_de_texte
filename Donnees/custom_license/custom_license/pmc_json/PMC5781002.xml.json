{
    "paper_id": "PMC5781002",
    "metadata": {
        "title": "Community\u2010acquired respiratory viruses and co\u2010infection among patients of Ontario sentinel practices, April 2009 to February 2010",
        "authors": [
            {
                "first": "Adriana",
                "middle": [],
                "last": "Peci",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anne\u2010Luise",
                "middle": [],
                "last": "Winter",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jonathan",
                "middle": [
                    "B."
                ],
                "last": "Gubbay",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Danuta",
                "middle": [
                    "M."
                ],
                "last": "Skowronski",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Elizabeth",
                "middle": [
                    "I."
                ],
                "last": "Balogun",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Cedric",
                "middle": [],
                "last": "De Lima",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Natasha",
                "middle": [
                    "S."
                ],
                "last": "Crowcroft",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anu",
                "middle": [],
                "last": "Rebbapragada",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "A novel influenza virus, A(H1N1)pdm09 emerged in April 2009 and spread rapidly, primarily through human\u2010to\u2010human transmission. Several million people were infected globally.\n1\n An important feature of this virus was that it mostly affected younger people with 60% of patients under 18 years of age, suggesting possible pre\u2010existing immunity in the elderly due to previous exposure to antigenically related influenza strains.\n2\n, \n3\n\n",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "The assumption made in most pandemic plans before 2009 was that the pandemic virus would be the dominant circulating respiratory virus.\n4\n Few studies performed extensive respiratory testing beyond influenza during the pandemic, and fewer still focused on community cases. Casalegno et al. documented cocirculation and co\u2010infection of A(H1N1)pdm09 with rhinovirus during the pandemic.\n5\n Watanabe et al. found a wide range of etiologic agents were identified among respiratory samples that were influenza negative, highlighting the need to diagnose other viral organisms that can co\u2010circulate with influenza.\n6\n Louie et al. investigated samples from laboratory\u2010confirmed fatal A(H1N1)pdm09 cases during the pandemic and bacterial pathogens were identified in 22 of 77 samples.\n7\n\n",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "Prior to the 2009 pandemic, respiratory viral co\u2010infection was reported in 7\u201327% of respiratory samples submitted for viral diagnosis.\n8\n, \n9\n, \n10\n, \n11\n, \n12\n, \n13\n Higher proportions of influenza A, respiratory syncytial virus (RSV), parainfluenza viruses, and rhinovirus, compared with other circulating viruses have been detected in patients with co\u2010infections.\n14\n, \n15\n, \n16\n, \n17\n, \n18\n\n",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "Co\u2010infection has not been fully explored due to limitations of several studies. Some studies focused on younger age groups, hospitalized patients or deceased individuals, which does not represent the general population.\n8\n, \n9\n, \n11\n, \n14\n, \n18\n, \n19\n, \n20\n Others have utilized a small sample size or limited their focus to certain viral pathogens, underestimating the role of other viruses in co\u2010infection.\n9\n, \n15\n, \n16\n, \n20\n, \n21\n\n",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "This study enrolled community patients presenting with (ILI) to a community sentinel network, during the influenza pandemic A(H1N1)pdm09 in Ontario, Canada and documented the profile of respiratory viruses causing ILI symptoms. This study aimed to describe respiratory viruses including co\u2010infections and host\u2010associated attributes such as age, sex, and comorbidity.",
            "cite_spans": [],
            "section": "Background",
            "ref_spans": []
        },
        {
            "text": "Data were collected as part of a multiprovincial case\u2013control sentinel network study that has been described elsewhere.\n21\n The sentinel network included 117 sentinels across the province of Ontario (with a population of 13\u00b74 million) who volunteered to participate in the study. It was anticipated that each sentinel would submit an average of 1\u20132 samples per week from their clinical practice during the study period, April 21, 2009 to February 25, 2010. This period was chosen to span the full pandemic in Ontario. Eligible patients were Ontario residents, who presented to a sentinel\u2019s office with influenza\u2010like illness (ILI) within seven days of symptom onset; number and selection of eligible patients was at the sentinel\u2019s discretion. ILI was defined as acute onset of fever and cough and one or more of the following: sore throat, myalgia, arthralgia, headache or prostration. Standard information was collected including date of birth, sex, chronic conditions, symptom onset, and sample collection date. The main outcome was the number of respiratory viruses detected per sample. Samples were categorized as negative, single infection or co\u2010infection when no virus, one virus, or at least two viruses were detected, respectively. Age was determined as age at symptom onset and categorized as 0\u20134, 5\u201314, 15\u201329, 30\u201354, 55\u201364, and 65+years. Time to sample collection was calculated as the difference between sample collection and symptom onset dates and categorized as less than or equal to two days or 3\u20137 days. Chronic condition was defined as heart/lung/renal/metabolic/blood/immune conditions or conditions that compromise the management of respiratory secretions and increase risk of aspiration and categorized as yes/no. This study was approved by the University of Toronto\u2019s Ethics Board and all patients gave verbal consent to participate.",
            "cite_spans": [],
            "section": "Study population ::: Methods",
            "ref_spans": []
        },
        {
            "text": "A nasal or nasopharyngeal sample was collected from each patient using Starswab\u00ae Multitrans collection swab and transported at 4\u00b0C for testing at Public Health Ontario Laboratory (PHOL)\u2010Toronto; in this study, each sample represents one patient. Viral RNA was extracted directly from samples using NucliSENS\u00ae easyMAG\u00ae (BioM\u00e9rieux, Inc., Marcy l\u2019Etoile, France). Samples were tested for influenza A and influenza B by influenza real\u2010time reverse transcriptase (rRT)\u2010PCR and also for influenza A, influenza B, enterovirus/rhinovirus, RSV, parainfluenza, adenovirus, coronaviruses, and metapneumovirus by a commercial multiplex PCR method [Luminex Respiratory Viral Panel (Luminex Molecular Diagnostics, Toronto, ON, Canada) or Seeplex RV (Seegene USA, Rockville, MD, USA)]. In the event of discrepant results between the two methods, positive results for influenza A by either method were considered positive.",
            "cite_spans": [],
            "section": "Viral diagnosis ::: Methods",
            "ref_spans": []
        },
        {
            "text": "rRT\u2010PCR was used for subtyping of influenza A samples; all influenza A specimens were subtyped, but not all attempts were successful.",
            "cite_spans": [],
            "section": "Viral diagnosis ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Statistical analyses were performed using STATA software version 10.0 (StataCorp, College Station, TX, USA).",
            "cite_spans": [],
            "section": "Statistical analysis ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Descriptive analyses were conducted to derive the proportion of single, co\u2010infection and no infection as well as describe patient characteristics using Chi\u2010Square. Crude and adjusted multinomial logistic regression were employed to evaluate any association of single, co\u2010infection and no\u2010infection with patient characteristics including age, sex, chronic condition, and time to sample collection. Odds ratios (OR) with 95% confidence intervals (CI) were calculated.",
            "cite_spans": [],
            "section": "Statistical analysis ::: Methods",
            "ref_spans": []
        },
        {
            "text": "A total of 1018 respiratory samples from 1018 patients with influenza\u2010like illness were included in this study after excluding 102 (9\u00b71%) samples that did not meet study inclusion criteria (Figure 1). At least one respiratory virus was detected in 668 (65\u00b76%) of the samples. Of the 831 detected viruses, influenza A was the most frequent accounting for 452 (54\u00b74%) followed by enterovirus/rhinovirus 194 (23\u00b73%) and RSV 120 (14\u00b74%) (Table 1). Of 452 influenza A viruses, 408 (90\u00b73%) were A(H1N1)pdm09, two (0\u00b74%) were H3, and 42 (9\u00b73%) could not be subtyped presumably due to low viral load. Peaks in detection for influenza A occurred in June and October 2010, for enterovirus/rhinovirus in September 2010, and for RSV in October 2010 (Figure 2).",
            "cite_spans": [],
            "section": "Respiratory viruses detected ::: Results",
            "ref_spans": [
                {
                    "start": 190,
                    "end": 198,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 738,
                    "end": 746,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "A single virus was detected in 512 (50\u00b73%) samples. Of these, 304 (59\u00b74%) were influenza A and 208 (40\u00b76%) were other respiratory viruses, the most common being enterovirus/rhinovirus, detected in 149 (29\u00b71%) samples (Table 2). Peaks for single infection occurred in June and September 2009, which were mainly due to the increase in influenza A and enterovirus/rhinovirus, respectively (Figure 3).",
            "cite_spans": [],
            "section": "Respiratory viruses detected ::: Results",
            "ref_spans": [
                {
                    "start": 387,
                    "end": 395,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "Viral co\u2010infection was detected in 156 (15\u00b73%) of the samples of which 149 (95\u00b75%) were dual infections and seven (4\u00b75%) triple infections. One hundred and forty\u2010eight (94\u00b79%) of the co\u2010infections were combination of A(H1N1)pdm09 and another respiratory virus and eight (5\u00b71%) were non\u2010influenza combinations. The most common co\u2010infections were influenza A/RSV B and influenza A/enterovirus/rhinovirus, responsible for 64\u00b71% and 21\u00b78% of co\u2010infections, respectively (Table 3).The highest proportion of co\u2010infection was detected in October, corresponding with peak activity of influenza A and RSV (Figure 3).",
            "cite_spans": [],
            "section": "Respiratory viruses detected ::: Results",
            "ref_spans": [
                {
                    "start": 597,
                    "end": 605,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "The median age of patients in the study was 23 years with a range of 3 months\u201396 years of age (Table 4). The highest proportion of single and co\u2010infections was observed in children 5\u201314 and 0\u20134 years of age, respectively. The proportion of those with no infection detected steadily increased with age, peaking at the elderly, aged 65 years and over (Figure 4). Females were overrepresented, comprising 599 (58\u00b78%) of the patients included in this study. Patients with no\u2010infection, single infection, and co\u2010infection did not differ with regards to sex. Two hundred and nineteen (21\u00b76%) patients had a chronic condition. Of these, 37\u00b74% had no virus detected, 44\u00b78% had single infections, and 17\u00b78% had co\u2010infections, whereas among the 795 participants without comorbidities, the distribution was 33\u00b76%, 51\u00b78%, and 14\u00b76%, respectively; however, that was not statistically significant (Table 4). The median number of days from symptom onset to sample collection was two with a range of 0\u20137 days. Five hundred and eighty\u2010two (57\u00b72%) and 436 (42\u00b78%) samples were collected within 2 days and 3\u20137 days, respectively. Of the 582 samples collected within 2 days of onset, 32\u00b79% had no virus detected, 48\u00b75% had single infections, and 18\u00b76% had co\u2010infections, whereas among those collected within 3\u20137 days, the distribution was 36\u00b73%, 52\u00b78%, and 11%, which was statistically significant.",
            "cite_spans": [],
            "section": "Patient characteristics ::: Results",
            "ref_spans": [
                {
                    "start": 350,
                    "end": 358,
                    "mention": "Figure 4",
                    "ref_id": "FIGREF3"
                }
            ]
        },
        {
            "text": "In crude and adjusted multinomial logistic regression, patients with single and co\u2010infections were compared to those with no infection. Compared to the elderly, patients under 65 years of age were more likely to have a single infection; the highest likelihood was observed in children 5\u201314 years of age (Table 5). Patients under 30 years of age were more likely to have co\u2010infections compared with patients 65 and over; this was most evident in the 0\u20134 age group. Presence of a chronic condition did not increase the likelihood of single infection but increased the likelihood of co\u2010infection; this did not achieve significance. Co\u2010infection was more likely detected in patients who had samples collected within 2 days as compared to 3\u20137 days; this did not apply for those with single infections. There was no sex difference.",
            "cite_spans": [],
            "section": "Patient characteristics ::: Results",
            "ref_spans": []
        },
        {
            "text": "In this study, 66% of samples tested during the 2009 pandemic in Ontario had at least one virus detected and 15% had co\u2010infections. These findings are consistent with reports from other studies with the range of co\u2010infection reported from 7\u201327%.\n8\n, \n9\n, \n10\n, \n11\n, \n12\n, \n13\n However, positivity and co\u2010infection rates vary widely between studies. There are various reasons for this finding: firstly, detection methods differ notably between studies, which impacts sensitivity, specificity and other technical parameters; secondly, viruses targeted differ from one study to another as does the study population.\n21\n\n",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "This study was conducted during the influenza pandemic A(H1N1)pdm09 which was associated with an increased number of samples submitted and high detection of A(H1N1)pdm09. Not surprisingly, influenza was the primary virus detected in June and October 2010; however, other viruses dominated for other months during both influenza pandemic waves (Figure 2). This demonstrates the importance of monitoring circulating respiratory viruses when advising clinicians to prescribe antivirals empirically during a pandemic.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 344,
                    "end": 352,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "Despite the higher prevalence of enterovirus/rhinovirus (23\u00b74%) than RSV (14\u00b75%), co\u2010infection with A(H1N1)pdm09/RSV was more common than A(H1N1)pdm09/enterovirus/rhinovirus, accounting for 64\u00b71% and 22\u00b78% of the co\u2010infections, respectively. This may reflect the younger age of patients infected by A(H1N1)pdm09, who were therefore also at greater risk of RSV. In addition, RSV cocirculated with A(H1N1)pdm09 more than enterovirus/rhinovirus, which peaked before the second wave (Figure 2). There may also be preferential interactions among certain pathogens; viral interactions were not assessed in this study.\n23\n When other respiratory samples positive for enterovirus/rhinovirus were further evaluated at our laboratory, they all were confirmed as rhinovirus, not enterovirus.\n24\n\n",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 480,
                    "end": 488,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "Single infection was more commonly detected in those less than 65 years of age. It is known that respiratory infections are more common in children for several reasons, including an immature immune system, lack of pre\u2010existing immunity particularly to new emerging viruses, and greater viral exposure opportunities.\n11\n, \n18\n, \n25\n Younger patients shed higher levels of virus when infected and also may be brought for medical care earlier than older patients, facilitating detection in these groups.\n26\n In addition, lower detection of single and co\u2010infection in elderly may be explained by pre\u2010existing immunity to A(H1N1)pdm09 and other respiratory viruses.\n2\n Co\u2010infection was more common in persons less than 30 years of age as compared to older adults. These data are congruent with findings from a previously published study where co\u2010infection was more likely in younger than older adults.\n18\n The combined effect of predominance of A(H1N1)pdm09 and the greater likelihood of infection with other respiratory viruses among younger ages likely explains our age\u2010related findings of co\u2010infection during the pandemic, which may not be generalizable to a typical influenza season.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The presence of comorbidities did not increase the likelihood of having a single infection but increased the likelihood of co\u2010infections; this did not achieve statistical significance. Patients with chronic conditions are at higher risk of severe disease and consequently may be more likely to seek medical care.\n26\n Selection bias is unlikely to influence these results as the proportion of patients with comorbidities was similar (21\u00b75%) to that in Ontario\u2019s population (20\u00b73%).\n27\n\n",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Sample collection within 2 days of symptom onset was found to independently increase the likelihood of detecting a viral co\u2010infection but not single infection. Long et al. reported an inverse relationship between duration of symptoms and viral detection rate due to greater viral shedding earlier in the disease process.\n11\n\n",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "This study was designed to examine circulating viruses and co\u2010infection. The presence of more than two viruses in the same sample may not always indicate clinical infection. As viruses may be detected in asymptomatic patients, it is impossible to determine which viruses caused symptoms. Previous studies suggest co\u2010infection may manifest higher disease severity, which may shorten the time to medical care and viral detection; disease severity was not assessed in the current study.\n19\n, \n28\n As viral\u2013bacterial co\u2010infections also occurred during the pandemic, it will be interesting for further studies to investigate their characteristics and impact on disease severity.\n28\n, \n29\n\n",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In summary, A(H1N1)pdm09 was frequently detected among community patients with ILI. However, other respiratory viruses cocirculated with A(H1N1)pdm09 during the pandemic, reinforcing the need to test for other viral agents even during a pandemic to appropriately guide clinical treatment decisions. Viral diagnosis, primarily A(H1N1)pdm09, was made more often in patients less than 65 years of age. Viral co\u2010infection was commonly detected in this study and was most likely detected in individuals less than 30 years of age. Earlier sample collection improves the detection of viral co\u2010infections. Understanding the contribution of other circulating respiratory pathogens during a pandemic may lead to improved individual diagnosis and recommendations for community\u2010based clinicians, and more effective prevention and treatment of respiratory infections, including use of influenza antivirals.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "DMS was Principal Investigator on a clinical trial of pediatric influenza vaccine dose response for which influenza vaccine was provided free by Sanofi Pasteur. JBG has received research grants from GlaxoSmithKline Inc. and Hoffman\u2010La Roche Ltd to study antiviral resistance in influenza.",
            "cite_spans": [],
            "section": "Conflict of interest",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1: Frequency of detected respiratory viruses among 1018 respiratory samples tested, April 21, 2009 to February 25, 2010, sentinel network, Ontario, Canada\n",
            "type": "table"
        },
        "TABREF1": {
            "text": "Table 2: Number and percent of viruses detected as single infections, April 21, 2009 to February 25, 2010, sentinel network, Ontario, Canada\n",
            "type": "table"
        },
        "TABREF2": {
            "text": "Table 3: Number and percent of viruses detected in samples where co\u2010infection was identified, April 21, 2009\u2010February 25, 2010 sentinel network, Ontario, Canada\n",
            "type": "table"
        },
        "TABREF3": {
            "text": "Table 4: Characteristics of patients and timing of specimen collection and testing, April 21, 2009 to February 25, 2010, sentinel network, Ontario, Canada\n",
            "type": "table"
        },
        "TABREF4": {
            "text": "Table 5: Crude and adjusted odds ratio of patient characteristics by infection status using no infection as the reference (n = 1014)*\n\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Figure 1:  Study inclusion criteria, April 21, 2009 to February 25, 2010, sentinel network, Ontario, Canada.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2:  Number of detection of the most common respiratory viruses by month of symptom onset, April 21, 2009 to February 25, 2010 sentinel network, Ontario, Canada.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3:  Frequency of single infection and co\u2010infections by month of symptom onset, April 21, 2009 to February 25, 2010, sentinel network, Ontario, Canada.",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Figure 4:  Age distribution of patients by virus detection status, April 21, 2009 to February 25, 2010, sentinel network, Ontario, Canada.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Novel swine\u2010origin influenza A (H1N1) virus in humans",
            "authors": [],
            "year": 2009,
            "venue": "N Engl J Meg",
            "volume": "360",
            "issn": "",
            "pages": "2605-2615",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Single versus dual respiratory virus infections in hospitalized infants: Impact on clinical course of disease and interferon\u2010y response",
            "authors": [],
            "year": 2005,
            "venue": "Pediatr Infect Dis J",
            "volume": "24",
            "issn": "",
            "pages": "605-610",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Prospective evaluation of a novel multiplex real\u2010time PCR assay for detection of fifteen respiratory pathogens. Duration of symptoms significantly affects detection rate",
            "authors": [],
            "year": 2010,
            "venue": "J Clin Virol",
            "volume": "47",
            "issn": "",
            "pages": "589-594",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Frequent detection of viral coinfection in children hospitalized with acute respiratory tract infection using a real\u2010time polymerase chain reaction",
            "authors": [],
            "year": 2008,
            "venue": "Pediatr Infect Dis J",
            "volume": "27",
            "issn": "",
            "pages": "589-594",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Multipathogen infections in hospitalized children with acute respiratory infections",
            "authors": [],
            "year": 2009,
            "venue": "Virol J",
            "volume": "6",
            "issn": "",
            "pages": "155-162",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Development of a respiratory virus panel test for detection of twenty human respiratory viruses by use of multiplex PCR and a fluid microbead\u2010based assay",
            "authors": [],
            "year": 2007,
            "venue": "J Clin Microbiol",
            "volume": "45",
            "issn": "",
            "pages": "2965-2970",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Low mortality rates related to respiratory virus infections after bone marrow transplant",
            "authors": [],
            "year": 2003,
            "venue": "Bone Marrow Transplant",
            "volume": "31",
            "issn": "",
            "pages": "695-700",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Incidence of common respiratory viral infections related to climate factors in hospitalized children in Hong Kong",
            "authors": [],
            "year": 2010,
            "venue": "Epidemiol Infect",
            "volume": "138",
            "issn": "",
            "pages": "226-235",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Respiratory viral infections in infants: causes, clinical symptoms, virology, and immunology",
            "authors": [],
            "year": 2010,
            "venue": "Clin Microbiol Rev",
            "volume": "23",
            "issn": "",
            "pages": "74-98",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Prevalence of human respiratory viruses in adults with acute respiratory tract infections in Beijing, 2005\u20102007",
            "authors": [],
            "year": 2009,
            "venue": "Clin Microbiol Infect",
            "volume": "15",
            "issn": "",
            "pages": "1146-1153",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Correlation of viral load of respiratory pathogens and co\u2010infections with disease severity in children hospitalized for lower respiratory tract infection",
            "authors": [],
            "year": 2010,
            "venue": "J Clin Virol",
            "volume": "48",
            "issn": "",
            "pages": "239-245",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Impact of respiratory virus infections on persons with chronic underlying conditions",
            "authors": [],
            "year": 2010,
            "venue": "JAMA",
            "volume": "283",
            "issn": "",
            "pages": "499-505",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Human metapneumovirus and respiratory syncytial virus in hospitalized Danish children with acute respiratory tract infection",
            "authors": [],
            "year": 2004,
            "venue": "Scand J Infect Dis",
            "volume": "36",
            "issn": "",
            "pages": "578-584",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Evidence from multiplex molecular assays for complex multipathogen interaction in acute respiratory infections",
            "authors": [],
            "year": 2008,
            "venue": "J Clin Microbiol",
            "volume": "46",
            "issn": "",
            "pages": "97-102",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Rhinovirus outbreaks in long\u2010term care facilities, Ontario, Canada",
            "authors": [],
            "year": 2010,
            "venue": "Emerg Infect Dis",
            "volume": "16",
            "issn": "",
            "pages": "1463-1465",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Viruses in community\u2010 acquired pneumonia in children aged less than 3\u2003years old; High rate of viral confection",
            "authors": [],
            "year": 2008,
            "venue": "J Med Virol",
            "volume": "80",
            "issn": "",
            "pages": "1843-1849",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "Correlation of pandemic (H1N1) 2009 viral load with disease severity and prolonged viral shedding in children",
            "authors": [],
            "year": 2010,
            "venue": "Emerg Infect Dis",
            "volume": "16",
            "issn": "",
            "pages": "1266-1272",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "Self\u2010reported pH1N1 influenza vaccination coverage for Ontario",
            "authors": [],
            "year": 2011,
            "venue": "Statistics Canada",
            "volume": "22",
            "issn": "",
            "pages": "1-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF19": {
            "title": "Streptococcus pneumonia coinfection is correlated with the severity of H1N1 pandemic influenza",
            "authors": [],
            "year": 2009,
            "venue": "PLoS ONE",
            "volume": "4",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF20": {
            "title": "Invasive group A streptococcal infection concurrent with 2009 H1N1 influenza",
            "authors": [],
            "year": 2010,
            "venue": "Clin Infect Dis",
            "volume": "50",
            "issn": "",
            "pages": "e59-e62",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF21": {
            "title": "Complications of seasonal and pandemic influenza",
            "authors": [],
            "year": 2010,
            "venue": "Crit Care Med",
            "volume": "38",
            "issn": "",
            "pages": "e91-e97",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF22": {
            "title": "WHO checklist for influenza pandemic preparedness planning 2005",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF23": {
            "title": "Rhinovirus, A (H1N1)v, RVS: The race for hivernal pandemics, France 2009\u20102010",
            "authors": [],
            "year": 2009,
            "venue": "Euro Surveill",
            "volume": "14",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF24": {
            "title": "Association between the 2008\u201009 seasonal influenza vaccine and pandemic H1N1 illness during spring\u2010summer 2009: four observational studies from Canada",
            "authors": [],
            "year": 2010,
            "venue": "PLoS Med.",
            "volume": "7",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF25": {
            "title": "Respiratory virus infections among hospitalized patients with suspected influenza A H1N1 2009 virus during the first pandemic wave in Brazil",
            "authors": [],
            "year": 2010,
            "venue": "CMAJ",
            "volume": "182",
            "issn": "",
            "pages": "257-264",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF26": {
            "title": "Bacterial coinfections in lung tissue specimens from fatal cases of 2009 Pandemic Influenza A (H1N1)",
            "authors": [],
            "year": 2009,
            "venue": "Morb Mortal Wkly Rep",
            "volume": "58",
            "issn": "",
            "pages": "1071-1074",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF27": {
            "title": "The association of newly identified respiratory viruses with lowers respiratory tract infections in Korean children, 2000\u20102005",
            "authors": [],
            "year": 2006,
            "venue": "Clin Infect Dis",
            "volume": "43",
            "issn": "",
            "pages": "585-592",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF28": {
            "title": "Association of rhinovirus infection with increased disease severity in acute bronchiolitis",
            "authors": [],
            "year": 2002,
            "venue": "Am J Respir Crit Care Med",
            "volume": "165",
            "issn": "",
            "pages": "1285-1289",
            "other_ids": {
                "DOI": []
            }
        }
    }
}