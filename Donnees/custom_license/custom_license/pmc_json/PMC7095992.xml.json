{
    "paper_id": "PMC7095992",
    "metadata": {
        "title": "gp41: HIV's shy protein",
        "authors": [
            {
                "first": "Michael",
                "middle": [
                    "B"
                ],
                "last": "Zwick",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Erica",
                "middle": [
                    "O"
                ],
                "last": "Saphire",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Dennis",
                "middle": [
                    "R"
                ],
                "last": "Burton",
                "suffix": "",
                "email": "burton@scripps.edu",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The structure of gp41 was a long-sought and elusive goal for HIV biologists. The difficulties of working with a membrane protein, the strong tendency of gp41 to aggregate, and other hurdles stood in the way. When Chan et al.1 and Weissenhorn et al.2 finally produced the first structures in 1997, followed shortly by Tan et al.3, they began to give molecular form to the apparatus of viral fusion, whose outline had emerged earlier in the decade. The structures are the focus of much work to design new inhibitors of the fusion process, and serve as an outstanding example of how findings in basic science can stimulate exploration of new therapeutics.",
            "cite_spans": [
                {
                    "start": 224,
                    "end": 225,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 248,
                    "end": 249,
                    "mention": "2",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 327,
                    "end": 328,
                    "mention": "3",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "This is what fusion looks like: envelope spikes composed of gp41 and gp120 decorate the surface of HIV-1. The surface glycoprotein of the spike, gp120, engages CD4 and CCR5 or CXCR4 coreceptors, primarily on macrophages and CD4+ T cells. gp120 undergoes conformational changes to activate gp41, a transmembrane glycoprotein that uses conserved structural elements to mediate the fusion of the host and viral membranes. Genetic information can then flow from the virus to the target cell.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The structure of the spikes is dictated by the virus' need to bind and enter target cells while avoiding a host neutralizing antibody response. In such a response, host antibodies bind to the virus and prevent it from entering the target cell To manage this problem, HIV-1 has evolved to keep the vulnerable conserved regions of the envelope spike\u2014such as those on gp41 that comprise the fusion machinery\u2014hidden as much as possible. This serves two purposes: it restricts the ability of conserved epitopes to elicit antibodies (low immunogenicity), and it helps prevent antibodies that are elicited from binding to conserved epitopes (low antigenicity). Neutralizing antibodies against conserved epitopes are a particular threat to HIV-1, as escape via mutation is more difficult than for variable regions of the envelope spikes.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In the mature, 'untriggered' spike, much of the surface of gp41 is masked by gp120. After receptor engagement, during which gp41 is perhaps most vulnerable, access to the fusion-intermediate state of gp41 is restricted by the closely opposing membranes. The fusion process itself takes only minutes, which significantly limits access to the transiently exposed epitopes on gp41. In contrast, the postfusion conformation of gp41 is a stable repeating unit that is highly immunogenic. This postfusion conformation appears to be so radically different from the fusion-intermediate conformations of gp41 that elicited antibodies are unable to bind to the intermediate conformations and therefore are unable to inhibit fusion.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "This description was given a molecular underpinning in 1997, with the X-ray crystal structures of the putative postfusion form of HIV-1 gp41 (refs. 1\u20133). Before then, it was known that the N- and C-terminal portions of the external domain of gp41 contain heptad repeat sequences. Peptides corresponding to the N-terminal (N-HR) and C-terminal (C-HR) heptad repeat regions of gp41, notably T21 (DP107) and T20 (DP178), respectively, could inhibit HIV-1 entry into cells4,5,6. T20, rebranded as enfuvirtide, was approved by the US Food and Drug Administration for patients last fall.",
            "cite_spans": [
                {
                    "start": 148,
                    "end": 149,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 150,
                    "end": 151,
                    "mention": "3",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 468,
                    "end": 469,
                    "mention": "4",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 470,
                    "end": 471,
                    "mention": "5",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 472,
                    "end": 473,
                    "mention": "6",
                    "ref_id": "BIBREF17"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "To overcome the technical hurdles of crystallizing gp41, it was necessary to truncate the protein to make it stable and soluble. Each of the three laboratories crystallized a proteolytically stable core of the extraviral domain of gp41; (devoid of the N-terminal hydrophobic fusion peptide, a disulfide-loop region linking the N-HR and C-HR regions, and the membrane-proximal region).",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Although each of the groups crystallized fragments of different sizes, the resulting structures were nearly identical. gp41 was found to have a trimeric core consisting of parallel \u03b1-helices corresponding to the N-HR region of gp41. Packed around this trimeric core were three, slightly oblique C-HR \u03b1-helices, arranged antiparallel to the inner core to create a trimer of heterodimers, or a six-helix bundle (Fig. 1).",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 410,
                    "end": 416,
                    "mention": "Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Notably, this arrangement had also been observed in the pH-induced conformation of the influenza hemagglutinin molecule7 and the transmembrane subunit of Moloney murine leukemia virus8. The structural motif has since been found in Ebola virus, Newcastle disease virus, respiratory syncytial virus and the coronavirus linked to severe acute respiratory syndrome9,10. The motif is a striking example of convergent evolution, and has revealed a vulnerability to be exploited in drug design.",
            "cite_spans": [
                {
                    "start": 119,
                    "end": 120,
                    "mention": "7",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 183,
                    "end": 184,
                    "mention": "8",
                    "ref_id": "BIBREF19"
                },
                {
                    "start": 360,
                    "end": 361,
                    "mention": "9",
                    "ref_id": "BIBREF20"
                },
                {
                    "start": 362,
                    "end": 364,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The gp41 structures provide an understanding of how T20 and other gp41-targeted fusion inhibitors work, and shed light on the difficulties of designing vaccines that elicit neutralizing antibodies to gp41.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The T20 peptide corresponds to the C-HR region of gp41 and inhibits HIV-1 entry by packing against the outer grooves on the trimer of \u03b1-helices, thereby preventing the formation of six-helix bundles11. Resistance to T20 has been documented, typically involving one or two substitutions along the outer grooves in the N-HR region of gp41.",
            "cite_spans": [
                {
                    "start": 198,
                    "end": 200,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The relatively new drug T1249, a peptide analog of T20, is an effective inhibitor of T20-resistant isolates of HIV-1 and is in clinical trials (http://www.trimeris.com/pipeline/clinical.html). Resistance to T1249 is anticipated but has yet to be documented. Peptides corresponding to the N-HR region of gp41 inhibit six-helix bundle formation by targeting the C-HR region. Monomeric N-HR peptides such as T21 are prone to aggregation, making them much less potent than T20; potency is increased substantially with soluble, trimeric N-HR constructs12,13.",
            "cite_spans": [
                {
                    "start": 547,
                    "end": 549,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 550,
                    "end": 552,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In principle, antibodies that could bind to the N-HR inner helical core after receptor activation should also block entry of HIV-1, similar to the fusion inhibitors. In terms of vaccine design, however, eliciting neutralizing antibodies to this target may be difficult because of spatial and temporal limitations during the fusion process. The broadly neutralizing antibodies defined to date seem to recognize gp41 epitopes that are close to the viral membrane but accessible during fusion14,15. Vaccinologists await structures for the fusion intermediates.",
            "cite_spans": [
                {
                    "start": 489,
                    "end": 491,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 492,
                    "end": 494,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The gp41 structures have stimulated progress in the design and selection of fusion inhibitors. In one study, an engineered trimer of the inner N-HR helical peptides was used to select peptides from a random phage display library. These peptides, selected as proteolytically stable mirror-image or D-peptides, potently inhibit HIV-1 entry into cells13. In another study, investigators constructed a five-helix bundle that lacks one of the outer C-HR peptides. The five-helix bundle potently inhibited HIV-1 entry, probably by binding to a single C-HR region on HIV-1 gp41 and preventing six-helix bundle formation16. In a particularly inspired demonstration of structure-based design, the solvent-exposed residues on one face of the \u03b1-helix of a gp41 C-HR peptide were substituted onto the solvent-exposed face of another helical scaffold, GCN-4. Indeed, the resulting hybrid peptide blocked HIV-1 entry into cells17.",
            "cite_spans": [
                {
                    "start": 348,
                    "end": 350,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 612,
                    "end": 614,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 913,
                    "end": 915,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "There are major problems with peptide therapeutics, including the cost and difficulty of manufacturing them. T20 currently costs $20,000 per year per patient, and the supply has yet to satisfy the demand. In an initial effort to develop nonpeptide inhibitors of HIV-1 entry using the crystal structures of gp41, molecular docking algorithms have been used to search small molecule libraries in silico for compounds that might bind in a deep hydrophobic cavity in the inner N-HR peptide core. Some of the best-ranking compounds inhibited HIV-1 infectivity with modest potency in vivo18.",
            "cite_spans": [
                {
                    "start": 582,
                    "end": 584,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Another group used a short C-HR peptide as a scaffold for attaching a nonpeptide combinatorial library onto its N terminus. The library was screened against an engineered trimer of N-HR peptides, and a hybrid ligand, composed of a synthetic moiety linked to the C-HR peptide, was selected that was able to inhibit HIV-1 envelope\u2013mediated cell-cell fusion19. Further optimization of the nonpeptide moiety is envisioned as a route to a stand-alone, small-molecule fusion inhibitor.",
            "cite_spans": [
                {
                    "start": 354,
                    "end": 356,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In another approach, a small molecule was designed to mimic a hydrophobic surface along one face of a heptad repeat \u03b1-helix; the proteomimetic was able to partially disrupt the six-helix bundle and inhibit HIV-1 mediated fusion20.",
            "cite_spans": [
                {
                    "start": 227,
                    "end": 229,
                    "mention": "20",
                    "ref_id": "BIBREF12"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "As the hidden secrets of gp41 continue to be revealed, new leads for HIV-1 therapies and vaccines will emerge. Interesting new targets for intervention may be uncovered by obtaining detailed structural information on the native and fusion-intermediate conformations of gp41. It will also be useful to elucidate the influence on gp41 structure and function of the segments that were omitted in the gp41 crystal structures.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Small-molecule drug design has been extremely effective in inhibiting the HIV-1 protease and reverse transcriptase, but it remains to be seen how well it will work against the envelope complex. Since drug resistance is so prevalent for HIV-1 and has already been documented for T20, drug cocktails may be the best solution. Indeed, the range of potential targets on gp41 and a growing arsenal of other entry inhibitors21 make synergy with existing and future drug combinations an exciting possibility.",
            "cite_spans": [
                {
                    "start": 418,
                    "end": 420,
                    "mention": "21",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\nThis material is part of Nature Medicine's 10 year anniversary series. For more content related to these special focus issues, please see \nhttp://www.nature.com/nm/special_focus/anniversary/index.html\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: The HIV-1 envelope spike is a putative trimer of gp120-gp41 heterodimers. (a) Before fusion. In the native, untriggered conformation, the ectodomain (extraviral portion) of gp41 is largely cloaked by gp120 (transparent here). The structural details of this native configuration are not well elucidated. (b) Fusion intermediate(s). Upon binding CD4 and coreceptor, gp120 undergoes conformational changes that expose gp41 and activate the fusion machinery, notably the N-HR regions and the fusion peptide (FP). gp41 is probably in a somewhat extended arrangement in which the N-HR region of three gp41 molecules form an \u03b1-helical bundle, positioning the fusion peptides for insertion into the host-cell membrane. The C-HR region of gp41 is drawn here without defined secondary structure, but some models show it in an \u03b1-helical conformation. gp41-targeting fusion inhibitors such as T20 act on the fusion-intermediate conformation(s) of gp41 to inhibit six-helix bundle formation. (c) After fusion. Fusion seems to coincide with chain reversal, or 'jackknifing', such that the antiparallel N-HR and C-HR regions form a six-helix bundle, represented here by the structure of Weissenhorn et al.2. Deborah Maizels",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "DC",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Fass",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Berger",
                    "suffix": ""
                },
                {
                    "first": "PS",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Cell",
            "volume": "89",
            "issn": "",
            "pages": "263-273",
            "other_ids": {
                "DOI": [
                    "10.1016/S0092-8674(00)80205-6"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Bosch",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "van der Zee",
                    "suffix": ""
                },
                {
                    "first": "CA",
                    "middle": [],
                    "last": "de Haan",
                    "suffix": ""
                },
                {
                    "first": "PJ",
                    "middle": [],
                    "last": "Rottier",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J. Virol.",
            "volume": "77",
            "issn": "",
            "pages": "8801-8811",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.77.16.8801-8811.2003"
                ]
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [
                {
                    "first": "GB",
                    "middle": [],
                    "last": "Melikyan",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "J. Cell Biol.",
            "volume": "151",
            "issn": "",
            "pages": "413-423",
            "other_ids": {
                "DOI": [
                    "10.1083/jcb.151.2.413"
                ]
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [
                {
                    "first": "CA",
                    "middle": [],
                    "last": "Bewley",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Louis",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Ghirlando",
                    "suffix": ""
                },
                {
                    "first": "GM",
                    "middle": [],
                    "last": "Clore",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J. Biol. Chem.",
            "volume": "277",
            "issn": "",
            "pages": "14238-14245",
            "other_ids": {
                "DOI": [
                    "10.1074/jbc.M201453200"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [
                {
                    "first": "DM",
                    "middle": [],
                    "last": "Eckert",
                    "suffix": ""
                },
                {
                    "first": "VN",
                    "middle": [],
                    "last": "Malashkevich",
                    "suffix": ""
                },
                {
                    "first": "LH",
                    "middle": [],
                    "last": "Hong",
                    "suffix": ""
                },
                {
                    "first": "PA",
                    "middle": [],
                    "last": "Carr",
                    "suffix": ""
                },
                {
                    "first": "PS",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "Cell",
            "volume": "99",
            "issn": "",
            "pages": "103-115",
            "other_ids": {
                "DOI": [
                    "10.1016/S0092-8674(00)80066-5"
                ]
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Binley",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J. Virol.",
            "volume": "77",
            "issn": "",
            "pages": "5678-5684",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.77.10.5678-5684.2003"
                ]
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [
                {
                    "first": "MB",
                    "middle": [],
                    "last": "Zwick",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "J. Virol.",
            "volume": "75",
            "issn": "",
            "pages": "10892-10905",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.75.22.10892-10905.2001"
                ]
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [
                {
                    "first": "MJ",
                    "middle": [],
                    "last": "Root",
                    "suffix": ""
                },
                {
                    "first": "MS",
                    "middle": [],
                    "last": "Kay",
                    "suffix": ""
                },
                {
                    "first": "PS",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Science",
            "volume": "291",
            "issn": "",
            "pages": "884-888",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1057453"
                ]
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [
                {
                    "first": "SK",
                    "middle": [],
                    "last": "Sia",
                    "suffix": ""
                },
                {
                    "first": "PS",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "100",
            "issn": "",
            "pages": "9756-9761",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.1733910100"
                ]
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [
                {
                    "first": "AK",
                    "middle": [],
                    "last": "Debnath",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Radigan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Jiang",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J. Med. Chem.",
            "volume": "42",
            "issn": "",
            "pages": "3203-3209",
            "other_ids": {
                "DOI": [
                    "10.1021/jm990154t"
                ]
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ferrer",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "Nat. Struct. Biol.",
            "volume": "6",
            "issn": "",
            "pages": "953-960",
            "other_ids": {
                "DOI": [
                    "10.1038/13324"
                ]
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Weissenhorn",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Dessen",
                    "suffix": ""
                },
                {
                    "first": "SC",
                    "middle": [],
                    "last": "Harrison",
                    "suffix": ""
                },
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "Skehel",
                    "suffix": ""
                },
                {
                    "first": "DC",
                    "middle": [],
                    "last": "Wiley",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Nature",
            "volume": "387",
            "issn": "",
            "pages": "426-430",
            "other_ids": {
                "DOI": [
                    "10.1038/387426a0"
                ]
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [
                {
                    "first": "JT",
                    "middle": [],
                    "last": "Ernst",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Angew. Chem. Int. Ed. Engl.",
            "volume": "41",
            "issn": "",
            "pages": "278-281",
            "other_ids": {
                "DOI": [
                    "10.1002/1521-3773(20020118)41:2<278::AID-ANIE278>3.0.CO;2-A"
                ]
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [
                {
                    "first": "JP",
                    "middle": [],
                    "last": "Moore",
                    "suffix": ""
                },
                {
                    "first": "RW",
                    "middle": [],
                    "last": "Doms",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "100",
            "issn": "",
            "pages": "10598-10602",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.1932511100"
                ]
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Tan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Shen",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "94",
            "issn": "",
            "pages": "12303-12308",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.94.23.12303"
                ]
            }
        },
        "BIBREF15": {
            "title": "",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Jiang",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Strick",
                    "suffix": ""
                },
                {
                    "first": "AR",
                    "middle": [],
                    "last": "Neurath",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Nature",
            "volume": "365",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1038/365113a0"
                ]
            }
        },
        "BIBREF16": {
            "title": "",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wild",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "91",
            "issn": "",
            "pages": "12676-12680",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.91.26.12676"
                ]
            }
        },
        "BIBREF17": {
            "title": "",
            "authors": [
                {
                    "first": "CT",
                    "middle": [],
                    "last": "Wild",
                    "suffix": ""
                },
                {
                    "first": "DC",
                    "middle": [],
                    "last": "Shugars",
                    "suffix": ""
                },
                {
                    "first": "TK",
                    "middle": [],
                    "last": "Greenwell",
                    "suffix": ""
                },
                {
                    "first": "CB",
                    "middle": [],
                    "last": "McDanal",
                    "suffix": ""
                },
                {
                    "first": "TJ",
                    "middle": [],
                    "last": "Matthews",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "91",
            "issn": "",
            "pages": "9770-9774",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.91.21.9770"
                ]
            }
        },
        "BIBREF18": {
            "title": "",
            "authors": [
                {
                    "first": "PA",
                    "middle": [],
                    "last": "Bullough",
                    "suffix": ""
                },
                {
                    "first": "FM",
                    "middle": [],
                    "last": "Hughson",
                    "suffix": ""
                },
                {
                    "first": "JJ",
                    "middle": [],
                    "last": "Skehel",
                    "suffix": ""
                },
                {
                    "first": "DC",
                    "middle": [],
                    "last": "Wiley",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Nature",
            "volume": "371",
            "issn": "",
            "pages": "37-43",
            "other_ids": {
                "DOI": [
                    "10.1038/371037a0"
                ]
            }
        },
        "BIBREF19": {
            "title": "",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Fass",
                    "suffix": ""
                },
                {
                    "first": "SC",
                    "middle": [],
                    "last": "Harrison",
                    "suffix": ""
                },
                {
                    "first": "PS",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 1996,
            "venue": "Nat. Struct. Biol.",
            "volume": "3",
            "issn": "",
            "pages": "465-469",
            "other_ids": {
                "DOI": [
                    "10.1038/nsb0596-465"
                ]
            }
        },
        "BIBREF20": {
            "title": "",
            "authors": [
                {
                    "first": "PM",
                    "middle": [],
                    "last": "Colman",
                    "suffix": ""
                },
                {
                    "first": "MC",
                    "middle": [],
                    "last": "Lawrence",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Nat. Rev. Mol. Cell Biol.",
            "volume": "4",
            "issn": "",
            "pages": "309-319",
            "other_ids": {
                "DOI": [
                    "10.1038/nrm1076"
                ]
            }
        }
    }
}