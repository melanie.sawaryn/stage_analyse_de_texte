{
    "paper_id": "PMC3294680",
    "metadata": {
        "title": "Pulmonary Tuberculosis and SARS, China",
        "authors": [
            {
                "first": "Wei",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Arnaud",
                "middle": [],
                "last": "Fontanet",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Pan-He",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Lin",
                "middle": [],
                "last": "Zhan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Zhong-Tao",
                "middle": [],
                "last": "Xin",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Fang",
                "middle": [],
                "last": "Tang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Laurence",
                "middle": [],
                "last": "Baril",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wu-Chun",
                "middle": [],
                "last": "Cao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "To the Editor: As part of a cohort study of 83 patients with severe acute respiratory syndrome (SARS) in Beijing, China, we conducted a follow-up study of all the patients by routine medical examination. During the process, 3 patients with chest radiographs consistent with active disease were identified as having pulmonary tuberculosis (TB). Here we describe the 1-year clinical outcome and immune response in these patients.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Demographic details and coexisting conditions are shown in the Table. Patient 1 was a healthcare worker who became infected with SARS-associated coronavirus (CoV) while on duty with SARS patients. After he was transferred to a hospital dedicated to SARS management, pulmonary TB was diagnosed (positive acid-fast bacilli smear on sputum samples). Patients 2 and 3 were known to have cases of pulmonary TB and became infected with SARS-CoV after contact with other patients hospitalized for SARS. These 2 patients were sputum smear\u2013negative for acid-fast bacilli, and diagnosis was made on the basis of previous exposure to TB, relevant symptoms of typical pulmonary TB, chest radiographs consistent with active disease, a positive tuberculin skin test result, and the finding of cavity regression on chest radiographs after anti-TB treatment was initiated. No cultures were obtained for isolation and comparison of Mycobacterium tuberculosis strains (1). All 3 patients had confirmed SARS based on amplification of SARS-CoV RNA by reverse transcriptase\u2013polymerase chain reaction (RT-PCR) from sputum and stool specimens (2). Patients 2 and 3 recovered without complications; patient 1 had the most severe disease and required mechanical ventilation in an intensive care unit before recovering.",
            "cite_spans": [
                {
                    "start": 951,
                    "end": 952,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1121,
                    "end": 1122,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 63,
                    "end": 68,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Both cellular and humoral immunity were evaluated during the follow-up of these patients. T-lymphocyte subsets were measured 6 months after disease onset by flow cytometry using fluorescein isothiocyanate\u2013labeled specific monoclonal antibodies. Compared to other SARS patients (n = 47), the 3 patients with TB had lower mean CD4+ T cells (368.4/\u03bcL vs. 656.6/\u03bcL, respectively; p = 0.05) and lower mean CD8+ T cells (371.0/\u03bcL vs. 490.1/\u03bcL, respectively; p = 0.39). SARS-CoV immunoglobulin G (IgG) antibody titers were measured by enzyme-linked immunosorbent assay kit (Huada Company, Beijing, China) at months 1, 2, 3, 4, 7, 10, and 16 after disease onset. (Titers were not measured for the 3 TB patients at month l.) Compared to most (26 [78.8%] of 33) other SARS patients whose antibodies remained detectable throughout follow-up, 2 of the 3 TB patients (patients 1 and 3) had undetectable antibody titers as of months 7 and 16, respectively. In patient 1, antibody titers, when detectable, were unusually low (40). Both patients 1 and 3 had prolonged viral excretion in stools, sputum, or both. While the median (range) duration of virus excretion in stools and sputa for the entire measurable cohort (n = 56) was 27 (16\u2013127) and 21 (14\u201352) days, respectively (3), it was 125 and 16 days for patient 1, and 109 and 52 days for patient 3 (viral excretion data could not be obtained from patient 2 because sequential specimens for detection were unavailable).",
            "cite_spans": [
                {
                    "start": 1262,
                    "end": 1263,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "TB in SARS patients has been reported on rare occasions (4,5). In a cohort of 236 patients in Singapore, it was diagnosed in 2 patients after recovery from SARS (4). As with patient 1 in this study, TB had developed after the patient acquired SARS, most likely as the result of reactivation of past infection or new infection with M. tuberculosis, while temporarily immunosuppressed because of SARS (6) and corticoid therapy. Such phenomena have been described with other viral infections such as measles and HIV (7,8). By contrast, patients 2 and 3 were known TB patients who acquired SARS through exposure to SARS patients in the same hospital wards. Both diseases are known to be transiently immunosuppressive (6,9), and their combined effect resulted in more pronounced CD4+ cell decreases in coinfected SARS patients than others. Such immunosuppression also resulted in poorer IgG antibody response in coinfected SARS patients than in others and delayed viral clearance, as shown by longer viral excretion in sputum and stools. While viral excretion could be prolonged in coinfected patients, no virus could be isolated from any RT-PCR\u2013positive specimen collected after 6 weeks of illness, which suggests that excreted viruses were no longer infectious (3).",
            "cite_spans": [
                {
                    "start": 57,
                    "end": 58,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 59,
                    "end": 60,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 162,
                    "end": 163,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 400,
                    "end": 401,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 514,
                    "end": 515,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 516,
                    "end": 517,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 714,
                    "end": 715,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 716,
                    "end": 717,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1259,
                    "end": 1260,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "These case reports remind us of the importance of strict isolation of SARS patients, careful use of steroids for their case management, and the possibility of coinfection with TB in SARS patients with incomplete recovery.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "National Tuberculosis Genotyping and Surveillance Network: design and methods.",
            "authors": [],
            "year": 2002,
            "venue": "Emerg Infect Dis",
            "volume": "8",
            "issn": "",
            "pages": "1192-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Revised U.S. surveillance case definition for severe acute respiratory syndrome (SARS) and update on SARS cases\u2014United States and worldwide, December 2003.",
            "authors": [],
            "year": 2003,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "52",
            "issn": "",
            "pages": "1202-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Long-term SARS-coronavirus excretion from a patient cohort in China.",
            "authors": [],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "1841-3",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Severe acute respiratory syndrome and pulmonary tuberculosis.",
            "authors": [],
            "year": 2004,
            "venue": "Clin Infect Dis",
            "volume": "38",
            "issn": "",
            "pages": "e123-5",
            "other_ids": {
                "DOI": [
                    "10.1086/421396"
                ]
            }
        },
        "BIBREF4": {
            "title": "Nosocomial transmission of Mycobacterium tuberculosis found through screening for severe acute respiratory syndrome\u2014Taipei, Taiwan, 2003.",
            "authors": [],
            "year": 2004,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "53",
            "issn": "",
            "pages": "321-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Significant changes of peripheral T lymphocyte subsets in patients with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2004,
            "venue": "J Infect Dis",
            "volume": "189",
            "issn": "",
            "pages": "648-51",
            "other_ids": {
                "DOI": [
                    "10.1086/381535"
                ]
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 1996,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Current concepts: tuberculosis in patients with human immunodeficiency virus infection.",
            "authors": [],
            "year": 1999,
            "venue": "N Engl J Med",
            "volume": "340",
            "issn": "",
            "pages": "367-73",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJM199902043400507"
                ]
            }
        },
        "BIBREF8": {
            "title": "Tuberculosis.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "362",
            "issn": "",
            "pages": "887-99",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)14333-4"
                ]
            }
        }
    }
}