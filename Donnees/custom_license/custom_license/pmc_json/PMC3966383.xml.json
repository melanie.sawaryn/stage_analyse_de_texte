{
    "paper_id": "PMC3966383",
    "metadata": {
        "title": "New Alphacoronavirus in Mystacina tuberculata Bats, New Zealand",
        "authors": [
            {
                "first": "Richard",
                "middle": [
                    "J."
                ],
                "last": "Hall",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jing",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Matthew",
                "middle": [],
                "last": "Peacey",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Nicole",
                "middle": [
                    "E."
                ],
                "last": "Moore",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kate",
                "middle": [],
                "last": "McInnes",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Daniel",
                "middle": [
                    "M."
                ],
                "last": "Tompkins",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Four bat guano samples were collected from known roost sites on the remote offshore island of Whenua hou (Codfish Island) (46\u00b047\u2032S, 167\u00b038\u2032E), which is situated at the southern coast of New Zealand. This small island is heavily forested and largely unmodified by humans. It has special conservation status for the protection of endangered species, and public access is not permitted (9).",
            "cite_spans": [
                {
                    "start": 384,
                    "end": 385,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Bat guano was held at 4\u00b0C during transport (<48 h), resuspended in 2 mL phosphate-buffered saline, and subjected to centrifugation at 6,000 \u00d7 g for 5 min. RNA was extracted from 400 \u00b5L of supernatant by using the iPrep PureLink Virus Kit (Life Technologies, Carlsbad, CA, USA) and eluted into 50 \u00b5L reverse transcription PCR molecular-grade water (Ambion, Austin, TX, USA). Metagenomic sequencing was then conducted for 1 of the samples by using an Illumina MiSeq Instrument (New Zealand Genomics Ltd., Massey Genome Service, Massey University, Palmerston North, New Zealand) after a series of steps involving DNase I treatment, reverse transcription, multiple displacement amplification (QIAGEN, Valencia, CA, USA), and Illumina TruSeq library preparation (New Zealand Genomics, Ltd.).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "A total of 10,749,878 paired-end sequence reads of 250 bp were generated and assembled into contigs by using Velvet 1.2.07 (10). Assembled contigs were searched for viral sequence by comparison to the nonredundant nucleotide database in Genbank (downloaded March 2013) by using the nucleotide basic local alignment search tool (National Center for Biotechnology Information, Bethesda, MD, USA). Forty-six contigs showed similarity to known genus Alphacoronavirus sequences (Table). (Raw sequence data are available on request from the authors.)",
            "cite_spans": [
                {
                    "start": 124,
                    "end": 126,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 474,
                    "end": 479,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Virus sequence from the genus Alphacoronavirus was confirmed in all 4 original guano samples by using a specific reverse transcription PCR based on the metagenomic data specific for 582 bp of the RNA-dependent RNA polymerase (RdRp) gene (GenBank accession nos. KF515987\u2013KF515990). The Rdrp sequence was identical in all 4 guano samples, and the closest relative was bat coronavirus HKU8 (GenBank accession no. DQ249228; 79% nt identity, 426/542). Phylogenetic analysis was performed for Rdrp (Figure 1) and for the spike protein, as derived from metagenomic data (GenBank accession no. KF575176) (Figure 2). We propose that these data support identification of a new alphacoronavirus, which has been designated as Mystacina bat CoV/New Zealand/2013.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 493,
                    "end": 501,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 597,
                    "end": 605,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The discovery of unknown coronaviruses provides information for a model of coronavirus evolution (11) and contributes to understanding the process of disease emergence, as in detection of Middle East respiratory syndrome coronavirus (12). The alphacoronavirus identified in this study from M. tuberculata bat guano is unique in respect to the extreme geographic and evolutionary isolation of the host bat species, which along with C. tuberculatus bats, has been separated from all other mammalian species for \u22481 million years.",
            "cite_spans": [
                {
                    "start": 98,
                    "end": 100,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 234,
                    "end": 236,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "The current estimate for a common ancestor for all coronaviruses is 8,100 bce (13). To be consistent with this estimate, Mystacina bat coronavirus would need to have been introduced to bats on Whenua hou within the past 800 years since humans first arrived on this island (given that the island had no other mammals before this time) (11) or is extant to modern alphacoronavirus phylogenetic radiation (genesis and expansion). Apart from humans, only 2 other terrestrial mammals have ever inhabited Whenua hou: the brushtail possum (Trichosurus vulpecula) and the Polynesian rat (Rattus exulans), both of which were eliminated from the island in the late 1980s (11); neither mammal has been reported as a host of alphacoronaviruses. Members of the genus Alphacoronavirus infect only mammals. Thus, an avian origin for this virus is unlikely.",
            "cite_spans": [
                {
                    "start": 79,
                    "end": 81,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 335,
                    "end": 337,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 662,
                    "end": 664,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "An alternative theory of an ancient origin for all coronaviruses has recently been proposed that involves an alternative evolutionary molecular clock analysis, which places the most recent common ancestor many millions of years ago (14). The discovery of Mystacina bat CoV/New Zealand/2013 virus could lend support to such a theory; despite potentially millions of years of isolation, it has diverged relatively little from other extant alphacoronaviruses, as shown by the close relationship of the Rdrp and spike protein genes to those of other extant alphacoronaviruses (Figures 1, 2). An expanded survey for Mystacina bat coronavirus in mammals in New Zealand and subsequent characterization of viral genomes would provide further insights into the origin of coronaviruses.",
            "cite_spans": [
                {
                    "start": 233,
                    "end": 235,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Conclusions",
            "ref_spans": [
                {
                    "start": 573,
                    "end": 582,
                    "mention": "Figures 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 584,
                    "end": 585,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "No instances of human zoonotic disease from New Zealand bat species have been reported. Only a small number of conservation staff handle these bats, these staff use standard personal protective equipment and work practice. Staff are also offered prophylactic rabies vaccination as a precautionary measure, even though New Zealand is free from rabies. The genus Alphacoronavirus includes several human and animal pathogens, but on the basis of phylogenetic data in this study, it is not possible to estimate the risk posed by Mystacina bat coronavirus to human or animal health.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "New Zealand is not considered a hot spot for emerging infectious diseases. This country is free from many human and animal diseases, such as rabies and foot-and-mouth disease, and infections with human arboviruses because of recent colonization by humans and strict biosecurity border controls (15). Thus, indigenous wildlife in New Zealand has generally been viewed as an almost sterile and unique biosphere. Given detection of this coronavirus, more thorough characterization of the ecology of viruses and other microorganisms in native wildlife should be considered to fulfill conservation needs and further safeguard human and domestic animal health against cross-species transmission.",
            "cite_spans": [
                {
                    "start": 295,
                    "end": 297,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Phylogenetic tree showing genetic relatedness of RNA-dependent RNA polymerase amino acid sequences for Mystacina sp. bat coronavirus (CoV)/New Zealand/2013 (shown in boldface) with those of known coronaviruses. Evolutionary history was inferred for 183 informative amino acid sites by using the maximum-likelihood method based on the Whilan and Goldman model with gamma distribution in MEGA 5.05 software (www.megasoftware.net). Bootstrap values are calculated from 1,000 trees (only bootstrap values >50% are shown). Scale bar indicates nucleotide substitutions per site. TGEV, transmissible gastroenteritis CoV; PRCV, porcine respiratory CoV; SARS, severe acute respiratory syndrome.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Phylogenetic tree showing genetic relatedness of spike protein amino acid sequence for Mystacina sp. bat coronavirus (CoV)/New Zealand/2013 (shown in boldface) with those of known coronaviruses. Evolutionary history was inferred for 492 informative amino acid sites by using the maximum-likelihood method based on the Whilan and Goldman + F model with gamma distribution and invariant sites in MEGA 5.05 software (www.megasoftware.net). Bootstrap values are calculated from 1,000 trees (only bootstrap values >50% are shown). Scale bar indicates nucleotide substitutions per site. TGEV, transmissible gastroenteritis CoV; PRCV, porcine respiratory CoV; SARS, severe acute respiratory syndrome.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2012,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Velvet: algorithms for de novo short read assembly using de Bruijn graphs.",
            "authors": [],
            "year": 2008,
            "venue": "Genome Res",
            "volume": "18",
            "issn": "",
            "pages": "821-9",
            "other_ids": {
                "DOI": [
                    "10.1101/gr.074492.107"
                ]
            }
        },
        "BIBREF2": {
            "title": "An archaeological sequence for Codfish Island (Whenua hou), Southland, New Zealand.",
            "authors": [],
            "year": 2009,
            "venue": "New Zealand Journal of Archaeology.",
            "volume": "30",
            "issn": "",
            "pages": "5-21",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Middle East respiratory syndrome coronavirus in bats, Saudi Arabia.",
            "authors": [],
            "year": 2013,
            "venue": "Emerg Infect Dis",
            "volume": "19",
            "issn": "",
            "pages": "1819-23",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1911.131172"
                ]
            }
        },
        "BIBREF4": {
            "title": "Discovery of seven novel mammalian and avian coronaviruses in the genus Deltacoronavirus supports bat coronaviruses as the gene source of Alphacoronavirus and Betacoronavirus and avian coronaviruses as the gene source of Gammacoronavirus and Deltacoronavirus.",
            "authors": [],
            "year": 2012,
            "venue": "J Virol",
            "volume": "86",
            "issn": "",
            "pages": "3995-4008",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.06540-11"
                ]
            }
        },
        "BIBREF5": {
            "title": "A case for the ancient origin of coronaviruses.",
            "authors": [],
            "year": 2013,
            "venue": "J Virol",
            "volume": "87",
            "issn": "",
            "pages": "7039-45",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.03273-12"
                ]
            }
        },
        "BIBREF6": {
            "title": "Emerging infectious diseases in an island ecosystem: the New Zealand perspective.",
            "authors": [],
            "year": 2001,
            "venue": "Emerg Infect Dis",
            "volume": "7",
            "issn": "",
            "pages": "767-72",
            "other_ids": {
                "DOI": [
                    "10.3201/eid0705.017501"
                ]
            }
        },
        "BIBREF7": {
            "title": "Evolution of New Zealand\u2019s terrestrial fauna: a review of molecular evidence.",
            "authors": [],
            "year": 2008,
            "venue": "Philos Trans R Soc Lond B Biol Sci",
            "volume": "363",
            "issn": "",
            "pages": "3319-34",
            "other_ids": {
                "DOI": [
                    "10.1098/rstb.2008.0114"
                ]
            }
        },
        "BIBREF8": {
            "title": "Miocene mammal reveals a Mesozoic ghost lineage on insular New Zealand, southwest Pacific.",
            "authors": [],
            "year": 2006,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "103",
            "issn": "",
            "pages": "19419-23",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.0605684103"
                ]
            }
        },
        "BIBREF9": {
            "title": "Advances in New Zealand mammalogy 1990\u20132000: long-tailed bat.",
            "authors": [],
            "year": 2001,
            "venue": "J R Soc N Z",
            "volume": "31",
            "issn": "",
            "pages": "43-57",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Advances in New Zealand mammalogy 1990\u20132000: short-tailed bats.",
            "authors": [],
            "year": 2001,
            "venue": "J R Soc N Z",
            "volume": "31",
            "issn": "",
            "pages": "59-81",
            "other_ids": {
                "DOI": [
                    "10.1080/03014223.2001.9517639"
                ]
            }
        },
        "BIBREF11": {
            "title": "A time-calibrated species-level phylogeny of bats (Chiroptera, Mammalia).",
            "authors": [],
            "year": 2011,
            "venue": "PLoS Curr",
            "volume": "3",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/currents.RRN1212"
                ]
            }
        },
        "BIBREF12": {
            "title": "Bats that walk: a new evolutionary hypothesis for the terrestrial behaviour of New Zealand\u2019s endemic mystacinids.",
            "authors": [],
            "year": 2009,
            "venue": "BMC Evol Biol",
            "volume": "9",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/1471-2148-9-169"
                ]
            }
        },
        "BIBREF13": {
            "title": "Infectious and emerging diseases of bats, and health status of bats in New Zealand.",
            "authors": [],
            "year": 2003,
            "venue": "Surveillance.",
            "volume": "30",
            "issn": "",
            "pages": "15-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": 2012,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}