{
    "paper_id": "PMC7107887",
    "metadata": {
        "title": "Severe acute respiratory syndrome in haemodialysis patients: a report of two cases",
        "authors": [
            {
                "first": "Hon",
                "middle": [
                    "Lok"
                ],
                "last": "Tang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Au",
                "middle": [],
                "last": "Cheuk",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok",
                "middle": [
                    "Hong"
                ],
                "last": "Chu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "William",
                "middle": [],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sze",
                "middle": [
                    "Ho"
                ],
                "last": "Wong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yuk",
                "middle": [
                    "Lun"
                ],
                "last": "Cheng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Alex",
                "middle": [
                    "Wai",
                    "Yin"
                ],
                "last": "Yu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ka",
                "middle": [
                    "Shun"
                ],
                "last": "Fung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wai",
                "middle": [
                    "Kay"
                ],
                "last": "Tsang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hilda",
                "middle": [
                    "Wai",
                    "Han"
                ],
                "last": "Chan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok",
                "middle": [
                    "Lung"
                ],
                "last": "Tong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In March 2003, an outbreak of severe acute respiratory syndrome (SARS) occurred in Hong Kong. As of July 10, 2003, the World Health Organization has reported 1755 cases of SARS in Hong Kong and 8437 cases worldwide [1]. Outbreaks and case clusters have been reported in the literature in recent months [2\u20135]. The first known case of SARS in Hong Kong was a physician working in a hospital in Guangdong Province who travelled to Hong Kong on February 21, 2003 [3]. A novel coronavirus has been identified as a possible cause of SARS [6,7]. SARS can occur in both healthy individuals and those with chronic illness including end-stage renal failure. Clinical information of SARS on renal dialysis patients is lacking. We describe here two end-stage renal failure patients, both receiving chronic haemodialysis, who acquired the disease after contact with SARS patients and had different outcomes.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "A 49-year-old male, who had end-stage renal failure and was receiving chronic haemodialysis twice weekly, was admitted to the Princess Margaret Hospital on April 6, 2003, because of fever, chills, rigors and cough for 1 day. He also had myalgia, malaise, anorexia, headache and dizziness (Table 1). He had a history of contact with SARS patients. From March 31 to April 2, 2003, he was hospitalized in a medical ward of the Alice Ho Miu Ling Nethersole Hospital, where there was an outbreak of SARS, for haemophilus influenzae pneumonia. Four days after discharge from that hospital (day 1), he began to run a fever and was admitted into the Princess Margaret Hospital the following day (day 2). After admission, serial chest radiographs from days 2 to 6 showed no consolidation (Figure 1A). His laboratory data at presentation revealed lymphopaenia with a normal total white cell count, mild thrombocytopaenia and an elevated C-reactive protein level (Table 2). Other laboratory results are shown in Table 2. His liver enzymes, lactate dehydrogenase and creatine phosphokinase were normal. After admission, he was started on broad-spectrum antibiotic therapy with levofloxacin. However, his fever persisted with a highest temperature of 39.5\u00b0C. In view of his contact history with SARS patients, he was put on anti-viral therapy with i.v. ribavirin on day 5. He was given 900 mg (20 mg/kg) as a loading dose, followed by 450 mg (10 mg/kg) every 24 h. In view of the negative chest radiographs, a high-resolution CT scan of the thorax was performed on day 7 (Figure 1B), which revealed patchy consolidation predominantly over the superior and posterior left lower lobe, small areas of consolidation and ground-glass shadowing over the left lingular lobe, anterior, posterior and lateral right lower lobe. His chest radiograph then began to deteriorate to a maximum degree on day 11 with involvement of the right lower zone, left middle zone and left lower zone (Figure 1C and D), associated clinically with dyspnoea. The patient was started on i.v. pulse methylprednisolone on day 7 after a CT scan of the thorax revealed bilateral involvement of the lungs. Methylprednisolone 0.5 g daily was given from days 7 to 9, then 1 g daily was given on days 10, 12 and 13. A total of 4.5 g of methylprednisolone was given over 7 days. His fever began to subside on day 13 and his lung condition improved with a decreasing intensity of consolidation (Figure 1E). Intubation was not required even during maximum deterioration of his pneumonia. The reverse-transcriptase polymerase chain reaction (RT\u2013PCR) assay for coronavirus of his throat and nasal swabs and stool specimens were all positive. He was discharged on day 25 after recovering from SARS. Eighteen days of ribavirin and 26 days of steroids including methylprednisolone were given during the course of his illness. The patient\u2019s serum coronavirus antibody titre showed a >8-fold rise during the convalescent phase at 5 weeks (from <1/25 to 1/200).\n",
            "cite_spans": [],
            "section": "Patient 1 ::: Cases",
            "ref_spans": []
        },
        {
            "text": "An 86-year-old male, who had end-stage renal failure secondary to diabetic nephropathy, had been receiving chronic haemodialysis thrice weekly for 3 years. He had other co-morbid diseases including diabetes mellitus, hypertension, ischaemic heart disease, a history of cerebral infarction and thalassaemia minor. He was admitted to the Princess Margaret Hospital on March 31, 2003. He had a history of close contact with SARS patients, his two Fhilipino maids, who took care of the patient. Ten days before his admission, his two maids were admitted into the hospital because of high fever and were later confirmed to have SARS. One day after admission (day 1), the patient began to develop a low-grade fever of 37.6\u00b0C. Apart from the low-grade fever, he had no chills, rigors, cough, sputum, haemoptysis, shortness of breath, myalgia, malaise, diarrhoea or headache (Table 1). Laboratory data revealed lymphopaenia with a normal total white cell count and an elevated lactate dehydrogenase level (Table 2). Other laboratory results are shown in Table 2. His chest radiograph on day 1 showed mild right upper zone haziness. He was covered with broad-spectrum antibiotic therapy with levofloxacin. In view of the strong contact history, he was started on i.v. ribavirin 400 mg (6.5 mg/kg) every 24 h on day 2. Despite ribavirin treatment, the patient ran a persistently low-grade fever and a chest radiograph on day 21 revealed new shadows over the right lower zone. The antibiotic was then changed to imipenem. A chest radiograph taken on day 26 showed new consolidation over the left middle zone. I.v. hydrocortisone 100 mg every 6 h was started on the same day. However, 2 days later (day 28) the patient developed cardiac arrest and succumbed. Throughout his course in hospital, the patient\u2019s fever was low grade, with a highest temperature of 38\u00b0C. The RT\u2013PCR assay for coronavirus of his throat and nasal swabs was negative on day 14 but a second specimen taken on day 24 became positive. The patient\u2019s serum coronavirus antibody titre checked on days 14 and 24 showed no rise (both titres were <1/25). However, the two serum specimens were taken only 11 days apart.",
            "cite_spans": [],
            "section": "Patient 2 ::: Cases",
            "ref_spans": []
        },
        {
            "text": "We report two cases of SARS occurring in end-stage renal failure patients. Both patients were receiving chronic haemodialysis. Apart from renal failure, patient 2 also had other multiple co-morbid diseases. The incubation period for SARS in patient 1 was 4\u20137 days, while that in patient 2 was \u223c11 days. Patient 1 had typical presentation of SARS with high fever (temperature >38\u00b0C), cough, chills, rigors, myalgia, malaise and headache. However, patient 2 was totally asymptomatic except for a low-grade fever of 37.6\u00b0C at presentation. The maximum temperature throughout his stay in hospital was only 38\u00b0C. The diagnosis of SARS was based on his strong contact history, chest radiograph shadows and the second RT\u2013PCR assay for coronavirus of his throat and nasal swabs on day 24. Both cases had lymphopaenia with a normal total white cell count at presentation. Patient 1 had a positive RT\u2013PCR assay for coronavirus of the throat and nasal swabs. The RT\u2013PCR assay of his stool specimen was also positive, despite the absence of diarrhoea. The first throat and nasal swabs of patient 2 for coronavirus RT\u2013PCR assay on day 14 was negative, while a second specimen on day 24 was positive. This may be due to the fact that throat and nasal swabs contain considerably less viral RNA than sputum, and the virus may escape detection [7]. Patient 1 demonstrated a >8-fold rise in coronavirus antibody titre during the convalescent phase, confirming a recent coronavirus infection.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Both patients had been treated with i.v. ribavirin, which was used as anti-viral therapy. The dose of ribavirin used ranged from 6.5 to 10 mg/kg every 24 h. However, the optimal dose for treating SARS in patient with end-stage renal failure and in those undergoing haemodialysis is not known. The use of i.v. pulse methylprednisolone during the phase of deterioration seemed to benefit our first patient, leading to improvement of pulmonary infiltrates. The total dose of methylprednisolone used was 4.5 g. Pulse methylprednisolone was not given in patient 2 in view of his advanced age and multiple co-morbidities. The first patient recovered after a maximum deterioration 11 days from the onset of symptoms, whereas the second patient died as a result of SARS.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In summary, the experiences in our two cases show that SARS, in end-stage renal failure patients, may have an atypical presentation. They can present with minimal symptoms, e.g. low-grade fever only, leading to diagnostic difficulty. The optimal ribavirin dose for treating SARS in patients with end-stage renal failure and in those receiving haemodialysis is unknown. Further studies are warranted to clarify these. I.v. pulse methylprednisolone given at the phase of deterioration seems to be of benefit. One patient died of SARS and the other recovered from the disease.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1.: Symptoms of two haemodialysis patients with SARS at presentation\n",
            "type": "table"
        },
        "TABREF1": {
            "text": "Table 2.: Laboratory data of two haemodialysis patients with SARS at presentation\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig. 1.: Radiographic images of patient 1. (A) Chest radiograph on day 6 after onset of symptoms showed no consolidation. (B) High-resolution CT scan of the thorax on day 7 showing patchy consolidation and ground-glass shadowing in both the left and right lower lobes. (C) Chest radiograph on day 9 began to reveal air-space shadowing over the left middle zone and right lower zone. (D) Chest radiograph during maximum deterioration (day 11) showing extensive pulmonary infiltrates over bilateral lung fields. (E) Chest radiograph on day 20 showing improvement of lung shadows after therapy.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}