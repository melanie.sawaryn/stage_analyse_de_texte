{
    "paper_id": "PMC7119913",
    "metadata": {
        "title": "Human Coronaviruses",
        "authors": [
            {
                "first": "Margret",
                "middle": [],
                "last": "Schuller",
                "suffix": "",
                "email": "mschuller@profitbd.com",
                "affiliation": {}
            },
            {
                "first": "Theo",
                "middle": [
                    "P."
                ],
                "last": "Sloots",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Gregory",
                "middle": [
                    "S."
                ],
                "last": "James",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Catriona",
                "middle": [
                    "L."
                ],
                "last": "Halliday",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ian",
                "middle": [
                    "W.J."
                ],
                "last": "Carter",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rebecca",
                "middle": [],
                "last": "Rockett",
                "suffix": "",
                "email": "r.rockett@uq.edu.au",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Human Coronaviruses (HCoVs) are recognised to be an important cause of the common cold. In 1962 HCoV-229E and HCoV-OC43 where first recognised, more recently HCoV-NL63 and HCoV-HKU-1 have been discovered in respiratory specimens from children and adults [3]. This protocol describes two real-time reverse-transcriptase polymerase chain reaction (RT-PCR) methods, a single target and triplex RT-PCR that identifies and differentiates HCoV infection.",
            "cite_spans": [
                {
                    "start": 255,
                    "end": 256,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Summary of Methods",
            "ref_spans": []
        },
        {
            "text": "Respiratory specimens (Nasopharyngeal aspirates, nose and throat swabs and bronchial samples).",
            "cite_spans": [],
            "section": "Acceptable Specimens",
            "ref_spans": []
        },
        {
            "text": "Respiratory specimens (200 \u03bcl) were extracted using the QIAGEN QIAxtractor, as per manufacturer\u2019s instructions giving a final volume of 50 \u03bcl.",
            "cite_spans": [],
            "section": "Sample Extraction",
            "ref_spans": []
        },
        {
            "text": "Primers and probe targets were obtained from previously published articles [1\u20133] (Table 42.1).\n\n",
            "cite_spans": [
                {
                    "start": 76,
                    "end": 77,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 78,
                    "end": 79,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Primer and Probe Sequences",
            "ref_spans": [
                {
                    "start": 88,
                    "end": 92,
                    "mention": "42.1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The first assay contains a single target for HCoV-HKU1, the conserved primer and probe sequences are found in the replicase 1b gene. The second assay detects three HCoV strains, HCoV-229E, HCoV-OC43 both contain target sequences in the N gene, and HCoV-NL63 which target sequences are in the 1a gene (Table 42.1). Both assays are performed using the QIAGEN One-step RT-PCR kit (QIAGEN, Australia) comprising 0.8 \u03bcM of forward and reverse primers (HCoV-HKU-1-F and HCoV-HKU1-R for the single target PCR and the triplex HCoV-229E-F, HCoV-229E-R, HCoV-OC43-F, HCoV-OC43-R, HCoV-NL63-F and HCoV-NL63-R) (Table 42.1), 0.2 \u03bcM of TaqMan probe (single target PCR, HCoV-HKU1-Pr and in the triplex HCoV-229E-Pr, HCoV-OC43-Pr and HCoV-NL63-Pr) (Table 42.1), in a total reaction volume of 25 \u03bcl including 5 \u03bcl of sample RNA. Both amplification reactions were performed on the RotorGene 3000 or 6000 (QIAGEN, Australia) using the following parameters; initial RT incubation of 20 min at 50\u00b0C, followed by 50 cycles of 95\u00b0C for 15 s, and 60\u00b0C for 1 min, with fluorescence acquired at the end of each 60\u00b0C step.",
            "cite_spans": [],
            "section": "PCR Amplification and Product Detection",
            "ref_spans": [
                {
                    "start": 307,
                    "end": 311,
                    "mention": "42.1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 606,
                    "end": 610,
                    "mention": "42.1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 740,
                    "end": 744,
                    "mention": "42.1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The single target HCoV-HKU1 assay was validated using 31 clinical specimens positive for HCoV, detected using various different techniques. No cross reaction was detected to samples positive for other respiratory pathogens. Sensitivity was measured by serial titration, 15 replicates where tested with 100% detection at 50 copies and 33.3% at 5 copies [1].",
            "cite_spans": [
                {
                    "start": 353,
                    "end": 354,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Quality Control and Validation Data",
            "ref_spans": []
        },
        {
            "text": "The HCoV triplex was validated by ATCC positive specimens for each strain. Each positive was tested as an individually target and in the triplex reaction with no loss of sensitivity or cross reaction between strains [2].",
            "cite_spans": [
                {
                    "start": 217,
                    "end": 218,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Quality Control and Validation Data",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 42.1: Sequence details of primers and probes, used for the detection of human coronaviruses by real-time PCR\n",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Human coronavirus infections in rural Thailand: a comprehensive study using real-time reverse-transcription polymerase chain reaction assays",
            "authors": [
                {
                    "first": "RK",
                    "middle": [],
                    "last": "Dare",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Fry",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Chittaganpitch",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Infect Dis",
            "volume": "196",
            "issn": "",
            "pages": "1321-1328",
            "other_ids": {
                "DOI": [
                    "10.1086/521308"
                ]
            }
        },
        "BIBREF1": {
            "title": "Real-time RT-PCR detection of 12 respiratory viral infections in four triplex reactions",
            "authors": [
                {
                    "first": "RN",
                    "middle": [],
                    "last": "Gunson",
                    "suffix": ""
                },
                {
                    "first": "TC",
                    "middle": [],
                    "last": "Collins",
                    "suffix": ""
                },
                {
                    "first": "WF",
                    "middle": [],
                    "last": "Carman",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Clin Virol",
            "volume": "33",
            "issn": "",
            "pages": "341-344",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcv.2004.11.025"
                ]
            }
        },
        "BIBREF2": {
            "title": "Frequent Detection of human coronaviruses in clinical specimens from patients with respiratory tract infection by use of a novel real-time reverse-transcriptase polymerase chain reaction",
            "authors": [
                {
                    "first": "LJR",
                    "middle": [],
                    "last": "van Elden",
                    "suffix": ""
                },
                {
                    "first": "AM",
                    "middle": [],
                    "last": "van Loon",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "van Alphen",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Infect Dis",
            "volume": "189",
            "issn": "",
            "pages": "652-657",
            "other_ids": {
                "DOI": [
                    "10.1086/381207"
                ]
            }
        }
    }
}