{
    "paper_id": "PMC6106426",
    "metadata": {
        "title": "Event-Based Surveillance at Community and Healthcare Facilities, Vietnam, 2016\u20132017",
        "authors": [
            {
                "first": "Alexey",
                "middle": [],
                "last": "Clara",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Trang",
                "middle": [
                    "T."
                ],
                "last": "Do",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anh",
                "middle": [
                    "T.P."
                ],
                "last": "Dao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Phu",
                "middle": [
                    "D."
                ],
                "last": "Tran",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tan",
                "middle": [
                    "Q."
                ],
                "last": "Dang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Quang",
                "middle": [
                    "D."
                ],
                "last": "Tran",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Nghia",
                "middle": [
                    "D."
                ],
                "last": "Ngu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tu",
                "middle": [
                    "H."
                ],
                "last": "Ngo",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hung",
                "middle": [
                    "C."
                ],
                "last": "Phan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Thuy",
                "middle": [
                    "T.P."
                ],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anh",
                "middle": [
                    "T."
                ],
                "last": "Lai",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Dung",
                "middle": [
                    "T."
                ],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "My",
                "middle": [
                    "K."
                ],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hieu",
                "middle": [
                    "T.M."
                ],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Steven",
                "middle": [],
                "last": "Becknell",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Christina",
                "middle": [],
                "last": "Bernadotte",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Huyen",
                "middle": [
                    "T."
                ],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Quoc",
                "middle": [
                    "C."
                ],
                "last": "Nguyen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Anthony",
                "middle": [
                    "W."
                ],
                "last": "Mounts",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "S.",
                "middle": [
                    "Arunmozhi"
                ],
                "last": "Balajee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The GDPM formed an EBS Technical Working Group (TWG) consisting of stakeholders from the Ministry of Health, including the 2 Regional Institutes, PATH (an international organization), CDC, WHO, and technical staff from the pilot province PPMC. In addition to guiding the EBS planning and preparations, the TWG served as the advisory group for implementation throughout the project. TWG members also served on an assessment team and later assisted in disseminating the assessment results to stakeholders.",
            "cite_spans": [],
            "section": "Establishing a Technical Working Group for EBS ::: Methods",
            "ref_spans": []
        },
        {
            "text": "EBS signals do not need to be disease specific. However, to reduce the background noise and to provide a framework for reporting, the TWG listed priority diseases and conditions that were important for early detection in Vietnam. Criteria for inclusion included diseases that 1) have large public health impact in the country, 2) are outbreak prone and pose a major public health threat, 3) have previously been prevalent and might reemerge, and 4) are slated for eradication or elimination. High-priority diseases identified were rabies, avian influenza, vaccine-preventable diseases, cholera, and emerging new diseases.",
            "cite_spans": [],
            "section": "Establishing a Technical Working Group for EBS ::: Methods",
            "ref_spans": []
        },
        {
            "text": "The TWG then drafted a list of signals that could serve as an early indication of the appearance of these priority diseases in the community. Community signals represented constellations of symptoms and patterns that do not require specialized healthcare training; signals aimed at HCF were based on unusual occurrences and/or disease patterns, such as surge in admissions.",
            "cite_spans": [],
            "section": "Establishing a Technical Working Group for EBS ::: Methods",
            "ref_spans": []
        },
        {
            "text": "The TWG drafted an Interim Technical Implementation Guideline and training materials (8). Other materials included posters and flyers to increase community awareness of the signals and need to report, notebooks for VHWs with printed signals and pages for notes, logbooks for recording signals, and a monitoring checklist for supervisory visits at each administrative level.",
            "cite_spans": [
                {
                    "start": 86,
                    "end": 87,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Establishing a Technical Working Group for EBS ::: Methods",
            "ref_spans": []
        },
        {
            "text": "A training of trainers workshop was conducted for the Regional Institutes and pilot provinces. These participants became master trainers and led cascade trainings in each province down to the commune level. At each level, a trainer from a higher administrative level provided mentorship and support.",
            "cite_spans": [],
            "section": "Training the Public Health Workforce in EBS ::: Methods",
            "ref_spans": []
        },
        {
            "text": "In addition to external funding for training, each province received a one-time start-up grant for infrastructural improvements, including purchase of a limited number of computers for reporting, a one-time allowance for VHW cellular phone minutes, and the printing and distribution of logbooks and communication materials. During the pilot phase, EBS district and provincial focal points received a small monthly honorarium for EBS oversight and support.",
            "cite_spans": [],
            "section": "Resources for Implementation Support ::: Methods",
            "ref_spans": []
        },
        {
            "text": "For EBS, the existing organizational structure and information flow from CHS to DHC to PPMC and to Regional Institutes was maintained with some enhancements (Figure 1), including 1) inclusion of VHWs at the CHS to identify and report signals; 2) addition of a triage step (the CHS decided which signals were \u201ctrue\u201d signals [rather than a spurious situation or nonthreatening rumor] before reporting these as events to DHCs); 3) training of DHCs and PPMCs in event verification and risk assessment; 4) distribution of logbooks for recording signals and events; 5) establishment of a requirement to immediately report events by phone call, in-person meeting, or email; and 6) training of healthcare providers to detect and immediately report signals to the correct public health unit.",
            "cite_spans": [],
            "section": "Enhancing Existing Information Flow and EBS Reporting ::: Methods",
            "ref_spans": [
                {
                    "start": 158,
                    "end": 166,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Approximately 9 months after launch, the TWG assessed the EBS pilot, with qualitative and quantitative methods, for timeliness of detection and reporting of events, as well as EBS acceptability and sustainability at all levels. This assessment included 1) a retrospective data collection table sent electronically to all districts to collect logbook time stamps for event notification and response, 2) questionnaires sent electronically to all levels with acceptability and sustainability related questions, and 3) key informant interviews and focus group discussions through field visits.",
            "cite_spans": [],
            "section": "Assessing the EBS Pilot ::: Methods",
            "ref_spans": []
        },
        {
            "text": "We used 3 criteria to select field visit sites. First, we assessed districts that were performing optimally and suboptimally as defined by the metric signal incidence rate. Signal incidence rates were the number of signals detected from each district, adjusted by the district\u2019s population and the number of days engaged in signal reporting. We defined optimal performance as districts with a signal incidence rate higher than the 50th percentile and suboptimal performance districts as districts with a signal incidence rate of the 50th percentile or lower. Second, we selected districts that investigated public health events reported through EBS that could be useful case studies. Third, we selected sites that were willing to receive assessors.",
            "cite_spans": [],
            "section": "Assessing the EBS Pilot ::: Methods",
            "ref_spans": []
        },
        {
            "text": "We sent the time stamp data extraction form to all 43 pilot districts. Approximately 7,000 participants encompassing EBS focal points and volunteers at all levels of the workforce in all 4 provinces received the acceptability/sustainability survey. In each province, 2 districts and 2 CHSs per district were selected for site visits and key informant interviews/focus group discussions deployment.",
            "cite_spans": [],
            "section": "Assessing the EBS Pilot ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Twenty-four master trainers were trained in August 2016: two from each province and 16 GDPM and Regional Institute staff. A cascade training to lower administrative levels followed the master training. By October 2016, >7,000 persons in 4 provinces were trained to detect, record, and report signals and events, and 52 DHC staff were trained in basic risk assessment. Staff from every district, CHS, and public hospital within each province were trained, achieving 100% training coverage (Table 2).",
            "cite_spans": [],
            "section": "Resources and EBS Workforce ::: Results",
            "ref_spans": [
                {
                    "start": 489,
                    "end": 496,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "At least 15,000 posters with community signals and reporting information were provided to CHSs (Figure 3). These posters were prominently displayed at public meeting places, CHS, village meetings, and other highly visible locations. In addition, 1,300 logbooks and 703,000 leaflets for the community were distributed (Table 3).",
            "cite_spans": [],
            "section": "Resources and EBS Workforce ::: Results",
            "ref_spans": [
                {
                    "start": 96,
                    "end": 104,
                    "mention": "Figure 3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 318,
                    "end": 325,
                    "mention": "Table 3",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "As of July 1, 2017, we received 2,105 acceptability/sustainability surveys from 5 PPMC staff, 39 DHCs, 428 CHSs, and 1,633 VHWs. Twenty-four (56%) of 43 districts returned the timeliness data extraction forms. We conducted 34 key informant interviews, and 32 focus group discussions, both totaling 160 participants (Figure 4).",
            "cite_spans": [],
            "section": "EBS Pilot Assessment ::: Results",
            "ref_spans": [
                {
                    "start": 316,
                    "end": 324,
                    "mention": "Figure 4",
                    "ref_id": "FIGREF3"
                }
            ]
        },
        {
            "text": "During September 2016\u2013May 2017, CHSs reported 2,520 signals to the districts (Figure 5). Quang Ninh province reported the largest number of signals. Of all 2,520 signals, 176 (7%) were verified as events by the districts and were responded to by the DHC or PPMC.",
            "cite_spans": [],
            "section": "EBS Pilot Assessment ::: Results",
            "ref_spans": [
                {
                    "start": 78,
                    "end": 86,
                    "mention": "Figure 5",
                    "ref_id": "FIGREF4"
                }
            ]
        },
        {
            "text": "Although no preexisting timeline data were available for comparison, the pilot demonstrated that the mean times from detection to notification and detection to response were within 24 hours and 48 hours, respectively (Table 4) (10). We identified a case study illustrating the value of early event detection resulting in timely response (Figure 6). A trained VHW learned that diarrhea and vomiting developed in 2 persons who had attended a wedding party meal on September 25, 2016, at \u224813:00 hrs. The VHW called the CHS and reported the signal 30 minutes after learning of the episode. The CHS EBS focal point visited the village and, after confirming the signal, immediately reported to the DHC EBS focal point, who joined the CHS team. The investigation found 93 other affected persons, 38 of whom were hospitalized. The DHC reported the event to the PPMC, which conducted a risk assessment classifying the event as high risk and launched a response the same day. The time to notification to the DHC was within 30 minutes, and the time to response was within 3 hours.",
            "cite_spans": [
                {
                    "start": 228,
                    "end": 230,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "EBS Pilot Assessment ::: Results",
            "ref_spans": [
                {
                    "start": 338,
                    "end": 346,
                    "mention": "Figure 6",
                    "ref_id": "FIGREF5"
                },
                {
                    "start": 218,
                    "end": 225,
                    "mention": "Table 4",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "At the community level, signals were being recognized and reported from multiple sources. The most frequent EBS reporters were VHWs, teachers, community members, traditional healers, veterinarians, and representatives from industrial complexes (Figure 7). Reported events included multiple suspected avian influenza poultry die-offs and human outbreaks of chickenpox, mumps, and foodborne disease.",
            "cite_spans": [],
            "section": "EBS Pilot Assessment ::: Results",
            "ref_spans": [
                {
                    "start": 245,
                    "end": 253,
                    "mention": "Figure 7",
                    "ref_id": "FIGREF6"
                }
            ]
        },
        {
            "text": "During the key informant interviews and focus group discussions, interviewees reported that the signal language should be further simplified, including alternatives for medical terms such \u201csevere,\u201d \u201cdehydration,\u201d and \u201ccomplications.\u201d Furthermore, some of the guideline language was deemed overly academic and needed to reflect everyday language. Most interviewees appreciated the illustrations in the posters and leaflets and noted their usefulness in areas that included ethnic minority populations that did not read Vietnamese.",
            "cite_spans": [],
            "section": "EBS Pilot Assessment ::: Results",
            "ref_spans": []
        },
        {
            "text": "A total of 82%\u201388% of VHW, CHS, and district respondents reported that EBS is very important in detecting public health events and helps to detect public health events earlier than before (Table 5). In addition, \u224885% of VHW and CHS respondents and 77% of district respondents said they were willing to continue participating in EBS. Data collected during field visits substantiated these results (data not shown).",
            "cite_spans": [],
            "section": "EBS Pilot Assessment ::: Results",
            "ref_spans": [
                {
                    "start": 189,
                    "end": 196,
                    "mention": "Table 5",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Key motivating factors for participation expressed by the VHWs were a sense of service to the community, opportunities to increase community ties, and improvement in community trust. Some VHWs also said that the EBS project better defined their responsibilities. Staff reported that the EBS project increased communications between different levels of the public health system, which aided in early detection of events and outbreaks.",
            "cite_spans": [],
            "section": "EBS Pilot Assessment ::: Results",
            "ref_spans": []
        },
        {
            "text": "The EBS pilot project builds on and expands the existing surveillance system in Vietnam to include community and HCF event-based surveillance. The pilot EBS implementation in Vietnam demonstrated earlier detection and reporting of outbreaks, improved collaboration among HCFs, the preventive health and animal health sectors of the government, and increased participation of communities in surveillance and reporting. Thus, EBS implementation contributes to Vietnam\u2019s compliance with IHR 2005, thereby enhancing global health security.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The pilot initiative trained an existing network of VHWs and health collaborators to increase their awareness to look for and report signals as they appear in the community and to improve their understanding of patterns of disease that could signal the start of an outbreak. In most communes, the CHSs also recruited and trained additional community members as health collaborators through the current project. Most were persons with strong community ties, including money lenders, insurance agents, veterinary health staff, landlords, factory managers, community leaders, and others in a good position to directly observe community events. This wide participation broadened the sources of reporting and resulted in the reporting of numerous signals that otherwise would have been missed, such as school absenteeism reported by teachers and the resulting multiple detections of vaccine-preventable diseases (e.g., mumps and chickenpox). In contrast to reporting by clinicians from HCFs, VHWs recognized connections between cases in the community that doctors can miss, such as clusters among neighbors, co-workers, or persons with social connections.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The system did not rely on data reporting, aggregation, and analysis but rather used direct reporting methods to existing district and provincial authorities responsible for outbreak response. Based on the pilot implementation of EBS, it is plausible that focusing on patterns of occurrence in the community enabled outbreaks to be detected before they were large enough for HCFs to notice. Although all district and provincial public hospitals reported, no private hospitals and clinics participated in the EBS, making community-level participation critical to the detection process.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In the pilot districts, all events were detected and reported within 48 hours, and response was timely. Before EBS, such a rapid response by DHCs would not have been possible because ill persons would have to have been hospitalized to alert the system and, for certain diseases, traditional reporting often bypassed the CHSs. For example, foodborne illness events would first have to be reported to the Department of Food Safety and Hygiene, rather than the CHS, and ultimately to the DHC, resulting in delays. Similarly, animal events such as poultry die-offs or rabid dogs previously would have been reported to the Animal Health Department, and human health officials would not necessarily be alerted. During field visits, the DHC staff stated that because of the EBS pilot, multisectoral communication, such as between food safety and public health and human and animal health sectors, improved substantially.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The greatest challenge in quantifying EBS impact was lack of baseline outbreak data. Although Circular 54 requires outbreak reporting through eCDS, outbreak reports are not recorded even if detected, and therefore baseline data were not available. However, the absence of preexisting data demonstrates another important EBS contribution: the availability of data on outbreaks and events for planning public health interventions.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The assessment was an important part of the pilot and highlighted several problems that had to be rectified. Specifically, for some signals, wording needed to be simplified for VHWs, and the signal list itself needed to be more concise. In addition, for some diseases, such as hand, foot, and mouth disease, ongoing surveillance requires reporting of every case rather than clusters, creating some confusion. In some jurisdictions, leadership decided unilaterally to broaden signals to include single case reports, whereas the signal had been defined as a cluster, increasing the system\u2019s sensitivity, but with a very low specificity. This change resulted in only 7% of all signals becoming public health events. In the future, adherence to accepted signal definitions by the workforce can be maintained with continuous training and experience. Based on the assessment, the guidelines and training materials were revised and will undergo pilot testing before scale-up (Table 6).",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 969,
                    "end": 976,
                    "mention": "Table 6",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Another challenge was the number of respondents to the online survey. The online acceptability survey was sent to the entire EBS workforce in the pilot provinces, but GDPM closed the survey after only 3 weeks. Thus, only a relatively small proportion (25%) of VHWs respondents were able to complete the survey, which might have limited the representativeness of some of the survey findings.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Despite the above limitations, experience gained through the pilot project in Vietnam might be useful for other countries looking to launch EBS. To that end, we recommend the following:",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Based on the experience gained by the initial EBS pilot project, the Vietnam Ministry of Health expanded the pilot to 2 new provinces in the central and highlands areas. The TWG revised training materials based on the findings of a final assessment and drafted with GDPM a decision letter to formally integrate EBS into the national surveillance system. The vice minister of health issued a mandate in March 2018 that directed all provinces to integrate event-based surveillance into the national surveillance strategy, ensuring sustainability of the CEBS program. The formalization of EBS as a Ministry of Health regulation will enable the provinces to seek funds in the provincial budget to support EBS. With the Ministry of Health mandate, revised EBS materials, and experience gained by launching an EBS pilot, Vietnam\u2019s surveillance system will soon function as an effective early warning and response system.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Existing surveillance and reporting system improved for event-based surveillance, Vietnam, September 2016\u2013May 2017. Enhancements are shown in dashed boxes; the reporting tools at each level are shown in gray dashed boxes.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Provinces participating in event-based surveillance pilot project (stars), Vietnam, September 2016\u2013May 2017.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3: Poster displaying community-level signals for pilot of event-based surveillance, Vietnam, September 2016\u2013May 2017.",
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Figure 4: Assessment tools deployed at each site for assessment of an EBS pilot project, Vietnam, September 2016\u2013May 2017. Acceptability survey and time stamp events tool were sent to all districts in the pilot provinces, not only to the sites selected for the FGD and KII site visits. CHS, commune health station; EBS, event-based surveillance; FGD, focus group discussion; FP, focal point; KII, key informant interview; POC, point of contact; PPMC, Provincial Preventive Medicine Center; VHW, village health worker.",
            "type": "figure"
        },
        "FIGREF4": {
            "text": "Figure 5: Number of signals reported to districts in the 4 event-based surveillance pilot provinces, Vietnam. Data were collected from the district monthly summary report during September 2016\u2013May 2017.",
            "type": "figure"
        },
        "FIGREF5": {
            "text": "Figure 6: Case study of a cluster of food poisoning illustrating the value of EBS in early detection leading to rapid response, Dai Thang commune, Vu ban District, Nam Dinh Province, Vietnam, September 2016. CHS, Commune Health Station; DHC, District Health Center; DPMC, District Preventive Medicine Center; EBS, event-based surveillance; PPMC, Provincial Preventive Medicine Center; VHW, village health worker.",
            "type": "figure"
        },
        "FIGREF6": {
            "text": "Figure 7: Sources contributing to signal detection and reporting through EBS at the community level in pilot provinces, Vietnam, September 2016\u2013May 2017. Data were extracted from 428 acceptability survey questionnaires completed by Commune Health Station EBS focal points in July 2017. Each bar represents the number of survey respondents who identified the information source as contributing to EBS within the last 4 weeks. EBS, event-based surveillance.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2006,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 2017,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Global health security: building capacities for early event detection, epidemiologic workforce, and laboratory response.",
            "authors": [],
            "year": 2016,
            "venue": "Health Secur",
            "volume": "14",
            "issn": "",
            "pages": "424-32",
            "other_ids": {
                "DOI": [
                    "10.1089/hs.2015.0062"
                ]
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2016,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 2016,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}