{
    "paper_id": "PMC7107972",
    "metadata": {
        "title": "In the Literature",
        "authors": [
            {
                "first": "Stan",
                "middle": [],
                "last": "Deresinski",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Allander T, Tammi MT, Eriksson M, et al. Cloning of a human parvovirus by molecular screening of respiratory tract samples. Proc Natl Acad Sci U S A 2005; 102:12891\u20136.",
            "cite_spans": [],
            "section": "Hbov, A Newly Identified Parvovirus (Bocavirus) That May Cause Respiratory Tract Infection in Humans",
            "ref_spans": []
        },
        {
            "text": "Despite application of multiple diagnostic methods, it has proven impossible to identify an etiologic pathogen in a significant proportion of patients with febrile respiratory illness. This problem is slowly being chipped away at by investigators using sophisticated molecular techniques.",
            "cite_spans": [],
            "section": "Hbov, A Newly Identified Parvovirus (Bocavirus) That May Cause Respiratory Tract Infection in Humans",
            "ref_spans": []
        },
        {
            "text": "A large number of nasopharyngeal aspirates submitted to the Karolinska University Laboratory were examined for the presence of novel viral species by a procedure involving depletion of host DNA, random PCR amplification, large-scale sequencing, and bioinformatics analysis. This led to the detection of a coronavirus with sequence similar to that of the recently described CoV-HKU1 [1], as well as a novel parvovirus that had a deduced amino acid sequence similar to members of the genus bocavirus. Bocaviruses are members of the family Parvoviridae, subfamily Parvovirinae; previously identified members of this genus are pathogens of bovines and canines. The newly identified parvovirus of human origin was given the appellation \u201cHBoV.\u201d",
            "cite_spans": [
                {
                    "start": 383,
                    "end": 384,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Hbov, A Newly Identified Parvovirus (Bocavirus) That May Cause Respiratory Tract Infection in Humans",
            "ref_spans": []
        },
        {
            "text": "HBoV was subsequently detected in 7 (2.6%) of 266 pediatric outpatients in culture-negative nasopharyngeal samples submitted to the laboratory over an 11-month period; the virus was not detected in any of 112 samples obtained from adults. In addition, it was identified in nasopharyngeal specimens obtained from 17 (3.1%) of 540 hospitalized patients that were submitted to the diagnostic laboratory, and in 3 of these samples, it was the only pathogen identified. The infection rate peaked during the Swedish winter.",
            "cite_spans": [],
            "section": "Hbov, A Newly Identified Parvovirus (Bocavirus) That May Cause Respiratory Tract Infection in Humans",
            "ref_spans": []
        },
        {
            "text": "All 14 inpatients with HBoV as the sole respiratory isolate were children, each of whom had been admitted with respiratory distress. Seven had a history of wheezing and were receiving inhaled bronchodilators and corticosteroids, 2 had previously been hospitalized for respiratory episodes with wheezing, 2 had been experiencing chronic lung disease since the neonatal period, and 5 had no previous history of chronic respiratory illness. Fever was \u201cprevalent,\u201d and 6 of the 7 children who underwent chest radiography had bilateral interstitial pulmonary infiltrates.",
            "cite_spans": [],
            "section": "Hbov, A Newly Identified Parvovirus (Bocavirus) That May Cause Respiratory Tract Infection in Humans",
            "ref_spans": []
        },
        {
            "text": "The family Parvoviridae comprises 2 subfamilies; these are the Densovirinae, which consist of arthropod pathogens, and Parvovirinae, made up of 5 genera (parvovirus, erythrovirus, dependovirus, amdovirus, and bocavirus). The type species of the Dependovirus subfamily are the adeno-associated viruses, which require helper viruses for replication and which are commonly utilized as vectors in gene therapy. Erythrovirus (parvovirus B19) is, of course, a well-known human pathogen. HBoV appears to be the second member of this family of DNA viruses that causes disease in humans. It should also be noted that an apparently novel parvovirus, PARV4, has recently been identified in the blood of 2 patients with febrile illnesses [2].",
            "cite_spans": [
                {
                    "start": 727,
                    "end": 728,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Hbov, A Newly Identified Parvovirus (Bocavirus) That May Cause Respiratory Tract Infection in Humans",
            "ref_spans": []
        },
        {
            "text": "Jones MS, Kapoor A, Lukashov VV, et al. New DNA viruses identified in patients with acute viral infection syndrome. J Virol 2005; 79:8230\u20136.",
            "cite_spans": [],
            "section": "Parv4, A Newly Identified Parvovirus Recovered From The Blood of A Febrile Patient",
            "ref_spans": []
        },
        {
            "text": "Plasma samples from 25 subjects (23 of whom were male) with acute viral syndrome who underwent screening for acute HIV infection (with negative results) were examined for the presence of viral nucleic acids using a sequence-independent PCR amplification method. This led to the identification of 3 DNA viruses that had not previously been identified. Two, SAV-1 and SAV-2, were viruses with circular DNA with regions of homology to the anelloviruses, TorqueTenoVirus (TTV) and TorqueTenoMiniVirus (TMV), which are present in high frequency in healthy blood donors. Examination of a plasma specimen from a third patient yielded a parvovirus, PARV4, that represented a distinct lineage between the adeno-associated viruses and BPV-3, which infects primates and chipmunks. The genetic distance of this virus from human parvovirus B19 indicates that the primers currently in use to detect the latter would not be capable of amplifying PARV4 DNA, an important issue both in clinical diagnostics and in blood-banking. However, the pathogenicity, if any, of PARV4, as well as of SAV-1 and SAV-2, remains to be determined. Another novel parvovirus, HBoV, has recently been identified in nasopharyngeal secretions from patients with acute respiratory tract infection [3].",
            "cite_spans": [
                {
                    "start": 1259,
                    "end": 1260,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Parv4, A Newly Identified Parvovirus Recovered From The Blood of A Febrile Patient",
            "ref_spans": []
        },
        {
            "text": "Woo PCY, Lau SKP, Chu CM, et al. Characterization and complete genome sequence of a novel coronavirus, coronavirus HKU1, from patients with pneumonia. J Virol 2005; 79:884\u201395.",
            "cite_spans": [],
            "section": "Cov-Hku1, A New Coronavirus Isolated From Patients With Pneumonia",
            "ref_spans": []
        },
        {
            "text": "In a search for novel respiratory tract pathogens, Woo and colleagues in Hong Kong examined nasopharyngeal secretions from symptomatic patients for evidence of coronavirus infection using RT-PCR of the pol gene with conserved primers followed by sequencing of the resulting amplicon. Although culture attempts were unsuccessful, a coronavirus was detected, in the absence of any other identified pathogen, in a specimen obtained from a 71-year-old man with chronic obstructive lung disease who had been hospitalized because of pneumonia in 2004. Phylogenetic analysis found that the virus, CoV-HKU1, is a new member of group 2 of the coronaviruses. CoV-HKU1 was present in concentrations of 8.5\u20139.6 \u00d7 106 copies/mL of nasopharyngeal secretions in the first week of illness, decreasing to 1.5 \u00d7 105 copies/mL in the second week and becoming undetectable thereafter. The illness was associated with increasing IgM and IgG antibody titers to recombinant nucleocapsid protein of CoV-HKU1.",
            "cite_spans": [],
            "section": "Cov-Hku1, A New Coronavirus Isolated From Patients With Pneumonia",
            "ref_spans": []
        },
        {
            "text": "The investigators then examined 400 nasopharyngeal aspirates collected during the severe acute respiratory syndrome (SARS) outbreak in 2003 and found one that was positive for CoV-HKU1 at a concentration of 1.13 copies/mL. The affected patient was a previously healthy woman with pneumonia of unknown etiology.",
            "cite_spans": [],
            "section": "Cov-Hku1, A New Coronavirus Isolated From Patients With Pneumonia",
            "ref_spans": []
        },
        {
            "text": "The coronaviruses that were previously known to cause illness in humans are HCoV-229E and HCoV-NL63 (both in group 1), HCoV-OC43 (like CoV-HKU1, a group 2 virus), and, in a group of its own, SARS-CoV. The latter, of course, causes severe respiratory infections, whereas the HCoV-229E and HCoV-Oc43 are frequent causes of winter colds. Although it also causes a mild respiratory illness, HCoV-NL63 has been linked to bronchiolitis and croup in children, and it has also been isolated from children with Kawasaki disease. Although CoV-HKU1 has only been identified in 2 patients, both were adults with pneumonia, suggesting the possibility that it may cause significant lower respiratory tract infection.",
            "cite_spans": [],
            "section": "Cov-Hku1, A New Coronavirus Isolated From Patients With Pneumonia",
            "ref_spans": []
        },
        {
            "text": "Taubenberger JK, Reid AH, Lourens R, et al. Characterization of the 1918 influenza virus polymerase genes. Nature 2005; 437:889\u201393.",
            "cite_spans": [],
            "section": "Looking Back May Provide A Hint of The Future: Lessons Learned From The 1918 Spanish Influenza Virus",
            "ref_spans": []
        },
        {
            "text": "Tumpey TM, Basler CF, Aguilar PV, et al. Characterization of the reconstructed 1918 Spanish influenza pandemic virus. Science 2005; 310:77\u201380.",
            "cite_spans": [],
            "section": "Looking Back May Provide A Hint of The Future: Lessons Learned From The 1918 Spanish Influenza Virus",
            "ref_spans": []
        },
        {
            "text": "Consistent with the hypothesis that the 1918 human influenza A virus emerged directly from an avian source, examination of the amino acid sequence of the polymerase protein sequences of the 1918 virus found variation from the avian consensus sequence at just 10 amino acids. Examination of synonymous differences, however, suggested some degree of evolutionary distance between the human and avian viruses. Examination of its complete genomic sequence by Taubenberger and colleagues, together with phylogenetic analysis, found that the 1918 human virus was \u201can entirely avian-like virus that adapted to humans.\u201d",
            "cite_spans": [],
            "section": "Looking Back May Provide A Hint of The Future: Lessons Learned From The 1918 Spanish Influenza Virus",
            "ref_spans": []
        },
        {
            "text": "Sequencing of the polymerase gene completed the task of sequencing all 8 viral RNA segments of the 1918 human influenza virus recovered from both archived postmortem tissues and tissues of an Alaskan person buried in permafrost in 1918. Using plasmid-based reverse genetic techniques, investigators have now generated complete infectious virus containing these 8 coding sequences. This virus demonstrated characteristics associated with high infectivity and virulence, including the ability to replicate in the absence of trypsin (indicating an ability to cleave and thus activate its hemagglutinin). Intranasal inoculation was lethal to mice, with virus having multiplied to very high titers in their lungs and causing severe histological changes consistent with those seen in human victims of the virus in 1918. The mouse virulence of the reconstructed 1918 virus was significantly greater than that observed with any other human influenza virus studied. Consistent with its origins\u2014and unlike most human influenza viruses\u2014the reconstructed virus was lethal to chicken embryos.",
            "cite_spans": [],
            "section": "Looking Back May Provide A Hint of The Future: Lessons Learned From The 1918 Spanish Influenza Virus",
            "ref_spans": []
        },
        {
            "text": "These data demonstrate the extraordinary virulence of the 1918 Spanish influenza pandemic virus. They also provide evidence that the virus arose directly from an avian influenza strain with limited evolution before initiation of the pandemic. The concern commonly expressed with regard to the potential of the current avian influenza epidemic to cause a human pandemic has been the potential occurrence of a reassortment event in a dually infected (with virulent avian and human influenza viruses) human or pig. The evidence discussed here with regard to the 1918 human influenza virus, however, demonstrates that virulence for humans, together with the capability of facile human-to-human transmission, may occur as the result of mutational events in the absence of reassortment.",
            "cite_spans": [],
            "section": "Looking Back May Provide A Hint of The Future: Lessons Learned From The 1918 Spanish Influenza Virus",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Characterization and complete genome sequence of a novel coronavirus, coronavirus HKU1, from patients with pneumonia",
            "authors": [
                {
                    "first": "PCY",
                    "middle": [],
                    "last": "Woo",
                    "suffix": ""
                },
                {
                    "first": "SKP",
                    "middle": [],
                    "last": "Lau",
                    "suffix": ""
                },
                {
                    "first": "CM",
                    "middle": [],
                    "last": "Chu",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "884-95",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "New DNA viruses identified in patients with acute viral infection syndrome",
            "authors": [
                {
                    "first": "MS",
                    "middle": [],
                    "last": "Jones",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kapoor",
                    "suffix": ""
                },
                {
                    "first": "VV",
                    "middle": [],
                    "last": "Lukashov",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "8230-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Cloning of a human parvovirus by molecular screening of respiratory tract samples",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Allander",
                    "suffix": ""
                },
                {
                    "first": "MT",
                    "middle": [],
                    "last": "Tammi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Eriksson",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "102",
            "issn": "",
            "pages": "12891-6",
            "other_ids": {
                "DOI": []
            }
        }
    }
}