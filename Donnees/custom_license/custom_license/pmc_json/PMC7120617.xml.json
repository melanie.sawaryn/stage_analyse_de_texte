{
    "paper_id": "PMC7120617",
    "metadata": {
        "title": "Multiprotein Complex Production in Insect Cells by Using Polyproteins",
        "authors": [
            {
                "first": "Yu",
                "middle": [
                    "Wai"
                ],
                "last": "Chen",
                "suffix": "",
                "email": "yu-wai.chen@kcl.ac.uk",
                "affiliation": {}
            },
            {
                "first": "Yan",
                "middle": [],
                "last": "Nie",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Itxaso",
                "middle": [],
                "last": "Bellon-Echeverria",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Simon",
                "middle": [],
                "last": "Trowitzsch",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Christoph",
                "middle": [],
                "last": "Bieniossek",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Imre",
                "middle": [],
                "last": "Berger",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Multiprotein complexes catalyze essential cellular activities. Studying their structure and function is an emerging focus of biological research. In most cases, recombinant production is required for obtaining sufficient amounts of homogenous material for detailed analyses [1]. MultiBac is an advanced expression system that has been designed for overexpressing multiprotein complexes in insect cells infected by a single composite multigene baculovirus. MultiBac has enabled production of many challenging multiprotein complexes, setting the stage to unlock their mechanism [2\u20134]. A bottleneck which can be encountered when many heterologous proteins are co-produced from individual expression cassettes derives from imbalanced expression levels of the individual proteins prohibiting proper complex assembly [3, 5]. Some subunits may be expressed stronger, others weaker, and occasionally one subunit is expressed at such a low level that it becomes detrimental to overall yield and a complex containing all desired subunits cannot be obtained.",
            "cite_spans": [
                {
                    "start": 275,
                    "end": 276,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 577,
                    "end": 578,
                    "mention": "2",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 579,
                    "end": 580,
                    "mention": "4",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 812,
                    "end": 813,
                    "mention": "3",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 815,
                    "end": 816,
                    "mention": "5",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "In order to balance expression levels and achieve properly assembled complexes with correct subunit stoichiometry, we have implemented a novel strategy based on polyproteins that are processed in vivo into individual subunits by a highly specific protease [3, 5] (Fig. 1). This approach derives from the strategy used by certain viruses such as Coronavirus to realize their proteome [6]. To facilitate polyprotein production with the MultiBac BEVS, new transfer plasmids have been created that rely on Tn7 transposon-mediated gene integration into the MultiBac baculovirus (Fig. 2). Other baculoviruses that are in use rely on homologous recombination for composite baculovirus generation (flashBAC from OET, BacVector series from Novagen, others). These baculoviruses can likewise be accessed for polyprotein expression by using the pOmni-PBac plasmid [7].\n\n\n",
            "cite_spans": [
                {
                    "start": 257,
                    "end": 258,
                    "mention": "3",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 260,
                    "end": 261,
                    "mention": "5",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 384,
                    "end": 385,
                    "mention": "6",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 854,
                    "end": 855,
                    "mention": "7",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Introduction",
            "ref_spans": [
                {
                    "start": 269,
                    "end": 270,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 579,
                    "end": 580,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "All polyprotein transfer plasmids contain the same expression cassette which encompasses a very late viral promoter (polyhedrin) followed by the gene encoding for NIa protease from tobacco etch virus (TEV) for subunit liberation, a short oligonucleotide sequence presenting a BstEII and an RsrII restriction endonuclease site, and finally a gene encoding for cyan fluorescent protein (CFP) for direct read-out of polyprotein expression. A TEV NIa protease cleavage site (tcs) is placed upstream of the CFP encoding gene (Fig. 2a). Heterologous genes of interest (GOIs) can be inserted into this polyprotein expression cassette by using the BstEII and RsrII sites and restriction\u2013ligation cloning (see\nNote\n1). Transfer plasmids are then used to integrate the resulting polyprotein expression cassette into baculovirus genomes of choice to generate composite baculovirus for protein expression (Fig. 2b). With this strategy, a number of complexes have been successfully produced with balanced subunit expression levels, including a ~700 kDa physiological core complex of human general transcription factor TFIID [4] (Fig. 3).\n\n",
            "cite_spans": [
                {
                    "start": 1112,
                    "end": 1113,
                    "mention": "4",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Introduction",
            "ref_spans": [
                {
                    "start": 526,
                    "end": 528,
                    "mention": "2a",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 899,
                    "end": 901,
                    "mention": "2b",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 1121,
                    "end": 1122,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "Multiprotein complexes can be expressed from a single polyprotein, or alternatively, from several polyproteins that are co-expressed, or a combination of single protein expression cassettes and one or several polyproteins, depending on the complex of choice. We recommend combining a maximum of four to five genes (in addition to the genes encoding for TEV protease and the fluorescent protein) into a single open reading frame (ORF). Otherwise, any later work to modify the genes of interest may become complicated.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "We observed that while it is sufficient to provide one TEV NIa protease gene in a co-expression experiment using several polyproteins, it appears that \u201ctagging\u201d all polyproteins with TEV NIa protease at the N-terminus balances overexpression levels between polyproteins (IB, unpublished data).",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "\nRestriction endonucleases BstEII and RsrII and reaction buffers (New England Biolabs, NEB).T4 DNA ligase and buffer (NEB).Gel extraction kit (i.e., Qiagen, Germany).Plasmid purification kit (i.e., Qiagen, Germany).Regular E. coli competent cells (TOP10, HB101, or comparable).\nE. coli competent cells containing pir gene (if Donor plasmids are used, see\nNote\n2).Antibiotics: chloramphenicol, gentamicin, kanamycin, spectinomycin (for concentrations see ref. 8).Agar for pouring plates.Media (LB, TB, SOC) for growing minicultures.\n",
            "cite_spans": [
                {
                    "start": 459,
                    "end": 460,
                    "mention": "8",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Materials for Inserting Polyprotein Constructs into Transfer Vectors via Restriction\u2013Ligation Cloning ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\n\nE. coli competent cells (DH10MultiBac, DH10EMBacY, DH10MultiBacCre) (see\nNote\n3).Antibiotics chloramphenicol, gentamicin, kanamycin, spectinomycin, tetracycline (for concentrations see ref. 8).Bluo-Gal or X-Gal.IPTG.Agar for pouring plates.Media (LB, TB, SOC) for growing minicultures.\n",
            "cite_spans": [
                {
                    "start": 192,
                    "end": 193,
                    "mention": "8",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Materials for Integrating Polyprotein Expression Cassettes into Baculovirus Genome ::: Materials",
            "ref_spans": []
        },
        {
            "text": "\nGroup genes into polyproteins based on a set of chosen criteria (such as putative interaction partners, physiological (sub)assemblies, subunits with the same copy number within a complex).Decide on the number of polyproteins that should be co-expressed (we recommend not to catenate more than four to five genes in addition to the protease and fluorescent marker encoding genes in each polyprotein ORF).Decide on placement of tags. Note that cleavage sites other than TEV protease cleavage sites have to be used if tags are to be removed at a later stage by a specific protease (i.e., PreScission protease, thrombin, enterokinase, others).Remove stop codons from individual genes, except for the last gene of interest if the option to monitor polyprotein expression via the plasmid-encoded fluorescent marker protein is not desired. If fluorescence read-out is desired, delete stop codons of all genes that are to be inserted.Decide on TEV protease cleavage site containing linker in between individual protein entities in the polyprotein. In particular if long unstructured tails are already predicted for example at the C-terminus of a given protein, we recommend adjoining the TEV NIa protease cleavage site (typically ENLYFQ\u2019G) directly. The glycine residue replaces the starting methionine of the following protein.Generate the DNA sequence. Add BstEII site to 5\u2032 end and RsrII site to 3\u2032 end.Create complete polyprotein expression construct in silico, predict translation, verify reading frame through the TEV NIa protease and the fluorescent marker.Decide on DNA assembly strategy (SLIC, restriction\u2013ligation, PCR assembly, others).Create all DNA sequences in silico and validate by simulating the reading frame.\n",
            "cite_spans": [],
            "section": "Polyprotein In Silico Design ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nChoose from pOmni-PBac, pPBac, or pKL-PBac to generate the polyprotein expressing construct for expression with the baculovirus of choice (see\nNote\n4). All polyprotein expression cassettes have the same design with BstEII and RsrII sites for DNA insertion between the gene encoding for TEV NIa protease and the gene encoding for CFP. pKL-PBac contains a LoxP site for integrating Donor plasmids with further genes of interest; pOmni-PBac contains elements for homologous recombination in addition to elements for Tn7 transposition.Digest several micrograms transfer plasmid by BstEII and RsrII enzymes according to manufacturers\u2019 recommendation. Sequential digestion is recommended as BstEII cuts optimally at 60 \u00b0C, while RsrII prefers 37 \u00b0C.Analyze the digestions by agarose gel electrophoresis to confirm that the digestions are complete.Purify digested plasmid by using commercial gel extraction kits (for example Qiagen gel extraction kit). It is recommended to elute the extracted DNA in the minimal volume defined by the manufacturer. Determine the concentration of the extracted DNA spectrophotometrically (e.g., Thermo Scientific NanoDrop 2000). Store in frozen aliquots.\n",
            "cite_spans": [],
            "section": "Preparation of Transfer Plasmid DNA ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nDigest several micrograms of the DNA (generated by DNA synthesis, SLIC, PCR assembly, or other methods of choice) encoding for the desired polyprotein with BstEII and RsrII enzymes according to the manufacturers\u2019 recommendation. Sequential digestion is recommended as BstEII cuts optimally at 60 \u00b0C, while RsrII prefers 37 \u00b0C.Purify digested insert DNA by using a commercial gel extraction kit. It is recommended to elute the extracted DNA in the minimal volume defined by the manufacturer. Determine the concentration of the extracted DNA spectrophotometrically.Set up ligation reactions by mixing purified insert and vector (see Subheading 3.2) in 10\u201320 \u03bcL reaction volume with T4 DNA ligase and specific buffer according to the recommendations from the supplier. Perform ligation reactions at 25 \u00b0C overnight. It is recommended to analyze the ligation reaction by agarose gel electrophoresis to evaluate the ligation efficiency.Transform regular E. coli competent cells (see\nNote\n2) with ligation reaction by following standard transformation procedures. Incubate the transformation reaction in a 37 \u00b0C shaker for 1\u20132 h and plate on agar plates in a dilution series to ensure optimal colony separation.Pick colonies, grow minicultures, and purify plasmids.Indentify positive clones by restriction digestion and DNA sequencing of the insert.\n",
            "cite_spans": [],
            "section": "Inserting Polyprotein Expression Cassettes into BstEII/RsrII Digested Transfer Vectors ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nTransform corresponding E. coli competent cells (DH10MultiBac or DH10EMBacY) with transfer plasmid by following standard transformation procedures. Incubate the transformation reaction in a 37 \u00b0C shaker overnight (see\nNote\n5).Plate the transformation reaction on agar plates containing antibiotics as described [9], IPTG (1 mM) and Bluo-Gal (or X-Gal) in a dilution series to ensure optimal colony separation. Incubate at 37 \u00b0C until blue and white colonies are well distinguishable.Restreak four to eight white colonies to unambiguously confirm that they are positive (white). It is recommended to restreak also a blue colony as negative control.Inoculate four confirmed white colonies in 2 mL aliquots of LB medium supplemented with corresponding antibiotics. After overnight incubation, use two to four of the cell cultures for bacmid purification, transfection, viral amplification, and multiprotein complex overexpression [8].\n",
            "cite_spans": [
                {
                    "start": 313,
                    "end": 314,
                    "mention": "9",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 929,
                    "end": 930,
                    "mention": "8",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Integrating Polyprotein Expression Constructs into the MultiBac Baculoviral Genome via Tn7 Transposition ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nPlace polyprotein expression cassette into Donor plasmid of choice by SLIC, restriction\u2013ligation, PCR assembly, or other methods of choice. Validate resulting constructs by restriction mapping.Transform DH10MultiBacCre electro-competent cells (these contain Cre recombinase expressed from a separate plasmid [9]) with this polyprotein expressing Donor plasmid by following standard electroporation procedures. Incubate the transformation reaction in a 37 \u00b0C shaker overnight.Plate the transformation reaction on agar plates containing corresponding antibiotics, IPTG (1 mM) and Bluo-Gal (or X-Gal) in a dilution series to ensure optimal colony separation. Incubate at 37 \u00b0C until blue color of the colonies is clearly observed.Restreak four to eight blue colonies on the same type of agar plates to confirm they are positive.Inoculate four confirmed blue colonies in 2 mL aliquots of LB medium supplemented with corresponding antibiotics. After overnight incubation, use all four cell cultures (see\nNote\n6) for bacmid purification, transfection, viral amplification, and multiprotein complex overexpression following published protocols [8].\n",
            "cite_spans": [
                {
                    "start": 310,
                    "end": 311,
                    "mention": "9",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1139,
                    "end": 1140,
                    "mention": "8",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Integrating Polyprotein Expression Cassettes into MultiBac Baculoviral Genome via In Vivo Cre-LoxP Reaction ::: Methods",
            "ref_spans": []
        },
        {
            "text": "\nThe BstEII enzyme has the asymmetric restriction site G^GTNAC_C, the RsrII restriction enzyme has the asymmetric restriction site CG^GWC_CG. In both cases the central base can have different contexts. When constructing the ORF encoding for the polyprotein, the sites have to be chosen such as to be compatible with the transfer plasmids.Donors and their derivatives can only be propagated in cells that express the pir gene (such as BW23473, BW23474, or PIR1 and PIR2, Invitrogen) due to the conditional origin present on these plasmids [13]. In contrast, Acceptors and their derivatives contain regular ColE1 origin of replication and can be propagated in regular E. coli strains (TOP10, HB101, or comparable) [3, 12].The generation of DH10MultiBacCre cells by expressing Cre recombinase is detailed in ref. 9.Plasmids pPBac and pKL-PBac rely on Tn7 transposition and a baculovirus genome in form of a bacterial artificial chromosome (bacmid) for composite baculovirus generation (i.e., Bac-to-Bac system from Invitrogen, MultiBac). Plasmid pOmni-PBac, in contrast, is a universal transfer plasmid that can access baculoviruses by both Tn7 transposition and homologous recombination [7].Besides polyprotein expression constructs, the Tn7 attachment site (mini-attTn7) and the LoxP site can also be used for integrating single protein and multigene expression constructs (Fig. 2b).It is recommended to check at least four blue colonies since the integration efficiency of in vivo Cre-LoxP reaction is generally lower than Tn7 transposition.\n",
            "cite_spans": [
                {
                    "start": 539,
                    "end": 541,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 713,
                    "end": 714,
                    "mention": "3",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 716,
                    "end": 718,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 810,
                    "end": 811,
                    "mention": "9",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1186,
                    "end": 1187,
                    "mention": "7",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Notes",
            "ref_spans": [
                {
                    "start": 1378,
                    "end": 1380,
                    "mention": "2b",
                    "ref_id": "FIGREF1"
                }
            ]
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 1: Design and in vivo processing of polyproteins. (a) Genes of interest (GOIs) are assembled into a single open reading frame (ORF), giving rise to a polyprotein. In this polyprotein, individual genes of interest are spaced apart by cleavage sites (tcs) for tobacco etch virus (TEV) NIa protease which is also encoded by the ORF. The C-terminal CFP serves to monitor heterologous expression level. (b) Composite MultiBac baculoviral genome DNA containing the polyprotein expression cassette (left ) is used to transfect cultured insect cells for multiprotein complex production (right)",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig. 2: Integration of polyprotein expression cassettes into MultiBac baculovirus. (a) MultiBac Acceptor vectors pPBac, pKL-PBac, and pOmni-PBac, tailored for polyprotein production, are shown schematically (top). They contain a regular ColE1 origin of replication and a polyprotein expression cassette, which encodes an N-terminal TEV protease and a C-terminal CFP spaced by a TEV cleavage site (tcs). BstEII and RsrII sites are used for inserting the polyprotein encoding ORF of interest. Donor vectors pIDC, pIDK, and pIDS contain a conditional origin of replication derived from the R6K\u03b3 phage [13]. The multiplication module flanking the expression cassettes contain a homing endonuclease site and a complementary BstXI site (boxes in light blue). Polh and p10 are baculoviral very late promoters; SV40 and HSVtk are polyadenylation signals. MCS1 and MCS2 stand for multiple cloning sites. Tn7L and Tn7R are specific DNA sequences for Tn7 transposition; the lef2/603 and Ori1629 homology regions are shown as gray boxes. LoxP sites are shown as red balls. Cm stands for chloramphenicol, Gn for gentamicin, Kn for kanamycin, Sp for spectinomycin. (b) Besides polyprotein expression cassettes, single protein and multigene expression cassettes can also be integrated into the Tn7 attachment site (mini-attTn7) harbored by the LacZ (lacZ\u03b1) gene, or the LoxP site of the MultiBac baculovirus. Ap stands for ampicillin. The F-Replicon is a single-copy bacterial origin of replication. For reagents contact: iberger@embl.fr",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Fig. 3: Multiprotein complexes produced from polyproteins. (a) The TAF8/TAF10 dimer (inserted into the Tn7 attachment site) was co-expressed as a polyprotein with the yellow fluorescent protein (YFP) inserted into the viral LoxP site from a composite baculovirus (EMBacY-TAF8/TAF10). YFP and CFP expression per one million cells were tracked for evaluating the viral infection and polyprotein production. St stands for a defined fluorescence standard (used to calibrate for 100,000 arbitrary units), dpa stands for day of proliferation arrest in the infected culture [5]. (b) SDS-PAGE (left ) shows balanced expressions of TAF8 and TAF10. Complete proteolysis of the TAF8/TAF10 polyprotein was confirmed by Western blot (right ) using antibody specific for the hexa histidine-tags of TAF10 and TEV protease (doublet ). M stands for molecular weight marker, C stands for cell control (uninfected insect cells), W stands for whole cell extract, S stands for supernatant. (c) Sections from SDS-PAGE are shown for TAF8/TAF10 dimer from size exclusion chromatography purification, SMAT complex from IMAC batch purification [5], 3TAF and core-TFIID complexes from size exclusion chromatography purification [4]",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Getting a grip on complexes",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Nie",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Viola",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Bieniossek",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Curr Genomics",
            "volume": "10",
            "issn": "",
            "pages": "558-572",
            "other_ids": {
                "DOI": [
                    "10.2174/138920209789503923"
                ]
            }
        },
        "BIBREF1": {
            "title": "Automated unrestricted multigene recombineering for multiprotein complex production",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Bieniossek",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Nie",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Frey",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Nat Methods",
            "volume": "6",
            "issn": "",
            "pages": "447-450",
            "other_ids": {
                "DOI": [
                    "10.1038/nmeth.1326"
                ]
            }
        },
        "BIBREF2": {
            "title": "Harnessing homologous recombination in vitro to generate recombinant DNA via SLIC",
            "authors": [
                {
                    "first": "MZ",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "Elledge",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Nat Methods",
            "volume": "4",
            "issn": "",
            "pages": "251-256",
            "other_ids": {
                "DOI": [
                    "10.1038/nmeth1010"
                ]
            }
        },
        "BIBREF3": {
            "title": "New baculovirus expression tools for recombinant protein complex production",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Trowitzsch",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Bieniossek",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Nie",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "J Struct Biol",
            "volume": "172",
            "issn": "",
            "pages": "45-54",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jsb.2010.02.010"
                ]
            }
        },
        "BIBREF4": {
            "title": "Use of the rep technique for allele replacement to construct new Escherichia coli hosts for maintenance of R6K\u03bb origin plasmids at different copy numbers",
            "authors": [
                {
                    "first": "WW",
                    "middle": [],
                    "last": "Metcalf",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Jiang",
                    "suffix": ""
                },
                {
                    "first": "BL",
                    "middle": [],
                    "last": "Wanner",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Gene",
            "volume": "138",
            "issn": "",
            "pages": "1-7",
            "other_ids": {
                "DOI": [
                    "10.1016/0378-1119(94)90776-5"
                ]
            }
        },
        "BIBREF5": {
            "title": "Baculovirus expression system for heterologous multiprotein complexes",
            "authors": [
                {
                    "first": "I",
                    "middle": [],
                    "last": "Berger",
                    "suffix": ""
                },
                {
                    "first": "DJ",
                    "middle": [],
                    "last": "Fitzgerald",
                    "suffix": ""
                },
                {
                    "first": "TJ",
                    "middle": [],
                    "last": "Richmond",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat Biotechnol",
            "volume": "22",
            "issn": "",
            "pages": "1583-1587",
            "other_ids": {
                "DOI": [
                    "10.1038/nbt1036"
                ]
            }
        },
        "BIBREF6": {
            "title": "MultiBac: expanding the research toolbox for multiprotein complexes",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Bieniossek",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Imasaki",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Takagi",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Trends Biochem Sci",
            "volume": "37",
            "issn": "",
            "pages": "49-57",
            "other_ids": {
                "DOI": [
                    "10.1016/j.tibs.2011.10.005"
                ]
            }
        },
        "BIBREF7": {
            "title": "The architecture of human general transcription factor TFIID core complex",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Bieniossek",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Papai",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Schaffitzel",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Nature",
            "volume": "493",
            "issn": "",
            "pages": "699-702",
            "other_ids": {
                "DOI": [
                    "10.1038/nature11791"
                ]
            }
        },
        "BIBREF8": {
            "title": "Robots, pipelines, polyproteins: enabling multiprotein expression in prokaryotic and eukaryotic cells",
            "authors": [
                {
                    "first": "LS",
                    "middle": [],
                    "last": "Vijayachandran",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Viola",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Garzoni",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J Struct Biol",
            "volume": "175",
            "issn": "",
            "pages": "198-208",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jsb.2011.03.007"
                ]
            }
        },
        "BIBREF9": {
            "title": "Nidovirales: evolving the largest RNA virus genome",
            "authors": [
                {
                    "first": "AE",
                    "middle": [],
                    "last": "Gorbalenya",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Enjuanes",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Ziebuhr",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Virus Res",
            "volume": "117",
            "issn": "",
            "pages": "17-37",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virusres.2006.01.017"
                ]
            }
        },
        "BIBREF10": {
            "title": "Gene gymnastics: synthetic biology for baculovirus expression vector system engineering",
            "authors": [
                {
                    "first": "LS",
                    "middle": [],
                    "last": "Vijayachandran",
                    "suffix": ""
                },
                {
                    "first": "DB",
                    "middle": [],
                    "last": "Thimiri Govinda Raj",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Edelweiss",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Gupta",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Maier",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Gordeliy",
                    "suffix": ""
                },
                {
                    "first": "DJ",
                    "middle": [],
                    "last": "Fitzgerald",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Berger",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Bioengineered",
            "volume": "4",
            "issn": "",
            "pages": "1-9",
            "other_ids": {
                "DOI": [
                    "10.4161/bioe.22966"
                ]
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Protein complex expression by using multigene baculoviral vectors",
            "authors": [
                {
                    "first": "DJ",
                    "middle": [],
                    "last": "Fitzgerald",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Berger",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Schaffitzel",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Nat Methods",
            "volume": "3",
            "issn": "",
            "pages": "1021-1032",
            "other_ids": {
                "DOI": [
                    "10.1038/nmeth983"
                ]
            }
        }
    }
}