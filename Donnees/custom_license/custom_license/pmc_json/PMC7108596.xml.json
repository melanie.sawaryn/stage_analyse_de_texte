{
    "paper_id": "PMC7108596",
    "metadata": {
        "title": "Prolonged presence of effector\u2010memory CD8 T cells in the central nervous system after dengue virus encephalitis",
        "authors": [
            {
                "first": "Robbert",
                "middle": [
                    "G."
                ],
                "last": "van der Most",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kaja",
                "middle": [],
                "last": "Murali\u2010Krishna",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Rafi",
                "middle": [],
                "last": "Ahmed",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Most of our understanding of immunological memory comes from the analysis of memory cells in lymphoid tissues. However, it is becoming increasingly clear in mouse models that long\u2010lived memory T cells are also present in non\u2010lymphoid tissues (1\u20138). Several studies have indicated that non\u2010lymphoid memory cells exhibit an activated phenotype (1\u20133), leading to the proposition that these cells represent an in situ rapid response force. Consistent with these ideas, two subsets of human memory T cells have recently been described, central and effector\u2010memory cells, that differ in the expression of the chemokine receptor CCR7 and the lymphoid homing marker L\u2010selectin (CD62L) (8). Whereas central memory cells (CCR7high/CD62Lhigh) represent classical lymphoid memory cells, effector\u2010memory cells (CCR7low/CD62Llow) circulate outside the lymphoid organs and were found to maintain various effector functions (8). It should be noted though that phenotypic classification of CD8 T cells during chronic human infection may be more complex: memory CD8 T cells appear to accumulate at different points along the differentiation pathway in different infections (9). At least three different phenotypes can be distinguished based on CD27 and CD28 expression (9). From these studies it was concluded that human memory CD8 T cells would be better defined on the basis of their activation status (9). Thus, comparing phenotypes between human and murine infections can be problematic.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "In the present study, we have found evidence for long\u2010lived effector\u2010memory T cells in the brains of dengue virus\u2010challenged mice. Previously, evidence for persisting antiviral CD8 T cells in the central nervous system (CNS) has come from experiments in which immunized mice were intranasally challenged with a neurotropic strain of influenza virus (1). In the brains of these mice, influenza\u2010specific CD8 T cells persisted for up to 320 days in a partially activated state (CD62Llow, CD25low, CD69high) (1). Additionally, primary infection of mice with a neurotropic strain of the coronavirus mouse hepatitis virus (MHV) also leads to prolonged presence of antiviral T cells in the CNS (10\u201312).",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "In our study, we have analyzed persisting CD8 T cells in the brains of immunized mice surviving dengue challenge. Intracranial challenge of naive mice with dengue virus induces lethal encephalitis (13,14), accompanied by a weak antiviral CD8 T cell response (15). Only low frequencies of dengue virus\u2010specific CD8 T cells can be detected in the spleens and in the brains of encephalitic mice (15). In contrast, immunized mice control the dengue virus challenge (13\u201315): immunization primes a cellular immune response that is effectively recruited in the CNS after intracranial challenge, similar to that described for influenza virus encephalitis (1). A detailed phenotypic analysis of these CNS\u2010resident T cells indicates that they should be categorized as effector\u2010memory T cells.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The chimeric YF/DEN virus (15) was grown on SW\u201013 cells, which were propagated in minimal essential medium supplemented with 10% FCS, 2 mM l\u2010glutamine, antibiotics and non\u2010essential amino acids. The vaccinia virus recombinant expressing pre\u2010membrane (prM) and envelope (E) from dengue\u20102 virus (16) was provided by Dr Ching\u2010Juh Lai (National Institutes of Health, Bethesda, MD). A20 cells were propagated in RPMI supplemented with 10% FCS, 2 mM l\u2010glutamine and antibiotics.",
            "cite_spans": [],
            "section": "Cells and virus ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Three\u2010week\u2010old BALB/c mice (Jackson Laboratory, Bar Harbor, ME) were immunized by s.c. injection of 5 \u00d7 105 p.f.u. of the chimeric YF/DEN virus. Results of control immunizations with PBS or with the parental yellow fever virus (YFV)\u201017D strain have been described previously (15). Mice were challenged 2 weeks after immunization by intracranial injection of 1.5 \u00d7 104 p.f.u. (100 50% lethal doses) of mouse\u2010adapted, neurovirulent dengue\u20102 virus (strain New Guinea C; kindly provided by Dr Kenneth Eckels, Walter Reed Army Institute for Research, Rockville, MD) (13,14).",
            "cite_spans": [],
            "section": "Immunization and challenge of mice ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Isolation of lymphocytes from the brains of YF/DEN immunized and dengue\u20102 virus\u2010challenged mice was done as described previously (10,11,12,15). Briefly, mouse brains were homogenized using a Tenbroeck homogenizer and lymphocytes were isolated by centrifugation over a Percoll cushion.",
            "cite_spans": [],
            "section": "Isolation of lymphocytes from mouse brain ::: Methods",
            "ref_spans": []
        },
        {
            "text": "Lymphocytes from the CNS and splenocytes were stained using FITC\u2010labeled mAb against CD62L, CD69, CD25 and CD11a (LFA\u20101) (clones MEL\u201014, H1.2F3, 7D4 and 2D7 respectively), R\u2010phycoerythrin (PE)\u2010conjugated antibodies against CD43, Ly6A/E, CD122 and CD80 (B7.1) (clones 1B11, D7, TM\u2010b1 and 16\u201010A1 respectively), and allophycocyanin (APC)\u2010labeled anti\u2010CD8 (clone 53\u20106.7). Intracellular staining to detect IFN\u2010\u03b3\u2010producing T cells was done as described (15). Briefly, A20 stimulator cells were infected with the prM\u2010E\u2010expressing vaccinia virus recombinant (15) at a m.o.i. = 1. Infected cells (2 \u00d7 105) were used at 8 h post\u2010infection, at which time they were incubated with 8 \u00d7 105 splenocytes or CNS lymphocytes, in the presence of Brefeldin A (PharMingen, San Diego, CA). As a control, cells were left unstimulated. After a 6\u2010h incubation, cells were surface stained with FITC\u2010labeled anti\u2010CD4 (clone GK1.5) and PE\u2010conjugated anti\u2010CD8 (clone 53\u20106.7) antibodies. Follow ing fixation and permeabilization (Cytofix/Cytperm kit; PharMingen), intracellular staining was performed using an APC\u2010conjugated antibody against IFN\u2010\u03b3 (clone XMG1.2). Samples were acquired using a FACSCalibur flow cytometer and analyzed using CellQuest software (Becton Dickinson, San Jose, CA). All mAb were obtained from PharMingen.",
            "cite_spans": [],
            "section": "Lymphocyte staining and flow cytometry ::: Methods",
            "ref_spans": []
        },
        {
            "text": "In our vaccination/challenge model, BALB/c mice were first immunized using a YFV/dengue chimeric virus (15). As described before, this recombinant virus expresses the prM and E genes from dengue\u20102 virus in a YFV\u201017D genetic background (15), and will therefore induce responses specific for the dengue virus prM\u2010E proteins. Based on the rules of prime\u2010boost immunization (17), it can be expected that the dengue\u2010specific T cell response after challenge will be predominantly directed against the two dengue proteins which were used for priming, i.e. prM and E. Thus, we have measured dengue virus prM\u2010 and E\u2010specific T cell responses in the spleen and in the CNS, both after immunization and after the challenge (15). To present the dengue virus prM and E proteins we used a recombinant vaccinia virus expressing prM and E (16) to infect syngeneic A20 cells (15). Responses were quantitated by intracellular IFN\u2010\u03b3 staining.",
            "cite_spans": [],
            "section": "Antiviral CD8 T cells persist in the brains, but not in the spleens, of dengue virus\u2010infected mice ::: Results",
            "ref_spans": []
        },
        {
            "text": "After immunization, dengue virus prM\u2010E\u2010specific CD8 T cell responses were readily detected in the spleen at a frequency of 0.5% of the total CD8 T cell population (day 14 post\u2010immunization, Fig. 1). We did observe some migration of CD8 T cells into the CNS, with 5\u20136% of these cells responding to dengue virus prM and E antigens (Fig. 1). However, it should be noted that the number of CD8+IFN\u2010\u03b3+ events was very small and that the brains had not been perfused. Therefore, it cannot be rigorously excluded that we observed responses from peripheral blood. At 14 days post\u2010immunization, mice were challenged intracranially with a mouse\u2010adapted, neurovirulent strain of dengue\u20102 virus (13). Whereas 13 out of 15 non\u2010immunized control mice succumbed to the ensuing dengue encephalitis, only one out of 12 immunized mice developed symptoms (15). In the spleens of immunized and challenged mice, we found that up to 4% of CD8 T cells were prM\u2010E specific at day 5 post\u2010challenge (Fig. 1) (15). In addition, we measured a strong influx of T cells into the CNS and 34% of CD8 T cells in the brain were prM\u2010E specific at day 5 post\u2010challenge (Fig. 1). Thus, as expected, virus\u2010specific T cells migrated to the site of infection, i.e. the CNS, during the acute response. Alternatively, it could be argued that a small population of CNS\u2010resident prM\u2010E\u2010specific CD8 T cells, primed by the immunization, expanded after the challenge. Clearly, prM\u2010E\u2010specific effector T cells, both in the spleen and in the CNS, displayed CD8 down\u2010regulation (Fig. 1 and Table 1).",
            "cite_spans": [],
            "section": "Antiviral CD8 T cells persist in the brains, but not in the spleens, of dengue virus\u2010infected mice ::: Results",
            "ref_spans": []
        },
        {
            "text": "Strikingly, however, we still detected high frequencies of prM\u2010E\u2010specific CD8 T cells in the CNS at 20, 40 and 56 days after the dengue virus challenge: 18\u201328% of CD8 T cells residing in the brain were specific for the dengue prM\u2010E proteins at these timepoints (Fig. 1). In addition, the absolute number of CD8 T cells in the brain did not decrease. Phenotypically, however, these cells did change: CD8 expression and the level of IFN\u2010\u03b3 production increased from day 5 to 56 (Table 1). In contrast, the kinetics of the splenic T cell response were more conventional: at day 56 post\u2010challenge, only 0.4% of CD8 T cells were prM\u2010E specific (Fig. 1). This 10\u2010fold decrease in the frequency of specific T cells in the transition from the effector phase to memory (the \u2018death phase\u2019) corresponds well to observations in other (acute infection) models (17). From these findings, we concluded that virus\u2010specific memory CD8 T cells preferentially persisted in the CNS.",
            "cite_spans": [],
            "section": "Antiviral CD8 T cells persist in the brains, but not in the spleens, of dengue virus\u2010infected mice ::: Results",
            "ref_spans": []
        },
        {
            "text": "As a first phenotypic characterization, we analyzed the expression patterns of cell\u2010surface O\u2010glycans using the 1B11 mAb. This antibody recognizes the O\u2010glycans on an activation\u2010associated form of CD43 (19,20). Positive 1B11 staining identifies the population of cytotoxic effector T cells that display direct ex vivo cytolytic activity (cytotoxic T lymphocytes) (19,20). Memory cells, in contrast, are 1B11low (19,20). Figure 2 shows the CD43/1B11 expression levels on dengue virus prM\u2010E\u2010specific CD8 T cells in the brains and spleens, before and after immunization. As can be deduced from Fig. 2, the prM\u2010E specific T cells (i.e. IFN\u2010\u03b3+ cells) in the spleen and in the CNS were all 1B11high at day 14 post\u2010immunization, indicating that these cells are effector cytotoxic T lymphocytes. Although these cells may not yet have acquired a full memory phenotype (18,19), we consistently found solid protection and recruitment of prM\u2010E\u2010specific CD8 T cells after challenge at 14 days post\u2010infection (14) (Fig. 1). At days 20 and 56 post\u2010challenge, we found that prM\u2010E\u2010specific CD8 T cells, in the spleen as well as in the CNS, expressed decreasing amounts of the 1B11 determinant (Fig. 2). This suggests that these cells were in the process of differentiating into memory cells (19,20). The kinetics of CD43 down\u2010regulation were somewhat slower in the CNS as compared to the spleen. Alternatively, the loss of CD43high CD8 T cells from the spleen at day 20 post\u2010challenge could reflect migration of activated cells into the CNS. Nevertheless, our data suggest that the majority of prM\u2010E\u2010specific CD8 T cells retained in the CNS at day 56 post\u2010challenge are non\u2010cytolytic, based on their 1B11 expression patterns (19).",
            "cite_spans": [],
            "section": "Expression of CD43: kinetics of the cytolytic phenotype ::: Results",
            "ref_spans": []
        },
        {
            "text": "We then characterized the entire population of CNS\u2010resident T cells, by measuring the expression of several different activation makers. Analysis of this population (which includes the IFN\u2010\u03b3+ prM\u2010E\u2010specific CD8 T cells, \u223c20\u201325%, but also other cells), shown in Fig. 3, revealed that all CD8 T cells in the CNS expressed the 1B11 determinant at day 5 post\u2010infection (Fig. 3A) and were therefore most likely true cytotoxic T cells. Clearly, two 1B11high CD8 T cell populations can be distinguished, and, as shown in Fig. 4, these populations appear to differ in the expression level of CD69, and, therefore, possibly in their activation status. Reflecting the differentiation pattern of the IFN\u2010\u03b3+ prM\u2010E\u2010specific CD8 T cells, CD43 expression levels were decreasing in the entire population of CNS\u2010resident CD8 T cells at day 56 post\u2010infection, although 1B11high CD8 T cells were still present. Thus, CNS\u2010resident CD8 T cells were heterogeneous in their 1B11 expression patterns. On the other hand, the expression patterns of several activation and homing markers (CD69, Ly6A/E and CD62L) suggested an activated phenotype. A large majority of CD8 T cells in the CNS had down\u2010regulated L\u2010selectin (CD62Llow), both at day 5 (85%) and day 56 (89%) post\u2010infection (Fig. 3B), consistent with their non\u2010lymphoid localization. In addition, the vast majority of the CD8 T cells in the brain were CD69high (89%) and Ly\u20106A/Ehigh (80%) at day 56 post\u2010infection (Fig. 3C and D). CD69 and Ly\u20106A/E were also up\u2010regulated in day 5 effector cells (Fig. 3C and D), although expression levels at day 5 were more heterogeneous than at day 56. The differences between the CD8 T cell populations at days 5 and 56 are most clearly illustrated by double\u2010staining for CD69 and 1B11/CD43: at day 56, more cells have up\u2010regulated CD69 expression and down\u2010regulated CD43/1B11 expression (Fig. 4). As a control, expression levels of CD43, CD62L, CD69 and Ly\u20106A/E in day 5 and 56 spleen cells are shown in Fig. 3: these CD8 T cells are 1B11low, CD62Lhigh, CD69low and Ly\u20106A/Elow. No changes in the expression levels of CD25 (IL\u20102R\u03b1), CD122 (IL\u20102R\u03b3), B7.1 and CD11a (LFA\u20101) were observed when comparing splenic and CNS\u2010derived lymphocytes at days 5 and 56 post\u2010challenge (data not shown). Expression levels of CD25, CD122 and B7.1 were uniformly low, and LFA\u20101 expression was high on all CD8 T lymphocytes.",
            "cite_spans": [],
            "section": "Phenotypic characterization of CNS\u2010resident CD8 T cells ::: Results",
            "ref_spans": []
        },
        {
            "text": "In the present study, we have found that virus\u2010specific CD8 T cells persisted at the site of infection and that these cells displayed a unique phenotype, featuring traits from both memory (CD43/1B11low) and activated T cells (CD62Llow, CD69high, Ly6A/Ehigh). The CD43 expression pattern of the population of CNS\u2010resident CD8 T cells changes from (biphasic) CD43high at day 5 post\u2010challenge towards a more heterogeneous pattern at day 56 post\u2010challenge Thus, it appears that the majority of CD8 T cells are differentiating into memory cells, as judged by CD43 expression (19,20). Based on the strong correlation between CD43 (1B11) expression and direct ex vivo cytotoxicity (19), this would suggest that fewer CD8 cells would display direct ex vivo cytotoxicity at day 56. Clearly, dengue virus\u2010specific CD8 T cells expressed low levels of the 1B11/CD43 determinant at late timepoints. It should be noted, however, that cytolytic clearance may not play a major role in the CNS, especially in the case of infected neurons. Instead, it has been shown in several viral infection models that T cells use IFN\u2010\u03b3 to clear virus from the brain (21\u201323). In this respect, an activated phenotype may be more important than direct ex vivo cytotoxicity in the brain.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The expression patterns of CD69 and Ly6A/E were striking: expression levels of these activation markers were higher and more homogenous at day 56 post\u2010challenge than immediately after challenge. This same pattern (i.e. heterogeneous CD69 expression during acute encephalitis and uniform high expression late after infection) was seen in mice infected with a neurovirulent strains of influenza virus (1) or MHV (10). As pointed out by Bergmann et al., CD69 expression is a common hallmark of CD8 T cells retained in the CNS (10,11). The CNS\u2010resident CD8 T cells after dengue virus infection were CD69high and also CD25low, similar to antiviral CD8 T cells in the brains of MHV\u2010infected rats and mice (10,24). It has been suggested that the phenotype of these cells reflects a state of \u2018anergy post\u2010activation\u2019 (10,25). Alternatively, as discussed below, CD69 may play a role in the regulation of T cell trafficking (26) and persistent CD69 up\u2010regulation could be involved in T cell retention.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Our study raises three important questions. The first question relates to the specificity of the majority of CNS\u2010resident CD8 T cells that are not prM\u2010E specific. In our view, there are three explanations. (i) We may be observing cells with unknown specificities that migrated into the brain through bystander activation. (ii) The remaining CD8 T cells could recognize other dengue virus epitopes, outside the prM\u2010E proteins. It is possible that other epitopes, cross\u2010reactive in the YFV vaccine and the dengue challenge virus (e.g. from the conserved NS5 polymerase protein), are responsible for the expansion of T cells. (iii) There may be a population of prM\u2010E\u2010specific CD8 T cells that failed to produce IFN\u2010\u03b3 upon antigenic stimulation. Inactivated CD8 T cells have been detected previously in the context of antiviral responses (27\u201329). The second question is perhaps the most important issue raised by our experiments and by several earlier studies (1,10,11): why are virus\u2010specific CD8 T cells retained in the brain and what keeps them in a partially activated state? There has been some debate on the role of persisting antigen in retaining T cells in the brain. Hawke et al. observed persisting antiviral CD8 T cells in the absence of antigen after challenge of immunized mice with influenza virus (1). However, as proposed by these authors (1) and given the extreme sensitivity of CD8 T cells (30), it is possible that CD8 T cells are retained by persisting MHC class I\u2013peptide complexes on neuronal cells that are below the threshold of detection. Marten et al., in contrast, reported that retention of both CD4 and CD8 T cells in the brains of MHV\u2010infected mice strictly coincided with the presence of viral RNA (11). These authors have provided two explanations for the observed differences (11). (i) T cells recruited during secondary influenza virus infection are memory cells, whereas primary cells are induced during MHV infection. (ii) Influenza virus infects neurons, which express low levels of MHC class I molecules (31,32), whereas MHV predominantly infects microglia and astrocytes, which express higher levels of both MHC class I and class II molecules (11). Clearly, our dengue virus infection model is more similar to secondary influenza virus infection: the intracranial dengue virus infection recruits memory T cells (induced by s.c. vaccination) and dengue virus has a neuronal tropism in mice (33). These differences should also be interpreted in the light of the complex phenotypic differences seen in human chronic infections (9): MHV does establish a persistent infection in mice (10,11), whereas influenza virus evidently does not (1). It is unclear whether dengue virus persists after the acute encephalitis and this will be a subject for further investigation. Importantly, however, the CD4 T cell response in the brain (the CD8\u2013 IFN\u2010\u03b3+ cells in Fig. 1) (14) strongly decreased from day 5 to 56. This decreasing CD4 T cell response is inconsistent with persistence of antigen, as shown by Marten et al. (11) in the case of persisting T cell responses against MHV. Thus, in the absence of antigen, CD4 T cells efflux from the CNS (11,34), whereas CD8 T cells can persist (1). A third issue that needs further investigation is the homing potential of the CNS\u2010resident CD8 T cells [reviewed in (35)]. It is currently unclear whether these cells express homing markers specific for the brain or a set of markers that merely specifies non\u2010lymphoid localization, irrespective of the tissue. One intriguing possibility is that CD69 plays a role in the retention of T cells in tissues. Recent results using CD69 transgenic mice indicate that thymocytes that constitutively express CD69 are retained in the thymus (26). This may implicate CD69 in the regulation of T cell trafficking, as suggested by the authors (26). In addition, CD43 (i.e. the high mol. wt form detected by the 1B11 antibody) may play a role in CNS localization: antigen\u2010specific CD8 T cells do not migrate into the brains of CD43 knockout mice that are intracerebrally infected with lymphocytic choriomeningitis virus (20).",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In conclusion, we hypothesize that the prM\u2010E\u2010specific CD8 T cells form a pool of local, activated memory cells that is independent and phenotypically different from the peripheral (lymphoid) memory T cell pool.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "We thank Dr Kenneth H. Eckels for providing dengue\u20102 virus, Dr Ching\u2010Juh Lai for the prME\u2010expressing vaccinia virus recombinant, Dr Jacob J. Schlesinger for helpful suggestions on the dengue challenge model and Madhavi Krishna for technical assistance. This work was supported by NIH grant RO1 AI49532 to R. A. and R. v. d. M.",
            "cite_spans": [],
            "section": "Acknowledgements",
            "ref_spans": []
        },
        {
            "text": "APC\u2014allophycocyanin",
            "cite_spans": [],
            "section": "Abbreviations",
            "ref_spans": []
        },
        {
            "text": "CNS\u2014central nervous system",
            "cite_spans": [],
            "section": "Abbreviations",
            "ref_spans": []
        },
        {
            "text": "E\u2014envelope",
            "cite_spans": [],
            "section": "Abbreviations",
            "ref_spans": []
        },
        {
            "text": "MHV\u2014mouse hepatitis virus",
            "cite_spans": [],
            "section": "Abbreviations",
            "ref_spans": []
        },
        {
            "text": "prM\u2014pre\u2010membrane",
            "cite_spans": [],
            "section": "Abbreviations",
            "ref_spans": []
        },
        {
            "text": "PE\u2014R\u2010phycoerythrin",
            "cite_spans": [],
            "section": "Abbreviations",
            "ref_spans": []
        },
        {
            "text": "YFV\u2014yellow fever virus",
            "cite_spans": [],
            "section": "Abbreviations",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1.: Expression levels of IFN\u2010\u03b3 and CD8, on the population of IFN\u2010\u03b3+ and CD8+ dengue prM\u2010E specific cells, expressed as mean fluoresence intensity\n Values were determined using CellQuest software, based on the data shown in Fig. 1.",
            "type": "table"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 1998,
            "venue": "J. Exp. Med.",
            "volume": "187",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 1999,
            "venue": "J. Immunol.",
            "volume": "163",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2000,
            "venue": "J. Virol.",
            "volume": "74",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2000,
            "venue": "J. Immunol.",
            "volume": "164",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 1987,
            "venue": "Am. J. Trop. Med. Hyg.",
            "volume": "36",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 1987,
            "venue": "J. Gen. Virol.",
            "volume": "68",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2000,
            "venue": "J. Virol.",
            "volume": "74",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 1991,
            "venue": "Virology",
            "volume": "185",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 1999,
            "venue": "Immunol. Rev.",
            "volume": "170",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": 1998,
            "venue": "Immunity",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": 2000,
            "venue": "J. Exp. Med.",
            "volume": "191",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "Eur. J. Immunol.",
            "volume": "31",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [],
            "year": 2002,
            "venue": "J. Immunol.",
            "volume": "168",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "Science",
            "volume": "293",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": 1993,
            "venue": "J. Immunol.",
            "volume": "150",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "",
            "authors": [],
            "year": 1999,
            "venue": "J. Immunol.",
            "volume": "162",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "",
            "authors": [],
            "year": 1995,
            "venue": "J. Neurovirol.",
            "volume": "1",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "",
            "authors": [],
            "year": 1997,
            "venue": "J. Immunol. Methods",
            "volume": "209",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "",
            "authors": [],
            "year": 2002,
            "venue": "Int. Immunol.",
            "volume": "14",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF19": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "Nat. Med.",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF20": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "J. Virol.",
            "volume": "75",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF21": {
            "title": "",
            "authors": [],
            "year": 1998,
            "venue": "J. Exp. Med.",
            "volume": "188",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF22": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "J. Immunol.",
            "volume": "166",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF23": {
            "title": "",
            "authors": [],
            "year": 1996,
            "venue": "Immunity",
            "volume": "4",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF24": {
            "title": "",
            "authors": [],
            "year": 1999,
            "venue": "Lab. Invest.",
            "volume": "79",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF25": {
            "title": "",
            "authors": [],
            "year": 1993,
            "venue": "Glia",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF26": {
            "title": "",
            "authors": [],
            "year": 1987,
            "venue": "J. Infect. Dis.",
            "volume": "155",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF27": {
            "title": "",
            "authors": [],
            "year": 1996,
            "venue": "J. Immunol.",
            "volume": "156",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF28": {
            "title": "",
            "authors": [],
            "year": 2002,
            "venue": "Adv. Virus Res.",
            "volume": "56",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF29": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "J. Exp. Med.",
            "volume": "193",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF30": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "Proc. Natl Acad. Sci. USA",
            "volume": "98",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF31": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "Science",
            "volume": "291",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF32": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "Nature",
            "volume": "410",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF33": {
            "title": "",
            "authors": [],
            "year": 1999,
            "venue": "Nature",
            "volume": "401",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF34": {
            "title": "",
            "authors": [],
            "year": 2002,
            "venue": "Nat. Med.",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}