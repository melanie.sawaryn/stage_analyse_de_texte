{
    "paper_id": "PMC5008692",
    "metadata": {
        "title": "Gene expression patterns in peripheral blood leukocytes in patients with recurrent ciguatera fish poisoning: Preliminary studies",
        "authors": [
            {
                "first": "Maria-Cecilia",
                "middle": [],
                "last": "Lopez",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ricardo",
                "middle": [
                    "F."
                ],
                "last": "Ungaro",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Henry",
                "middle": [
                    "V."
                ],
                "last": "Baker",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Lyle",
                "middle": [
                    "L."
                ],
                "last": "Moldawer",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Alison",
                "middle": [],
                "last": "Robertson",
                "suffix": "",
                "email": "arobertson@disl.org",
                "affiliation": {}
            },
            {
                "first": "Margaret",
                "middle": [],
                "last": "Abbott",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sparkle",
                "middle": [
                    "M."
                ],
                "last": "Roberts",
                "suffix": "",
                "email": "sroberts@som.umaryland.edu",
                "affiliation": {}
            },
            {
                "first": "Lynn",
                "middle": [
                    "M."
                ],
                "last": "Grattan",
                "suffix": "",
                "email": "lgrattan@som.umaryland.edu",
                "affiliation": {}
            },
            {
                "first": "J.",
                "middle": [
                    "Glenn"
                ],
                "last": "Morris",
                "suffix": "Jr.",
                "email": "jgmorris@epi.ufl.edu",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Ciguatera fish poisoning (CFP) is caused by consumption of tropical reef fish carrying ciguatoxin, a toxin which originates in species of the dinoflagellate Gambierdiscus, and is passed up through the marine food chain. Global estimates of incidence are in the range of 50,000\u2013500,000 cases per year (Fleming et al., 1998). In studies conducted by the research team in 2011\u20132012 on St. Thomas, U.S. Virgin Islands, incidence was 12 cases/1000 people/year, with 41% of cases representing recurrent illness (Radke et al., 2013a).",
            "cite_spans": [
                {
                    "start": 301,
                    "end": 321,
                    "mention": "Fleming et al., 1998",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 506,
                    "end": 525,
                    "mention": "Radke et al., 2013a",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Illness is characterized by a distinctive combination of gastrointestinal (diarrhea and/or vomiting) and neurologic (circumoral and/or extremity paresthesias, weakness and pain in legs, fatigue) symptoms; while gastrointestinal symptoms tend to resolve within 48 h, neurologic symptoms can persist in acute cases for several weeks (Morris et al., 1982a, Bagnis et al., 1979, Radke et al., 2013b). Multiple studies have found that patients who have had ciguatera once are significantly more likely to have recurrent episodes (Bagnis et al., 1979, Glaziou and Martin, 1993, Morris et al., 1982b, Radke et al., 2013a). A subset of patients develop \u201cchronic\u201d ciguatera, which is marked by persistence of symptoms, fatigue (reminiscent of chronic fatigue syndromes (Pearn, 1997)), and long-term disability.",
            "cite_spans": [
                {
                    "start": 332,
                    "end": 352,
                    "mention": "Morris et al., 1982a",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 354,
                    "end": 373,
                    "mention": "Bagnis et al., 1979",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 375,
                    "end": 394,
                    "mention": "Radke et al., 2013b",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 525,
                    "end": 544,
                    "mention": "Bagnis et al., 1979",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 546,
                    "end": 570,
                    "mention": "Glaziou and Martin, 1993",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 572,
                    "end": 592,
                    "mention": "Morris et al., 1982b",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 594,
                    "end": 613,
                    "mention": "Radke et al., 2013a",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 761,
                    "end": 772,
                    "mention": "Pearn, 1997",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "There are currently no diagnostic tests for ciguatera: diagnosis is based on clinical presentation, and confirmatory electromyography, where appropriate. Factors responsible for increased rates of recurrence among persons who have had an initial episode of ciguatera are not well understood, nor is it understood why a subset of patients go on to have symptoms of chronic disease. To look for biomarkers of illness, and explore factors that may contribute to clinical presentation, gene expression in peripheral blood leukocytes (PBLs) collected from 10 patients in the U.S. Virgin Islands who had been diagnosed with CFP and 5 healthy control subjects from Florida was assessed.",
            "cite_spans": [],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "Ten patients with CFP were drawn from a larger study of acute and recurrent CFP on the Island of St. Thomas, U.S. Virgin Islands. As previously described (Radke et al., 2013b), patients were enrolled from the Emergency Department of the Roy Lester Schneider Hospital (the only hospital on St. Thomas), with follow-ups at 3, 6, and 12 months after acute presentation. Seven of the 10 patients had been followed for between 3 and 12 months after acute illness, and had recurrent symptoms. Three patients did not have recurrent symptoms after their initial illness. For one patient, blood was drawn for the current study approximately 4 months from the time of acute illness; the remaining two patients were approximately 2 weeks from the time of their acute episode of illness, and still had residual symptoms. The mean age of ciguatera case patients was 55 years; 4 were male and 6 female. Eight of the 10 patients had an underlying medical diagnosis, including diabetes (2 patients), arthritis (2), hypertension (4), atherosclerotic heart disease (1), thyroid disease (1), and asthma (1). Samples were also collected from 5 control persons: mean age was 35; 4 were male and 1 was female; all lived in Florida; all were in good health; and none had any medical history consistent with CFP. Studies were approved by the Institutional Review Board of the University of Florida.",
            "cite_spans": [
                {
                    "start": 155,
                    "end": 174,
                    "mention": "Radke et al., 2013b",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "Whole Blood was collected into a 7.0 mL K3 EDTA vacutainer tube (Becton Dickinson #366450). The whole blood was lysed with Buffer EL (Qiagen #79217) to eliminate red blood cells and isolate the total leukocyte population. For the CFP patient samples only, RNAprotect Cell Reagent (Becton Dickinson #76526) was added to the resultant total leukocyte population to stabilize the cells for subsequent shipment to Florida. The total leukocytes for the CFP and control patients were then processed with the Qiagen RNeasy Plus Mini Kit (Qiagen #74134) to isolate the RNA.",
            "cite_spans": [],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "Total RNA was quantified spectrophotometrically using the NanoDrop 1000 instrument (Thermo Scientific, Wilmington, DE). RNA quality was assessed using an RNA PicoChip on an Agilent 2100 BioAnalyzer (Agilent, Andover, MA). All specimens had RNA Integrity Number (RIN) scores greater than 5. Hundred nanograms of total RNA was processed for GeneChip analysis using the GeneChip\u00ae WT PLUS Reagent Kit (Affymetrix, Santa Clara, CA) following manufacturer's recommendations. 5.5 \u03bc coof resulting cDNA was fragmented, terminally labeled, and targets were hybridized to Affymetrix GeneChip\u00ae Human Transcriptome Array 2.0 (HTA 2.0) for 16 h at 45 \u00b0C and washed according to Affymetrix fluidics protocols FS450_001. Microarrays were normalized using Robust Multi-array Average (RMA) as implemented in Partek Genomics Suite 6.6 (Partek Incorporated, St Louis MO). Only annotated probe sets were used in the subsequent analysis. The resulting 26,831 annotated probe sets represented 25,193 genes. Significant genes (p\n < 0.001) were identified using the class prediction tool implemented in Biometric Research Branch BRB-ArrayTools version 4.3.0 Stable Release, developed by Richard Simon & BRB-ArrayTools Development Team (http://linus.nci.nih.gov/BRB-ArrayTools.html). The ability of genes significant at p\n < 0.001 to distinguish between the classes was determined using leave-one-out-cross-validation studies and Monte Carlo simulations using algorithms implemented in BRB-Array Tools. Gene set analysis was also conducted using BRB ArrayTools.",
            "cite_spans": [],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "Significant differences in PBL gene expression patterns were seen in CFP patients compared with controls, with 3285 of 26,831 genes screened having a significance of p\n < 0.001. Findings are reflected in the principal component analysis shown in Fig. 1\n, and the heat map in Fig. 2\n. Significant differences in gene expression were not seen when CFP patients with recurrent symptoms were compared with those with only acute symptoms. In assessing differences in patterns of expression of genes/gene pathways, the strongest associations were with genes linked with platelet aggregation (h_ephA4Pathway) and chemokine gene expression (h_fMLPPathway) (Table 1\n).",
            "cite_spans": [],
            "section": "Results and discussion",
            "ref_spans": [
                {
                    "start": 246,
                    "end": 252,
                    "mention": "Fig. 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 275,
                    "end": 281,
                    "mention": "Fig. 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 649,
                    "end": 656,
                    "mention": "Table 1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The finding of significant differences in PBL gene expression patterns between patients in the U.S. Virgin Island ciguatera population and a healthy control population from Florida must be approached with caution. It is well recognized that gene expression studies have the potential to yield false positive findings if the ancestry of cases and controls are not appropriately matched, as gene expression can be both heritable and under strong genetic control (Byrnes et al., 2009, Cheung et al., 2005). In these studies, the majority of the CFP patients came either from the Virgin Islands or neighboring islands in the Caribbean; the controls, in contrast, tended to have Caucasian backgrounds. To confirm the findings, it will be necessary to conduct additional studies, with carefully matched cases and controls. Similarly, failure to find significant differences in gene expression between patients with recurrent and acute ciguatera is of uncertain relevance: acute case numbers, in particular, were small, and blood was drawn, in one instance, 4 months after the initial acute episode.",
            "cite_spans": [
                {
                    "start": 461,
                    "end": 480,
                    "mention": "Byrnes et al., 2009",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 482,
                    "end": 501,
                    "mention": "Cheung et al., 2005",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Results and discussion",
            "ref_spans": []
        },
        {
            "text": "There are also uncertainties regarding the physiologic relevance of the gene pathways that were shown to have significantly different expression patterns. Ciguatoxin, the causative agent for the disease, potentiates voltage-gated sodium channels, with resultant effects on neurons; there is also a suggestion that it has direct, calcium-mediated enterotoxin activity (Fasano et al., 1991). While relevance to human cases is uncertain, there is one study of gene expression in mouse brains exposed to ciguatoxin in which there was enrichment of expression pathways related to complement and coagulation cascades (Ryan et al., 2010). Given the clinical similarities between chronic ciguatera and chronic fatigue syndromes, it could be hypothesized that similar physiologic mechanisms were operational in the two syndromes. There was some overlap with gene pathways identified in patients with chronic fatigue (e.g., ARF1 (Vernon et al., 2002); CEACAM family (Kaushik et al., 2005); however, more recent studies have raised questions about the relevance of these findings (Byrnes et al., 2009).",
            "cite_spans": [
                {
                    "start": 368,
                    "end": 387,
                    "mention": "Fasano et al., 1991",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 612,
                    "end": 629,
                    "mention": "Ryan et al., 2010",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 920,
                    "end": 939,
                    "mention": "Vernon et al., 2002",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 957,
                    "end": 977,
                    "mention": "Kaushik et al., 2005",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1070,
                    "end": 1089,
                    "mention": "Byrnes et al., 2009",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Results and discussion",
            "ref_spans": []
        },
        {
            "text": "Ciguatera remains an important clinical entity in areas where there is high dependence on tropical reef fish for food. Identification of a biomarker for CFP (particularly recurrent or chronic CFP), and/or for CFP susceptibility, would have substantial clinical relevance, both in terms of diagnosis, and as a basis for development of therapeutic interventions. Findings of this preliminary study are intriguing: there is a need to follow-up on these results (with appropriate control populations) and to further explore potential physiologic relevance of the identified gene pathways.",
            "cite_spans": [],
            "section": "Results and discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1: A list of gene sets that pass p < 0.005 threshold among any one of the four tests: Least Square (LS) permuation test, the Kolmogorov\u2013Smirnov (KS) permutation test, and Efron\u2013Tibshirani's Gene Set Analysis (GSA) maxmean test.\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig. 1: A 3D principal component analysis (PCA) plot of the RNA data that characterizes the trends exhibited by the expression profiles of ciguatera fish poisoning (gray) and healthy controls (black). Each dot represents a sample and the shade represents the type of the sample.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig. 2: Hierarchical clustering of 3285 genes differentially expressed between CFP and controls at a significance of p < 0.001. Each row of the data matrix represents a gene and each column represents a sample. Expression levels are depicted according to the color scale shown at the bottom. Red and blue indicate expression levels respectively above and below the mean. (For interpretation of the references to color in this figure legend, the reader is referred to the web version of this article.)",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Clinical observations on 3009 cases of ciguatera (fish poisoning) in the South Pacific",
            "authors": [
                {
                    "first": "R.",
                    "middle": [],
                    "last": "Bagnis",
                    "suffix": ""
                },
                {
                    "first": "T.",
                    "middle": [],
                    "last": "Kuberski",
                    "suffix": ""
                },
                {
                    "first": "S.",
                    "middle": [],
                    "last": "Laugier",
                    "suffix": ""
                }
            ],
            "year": 1979,
            "venue": "Am. J. Trop. Med. Hyg.",
            "volume": "28",
            "issn": "",
            "pages": "1067-1073",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Gene expression in peripheral blood leukocytes in monozygotic twins discordant for chronic fatigue: no evidence of a biomarker",
            "authors": [
                {
                    "first": "A.",
                    "middle": [],
                    "last": "Byrnes",
                    "suffix": ""
                },
                {
                    "first": "A.",
                    "middle": [],
                    "last": "Jacks",
                    "suffix": ""
                },
                {
                    "first": "K.",
                    "middle": [],
                    "last": "Dahlman-Wright",
                    "suffix": ""
                },
                {
                    "first": "B.",
                    "middle": [],
                    "last": "Evengard",
                    "suffix": ""
                },
                {
                    "first": "F.A.",
                    "middle": [],
                    "last": "Wright",
                    "suffix": ""
                },
                {
                    "first": "N.L.",
                    "middle": [],
                    "last": "Pedersen",
                    "suffix": ""
                },
                {
                    "first": "P.F.",
                    "middle": [],
                    "last": "Sullivan",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "PLoS One",
            "volume": "4",
            "issn": "6",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Mapping determinants of human gene expression by regional and genome-wide association",
            "authors": [
                {
                    "first": "V.G.",
                    "middle": [],
                    "last": "Cheung",
                    "suffix": ""
                },
                {
                    "first": "R.S.",
                    "middle": [],
                    "last": "Spielman",
                    "suffix": ""
                },
                {
                    "first": "K.G.",
                    "middle": [],
                    "last": "Ewens",
                    "suffix": ""
                },
                {
                    "first": "T.M.",
                    "middle": [],
                    "last": "Weber",
                    "suffix": ""
                },
                {
                    "first": "M.",
                    "middle": [],
                    "last": "Morley",
                    "suffix": ""
                },
                {
                    "first": "J.T.",
                    "middle": [],
                    "last": "Burdick",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Nature",
            "volume": "437",
            "issn": "",
            "pages": "1365-1369",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Diarrhea in ciguatera fish poisoning: preliminary evaluation of pathophysiological mechanisms",
            "authors": [
                {
                    "first": "A.",
                    "middle": [],
                    "last": "Fasano",
                    "suffix": ""
                },
                {
                    "first": "Y.",
                    "middle": [],
                    "last": "Hokama",
                    "suffix": ""
                },
                {
                    "first": "R.",
                    "middle": [],
                    "last": "Russell",
                    "suffix": ""
                },
                {
                    "first": "J.G.",
                    "middle": [],
                    "last": "Morris",
                    "suffix": "Jr."
                }
            ],
            "year": 1991,
            "venue": "Gastroenterology",
            "volume": "100",
            "issn": "2",
            "pages": "471-476",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [
                {
                    "first": "L.E.",
                    "middle": [],
                    "last": "Fleming",
                    "suffix": ""
                },
                {
                    "first": "D.G.",
                    "middle": [],
                    "last": "Baden",
                    "suffix": ""
                },
                {
                    "first": "J.A.",
                    "middle": [],
                    "last": "Bean",
                    "suffix": ""
                },
                {
                    "first": "R.",
                    "middle": [],
                    "last": "Weisman",
                    "suffix": ""
                },
                {
                    "first": "D.G.",
                    "middle": [],
                    "last": "Blythe",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Harmful Algae",
            "volume": "",
            "issn": "",
            "pages": "245-248",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Study of factors that influence the clinical-response to ciguatera fish poisoning",
            "authors": [
                {
                    "first": "P.",
                    "middle": [],
                    "last": "Glaziou",
                    "suffix": ""
                },
                {
                    "first": "P.M.V.",
                    "middle": [],
                    "last": "Martin",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Toxicon",
            "volume": "31",
            "issn": "9",
            "pages": "1151-1154",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Gene expression in peripheral blood mononuclear cells from patients with chronic fatigue syndrome",
            "authors": [
                {
                    "first": "N.",
                    "middle": [],
                    "last": "Kaushik",
                    "suffix": ""
                },
                {
                    "first": "D.",
                    "middle": [],
                    "last": "Fear",
                    "suffix": ""
                },
                {
                    "first": "S.C.M.",
                    "middle": [],
                    "last": "Richards",
                    "suffix": ""
                },
                {
                    "first": "C.R.",
                    "middle": [],
                    "last": "McDermott",
                    "suffix": ""
                },
                {
                    "first": "E.F.",
                    "middle": [],
                    "last": "Nuwaysir",
                    "suffix": ""
                },
                {
                    "first": "P.",
                    "middle": [],
                    "last": "Kellam",
                    "suffix": ""
                },
                {
                    "first": "T.J.",
                    "middle": [],
                    "last": "Harrison",
                    "suffix": ""
                },
                {
                    "first": "R.J.",
                    "middle": [],
                    "last": "Wilkinson",
                    "suffix": ""
                },
                {
                    "first": "D.A.J.",
                    "middle": [],
                    "last": "Tyrrell",
                    "suffix": ""
                },
                {
                    "first": "S.T.",
                    "middle": [],
                    "last": "Holgate",
                    "suffix": ""
                },
                {
                    "first": "J.R.",
                    "middle": [],
                    "last": "Kerr",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Clin. Pathol.",
            "volume": "58",
            "issn": "8",
            "pages": "826-832",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Ciguatera fish poisoning: epidemiology of the disease on St. Thomas, U.S. Virgin Islands",
            "authors": [
                {
                    "first": "J.G.",
                    "middle": [],
                    "last": "Morris",
                    "suffix": "Jr."
                },
                {
                    "first": "P.",
                    "middle": [],
                    "last": "Lewin",
                    "suffix": ""
                },
                {
                    "first": "C.W.",
                    "middle": [],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "P.A.",
                    "middle": [],
                    "last": "Blake",
                    "suffix": ""
                },
                {
                    "first": "R.",
                    "middle": [],
                    "last": "Schneider",
                    "suffix": ""
                }
            ],
            "year": 1982,
            "venue": "Am. J. Trop. Med. Hyg.",
            "volume": "31",
            "issn": "3 Pt 1",
            "pages": "574-578",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Chronic fatigue syndrome: chronic ciguatera poisoning as a differential diagnosis",
            "authors": [
                {
                    "first": "J.H.",
                    "middle": [],
                    "last": "Pearn",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Med. J. Aust.",
            "volume": "166",
            "issn": "6",
            "pages": "309-310",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Ciguatera incidence in the U.S. Virgin Islands has not increased over a 30 year time period, despite rising seawater temperatures",
            "authors": [
                {
                    "first": "E.G.",
                    "middle": [],
                    "last": "Radke",
                    "suffix": ""
                },
                {
                    "first": "L.M.",
                    "middle": [],
                    "last": "Grattan",
                    "suffix": ""
                },
                {
                    "first": "R.L.",
                    "middle": [],
                    "last": "Cook",
                    "suffix": ""
                },
                {
                    "first": "T.B.",
                    "middle": [],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "D.M.",
                    "middle": [],
                    "last": "Anderson",
                    "suffix": ""
                },
                {
                    "first": "J.G.",
                    "middle": [],
                    "last": "Morris",
                    "suffix": "Jr."
                }
            ],
            "year": 2013,
            "venue": "Am. J. Trop. Med. Hyg.",
            "volume": "88",
            "issn": "5",
            "pages": "908-913",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Association of cardiac disease and alcohol use with the development of severe ciguatera",
            "authors": [
                {
                    "first": "E.G.",
                    "middle": [],
                    "last": "Radke",
                    "suffix": ""
                },
                {
                    "first": "L.M.",
                    "middle": [],
                    "last": "Grattan",
                    "suffix": ""
                },
                {
                    "first": "J.G.",
                    "middle": [],
                    "last": "Morris",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "South. Med. J.",
            "volume": "106",
            "issn": "12",
            "pages": "655-657",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Gene expression profiling in brain of mice exposed to the marine neurotoxin ciguatoxin reveals an acute anti-infloammatory, neuroprotective response",
            "authors": [
                {
                    "first": "J.C.",
                    "middle": [],
                    "last": "Ryan",
                    "suffix": ""
                },
                {
                    "first": "J.S.",
                    "middle": [],
                    "last": "Morey",
                    "suffix": ""
                },
                {
                    "first": "M.Y.",
                    "middle": [],
                    "last": "Bottein",
                    "suffix": ""
                },
                {
                    "first": "J.S.",
                    "middle": [],
                    "last": "Ramsdell",
                    "suffix": ""
                },
                {
                    "first": "F.M.",
                    "middle": [],
                    "last": "Dolah",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "BMC Neurosci.",
            "volume": "11",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Utility of the blood for gene expression profiling and biomarker discovery in chronic fatigue syndrome",
            "authors": [
                {
                    "first": "S.D.",
                    "middle": [],
                    "last": "Vernon",
                    "suffix": ""
                },
                {
                    "first": "E.R.",
                    "middle": [],
                    "last": "Unger",
                    "suffix": ""
                },
                {
                    "first": "I.M.",
                    "middle": [],
                    "last": "Dimulescu",
                    "suffix": ""
                },
                {
                    "first": "M.",
                    "middle": [],
                    "last": "Rajeevan",
                    "suffix": ""
                },
                {
                    "first": "W.C.",
                    "middle": [],
                    "last": "Reeves",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Dis. Markers",
            "volume": "18",
            "issn": "4",
            "pages": "193-199",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Clinical features of ciguatera fish poisoning: A study of the disease in the U.S Virgin Islands",
            "authors": [
                {
                    "first": "J.G.",
                    "middle": [],
                    "last": "Morris",
                    "suffix": "Jr."
                },
                {
                    "first": "P.",
                    "middle": [],
                    "last": "Lewin",
                    "suffix": ""
                },
                {
                    "first": "N.T.",
                    "middle": [],
                    "last": "Hargrett",
                    "suffix": ""
                },
                {
                    "first": "C.W.",
                    "middle": [],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "P.A.",
                    "middle": [],
                    "last": "Blake",
                    "suffix": ""
                },
                {
                    "first": "R.",
                    "middle": [],
                    "last": "Schneider",
                    "suffix": ""
                }
            ],
            "year": 1982,
            "venue": "Arch. Intern. Med.",
            "volume": "142",
            "issn": "",
            "pages": "1090-1092",
            "other_ids": {
                "DOI": []
            }
        }
    }
}