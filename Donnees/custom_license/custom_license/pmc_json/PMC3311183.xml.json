{
    "paper_id": "PMC3311183",
    "metadata": {
        "title": "Knowledge of Avian Influenza (H5N1) among Poultry Workers, Hong Kong, China",
        "authors": [
            {
                "first": "Jean",
                "middle": [
                    "H."
                ],
                "last": "Kim",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Fung",
                "middle": [
                    "Kuk"
                ],
                "last": "Lo",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ka",
                "middle": [
                    "Kin"
                ],
                "last": "Cheuk",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ming",
                "middle": [
                    "Sum"
                ],
                "last": "Kwong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "William",
                "middle": [
                    "B."
                ],
                "last": "Goggins",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yan",
                "middle": [
                    "Shan"
                ],
                "last": "Cai",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Shui",
                "middle": [
                    "Shan"
                ],
                "last": "Lee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Sian",
                "middle": [],
                "last": "Griffiths",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "An anonymous, cross-sectional survey was conducted during June\u2013November 2009. Interviewers approached 132 licensed live-poultry retail businesses in wet markets and 23 wholesale establishments. The final sample was 360 poultry workers (194 retailers and 166 wholesalers; response rate 68.1%).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Respondents were asked about their demographics, past month\u2019s work and preventive behavior, and avian influenza\u2013related knowledge on the basis of a World Health Organization factsheet (14). We asked perception questions based on the Health Belief Model and the likelihood of adopting certain behavior patterns in the event of a local bird-to-bird or bird-to-human outbreak of avian influenza.",
            "cite_spans": [
                {
                    "start": 185,
                    "end": 187,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Summative scores were computed for avian influenza\u2013related knowledge, current preventive behavior patterns, outbreak preparedness, and various perception domains. Higher scores reflected more beneficial levels of each domain. Unconditional multilevel regression indicated no evidence of clustering effect by poultry market. Standard multivariable linear regression was conducted by using SAS version 9.1.3 (SAS Institute, Cary, NC, USA) with knowledge, practice, and preparedness scores as outcomes and potential predictors showing p<0.25 in unadjusted analyses as input variables. Distribution of standardized residuals and their association with predicted values were examined to assess model assumptions.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Most (208, 60.1%) respondents were men 35\u201354 years of age, of whom 192 (55.3%) had worked a mean of 16.1 years in the poultry industry. Respondents showed low mean summative scores for knowledge of avian influenza (Table A1). Nearly two thirds (232, 64.1%) of poultry workers reported that avian influenza virus (H5N1) infects wild birds, but fewer workers reported that this virus could infect live poultry (212, 60.1%), domesticated birds (159, 44.8%), or humans (178, 50.0%).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 215,
                    "end": 223,
                    "mention": "Table A1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "A total of 242 (69.1%) workers reported that consuming undercooked poultry could transmit the virus, and 210 (59.7%) knew that infection could result from touching bird feces. For other transmission routes, awareness was lower, ranging from 14.0% (48) for eating undercooked eggs to 29.1% (102) for slaughtering poultry.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Ninety-six (27.4%) workers were unsure whether avian influenza virus (H5N1) infection had occurred in humans in Hong Kong, 198 (58%) incorrectly believed that nearly everyone survives this infection, and 110 (32.8%) incorrectly believed that a human vaccine for avian influenza was available. Most (208, 89.9%) respondents were familiar with influenza-like symptoms of avian influenza virus (H5N1) infection such as fever, but fewer workers were aware of respiratory and gastrointestinal symptoms of virus infection.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The Internet and other sources (e.g., health talks) of information about avian influenza were strong independent predictors of greater knowledge. However, training did not result in higher knowledge levels.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Poultry workers reported low-to-moderate levels of compliance with hand hygiene and other preventive measures (ranging from 7.3% [36] using eye protection to 65.2% [245] using handwashing with soap after slaughtering poultry). Working in the poultry industry \u226510 years, lower perceived barriers to preventive behavior, and retail poultry work were independent predictors of higher preventive behavior scores.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "With regard to avian influenza\u2013related perceptions, lack of training (277, 83.4%) and the view that compliance with all infection regulations is difficult during peak hours (218, 64.9%) were the most frequently cited barriers to adoption of preventive behavior. A total of 154 (46.4%) workers believed that face masks reduced business, and 153 (46.1%) believed that vaccination was expensive.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Low anxiety about illness was reported by 242 (76.6%) respondents. In the event of a local outbreak, workers expressed various levels of acceptance for precautionary actions, ranging from 15.8% (56) for reducing work hours to 82.4% (290) for seeking medical care for influenza-like symptoms. Ninety-six (27.4%) respondents anticipated taking oseltamivir. Greater perceived benefit of preventive behavior was the strongest independent predictor of higher preparedness scores (Table A2).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 475,
                    "end": 483,
                    "mention": "Table A2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Similar to other regions (8\u201311), poultry workers in Hong Kong showed low risk perceptions for avian influenza, inadequate knowledge, and a wide range of compliance with preventive measures. Because training (6) was not associated with overall preventive behavior or preparedness, there may be an unmet need for occupation-specific health information.",
            "cite_spans": [
                {
                    "start": 26,
                    "end": 27,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 28,
                    "end": 30,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 208,
                    "end": 209,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Higher levels of knowledge demonstrated by workers who accessed health information sources (e.g., Internet) that provide detailed information suggest that comprehensive, occupation-relevant information should be more widely accessible. However, occupational practices of animal workers might not be amenable to change solely on the basis of improvements in knowledge. Only 129 (42.1%) respondents reported that poultry workers could realistically adhere to all government guidelines (6). Interference with work, high cost, and reduction of business were repeatedly cited as impediments to the adoption of preventive behavior. Even in the event of local outbreaks of avian influenza, most workers were not amenable to actions having adverse economic effects such as reducing work hours. Animal workers are thereby unlikely to widely adopt preventive behavior if these measures conflict with their economic interests.",
            "cite_spans": [
                {
                    "start": 484,
                    "end": 485,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Despite the ongoing government regulations regarding avian influenza in Hong Kong (6), a complete ban on live poultry is unrealistic because of the culturally entrenched demand for fresh poultry. Increasing knowledge and risk perceptions while simultaneously reducing occupational barriers to preventive behavior thereby continues to be the cornerstone of effective zoonotic infection control among animal workers.",
            "cite_spans": [
                {
                    "start": 83,
                    "end": 84,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Implications of these findings extend to other poultry-borne pathogens, such as Campylobacter spp. and Salmonella spp., which share common preventive measures. Close adherence to workplace measures will likely reduce outbreak risk for other poultry-borne diseases. Therefore, a framework for greater integration of risk management strategies and worker education of these poultry-borne infections tailored to the local context is worthwhile and cost-effective.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "In the spirit of the One Health Commission, which calls for an integrated, interdisciplinary approach to human\u2013veterinary\u2013environmental health challenges (15), the fight against global pandemics, such as those of avian influenza virus (H5N1), necessitates greater dialogue and collaborative leadership between governments and livestock industries. Development of realistic occupational safety measures is an ongoing challenge for national governments.",
            "cite_spans": [
                {
                    "start": 155,
                    "end": 157,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure: Location of live poultry wet markets (open food stall markets) in relation to population density, Hong Kong, China, June\u2013November 2009.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": 2007,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Poultry-handling practices during avian influenza outbreak, Thailand.",
            "authors": [],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "1601-3",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Knowledge, attitudes, and practices regarding avian influenza (H5N1), Afghanistan.",
            "authors": [],
            "year": 2008,
            "venue": "Emerg Infect Dis",
            "volume": "14",
            "issn": "",
            "pages": "1459-61",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1409.071382"
                ]
            }
        },
        "BIBREF3": {
            "title": "Avian influenza risk perception among poultry workers, Nigeria.",
            "authors": [],
            "year": 2009,
            "venue": "Emerg Infect Dis",
            "volume": "15",
            "issn": "",
            "pages": "616-7",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1504.070159"
                ]
            }
        },
        "BIBREF4": {
            "title": "Environmental sampling for avian influenza virus A (H5N1) in live-bird markets, Indonesia.",
            "authors": [],
            "year": 2010,
            "venue": "Emerg Infect Dis",
            "volume": "16",
            "issn": "",
            "pages": "1889-95",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 2006,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": 2009,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2006,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Wet markets: a continuing source of severe acute respiratory syndrome and influenza?",
            "authors": [],
            "year": 2004,
            "venue": "Lancet",
            "volume": "363",
            "issn": "",
            "pages": "234-6",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)15329-9"
                ]
            }
        },
        "BIBREF9": {
            "title": "Risk of influenza A (H5N1) infection among poultry workers, Hong Kong, 1997\u20131998.",
            "authors": [],
            "year": 2002,
            "venue": "J Infect Dis",
            "volume": "185",
            "issn": "",
            "pages": "1005-10",
            "other_ids": {
                "DOI": [
                    "10.1086/340044"
                ]
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": 2006,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Risk for infection with highly pathogenic influenza A virus (H5N1) in chickens, Hong Kong, 2002.",
            "authors": [],
            "year": 2007,
            "venue": "Emerg Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "412-8",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1303.060365"
                ]
            }
        },
        "BIBREF13": {
            "title": "Case\u2010control study of risk factors for avian influenza A (H5N1) disease, Hong Kong, 1997.",
            "authors": [],
            "year": 1999,
            "venue": "J Infect Dis",
            "volume": "180",
            "issn": "",
            "pages": "505-8",
            "other_ids": {
                "DOI": [
                    "10.1086/314903"
                ]
            }
        },
        "BIBREF14": {
            "title": "Knowledge, attitudes, and practices of avian influenza, poultry workers, Italy.",
            "authors": [],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "1762-5",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1211.060671"
                ]
            }
        }
    }
}