{
    "paper_id": "PMC3294354",
    "metadata": {
        "title": "A Novel Paramyxovirus?",
        "authors": [
            {
                "first": "Christopher",
                "middle": [
                    "F."
                ],
                "last": "Basler",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Adolfo",
                "middle": [],
                "last": "Garc\u00eda-Sastre",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peter",
                "middle": [],
                "last": "Palese",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "A BLAST search restricted to mammalian protein sequences was performed through the NCBI Web page (http://www.ncbi.nlm.nih.gov/BLAST) by using the Nipah virus matrix protein sequence as the query. One sequence, accession number AAK76747, with homology to the Nipah virus matrix protein, was identified. This protein, called Angrem52 (for angiotensin II-induced, renal mesangial cell gene 52), displays 53% amino acid identity and 73% amino acid similarity over 337 amino acids (aa) to the Nipah virus matrix protein (data not shown). The Angrem52 protein sequence is derived from a theoretical translation of an open reading frame (ORF) from nucleotides (nt) 16 to 1038 within the 3170-nt long Angrem52 nucleotide sequence (GenBank accession no. AY040225). This notable homology suggests that Angrem52 is actually a paramyxovirus M gene (Figure 1A).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 837,
                    "end": 846,
                    "mention": "Figure 1A",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Upon further analysis of sequences within the Angrem52 cDNA downstream of the putative matrix gene, several relatively short ORFs were found to encode peptides with homology to paramyxovirus fusion (F) proteins. Modification of the reported Angrem52 sequence in several positions yields what appears to be a full-length or near full-length paramymyxovirus fusion (F) protein gene, which would be separated from the M ORF by 355 nt (Figure 1A). Specifically, the F ORF within the original Angrem52 sequence begins at position 1393 but appears to terminate prematurely with a stop codon at 2118. To obtain what appears to be a \u201cfull-length\u201d F ORF, several modifications were made to the reported sequence in order to to incorporate the additional \u201cF-like\u201d sequences. An A at position 2110 was deleted. A T at position 2155 was deleted. A single nucleotide, either C or T, was added between positions 2296 and 2297. A T was deleted at 2461. The theoretical translation of this modified ORF yields a protein of 546 aa, the same length as the Nipah virus F protein (data not shown). A pairwise alignment of the resulting protein with the F protein of Nipah virus shows 32% identity and 53% similarity over 509 aa (data not shown). Within this protein, a putative fusion peptide is readily identifiable based on homology to those of other paramyxoviruses (Figure 1B). Although the cleavage site adjacent to the fusion peptide is typically a basic amino acid, the reported Angrem52 cDNA sequence has an acidic glutamic acid at this position (Figure 1B). Both Nipah and Hendra viruses possess F proteins that are cleaved at the expected site (Figure 1B) but are apparently processed by novel but ubiquitous proteases. Cleavage of these sites can occur even when the residue immediately left of the cleavage site is mutated to a nonbasic residue (A. Maisner, R.E. Dutch, pers. comm.).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 432,
                    "end": 441,
                    "mention": "Figure 1A",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1350,
                    "end": 1359,
                    "mention": "Figure 1B",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1535,
                    "end": 1544,
                    "mention": "Figure 1B",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1635,
                    "end": 1644,
                    "mention": "Figure 1B",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Other features common to paramyxovirus fusion proteins, type I transmembrane glycoproteins, are a signal sequence, a transmembrane domain, and 2-heptad repeats. The 2- heptad repeats play an essential role in membrane fusion and are able to form trimeric coiled coils (13). For the putative OPmV F, a potential signal sequence from residues 1 to 23 and a potential transmembrane domain is found between residues 497 and 516. Further, the putative OPmV F has heptad repeats in the positions expected for a paramyxovirus F protein (residues 108\u2013190 and 428\u2013481).",
            "cite_spans": [
                {
                    "start": 269,
                    "end": 271,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Paramyxovirus genes are separated by cis-acting elements in the genome. The signals that lie between genes include a gene-end signal, an intergenic sequence, and a gene-start signal. The Angrem52 sequence, which contains the continuous sequence for the M and F genes, possesses a sequence with similarity to the regulatory sequences in other paramyxoviruses (Figure 1A).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 359,
                    "end": 368,
                    "mention": "Figure 1A",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Another reported angiotensin II-induced gene, Angrem104 (accession no. AF367870), appears to be a paramyxovirus phosphoprotein (P) gene. The reported Angrem104 sequence is 1690-nt long (10,11). The ATG that begins the P ORF is at position 90. Based on the reported sequence, an ORF is present from nt 90 to 1133, and the theoretical translation of this ORF yields a protein with homology to paramyxovirus P proteins but shorter than reported P proteins. However, the insertion of a single T residue between nucleotides at positions 1130 and 1131 results in a single reading frame that ends at position 1579 of the reported Angrem104 sequence and encodes a protein of 496 aa. Alignment of the modified protein sequence to the Nipah virus P protein shows 20.1% amino acid identity over the length of the putative OPmV protein.",
            "cite_spans": [
                {
                    "start": 186,
                    "end": 188,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 189,
                    "end": 191,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Paramyxovirus P genes frequently encode multiple proteins (13). For example, C proteins are encoded by alternate ORFs near the 5\u2032 end of P genes in a number of paramyxoviruses. In addition, through the process of \u201cRNA editing,\u201d the site-specific insertion of nontemplate encoded nucleotides by the viral polymerase, additional proteins, such as V and W proteins, can be produced (13). These latter proteins share amino-terminal sequences with the P proteins but differ after the editing site and thus have distinct carboxy-terminal ends (13). In the case of V proteins, the unique carboxy-terminus is characterized by a relatively conserved cysteine-rich domain. Analysis of the modified Angrem104 sequence identifies a C ORF (from positions 109 to 598) potentially encoding a 163-aa protein. In addition, a possible RNA editing signal similar to that found in the P gene of other paramyxoviruses is present (Figure 1A). One or 2 additional G residues added to the newly synthesized mRNA transcribed from this template sequence (i.e., the singly edited mRNA sequence would then be AAAAAAGGG) would give rise to mRNAs encoding a V or W protein (Figure 1C). The V ORF would begin at nt 90 of our modified Angrem104 sequence and end at position 964 of the modified Angrem104 sequence (this numbering does not count the additional G residue found in the edited mRNA). The carboxy-terminus of the predicted V protein is cysteine-rich, as expected for a paramyxovirus V protein (Figure 1D). The W ORF would also begin at nt 90 but would end at nt 1027 of the original Angrem104 sequence (again not including the 2 extra G nucleotides introduced by editing [Figure 1C]).",
            "cite_spans": [
                {
                    "start": 59,
                    "end": 61,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 380,
                    "end": 382,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 538,
                    "end": 540,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 909,
                    "end": 918,
                    "mention": "Figure 1A",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1144,
                    "end": 1153,
                    "mention": "Figure 1C",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1473,
                    "end": 1482,
                    "mention": "Figure 1D",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1651,
                    "end": 1660,
                    "mention": "Figure 1C",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Based on the similarity of the predicted Angrem52 and Angrem104 sequences to paramyxovirus protein sequences, a reasonable conclusion is that Angrem52 and Angrem104 are actually paramyxovirus genes. Phylogenetic comparison of the putative OPmV P, M, and F proteins suggests that the putative OPmV belongs to a previously uncharacterized genus in the paramyxovirus family. Comparison of the M proteins may provide the most compelling argument for the uniqueness of the putative OPmV, given that an intact ORF was present in the Angrem52 nucleotide sequence and did not require additional manipulation before analysis. The putative OPmV M is found to be slightly more similar to the Henipah virus genus than to other paramyxoviruses but distinct from even Nipah and Hendra virus (Figure 2A). Likewise, the putative OPmV F protein shows the highest degree of sequence identity with the F protein of tupaia paramyxovirus (Figure 2B). Finally, analysis of the putative OPmV P gene places the putative OPmV protein on a separate branch with slightly greater similarity to the P proteins of Hendra and Nipah virus (Figure 2C). Final evidence for classifying the putative OPmV in a distinct phylogenetic group is the fact that the nucleotide sequences of its genes do not show substantial similarity to other paramyxoviruses (data not shown). Typically, notable nucleotide identity is seen between members of a paramyxovirus genus but is not seen when nucleotide sequences are compared across genera. For example, morbillivirus M genes share nucleotide identity with one another but not with the M genes of Henipah viruses (data not shown).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 778,
                    "end": 787,
                    "mention": "Figure 2A",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 918,
                    "end": 927,
                    "mention": "Figure 2B",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 1108,
                    "end": 1117,
                    "mention": "Figure 2C",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The Angrem52 and Angrem104 genes were identified by a reverse transcriptase\u2013polymerase chain reaction (RT-PCR)\u2013based method from primary human mesangial cells (10,11). Basal expression of each gene was detected, but each gene was identified as an angiotensin II\u2013induced gene (10,11). The apparent presence of viral genes in a primary human cell culture system is intriguing. The presence of these genes could reflect the presence of a virus in any of several states. These include the presence of an actively replicating, fully competent virus; the presence in the cells of a persistent virus infection; or the presence of a replicating but defective virus (14). Although unlikely, these genes could also reflect the integration into the cellular genome of a viral genome as cDNA (15,16).",
            "cite_spans": [
                {
                    "start": 160,
                    "end": 162,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 163,
                    "end": 165,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 276,
                    "end": 278,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 279,
                    "end": 281,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 658,
                    "end": 660,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 781,
                    "end": 783,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 784,
                    "end": 786,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "We have performed several experiments in an effort to determine whether these paramyxoviruslike sequences are universally present within the human genome or whether they represent a very common infection found in human mesangial cells. Searches of publicly available human and mouse sequence databases have not identified proteins or predicted proteins with homology to the putative OPmV sequences (data not shown). We obtained a human 12-tissue, multiple tissue, Northern blot from Clontech and probed this with recombinant probes corresponding to the putative OPmV M and P genes. No specific signal could be obtained under conditions in which a \u03b2-actin probe efficiently recognized its mRNA (data not shown). We also screened 4 lots of Clonetics primary human mesangial cells (Cambrex, East Rutherford, NJ) for the presence of the putative OPmV mRNAs and products when the cells were untreated and after treatment with a range of concentrations of human angiotensin II (Sigma Chemical Co., St. Louis, MO). RT-PCR analysis using a number of primer sets for each of the putative OPmV genes yielded consistently negative results, and antibodies raised against recombinant forms of the putative OPmV M and P proteins failed to detect the putative OPmV proteins (data not shown). Based on these data, it appears that the putative OPmV genes are not human genes and are not universally expressed in human mesangial cells.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "The source of OPmV, the genes of which were identified as Angrem52 and Angrem104, remains unclear. Infection of the cells after their establishment in culture cannot be excluded, nor can contamination of the PCR reactions used to identify Angrem52 and Angrem104 be ruled out. However, the cells may also have been infected in vivo before the generation of the primary cell culture. In this respect, the putative OPmV might be similar to simian virus 5 (SV5), which can cause persistent infection in monkey kidneys and be detected in monkeys for long periods after initial infection (14). Given the possibility that the putative OPmV infects human cells, the possible association of the putative OPmV with human disease is worth exploring.",
            "cite_spans": [
                {
                    "start": 583,
                    "end": 585,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Angrem52 and Angrem104 appear to be paramyxovirus genes. A) Gene positions of a generic paramyxovirus and predicted genome position of Angrem104 (top), the phosphoprotein (P) gene, Angrem52 (bottom), the matrix protein (M) and fusion protein (F) genes. A potential editing site (nucleotides 783\u2013795), which might allow production from the OPmV P gene of V and W/D proteins, is shown in genomic (negative) sense aligned with the proposed editing sites of Nipah virus (NC_002728) (1) and Hendra viruses (NC_001906). The full-length P open reading frame (ORF) was obtained by inserting an additional nucleotide in the reported Angrem104 sequence (see text). Angrem52 is predicted to be a \u201cread-through\u201d product of the M and F genes, a novel paramyxovirus. The full-length F ORF was obtained by making 5 changes to the reported Angrem52 sequence (see text). Putative gene-end, intergenic (IG), and gene-start transcription regulatory signals lying between OPmV M and F genes are shown aligned to the corresponding signals from Nipah and Hendra virus (shown in genomic sense [12]). B) The putative OPmV F protein contains a fusion peptide. The sequences surrounding the F protein cleavage sites, including most fusion peptides, of several paramyxoviruses, including putative OPmV, were aligned by using the AlignX program of the Vector NTI6 software package. The arrow indicates the cleavage site. Residues in red are absolutely conserved. Residues in blue are conserved in most sequences. C) Putative genome organization of the putative OPmV P gene, allowing translation of P, V, W, and C ORFs. D) Alignment of cysteine-rich carboxy-termini of the putative OPmV and Nipah virus V proteins. The conserved carboxy-terminal regions of the V proteins were aligned by using the AlignX program of the Vector NTI6 software package. Conserved residues are indicated in red, except for conserved cysteines, which are in blue. Underlined residues are conserved among all paramyxovirus V proteins.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Phylogenetic comparison of OPmV proteins to other paramyxovirus proteins. A) Phylogenetic tree showing the relationship of the putative OPmV M protein to the M proteins of other paramyxoviruses representative of the various genera in the family Paramyxoviridae. B) Phylogenetic tree showing the relationship of the putative OPmV F protein to the F proteins of other representative paramyxoviruses. C) Phylogenetic tree showing the relationship of the putative OPmV P protein to the P proteins of other representative paramyxoviruses. Sequence alignments were made with the ClustalW method of the AlignX program of the Vector NTI6 software package. The trees were generated from these alignments by using neighbor-joining methods through the computer program MEGA version 2.1 (available from http://www.megasoftware.net/). The position of the putative OPmV sequences are indicated by arrows; distance bars, which represent 0.2 amino acid changes per position, are shown below the trees. The sequences from which the trees were constructed are as follows: Mossman, Mossman virus (NC_005339); Tupaia, Tupaia paramyxovirus (NC_002199); NiV, Nipah virus (NC_002728); HeV, Hendra virus (NC_001906); SeV, Sendai virus (AB065188); hPIV-1, human parainfluenza virus 1 (NC_003461); hPIV-3, human parainfluenza virus type 3 (NC_001796); bPIV-3, bovine parainfluenza virus type 3 (AF178655); hRSV, human respiratory syncytial virus (GI:133665); BRSV, Bovine respiratory syncytial virus (NC_001989); PMV, Pneumonia virus of mice (AY573814)AMnV, avian metapneumovirus (AY028582); HMnV, human metapneumovirus (NC_004148); SV5, simian paramyxovirus SV5 (D13868); hPIV-2, human parainfluenza virus type 2 (NC_003443); MuV, mumps virus (AY309060); NDV, Newcastle disease virus (NC_002617); MeV, measles virus (AF266288); RPV, rinderpest virus (AB021977, M21514, M34018); DMV, dolphin morbillivirus (NC_005283); CDV, canine distemper virus (NC_005283).",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "The discovery of HIV as the cause of AIDS.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": "2283-5",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMp038194"
                ]
            }
        },
        "BIBREF1": {
            "title": "Screening and identification of the up-regulated genes in human mesangial cells exposed to angiotensin II.",
            "authors": [],
            "year": 2003,
            "venue": "Hypertens Res",
            "volume": "26",
            "issn": "",
            "pages": "225-35",
            "other_ids": {
                "DOI": [
                    "10.1291/hypres.26.225"
                ]
            }
        },
        "BIBREF2": {
            "title": "AngRem104, an angiotensin II-induced novel upregulated gene in human mesangial cells, is potentially involved in the regulation of fibronectin expression.",
            "authors": [],
            "year": 2003,
            "venue": "J Am Soc Nephrol",
            "volume": "14",
            "issn": "",
            "pages": "1443-51",
            "other_ids": {
                "DOI": [
                    "10.1097/01.ASN.0000067860.64692.C0"
                ]
            }
        },
        "BIBREF3": {
            "title": "Molecular characterization of Nipah virus, a newly emergent paramyxovirus.",
            "authors": [],
            "year": 2000,
            "venue": "Virology",
            "volume": "271",
            "issn": "",
            "pages": "334-49",
            "other_ids": {
                "DOI": [
                    "10.1006/viro.2000.0340"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": 1991,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "A non-retroviral RNA virus persists in DNA form.",
            "authors": [],
            "year": 1997,
            "venue": "Nature",
            "volume": "390",
            "issn": "",
            "pages": "298-301",
            "other_ids": {
                "DOI": [
                    "10.1038/36876"
                ]
            }
        },
        "BIBREF7": {
            "title": "Integration of viral genomes.",
            "authors": [],
            "year": 1975,
            "venue": "Nature",
            "volume": "256",
            "issn": "",
            "pages": "471-3",
            "other_ids": {
                "DOI": [
                    "10.1038/256471a0"
                ]
            }
        },
        "BIBREF8": {
            "title": "Isolation of a cDNA clone derived from a blood-borne non-A, non-B viral hepatitis genome.",
            "authors": [],
            "year": 1989,
            "venue": "Science",
            "volume": "244",
            "issn": "",
            "pages": "359-62",
            "other_ids": {
                "DOI": [
                    "10.1126/science.2523562"
                ]
            }
        },
        "BIBREF9": {
            "title": "Genetic analysis of West Nile New York 1999 encephalitis virus.",
            "authors": [],
            "year": 1999,
            "venue": "Lancet",
            "volume": "354",
            "issn": "",
            "pages": "1971-2",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(99)05384-2"
                ]
            }
        },
        "BIBREF10": {
            "title": "The severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": "2431-41",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMra032498"
                ]
            }
        },
        "BIBREF11": {
            "title": "A previously undescribed coronavirus associated with respiratory disease in humans.",
            "authors": [],
            "year": 2004,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "101",
            "issn": "",
            "pages": "6212-6",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.0400762101"
                ]
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [],
            "year": 1994,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "The natural history of Hendra and Nipah viruses.",
            "authors": [],
            "year": 2001,
            "venue": "Microbes Infect",
            "volume": "3",
            "issn": "",
            "pages": "307-14",
            "other_ids": {
                "DOI": [
                    "10.1016/S1286-4579(01)01384-3"
                ]
            }
        },
        "BIBREF14": {
            "title": "Nipah virus: a recently emergent deadly paramyxovirus.",
            "authors": [],
            "year": 2000,
            "venue": "Science",
            "volume": "288",
            "issn": "",
            "pages": "1432-5",
            "other_ids": {
                "DOI": [
                    "10.1126/science.288.5470.1432"
                ]
            }
        },
        "BIBREF15": {
            "title": "A newly discovered human pneumovirus isolated from young children with respiratory tract disease.",
            "authors": [],
            "year": 2001,
            "venue": "Nat Med",
            "volume": "7",
            "issn": "",
            "pages": "719-24",
            "other_ids": {
                "DOI": [
                    "10.1038/89098"
                ]
            }
        }
    }
}