{
    "paper_id": "PMC7108013",
    "metadata": {
        "title": "A Patient with Asymptomatic Severe Acute Respiratory Syndrome (SARS) and Antigenemia from the 2003\u20132004 Community Outbreak of SARS in Guangzhou, China",
        "authors": [
            {
                "first": "Xiao-yan",
                "middle": [],
                "last": "Che",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Biao",
                "middle": [],
                "last": "Di",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Guo-ping",
                "middle": [],
                "last": "Zhao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ya-di",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Li-wen",
                "middle": [],
                "last": "Qiu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wei",
                "middle": [],
                "last": "Hao",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ming",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peng-zhe",
                "middle": [],
                "last": "Qin",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yu-fei",
                "middle": [],
                "last": "Liu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok-hong",
                "middle": [],
                "last": "Chan",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Vincent",
                "middle": [
                    "C.",
                    "C."
                ],
                "last": "Cheng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok-yung",
                "middle": [],
                "last": "Yuen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The present study was performed retrospectively. Seventeen serum specimens were collected from 4 index case patients who exhibited recurrence of SARS with laboratory-confirmed SARS-CoV infection in Guangzhou City, China, from 22 December 2003 through 30 January 2004. An additional 118 serum specimens were collected from 115 contacts of the 4 index case patients with SARS.",
            "cite_spans": [],
            "section": "patients|methods",
            "ref_spans": []
        },
        {
            "text": "To identify the nucleocapsid (N) protein of SARS-CoV in serum specimens, 2 antigen-capture assays were performed: an N antigen\u2013capture ELISA and an N antigen\u2013capture chemiluminescent immunoassay (CIA). The development of these 2 assays was based on the detection of 3 monoclonal antibodies directed against the N protein of SARS-CoV, as described elsewhere [4\u20136]. Serum IgG and IgM antibodies to SARS-CoV were detected using commercially available indirect ELISA kits (BGI-GBI; Biotech) according to the manufacturer's instructions [6]. The sensitivity and specificity of ELISA for the detection of IgG antibodies to SARS-CoV were 99%\u2013100% and 89%\u201397.2%, respectively, and the sensitivity and specificity of ELISA for the detection of IgM antibodies to SARS-CoV were 89.8% and 97.6%, respectively [6\u20138]. An indirect immunofluorescent assay (IFA) for the detection of IgG antibodies to SARS-CoV was performed using a commercially available IFA kit (Euroimmun), according to the manufacturer's instructions [9]. This IFA has a specificity of 100% (with detection of IgG antibodies to SARS-CoV in 200 healthy blood donors) and a sensitivity of 97%\u2013100% (with detection of IgG antibodies to SARS-CoV in 150 serum specimens obtained, at least 10 days after the onset of symptoms, from patients with SARS). In-house IFAs for the detection of IgG specific to human coronaviruses 229E and OC43 were performed as described by us elsewhere [10]. A microneutralization assay was performed according to procedures described elsewhere, with modifications [1]. In brief, a 96-well microtiter plate that contained confluent fetal rhesus kidney\u20134 cells in 100 \u00b5 L of maintenance medium was prepared. Two-fold dilutions of patient serum samples (50 \u00b5 L), which started at 1 : 10 and increased to 1 : 320, were premixed with 50 \u00b5 L TCID50 of SARS-CoV (the HKU-39849 isolate) and were incubated at 37\u00b0C for 1.5 h. Then, 100 \u00b5 L of the virus-serum mixture was inoculated in duplicate wells with fetal rhesus kidney\u20134 cells and was further incubated at 37\u00b0C. A cytopathic effect was observed at 72 and 96 h. The neutralization titer was determined to be the reciprocal of the highest serum dilution that produced 50% cytopathic effect on cells. A neutralizing antibody titer of \u2a7e10 was considered to be positive for SARS-CoV.",
            "cite_spans": [
                {
                    "start": 358,
                    "end": 361,
                    "mention": "4\u20136",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 533,
                    "end": 534,
                    "mention": "6",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 798,
                    "end": 801,
                    "mention": "6\u20138",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1006,
                    "end": 1007,
                    "mention": "9",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 1431,
                    "end": 1433,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1543,
                    "end": 1544,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "patients|methods",
            "ref_spans": []
        },
        {
            "text": "All serum specimens were tested for the N protein of SARS-CoV by use of both N antigen\u2013capture ELISA and N antigen\u2013capture CIA. The findings of these 2 assays had been validated previously with the use of serum specimens obtained from patients with serologically confirmed SARS, and the sensitivity and specificity of the N antigen\u2013capture ELISA were documented [4\u20136]. Although the characteristic of the N antigen\u2013capture CIA was shown to be equivalent to that of the N antigen\u2013capture ELISA (data not shown), the N antigen\u2013capture CIA seemed to be more sensitive than N antigen\u2013capture ELISA in the present study. Both N antigen\u2013capture assays detected N protein in serially obtained serum specimens that were collected from 3 patients 6\u20139 days after the onset of symptoms; CIA was also able to detect N protein in serum samples collected from the second index case patient on the ninth and 10th days after the onset of symptoms, although these samples had negative results according to the N antigen\u2013capture ELISA (table 1). This result was not surprising, because enzyme-amplified chemiluminescent chemical analysis is generally more sensitive than conventional ELISA [11]. For index case patient 4, the first serum sample was obtained on day 17 after the onset of symptoms, and N protein was not detectable in serially obtained serum specimens during this patient's illness.",
            "cite_spans": [
                {
                    "start": 363,
                    "end": 366,
                    "mention": "4\u20136",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1172,
                    "end": 1174,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": [
                {
                    "start": 1017,
                    "end": 1024,
                    "mention": "table 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Of the 118 serum specimens obtained from the 115 contacts of the index case patients, 1 specimen, which was collected on 6 January 2004, was from a female contact of the second index case patient, who experienced onset of symptoms on 26 December 2003. This female contact was found to be positive for the N protein by means of N antigen\u2013capture CIA, and the presence of SARS-CoV infection was further confirmed by serological tests (table 2). IFA detected a 16-fold increase in the SARS-CoV antibody titer in paired serum samples collected over a 1-week period. IgM and IgG antibodies to SARS-CoV were also detected by ELISA. Antibodies to SARS-CoV were also detected in the convalescent-phase serum sample obtained from this female contact case patient 12 weeks later, and the results of the SARS-CoV microneutralization assay were also found to be positive, with a titer of 20 determined for the convalescent-phase serum sample obtained from this female contact case patient. The serological results were verified by the Center for Disease Control and Prevention of Guangzhou, which is one of the SARS reference laboratories in China.",
            "cite_spans": [],
            "section": "Results and Discussion",
            "ref_spans": [
                {
                    "start": 433,
                    "end": 440,
                    "mention": "table 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "To exclude the possibility of a false-positive result, serum specimens from the contact case patient were tested for antibodies against the human coronaviruses 229E and OC43, by use of IFA. Paired serum samples showed no increase in the titer of antibodies to either 229E or OC43. These tests were also performed for serially obtained serum specimens from the 4 index case patients (table 1). Although the paired serum samples obtained from the first and second index case patients did show an increase in the titers of antibodies to 229E and OC43, no neutralizing antibodies against 229E and OC43 were detected in these index case patients during a previous study [1].",
            "cite_spans": [
                {
                    "start": 666,
                    "end": 667,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": [
                {
                    "start": 383,
                    "end": 390,
                    "mention": "table 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Although none of the 4 index case patients showed evidence of secondary spread of the infection [1], the direct detection of SARS-CoV N protein by the highly sensitive CIA, complemented by the results of serologic tests, clearly demonstrated that a fifth case of SARS-CoV infection occurred, although the 4 index cases occurred during the 2003\u20132004 community outbreak of SARS in China. On the other hand, because the contact case patient did not show any signs or symptoms of respiratory illness, her case is the first confirmed and documented asymptomatic case of SARS in this outbreak.",
            "cite_spans": [
                {
                    "start": 97,
                    "end": 98,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": []
        },
        {
            "text": "This asymptomatic case of infection occurred in a 31-year-old woman who was working as a service chief in the local restaurant TDLR [2], where the second index case patient worked as a waitress [2, 3]. Although the asymptomatic case patient had only indirect contact with the second index case patient, she had a history of clear and close animal (i.e., palm civet) contact. She often showed the wild animals to customers who were ordering exotic food. Subsequently, SARS-CoV N protein was detected, by use of N antigen\u2013capture ELISA, in 5 of a total of 6 palm civet serum samples obtained during the same period from the restaurant TDLR; results revealed a high level of N protein, with a mean OD450 value of 1.556, compared with the cutoff value of 0.21. Viral RNA was also detected in all throat and rectal swab specimens obtained from 6 palm civets, by use of RT-PCR for the detection of polyprotein and N genes of SARS-CoV, and 3 full genome sequences and 2 complete S gene sequences of SARS-CoV were identified in the specimens obtained from 5 of 6 palm civets [3]. When this information is considered together with the viral genomic information from the palm civets that was collected from the same restaurant during the same period [2\u20133], the clear conclusion is that the palm civets used for exotic food in the restaurant must have been heavily contaminated with SARS-CoV.",
            "cite_spans": [
                {
                    "start": 133,
                    "end": 134,
                    "mention": "2",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 195,
                    "end": 196,
                    "mention": "2",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 198,
                    "end": 199,
                    "mention": "3",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1068,
                    "end": 1069,
                    "mention": "3",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1241,
                    "end": 1244,
                    "mention": "2\u20133",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": []
        },
        {
            "text": "With the limited samples available, the kinematics of the antigenemia and antibody responses of the second index case patient were compared with those of the asymptomatic case patient. The asymptomatic case patient had antigenemia (i.e., detection of N antigen in serum; antigenemia was detected in the asymptomatic case patient on 6 January 2004) disappear at almost the same time as it disappeared in the second index case patient (who had detectable antigenemia up to 5 January 2004). Meanwhile, the second index case patient had seroconversion occur on 3 January 2004, whereas the asymptomatic case patient had a positive antibody result from 6 January 2004; however, because serum samples were unavailable before this date, the exact date of seroconversion cannot be determined. Nevertheless, because the mean incubation period for SARS is 6 days [12], it seems that the asymptomatic case patient was unlikely to have acquired infection directly from the second index case patient. Combining the epidemiological information regarding the contact history with the molecular diagnostic profiling of either the animals (the palm civets) or the human (the second index case patient), we infer that this asymptomatic case of SARS was more likely to have resulted from an animal-to-human infection (likely due to SARS-CoV carried by the palm civets from the restaurant) than from the second index case patient.",
            "cite_spans": [
                {
                    "start": 853,
                    "end": 855,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": []
        },
        {
            "text": "SARS is a zoonotic disease. SARS-CoV evolved consistently and rapidly within its animal and human hosts, while both the infectivity of the virus and the severity of the disease varied, along with the variation/adaptation of the virus to its hosts [2, 13]. It is particularly significant that even in the \u201cearly phase\u201d of the 2002\u20132003 SARS epidemic [13], human-to-human transmission of infection was observed, and the symptoms were so severe that the disease was eventually named after its primary symptomatic characteristics. However, in the 2003\u20132004 community outbreak of SARS, none of the 4 index case patients with confirmed SARS had severe illness, and they all seemed to have acquired infection with SARS-CoV directly from animals. Although SARS-CoV\u2013specific antibodies previously had been detected at a relatively high rate among the population handling wild animals or had been observed in health care workers [14\u201316], it was impossible to correlate serological results with the corresponding onset of clinical symptoms in these retrospective screenings. Therefore, the asymptomatic case patient described in the present article is the first patient with asymptomatic SARS-CoV infection with detectable antigenemia and seroconversion caused by animal-to-human infection.",
            "cite_spans": [
                {
                    "start": 248,
                    "end": 249,
                    "mention": "2",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 251,
                    "end": 253,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 350,
                    "end": 352,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 920,
                    "end": 925,
                    "mention": "14\u201316",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": []
        },
        {
            "text": "This finding, together with the previously reported findings of serological analysis of the handlers of wild animals and the 4 index case patients from the community outbreak of SARS in 2004, reveals that there likely is an intermediate epidemiological phase, which might be critical for genetic adaptation of the virus to its new hosts before critical mutation enables it to eventually cause severe symptomatic SARS, as was seen in early 2003. Although it is usually difficult to observe the virus in the intermediate phase, SARS-CoV is one of the rare viruses for which both genotypes [2, 13] and the antigen/antibody corresponding to the intermediate phase were detected.",
            "cite_spans": [
                {
                    "start": 588,
                    "end": 589,
                    "mention": "2",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 591,
                    "end": 593,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": []
        },
        {
            "text": "This finding may also be significant for the prevention of SARS epidemics. Although the most contagious human epidemic strain of SARS-CoV from the middle and late phases of the 2002\u20132003 pandemic has not been seen in nature since June 2003 [13], the risk of asymptomatic or mildly symptomatic infection with SARS-CoV should not be ignored. To date, molecular epidemiological data have indicated that, although it might take decades for SARS-CoV to evolve into a contagious strain for human infection, it would not be too difficult for SARS-CoV to reach the stage in which it might cause infection either without symptoms or with mild symptoms in humans. This process could be accelerated if appropriate susceptible animal hosts, such as palm civets, were largely present together with the human population. On the other hand, if the high-risk population with infection can be properly monitored at the asymptomatic stage and the disease can be promptly controlled at the mildly symptomatic stage (both objectives are technically feasible now), future threats of a reemerging SARS epidemic could be much lower than once was surmised.",
            "cite_spans": [
                {
                    "start": 241,
                    "end": 243,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Table 1: Results of detection of severe acute respiratory syndrome (SARS)-associated coronavirus (SARS-CoV) nucleocapsid (N) antigen, antibodies to SARS-CoV, and antibodies to the human coronaviruses 229E and OC43 in the 4 index case patients with SARS from the 2003\u20132004 outbreak in Guangzhou City, China.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Table 2: Results of detection of severe acute respiratory syndrome (SARS)-associated coronavirus (SARS-CoV) nucleocapsid (N) antigen, antibodies to SARS-CoV, and antibodies to human coronaviruses 229E and OC43 in a patient with an asymptomatic case of SARS in the 2003\u20132004 community outbreak of SARS in Guangzhou City, China.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Laboratory diagnosis of four recent, sporadic cases of community-acquired SARS, Guangdong Province, China",
            "authors": [
                {
                    "first": "GD",
                    "middle": [],
                    "last": "Liang",
                    "suffix": ""
                },
                {
                    "first": "QX",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "JG",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "1774-81",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Antigenic cross-reactivity between severe acute respiratory syndrome-associated coronavirus and human coronaviruses 229E and OC43",
            "authors": [
                {
                    "first": "XY",
                    "middle": [],
                    "last": "Che",
                    "suffix": ""
                },
                {
                    "first": "LW",
                    "middle": [],
                    "last": "Qiu",
                    "suffix": ""
                },
                {
                    "first": "ZY",
                    "middle": [],
                    "last": "Liao",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "191",
            "issn": "",
            "pages": "2033-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Comparison of chemiluminescent assays and colorimetric ELISAs for quantification of murine IL-12, human IL-4 and murine IL-4: chemiluminescent substrates provide markedly enhanced sensitivity",
            "authors": [
                {
                    "first": "IP",
                    "middle": [],
                    "last": "Lewkowich",
                    "suffix": ""
                },
                {
                    "first": "JD",
                    "middle": [],
                    "last": "Campbell",
                    "suffix": ""
                },
                {
                    "first": "KT",
                    "middle": [],
                    "last": "HayGlass",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "J Immunol Methods",
            "volume": "247",
            "issn": "",
            "pages": "111-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "The severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Peiris",
                    "suffix": ""
                },
                {
                    "first": "KY",
                    "middle": [],
                    "last": "Yuen",
                    "suffix": ""
                },
                {
                    "first": "AD",
                    "middle": [],
                    "last": "Osterhaus",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Stohr",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": "2431-41",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Molecular evolution of the SARS-coronavirus during the course of the SARS epidemic in China",
            "authors": [],
            "year": 2004,
            "venue": "Science",
            "volume": "303",
            "issn": "",
            "pages": "1666-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Isolation and characterization of viruses related to the SARS coronavirus from animals in southern China",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "YQ",
                    "middle": [],
                    "last": "He",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Science",
            "volume": "302",
            "issn": "",
            "pages": "276-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Asymptomatic SARS coronavirus infection among healthcare workers, Singapore",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wilder-Smith",
                    "suffix": ""
                },
                {
                    "first": "MD",
                    "middle": [],
                    "last": "Teleman",
                    "suffix": ""
                },
                {
                    "first": "BH",
                    "middle": [],
                    "last": "Heng",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "1142-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Mild illness associated with severe acute respiratory syndrome coronavirus infection: lessons from a prospective seroepidemiologic study of health-care workers in a teaching hospital in Singapore",
            "authors": [
                {
                    "first": "KY",
                    "middle": [],
                    "last": "Ho",
                    "suffix": ""
                },
                {
                    "first": "KS",
                    "middle": [],
                    "last": "Singh",
                    "suffix": ""
                },
                {
                    "first": "AG",
                    "middle": [],
                    "last": "Habib",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Infect Dis",
            "volume": "189",
            "issn": "",
            "pages": "642-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Cross-host evolution of severe acute respiratory syndrome coronavirus in palm civet and human",
            "authors": [
                {
                    "first": "HD",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "CC",
                    "middle": [],
                    "last": "Tu",
                    "suffix": ""
                },
                {
                    "first": "GW",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Proc Natl Acad Sci USA",
            "volume": "102",
            "issn": "",
            "pages": "2430-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "SARS-CoV infection in a restaurant from palm civet",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "MY",
                    "middle": [],
                    "last": "Yan",
                    "suffix": ""
                },
                {
                    "first": "HF",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "1860-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Nucleocapsid protein as early diagnostic marker for SARS",
            "authors": [
                {
                    "first": "XY",
                    "middle": [],
                    "last": "Che",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Hao",
                    "suffix": ""
                },
                {
                    "first": "YD",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "1947-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Sensitive and specific monoclonal antibody-based capture enzyme immunoassay for detection of nucleocapsid antigen in sera from patients with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "XY",
                    "middle": [],
                    "last": "Che",
                    "suffix": ""
                },
                {
                    "first": "LW",
                    "middle": [],
                    "last": "Qiu",
                    "suffix": ""
                },
                {
                    "first": "YX",
                    "middle": [],
                    "last": "Pan",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Clin Microbiol",
            "volume": "42",
            "issn": "",
            "pages": "2629-35",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Detection of the nucleocapsid protein of severe acute respiratory syndrome coronavirus in serum: comparison with results of other viral markers",
            "authors": [
                {
                    "first": "YH",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "XE",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Virol Methods",
            "volume": "130",
            "issn": "",
            "pages": "45-50",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Evaluation by indirect immunofluorescent assay and enzyme linked immunosorbent assay of the dynamic changes of serum antibody responses against severe acute respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "HY",
                    "middle": [],
                    "last": "Mo",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "XL",
                    "middle": [],
                    "last": "Ren",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Chin Med J (Engl)",
            "volume": "118",
            "issn": "",
            "pages": "446-50",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Anti-SARS-CoV immunoglobulin G in healthcare workers, Guangzhou, China",
            "authors": [
                {
                    "first": "WQ",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "CY",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "TW",
                    "middle": [],
                    "last": "Wong",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Emerg Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "89-94",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Novel immunofluorescence assay using recombinant nucleocapsid-spike fusion protein as antigen to detect antibodies against severe acute respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "He",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Manopo",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Clin Diagn Lab Immunol",
            "volume": "12",
            "issn": "",
            "pages": "321-8",
            "other_ids": {
                "DOI": []
            }
        }
    }
}