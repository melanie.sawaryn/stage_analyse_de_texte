{
    "paper_id": "PMC4175981",
    "metadata": {
        "title": "A divergent clade of circular single-stranded DNA viruses from pig feces",
        "authors": [
            {
                "first": "Andrew",
                "middle": [
                    "K."
                ],
                "last": "Cheung",
                "suffix": "",
                "email": "Andrew.Cheung@ars.usda.gov",
                "affiliation": {}
            },
            {
                "first": "Terry",
                "middle": [
                    "F."
                ],
                "last": "Ng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kelly",
                "middle": [
                    "M."
                ],
                "last": "Lager",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Darrell",
                "middle": [
                    "O."
                ],
                "last": "Bayles",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "David",
                "middle": [
                    "P."
                ],
                "last": "Alt",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Eric",
                "middle": [
                    "L."
                ],
                "last": "Delwart",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Roman",
                "middle": [
                    "M."
                ],
                "last": "Pogranichniy",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marcus",
                "middle": [
                    "E."
                ],
                "last": "Kehrli",
                "suffix": "Jr.",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The application of pyrosequencing technology to study the pig virome has led to the discovery of unique viruses that may or may not necessarily play a role in disease [8]. Recently, previously unknown single\u2013stranded (ss) circular DNA viruses, similar to chimpanzee stool\u2013associated circular virus (ChiSCV) [1], were identified in fecal samples collected from sick or healthy pigs. The genomes of these pig\u2013 or porcine\u2013stool\u2013associated viruses (PigSCV and PoSCV1) [7, 9] are about 2.5 kilobases (kb) in size and contain two major open reading frames (ORFs) that encode the capsid protein (Cap) and replication initiator protein (Rep). Both PigSCV and PoSCV1 contain a palindromic sequence capable of forming a stem\u2013loop structure in the small intergenic region (SIR), which suggests they may synthesize their respective genomes by the rolling\u2013circle replication mechanism. Whereas the Rep and Cap of PigSCV are encoded by the same DNA strand, the Rep and Cap of PoSCV1 are transcribed bidirectionally from the large intergenic region (LIR) in opposite orientations.",
            "cite_spans": [
                {
                    "start": 168,
                    "end": 169,
                    "mention": "8",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 308,
                    "end": 309,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 465,
                    "end": 466,
                    "mention": "7",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 468,
                    "end": 469,
                    "mention": "9",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In this study, diarrheal fecal materials were collected from sick pigs (1 day to 6 weeks of age) with no common background from various Midwest farms in the United States that had been submitted to the Indiana Animal Disease Diagnostic Laboratory, Purdue University, West Lafayette, Indiana, between December, 2009 and June, 2010. Following routine diagnostic analysis, rotaviruses, coronaviruses and enteroviruses were detected in these samples that were subsequently submitted to National Animal Disease Center for additional study. Fecal samples from six pigs were pooled and processed to prepare a viral nucleic acid library. Briefly, viral particles were first purified using size filtration and nucleases [8]. The extracted viral nucleic acids were amplified by random PCR with a specific nucleotide sequence tag for identification. Several libraries, each prepared with a different sequence tag for identification, were combined and subjected to 454 pyrosequencing and analyzed as described previously [4]. Sequencing was performed on a Roche FLX sequencer using Titanium chemistry (Roche, Branford, CT). For comparative purposes, the best BLASTx results were used to categorize the sequences (contigs and singletons) into virus family and genus.",
            "cite_spans": [
                {
                    "start": 712,
                    "end": 713,
                    "mention": "8",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1010,
                    "end": 1011,
                    "mention": "4",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Of the 1,296,370 total keypass reads generated, 125,282 reads contained sequence tags belonging to the six pooled fecal samples. Positive sequence reads for a taxonomic group were identified based on deduced protein sequence similarity using a stringent expectation value, best BLASTx expectation scores of \u2264 10\u221210, as the cutoff. Sequence reads at this cutoff level exhibited highly significant protein sequence similarities with known viruses in the database. Viral sequences (coronavirus, enterovirus, rotavirus) corresponding to the viruses identified by the Indiana Animal Disease Diagnostic Laboratory (West Lafayette, IN) were detected. Other viral sequences belonging to the RNA virus families (astrovirus, picobirnavirus, teschovirus, torovirus and sapelovirus) and DNA virus families (anellovirus, circovirus, and parvovirus) were also observed. Several sequences encoding amino acid sequences related to Rep of ChiSCV and PoSCV1 were identified.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The ChiSCV\u2013 and PoSCV1\u2013related nucleotide sequences detected by deep sequencing (designated Tp1 and Tp2) were used to design primers for PCR. DNA amplification employing converging primers (conventional PCR) was used to confirm the presence of contig sequences in the sample, and diverging primers (inverse PCR) were used to amplify and clone the complete circular viral genomes. Nucleic acids were extracted directly from fecal samples using a QIAamp MinEluteVirus Vacuum Kit (QIAGEN, Valencia, CA) and subjected to rolling\u2013circle amplification to amplify circular DNA molecules (Illustra GenomiPhi V2 DNA Amplification Kit, GE Healthcare Biosciences, Piscataway, NJ). The amplified DNA was used as a template for PCR using converging or diverging primers based on 454 pyrosequencing results. The amplicons were resolved and excised from agarose gels, cloned into plasmid TOPO\u2013CLX104 and introduced into Eschericheria coli TOP10 (Invitrogen, Carlsbad, CA) by transformation. Multiple clones were picked and used for sequence determination using Sanger methods. From the Tp1 PCR product, three clones were analyzed, and they all yielded identical sequences. This viral genome was designated porcine stool\u2013associated circular virus 2 (PoSCV2; GenBank accession number KC545226). From the Tp2 PCR product, four variant genomes were obtained, and the individual genomes were designated PoSCV3\u20134L5, \u20133L7, \u2013LT2 and \u20134L13 with GenBank accession numbers KC545229, KC545227, KC545230 and KC545228, respectively.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Similar to the genome organization of other SCVs, the Tp clones (PoSCV2 and all four PoSCV3 clones) were about 2.5 kb in length (Fig. 1a). The viral genomes can be divided into four regions: two large ORFs with deduced amino acid sequences exhibiting homology to the Rep and Cap of ChiSCV, a LIR that encodes multiple overlapping ORFs, and an SIR that contains a palindromic sequence capable of forming a stem\u2013loop structure. The Rep ORF and Cap ORF are transcribed divergently from the LIR and converge at the SIR. In contrast to the LIR of PoSCV1, which encodes two small ORFs (ORF3 and ORF4) in the same orientation as the Cap gene, the LIRs of PoSCV2 and PoSCV3 also contain an additional ORF (ORF5) in the reverse orientation as the Cap gene.\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 134,
                    "end": 135,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "The four PoSCV3 genomes were aligned, and a schematic representation is shown in Fig. 2a. The LIR, Cap region and 5\u2032 portion of the Rep region exhibited few to no nucleotide differences. Genetic differences were concentrated around the stem\u2013loop structure in the SIR and the 3\u2032 portion of the Rep ORF. The four genome regions (SIR, Rep\u2013ORF, Cap\u2013ORF and LIR) are described individually in greater detail below.\n",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 86,
                    "end": 87,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "\nSIR: The SIR sequences of PoSCV2 and PoSCV3 are shown in Fig. 2b. Whereas the Rep ORF of PoSCV3\u20134L5 overlaps the stem\u2013loop structure, the other four PoSCV3 genomes do not. All five genomes contain a palindromic sequence in the SIR that is capable of forming a stem\u2013loop structure whose nucleotide sequence is well conserved. This stem\u2013loop structure may be part of the origin of DNA replication. Among the PoSCV3 genomes, the SIR sequences on the Cap\u2013gene side are more conserved, while sequences on the Rep\u2013gene side exhibit the greatest differences.",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 63,
                    "end": 64,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "\nRep ORF: Phylogenetic and pairwise identity analyses were conducted to determine the relationship of Tp clones to other viruses. A phylogram was created based on the deduced amino acid sequences encoded by the Rep gene (Fig. 2c). The amino acid sequences were aligned using Mafft 5.8 [2] with the E\u2013INS\u2013I alignment strategy and previously described parameters [5, 6]. A maximum-likelihood tree was created using RaxML based on the Mafft alignment with previously described parameters [6, 10]. The resulting tree was midpoint rooted using MEGA4 [11]. Pairwise identity analysis of the PoSCV genes and ORFs was also performed using MEGA4 [11]. The results showed that the Tp clones were most closely related to ChiSCV or PoSCV1, and they clustered into a distinct clade with PoSCV2 and PoSCV3, separated into two different sub\u2013groups. There is limited amino acid sequence identity (23\u201332 %) between the Tp clones and bovine SCV (BoSCV) [3] or PigSCV.",
            "cite_spans": [
                {
                    "start": 286,
                    "end": 287,
                    "mention": "2",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 362,
                    "end": 363,
                    "mention": "5",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 365,
                    "end": 366,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 486,
                    "end": 487,
                    "mention": "6",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 489,
                    "end": 491,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 546,
                    "end": 548,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 638,
                    "end": 640,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 936,
                    "end": 937,
                    "mention": "3",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 226,
                    "end": 227,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "The amino acid sequence identities between Tp:PoSCV1 and Tp:ChiSCV were approximately 50 % and 40 %, respectively (Table 1a). The nucleotide or amino acid sequence identity between PoSCV2 and PoSCV3 was approximately 87 %, and the sequence identity among the PoSCV3 variants was 93\u2013100 %. In addition, rolling-circle replication (RCR) amino acid sequence motifs (RCR\u2013I, RCR\u2013II, RCR\u2013III, walker A and walker B) commonly found among the Rep proteins involved in RCR were detected [9] (Fig. 1b). These motifs were conserved among members of this new clade.\n",
            "cite_spans": [
                {
                    "start": 479,
                    "end": 480,
                    "mention": "9",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 488,
                    "end": 489,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 121,
                    "end": 122,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "\nCap ORF: The deduced Cap protein sequences of selected SCV were compared (Table 1b). There is limited amino acid sequence homology (17\u201326 %) between Tp clones and BoSCV, ChiSCV, PigSCV or PoSCV1. The nucleotide sequence identity of the Cap gene (46\u201348 %) was lower than the amino acid sequence identity (60\u201362 %) between PoSCV2 and PoSCV3. In general, the Rep gene is more conserved than the Cap gene across the ssDNA viruses. Therefore, it was unusual to find that the nucleotide and amino acid sequence identities among the PoSCV3 Cap genes (99\u2013100 %) were higher than those of the Rep genes (94\u2013100 %).",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 81,
                    "end": 82,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "\nLIR: The LIR nucleotide sequence identity between PoSCV2 and PoSCV3 was 70.2 %, and the sequences of the PoSCV3s were identical. Both PoSCV1 and the Tp clones exhibit two overlapping ORFs, ORF3 and ORF4, transcribed in the same orientation as the Cap gene. There were no detectable amino acid homologies between the PoSCV1 and the Tp clones. For ORF3, the amino acid sequence identity between PoSCV2 and PoSCV3 was approximately 68 %, and the sequence identity among the PoSCV3s was 99\u2013100 % (Table 1c). For ORF4, the amino acid sequence identity between PoSCV2 and PoSCV3 was approximately 64 %, and the sequence identity among the PoSCV3 genes was 99\u2013100 % (Table 1c). It is expected that the deduced amino acid sequences of ORF3 and ORF4 would be identical among the PoSCV3 variants since the nucleotide sequences are identical. However, it is surprising that the amino acid identity of these two ORFs between PoSCV2 and PoSCV3 was 64\u201368 %, which is slightly higher than the capsid protein homology of 61 %. This finding lends credence to the speculation that either ORF may code for an important functional domain or protein.",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 500,
                    "end": 501,
                    "mention": "1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 667,
                    "end": 668,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The LIRs of PoSCV2 and PoSCV3 also code for an additional ORF5 that is transcribed in the opposite orientation to the Cap gene and overlaps ORF3 and ORF4. The amino acid sequence identity between PoSCV2 and PoSCV3 was approximately 59 %, and the sequence identity among the PoSCV3 variants was 99\u2013100 % (Table 1c). Thus, the amino acid sequence identity of ORF5 between PoSCV2 and PoSCV3 was almost as high as that of the capsid protein identity of 61 %.",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 310,
                    "end": 311,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "In this work, we report a clade of novel viruses that includes PoSCV2 and PoSCV3, which encode a Rep\u2013like protein and a palindromic sequence capable of forming a stem\u2013loop structure (in the SIR), suggesting that their genomes may replicate via a common RCR mechanism. Interestingly, this clade of viruses encodes three overlapping \u201cconserved\u201d ORFs (ORF3, ORF4 and ORF5) in the LIR. Whereas the amino acid sequence identities between PoSCV2 and PoSCV3 for these ORFs range from 58.9 % to 68.6 %, the amino acid sequence identities among the capsid proteins range from 60.7 % to 64.1 %. Whether these additional ORFs code for functionally important proteins is not known. Likewise, the role of these viruses in any disease is unknown. The growing diversity of SCV\u2013related genomes currently reported in the stool of chimpanzees, cows, and pigs likely portend further identification in other mammalian species. However, it remains to be seen whether these stool\u2013associated viruses replicate in the host or that they are pass\u2013through viruses present in the diet. Confirmation of their host and organ tropisms will require detection of SCV-specific antibodies or finding virions in animal tissues. A high level of co\u2013infections involving numerous known viruses (coronavirus, enterovirus, rotavirus, astrovirus, picobirnavirus, teschovirus, torovirus, sapelovirus, anellovirus, circovirus and parvovirus) was detected in just six animals from this study. This report, and the work of others, demonstrates the growing complexity of the pig virome and the challenge to understand the biology, interactions and significance of these newly discovered viruses.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table\u00a01: Deduced amino acid sequence identities of the (a) Rep proteins, (b) Cap proteins, (c) ORF3s, ORF4s, ORF5s of selected SCV\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig.\u00a01: Genome organization of selected SCVs. (a) The circular genomes of PoSCV2 and PoSCV3 (clones 3L7 and L2T) illustrated alongside several distantly related reference genomes (BoSCV\u2013JN634851, ChiSCV\u2013GQ351277, PigSCV\u2013JQ23166 and PoSCV1\u2013JQ274036). The Rep, Cap, LIR and SIR (with stem\u2013loop) regions are indicated. (b) RCR motifs found in the Rep proteins of selected SCVs",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig.\u00a02: Comparative analysis of the PoSCV3 variant genomes. (a) Schematic representations of the circular genome. The 3L7 genome (2495 nt) was used as a reference, and gaps were introduced for sequence alignment. Termination sites of the Rep ORF are indicated by black triangles. Point mutations are indicated by black dots, and the long stretches of nucleotide differences are indicated in brackets. The areas showing identical mutations in 4L5 and L2T are indicated by black boxes. (b) Sequence analysis showing the SIR. Nucleotide differences are colored. Dashes were introduced to align the sequences. The stem\u2013loop sequences are indicated by shaded boxes, and C\u2013terminal coding sequences of Rep and Cap are indicated by black arrows. (c) A phylogenetic tree derived from the deduced amino acid sequences of the Rep genes from selected single\u2013stranded DNA viruses",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Novel circular DNA viruses in stool samples of wild-living chimpanzees",
            "authors": [
                {
                    "first": "O",
                    "middle": [],
                    "last": "Blinkova",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Victoria",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "BF",
                    "middle": [],
                    "last": "Keele",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Sanz",
                    "suffix": ""
                },
                {
                    "first": "JB",
                    "middle": [],
                    "last": "Ndjango",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Peeters",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Travis",
                    "suffix": ""
                },
                {
                    "first": "EV",
                    "middle": [],
                    "last": "Lonsdorf",
                    "suffix": ""
                },
                {
                    "first": "ML",
                    "middle": [],
                    "last": "Wilson",
                    "suffix": ""
                },
                {
                    "first": "AE",
                    "middle": [],
                    "last": "Pusey",
                    "suffix": ""
                },
                {
                    "first": "BH",
                    "middle": [],
                    "last": "Hahn",
                    "suffix": ""
                },
                {
                    "first": "EL",
                    "middle": [],
                    "last": "Delwart",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "J Gen Virol",
            "volume": "91",
            "issn": "",
            "pages": "74-86",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.015446-0"
                ]
            }
        },
        "BIBREF1": {
            "title": "A rapid bootstrap algorithm for the RA\u00a0\u00d7\u00a0ML Web servers",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Stamatakis",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Hoover",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rougemont",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Syst Biol",
            "volume": "57",
            "issn": "",
            "pages": "758-771",
            "other_ids": {
                "DOI": [
                    "10.1080/10635150802429642"
                ]
            }
        },
        "BIBREF2": {
            "title": "MEGA4: Molecular Evolutionary Genetics Analysis (MEGA) software version 4.0",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Tamura",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Dudley",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Nei",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Kumar",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Mol Biol Evol",
            "volume": "24",
            "issn": "",
            "pages": "1596-1599",
            "other_ids": {
                "DOI": [
                    "10.1093/molbev/msm092"
                ]
            }
        },
        "BIBREF3": {
            "title": "MAFFT version 5: improvement in accuracy of multiple sequence alignment",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Katoh",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kuma",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Toh",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Miyata",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Nucleic Acids Res",
            "volume": "33",
            "issn": "",
            "pages": "511-518",
            "other_ids": {
                "DOI": [
                    "10.1093/nar/gki198"
                ]
            }
        },
        "BIBREF4": {
            "title": "Identification of a novel single-stranded, circular DNA virus from bovine stool",
            "authors": [
                {
                    "first": "HK",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "Park",
                    "suffix": ""
                },
                {
                    "first": "VG",
                    "middle": [],
                    "last": "Nguyen",
                    "suffix": ""
                },
                {
                    "first": "DS",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "HJ",
                    "middle": [],
                    "last": "Moon",
                    "suffix": ""
                },
                {
                    "first": "BK",
                    "middle": [],
                    "last": "Kang",
                    "suffix": ""
                },
                {
                    "first": "BK",
                    "middle": [],
                    "last": "Park",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "J Gen Virol",
            "volume": "93",
            "issn": "",
            "pages": "635-639",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.037838-0"
                ]
            }
        },
        "BIBREF5": {
            "title": "Diversity of viruses detected by deep sequencing in pigs from a common background",
            "authors": [
                {
                    "first": "KM",
                    "middle": [],
                    "last": "Lager",
                    "suffix": ""
                },
                {
                    "first": "TF",
                    "middle": [],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "DO",
                    "middle": [],
                    "last": "Bayles",
                    "suffix": ""
                },
                {
                    "first": "DP",
                    "middle": [],
                    "last": "Alt",
                    "suffix": ""
                },
                {
                    "first": "EL",
                    "middle": [],
                    "last": "Delwart",
                    "suffix": ""
                },
                {
                    "first": "AK",
                    "middle": [],
                    "last": "Cheung",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "J Vet Diagn Invest",
            "volume": "24",
            "issn": "",
            "pages": "1177-1179",
            "other_ids": {
                "DOI": [
                    "10.1177/1040638712463212"
                ]
            }
        },
        "BIBREF6": {
            "title": "Metagenomic identification of a novel anellovirus in Pacific harbor seal (Phoca vitulina richardsii) lung samples and its detection in samples from multiple years",
            "authors": [
                {
                    "first": "TF",
                    "middle": [],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Wheeler",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Greig",
                    "suffix": ""
                },
                {
                    "first": "TB",
                    "middle": [],
                    "last": "Waltzek",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Gulland",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Breitbart",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J Gen Virol",
            "volume": "92",
            "issn": "",
            "pages": "1318-1323",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.029678-0"
                ]
            }
        },
        "BIBREF7": {
            "title": "High variety of known and new RNA and DNA viruses of diverse origins in untreated sewage",
            "authors": [
                {
                    "first": "TF",
                    "middle": [],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Marine",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Simmonds",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Kapusinszky",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Bodhidatta",
                    "suffix": ""
                },
                {
                    "first": "BS",
                    "middle": [],
                    "last": "Oderinde",
                    "suffix": ""
                },
                {
                    "first": "KE",
                    "middle": [],
                    "last": "Wommack",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Delwart",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "J Virol",
            "volume": "86",
            "issn": "",
            "pages": "12161-12175",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.00869-12"
                ]
            }
        },
        "BIBREF8": {
            "title": "Simultaneous identification of DNA and RNA viruses present in pig faeces using process-controlled deep sequencing",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Sachsenroder",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Twardziok",
                    "suffix": ""
                },
                {
                    "first": "JA",
                    "middle": [],
                    "last": "Hammerl",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Janczyk",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wrede",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Hertwig",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Johne",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "PLoS One",
            "volume": "7",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.pone.0034631"
                ]
            }
        },
        "BIBREF9": {
            "title": "The fecal virome of pigs on a high-density farm",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Shan",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Simmonds",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Moeser",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Delwart",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J Virol",
            "volume": "85",
            "issn": "",
            "pages": "11697-11708",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.05217-11"
                ]
            }
        },
        "BIBREF10": {
            "title": "Discovery of a novel circular single-stranded DNA virus from porcine faeces",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Sikorski",
                    "suffix": ""
                },
                {
                    "first": "GR",
                    "middle": [],
                    "last": "Arguello-Astorga",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Dayaram",
                    "suffix": ""
                },
                {
                    "first": "RC",
                    "middle": [],
                    "last": "Dobson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Varsani",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Arch Virol",
            "volume": "158",
            "issn": "1",
            "pages": "283-289",
            "other_ids": {
                "DOI": [
                    "10.1007/s00705-012-1470-0"
                ]
            }
        }
    }
}