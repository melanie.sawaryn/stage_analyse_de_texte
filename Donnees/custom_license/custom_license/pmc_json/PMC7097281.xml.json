{
    "paper_id": "PMC7097281",
    "metadata": {
        "title": "SARS unanswered questions",
        "authors": [
            {
                "first": "Catherine",
                "middle": [],
                "last": "Zandonella",
                "suffix": "",
                "email": "catzan@nasw.org",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Even as the World Health Organization declared that severe acute respiratory syndrome (SARS) is coming under control everywhere but in China, researchers meeting in New York on Saturday said that too little is known about the SARS virus to predict what will happen next. To gain some clues, the scientists looked to other coronaviruses to shed light on the new disease's next move.",
            "cite_spans": [
                {
                    "start": 38,
                    "end": 46,
                    "mention": "declared",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Chief among the questions on their minds: Where did the new virus come from, what causes some people to become \"super spreaders\" of the virus, and how can scientists use years of expertise in developing treatments and vaccines for animal coronaviruses to design therapeutics for SARS?",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\"Perhaps if we spend more time studying disease in wildlife, we might learn enough to prevent diseases like SARS from causing problems in humans,\" said W. Ian Lipkin, director of the Center for Immunopathogenesis and Infectious Diseases at the Mailman School of Public Health at Columbia University.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Lipkin was one of the organizers of Saturday's meetingat the New York Academy of Sciences to trade information on the latest SARS research. However, he addressed the attendees via telephone from home, having quarantined himself after developing a fever and cough following his return from China last week.",
            "cite_spans": [
                {
                    "start": 47,
                    "end": 54,
                    "mention": "meeting",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In all the coronaviruses, which cause a variety of diseases in cows, pigs, chickens, house cats, and other animals, the spike (S) glycoprotein is the key protein involved in docking the virus to a cell and infusing the virus into the cell membrane during infection. It also elicits an immune response from the body, making it an attractive target for a vaccine.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "But coronaviruses are otherwise extremely species-specific-viruses that infect pigs do not tend to infect cows - so finding an animal model for SARS could be difficult. The two previously known human coronaviruses, which cause colds, do not have animal models.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Experimentally, researchers have been able to mutate the S glycoprotein and cause the coronavirus to jump species. But the sequence of the SARS virus is so different from those of the three known subfamilies of coronaviruses, \"It doesn't have enough similarity to any known species to have jumped,\" said Kathryn V. Holmes, a microbiologist at the University of Colorado Health Sciences Center. \"It is much more likely to have evolved separately.\"",
            "cite_spans": [
                {
                    "start": 123,
                    "end": 131,
                    "mention": "sequence",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 156,
                    "end": 165,
                    "mention": "different",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "As to speculation that the virus has already mutated since first appearing in humans, Holmes said RNA viruses mutate at a much higher rate than other viruses, but most of the mutant strains don't radically change the behavior of the virus. \"More likely the mutants will serve as epidemiological marks for tracing who gave the virus to whom.\"",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Even so, small mutations in animal coronaviruses have led to major changes in how the virus behaves. A deletion mutation in the gene coding for the S glycoprotein in a pig virus caused it to shift from colonizing the gastrointestinal tract and causing diarrhea to invading the respiratory tract and causing pneumonia, said Linda Saif, an expert on animal coronaviruses at Ohio State University.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In cattle, the route of infection can determine the severity of the disease, said Saif. An animal infected via inhaled aerosolized particles sheds virus for a longer period of time and experiences more severe illness than an animal infected via the inhaled droplets spread through \"close contact.\" The infection dose also determines the duration and amount of shedding of the virus, said Saif.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Scientists are still unsure how the virus gets deep into the lungs, where it causes alveolar damage that can be fatal. Most cold and flu viruses lodge in the upper respiratory tract, including the nose, sinuses, and throat. The SARS virus may lodge there and then work its way down, or it may diffuse into the bloodstream and reemerge in the lungs. If the latter is true, then blood levels of virus, or titer, could be very important in charting the course of the disease.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "No one has yet correlated titer with severity of the disease because samples from patients were not collected in the early days of the outbreak. \"We were too busy dealing with the disease,\" said Donald Low, who treated SARS patients at Mount Sinai Hospital in Toronto.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Scientists are also still puzzled about why some SARS-infected individuals are able to infect tens or hundreds of other people. The answer does not lie in the genome, said the experts. \"There is no unique genetic sequence for super spreaders,\" said Holmes.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Instead, super spreaders may be coinfected with another disease that makes them more contagious. In cows, dual infection with a coronavirus and flu leads to a longer period of shedding and increased fecal shedding as well as prolonged fever and respiratory disease.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "David Ho, an HIV researcher at the Aaron Diamond AIDS Research Center, thinks super spreading is due not just to the spreader but also to something about the immune systems of the people the super spreader infects. He thinks the spreading has a strong genetic component. \"Why else would everyone in the family succumb?\" he asked.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Scientists hope to use what they know about animal coronaviruses to inform the process of designing therapeutics and vaccines. The history of developing a vaccine for coronavirus disease in cats suggests researchers need to proceed with caution, warned Thomas Monath of Acambis. In that case, the vaccine actually helped the virus enter the host cells. \"We must not increase the risk of infection on re-exposure to the virus in the future.\"",
            "cite_spans": [
                {
                    "start": 117,
                    "end": 125,
                    "mention": "vaccines",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "He and others emphasized the urgent need to find an animal model for the disease. \"Until we get species easier to work with than monkeys,\" said Monath, \"we won't make much headway.\" He estimated that developing a vaccine would cost $60 to 100 million over five to six years.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Ho reported that China is moving ahead with a number of studies using killed viruses and serum from convalescing patients. Last week, Ho went to China to test a strategy of stopping the fusion of the virus into the host cell using peptides that block the virus from entering the cell - a strategy he developed to stop HIV. Five of the 12 peptides tested had antiviral activity in cell culture, Ho told the meeting. He plans to try the peptides in monkeys next.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In the end, treatment will probably involve both attacking the virus and boosting the immune system of the patient, said Catherine Laughlin of the National Institute of Allergy and Infectious Diseases. Existing therapies using a combination of the antiviral drug ribavirin and steroids may actually do more harm than good. Ribavirin is very toxic, and steroidal treatments could actually delay the body's ability to clear the virus, according to studies done with respiratory syncytial virus.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "\"We have to move very carefully clinically to not exacerbate the disease,\" said Laughlin.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}