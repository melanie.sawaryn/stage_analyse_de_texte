{
    "paper_id": "PMC7095116",
    "metadata": {
        "title": "Broad reception for coronavirus",
        "authors": [
            {
                "first": "Tom",
                "middle": [],
                "last": "Gallagher",
                "suffix": "",
                "email": "tgallag@lumc.edu",
                "affiliation": {}
            },
            {
                "first": "Stanley",
                "middle": [],
                "last": "Perlman",
                "suffix": "",
                "email": "stanley-perlman@uiowa.edu",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Among the vast pool of zoonotic viruses \u2014 those that can be transmitted from animals to humans \u2014 are the coronaviruses. These pathogens cause common colds and, rarely, more severe infections, and their entrances into the human population provide case studies in virus evolution and the emergence of infectious disease. A new coronavirus, designated hCoV-EMC, has appeared in humans in Middle Eastern countries and England on several occasions over the past year. Although only a few people have been infected, around half of them have died1. The identification of the receptor for this virus, reported by Raj et al.2 on page 251 of this issue, will help us to understand how this and other coronaviruses can cross species borders.",
            "cite_spans": [
                {
                    "start": 539,
                    "end": 540,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 615,
                    "end": 616,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Coronaviruses made the headlines during the 2002\u201303 epidemic of human severe acute respiratory syndrome (SARS). The origin of the pathogenic human virus (SARS-CoV) that caused these infections was traced back to infected bats3. In expanding its range to humans, the SARS-like bat viruses evolved the capacity to latch onto human lung-cell receptors4. This probably came about through infections in intermediate host animals, in which genetic recombinations and small mutations generated a human-adapted virus that could no longer infect bat cells. The infrequency and requisite specificity of such events may partly explain why we have not seen another SARS-like coronavirus epidemic in the past decade.",
            "cite_spans": [
                {
                    "start": 225,
                    "end": 226,
                    "mention": "3",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 348,
                    "end": 349,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The emergence of hCoV-EMC, which is associated with severe respiratory disease, multi-organ failure and a high mortality rate in patients, has prompted intensive investigation into the virus's replication and cell-targeting strategies. Once again, its closest known relatives are viruses that infect various bat species5. However, in stark contrast to SARS-CoV, hCoV-EMC indiscriminately infects cells from many sources, including bats, pigs, monkeys and humans6. A polytropic coronavirus of this type is highly unusual and, from an epidemiological perspective, seems alarming. The suggestion is that hCoV-EMC has acquired facile interspecies transmissibility by adapting to evolutionarily conserved host-cell components \u2014 including host-cell receptors.",
            "cite_spans": [
                {
                    "start": 319,
                    "end": 320,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 461,
                    "end": 462,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Raj and colleagues' results validate this idea. They found that hCoV-EMC binds to DPP4 (also called CD26), a protein found on the surface of several cell types, including cells in the human airways. The amino-acid sequence of human DPP4 is very similar to its homologues in Pipistrellus pipistrellus bats, and the authors demonstrate that bat DPP4 also functions as a hCoV-EMC receptor (Fig. 1). The sequence similarities extend to the DPP4s of other wild and domesticated animals, which increases the likelihood that dissemination of hCoV-EMC throughout nature involves the use of DPP4 for cell entry.",
            "cite_spans": [],
            "section": "",
            "ref_spans": [
                {
                    "start": 387,
                    "end": 393,
                    "mention": "Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Insight into whether hCoV-EMC has adapted to bind exclusively to an interspecies-conserved domain of DPP4 must await characterization of the binding interface, which will require mutational analyses of DPP4 and the glycoproteins on the hCoV-EMC surface. These analyses will be guided in part by comparisons with the closest known hCoV-EMC relatives \u2014 bat coronaviruses HKU4 and HKU5. In addition, X-ray crystallography of receptor-binding domains of viral proteins in complex with soluble DPP4 will be especially enlightening. Dissecting the hCoV-EMC receptor-binding domain and its interacting DPP4 domain in these ways may reveal opportunities for therapeutic blockade of virus entry into cells, including the development of vaccines based on the receptor-binding domains that can elicit antibodies able to interfere sterically with viral entry.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Beyond the potential therapeutic applications, the identification of the hCoV-EMC receptor may also provide clues about the virus's pathogenesis. This is because the DPP4 receptor resembles the \u03b1-coronavirus receptor APN and the SARS-CoV receptor ACE2. All three receptors are ectopeptidase enzymes that cleave amino acids from biologically active peptides, thereby regulating an array of physiological responses. However, APN, ACE2 and DPP4 do not share obvious structural features, and their peptidase activities are not necessary for virus entry7,8. Coronavirus adaptation to ectopeptidase receptors may, therefore, simply reflect the abundance or subcellular positioning of these enzymes on airway cells.",
            "cite_spans": [
                {
                    "start": 548,
                    "end": 549,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 550,
                    "end": 551,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "That said, once they have robustly infected a cell, viruses interfere with the presentation of such receptors at the cell surface, and decreased ACE2 levels during SARS-CoV infection are correlated with enhanced disease severity9. Further research to determine whether hCoV-EMC disease is similarly related to dysregulated DPP4-mediated physiological responses will address the intriguing hypothesis that aspects of coronavirus pathogenesis are outcomes of adaptation to ectopeptidase receptors.",
            "cite_spans": [
                {
                    "start": 228,
                    "end": 229,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Knowing the identity of the hCoV-EMC receptor will also allow the development of animal models of infection to assess whether there are causal links between DPP4 levels, hCoV-EMC infection and disease. For example, an evaluation of DPP4 distribution in the lungs will help to show whether the receptors' location restricts hCoV-EMC infections to the lower respiratory tract, which might limit the virus's transmissibility. DPP4 is found on non-ciliated airway cells, whereas ACE2 is expressed by ciliated cells (Fig. 1); such cell-target differences may contribute to the differences in transmissibility and pathogenicity of infections caused by hCoV-EMC and SARS-CoV. The potential for other factors \u2014 such as soluble DPP4, which may be abundant in extracellular fluids \u2014 to preclude infection and disease should also be tested. Moreover, DPP4 is known to have roles in recruiting protective immune responses in the host10; as such, the effects of virus-induced receptor dysregulation may feature prominently in elucidating immunopathological aspects of the disease.",
            "cite_spans": [
                {
                    "start": 921,
                    "end": 923,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 512,
                    "end": 518,
                    "mention": "Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Although hCoV-EMC can be transmitted from human to human, fortunately this seems to occur infrequently. Further epidemiological studies should assess whether the human infection is truly rare and always severe or, alternatively, is widespread but generally mild and therefore not detected. Similar epidemiological considerations apply to animals. Although the immediate implication of Raj and colleagues' findings might be to postulate a direct transmission from bats to humans, the conservation of the DPP4 receptor among species also raises questions about the extent of hCoV-EMC in nature and the most proximal animal source of the human infections. The interesting and perhaps troubling findings from studies of this virus thus far are that there may be a plethora of sources for its intrusion into the human population. Is this the case, or are there distinct interspecies barriers to hCoV-EMC infection? If so, what is the nature of the barriers, and how might the virus adapt to cross them and occupy the human-host niche? Virus adaptations involve much more than evolving new receptor-binding domains and so, in further studies of this emergent pathogen, it will be important to consider other genetic determinants of hCoV-EMC transmission to humans, such as virus interactions with the innate immune system.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Raj and colleagues2 have identified the cell-surface protein DPP4 as the receptor for hCoV-EMC, a new coronavirus that causes severe respiratory infections in humans. DPP4 is expressed on non-ciliated cells in the human airway. The virus is also able to use the homologous protein in bats for infection, which suggests that direct and reversible transmission of the virus between bats and humans may occur (although transmission through an intermediate host remains a possibility). By contrast, another pathogenic coronavirus (SARS-CoV), which binds to the ACE2 receptor on ciliated airway cells, probably cannot be transmitted directly and is likely to have jumped from bats to humans through evolutionary processes in intermediate hosts, such as civet cats. Open arrows indicate putative routes of transmission, crosses indicate evidence against transmission and a question mark indicates speculation on transmission.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "",
            "authors": [
                {
                    "first": "AM",
                    "middle": [],
                    "last": "Zaki",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "van Boheemen",
                    "suffix": ""
                },
                {
                    "first": "TM",
                    "middle": [],
                    "last": "Bestebroer",
                    "suffix": ""
                },
                {
                    "first": "AD",
                    "middle": [],
                    "last": "Osterhaus",
                    "suffix": ""
                },
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Fouchier",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "N. Engl. J. Med.",
            "volume": "367",
            "issn": "",
            "pages": "1814-1820",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1211721"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Ansorge",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Clin. Chem. Lab. Med.",
            "volume": "47",
            "issn": "",
            "pages": "253-261",
            "other_ids": {
                "DOI": [
                    "10.1515/CCLM.2009.063"
                ]
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [
                {
                    "first": "VS",
                    "middle": [],
                    "last": "Raj",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Nature",
            "volume": "495",
            "issn": "",
            "pages": "251-254",
            "other_ids": {
                "DOI": [
                    "10.1038/nature12005"
                ]
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Science",
            "volume": "310",
            "issn": "",
            "pages": "676-679",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1118391"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [
                {
                    "first": "MM",
                    "middle": [],
                    "last": "Becker",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Proc. Natl Acad. Sci. USA",
            "volume": "105",
            "issn": "",
            "pages": "19944-19949",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.0808116105"
                ]
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "van Boheemen",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "mBio",
            "volume": "3",
            "issn": "",
            "pages": "e00473-12",
            "other_ids": {
                "DOI": [
                    "10.1128/mBio.00473-12"
                ]
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Kindler",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "mBio",
            "volume": "4",
            "issn": "",
            "pages": "e00611-12",
            "other_ids": {
                "DOI": [
                    "10.1128/mBio.00611-12"
                ]
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Reguera",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "PLoS Pathogens",
            "volume": "8",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.ppat.1002859"
                ]
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farzan",
                    "suffix": ""
                },
                {
                    "first": "SC",
                    "middle": [],
                    "last": "Harrison",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Science",
            "volume": "309",
            "issn": "",
            "pages": "1864-1868",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1116480"
                ]
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kuba",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Nature Med.",
            "volume": "11",
            "issn": "",
            "pages": "875-879",
            "other_ids": {
                "DOI": [
                    "10.1038/nm1267"
                ]
            }
        }
    }
}