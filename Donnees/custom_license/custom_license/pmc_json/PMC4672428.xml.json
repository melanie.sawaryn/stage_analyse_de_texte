{
    "paper_id": "PMC4672428",
    "metadata": {
        "title": "Asymptomatic MERS-CoV Infection in Humans Possibly Linked to Infected Dromedaries Imported from Oman to United Arab Emirates, May 2015",
        "authors": [
            {
                "first": "Zulaikha",
                "middle": [
                    "M."
                ],
                "last": "Al Hammadi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Daniel",
                "middle": [
                    "K.W."
                ],
                "last": "Chu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yassir",
                "middle": [
                    "M."
                ],
                "last": "Eltahir",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Farida",
                "middle": [],
                "last": "Al Hosani",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mariam",
                "middle": [],
                "last": "Al Mulla",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Wasim",
                "middle": [],
                "last": "Tarnini",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Aron",
                "middle": [
                    "J."
                ],
                "last": "Hall",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ranawaka",
                "middle": [
                    "A.P.M."
                ],
                "last": "Perera",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mohamed",
                "middle": [
                    "M."
                ],
                "last": "Abdelkhalek",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "J.S.M.",
                "middle": [],
                "last": "Peiris",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Salama",
                "middle": [
                    "S."
                ],
                "last": "Al Muhairi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Leo",
                "middle": [
                    "L.M."
                ],
                "last": "Poon",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "A 29-year-old man (contact 1) transported 8 dromedaries from Oman to United Arab Emirates on May 7, 2015 (Table 1). The same day, as part of a national policy for controlling MERS, samples were collected from the dromedaries at a screening center located at the United Arab Emirates border. The samples were tested by reverse transcription PCR (RT-PCR) on May 10 and found to be positive for the MERS-CoV open reading frame (ORF) 1A and upstream of E genes (10). This finding led local public health authorities to conduct active surveillance on humans who had contact with the infected dromedaries.",
            "cite_spans": [
                {
                    "start": 458,
                    "end": 460,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 106,
                    "end": 113,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "A sputum sample collected from contact 1 on May 10, 2015, was tested by RT-PCR on May 12 and found to be positive for MERS-CoV; the man was admitted to a hospital the same day. Follow-up respiratory samples obtained on May 13 and 14 were still RT-PCR\u2013positive, but a sample obtained on May 18 was negative. The patient was asymptomatic at hospital admission and throughout his hospital stay (Technical Appendix).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Contact 2 was a 33-year-old man who worked at the screening center mentioned above. He had direct contact with the same group of infected dromedaries during the sampling procedures. A nasal aspirate sample was obtained from the man on May 14, 2015, and found to be RT-PCR positive for MERS-CoV. Contact 2 was hospitalized on May 18. A follow-up sample obtained on May 18 was RT-PCR negative for MERS-CoV. Contact 2 was asymptomatic throughout his hospitalization (Technical Appendix).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Samples from 32 other persons were also tested by RT-PCR (Technical Appendix). None tested positive.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "After the initial positive test results, the dromedaries were quarantined. Seven days later (May 14), follow-up nasal swab samples from 5 dromedaries were still positive by RT-PCR (Table 2); the animals also had mucopurulent nasal discharge. The animals were tested for the presence of MERS-CoV\u2013specific neutralizing antibodies (11); all were seropositive. Two 4-month-old calves (ADFCA-HKU1 and ADFCA-HKU2) had the highest virus loads by real-time RT-PCR and the lowest neutralizing antibody titers (Table 2). Nasal swab samples from these 2 dromedaries were also MERS-CoV\u2013positive by rapid antigen testing (12), which suggests the calves were still shedding virus 7 days after the first detection of virus. Virus culture was not attempted. On May 25, 2015, the 2 calves were RT-PCR negative for MERS-CoV, and the whole group of camels was released from quarantine.",
            "cite_spans": [
                {
                    "start": 329,
                    "end": 331,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 609,
                    "end": 611,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 181,
                    "end": 188,
                    "mention": "Table 2",
                    "ref_id": null
                },
                {
                    "start": 501,
                    "end": 508,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Respiratory specimens from the 2 infected humans and the 5 dromedaries that were still positive at the second sampling were analyzed by dideoxy sequencing as previously described (13); the nucleocapsid gene sequences of all dromedary samples were found to be identical. Samples from dromedaries ADFCA-HKU1\u20133 were selected for further analysis, and a sequence contig encompassing the 3\u2032 end of the ORF1AB gene through the 3\u2032 untranslated region of the MERS-CoV genome (\u22488,900 nt; sequence coverage 4) was obtained from each sample. Contigs from the 3 samples were identical, with the exception of a V221I (GTT\u2192ATT) mutation in the ORF4b protein of the sample from dromedary ADFCA-HKU2. The viral RNA content of the 2 human samples available for analysis was too low to provide long PCR amplicons (cycle threshold 35.5 and 36.9 by upstream of E gene assay). However, partial sequences of MERS-CoV spike (466 nt, contacts 1 and 2), ORF3\u20134a (273 nt, contact 1), and nucleocapsid (451 nt, contacts 1 and 2) gene regions could be detected from the samples. All of these sequences were identical to those deduced from the dromedary specimens. Genomic sequences determined from this study were submitted to GenBank (accession nos. KT275306\u2013KT275315).",
            "cite_spans": [
                {
                    "start": 180,
                    "end": 182,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The 3 sequence contigs obtained from the dromedary samples were phylogenetically closely related to those of viruses detected in humans in the Saudi Arabia, China, and South Korea in 2015 (Figure). All sequences from this cluster, together with the partial ORF3\u20134a sequence detected in the sample from contact 1, shared 2 cluster-specific mutations, 79S (TCA\u2192TCT) and P86L (CCT\u2192 CTT), in the ORF3 protein, suggesting that these viruses may share a common lineage. Apart from the unique V221I mutation, the sequences for viruses from the 3 dromedaries shared a unique ORF4a-Q102E (GAG\u2192CAG) mutation that was not found in any published MERS-CoV genomes. Other than those mutations, all of the ORFs (nonstructural protein 13, spike, ORF3, ORF5, envelope, membrane, nucleocapsid, and ORF8b) of these virus sequences were unremarkable.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 189,
                    "end": 195,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "We report 2 cases of MERS-CoV infection in men who had direct contact with the same group of infected dromedaries. Neither man had a concurrent medical condition or a history of exposure to human MERS cases in the 14 days before their first MERS-CoV\u2013positive test results. Genomic sequences for the viruses derived from the men and dromedaries and findings from the epidemiologic investigation suggest possible zoonotic transmission of MERS-CoV from dromedaries to humans. Although it is unlikely, we cannot exclude the possibility that the men and dromedaries were independently infected by other sources.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Both infected humans were kept in the hospital for \u22482 incubation periods and were asymptomatic during this period. Clinical observations and positive RT-PCR results suggest that the men were asymptomatically infected with MERS-CoV. Asymptomatic infections have been detected previously (14). Our findings provide further evidence that asymptomatic human infections can be caused by zoonotic transmission. It is not clear whether asymptomatic infection can lead to transmission between humans. Nonetheless, our findings highlight the importance of systematic surveillance of persons who have frequent contact with dromedaries. A recent study demonstrated that persons who have frequent exposure to camels are more likely than the general population to be seropositive for MERS-CoV (4). The unique border screening program and multisectoral collaborations highlighted in this investigation serve as a model for effective MERS-CoV surveillance at the animal\u2013human interface.",
            "cite_spans": [
                {
                    "start": 287,
                    "end": 289,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 781,
                    "end": 782,
                    "mention": "4",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Our study had some limitations. We did not test serum samples from the human contacts; such testing would be of interest for follow-up investigation of the patients\u2019 serologic responses. We also obtained limited RNA samples from these persons, which prevented us from conducting more extensive viral sequence analyses.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "MERS-CoV genomic sequences determined in this study are similar to those of viruses detected in 2015 in patients in Saudi Arabia and South Korea with hospital-acquired infections. The infected dromedaries in this study were imported from Oman, which suggests that viruses from this clade are widely circulating on the Arabian Peninsula. Sequence analyses of MERS-CoVs found in South Korea and China do not suggest that viruses from this clade are necessarily more transmissible variants (15). However, given that a single introduction of MERS-CoV from this clade caused >180 human infections in hospital settings (2) and that viruses of this clade are causing other human infections in Saudi Arabia, further phenotypic risk assessment of this particular MERS-CoV clade should be a priority.",
            "cite_spans": [
                {
                    "start": 488,
                    "end": 490,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 614,
                    "end": 615,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure: Phylogenetic analyses of partial Middle East respiratory syndrome coronavirus (MERS-CoV) genomic sequences for viruses detected in dromedaries imported from Oman to United Arab Emirates, May 2015. A partial viral RNA sequence spanning the 3\u2032 end of the open reading frame 1AB gene through the 3\u2032 untranslated region of the MERS-CoV genome (\u22488,900 nt) was used in the analysis. The phylogenetic tree was constructed with MEGA6 software (http://www.megasoftware.net/) by using the neighbor-joining method. Numbers at nodes indicate bootstrap values determined by 1,000 replicates. Only bootstrap values >70 are denoted. Underlining indicates sequences for viruses detected in this study. GenBank accession numbers are shown for published sequences. Symbols indicate MERS-CoVs detected from dromedaries s. Scale bar indicates the estimated genetic distance of these viruses.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Isolation of a novel coronavirus from a man with pneumonia in Saudi Arabia.",
            "authors": [],
            "year": 2012,
            "venue": "N Engl J Med",
            "volume": "367",
            "issn": "",
            "pages": "1814-20",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1211721"
                ]
            }
        },
        "BIBREF1": {
            "title": "Assays for laboratory confirmation of novel human coronavirus (hCoV-EMC) infections.",
            "authors": [],
            "year": 2012,
            "venue": "Euro Surveill",
            "volume": "17",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Seroepidemiology for MERS coronavirus using microneutralisation and pseudoparticle virus neutralisation assays reveal a high prevalence of antibody in dromedary camels in Egypt, June 2013.",
            "authors": [],
            "year": 2013,
            "venue": "Euro Surveill",
            "volume": "18",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Development and validation of a rapid immunochromatographic assay for detection of Middle East respiratory syndrome coronavirus antigen in dromedary camels.",
            "authors": [],
            "year": 2015,
            "venue": "J Clin Microbiol",
            "volume": "53",
            "issn": "",
            "pages": "1178-82",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.03096-14"
                ]
            }
        },
        "BIBREF4": {
            "title": "MERS coronaviruses in dromedary camels, Egypt.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "1049-53",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2006.140299"
                ]
            }
        },
        "BIBREF5": {
            "title": "2014 MERS-CoV outbreak in Jeddah\u2014a link to health care facilities.",
            "authors": [],
            "year": 2015,
            "venue": "N Engl J Med",
            "volume": "372",
            "issn": "",
            "pages": "846-54",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1408636"
                ]
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Presence of Middle East respiratory syndrome coronavirus antibodies in Saudi Arabia: a nationwide, cross-sectional, serological study.",
            "authors": [],
            "year": 2015,
            "venue": "Lancet Infect Dis",
            "volume": "15",
            "issn": "",
            "pages": "559-64",
            "other_ids": {
                "DOI": [
                    "10.1016/S1473-3099(15)70090-3"
                ]
            }
        },
        "BIBREF10": {
            "title": "Evidence for camel-to-human transmission of MERS coronavirus.",
            "authors": [],
            "year": 2014,
            "venue": "N Engl J Med",
            "volume": "370",
            "issn": "",
            "pages": "2499-505",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa1401505"
                ]
            }
        },
        "BIBREF11": {
            "title": "Human infection with MERS coronavirus after exposure to infected camels, Saudi Arabia, 2013.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "1012-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Middle East respiratory syndrome coronavirus in dromedary camels: an outbreak investigation.",
            "authors": [],
            "year": 2014,
            "venue": "Lancet Infect Dis",
            "volume": "14",
            "issn": "",
            "pages": "140-5",
            "other_ids": {
                "DOI": [
                    "10.1016/S1473-3099(13)70690-X"
                ]
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}