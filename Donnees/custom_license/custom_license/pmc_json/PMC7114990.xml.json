{
    "paper_id": "PMC7114990",
    "metadata": {
        "title": "Article 14 Prevention of Spread of Disease",
        "authors": [
            {
                "first": "Ruwantissa",
                "middle": [],
                "last": "Abeyratne",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "This provision imposes a responsibility on States to take measures in preventing the spread of disease through air transport. It is a very resilient provision that was designed to address communicable diseases prevalent in 1944 but leaves the door open to be inclusive to modern day diseases, two of which that spread were SARS and the Avian Flu pandemic. This provision explicitly devolves primary responsibility on States to take effective measures to prevent airborne diseases in aircraft and implicitly requires States to issue guidelines for airlines, by liaising with the international agencies concerned. Non obstante, airlines have to face certain legal issues themselves in terms of their conduct. Primarily, airlines are expected to conform to applicable international health regulations and the laws of the countries in which their aircraft land.00151 Furthermore, the airline owes its passengers a duty of care00152 to exercise all caution in protecting their rights, so that a blatant instance of a person who looks sickly and coughs incessantly at the check-in counter cannot be ignored. Common law principles of tort law vigorously distinguish between negligence, recklessness and wilful blindness. Of these elements of liability, wilful blindness is particularly relevant since it brings to bear the need for an airline to be vigilant in observing passenger profiles in potentially dangerous or threatening situations",
            "cite_spans": [],
            "section": "Foresight Regarding Airborne Diseases",
            "ref_spans": []
        },
        {
            "text": "From an aviation perspective, it was important to be aware of the grave risk that may be posed by the SARS virus in an in-flight situation. To have full realization, the nature of the disease and the manner in which it spreads has to be fully understood. In general, SARS begins with a fever greater than 100.4 \u00b0F (>38.0 \u00b0C). Other symptoms may include headache, an overall feeling of discomfort, and body aches. Some people also experience mild respiratory symptoms. After 2\u20137 days, SARS patients may develop a dry cough and have trouble breathing.",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "The primary way through which SARS appeared to spread is by close person-to-person contact. Most cases of SARS have involved people who cared for or lived with someone with SARS, or had direct contact with infectious material (for example, respiratory secretions) from a person who has SARS. Potential ways in which SARS can be spread include touching the skin of other people or objects that are contaminated with infectious droplets and then touching your eye(s), nose, or mouth. This can happen when someone who is sick with SARS coughs or sneezes droplets onto themselves, other people, or nearby surfaces. It also is possible that SARS can be spread more broadly through the air or by other ways that are currently not known. Thus, the aircraft cabin environment is highly conducive to the spread of the SARS virus.",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "Cases of SARS continued to be reported mainly among people who have had direct close contact with an infected person, such as those sharing a household with a SARS patient and healthcare workers who did not use infection control procedures while taking care of a SARS patient. Any airborne disease such as SARS is impacted by the environment, particularly if such were to be an enclosed one as in an aircraft cabin. The ventilation system plays a critical part in this regard and therefore, it is crucial to an air carrier\u2019s conduct to determine the manner in which an air carrier decides on ventilation systems in its aircraft. For instance, early jet aircraft until the last decade offered 100 % fresh air in the cabin. However, in the 1990s, ironically with more evolved technology, ventilation systems in aircraft were built in such a way as to recycle stale air, thus increasing the chances of survival of bacteria in the aircraft cabin. Even if such a practice were ineluctable, in that recycling is a universal practice which is calculated to conserve fuel, a prudent airline would take other measures, such as change of air filters through which ventilation is provided.",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "Air in the cabin is usually dry and lacking in humidity since the outside air at cruising altitudes has an extremely low water content. The humidity level in the air of an aircraft cabin at cruising level has been recognized as being of 10\u201320 % humidity which is approximately the same as desert air. The lack of humidity per se does not facilitate the transmission of airborne vectors, but makes breathing difficult, particularly for persons suffering from respiratory diseases, such as Asthma. When dry air becomes stale through recycling, the chances of removing droplets of air which is usually accomplished by fresh air becomes remote. A suggested solution for a prudent airline to take in this regard is to reintroduce 100 % fresh air which is humidified.",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "One of the major preoccupations of the World Health Organization (WHO) is to ensure the international prevention of disease. Quarantine regulations, which was the first step toward this aim, has a long history, having been introduced during the tenth Century. WHO adopted International Health Regulations in 1951,00153 the philosophy of which was recognized subsequently as:",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "The purpose of International Health Regulations is to prevent international spread of disease, and in the context of international travel, to do so with the minimum of inconvenience to the passenger. This requires international collaboration in the detection, reduction or elimination of the sources from which infection spreads rather than attempts to prevent the introduction of disease by legalistic barriers that over the years have proved to be ineffective.00154\n",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "Of course, the purpose of this philosophy will be defeated if individual States have no willingness or the political will to notify the outbreak of communicable diseases to WHO, particularly in the absence of a monitoring body, incentives for States to notify or sanctions. Therefore the preeminent obligation of States is to ensure that the outbreak of any communicable disease is notified in a manner that would benefit the world and help prevent the spread of the disease across national boundaries. Regrettably, there have been instances recorded where WHO reports that no new instances of a communicable disease has been recorded while the news media give contrary information simultaneously.00155 One of the reasons adduced for the lack of interest on the part of States to report the incidence of communicable diseases to a world body such as WHO has been identified as the lack of importance attributed to International Health Regulations (IHR) by States who consider the regulations as an obsolete relic.00156\n",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "The international health dimension of SARS involves human rights issues as well. International human rights law has laid down two critical aspects relating to public health: that protection of public health constitutes legitimate grounds for limiting human rights in certain circumstances (such as detention of persons or house arrest tantamount to quarantine exercises would be justified in order to contain a disease); and individuals have an inherent right to health. In this context it is not only the State or nation that has an obligation to notify WHO of communicable disease but the human concerned as well, who has an abiding moral and legal obligation. In 1975, WHO issued a policy statement which subsumed its philosophy on health and human rights which stating:",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "The individual is obliged to notify the health authorities when he is suffering from a communicable disease (including venereal diseases) or has been exposed to infection, and must undergo examination, treatment, surveillance, isolation or hospitalization. In particular, obligatory isolation or hospitalization in such cases constitutes a limitation on freedom of movement and the right to liberty and security of person.00157\n",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "It is critical for an evaluation of the health and aeronautical implications of SARS that the term \u201chealth\u201d be defined in context. While the WHO Constitution identifies as an objective of the Organization \u201cattainment of the highest possible level of health\u201d, the state of health is defined as \u201ca state of complete physical, mental and social well-being and not merely the absence of disease or infirmity\u201d.00158 In an aeronautical perspective, as will be seen later in this chapter, this is a tough act to follow, as international responsibility in the carriage of persons extends only as far as the obligation to prevent injury, wounding or death, and not to the physical or mental well-being of a person.",
            "cite_spans": [],
            "section": "Health Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "During the period November 1 2002\u201322 April 2003, the WHO had recorded 78 SARS related deaths and 2,223 suspected cases of SARS in 18 countries.00159 Following these statistics WHO declared that passengers with symptoms of SARS or those who may have been exposed to the virus should not be allowed to fly.001510 Some countries took immediate action, one of the first being the United States which advised its citizens to defer non essential travel to affected regions. Canada declared a health emergency and Taiwan advised against travel to the mainland.001511 It was reported that Airbus Industries had revealed in early May 2003 that several airlines hit by the SARS crisis requested formal postponement of aircraft deliveries.001512 According to this report the SARS crisis was persistent and affected traffic figures adversely, compounding problems already caused by the war in Iraq. The enormity of the problem is brought to bear by the response of the International Civil Aviation Organization (ICAO) which issued guidelines on 2 May 2003 urging member States to:Implement pre-boarding medical screening of passengers at check-in;Provide all incoming passengers with a detailed information leaflet on SARS;Implement medical screening of passengers arriving directly from or via affected areas;Advise pilots to radio ahead if someone on board exhibits SARS symptoms;Instruct crew on dealing with suspected SARS-patients in flight; andDisinfect aircraft on which a suspected SARS-patient has travelled.001513\n\n",
            "cite_spans": [],
            "section": "Aeronautical Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "The International Air Transport Association (IATA) as part of its response to the crisis, set up a SARS operations Centre in Singapore, one of the worst hit States, in order to help coordinate efforts in the region in containing the disease.001514 IATA\u2019s aim was to assist in the establishment of effective and efficient screening processes that could be the result of combined public health expertise offered by Governments along with operational expertise of airports and airlines.001515 Furthermore, IATA and WHO met in Bangkok in April 2003 to coordinate and refine plans to curb the possibility of the disease affecting air transport, where IATA identified the disease as aglobal problem, requiring a global solution, needing the coordinated support and understanding of governments\u2026which meant that the imposition of reactionary and inefficient countermeasures must be avoided 001516\n\n",
            "cite_spans": [],
            "section": "Aeronautical Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "IATA\u2019s official view pertaining to the effects of SARS on the air transport industry was that the virus posed the biggest threat the airlines have ever faced and that SARS related airline losses would overtake the $10 billion loss suffered as a result of the Iraq war.001517 According to IATA, passenger loads on all airlines plunged as a result directly or indirectly of SARS and Hong Kong carriers such as Cathay Pacific and Dragonair had suffered losses as much as 70 %.001518\n",
            "cite_spans": [],
            "section": "Aeronautical Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "On the insurance front, the London underwriters were reported to have withdrawn aviation insurance coverage for travel to countries affected by SARS.001519 The Air Transport Association of the United States announced thatthe world situation continues to play havoc with the airline market place\u2026and that for the week ending 6 April, systemwide traffic for the biggest US carriers had dropped by 17.4% and domestic travel had fallen almost 15% compared to the same period in 2002.001520\n\n",
            "cite_spans": [],
            "section": "Aeronautical Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "Elsewhere, there were at least two airlines which reduced scheduled flights or operations as a result of the crisis: KLM announced its reduction of flights to Asia and its intent to fly smaller aircraft with lesser capacity to Asian destinations, thus reducing its total capacity by 3 %001521; and QANTAS delayed its aircraft orders and downsized its staff by 400.001522 Cathay Pacific announced the most comprehensive and aggressive cabin health programme ever launched by a commercial carrier in order to ensure the health of passengers and reassure air crews of cabin safety despite the SARS threat.001523\n",
            "cite_spans": [],
            "section": "Aeronautical Implications of SARS ::: SARS",
            "ref_spans": []
        },
        {
            "text": "The greatest influenza pandemic occurred in 1918\u20131919 and caused an estimated forty to fifty million deaths worldwide.001534 Although healthcare has significantly improved in the past decades, epidemiological models developed in the US Centres for Disease Control and Prevention in Atlanta project that an avian flu pandemic will likely cause 2\u20137.4 million deaths globally001535 WHO\u2019s estimates are consistent with these figures.001536\n",
            "cite_spans": [],
            "section": "Consequences of an Influenza Pandemic ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "As for the potential economic impact of an avian flu pandemic, any conjecture on the possible human and economic impact would be fundamentally flawed if the nature of the pandemic and possible economic fallout are not fully certain001537 It has been estimated that the gross attack rate (infection rate), which reflects the percentage of the population that will be infected and become clinically ill will be typical to influenza rates usually seen at 20\u201340 %001538 In the case of the Spanish Flu, the mortality rate was between 2.5 % and 5 %.001539\n",
            "cite_spans": [],
            "section": "Consequences of an Influenza Pandemic ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "From an economic perspective, a flu pandemic may have different consequences from the SARS outbreak which occurred earlier this century. Whereas the impact of SARS was on the demand side, in the form of consumption and the demand for services contracted,001540 a flu pandemic will also affect and impact the supply side, as members of the labour force fall sick and in some cases succumb to the disease. A flu pandemic will also destroy human and physical capital, reducing global growth potential and having a significant impact on the global economy. Furthermore, such a pandemic will make investment drop significantly and will not allow a revival for a long time. Deaths resulting from avian flu will reduce the work force drastically and a widespread pandemic could lower the world GDP by 3.6 points than in a case where there is no pandemic.",
            "cite_spans": [],
            "section": "Consequences of an Influenza Pandemic ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "Another factor that would affect the global economy and in turn the air transport industry is the psychological factor. Regionally, a virulent global pandemic could have serious results on the confidence of Europe, North America and Asia which have built their economies on their growth potential. There will be a significant loss to business as importers, exporters and the service industry experience a serious drop in demand. A direct corollary to this trend would be the closure of many businesses, lowering future investment and employment.",
            "cite_spans": [],
            "section": "Consequences of an Influenza Pandemic ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "The major role in combating a possible avian flu pandemic should be played by both governments and international Organizations, by preventing and mitigating a flu pandemic. Such an effort would naturally require cooperation and coordination, along with a concerted effort on the part of the international community to coordinate assistance with a view to ensuring support for all major areas while obviating duplication of efforts. A key support area would lie in financing, particularly poor countries and the provision of critical commodities to them. Needless to say, air transport would be playing a key role in this endeavour, which is all the more reason to have a contingency plan for the sustenance of global air transport in a crisis situation.",
            "cite_spans": [],
            "section": "What Can Be Done? ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "The Avian flu situation was different from earlier outbreaks of an influenza pandemic. Firstly, the world had been warned in advance. Secondly, this warning gave us ample opportunity to prepare for an outbreak. WHO observed that, since late 2003, the world had progressively moved closer to a pandemic since 1968 when the last pandemic of the twentieth century occurred. WHO also said that, during 2005, ominous changes have been observed in the epidemiology of the disease in animals.001541 WHO advised that, as a response to a pandemic threat, the world should take advantage of the gradual process of the adaptive mutation of the virus and implement early intervention with antiviral drugs, supported by other public health measures.001542\n",
            "cite_spans": [],
            "section": "What Can Be Done? ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "In this regard, measures had already been proposed both by the Food and Agriculture Organization (FAO) and the World Organization for Animal Health (OIE)001543 along with a draft global strategy.001544 WHO had prepared a comprehensive avian influenza preparedness plan which recognizes that air travel might hasten the spread of a new virus and decrease the time available for preparing intervention.001545 At an international meeting of health ministers held in Ottawa in October 2005, it was stressed that there was a need for a multi sectoral approach calculated to: strengthen the capacity for surveillance; develop a global approach to vaccine and anti-viral policy for research and development; and, above all, achieve full transparency between countries and institutions involved in responding to the risk of a pandemic, while carrying out a global programme to conduct disease surveillance.001546\n",
            "cite_spans": [],
            "section": "What Can Be Done? ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "On 18 November 2005, temperature screening of people arriving at Hong Kong at Lowu and Lok Ma Chau were activated001547 using infra-red thermo imagery techniques. This measure amply demonstrates that, from an air transport perspective, technology is available to combat an outbreak of flu around the world as States will find it increasingly easier to implement measures once used during the SARS crisis, particularly as both ICAO and IATA carried out an exhaustive programme of action when the SARS crisis erupted. Both Organizations worked closely with WHO during that crisis and are continuing their efforts at present in the context of the new threat to public health. IATA\u2019s Medical Advisory Group has worked with WHO to develop guidelines for check-in agents, cabin crew, cleaning staff and maintenance staff. ICAO has already put into action a systemic approach to a possible outbreak of communicable disease. At the 35th Session of the ICAO Assembly, held in September/October 2004, ICAO Contracting States adopted Resolution A 35\u201312,001548 which declares that the protection of the health of passengers and crews on international flights is an integral element of safe air travel and that conditions should be in place to ensure its preservation in a timely and cost effective manner. Through this Resolution, the Council has been requested to review existing Standards and Recommended Practices (SARPs) of relevant Annexes to the Chicago Convention and adopt new SARPs as necessary, while maintaining institutional arrangements to coordinate efforts by Contracting States and other members of the international civil aviation community.",
            "cite_spans": [],
            "section": "The Air Transport Perspective ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "It is quite evident that both ICAO and IATA are concentrating on protecting the health of passengers and crew on the basis that the spread of a communicable disease within the aircraft should be avoided. Much has already been done regarding this area of concern in a technological context so much so that it can now be reasonably assumed that there is little possibility of the spread of a communicable disease through the ventilation system of an aircraft. As one commentator has observed:\u2026there is nothing about an aircraft cabin that makes it easier to contract a communicable disease. In fact, quite the opposite appears to be true. The ventilation patterns on aircraft, combined with the circulation of air through High Efficiency Particulate Air (HEPA) filters reduces the spread of airborne pathogens, especially when compared with other public places001549\n\n",
            "cite_spans": [],
            "section": "The Air Transport Perspective ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "While all this is well and good, the question is whether, as was experienced during the outbreak of SARS in Toronto, where two Toronto residents brought SARS from Hong Kong to Toronto after travelling by air, the international community should be more concerned with the transmission of the disease across boundaries, which is the real danger and not merely within the aircraft itself.",
            "cite_spans": [],
            "section": "The Air Transport Perspective ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "The international health dimension of avian flu involves human rights issues as well. International human rights law has laid down two critical aspects relating to public health: that protection of public health constitutes legitimate grounds for limiting human rights in certain circumstances (such as detention of persons or house arrest tantamount to quarantine exercises would be justified in order to contain a disease); and individuals have an inherent right to health. In this context it is not only the State or nation that has an obligation to notify WHO of communicable disease but the human concerned as well, who has an abiding moral and legal obligation. In 1975, WHO issued a policy statement which subsumed its philosophy on health and human rights which stated:",
            "cite_spans": [],
            "section": "International Regime Relating to Public Health ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "The individual is obliged to notify the health authorities when he is suffering from a communicable disease (including venereal diseases) or has been exposed to infection, and must undergo examination, treatment, surveillance, isolation or hospitalization. In particular, obligatory isolation or hospitalization in such cases constitutes a limitation on freedom of movement and the right to liberty and security of person.001550\n",
            "cite_spans": [],
            "section": "International Regime Relating to Public Health ::: The Avian Flu Crisis",
            "ref_spans": []
        },
        {
            "text": "It is critical for an evaluation of the health and aeronautical implications of avian flu that the term \u201chealth\u201d be defined in context. While the WHO Constitution identifies as an objective of the Organization \u201cattainment of the highest possible level of health\u201d, the state of health is defined as \u201ca state of complete physical, mental and social well-being and not merely the absence of disease or infirmity\u201d.001551 In an aeronautical perspective, this is a tough act to follow, as international responsibility in the carriage of persons extends only as far as the obligation to prevent injury, wounding or death, and not to the physical or mental well-being of a person.001552\n",
            "cite_spans": [],
            "section": "International Regime Relating to Public Health ::: The Avian Flu Crisis",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Mental injury caused in accidents during international air carriage - a point of view",
            "authors": [
                {
                    "first": "RIR",
                    "middle": [],
                    "last": "Abeyratne",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "Aviat Q",
            "volume": "4",
            "issn": "",
            "pages": "193-205",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 1995,
            "venue": "United Nations legal order",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Chasing the elusive 1918 virus: preparing for the future by examining the past",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Taubenberger",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "The threat of pandemic influenza: are we ready?",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Attacks on America - privacy implications of heightened security measures in the United States, Europe, and Canada",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Abeyratne",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J Air Law Commerce",
            "volume": "67",
            "issn": "1",
            "pages": "83-115",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "International responsibility in preventing the spread of communicable diseases through air carriage - the SARS crisis",
            "authors": [
                {
                    "first": "RIR",
                    "middle": [],
                    "last": "Abeyratne",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Transp Law J",
            "volume": "30",
            "issn": "1",
            "pages": "53-80",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}