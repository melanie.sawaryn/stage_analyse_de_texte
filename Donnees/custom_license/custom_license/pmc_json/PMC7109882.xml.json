{
    "paper_id": "PMC7109882",
    "metadata": {
        "title": "Human Genes and Influenza",
        "authors": [
            {
                "first": "Samira",
                "middle": [],
                "last": "Mubareka",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peter",
                "middle": [],
                "last": "Palese",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Why some individuals resist infection or recover quickly, whereas others experience severe disease associated with infection, is a fundamental question that medicine has struggled to answer. Pathogens and host immune factors have been extensively investigated for many infectious diseases, to address these questions. However, limited information is available concerning the influence of host genetics on the response to viral infections. Genetic determinants have the potential to play a role at numerous points during the course of viral infection, including viral attachment and entry, replication, disease progression and development of severity, and, finally, transmission. In this issue of the Journal, Albright et al. [1] propose that the severity of influenza illness may have a heritable component. To investigate this hypothesis, the authors used as a resource the Utah Population Data Base, which contains data from founding families and their descendants, comprised primarily of members of the Church of Jesus Christ of Latter-Day Saints (i.e., Mormons), thus representing a relatively ethnically homogeneous population.",
            "cite_spans": [
                {
                    "start": 726,
                    "end": 727,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Albright et al. [1] estimated the relative risk (RR) of death for relatives of 4855 individuals (spanning \u2a7e3 generations) who died of influenza. A substantial proportion of deaths occurred during the 1918 influenza pandemic, when a total of 1937 deaths were documented between 1918 and 1921, and 1293 additional deaths occurred between 1922\u20131932, followed by a dramatic decrease in the number of deaths occurring annually.",
            "cite_spans": [
                {
                    "start": 17,
                    "end": 18,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The RR of death for first-degree relatives was 1.54 (P < .0001). The RR was higher for spouses (1.98) and for secondand third-degree relatives (1.22 and 1.16, respectively). The timing of the deaths of third-degree relatives suggests that the deaths were not the result of a common exposure. To control for shared environment, the RR of death for spouses' relatives was compared and was found to be lower for first-, second-, and third-degree relatives.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Excess relatedness among individuals dying of influenza was estimated using the Geneological Index of Familiality, which demonstrated that relatedness among these individuals, including individuals who died during the 1918 pandemic, was greater than expected (P < .001). The analysis was repeated with close relatives excluded (to control for shared environment), and the results were consistent with previous findings. Consistent results were not observed when the same analysis was repeated for individuals with diphtheria-associated deaths. For such individuals, excess relatedness was demonstrated; however, when close relationships were excluded, no excess relatedness was detected.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Specific genes responsible for the host immune response have been invoked as major determinants of the clinical course of HIV-associated disease and hepatitis B and C virus infections [2, 3]. However, there is very little information with respect to genetic determinants as they relate to severe influenza. Over the past decade, a greater understanding of the immune evasion strategies of influenza virus has developed. This knowledge can be used to propose several candidate genes that may be responsible for severe illness. Clinical and animal studies indicate that cytokine dysregulation is associated with acute respiratory distress syndrome and death among hosts infected with avian influenza virus (H5N1) [4-6].",
            "cite_spans": [
                {
                    "start": 185,
                    "end": 186,
                    "mention": "2",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 188,
                    "end": 189,
                    "mention": "3",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 712,
                    "end": 715,
                    "mention": "4-6",
                    "ref_id": "BIBREF16"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Toll-like receptors (TLRs), particularly TLR3 (which recognizes double-stranded RNA) and TLR7 and TLR8 (which recognize single-stranded RNA), are central to antiviral innate immunity [7]. Singlenucleotide polymorphisms in TLR genes are not uncommon and vary among populations [8]. TLR4 has been implicated in the innate immune response to respiratory syncytial virus (RSV) infection, and polymorphisms in the TLR4 gene have been associated with severe bronchiolitis in RSV-infected infants, although the significance of the Asp299Gly polymorphism appears to be a matter of ongoing debate [9-12]. A missense mutation in the TLR3 gene (F303S) conferring loss of function in in vitro assays has been identified in a Japanese child with influenzaassociated encephalopathy [13]. Interestingly, better survival rates have been demonstrated for TLR3 knockout mice than for wild-type mice, despite the TLR3 knockout mice having higher lung virus titers after influenza virus infection [14]. TLR genes (in particular, TLR3) would therefore be worthy candidate genes to investigate. In addition, RNA helicases, such as retinoic acid inducible gene I (RIG-I), recognize double-stranded RNA and, thus, contribute to the antiviral state of an infected cell [7]. Inhibition of RIG-1 by nonstructural protein 1 (NS1) of influenza virus antagonizes interferon (IFN) production and is a means by which the virus evades the host immune system [15]. Therefore, the RIG-I gene should also be considered to be a candidate for attempts to elucidate aberrations in the immune system that may lead to severe influenza illness.",
            "cite_spans": [
                {
                    "start": 184,
                    "end": 185,
                    "mention": "7",
                    "ref_id": "BIBREF19"
                },
                {
                    "start": 277,
                    "end": 278,
                    "mention": "8",
                    "ref_id": "BIBREF20"
                },
                {
                    "start": 589,
                    "end": 593,
                    "mention": "9-12",
                    "ref_id": "BIBREF21"
                },
                {
                    "start": 769,
                    "end": 771,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 978,
                    "end": 980,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1245,
                    "end": 1246,
                    "mention": "7",
                    "ref_id": "BIBREF19"
                },
                {
                    "start": 1426,
                    "end": 1428,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Other host gene products involved in IFN production and induction antagonized by the influenza virus NS1 are worthy of consideration [16]. Changes in genes involved in other aspects of immunity, such as the complement system, have also been associated with recurrent viral upper respiratory tract infections, and they should not be overlooked [17]. Mannose-binding lectins are part of the innate immune system, binding pathogenassociated carbohydrates and activating the complement system through the classical pathway. Mutations within the MBL gene have been associated with severe acute respiratory syndrome (SARS) [18]. Susceptibility to the SARS coronavirus has been associated with changes in the 2\u20325\u2032- oligoadenylate synthetase 1 (OAS1) and myxovirus resistance 1 (MxA) genes as well [19]. All of these genes would be excellent candidates for an analysis to identify determinants of severity of disease after influenza virus infection.",
            "cite_spans": [
                {
                    "start": 134,
                    "end": 136,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 344,
                    "end": 346,
                    "mention": "17",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 618,
                    "end": 620,
                    "mention": "18",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 791,
                    "end": 793,
                    "mention": "19",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The findings of the study by Albright et al. [1] raise many complex questions. The outcome of influenza illness is likely to be dependent on environmental, nutritional, demographic, immunological, and (especially) virologic factors. Undoubtedly, host factors, including comorbidities, will also have a bearing on outcome, and the relative contribution of different genetic determinants is likely to vary depending on environmental and virologic contexts. A significant proportion of the data collected in this study emanates from the influenza pandemic of 1918, which occurred in a setting very different from that of interpandemic cases. In addition, the use of death certificates imposes significant limitations. During the 1918 pandemic, anyone dying with respiratory symptoms was likely to have been given a code denoting \u201cpneumonia and influenza,\u201d which introduced a potential bias. One could argue that errors related to certification of death would be random rather than systematic and should have minimal bearing on the results. Nonetheless, death certificates are notoriously inaccurate, and the absence of microbiological and/or serologic data is a significant limitation of this study.",
            "cite_spans": [
                {
                    "start": 46,
                    "end": 47,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "There are also a number of potential confounding factors in this investigation. Other heritable factors, such as cardiopulmonary conditions or cystic fibrosis (the most common autosomal recessive lethal disorder in whites), are possible examples. Other examples would include common conditions that have a genetic component, such as asthma and coronary artery disease. There is reasonable evidence that the influenza-associated mortality rate is higher for individuals with established coronary artery disease (than for those with other chronic conditions) [20]. Coinfections may also confound the data, and the contribution of coinfecting pathogens to mortality cannot be excluded, bearing in mind that genetic susceptibility to such organisms as the pneumococcus has been proposed [21]. Environmental and geographic factors, such as socioeconomic variables and environmental and occupational (e.g., mining) exposures, may also cluster in families, and such factors as poverty, malnutrition, and overcrowding may span generations. In addition, access to health care may have been limited for certain families in remote settings, thus contributing to the lethality of influenza virus infection, particularly in individuals at the extremes of age.",
            "cite_spans": [
                {
                    "start": 558,
                    "end": 560,
                    "mention": "20",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 784,
                    "end": 786,
                    "mention": "21",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "The implications of this study by Albright et al. [1] are that heritable factors for severe influenza illness may well exist and that elucidation of these factors remains a challenge. Identification and characterization of genetic determinants of the outcome of infection with influenza virus would lead to further insight into the pathophysiology of the virus and its immune evasion strategies. One would hope that this insight would translate into identification of prognostic factors and improved management of individuals at risk for adverse outcomes after influenza virus infection. Identification of genetic determinants of outcome after influenza virus infection would require a substantial number of severe cases of influenza (in which severity of illness is not the result of other factors, such as underlying cardiovascular disease) to gain sufficient power and produce interpretable data. Resources stemming from the sequencing of the human genome are now available and render this challenging task more feasible. Such approaches as the genomewide and candidate gene study designs are reviewed by Burgner et al. elsewhere [22]. The latter approach may be more feasible, given the growing knowledge regarding IFN antagonism and immune evasion by influenza viruses. One must bear in mind that findings may not apply across all host or virus populations. In the event of a pandemic, however, early identification of and intervention for individuals at higher risk for severe illness and death resulting from influenza due to a predisposing genetic determinant would be invaluable.",
            "cite_spans": [
                {
                    "start": 51,
                    "end": 52,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1134,
                    "end": 1136,
                    "mention": "22",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Evidence for heritable predisposition to death due to influenza",
            "authors": [
                {
                    "first": "FS",
                    "middle": [],
                    "last": "Albright",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Orlando",
                    "suffix": ""
                },
                {
                    "first": "AT",
                    "middle": [],
                    "last": "Pavia",
                    "suffix": ""
                },
                {
                    "first": "GG",
                    "middle": [],
                    "last": "Jackson",
                    "suffix": ""
                },
                {
                    "first": "LA",
                    "middle": [],
                    "last": "Cannon Albright",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Infect Dis",
            "volume": "197",
            "issn": "",
            "pages": "18-24",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "TLR-4 and CD14 polymorphisms in respiratory syncytial virus associated disease",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Puthothu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Forster",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Heinzmann",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Krueger",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Dis Markers",
            "volume": "22",
            "issn": "",
            "pages": "303-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Association between common Toll-like receptor 4 mutations and severe respiratory syncytial virus disease",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Tal",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mandelberg",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Dalal",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Infect Dis",
            "volume": "189",
            "issn": "",
            "pages": "2057-63",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "CommonhumanTolllike receptor 4 polymorphisms\u2014role in susceptibility to respiratory syncytial virus infection and functional immunological relevance",
            "authors": [],
            "year": 2007,
            "venue": "Clin Immunol",
            "volume": "123",
            "issn": "",
            "pages": "252-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "A missense mutation of the Toll-like receptor 3 gene in a patient with influenza-associated encephalopathy",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Hidaka",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Matsuo",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Muta",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Takeshige",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Mizukami",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Nunoi",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Clin Immunol",
            "volume": "119",
            "issn": "",
            "pages": "188-94",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Detrimental contribution of the Toll-like receptor (TLR)3 to influenza A virus-induced acute pneumonia",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Le Goffic",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Balloy",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Lagranderie",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "PLoS Pathog",
            "volume": "2",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Inhibition of retinoic acid-inducible gene I-mediated induction of beta interferon by the NS1 protein of influenza A virus",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Mibayashi",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Martinez-Sobrido",
                    "suffix": ""
                },
                {
                    "first": "YM",
                    "middle": [],
                    "last": "Loo",
                    "suffix": ""
                },
                {
                    "first": "WB",
                    "middle": [],
                    "last": "Cardenas",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Gale",
                    "suffix": "Jr"
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Garcia-Sastre",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Virol",
            "volume": "81",
            "issn": "",
            "pages": "514-24",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Type 1 interferons and the virus-host relationship: a lesson in d\u00e9tente",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Garcia-Sastre",
                    "suffix": ""
                },
                {
                    "first": "CA",
                    "middle": [],
                    "last": "Biron",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Science",
            "volume": "312",
            "issn": "",
            "pages": "879-82",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Homozygous deletion of the CYP21A-TNXA-RP2-C4B gene region conferring C4B deficiency associated with recurrent respiratory infections",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Jaatinen",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Ruuskanen",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Truedsson",
                    "suffix": ""
                },
                {
                    "first": "ML",
                    "middle": [],
                    "last": "Lokki",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "Hum Immunol",
            "volume": "60",
            "issn": "",
            "pages": "707-14",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Association between mannose-binding lectin gene polymorphisms and susceptibility to severe acute respiratory syndrome coronavirus infection",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhi",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "192",
            "issn": "",
            "pages": "1355-61",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Association of SARS susceptibility with single nucleic acid polymorphisms of OAS1 and MxA genes: a case-control study",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "He",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Feng",
                    "suffix": ""
                },
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "de Vlas",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "BMC Infect Dis",
            "volume": "6",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Human genes that limit AIDS",
            "authors": [
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "O'Brien",
                    "suffix": ""
                },
                {
                    "first": "GW",
                    "middle": [],
                    "last": "Nelson",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat Genet",
            "volume": "36",
            "issn": "",
            "pages": "565-74",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Influenza vaccination as secondary prevention for cardiovascular disease: a science advisory from the American Heart Association/American College of Cardiology",
            "authors": [
                {
                    "first": "MM",
                    "middle": [],
                    "last": "Davis",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Taubert",
                    "suffix": ""
                },
                {
                    "first": "AL",
                    "middle": [],
                    "last": "Benin",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J AmColl Cardiol",
            "volume": "48",
            "issn": "",
            "pages": "1498-502",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "\nMBL genotype and risk of invasive pneumococcal disease: a casecontrol study",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Roy",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Knox",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Segal",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Lancet",
            "volume": "359",
            "issn": "",
            "pages": "1569-73",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Genetic susceptibility to infectious diseases: big is beautiful, but will bigger be even better?",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Burgner",
                    "suffix": ""
                },
                {
                    "first": "SE",
                    "middle": [],
                    "last": "Jamieson",
                    "suffix": ""
                },
                {
                    "first": "JM",
                    "middle": [],
                    "last": "Blackwell",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Lancet Infect Dis",
            "volume": "6",
            "issn": "",
            "pages": "653-63",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "A comparative review of HLA associations with hepatitis B and C viral infections across global populations",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Singh",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Kaul",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kaul",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Khan",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "World J Gastroenterol",
            "volume": "13",
            "issn": "",
            "pages": "1770-87",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "Acute respiratory distress syndrome induced by avian influenza A (H5N1) virus in mice",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Qiao",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Am J Respir Crit Care Med",
            "volume": "174",
            "issn": "",
            "pages": "1011-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "Role of host cytokine responses in the pathogenesis of avian H5N1 influenza viruses in mice",
            "authors": [
                {
                    "first": "KJ",
                    "middle": [],
                    "last": "Szretter",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gangappa",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Virol",
            "volume": "81",
            "issn": "",
            "pages": "2736-44",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "Fatal outcome of human influenza A (H5N1) is associated with high viral load and hypercytokinemia",
            "authors": [
                {
                    "first": "MD",
                    "middle": [],
                    "last": "de Jong",
                    "suffix": ""
                },
                {
                    "first": "CP",
                    "middle": [],
                    "last": "Simmons",
                    "suffix": ""
                },
                {
                    "first": "TT",
                    "middle": [],
                    "last": "Thanh",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Nat Med",
            "volume": "12",
            "issn": "",
            "pages": "1203-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF19": {
            "title": "Toll-like receptors and RNA helicases: two parallel ways to trigger antiviral responses",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Meylan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Tschopp",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Mol Cell",
            "volume": "22",
            "issn": "",
            "pages": "561-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF20": {
            "title": "Genetic polymorphisms of viral infection-associated Toll-like receptors in Chinese population",
            "authors": [
                {
                    "first": "P-L",
                    "middle": [],
                    "last": "Cheng",
                    "suffix": ""
                },
                {
                    "first": "H-L",
                    "middle": [],
                    "last": "Eng",
                    "suffix": ""
                },
                {
                    "first": "M-H",
                    "middle": [],
                    "last": "Chou",
                    "suffix": ""
                },
                {
                    "first": "H-L",
                    "middle": [],
                    "last": "You",
                    "suffix": ""
                },
                {
                    "first": "T-M",
                    "middle": [],
                    "last": "Lin",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Transl Res",
            "volume": "150",
            "issn": "",
            "pages": "311-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF21": {
            "title": "Involvement of toll-like receptor 4 in innate immunity to respiratory syncytial virus",
            "authors": [
                {
                    "first": "LM",
                    "middle": [],
                    "last": "Haynes",
                    "suffix": ""
                },
                {
                    "first": "DD",
                    "middle": [],
                    "last": "Moore",
                    "suffix": ""
                },
                {
                    "first": "EA",
                    "middle": [],
                    "last": "Kurt-Jones",
                    "suffix": ""
                },
                {
                    "first": "RW",
                    "middle": [],
                    "last": "Finberg",
                    "suffix": ""
                },
                {
                    "first": "LJ",
                    "middle": [],
                    "last": "Anderson",
                    "suffix": ""
                },
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Tripp",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "J Virol",
            "volume": "75",
            "issn": "",
            "pages": "10730-7",
            "other_ids": {
                "DOI": []
            }
        }
    }
}