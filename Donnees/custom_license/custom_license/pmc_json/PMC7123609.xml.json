{
    "paper_id": "PMC7123609",
    "metadata": {
        "title": "Respiratory Infections with Particular Emphasis on Influenza Virus Activity in Persons Over 14 Years of Age in the Epidemic Season 2016/2017 in Poland",
        "authors": [
            {
                "first": "Mieczyslaw",
                "middle": [],
                "last": "Pokorski",
                "suffix": "",
                "email": "m_pokorski@hotmail.com",
                "affiliation": {}
            },
            {
                "first": "D.",
                "middle": [],
                "last": "Kowalczyk",
                "suffix": "",
                "email": "dorota.kowalczyk@pzh.gov.pl",
                "affiliation": {}
            },
            {
                "first": "K.",
                "middle": [],
                "last": "Szyma\u0144ski",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "K.",
                "middle": [],
                "last": "Cie\u015blak",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "E.",
                "middle": [],
                "last": "Hallmann-Szeli\u0144ska",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "L.",
                "middle": [
                    "B."
                ],
                "last": "Brydak",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Infections caused by influenza A and B viruses are observed each season, and the course of illness may differ from one another (Brydak 2008). In each case, however, quick confirmation of the presence of the influenza virus is important since inappropriately treated infections can lead to sometimes deadly complications, especially among people from the risk groups (Fiore et al. 2011). Generally, the diagnostic process is carried out using molecular biology methods (Bednarska et al. 2016b), which allows the obtaining of results promptly. The use of antiviral treatment with neuraminidase inhibitors reduces the risk of complications, also often emanating from the respiratory tract (Bednarska et al. 2015; Brydak 2015).",
            "cite_spans": [
                {
                    "start": 135,
                    "end": 139,
                    "mention": "2008",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 380,
                    "end": 384,
                    "mention": "2011",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 486,
                    "end": 491,
                    "mention": "2016b",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 704,
                    "end": 708,
                    "mention": "2015",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 717,
                    "end": 721,
                    "mention": "2015",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The epidemic season 2016/2017 in Poland was characterized by a high incidence of influenza and influenza-like illness (ILI) (4,919,110) and of the number of hospitalizations (16,890). The number of deaths due to complications amounted to 25 cases in persons over 14 years of age, out of which 20 cases were reported in persons older than 65 years (NIPH-NIH 2017). The influenza activity was rather moderate with the peak incidence of ILI at week 4 of 2017, whereas the highest number of confirmed influenza viruses was observed at week 3 of 2017. Not without significance remains the fact that the proportion of population in Poland vaccinated against influenza is declining in Poland season by season. In 2016, only 2.48% people aged over 14 and 6.87% people aged over 65 were vaccinated, the proportions comparable to those in the preceding year (EPIMELD 2016). The dismal influenza vaccination coverage rate is particularly relevant in the face of a clearly increasing incidence of respiratory infections in adult persons in recent years. The aim of the present study was to evaluate the activity of influenza and influenza viruses in individuals in the age bracket of 14\u201365 years in the epidemic season 2016/2017 in Poland.",
            "cite_spans": [
                {
                    "start": 357,
                    "end": 361,
                    "mention": "2017",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 857,
                    "end": 861,
                    "mention": "2016",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "The study was approved by an institutional ethics committee, and it was conducted in accordance with the Declaration of Helsinki for Human Research. The study material consisted of 2982 clinical specimens taken from patients from four age groups (15\u201325, 26\u201344, 45\u201364, and \u2265 65 years) within the Sentinel and non-Sentinel surveillance programs. The specimens were nasal and throat swabs, and bronchial alveolar lavage fluid (BALF), collected during the epidemic season 2016/2017. All laboratory investigations were performed in the Department of Influenza Research, National Influenza Centre in the National Institute of Public Health-National Institute of Hygiene (NIC NIPH-NIH) in Warsaw and in 16 Voivodeship Sanitary Epidemiological Stations (VSES) in Poland. The tests investigated in VSES were reinvestigated in the reference laboratory in the (NIC NIPH-NIH).",
            "cite_spans": [],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "Viral RNA was isolated using the Maxwell 16 Viral Total Nucleic Acid Purification Kit (Promega Corp; Madison, WI) according to the manufacturer\u2019s instructions. The isolates were eluted in 50 \u03bcL of RNase-free water. To determine the presence of influenza viruses, real-time RT-PCR (qRT-PCR) was performed in capillary tubes using a Roche LightCycler 2.0 System (Roche Diagnostics; Rotkreuz, Switzerland) and SuperScript \u00ae III/Platinum \u00ae Taq Mix (Invitrogen by Life Technologies-Thermo Fisher Scientific; Carlsbad, CA). Primers and probes for the analysis were obtained through the International Reagent Resources (IRR), part of the Centers for Disease Prevention and Control in the USA. Positive controls consisted of the RNA isolated from the reference viruses A/California/7/2009 (H1N1) pdm09, A/Hong Kong/4801/2014 (H3N2), and B/Brisbane / 60/2008 as recommended by the WHO for the epidemic season 2016/2017. The RNase-free water was used as a negative control.",
            "cite_spans": [],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "A conventional multiplex RT-PCR was performed to confirm the presence of influenza-like viruses, using a RV15 OneStep ACE Detection Kit (Seegene; Seoul, South Korea). Specimens were tested for the following respiratory viruses: influenza A and B; adenoviruses (ADV); respiratory syncytial viruses (RSV) A and B; human metapneumovirus (HMPV); human coronavirus (HcoV); human parainfluenza viruses 1, 2, 3, and 4; human bocavirus (HboV); and enteroviruses.",
            "cite_spans": [],
            "section": "Methods",
            "ref_spans": []
        },
        {
            "text": "In total 2882 clinical specimens were tested in people aged over 14. The presence of influenza viruses and influenza-like infections (ILI) was confirmed in 46.5% of cases. The percentages of respiratory viruses stratified by age groups and virus types are shown in Figs. 1 and 2, respectively. The highest percentage of infections was noticed in the age group \u226565 years (49.4%), slightly less in 45\u201364 years of age (44.9%), while in the 15\u201325 and 26\u201344 years of age, the percentage was at a comparable level of about 41.5%. Taking all age groups combined, 71% of confirmed infections were caused by influenza A unsubtyped, 26% by A/H3N2/ subtype, and 2% by influenza B virus. Other respiratory viruses were confirmed in 1% of cases (Fig. 2).",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 271,
                    "end": 272,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 277,
                    "end": 278,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 738,
                    "end": 739,
                    "mention": "2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "In the season 2016/2017, infections caused by influenza A virus remained prevalent (Fig. 3), with the highest number of confirmed cases noted in the age group \u226565 (n = 356). In age groups 45\u201364 and 26\u201344 years, infections caused by influenza A unsubtyped amounted to 279 and 230 cases, respectively. The lowest number of confirmed influenza A unsubtyped cases was observed in the age group 15\u201325 years (n = 86). The presence of influenza virus subtype A/H3N2/ was observed across all age ranges. The highest number of confirmed cases of A/H3N2/ subtype was observed in the age groups 26\u201344 and 45\u201364 years, 105 and 104, respectively, slightly lower in the age group \u226565 years (n = 91), and the lowest in 15\u201325-year-old subjects (n = 45). There were single cases of influenza B virus, but no A/H1N1/pdm09 (Fig. 3). During the epidemic season 2016/2017, there were only 22 cases of ILI caused by other influenza respiratory viruses reported in people aged over 14, with the predominance of RSV (n = 11).",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 89,
                    "end": 90,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 810,
                    "end": 811,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "Comparing epidemiological data, the epidemic season 2016/2017 was characterized by similar intensity to the preceding 2015/2016 season. The number of cases and suspected cases of influenza and influenza-like viruses and the number of hospitalizations were akin to each other in both seasons, exceeding 4 million cases (NIPH-NIH 2017). However, there was almost sixfold fewer deaths due to postinfection complications in the season 2016/2017 (Table 1).",
            "cite_spans": [
                {
                    "start": 328,
                    "end": 332,
                    "mention": "2017",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 448,
                    "end": 449,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "During the epidemic season 2016/2017, the number of specimens analyzed to confirm influenza infections was lower than that in the previous seasons. However, the percentage of confirmations among people aged over 14 was higher (45.0%) compared with the seasons 2015/2016 (40.2%) and 2014/2015 (21.2%) (Kowalczyk et al. 2017a; Bednarska et al. 2016b). Akin to the season 2014/2015, the influenza A unsubtyped (71%) virus prevailed in 2016/2017, followed by the A/H3N2/ subtype (26%) (Hallmann-Szeli\u0144ska et al. 2016). While in the season 2015/2016 influenza A/H1N1/pdm09 was the prevailing subtype, there were no confirmed cases of this subtype among people aged over 14 years in the season 2016/2017 (Kowalczyk et al. 2017a).",
            "cite_spans": [
                {
                    "start": 318,
                    "end": 323,
                    "mention": "2017a",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 342,
                    "end": 347,
                    "mention": "2016b",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 508,
                    "end": 512,
                    "mention": "2016",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 716,
                    "end": 721,
                    "mention": "2017a",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Concerning the age-dependent differences in the type of influenza virus causing infections (Fig. 1), the highest number of confirmations in 2016/2017 was observed in persons over 65 years of age (49.4%), which was different from 2015/2016 when the highest number was in those in the age brackets of 45\u201364 and 26\u201344 (Kowalczyk et al. 2017a). The results are consistent with the data from other EU countries concerning influenza virus infections; the prevalence of A/H3N2/ subtype was noted in the season2016/2017, especially in persons aged over 65, who were most vulnerable to infection, according to the ECDC risk assessment (Flu News Europe 2017; ECDC 2016). An additional factor that differentiates the epidemic seasons 2014/2015 and 2015/2016 from 2016/2017 is a pronounced decrease in the number of influenza B virus infections in the latter season. In 2014/2015, the presence of influenza B virus was reported in 34.1% of cases (Bednarska et al. 2016b). In 2015/2016, B virus was the dominant type in the 15\u201325 and 26\u201344 age groups; its presence was confirmed in 201 cases in the latter group. In contrast, in the season 2016/2017 there were only single confirmed cases of influenza B virus (Fig. 3).",
            "cite_spans": [
                {
                    "start": 333,
                    "end": 338,
                    "mention": "2017a",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 643,
                    "end": 647,
                    "mention": "2017",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 654,
                    "end": 658,
                    "mention": "2016",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 952,
                    "end": 957,
                    "mention": "2016b",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 97,
                    "end": 98,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1203,
                    "end": 1204,
                    "mention": "3",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "Comparing confirmed ILI infections among individuals aged over 14 in the epidemic seasons 2015/2016 and 2016/2017, the number of confirmations was at a comparable level (n = 16 and n = 22, respectively). In both seasons, also in 2014/2015, RSV infections were predominant (Kowalczyk et al. 2017b; Bednarska et al. 2016a).",
            "cite_spans": [
                {
                    "start": 290,
                    "end": 295,
                    "mention": "2017b",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 314,
                    "end": 319,
                    "mention": "2016a",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Given the increase in the number of infections among the elderly over 65 years of age in the analyzed season 2016/2017, a low percentage of people vaccinated against influenza in Poland appears to be an important factor. Although in many voivodships vaccination for this age group is paid by the marshal\u2019s offices, 6.87% of them have been vaccinated (NIC 2017). There also was an alarming increase in the number of deaths due to complications among the elderly, 20 out of the 25 cases in 2016/2017, compared with the 56 out of the 140 influenza-related deaths in the preceding season. This dramatic increase in the proportion of influenza-related deaths in the elderly should signal an urgent need for intensified attempts to increase the vaccination coverage in Poland. According to the recommendations issued by WHO, 15 academic medical societies, and 143 national influenza centers, vaccination is the only and most effective method of preventing influenza complications and deaths (NIC 2017; Grohskopf et al. 2016; Brydak et al. 2012; Brydak 2008).",
            "cite_spans": [
                {
                    "start": 355,
                    "end": 359,
                    "mention": "2017",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 990,
                    "end": 994,
                    "mention": "2017",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1013,
                    "end": 1017,
                    "mention": "2016",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1033,
                    "end": 1037,
                    "mention": "2012",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1046,
                    "end": 1050,
                    "mention": "2008",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1: Epidemiological indicators for influenza and influenza-like virus infections in the 2015/2016 and 2016/2017 epidemic seasons in Poland (according to NIPH-NIH)\nNIPH-NIH National Influenza Center of the National Institute of Public Health-National Institute of Hygiene in Warsaw, Poland",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig. 1: Percentage of confirmed cases of influenza and influenza-like viruses in people over the 14 years of age in the epidemic season 2016/2017 in Poland",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig. 2: Percentage of influenza and influenza-like virus infections in persons over 14 years of age in Poland in the epidemic season 2016/2017",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Fig. 3: The number of confirmed cases of influenza virus infection in people over 14 years of age in Poland in the epidemic season 2016/2017 in age groups",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Evaluation of the activity of influenza and influenza-like viruses in the epidemic season 2013/2014",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kondratiuk",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Adv Exp Med Biol",
            "volume": "857",
            "issn": "",
            "pages": "1-7",
            "other_ids": {
                "DOI": [
                    "10.1007/5584_2015_116"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Prevention and control of influenza with vaccines",
            "authors": [
                {
                    "first": "LA",
                    "middle": [],
                    "last": "Grohskopf",
                    "suffix": ""
                },
                {
                    "first": "LZ",
                    "middle": [],
                    "last": "Sokolow",
                    "suffix": ""
                },
                {
                    "first": "KR",
                    "middle": [],
                    "last": "Broder",
                    "suffix": ""
                },
                {
                    "first": "SJ",
                    "middle": [],
                    "last": "Olsen",
                    "suffix": ""
                },
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Karron",
                    "suffix": ""
                },
                {
                    "first": "DB",
                    "middle": [],
                    "last": "Jernigan",
                    "suffix": ""
                },
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Bresee",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "MMWR Recomm Rep",
            "volume": "65",
            "issn": "5",
            "pages": "1-54",
            "other_ids": {
                "DOI": [
                    "10.15585/mmwr.rr6505a1"
                ]
            }
        },
        "BIBREF3": {
            "title": "Virological characteristic of the 2014/2015 influenza season based on molecular analysis of biological material derived from I-MOVE study",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Korczy\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Paradowska-Stankiewicz",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Adv Exp Med Biol",
            "volume": "857",
            "issn": "",
            "pages": "45-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Kowalczyk",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Szyma\u0144ski",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Cie\u015blak",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Circulation of antibodies against influenza virus Hemagglutinins in the 2014/2015 epidemic season in Poland",
            "volume": "",
            "issn": "",
            "pages": "35-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "The activity of influenza and influenza-like viruses in individuals aged over 14 in the 2015/2016 influenza season in Poland",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Kowalczyk",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Cie\u015blak",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Szyma\u0144ski",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Adv Exp Med Biol",
            "volume": "980",
            "issn": "",
            "pages": "45-50",
            "other_ids": {
                "DOI": [
                    "10.1007/5584_2016_202"
                ]
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Antigenic drift of a/h3n2/virus and circulation of influenza-like viruses during the 2014/2015 influenza season in Poland",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kondratiuk",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Adv Exp Med Biol",
            "volume": "905",
            "issn": "",
            "pages": "33-38",
            "other_ids": {
                "DOI": [
                    "10.1007/5584_2016_216"
                ]
            }
        },
        "BIBREF9": {
            "title": "Novelties in influenza surveillance in Poland",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kondratiuk",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Rabczenko",
                    "suffix": ""
                },
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Problemy Higieny i Epidemiologii",
            "volume": "97",
            "issn": "2",
            "pages": "101-105",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Implementing an influenza vaccination programme for adults aged \u226565 years in Poland",
            "authors": [
                {
                    "first": "LB",
                    "middle": [],
                    "last": "Brydak",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Roiz",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Faivre",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Reygrobellet",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Clin Drug Investig",
            "volume": "32",
            "issn": "",
            "pages": "73-85",
            "other_ids": {
                "DOI": [
                    "10.2165/11594030-000000000-00000"
                ]
            }
        },
        "BIBREF13": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Antiviral agents for the treatment and chemoprophylaxis of influenza \u2013 recommendations of the Advisory Committee on Immunization Practices (ACIP)",
            "authors": [
                {
                    "first": "AE",
                    "middle": [],
                    "last": "Fiore",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Fry",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Shay",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Gubareva",
                    "suffix": ""
                },
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Bresee",
                    "suffix": ""
                },
                {
                    "first": "TM",
                    "middle": [],
                    "last": "Uyeki",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "MMWR Recomm Rep",
            "volume": "60",
            "issn": "1",
            "pages": "1-24",
            "other_ids": {
                "DOI": []
            }
        }
    }
}