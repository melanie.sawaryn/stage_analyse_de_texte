{
    "paper_id": "PMC7107421",
    "metadata": {
        "title": "Detection of Host Response to Viral Respiratory Infection by Measurement of Messenger RNA for MxA, TRIM21, and Viperin in Nasal Swabs",
        "authors": [
            {
                "first": "Mohamed",
                "middle": [],
                "last": "Yahya",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Maris",
                "middle": [],
                "last": "Rulli",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Laura",
                "middle": [],
                "last": "Toivonen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Matti",
                "middle": [],
                "last": "Waris",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ville",
                "middle": [],
                "last": "Peltola",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "This study was a part of the prospective birth-cohort study Steps to the Healthy Development and Well-being of Children (STEPS) [11]. Of the 1827 children recruited into the study, 923 were followed for RTIs during the first 2 years of life by parent-filled symptom diaries and visits to the study clinic at the occurrence of symptoms of RTI. Scheduled appointments were set to coincide with the subject\u2019s age of 2 and 13 months. Study personnel documented symptoms and signs using structured forms and obtained nasal swab specimens by using flocked nylon swabs (Copan). Samples for this study were collected between February 2009 and June 2010. Vaccination information was collected from electronic registries of well-baby clinics.",
            "cite_spans": [
                {
                    "start": 129,
                    "end": 131,
                    "mention": "11",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "Subjects and Study Design ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "The Ethics Committee of the Hospital District of Southwest Finland approved the study. The parents gave their written informed consent.",
            "cite_spans": [],
            "section": "Subjects and Study Design ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "Nasal swabs were suspended in phosphate-buffered saline. Nucleic acids were extracted by NucliSense easyMag (bioM\u00e9rieux) or MagnaPure 96 (Roche) instrument. Reverse transcription and real-time PCR for rhinovirus, enterovirus, and respiratory syncytial virus (RSV) was done by using locked nucleic acids as described previously [5, 12]. Seeplex RV12 multiplex PCR assay (Seegene) was performed according to the manufacturer\u2019s instructions for adenovirus, coronaviruses 229E/NL63 and OC43/HKU1, human metapneumovirus, rhinovirus, influenza A and B viruses, parainfluenza virus types 1, 2, and 3, and RSV-A and RSV-B. For rhinovirus and RSV, a positive result in either one of the tests was considered a positive test result. Human bocavirus was detected with a separate PCR test done as previously described [13].",
            "cite_spans": [
                {
                    "start": 328,
                    "end": 329,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 331,
                    "end": 333,
                    "mention": "12",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 807,
                    "end": 809,
                    "mention": "13",
                    "ref_id": "BIBREF12"
                }
            ],
            "section": "Virus Detection ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "Nasal swab nucleic acids were first examined for MxA mRNA and then for viperin and TRIM21 mRNA. We related MxA, viperin, and TRIM21 mRNA to cell density by using \u03b2-actin mRNA as a surrogate measure of cells in the sample. Quantitation of \u03b2-actin mRNA was first done along with MxA mRNA and subsequently along with viperin and TRIM21 mRNA measurements. Expression indexes were calculated by dividing the MxA, viperin, and TRIM21 mRNA copy numbers by the actin mRNA copy number per sample. Samples with \u03b2-actin mRNA copy number <1000 in both measurements were excluded from the analysis.",
            "cite_spans": [],
            "section": "Gene Expression Measurements ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "Multiple comparisons were made by using Kruskal\u2013Wallis test and pairwise comparisons by Mann\u2013Whitney U test. Receiver operating characteristic (ROC) analyses were conducted to evaluate the capability of nasal expression indexes to distinguish symptomatic viral infections from asymptomatic virus-negative cases. The Wilson score interval was used to calculate 95% confidence intervals (CIs) for sensitivities and specificities. P values <.05 were considered significant. Statistical analyses were performed using IBM SPSS Statistics, version 21.0.",
            "cite_spans": [],
            "section": "Statistical Analysis ::: METHODS",
            "ref_spans": []
        },
        {
            "text": "We obtained 145 nasal swab specimens from 133 children. Eight cases were excluded because the copy number of \u03b2-actin was <1000 per sample in both measurements, resulting in the total of 137 cases (Table 1). Clinical manifestations were mostly mild upper RTIs. The duration of symptoms at the time of sampling varied from 1 to 4 days. A virus or viruses were detected in 76% of symptomatic children and in 13% of asymptomatic children. Rhinovirus was the most frequent finding, followed by coronaviruses, RSV, influenza A virus, and parainfluenza viruses. Adenovirus, enterovirus, and influenza B virus were not detected in any subject. The median values and interquartile ranges (IQRs) of MxA, viperin, and TRIM21 indexes according to detected viruses are shown in Supplementary Table 1. Characteristics, virus findings, and gene expression indexes of all 8 asymptomatic children positive for a respiratory virus are shown in Supplementary Table 2.",
            "cite_spans": [],
            "section": "Subject Characteristics ::: RESULTS",
            "ref_spans": [
                {
                    "start": 197,
                    "end": 204,
                    "mention": "Table 1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "Of 127 children with vaccination histories available, 53 were vaccinated <30 days before sampling. Live attenuated virus vaccines had been administered to 30 children; 26 of them had received an oral rotavirus vaccine and 4 had received measles, mumps, and rubella (MMR) vaccine. The asymptomatic virus-negative children were of particular interest with regard to vaccinations. In this group, 17 children had received an oral rotavirus vaccine, 1 an MMR vaccine, and 5 inactivated vaccines <30 days before nasal sampling.",
            "cite_spans": [],
            "section": "Subject Characteristics ::: RESULTS",
            "ref_spans": []
        },
        {
            "text": "Nasal MxA index was higher in symptomatic virus-positive children (median, 3.47 [IQR, 1.51\u201312.63] \u00d7 10\u22123) compared with asymptomatic virus-negative children (median, 1.04 [IQR, 0.39\u20132.96] \u00d7 10\u22123) (P < .001; Figure 1A). Recent administration of oral rotavirus vaccine appeared to increase the nasal MxA index in asymptomatic virus-negative children (Supplementary Figure 1). When children who had been vaccinated with a live virus vaccine within 30 days prior to nasal sampling or had missing vaccination information were excluded, the median MxA index was 3.50 (IQR, 1.61\u201311.02) \u00d7 10\u22123 in symptomatic virus-positive children and 0.49 (IQR, 0.17 \u20131.65) \u00d7 10\u22123 in the asymptomatic virus-negative group (P < .001; Figure 1B). Inactivated vaccines had no effect on MxA.",
            "cite_spans": [],
            "section": "MxA ::: RESULTS",
            "ref_spans": [
                {
                    "start": 207,
                    "end": 216,
                    "mention": "Figure 1A",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 711,
                    "end": 720,
                    "mention": "Figure 1B",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "A ROC analysis was done separately for all cases and for those not vaccinated with a live virus vaccine in the 30 days prior to MxA sampling, comparing symptomatic virus-positive and asymptomatic virus-negative children (Supplementary Figure 2). The areas under the ROC curves (AUC) were 0.74 (95% CI, .64\u2013.83) and 0.83 (95% CI, .73\u2013.92), respectively. MxA index cutoff level of 1.21 \u00d7 10\u22123 was determined by the largest sum of sensitivity and specificity in the ROC analysis of cases without recent live vaccines. This cutoff yielded a sensitivity of 84% (95% CI, 73%\u201391%) and a specificity of 56% (95% CI, 42%\u201368%) in distinguishing symptomatic virus-positive children from asymptomatic virus-negative children when all cases were included and a sensitivity of 89% (95% CI, 76%\u201395%) and a specificity of 74% (95% CI, 57%\u201386%) when children with recent live virus vaccination were excluded. MxA index was above the cutoff level in 6 of 8 asymptomatic virus-positive children, including 4 children without recent live vaccine administration (Supplementary Table 2).",
            "cite_spans": [],
            "section": "MxA ::: RESULTS",
            "ref_spans": []
        },
        {
            "text": "Viperin index was measured in 136 samples. Increased viperin indexes were found in children with all detected respiratory viruses (Supplementary Table 1). Nasal viperin index was high in symptomatic virus-positive children (median, 9.81 [IQR, 1.86\u201322.35] \u00d7 10\u22124) and low in asymptomatic virus-negative children (median, 0.00 [IQR, 0.00\u20130.61] \u00d7 10\u22124) (P < .001; Figure 1C). Symptomatic virus-positive children had higher viperin expression indexes than asymptomatic virus-positive children (median, 0.58 [IQR, 0.21\u20131 .20] \u00d7 10\u22124; P = .004). Viperin index was above the cutoff level in 1 of 8 asymptomatic virus-positive children (Supplementary Table 2). Vaccinations did not affect viperin expression (Supplementary Figure 1). The ROC analysis AUC was 0.91 (95% CI, .85\u2013.97) for viperin index in differentiation between symptomatic virus-positive and asymptomatic virus-negative children (Supplementary Figure 2). The ROC analysis\u2013derived cutoff value of 0.157 provided a sensitivity of 80% (95% CI, 67%\u201388%) and a specificity of 94% (95% CI, 85%\u201398%).",
            "cite_spans": [],
            "section": "Viperin ::: RESULTS",
            "ref_spans": [
                {
                    "start": 361,
                    "end": 370,
                    "mention": "Figure 1C",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Nasal TRIM21 index was determined in 132 samples. TRIM21 index did not significantly differ between the groups (Figure 1D). The median TRIM21 index was 2.64 (IQR, 1.02\u20135.76) \u00d7 10\u22124 in symptomatic virus-positive children and 1.46 (IQR, 0.00\u20133.08) \u00d7 10\u22124 in asymptomatic virus-negative children (P = .127). Vaccinations did not affect TRIM21 expression (Supplementary Figure 1). The ROC analysis showed poor performance of TRIM21 index in differentiation between symptomatic virus-positive and asymptomatic virus-negative children (Supplementary Figure 2).",
            "cite_spans": [],
            "section": "TRIM21 ::: RESULTS",
            "ref_spans": [
                {
                    "start": 112,
                    "end": 121,
                    "mention": "Figure 1D",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "We found nasal MxA and viperin expression to be increased in children with a symptomatic viral respiratory infection. The biomarker mRNA measurements and respiratory virus detection were performed by PCR from the same nasal swabs.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Blood MxA concentration has been reported to be a promising biomarker of viral infections [5\u20138]. Earlier we reported blood MxA levels in children according to symptoms and virus detection, similar to this study [5]. In line with the current study, blood MxA levels were found to be increased in symptomatic virus-positive subjects. In a study done by Nakabayashi et al, blood MxA levels were higher in children with viral infections compared with bacterial infections [7]. Forster et al reported increased MxA protein levels in nasopharyngeal swabs in children with RTIs by using an enzyme-linked immunosorbent assay [8]. To our knowledge, this is the first study of nasal MxA mRNA expression. In our data, nasal MxA index was elevated also in asymptomatic virus-positive children, which limits the usefulness of this biomarker as an indicator of virus as the cause of the illness.",
            "cite_spans": [
                {
                    "start": 91,
                    "end": 94,
                    "mention": "5\u20138",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 212,
                    "end": 213,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 469,
                    "end": 470,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 618,
                    "end": 619,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Viperin is a potent antiviral protein, the gene expression of which has been documented to be increased in the blood and nasal epithelium during respiratory viral infections [9]. Supporting previous studies, we found viperin expression in nasal samples to be markedly increased during virus-positive RTIs. Viperin mRNA copy numbers were close to zero both in asymptomatic virus-negative children and in asymptomatic virus-positive children (except 1 case). Thus, viperin seems to perform better than MxA as a marker of symptomatic viral RTI. However, viperin has been reported to be induced also by bacterial lipopolysaccharide [14]. The potential of viperin in differentiation between viral and bacterial infections remains to be clarified.",
            "cite_spans": [
                {
                    "start": 175,
                    "end": 176,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 629,
                    "end": 631,
                    "mention": "14",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In this study, TRIM21 index was not able to distinguish children with RTI from healthy children. Our findings support the notion that TRIM21 is ubiquitously expressed somewhat independently of virus activated innate immune receptors.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The benefits of nasal mRNA measurement of biomarkers compared with blood protein level determination include the lack of need for blood sampling and possibilities for simultaneous and automated analysis in parallel with virus PCR. The possible disadvantage of this method is that gene expression probably varies rapidly during the illness, compared with relatively steady blood protein levels. Furthermore, although actin mRNA can be used as a marker of cellular quantity in the sample, quantitative analysis is more difficult in nasal swab samples than in blood.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Effects of vaccines are an important consideration in the development of infection biomarkers for children. In our study, live attenuated rotavirus vaccine administration was associated with an increased nasal MxA index. Similar effects of live vaccines on blood MxA were found in our earlier study [5] and in a study done with a yellow fever vaccine [15]. We found no correlation between recent vaccinations and viperin or TRIM21 expression.",
            "cite_spans": [
                {
                    "start": 300,
                    "end": 301,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 352,
                    "end": 354,
                    "mention": "15",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "We did PCR testing for 15 respiratory viruses but, as a limitation, all potential viruses were not tested for. A strength in our virologic methodology was the use of a separate PCR for rhinovirus, enteroviruses, and RSV, in addition to the multiplex PCR. The low number of asymptomatic virus-positive cases and the fact that fecal samples were not tested for enteric viruses are other limitations of this study. The mean ages of children with or without RTI were rather close to each other. We earlier reported higher baseline level of blood MxA in children compared with adults but similar levels at the ages of 2 and 13 months [5].",
            "cite_spans": [
                {
                    "start": 630,
                    "end": 631,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "To conclude, our study suggests that nasal MxA and viperin mRNA indexes are potential biomarkers of viral RTI in children. In particular, viperin mRNA index was found to be a robust biomarker of a respiratory virus being the cause of symptoms. These biomarkers can be analyzed by PCR together with viruses from the same nasal swab sample. Further studies are needed to establish the possible role of nasal MxA and viperin indexes in guiding clinical decision making.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table 1.: Characteristics of the Study Children (N = 137)\n",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Figure 1.: Nasal myxovirus resistance protein A (MxA), tripartite-motif 21 (TRIM21), and viperin indexes represent the messenger RNA (mRNA) copy number of the respective protein multiplied by 1000 and divided by \u03b2-actin mRNA copy number. The boxes represent the interquartile range and the whiskers the maximum and minimum values excluding outliers. Outliers are marked by a circle and extreme outliers by an asterisk. The percentages of values above the cutoff level (horizontal line), derived from the receiver operating characteristic (ROC) analysis, are shown above different groups. Numbers of children are given below the groups. MxA indexes in all subjects (A) and in subjects with no live virus vaccine within 30 days prior to sampling (B), according to virus detection and symptoms. In panels A and B, the horizontal reference line represents the nasal MxA index value of 1.21. When all cases are included (A), this reference value yields a sensitivity of 84% and a specificity of 56% for distinguishing symptomatic virus-positive subjects from asymptomatic virus-negative subjects. When only those without live virus vaccinations are included (B), the cutoff level of 1.21 yields a sensitivity of 89% and a specificity of 74%. C, The distribution of nasal viperin index according to virus detection and presence of symptoms. The cutoff level of 0.157 yields a sensitivity of 80% and a specificity of 94%. D, The distribution of nasal TRIM21 index according to virus detection and presence of symptoms. The ROC analysis\u2013derived cutoff level (horizontal line) is set to 0.162. This value yields a sensitivity of 62% and a specificity of 56%.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Identification of respiratory viruses in asymptomatic subjects: asymptomatic respiratory viral infections",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Jartti",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Jartti",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Peltola",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Waris",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Ruuskanen",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Pediatr Infect Dis J",
            "volume": "27",
            "issn": "",
            "pages": "1103-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Induction of MxA gene expression by influenza A virus requires type I or type III interferon signaling",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Holzinger",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Jorns",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Stertz",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Virol",
            "volume": "81",
            "issn": "",
            "pages": "7776-85",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Viperin (cig5), an IFN-inducible antiviral protein directly induced by human cytomegalovirus",
            "authors": [
                {
                    "first": "KC",
                    "middle": [],
                    "last": "Chin",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Cresswell",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "98",
            "issn": "",
            "pages": "15125-30",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Expression of the immune regulator tripartite-motif 21 is controlled by IFN regulatory factors",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Sj\u00f6strand",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ambrosi",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Brauner",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Immunol",
            "volume": "191",
            "issn": "",
            "pages": "3753-63",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Blood MxA protein as a marker for respiratory virus infections in young children",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Toivonen",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Schuez-Havupalo",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Rulli",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Clin Virol",
            "volume": "62",
            "issn": "",
            "pages": "8-13",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Diagnosis of viral infections using myxovirus resistance protein A (MxA)",
            "authors": [
                {
                    "first": "I",
                    "middle": [],
                    "last": "Engelmann",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Dubos",
                    "suffix": ""
                },
                {
                    "first": "PE",
                    "middle": [],
                    "last": "Lobert",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Pediatrics",
            "volume": "135",
            "issn": "",
            "pages": "e985-93",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "MxA-based recognition of viral illness in febrile children by a whole blood assay",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Nakabayashi",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Adachi",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Itazawa",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Pediatr Res",
            "volume": "60",
            "issn": "",
            "pages": "770-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "MxA protein in infants and children with respiratory tract infection",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Forster",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Schweizer",
                    "suffix": ""
                },
                {
                    "first": "RF",
                    "middle": [],
                    "last": "Schumacher",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kaufmehl",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Lob",
                    "suffix": ""
                }
            ],
            "year": 1996,
            "venue": "Acta Paediatr",
            "volume": "85",
            "issn": "",
            "pages": "163-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Gene expression profiles during in vivo human rhinovirus infection: insights into the host response",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Proud",
                    "suffix": ""
                },
                {
                    "first": "RB",
                    "middle": [],
                    "last": "Turner",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Winther",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Am J Respir Crit Care Med",
            "volume": "178",
            "issn": "",
            "pages": "962-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "The role of viperin in the innate antiviral response",
            "authors": [
                {
                    "first": "KJ",
                    "middle": [],
                    "last": "Helbig",
                    "suffix": ""
                },
                {
                    "first": "MR",
                    "middle": [],
                    "last": "Beard",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "J Mol Biol",
            "volume": "426",
            "issn": "",
            "pages": "1210-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Cohort profile: steps to the healthy development and well-being of children (the STEPS Study)",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Lagstr\u00f6m",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Rautava",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kaljonen",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Int J Epidemiol",
            "volume": "42",
            "issn": "",
            "pages": "1273-84",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Simultaneous detection and differentiation of human rhino- and enteroviruses in clinical specimens by real-time PCR with locked nucleic acid probes",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Osterback",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Tevaluoto",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ylinen",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Clin Microbiol",
            "volume": "51",
            "issn": "",
            "pages": "3960-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Human bocavirus in children with acute lymphoblastic leukemia",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Koskenvuo",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "M\u00f6tt\u00f6nen",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Waris",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Allander",
                    "suffix": ""
                },
                {
                    "first": "TT",
                    "middle": [],
                    "last": "Salmi",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Ruuskanen",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Eur J Pediatr",
            "volume": "167",
            "issn": "",
            "pages": "1011-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Toll-like receptor-dependent and -independent viperin gene expression and counter-regulation by PRDI-binding factor-1/BLIMP1",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Severa",
                    "suffix": ""
                },
                {
                    "first": "EM",
                    "middle": [],
                    "last": "Coccia",
                    "suffix": ""
                },
                {
                    "first": "KA",
                    "middle": [],
                    "last": "Fitzgerald",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Biol Chem",
            "volume": "281",
            "issn": "",
            "pages": "26188-95",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "MxA gene expression after live virus vaccination: a sensitive marker for endogenous type I interferon",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Roers",
                    "suffix": ""
                },
                {
                    "first": "HK",
                    "middle": [],
                    "last": "Hochkeppel",
                    "suffix": ""
                },
                {
                    "first": "MA",
                    "middle": [],
                    "last": "Horisberger",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Hovanessian",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Haller",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "J Infect Dis",
            "volume": "169",
            "issn": "",
            "pages": "807-13",
            "other_ids": {
                "DOI": []
            }
        }
    }
}