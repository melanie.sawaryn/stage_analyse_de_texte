{
    "paper_id": "PMC7107917",
    "metadata": {
        "title": "Reply",
        "authors": [
            {
                "first": "Wei",
                "middle": [],
                "last": "Cui",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "SIR\u2014My colleagues and I carefully read the letter by Zhou and Chen [1], and we do not agree with their argument. The mechanism of lymphopenia in patients with severe acute respiratory syndrome (SARS) is still unknown. Our hypothesis that T lymphocytes are damaged by the SARS coronavirus (SARS-CoV) is based on current pathological findings. Li et al. [2] detected SARS-CoV RNA in PBMCs obtained from patients with SARS. Wong et al. [3] demonstrated that, in patients with SARS, lymphopenia was observed in various lymphoid organs, there were scanty numbers of lymphocytes in exudates from inflamed lungs, and lymphocytosis was not found in any organs.",
            "cite_spans": [
                {
                    "start": 68,
                    "end": 69,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 353,
                    "end": 354,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 434,
                    "end": 435,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In another article [4] cited by Zhou and Chen [1], hemorrhagic necrosis and attenuation of lymphocytes were also evident in lymph nodes and spleen specimens obtained from patients with SARS, which is direct evidence of damaged immune organs and tissues in patients with SARS. On the basis of these findings, Lang et al. [4] concluded that the severe damage to the pulmonary and immunological systems was responsible for the clinical features of SARS and might result in patient death [4]. Ding et al. [5] reached the same conclusion. They note that \u201cSARS is a systemic disease that injures many organs. The lungs, immune organs, and systemic small vessels are the main targets of virus attack so that extensive consolidation of the lung, diffuse alveolar damage with hyaline membrane formation, respiratory distress, and decreased immune function are the main causes of death\u201d [5, p. 282].",
            "cite_spans": [
                {
                    "start": 20,
                    "end": 21,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 47,
                    "end": 48,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 321,
                    "end": 322,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 485,
                    "end": 486,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 502,
                    "end": 503,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 878,
                    "end": 879,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "At the time of writing, there is still no evidence to support the view of Zhou and Chen [1] that lymphopenia is due to the redistribution and migration of lymphocytes. With respect to the expression of lymphocytes and their subsets, Li et al. [6] arrived at a conclusion identical to the one we reported [7]. Therefore, we believe that the hypothesis that SARS-CoV may damage the immune system might be supported by recently published studies.",
            "cite_spans": [
                {
                    "start": 89,
                    "end": 90,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 244,
                    "end": 245,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 305,
                    "end": 306,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We believe that Zhou and Chen [1] need more data to support their conclusion that the immune function of B cells in our patients appeared not to be impaired because specific anti\u2013SARS-CoV could be detected as early as 10 days after the onset of illness. Although anti\u2013SARS-CoV was observed in patients with SARS, it was not proper to conclude that B cells in these patients were unimpaired, because the extent of impairment might be different and because the function of B cells was not limited to the production of antibody. The fact that 76% of our patients with SARS had low B cell counts could imply that B cells were damaged by SARS-CoV, although the damage was not too severe to affect the production of antibody.",
            "cite_spans": [
                {
                    "start": 31,
                    "end": 32,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "There are still too many unanswered questions about SARS. Although the incidence of SARS has subsided in Beijing, more investigations are underway. We welcome different opinions and suggestions as we continue our research.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Is the immune system impaired in patients with severe acute respiratory syndrome [letter]",
            "authors": [
                {
                    "first": "Y-H",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Clin Infect Dis",
            "volume": "38",
            "issn": "",
            "pages": "921-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "SARS-coronavirus replicates in mononuclear cells of peripheral blood (PBMCs) from SARS patients",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wo",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Shao",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Clin Virol",
            "volume": "28",
            "issn": "",
            "pages": "239-44",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Haematological manifestations in patients with severe acute respiratory syndrome: retrospective analysis",
            "authors": [
                {
                    "first": "RS",
                    "middle": [],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "KF",
                    "middle": [],
                    "last": "To",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "BMJ",
            "volume": "326",
            "issn": "",
            "pages": "1358-62",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Pathological study on severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Lang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Chin Med J (Engl)",
            "volume": "116",
            "issn": "",
            "pages": "976-80",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "The clinical pathology of severe acute respiratory syndrome (SARS): a report from China",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Ding",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Shen",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Pathol",
            "volume": "200",
            "issn": "",
            "pages": "282-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Rapid loss of both CD4+ and CD8+ T lymphocyte subsets during the acute phase of severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Qiu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Han",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Chin Med J (Engl)",
            "volume": "116",
            "issn": "",
            "pages": "985-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Expression of lymphocytes and lymphocyte subsets in patients with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Cui",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "JY",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "AP",
                    "middle": [],
                    "last": "Ni",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Clin Infect Dis",
            "volume": "37",
            "issn": "",
            "pages": "857-9",
            "other_ids": {
                "DOI": []
            }
        }
    }
}