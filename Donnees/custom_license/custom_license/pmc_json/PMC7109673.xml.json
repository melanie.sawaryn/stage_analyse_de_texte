{
    "paper_id": "PMC7109673",
    "metadata": {
        "title": "An Outbreak of Coronavirus OC43 Respiratory Infection in Normandy, France",
        "authors": [
            {
                "first": "Astrid",
                "middle": [],
                "last": "Vabret",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Thomas",
                "middle": [],
                "last": "Mourez",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "St\u00e9phanie",
                "middle": [],
                "last": "Gouarin",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jo\u00eblle",
                "middle": [],
                "last": "Petitjean",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Fran\u00e7ois",
                "middle": [],
                "last": "Freymuth",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "\nSamples and patients. From October 2000 through April 2001, 1803 respiratory samples were received at the Laboratory of Human and Molecular Virology (University Hospital, Caen, France). All of these specimens were tested for the presence of influenza virus types A and B; respiratory syncytial virus (RSV); parainfluenza virus types 1, 2, and 3; adenovirus; rhinovirus; enterovirus; and HCoVs 229E and OC43. No coronaviruses were detected before 8 February 2001 or after 27 March 2001. Thus, the subsequent analysis included the 501 respiratory samples obtained in February and March 2001. These samples were obtained from patients hospitalized at the University Hospital of Caen (262 [52%] of 501 samples) or Flers Hospital (Lower Normandy, France; 189 [38%] of 501) and from patients for whom samples were obtained by practitioners in the Groupe R\u00e9gional d'Observation de la Grippe (GROG), the influenza surveillance network in the Lower Normandy region (50 [10%] of 501).",
            "cite_spans": [],
            "section": "Patients, Materials, and Methods",
            "ref_spans": []
        },
        {
            "text": "\nLaboratory methods for the detection of viruses. The conventional methods used for fresh specimens included the following: the virus isolation technique (VIT) with MRC5 and MDCK cells, with use of procedures reported elsewhere; and immunofluorescence assay (IFA) of nasal smears using monoclonal antibodies (Imagen) to influenza A and B viruses, parainfluenza virus types 1\u20133, RSV, and adenovirus [8]. Molecular techniques to detect rhinovirus, enterovirus, and coronavirus in frozen samples were performed using previously reported procedures [9, 10] and the usual precautions to avoid contamination with PCR products. In brief, the primers and probes used for HCoVs 229E and OC43 were selected from reported sequences of the M gene. RT-PCR was performed with the DNA Enzyme Immunoassay (GEN-ETI-K DEIA; Sorin). The assay was performed as recommended by the manufacturer. All borderline results were tested using a second molecular technique with primers and probes located in another gene (gene N), to exclude the possible contamination of amplified products [11]. In addition, 20 amplified products in gene M were sequenced (Genopole/E.S.G.S) and compared with the prototype strain OC43 (EACC no. 86040306) and the strain referenced in GenBank (accession number M93390). A phylogenetic tree was constructed using the Clustal method.",
            "cite_spans": [
                {
                    "start": 399,
                    "end": 400,
                    "mention": "8",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 546,
                    "end": 547,
                    "mention": "9",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 549,
                    "end": 551,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1063,
                    "end": 1065,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "Patients, Materials, and Methods",
            "ref_spans": []
        },
        {
            "text": "\nExamination of medical records. All medical data for patients with a recorded HCoV OC43 infection were examined retrospectively. We looked for the following symptoms: fever, general symptoms (such as myalgia and headache), abdominal pain, emesis, diarrhea, and respiratory symptoms. We used the final diagnosis established by the physician. All results of bacteriological tests were studied to identify any possible mixed infections.",
            "cite_spans": [],
            "section": "Patients, Materials, and Methods",
            "ref_spans": []
        },
        {
            "text": "A virus was detected in 189 (37.8%) of the 501 samples evaluated in February and March 2001, as follows: RSV, in 31 samples (6.1%); parainfluenza virus 3, in 5 (1%); influenza virus A, in 39 (7.8%); influenza virus B, in 36 (7.2%); rhinovirus, in 32 (6.4%); enterovirus, in 5 (1%); adenovirus, in 11 (2%); and HCoV OC43, in 30 (6%). Parainfluenza virus types 1 and 2 and HCoV 229E were not detected (table 1). There were 29 nasal aspirate specimens and 1 bronchial aspirate specimen that tested positive for HCoV OC43. Of the respiratory samples that tested positive for HCoV OC43, there were 2 that tested positive for adenovirus (the results of IFA were negative, and the results of VIT were positive). This is consistent with a secondary role for adenovirus in the causality of the concurrent illness.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 400,
                    "end": 407,
                    "mention": "table 1",
                    "ref_id": "FIGREF2"
                }
            ]
        },
        {
            "text": "Of the 30 respiratory samples in which we detected HCoV OC43 in the M gene, we also detected HCoV OC43 in the N gene in 22 of these samples; however, we have previously shown that the analytic sensitivities of these 2 molecular techniques for detection of HCoV OC43 in the M and N genes were <1 TCID50/mL and 500 TCID50/mL, respectively [10]. The nucleotide sequence of the prototype strain HCoV OC43 used as the control was first compared with the sequence of HCoV OC43 referenced in GenBank. The nucleotide sequence homology was 100%. Phylogenetic data showed that our 20 isolates had several nucleotide changes and clustered in different groups (figure 1). This showed that there had been no laboratory cross-contamination of products amplified in the M gene of HCoV OC43.",
            "cite_spans": [
                {
                    "start": 338,
                    "end": 340,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 649,
                    "end": 657,
                    "mention": "figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "The 501 patients in this study comprised of 221 patients aged <2 years (44%), 114 aged 2\u201315 years (22.7%), 98 patients aged 16\u201365 years (19.5%), and 68 patients aged >65 years (13.6%). The 30 HCoV OC43-positive respiratory specimens were obtained from 16 patients aged <2 years (7.24%), 6 patients aged 2\u201315 years (5.26%), and 8 patients aged \u2a7e16 years (4.81%). The age distribution of the patients in whom HCoV OC43 was detected was identical to the age distribution of the population sampled. The origins of the respiratory samples positive for HCoV OC43 were as follows: 12 (41%) of 30 were obtained from children (age, \u2a7d15 years) hospitalized in Flers Hospital, 7 (24%) were recovered from children and adults whose samples were provided to the influenza surveillance network (GROG), 6 (20%) were recovered from children aged <2 years who were hospitalized in the pediatric department at the University Hospital of Caen, and 5 (15%) were obtained from patients aged \u2a7e16 years hospitalized at the University Hospital of Caen.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": []
        },
        {
            "text": "We examined the medical reports for the 30 patients with HCoV OC43-positive samples. The records showed that both the upper and lower respiratory tract could be infected. The following symptoms were noted: fever, in 18 (60%) of 30 patients; general symptoms (i.e., headache, anorexia, and myalgia), in 9 (30%); digestive problems (i.e., emesis, diarrhea, and abdominal pain), in 17 (56.7%); rhinitis, in 11 (36.7%); pharyngitis, in 9 (30%); laryngitis, in 1 (3.3%); and otitis, in 4 (13.3%). Overall, nearly one-third of the patients had lower-respiratory tract infection, as follows: bronchitis, in 5 (16.7%) of 30 patients; bronchiolitis, in 3 (10%); and pneumonia, in 2 (6.7%) (figure 2). The 2 patients with pneumonia were in poor medical condition: one was 84 years old, and the other was a heart transplant recipient. There were no associated bacterial infections in the HCoV OC43-infected patients.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 681,
                    "end": 689,
                    "mention": "figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "At the end of the winter season, when RSV, influenza A and B, and rhinovirus infections were still prevalent, HCoV OC43 infection was diagnosed by molecular techniques in 6% of patients with acute respiratory tract disease\u2014the same proportion as infections due to influenza viruses, RSV, or rhinovirus. Thus, the systematic detection of HCoV in respiratory specimens improves the virological diagnosis of respiratory diseases both in children and adults.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The total impact of respiratory infection due to coronavirus cannot be known precisely. Because coronaviruses usually cause a respiratory illness indistinguishable from that caused by many other viruses, it is not possible to recognize the clinical aspects of HCoV infection in the absence of laboratory diagnosis. Most investigators have used serological techniques to evaluate the role of coronaviruses in respiratory diseases [4\u20137]. Coronaviruses are a large group of viruses that infect humans and animals, and the total number of serological types that infect humans has not yet been defined. The assumption must be made that the 2 types described above, HCoV 229E and HCoV OC43, are typical of the other viruses. The incidence and the prevalence of coronavirus infection and its geographic and temporal distribution have been studied in several reports, mainly in the United States. Coronavirus infection seems to be rare outside of the period from December through May. There was a cyclical pattern to coronavirus infection noted in all longitudinal studies for both HCoV 229E and HCoV OC43, with outbreaks of infection occurring every 2 to 4 years [4, 5, 12]. The outbreak of HCoV OC43 respiratory infection observed in Normandy in February and March 2001 is consistent with these epidemiological data. No cases of infection due to HCoV 229E were detected by molecular techniques during this period. Similarly, coronavirus respiratory infection affected patients of all age groups; this correlates well with the reported data that show that there is no decrease in the rates of coronavirus infection as patient age increases. This is in contrast to infections due to other respiratory viruses, such as RSV.",
            "cite_spans": [
                {
                    "start": 430,
                    "end": 433,
                    "mention": "4\u20137",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1157,
                    "end": 1158,
                    "mention": "4",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1160,
                    "end": 1161,
                    "mention": "5",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1163,
                    "end": 1165,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "During the surveillance of community-acquired viral infections due to respiratory viruses in the Rhones-Alpes region in France during winter of 1994\u20131995, Lina et al. [13] reported a seasonal distribution of coronavirus infection. In that study, only HCoV 229E was detected by immunostaining with an in-house monoclonal antibody. No studies have reported respiratory infections due to HCoV OC43. The high rate of detection of coronavirus infection in our study is the result of use of a highly sensitive assay (RT-PCR) rather than other conventional methods. The use of this type of amplification technique raises the question of the role of the virus in causality of the illness. In a previous study from Finland that was conducted from November 1999 through March 2000, HCoV 229E and HCoV OC43 were not found by RT-PCR of nasopharyngeal aspirates obtained from children who did not have significant concurrent respiratory symptoms [14]. However, detection of viral RNA by RT-PCR must be always interpreted cautiously, because the duration of respiratory virus colonization in the upper respiratory tract is not fully known.",
            "cite_spans": [
                {
                    "start": 168,
                    "end": 170,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 934,
                    "end": 936,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In our study, HCoV OC43 was the only infectious agent detected in the respiratory specimens obtained from patients with acute respiratory disease. The coronaviruses are mostly known as viruses responsible for coldlike illnesses. Nevertheless, there is increasing evidence that these viruses cause severe lower-respiratory tract illness in frail patients, such as infants and elderly adults [2, 15]. In the patients enrolled in our study, the site of infection was not limited to one part of the respiratory tract; however, lower-respiratory tract diseases (i.e., pneumonia and bronchiolitis) were observed in hospitalized patients.",
            "cite_spans": [
                {
                    "start": 391,
                    "end": 392,
                    "mention": "2",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 394,
                    "end": 396,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Digestive problems were noted in 58% of patients, all of whom were children. Coronavirus-like particles have been identified in the stool of persons with diarrhea, and, in the 1980s, a role of coronavirus in the etiology of acute enteric disease was suggested [16]. There is no clear evidence that HCoVs cause enteric illness, although this would not be surprising, in view of the clear involvement of some strains in severe diarrheal disease in young domestic animals. Thus, research is needed to clarify the origin of these digestive symptoms.",
            "cite_spans": [
                {
                    "start": 261,
                    "end": 263,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "In summary, active surveillance of coronavirus respiratory infections during the period from October 2000 through April 2001 allowed us to identify an outbreak of HCoV OC43 infection in Normandy. This is the first observation of epidemic circulation of HCoV OC43 in France. Coronaviruses are not as common as other respiratory viruses, but they also circulate at the end of winter and early spring and can produce a similar clinical illness. The epidemiological surveillance of coronavirus infections can be facilitated by using RT-PCR techniques for routine diagnosis.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Phylogenetic tree of the 20 respiratory isolates of human coronavirus OC43 and the prototype strain OC43 used as the control in our laboratory. The tree was created using the Clustal method. The OC43 strain and the strain referenced in GenBank (accession number M93390) did not differ. The isolates had several changes and clustered in different groups, conforming that there had been no contamination of PCR products.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Clinical data for 30 patients who tested positive for human coronavirus OC43 by RT-PCR hybridization, February and March 2001, Lower Normandy, France. Gen., general; pb, problems.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Table 1: Viruses detected in 501 respiratory samples received at the Laboratory of Human and Molecular Virology, University Hospital, Caen, France, February and March 2001.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Human coronaviruses: a brief review",
            "authors": [
                {
                    "first": "SH",
                    "middle": [],
                    "last": "Myint",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Medical Virology",
            "volume": "4",
            "issn": "",
            "pages": "35-46",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Direct diagnosis of human respiratory coronaviruses 229E and OC43 by the polymerase chain reaction",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Vabret",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Mouthon",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Mourez",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gouarin",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Petitjean",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Freymuth",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "J Virol Methods",
            "volume": "97",
            "issn": "",
            "pages": "59-66",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Evaluation of nested polymerrase chain methods for the detection of human coronaviruses 229E and OC43",
            "authors": [
                {
                    "first": "SH",
                    "middle": [],
                    "last": "Myint",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Johnston",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Sanderson",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Simpson",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Mol Cell Probes",
            "volume": "8",
            "issn": "",
            "pages": "357-64",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Seroepidemiologic survvey of coronavirus (strain OC43) related infections in a children's population",
            "authors": [
                {
                    "first": "HS",
                    "middle": [],
                    "last": "Kaye",
                    "suffix": ""
                },
                {
                    "first": "HB",
                    "middle": [],
                    "last": "Marsh",
                    "suffix": ""
                },
                {
                    "first": "WR",
                    "middle": [],
                    "last": "Dowdle",
                    "suffix": ""
                }
            ],
            "year": 1971,
            "venue": "Am J Epidemiol",
            "volume": "94",
            "issn": "",
            "pages": "43-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Surveillance of community-acquired viral infections due respiratory viruses in Rhone-Alpes (France) during winter 1994 to 1995",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Lina",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Valette",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Foray",
                    "suffix": ""
                }
            ],
            "year": 1996,
            "venue": "J Clin Microbiol",
            "volume": "34",
            "issn": "",
            "pages": "3007-11",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Human picornavirus and coronavirus RNA in nasopharynx of children without concurrent respiratory symptoms",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Nokso-Koivisto",
                    "suffix": ""
                },
                {
                    "first": "TJ",
                    "middle": [],
                    "last": "Kinnary",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lindahl",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Hovi",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pitk\u00c5ranta",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J Med Virol",
            "volume": "66",
            "issn": "",
            "pages": "417-20",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Impact of respiratory virus infections on persons with chronic underlying conditions",
            "authors": [
                {
                    "first": "PW",
                    "middle": [],
                    "last": "Glezen",
                    "suffix": ""
                },
                {
                    "first": "SB",
                    "middle": [],
                    "last": "Greenberg",
                    "suffix": ""
                },
                {
                    "first": "RL",
                    "middle": [],
                    "last": "Atmar",
                    "suffix": ""
                },
                {
                    "first": "PA",
                    "middle": [],
                    "last": "Piedra",
                    "suffix": ""
                },
                {
                    "first": "RB",
                    "middle": [],
                    "last": "Couch",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "JAMA",
            "volume": "283",
            "issn": "",
            "pages": "499-505",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Association of coronavirus infection with neonatal necrotizing enterocolitis",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Chany",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Moscovici",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lebon",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Rousset",
                    "suffix": ""
                }
            ],
            "year": 1982,
            "venue": "Pediatrics",
            "volume": "69",
            "issn": "",
            "pages": "209-14",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Spectrum of clinical illness in hospitalized patients with \u201ccommon cold\u201d virus infection",
            "authors": [
                {
                    "first": "HM",
                    "middle": [],
                    "last": "El-Sahly",
                    "suffix": ""
                },
                {
                    "first": "RL",
                    "middle": [],
                    "last": "Atmar",
                    "suffix": ""
                },
                {
                    "first": "WP",
                    "middle": [],
                    "last": "Glezen",
                    "suffix": ""
                },
                {
                    "first": "SB",
                    "middle": [],
                    "last": "Greenberg",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Clin Infect Dis",
            "volume": "31",
            "issn": "",
            "pages": "96-100",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "Rhinovirus and coronavirus infection-associated hospitalizations among older adults",
            "authors": [
                {
                    "first": "AR",
                    "middle": [],
                    "last": "Falsey",
                    "suffix": ""
                },
                {
                    "first": "EE",
                    "middle": [],
                    "last": "Walsh",
                    "suffix": ""
                },
                {
                    "first": "FG",
                    "middle": [],
                    "last": "Hayden",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J Infect Dis",
            "volume": "185",
            "issn": "",
            "pages": "1338-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "The Tecumseh study of respiratory illness. VI. Frequency of and relationship between outbreaks of coronavirus infection",
            "authors": [
                {
                    "first": "AS",
                    "middle": [],
                    "last": "Monto",
                    "suffix": ""
                },
                {
                    "first": "SK",
                    "middle": [],
                    "last": "Lim",
                    "suffix": ""
                }
            ],
            "year": 1974,
            "venue": "J Infect Dis",
            "volume": "129",
            "issn": "",
            "pages": "271-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Seroepidemiologic studies of coronavirus infection in adults and children",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "McIntosh",
                    "suffix": ""
                },
                {
                    "first": "AZ",
                    "middle": [],
                    "last": "Kapikian",
                    "suffix": ""
                },
                {
                    "first": "HC",
                    "middle": [],
                    "last": "Turner",
                    "suffix": ""
                },
                {
                    "first": "JW",
                    "middle": [],
                    "last": "Hartley",
                    "suffix": ""
                },
                {
                    "first": "RH",
                    "middle": [],
                    "last": "Parrott",
                    "suffix": ""
                },
                {
                    "first": "RM",
                    "middle": [],
                    "last": "Chanock",
                    "suffix": ""
                }
            ],
            "year": 1970,
            "venue": "Am J Epidemiol",
            "volume": "91",
            "issn": "",
            "pages": "585-92",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Occurence and frequency of coronavirus infections in humans as determined by enzyme-linked immunosorbent assay",
            "authors": [
                {
                    "first": "MR",
                    "middle": [],
                    "last": "Macnaughton",
                    "suffix": ""
                }
            ],
            "year": 1982,
            "venue": "Infect Immun",
            "volume": "38",
            "issn": "",
            "pages": "419-23",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "Virologic studies of acute respiratory disease in young adults. V. Coronavirus 229E infections during six years of surveillance",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Hamre",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Been",
                    "suffix": ""
                }
            ],
            "year": 1972,
            "venue": "Am J Epidemiol",
            "volume": "96",
            "issn": "",
            "pages": "94-106",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Detection of respiratory syncytial virus, parainfluenza 3, adenovirus and rhinovirus in respiratory tract of infants by PCR and hybridization",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Freymuth",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Vabret",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Galateau-Salle",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Clin Diagn Virol",
            "volume": "8",
            "issn": "",
            "pages": "31-40",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Detection of viral, Chlamydia pneumoniae and Mycoplasma pneumoniae infections in exacerbations of asthma in children",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Freymuth",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Vabret",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Brouard",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J Clin Virol",
            "volume": "13",
            "issn": "",
            "pages": "131-9",
            "other_ids": {
                "DOI": []
            }
        }
    }
}