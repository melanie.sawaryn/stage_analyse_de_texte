{
    "paper_id": "PMC7087295",
    "metadata": {
        "title": "Vaccine design: Innovative Approaches and Novel Strategies Edited by Rino Rappuoli and Fabio Bagnoli (2011)",
        "authors": [
            {
                "first": "Marc",
                "middle": [],
                "last": "van Regenmortel",
                "suffix": "",
                "email": "vanregen@esbs.u-strasbg.fr",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "This timely book reviews major advances in the field of bacterial vaccines made possible by the increasing number of whole genome sequences of bacterial pathogens determined in the last 15 years. Following an introductory chapter by Ruth Arnon of the Weizmann Institute describing various vaccine strategies used against microbial diseases, Chapter 2 by Rino Rappuoli and his colleagues from Novartis Vaccines in Siena, gives a comprehensive overview of a new approach known as reverse vaccinology. This approach, introduced ten years ago, was called reverse vaccinology because it does not search for vaccine candidates by the classical approach of fractionating bacterial extracts and establishing which antigens are able to induce a protective immune response, but by predicting potential vaccine immunogens using bioinformatic analyses of entire bacterial genomes.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In silico analysis of the genome provides a list of all the antigens that the pathogen is able to express and this allows investigators to choose in a reverse manner, starting from the genome rather than from the organism, which potentially surface-exposed proteins should be further investigated. The surface proteins are expressed using high-throughput technologies and then tested for immunoreactivity with patient sera and in immunogenicity and protection studies. Chapter 2 also describes various techniques used in reverse vaccinology such as in silico antigen prediction, protein identification by mass spectrometry, high-throughput antigen generation, the use of animal models for testing protection efficacy and the methods used for testing the immunogenicity of vaccine candidates. Compared to earlier approaches which were always restricted to only a small number of purified antigens, reverse vaccinology identifies hundreds of proteins as potential vaccine candidates and tests them for immunogenicity and protective efficacy. Another advantage is that this strategy can be used with microorganisms that cannot be cultivated.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "One chapter of the book is devoted to the first successful application of this new approach, namely the development of a vaccine against Meningococcus B (Men B), the pathogen that causes 50% of meningococcal meningitis worldwide. From a total of 2158 predicted open reading frames in the genome, 570 were selected as being potentially surface-exposed and 350 were successfully cloned in E. coli and purified in sufficient amounts for immunizing mice. Analysis of the mouse sera revealed 90 previously unknown surface located proteins, 28 of which were able to induce antibodies that killed the bacteria in the presence of complement. In earlier studies, only 12 surface antigens of meningococcus had been described and only 4 had been shown to possess bactericidal activity. Phase III clinical trials of a Men B vaccine effective against most meningococcal strains are currently underway",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Other chapters of the book are devoted to bacterial toxins, glycoconjugate chemistry, novel adjuvants, intralymphatic vaccination and the development of vaccines against infections caused by Pseudomonas aeruginosa, Staphylococcus aureus and Streptococcus pneumoniae.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Only one chapter is entirely devoted to the development of a viral vaccine, namely against severe acute respiratory syndrome coronavirus (SARS-CoV). However, in various parts of the book, it is suggested that in future, reverse vaccinology together with structural biology may be used to develop vaccines against hepatitis C virus, human rotavirus and human immunodeficiency virus 1. It should be noted that virologists tend to use the term reverse vaccinology in a different way from the one used in this book. Following Dennis Burton (Nat. Rev. Immunol 2, 706; 2002) virologists use the term to refer to the strategy of generating a vaccine from the known crystallographic structure of neutralizing monoclonal antibodies, rather than by the usual reverse approach of generating protective antibodies by immunization. Attempts to generate an HIV-1 vaccine by this type of reverse vaccinology, using crystal structures of HIV-1 epitopes bound to antibodies have failed so far because it is not possible to extrapolate from antigenic structures observed in an antigen-antibody complex to the immunogenic structure needed in a vaccine.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Unfortunately there are no algorithms to predict protective vaccine epitopes, from either bacterial or viral genomes, nor is it possible to predict whether structurally defined antigenic sites, when used as immunogens, will give rise to neutralizing antibodies. Compared to viruses, however, bacterial pathogens have the advantage of expressing much larger numbers of antigens that can all be evaluated as potential vaccine immunogens. Nevertheless, this book shows that, in the end, empirical trial-and-error experimentation is still the only way to discover which are the few bacterial proteins that should be combined to make up an effective bacterial vaccine.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "This book is essential reading for microbiologists and vaccinologists who want to acquaint themselves with the new approaches used to develop bacterial vaccines.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {}
}