{
    "paper_id": "PMC4036782",
    "metadata": {
        "title": "New Hepatitis E Virus Genotype in Camels, the Middle East",
        "authors": [
            {
                "first": "Patrick",
                "middle": [
                    "C.Y."
                ],
                "last": "Woo",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Susanna",
                "middle": [
                    "K.P."
                ],
                "last": "Lau",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jade",
                "middle": [
                    "L.L."
                ],
                "last": "Teng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Alan",
                "middle": [
                    "K.",
                    "L."
                ],
                "last": "Tsang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marina",
                "middle": [],
                "last": "Joseph",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Emily",
                "middle": [
                    "Y.M."
                ],
                "last": "Wong",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ying",
                "middle": [],
                "last": "Tang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Saritha",
                "middle": [],
                "last": "Sivakumar",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jun",
                "middle": [],
                "last": "Xie",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ru",
                "middle": [],
                "last": "Bai",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Renate",
                "middle": [],
                "last": "Wernery",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Ulrich",
                "middle": [],
                "last": "Wernery",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kwok-Yung",
                "middle": [],
                "last": "Yuen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "As part of a molecular epidemiology study, 203 fecal samples from 203 adult dromedaries (Camelus dromedarius) were submitted to the Central Veterinary Research Laboratory in Dubai, United Arab Emirates, over a 7-month period (January\u2013July 2013). RNA extraction and reverse transcription were performed, as described, to detect other positive-sense single-stranded RNA viruses (5,6). Screening for HEV was performed by PCR amplification of a 284-bp fragment of open reading frame (ORF) 2 in HEV; specific primers used were 5\u2032-TTTATTCTCGTCCAGTCGTTTC-3\u2032 and 5\u2032-GTCAGTGGAGGACCCATATGT-3\u2032, designed from sequence information from our metagenomic study (P.C.Y. Woo et al., unpub. data). PCR was performed according to previously described conditions (7); annealing temperature were set at 50\u00b0C. DNA sequencing and quantitative real-time reverse transcription PCR were also performed as described (8). Using strategies we have reported for other positive-sense single-stranded RNA viruses, we performed complete-genome sequencing on 2 HEV-positive samples (5,6). Comparative genomic analysis was performed as described (9). Phylogenetic analysis was conducted in MrBayes5D version 3.1.2 (www.fifthdimension.jp/products/mrbayes5d/) by using an optimal substitution model with 1 million Markov chain Monte Carlo generations; sampling was conducted every 100 generations with a burn-in of 25,000. The substitution model was selected on the basis of the corrected Akaike information criterion by ProtTest version 2.4 (http://darwin.uvigo.es/software/prottest.html).",
            "cite_spans": [
                {
                    "start": 377,
                    "end": 378,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 379,
                    "end": 380,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 744,
                    "end": 745,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 890,
                    "end": 891,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 1049,
                    "end": 1050,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1051,
                    "end": 1052,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1112,
                    "end": 1113,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Reverse transcription PCR for a 284-bp fragment in ORF2 of this HEV, which we named dromedary camel HEV (DcHEV), was positive for 3 fecal samples; viral loads were 3.7 \u00d7 105, 4.5 \u00d7 105, and 3.2 \u00d7 107 copies/mL. Complete-genome sequence data for 2 DcHEV strains (GenBank accession nos. KJ496143\u2013KJ496144) revealed that the genome size was 7,220 bases and had a G+C content of 55% (Table). Overall, the DcHEV genomes differed from all other HEVs by >20% nt (Technical Appendix Table).",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 380,
                    "end": 385,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "The DcHEV genome contained 3 major ORFs (Table, Figure 1). ORF1 polyprotein contained motifs consistent with a methyltransferase, a peptide containing a Y domain, a papain-like cysteine protease, a peptide with a hypervariable region (HVR), a helicase, and an RNA-dependent RNA polymerase. Also present in DcHEV were conserved sequences TLYTRTWS and RRLLXTYPDG, which bound the HVR of HEV1\u20134 and of 2 recently discovered wild boar HEV strains (10, 11) but not the HVR of ferret, rat, bat, avian or cutthroat trout HEVs. A conserved motif, (T/V)SGFSS(D/C)F(S/A)P, immediately preceding the HVR of only HEV3 and HEV4 was present in DcHEV as VSGFSSDFAP. The relative excess of proline and serine observed in the HVR of all other HEVs was also observed for DcHEV. For DcHEV strain 178C, ORF2 began at nt 5172, similar to what is found for HEV4 and wild boar HEV, with an insertion of a single nucleotide (U) at nt 5146, and ended at nt 7154, encoding a capsid protein of 660 aa (Technical Appendix Figure) (11\u201313). As for DcHEV strain 180C, because of the lack of the U insertion as in HEV1, HEV2, and HEV3, ORF2 began at nt 5171 (Technical Appendix Figure). For DcHEV strain 178C, similar to HEV4 and the 2 recently discovered wild boar HEV strains (11), ORF3 began at nt 5161 (Technical Appendix Figure) and ended at nt 5502, encoding a small phosphoprotein of 113 aa with a multifunctional C-terminal region. As for DcHEV strain 180C, because of the lack of the U insertion as in HEV1, HEV2, and HEV3, ORF3 began at nt 5160. The conserved cis-reactive element (UGAAUAACAUGU) located upstream of ORF2 and ORF3 in both strains might serve as promoter for the synthesis of the subgenomic mRNA for these 2 ORFs.",
            "cite_spans": [
                {
                    "start": 444,
                    "end": 446,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 448,
                    "end": 450,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1003,
                    "end": 1005,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1006,
                    "end": 1008,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1247,
                    "end": 1249,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 48,
                    "end": 56,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 41,
                    "end": 46,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Phylogenetic trees constructed by using ORF1, ORF2, ORF3, and concatenated ORF1/ORF2 excluding the HVR showed that DcHEV was clustered with different HEVs in different phylogenetic trees (Figure 2). For ORF1 and concatenated ORF1/ORF2 excluding the HVR, DcHEV was clustered with HEV3; but for ORF2 and ORF3, DcHEV was clustered with HEV1 and HEV2. Recombination analysis performed by using bootscan revealed no obvious and definite site of recombination, similar to what we observed in previous studies for other viruses (14), although different regions of the DcHEV genome might be more similar to different genotypes of HEV (data not shown).",
            "cite_spans": [
                {
                    "start": 522,
                    "end": 524,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 188,
                    "end": 196,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "We discovered HEV in dromedaries from the Middle East and named the virus DcHEV. In a recent study conducted in Dubai, HEV accounted for 40% of cases of acute hepatitis in humans (15). Although HEV is a major pathogen in the Middle East, sequence data for HEVs in the Arabian Peninsula are limited. The study reported here revealed that 1.5% of the adult dromedary fecal samples showed evidence of DcHEV RNA. Because humans come in close contact with dromedaries, our finding of DcHEV in dromedaries indicates a previously unknown potential reservoir and source of HEV infection for humans.",
            "cite_spans": [
                {
                    "start": 180,
                    "end": 182,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Comparative genomic and phylogenetic analyses showed that DcHEV probably represents a previously unrecognized HEV genotype. The conserved motif preceding the HVR in ORF1 resembled those found in HEV3, HEV4, and the 2 recently discovered wild boar HEV strains. Although phylogenetically ORF1 of DcHEV was clustered with HEV3, ORF2 and ORF3 of DcHEV were clustered with HEV1 and HEV2. Of note, ORF2 and ORF3 of the 2 DcHEV strains with complete genomes sequenced in this study resembled those of different HEV genotypes. The presence of a U insertion downstream to the second possible start codon for ORF2 (AUG2) in DcHEV strain 178C resembled the presence of a U insertion in HEV4 and wild boar HEV, leading to 3 possible start codons for its ORF2 but 1 possible start codon for its ORF3; whereas, the lack of this U insertion downstream to AUG2 in DcHEV strain 180C resembled the lack of U insertions in HEV1, HEV2 and HEV3, leading to only 1 possible start codon for its ORF2 but 3 possible start codons for its ORF3. To our knowledge, this presence or absence of such a U insertion in different strains of the same HEV has never been observed in other HEV genotypes and is unique to DcHEV. Although different regions of the DcHEV genome possessed characteristics associated with different kinds of HEV, no significant recombination was detected between DcHEV and the other HEVs. Because different regions of the genomes of DcHEV resembled those of different HEV genotypes, and even the genomes of different strains of DcHEV resembled those of different HEV genotypes, we propose that DcHEV should constitute a new HEV genotype.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Predicted genomic organization of hepatitis E virus (HEV) from dromedary camel (DcHEV) and other HEVs, considering the reading frame of open reading frame (ORF) 1 as frame 1.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Phylogenetic analyses of open reading frame (ORF) 1 (A), ORF2 (B), ORF3 (C), and ORF1/ORF2 proteins, excluding the hypervariable region (HVR) (D) of hepatitis E virus (HEV) from dromedary camels (DcHEV). The trees were constructed by using Bayesian methods of phylogenetic reconstruction (www.fifthdimension.jp/products/mrbayes5d/), and ProtTest-suggested JTT+I+G+F, MtMam+I+G+F, HIVw+I+G+F, and JTT+I+G+F (http://darwin.uvigo.es/software/prottest.html) are the optimal substitution models for ORF1, ORF2, ORF3, and concatenated ORF1/ORF2 excluding HVR, respectively. For this analysis we included amino acid positions 1698, 660, 113, and 2282 in ORF1, ORF2, ORF3 and concatenated ORF1/ORF2 excluding HVR, respectively. For ORF2 and concatenated ORF1/ORF2 excluding HVR, the scale bars indicate the estimated number of substitutions per 50 aa. For ORF1 and ORF3, the scale bars indicate the estimated number of substitutions per 20 aa. Boldface indicates the 2 strains of DcHEV with complete genomes sequenced in this study.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Novel hepatitis E virus in ferrets, the Netherlands.",
            "authors": [],
            "year": 2012,
            "venue": "Emerg Infect Dis",
            "volume": "18",
            "issn": "",
            "pages": "1369-70",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1808.111659"
                ]
            }
        },
        "BIBREF1": {
            "title": "The hepatitis E virus polyproline region is involved in viral adaptation.",
            "authors": [],
            "year": 2012,
            "venue": "PLoS ONE",
            "volume": "7",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.pone.0035974"
                ]
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2011,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "A bicistronic subgenomic mRNA encodes both the ORF2 and ORF3 proteins of hepatitis E virus.",
            "authors": [],
            "year": 2006,
            "venue": "J Virol",
            "volume": "80",
            "issn": "",
            "pages": "5919-26",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.00046-06"
                ]
            }
        },
        "BIBREF4": {
            "title": "Initiation at the third in-frame AUG codon of open reading frame 3 of the hepatitis E virus is essential for viral infectivity in vivo.",
            "authors": [],
            "year": 2007,
            "venue": "J Virol",
            "volume": "81",
            "issn": "",
            "pages": "3018-26",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02259-06"
                ]
            }
        },
        "BIBREF5": {
            "title": "Comparative analysis of 22 coronavirus HKU1 genomes reveals a novel genotype and evidence of natural recombination in coronavirus HKU1.",
            "authors": [],
            "year": 2006,
            "venue": "J Virol",
            "volume": "80",
            "issn": "",
            "pages": "7136-45",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.00509-06"
                ]
            }
        },
        "BIBREF6": {
            "title": "Hepatitis E: a common cause of acute viral hepatitis.",
            "authors": [],
            "year": 2009,
            "venue": "J Pak Med Assoc",
            "volume": "59",
            "issn": "",
            "pages": "92-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Hepatitis E virus variant in farmed mink, Denmark.",
            "authors": [],
            "year": 2013,
            "venue": "Emerg Infect Dis",
            "volume": "19",
            "issn": "",
            "pages": "2028-30",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1912.130614"
                ]
            }
        },
        "BIBREF8": {
            "title": "Novel hepatitis E virus genotype in Norway rats, Germany.",
            "authors": [],
            "year": 2010,
            "venue": "Emerg Infect Dis",
            "volume": "16",
            "issn": "",
            "pages": "1452-5",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1609.100444"
                ]
            }
        },
        "BIBREF9": {
            "title": "Severe hepatitis E virus infection after ingestion of uncooked liver from a wild boar.",
            "authors": [],
            "year": 2003,
            "venue": "J Infect Dis",
            "volume": "188",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1086/378074"
                ]
            }
        },
        "BIBREF10": {
            "title": "Discovery of seven novel mammalian and avian coronaviruses in the genus Deltacoronavirus supports bat coronaviruses as the gene source of Alphacoronavirus and Betacoronavirus and avian coronaviruses as the gene source of Gammacoronavirus and Deltacoronavirus.",
            "authors": [],
            "year": 2012,
            "venue": "J Virol",
            "volume": "86",
            "issn": "",
            "pages": "3995-4008",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.06540-11"
                ]
            }
        },
        "BIBREF11": {
            "title": "Natural occurrence and characterization of two internal ribosome entry site elements in a novel virus, canine picodicistrovirus, in the picornavirus-like superfamily.",
            "authors": [],
            "year": 2012,
            "venue": "J Virol",
            "volume": "86",
            "issn": "",
            "pages": "2797-808",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.05481-11"
                ]
            }
        },
        "BIBREF12": {
            "title": "Novel betacoronavirus in dromedaries of the Middle East, 2013.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "560-72",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2004.131769"
                ]
            }
        },
        "BIBREF13": {
            "title": "Discovery of a novel bottlenose dolphin coronavirus reveals a distinct species of marine mammal coronavirus in gammacoronavirus.",
            "authors": [],
            "year": 2014,
            "venue": "J Virol",
            "volume": "88",
            "issn": "",
            "pages": "1318-31",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02351-13"
                ]
            }
        },
        "BIBREF14": {
            "title": "Genetic variability and the classification of hepatitis E virus.",
            "authors": [],
            "year": 2013,
            "venue": "J Virol",
            "volume": "87",
            "issn": "",
            "pages": "4161-9",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02762-12"
                ]
            }
        }
    }
}