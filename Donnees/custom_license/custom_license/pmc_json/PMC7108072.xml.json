{
    "paper_id": "PMC7108072",
    "metadata": {
        "title": "Interpreting Results From Environmental Contamination Studies of Middle East Respiratory Syndrome Coronavirus",
        "authors": [
            {
                "first": "Maria",
                "middle": [
                    "D."
                ],
                "last": "Van Kerkhove",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Malik",
                "middle": [
                    "J.",
                    "S."
                ],
                "last": "Peiris",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mamunur",
                "middle": [
                    "Rahman"
                ],
                "last": "Malik",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Peter",
                "middle": [],
                "last": "Ben Embarek",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "To the Editor\u2014Middle East respiratory syndrome coronavirus (MERS-CoV) continues to pose a threat to human health among populations in contact with infected dromedary camels and globally through the travel of persons who acquired infection from areas with enzootic dromedary infection. Transmission of MERS-CoV is categorized as zoonotic, with repeated introductions into the human population after contact with infected dromedaries, resulting in limited human-to-human transmission, notably in healthcare settings [1] The MERS-CoV outbreak in South Korea in 2015, resulting in 186 cases and 38 deaths [2], reminds us that the introduction of a single case into an unsuspecting healthcare system can trigger a very large outbreak with serious public health and socioeconomic consequences.",
            "cite_spans": [
                {
                    "start": 515,
                    "end": 516,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 602,
                    "end": 603,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Although we have known for quite some time that nosocomial transmission is responsible for more than half of reported MERS-CoV cases worldwide [1], we know little about how transmission occurs in healthcare settings. We have suspected, and our involvement in several missions to affected countries in the Kingdom of Saudi Arabia, Jordan, and South Korea has confirmed, that delayed recognition and slow isolation of infected patients, extended stays in overcrowded emergency rooms, and suboptimal adherence to infection prevention and control procedures have been responsible for secondary cases in these settings [3], but the role of environmental contamination has received relatively little attention. Researchers in Korea have recently published studies [4, 5] evaluating environmental and/or air contamination by MERS-CoV and should be commended for their efforts to evaluate the hospital outbreaks in their country.",
            "cite_spans": [
                {
                    "start": 144,
                    "end": 145,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 615,
                    "end": 616,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 759,
                    "end": 760,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 762,
                    "end": 763,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Taken together, these results from Kim et al [4] and others [5\u20136] suggest that MERS-CoV can persist on the surfaces in contaminated environments, such as patient rooms and equipment, and underscore the critical importance of adequate and thorough disinfection of patient rooms during and after their treatment in healthcare settings.",
            "cite_spans": [
                {
                    "start": 46,
                    "end": 47,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 61,
                    "end": 64,
                    "mention": "5\u20136",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Although the findings from Kim et al [4] are very interesting and raise questions about the potential presence of MERS-CoV in the air, they provide no evidence of airborne transmission. Some concerns have been expressed that the claimed \u201cvirus isolation\u201d did not yield virus isolates but rather was based on reverse-transcription polymerase chain reaction detection of infected cells [7]. Kim et al [4] did go further to demonstrate passage of virus-infected cells leading to detectable viral antigen detection, which is better evidence of replicating virus. We are aware of a number of other environmental studies conducted in hospitals that have found surface contamination but have not been able to isolate virus from air samples. These studies have not yet been published in the peer-reviewed literature. It is important that these and any negative results are also published, or else we will be left with a skewed selection of positive findings. The partial inconsistencies in results between studies will need further examination. In particular, it is important that future studies include \u201cnegative controls\u201d with comparable sampling and testing strategies applied in settings where patients with MERS were not housed.",
            "cite_spans": [
                {
                    "start": 38,
                    "end": 39,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 385,
                    "end": 386,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 400,
                    "end": 401,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "To fully understand the possible role of environmental contamination, including the possible detection of MERS-CoV in the air, additional studies must be conducted to see whether these results can be replicated\u2014for example, in hospitals in the Middle East where patients with MERS-CoV are treated, to evaluate virus persistence in hospital environments. A more complete understanding of the results of environmental and air contamination studies will have important implications for the application of infection prevention and control measures [8] currently used in hospitals treating patients with MERS-CoV, possibly leading to more detailed recommendations.",
            "cite_spans": [
                {
                    "start": 545,
                    "end": 546,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Middle East respiratory syndrome coronavirus (MERS-CoV): current situation 3 years after the virus was first identified",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Ben Embarek",
                    "suffix": ""
                },
                {
                    "first": "MD",
                    "middle": [],
                    "last": "Van Kerkhove",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Wkly Epidemiol Rec",
            "volume": "90",
            "issn": "",
            "pages": "245-50",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Preliminary epidemiological assessment of MERS-CoV outbreak in South Korea, May to June 2015",
            "authors": [
                {
                    "first": "BJ",
                    "middle": [],
                    "last": "Cowling",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Park",
                    "suffix": ""
                },
                {
                    "first": "VJ",
                    "middle": [],
                    "last": "Fang",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "GM",
                    "middle": [],
                    "last": "Leung",
                    "suffix": ""
                },
                {
                    "first": "JT",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Eurosurveillance",
            "volume": "20",
            "issn": "",
            "pages": "7-13",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Notes from the field: nosocomial outbreak of Middle East respiratory syndrome in a large tertiary care hospital\u2014Riyadh, Saudi Arabia, 2015",
            "authors": [
                {
                    "first": "HH",
                    "middle": [],
                    "last": "Balkhy",
                    "suffix": ""
                },
                {
                    "first": "TH",
                    "middle": [],
                    "last": "Alenazi",
                    "suffix": ""
                },
                {
                    "first": "MM",
                    "middle": [],
                    "last": "Alshamrani",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "MMWR",
            "volume": "65",
            "issn": "",
            "pages": "163-4",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Extensive viable Middle East respiratory syndrome (MERS) coronavirus contamination in air and surrounding environment in MERS outbreak units",
            "authors": [
                {
                    "first": "SH",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Sung",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Clin Infect Dis",
            "volume": "63",
            "issn": "",
            "pages": "363-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Environmental contamination and viral shedding in MERS patients during MERS-CoV outbreak in South Korea",
            "authors": [
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Bin",
                    "suffix": ""
                },
                {
                    "first": "JY",
                    "middle": [],
                    "last": "Heo",
                    "suffix": ""
                },
                {
                    "first": "MS",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Clin Infect Dis",
            "volume": "62",
            "issn": "",
            "pages": "755-60",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Stability of Middle East respiratory syndrome coronavirus (MERS-CoV) under different environmental conditions",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "van Doremalen",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Bushmaker",
                    "suffix": ""
                },
                {
                    "first": "VJ",
                    "middle": [],
                    "last": "Munster",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Euro Surveill",
            "volume": "18",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Environmental contamination and viral shedding in MERS patients",
            "authors": [
                {
                    "first": "MD",
                    "middle": [],
                    "last": "Oh",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Clin Infect Dis",
            "volume": "62",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Middle East respiratory syndrome coronavirus (MERS-CoV)",
            "authors": [],
            "year": 2016,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}