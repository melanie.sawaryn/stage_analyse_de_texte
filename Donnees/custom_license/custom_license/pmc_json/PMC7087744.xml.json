{
    "paper_id": "PMC7087744",
    "metadata": {
        "title": "Isolation and Characterization of Polymorphic Microsatellite Markers for the Masked Palm Civet (Paguma larvata)",
        "authors": [
            {
                "first": "J.",
                "middle": [
                    "P."
                ],
                "last": "Chen",
                "suffix": "",
                "email": "chen_jinping@gibh.ac.cn",
                "affiliation": {}
            },
            {
                "first": "D.",
                "middle": [
                    "H."
                ],
                "last": "Andersen",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "G.",
                "middle": [],
                "last": "Veron",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "E.",
                "middle": [],
                "last": "Randi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "S.",
                "middle": [
                    "Y."
                ],
                "last": "Zhang",
                "suffix": "",
                "email": "syzhang@bio.ecnu.edu.cn",
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The masked palm civet (Paguma larvata) is widely distributed in tropical and subtropical Asia, throughout the Himalayas from north Pakistan, Nepal, Bhutan, Jammu, Kashmir, and Assam southward to Myanmar, Thailand, Laos, Vietnam, the Malay Peninsula, Borneo, Sumatra, and the Andaman Islands (Pocock 1939; Roberts 1977; Corbet and Hill 1992; Choudhury 1997a, b, 1999; Jha 1999). In China, P. larvata lives mainly in the southern regions, but it can also be found in Hebei, Shanxi, Shaanxi, and Xizang provinces (Gao et al. 1987; Yin and Liu 1993). Due to habitat destruction, fragmentation, and hunting, the masked palm civet is threatened by local extinctions in the wild; it has become an endangered species and is included in the Chinese Protected Animal List (Tan 1989). In south China, P. larvata is reproduced in captivity and traded for meat production. The associated coronavirus (SARS-like CoV), which causes severe acute respiratory syndrome in humans, has been isolated from wild and captive masked palm civets, suspected to be the hosts of SARS virus (Nicastri et al. 2003). However, the intraspecific taxonomy of P. larvata is poorly known, and there are no species-specific markers available for population genetic studies. In this paper, we report the isolation and characterization of six polymorphic microsatellite loci from a P. larvata DNA library enriched with (CA)n repeats, and their polymorphism in two farmed populations in China.",
            "cite_spans": [
                {
                    "start": 299,
                    "end": 303,
                    "mention": "1939",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 313,
                    "end": 317,
                    "mention": "1977",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 335,
                    "end": 339,
                    "mention": "1992",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 351,
                    "end": 356,
                    "mention": "1997a",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 358,
                    "end": 359,
                    "mention": "b",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 361,
                    "end": 365,
                    "mention": "1999",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 371,
                    "end": 375,
                    "mention": "1999",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 522,
                    "end": 526,
                    "mention": "1987",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 540,
                    "end": 544,
                    "mention": "1993",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 767,
                    "end": 771,
                    "mention": "1989",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1079,
                    "end": 1083,
                    "mention": "2003",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Introduction",
            "ref_spans": []
        },
        {
            "text": "A dinucleotide-enriched library for P. larvata was obtained according to Kandpal et al. (1994) and Karp et al. (1998) with some modifications. DNA was extracted from a single individual using a modified phenol\u2013chloroform extraction method (Sambrook and Russell 2002; Chen et al. 2005). Genomic DNA was digested with MboI, and the fragments between 350 and 600 bp were cut out from an agarose gel and purified with an agarose gel DNA Purification Kit (TaKaRa). The selected fragments were ligated to linker sequences and amplified using linker-specific primers on a PE9700 thermal cycler (Armour et al. 1994; Hammond et al. 1998). The PCR product was denatured and hybridized to a biotin-labeled dinucleotide repeat oligonucleotide (CA)15 probe, and hybridization was carried out at 50\u00b0C for 18 h. The hybridization mixture was mixed with 50-mg Vectrex Avidin D (Vector Laboratories). After a first wash with 0.1\u00d7 buffer A (15 mM NaCl/10 mM Tris, pH 7.5) at 55\u00b0C for 30 min, and two washes with ddH2O at 65\u00b0C for 30 min to remove unbound fragments, the matrix was washed again with ddH2O to elute the bound fragments, and the supernatant was retained. Another PCR was performed to amplify the target genomic fragments, this PCR condition being similar to the first PCR (Armour et al. 1994; Hammond et al. 1998), and the products were ligated to PMD19-T vector (TaKaRa). Escherichia coli DH-5\u03b1 competent cells were transformed with the ligation product to construct the (CA)n-enriched DNA library. Out of 140 recombinant colonies, 120 clones were screened by amplification using linker primers. A total of 57 clones were sequenced on an ABI377 sequencer with M13 universal primers (Invitrogen), and 38 contained simple sequence repeats. PCR primers for amplifying the found microsatellite loci were designed using the Primer Premier 5.0 software (Premier Biosoft International, Palo Alto, CA) (Table 1). The PCR conditions were optimized through gradient PCR amplification (MJ Research). Only six primer pairs reliably amplified a single polymorphic locus.\n\n",
            "cite_spans": [
                {
                    "start": 89,
                    "end": 93,
                    "mention": "1994",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 112,
                    "end": 116,
                    "mention": "1998",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 261,
                    "end": 265,
                    "mention": "2002",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 279,
                    "end": 283,
                    "mention": "2005",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 602,
                    "end": 606,
                    "mention": "1994",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 623,
                    "end": 627,
                    "mention": "1998",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1283,
                    "end": 1287,
                    "mention": "1994",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1304,
                    "end": 1308,
                    "mention": "1998",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "Methods and Materials",
            "ref_spans": [
                {
                    "start": 1898,
                    "end": 1899,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "To characterize each locus, we genotyped two farmed masked palm civet populations from China (40 individuals; Shanxi and Guangxi provinces). Genomic DNA was extracted from hair and skin tissues using a modified phenol\u2013chloroform extraction method (Sambrook and Russell 2002; Chen et al. 2005). PCR reactions were carried out in a PE9700 thermal cycler using the following conditions in a total volume of 10 \u03bcl: 50\u2013100 ng genomic DNA, 0.25 \u03bcM of each primer, and 1\u00d7 PCR buffer containing 2 mM MgCl2, 0.2 mM of each dNTP, 0.5 U Taq DNA polymerase (Premix Taq, TaKaRa). The reactions were performed under the following conditions: denaturation at 95\u00b0C for 3 min, followed by 35 cycles of 94\u00b0C for 30 s, annealing temperature for 30 s (Table 1), 72\u00b0C for 30 s, with a final extension step at 72\u00b0C for 10 min. Genotyping of the individuals was performed on an ABI 3100 genetic analyzer using 5\u2032 fluorescent-labeled (MedProbe) forward primers.",
            "cite_spans": [
                {
                    "start": 269,
                    "end": 273,
                    "mention": "2002",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 287,
                    "end": 291,
                    "mention": "2005",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Methods and Materials",
            "ref_spans": [
                {
                    "start": 738,
                    "end": 739,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "All six loci were highly polymorphic in Paguma larvata; the number of alleles ranged from 3 (PC119) to 15 (PC11), with a mean of 8.0 (Fig. 1), and all loci were dinucleotide repeats. The expected heterozygosity was 0.508 and 0.681 in the two farmed populations. Observed heterozygosity ranged from 0.471 to 0.687 (Table 1). Exact tests for Hardy-Weinberg equilibrium (Guo and Thompson 1992) were computed at each locus using GenePop (Raymond and Rousset 1995); no significant deviations from Hardy-Weinberg equilibrium (P < 0.01) were observed in the two populations. Tests for linkage disequilibrium between the polymorphic loci within each species were calculated using FStat version 2.9.3.2 (Goudet 2002). Significant linkage disequilibrium was found only between locus PC55 and locus PC58 in the Guangxi population. Allele frequency distributions showed 15 rare alleles (frequency < 5%) of a total of 48 alleles summed over all loci, for a mean of 31.3% (Fig. 1).\n\n",
            "cite_spans": [
                {
                    "start": 385,
                    "end": 389,
                    "mention": "1992",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 454,
                    "end": 458,
                    "mention": "1995",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 702,
                    "end": 706,
                    "mention": "2002",
                    "ref_id": "BIBREF19"
                }
            ],
            "section": "Results and Discussion",
            "ref_spans": [
                {
                    "start": 139,
                    "end": 140,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 964,
                    "end": 965,
                    "mention": "1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 320,
                    "end": 321,
                    "mention": "1",
                    "ref_id": "TABREF0"
                }
            ]
        },
        {
            "text": "The markers presented in this study will provide a useful tool for monitoring genetic diversity and population structure in the masked palm civet.",
            "cite_spans": [],
            "section": "Results and Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "TABREF0": {
            "text": "Table\u00a01: Six microsatellite loci in Paguma larvata\n\n\nNote: Tm, annealing temperature. H\no, observed heterozygosity. H\ne, expected heterozygosity",
            "type": "table"
        },
        "FIGREF0": {
            "text": "Fig.\u00a01: Allele frequency (on y-axis) and allele base-pair length (on x-axis) for six masked palm civet microsatellite loci (a\u2013f) in two farmed populations",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Isolation of human simple repeat loci by hybridization selection",
            "authors": [
                {
                    "first": "JAL",
                    "middle": [],
                    "last": "Armour",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Neumann",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gobert",
                    "suffix": ""
                },
                {
                    "first": "AJ",
                    "middle": [],
                    "last": "Jeffreys",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Hum Mol Genet",
            "volume": "3",
            "issn": "",
            "pages": "599-605",
            "other_ids": {
                "DOI": [
                    "10.1093/hmg/3.4.599"
                ]
            }
        },
        "BIBREF1": {
            "title": "Isolation of microsatellite markers in animals",
            "authors": [
                {
                    "first": "RL",
                    "middle": [],
                    "last": "Hammond",
                    "suffix": ""
                },
                {
                    "first": "IJ",
                    "middle": [],
                    "last": "Saccheri",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Ciofi",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Coote",
                    "suffix": ""
                },
                {
                    "first": "SW",
                    "middle": [],
                    "last": "Funk",
                    "suffix": ""
                },
                {
                    "first": "WO",
                    "middle": [
                        "MK"
                    ],
                    "last": "Mcmillan Bayes",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Taylor",
                    "suffix": ""
                },
                {
                    "first": "MW",
                    "middle": [],
                    "last": "Bruford",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Molecular tools for screening biodiversity",
            "volume": "",
            "issn": "",
            "pages": "279-285",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "A preliminary survey on the status of civets in Nandapha Biosphere Reserve in Arunachal Pradesh",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Jha",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "Tiger Paper",
            "volume": "26",
            "issn": "",
            "pages": "1-5",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Construction of libraries enriched for sequence repeats and jumping clones, and hybridization selection for region-specific markers",
            "authors": [
                {
                    "first": "RP",
                    "middle": [],
                    "last": "Kandpal",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Kandpal",
                    "suffix": ""
                },
                {
                    "first": "SM",
                    "middle": [],
                    "last": "Weissman",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Proc Natl Acad Sci USA",
            "volume": "91",
            "issn": "",
            "pages": "88-92",
            "other_ids": {
                "DOI": [
                    "10.1073/pnas.91.1.88"
                ]
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Karp",
                    "suffix": ""
                },
                {
                    "first": "PG",
                    "middle": [],
                    "last": "Isaac",
                    "suffix": ""
                },
                {
                    "first": "DS",
                    "middle": [],
                    "last": "Ingram",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Molecular tools for screening biodiversity",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Severe acute respiratory syndrome: the first transmissible disease of the 21st century",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Nicastri",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Petrosillo",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Macri",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Ippolito",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Recenti Prog Med",
            "volume": "94",
            "issn": "",
            "pages": "295-303",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "",
            "authors": [
                {
                    "first": "RI",
                    "middle": [],
                    "last": "Pocock",
                    "suffix": ""
                }
            ],
            "year": 1939,
            "venue": "The fauna of British India including Ceylon and Burma",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "GenePop (Version 1.2): population genetics software for exact tests and ecumenicism",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Raymond",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Rousset",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "J Hered",
            "volume": "86",
            "issn": "",
            "pages": "248-249",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "",
            "authors": [
                {
                    "first": "TJ",
                    "middle": [],
                    "last": "Roberts",
                    "suffix": ""
                }
            ],
            "year": 1977,
            "venue": "Mammals of Pakistan",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF9": {
            "title": "",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Sambrook",
                    "suffix": ""
                },
                {
                    "first": "DW",
                    "middle": [],
                    "last": "Russell",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Molecular cloning: a laboratory manual",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF10": {
            "title": "Conservation and economic importance of the Mustelids and Viverrids in China",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Tan",
                    "suffix": ""
                }
            ],
            "year": 1989,
            "venue": "Small Carnivore Conserv",
            "volume": "1",
            "issn": "",
            "pages": "5-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "Genetic analysis of four wild chum salmon Oncorhynchus keta populations in China based on microsatellite markers",
            "authors": [
                {
                    "first": "JP",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "DJ",
                    "middle": [],
                    "last": "Sun",
                    "suffix": ""
                },
                {
                    "first": "CZ",
                    "middle": [],
                    "last": "Dong",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Liang",
                    "suffix": ""
                },
                {
                    "first": "WH",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "SY",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Environ Biol Fishes",
            "volume": "73",
            "issn": "",
            "pages": "181-188",
            "other_ids": {
                "DOI": [
                    "10.1007/s10641-004-6114-0"
                ]
            }
        },
        "BIBREF12": {
            "title": "",
            "authors": [
                {
                    "first": "BG",
                    "middle": [],
                    "last": "Yin",
                    "suffix": ""
                },
                {
                    "first": "WL",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Wild animals and their protection in Xizang province",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF13": {
            "title": "The distribution and status of small carnivores (Mustelids, Viverrids and Herpestids) in Assam, India",
            "authors": [
                {
                    "first": "AU",
                    "middle": [],
                    "last": "Choudhury",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Small Carnivore Conserv",
            "volume": "16",
            "issn": "",
            "pages": "25-26",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF14": {
            "title": "Small carnivores (Mustelids, viverrids, Herpestids and one Ailurid) in Arunachal Pradesh, India",
            "authors": [
                {
                    "first": "AU",
                    "middle": [],
                    "last": "Choudhury",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Small Carnivore Conserv",
            "volume": "17",
            "issn": "",
            "pages": "7-9",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF15": {
            "title": "Conservation of small carnivores (Mustelids, Viverrids, Herpestids and one Ailurid) in North Bengal, India",
            "authors": [
                {
                    "first": "AU",
                    "middle": [],
                    "last": "Choudhury",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "Small Carnivore Conserv",
            "volume": "20",
            "issn": "",
            "pages": "15-17",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF16": {
            "title": "",
            "authors": [
                {
                    "first": "GB",
                    "middle": [],
                    "last": "Corbet",
                    "suffix": ""
                },
                {
                    "first": "JE",
                    "middle": [],
                    "last": "Hill",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "The mammals of the Indomalayan region: a systematic review",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF17": {
            "title": "",
            "authors": [
                {
                    "first": "YT",
                    "middle": [],
                    "last": "Gao",
                    "suffix": ""
                }
            ],
            "year": 1987,
            "venue": "Mammalia: carnivora (in Chinese zoography)",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF18": {
            "title": "Performing the exact test of Hardy-Weinberg proportions for multiple alleles",
            "authors": [
                {
                    "first": "SW",
                    "middle": [],
                    "last": "Guo",
                    "suffix": ""
                },
                {
                    "first": "EA",
                    "middle": [],
                    "last": "Thompson",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "Biometrics",
            "volume": "48",
            "issn": "",
            "pages": "361-372",
            "other_ids": {
                "DOI": [
                    "10.2307/2532296"
                ]
            }
        },
        "BIBREF19": {
            "title": "",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}