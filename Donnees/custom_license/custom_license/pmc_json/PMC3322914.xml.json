{
    "paper_id": "PMC3322914",
    "metadata": {
        "title": "Atypical SARS in Geriatric Patient",
        "authors": [
            {
                "first": "Augustine",
                "middle": [
                    "K.H."
                ],
                "last": "Tee",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Helen",
                "middle": [
                    "M.L."
                ],
                "last": "Oh",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "K.P.",
                "middle": [],
                "last": "Hui",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Christopher",
                "middle": [
                    "T.C."
                ],
                "last": "Lien",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "K.",
                "middle": [],
                "last": "Narendran",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "B.H.",
                "middle": [],
                "last": "Heng",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "A.E.",
                "middle": [],
                "last": "Ling",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Since the issue of a global alert on atypical pneumonia by the World Health Organization on March 12, reported cases of SARS increased daily and appeared in other countries, including Canada, the United States, Europe, and Africa. The first three cases in Singapore were reported on March 13. These cases were traced to a doctor from Guangdong who infected 13 guests at a Hong Kong hotel (13). The clinical features of SARS are fairly nonspecific with a body temperature of >38\u00b0C, occurring in 100% of patients, being the most sensitive feature in all the case series published thus far (6\u20138). Other symptoms described thus far have included nonproductive cough, dyspnea, malaise, diarrhea, chest pain, headache, myalgia, and vomiting.",
            "cite_spans": [
                {
                    "start": 389,
                    "end": 391,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 588,
                    "end": 589,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 590,
                    "end": 591,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "We describe here a fairly complicated atypical signs and symptoms of SARS in an elderly patient. The patient had a fever, which responded to a course of broad-spectrum antimicrobial drugs, thus behaving in a manner not much different from a typical community-acquired pneumonia. The absence of fever during the final course of the patient\u2019s hospitalization could have been caused by an altered immune response in the geriatric age group, with a resulting normal leukocyte count. Furthermore, prior usage of antimicrobial drugs and possible aspiration from dysphagia may further complicate detection of the disease. The suspicion of SARS in this case was thus low before eventual epidemiologic links were established retrospectively. Dyspnea is a common symptom reported previously, ranging from 60% to 80% of patients. Cough has also been noted in 80% to 100% of cases in previous studies (6,8). However the absence of cough, especially in the elderly, could be due to an underlying weak cough reflex. Vomiting, though present in our patient, was only accounted for in 10% of cases in the Canadian series (8). In a frail older person, this could also be caused by a number of circumstances.",
            "cite_spans": [
                {
                    "start": 890,
                    "end": 891,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 892,
                    "end": 893,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 1106,
                    "end": 1107,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Our patient had characteristic lymphopenia, which was seen in about 90% of reported cases. In addition, she also had mild hyponatremia and elevated C-reactive protein. However, thrombocytopenia, elevated transaminases, or raised creatine kinase levels were absent.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Serial chest radiograph progressed from a predominantly right lower lobe patchy consolidation to a radiographic picture of congestive cardiac failure. Reports from SARS cases have described mainly basal lung opacities, without any pleural effusion. An underlying poor cardiac function may masquerade the true picture of the air space disease characteristic of SARS, especially if the stress of infection decompensates left ventricular ejection fraction. This radiologic interpretation could potentially mislead clinicians and lead to more patients, family members, and healthcare workers becoming infected. In addition, a bimodal pattern of time to deterioration of clinical symptoms has been previously reported (14).",
            "cite_spans": [
                {
                    "start": 714,
                    "end": 716,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "The information currently available on transmission of SARS has been attributed to respiratory droplets from close contact which has been defined by WHO to be having cared for, having lived with, or having direct contact with respiratory secretions or body fluids of a patient known to be a suspected SARS case. As the patient lived in a nursing home, the brief social contact during visits by family and friends, may prove sufficient for transmitting the virus.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Furthermore, the issue of possible coinfection and the influence of coexisting conditions have not been thoroughly investigated, which may change the clinical picture of SARS so as to conceal detection. Uncharacteristic clinical signs and symptoms, without any travel or contact history, are difficult to recognize.",
            "cite_spans": [],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "Our case serves to highlight atypical signs and symptoms of SARS, especially the resolving fever, delay in establishing a positive contact history, and the nonspecific chest radiographic appearance that could be affected by concurrent coexisting conditions, such as cardiac failure. We wish to draw attention to clinicians, so that a high level of suspicion is present as the SARS-CoV is highly contagious and can cause severe disease. We observed that despite being cared for in the general ward by staff without full personal protective equipment, only one healthcare worker in Changi General Hospital was infected. This observation supports the hypothesis that the virus may not transmit effectively under certain conditions. Nevertheless, late diagnosis may lead to large clusters, as delayed isolation of suspect cases increases the risk of onward transmission in the community (15). A positive contact history may not be obvious, particularly in patients with cognitive impairment, until retrospective analysis is done. There is thus a need for continued surveillance of fever and clusters of pneumonia cases to improve the chances of early detection. Nonetheless, with the imminent availability of accurate and rapid diagnostic tests, there is hope that the diagnosis of SARS can be made with more certainty. This could be further enhanced by a revised case definition.",
            "cite_spans": [
                {
                    "start": 884,
                    "end": 886,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Chest radiograph at first admission.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Repeat chest radiograph as second admission.",
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Figure 3: Cases linked to index D.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Coronavirus as a possible cause of severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1319-25",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13077-2"
                ]
            }
        },
        "BIBREF1": {
            "title": "Preventing local transmission of SARS: lessons from Singapore.",
            "authors": [],
            "year": 2003,
            "venue": "Med J Aust",
            "volume": "178",
            "issn": "",
            "pages": "555-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Severe acute respiratory syndrome\u2014Singapore, 2003.",
            "authors": [],
            "year": 2003,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "52",
            "issn": "",
            "pages": "405-11",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Severe acute respiratory syndrome (SARS) in Singapore: clinical features of index patient and initial contacts.",
            "authors": [],
            "year": 2003,
            "venue": "Emerg Infect Dis",
            "volume": "9",
            "issn": "",
            "pages": "713-7",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Update: outbreak of severe acute respiratory syndrome\u2014worldwide, 2003.",
            "authors": [],
            "year": 2003,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "52",
            "issn": "",
            "pages": "241-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Clinical progression and viral load in a community outbreak of coronavirus-associated SARS pneumonia: a prospective study.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1767-72",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13412-5"
                ]
            }
        },
        "BIBREF6": {
            "title": "Epidemiological determinants of spread of causal agent of severe acute respiratory syndrome in Hong Kong.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1761-6",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13410-1"
                ]
            }
        },
        "BIBREF7": {
            "title": "A novel coronavirus associated with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1956-66",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030781"
                ]
            }
        },
        "BIBREF8": {
            "title": "Identification of a novel coronavirus in patients with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1967-76",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030747"
                ]
            }
        },
        "BIBREF9": {
            "title": "A multicentre collaboration to investigate the cause of severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1730-3",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13376-4"
                ]
            }
        },
        "BIBREF10": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF11": {
            "title": "A cluster of cases of severe acute respiratory syndrome in Hong Kong.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1977-85",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030666"
                ]
            }
        },
        "BIBREF12": {
            "title": "A major outbreak of severe acute respiratory syndrome in Hong Kong.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1984-94",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030685"
                ]
            }
        },
        "BIBREF13": {
            "title": "Identification of severe acute respiratory syndrome in Canada.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1995-2005",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030634"
                ]
            }
        },
        "BIBREF14": {
            "title": "Evaluation of WHO criteria for identifying patients with Severe Acute Respiratory Syndrome out of hospital: prospective observational study.",
            "authors": [],
            "year": 2003,
            "venue": "BMJ",
            "volume": "326",
            "issn": "",
            "pages": "1354-8",
            "other_ids": {
                "DOI": [
                    "10.1136/bmj.326.7403.1354"
                ]
            }
        }
    }
}