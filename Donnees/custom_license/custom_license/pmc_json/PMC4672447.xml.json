{
    "paper_id": "PMC4672447",
    "metadata": {
        "title": "Porcine Epidemic Diarrhea Virus among Farmed Pigs, Ukraine",
        "authors": [
            {
                "first": "Akbar",
                "middle": [],
                "last": "Dastjerdi",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "John",
                "middle": [],
                "last": "Carr",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Richard",
                "middle": [
                    "J."
                ],
                "last": "Ellis",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Falko",
                "middle": [],
                "last": "Steinbach",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Susanna",
                "middle": [],
                "last": "Williamson",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "The outbreak occurred at a large, indoor, 5,000-sow farm in the Poltava region of Ukraine where 240 sows per week were kept to give birth, which is referred to as farrowing on porcine farms. ",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Clinical signs were first seen in a farrowing sow that was vomiting and had profuse diarrhea 10 days postfarrowing. Within hours, her piglets began to vomit and show profuse watery diarrhea. Vomiting and diarrhea then spread throughout the farrowing area. Disease was most severe among piglets <10 days of age; the case-fatality rate reached nearly 100%. The decision was made to euthanize piglets <10 days of age during a 3-week period from the start of the outbreak. Piglets >10 days old became sick, but most (95%) survived. Disease was less severe in adults, whose appetite returned and diarrhea ceased within 3 days. ",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Abortions occurred immediately following the outbreak in 38% of 1 batch of sows that were at 28\u201335 days\u2019 gestation; no diagnostic investigation was made to determine the cause. It was not established whether they were a result of the PED outbreak or control methods such as \u201cback-feeding\u201d fecal material to piglets. Abortions did not occur in other groups of sows and have not been reported as a feature of PED outbreaks in North America. The reproductive status of sows at other stages of gestation was not substantially affected by the initial PED outbreak, and no higher return rate was observed. Postpartum sows did not fail to produce milk, but those affected by PEDV had a reduced feed intake and associated reduced milk output. ",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The performance of the pig unit took 20 weeks to return to preoutbreak levels. During that time, a total of 30,000 piglets died, which equates to a loss of 6 weaned piglets per sow per year.",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The outbreak was controlled by a combination of 2 methods. Lactogenic immunity enhancement was initiated by deliberate reexposure of pregnant sows to infected piglet feces 6 weeks before farrowing. The environmental viral load was reduced by cleaning and disinfection of the area, euthanizing of neonatal piglets, and reduction of transmission by humans and other vectors through enhanced internal biosecurity. ",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "A diagnosis of PED was made after clinical and postmortem examination of affected piglets. We tested them onsite using a lateral flow device, Antigen Rapid PED/TGE Ag Test Kit (Bionote, Hwasung-si, Korea), which indicated the presence of PEDV antigen in the feces. We confirmed these findings at the UK Animal and Plant Health Agency using an in-house PEDV PCR and a commercially available PEDV/transmissible gastroenteritis virus quantitative reverse transcription-PCR kit (QIAGEN, Hilden, Germany). A BLAST search (http://blast.ncbi.nlm.nih.gov/Blast.cgi) of the 160-nt PCR amplicon revealed the highest similarity (99%) to PEDV strains from the United States and China. PEDV RNA was then subjected to deoxyribonuclease I digestion and converted to cDNA for preparation of sequencing libraries by using a Nextera XT kit (Illumina, San Diego, CA, USA). Paired-end sequencing was performed on an Illumina MiSeq. The consensus sequence was obtained by de novo assembly by using the Velvet 1.2.10 algorithm (12) of the sequence reads that mapped to the reference strain (GenBank accession no. NC003426).",
            "cite_spans": [
                {
                    "start": 1006,
                    "end": 1008,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "The PEDV Ukraine/Poltava01/2014 strain genome (GenBank accession no. KP403954) is 27823 nt (excluding the 3\u2032 poly A tail). Nucleotide analyses of the full genome of the virus showed the highest similarity to PEDV strains reported in 2013 from the United States; specifically, strains USA/Kansas29/2013 (GenBank accession no. KJ645637.1) and USA/Colorado30/2013 (GenBank accession no. KJ645638.1), with 99.8% nucleotide identity. The nucleotide identity was substantially lower (98.5%) when compared to those of the 2014 strains isolated in Germany, which are similar to another strain isolated in North America, Ohio851/2013 (8). Accordingly, the Ukraine virus clustered phylogenetically with PEDV strains from North America in genetic clade II (6) but was distinct from strains currently and previously found elsewhere in Europe, such as the prototype PEDV strain CV777, with which it shares only 96.5% homology (Figure [13]). However, the genetic analysis does not, at this stage, support drawing conclusions regarding the relative pathogenicity of this apparently virulent PEDV strain, similar to past instances of PEDV in Europe, of which very few have been characterized; parallel experimental infection of pigs would be required for further investigation.",
            "cite_spans": [
                {
                    "start": 626,
                    "end": 627,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 746,
                    "end": 747,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 922,
                    "end": 924,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 914,
                    "end": 920,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "The farm was found to be free from Mycoplasma hyopneumoniae, Brachyspira hyodysenteriae, mange, toxigenic Pasteurella multocida, porcine reproductive and respiratory syndrome virus, transmissible gastroenteritis virus, Aujeszky\u2019s Disease virus, and classical and African swine fever viruses. Persons on the farm practiced strict biosecurity measures to maintain a specific pathogen free status. No breach in biosecurity could be identified after a review of potential PEDV introduction routes involving pigs or humans or vehicles, equipment, and other fomites; the source of infection remains unknown. However, there were reports of a PED-affected pig farm 1.5 km from this unit, and the potential for unidentified fomite or other transmission from this herd, or another undisclosed infected herd, therefore existed. Windborne transmission has been suggested to explain some outbreaks in the United States (14) and is another possibility to consider in this case.",
            "cite_spans": [
                {
                    "start": 907,
                    "end": 909,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "This PED outbreak in Ukraine showed clinical characteristics similar to outbreaks caused by virulent strains of PEDV reported from North America (4). The virus clusters phylogenetically with viruses from recent outbreaks in the United States and Mexico (6). The presence of such a PEDV strain in Ukraine highlights the threat to neighboring countries and those in the European Union where PEDV has not been detected (e.g., Scandinavia) or has not caused disease in recent decades (e.g., the United Kingdom) and where pig herds are considered largely naive to PEDV. Furthermore, pig farming has changed over recent decades, and the establishment of more large holdings would produce more virus following introduction of PEDV. ",
            "cite_spans": [
                {
                    "start": 146,
                    "end": 147,
                    "mention": "4",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 254,
                    "end": 255,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        },
        {
            "text": "This outbreak emphasizes the need for decision-makers of countries, pig farms, and allied industries to implement and maintain biosecurity measures to minimize the risk for spread of PEDV to new areas. Early detection of suspected clinical signs on pig farms and, when these occur, prompt testing for PEDV by PCR are also vital (15). A coordinated approach is essential to prevent introduction of PEDV, promote early detection should introduction occur, control disease, and minimize spread of infection.",
            "cite_spans": [
                {
                    "start": 329,
                    "end": 331,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Conclusions",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure: Phylogenetic analysis of the full-length genome of the porcine epidemic diarrhea virus (PEDV) Ukraine/Poltava01/2014 (bold text). The full-length genomes of PEDV were aligned by using the MegAlign software of the DNASTAR Lasergene Core Suite (DNASTAR, Inc., Madison, WI, USA) and phylogenetic analysis was done by using MEGA 5.2 software (13). The tree was constructed by using the neighbor-joining method and 1,000 bootstrap replications. Only bootstrap values of more than 50% are shown in the figure. Each virus on the tree is represented by accession number, strain, and year of sample collection. Ukraine/Poltava01/2014 clusters in close proximity to recent strains in the United States other than the Ohio851 variant, and both are substantially genetically different from the previous European variants, such as the prototype strain CV777, which is embedded in another cluster. Scale bar indicates nucleotide substitutions per site.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "An apparently new syndrome of porcine epidemic diarrhea.",
            "authors": [],
            "year": 1977,
            "venue": "Vet Rec",
            "volume": "100",
            "issn": "",
            "pages": "243-4",
            "other_ids": {
                "DOI": [
                    "10.1136/vr.100.12.243"
                ]
            }
        },
        "BIBREF1": {
            "title": "",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Velvet: algorithms for de novo short read assembly using de Bruijn graphs.",
            "authors": [],
            "year": 2008,
            "venue": "Genome Res",
            "volume": "18",
            "issn": "",
            "pages": "821-9",
            "other_ids": {
                "DOI": [
                    "10.1101/gr.074492.107"
                ]
            }
        },
        "BIBREF4": {
            "title": "Molecular evolutionary genetics analysis using maximum likelihood, evolutionary distance, and maximum parsimony methods.",
            "authors": [],
            "year": 2011,
            "venue": "Mol Biol Evol",
            "volume": "28",
            "issn": "",
            "pages": "2731-9",
            "other_ids": {
                "DOI": [
                    "10.1093/molbev/msr121"
                ]
            }
        },
        "BIBREF5": {
            "title": "Evidence of infectivity of airborne porcine epidemic diarrhea virus and detection of airborne viral RNA at long distances from infected herds.",
            "authors": [],
            "year": 2014,
            "venue": "Vet Res",
            "volume": "45",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1186/s13567-014-0073-z"
                ]
            }
        },
        "BIBREF6": {
            "title": "Emergence of severe porcine epidemic diarrhea in pigs in the USA.",
            "authors": [],
            "year": 2013,
            "venue": "Vet Rec",
            "volume": "173",
            "issn": "",
            "pages": "146-8",
            "other_ids": {
                "DOI": [
                    "10.1136/vr.f4947"
                ]
            }
        },
        "BIBREF7": {
            "title": "Porcine epidemic diarrhea virus: a comprehensive review of molecular epidemiology, diagnosis, and vaccines.",
            "authors": [],
            "year": 2012,
            "venue": "Virus Genes",
            "volume": "44",
            "issn": "",
            "pages": "167-75",
            "other_ids": {
                "DOI": [
                    "10.1007/s11262-012-0713-1"
                ]
            }
        },
        "BIBREF8": {
            "title": "Outbreak of porcine epidemic diarrhea in suckling piglets, China.",
            "authors": [],
            "year": 2012,
            "venue": "Emerg Infect Dis",
            "volume": "18",
            "issn": "",
            "pages": "161-3",
            "other_ids": {
                "DOI": [
                    "10.3201/eid1801.111259"
                ]
            }
        },
        "BIBREF9": {
            "title": "Emergence of Porcine epidemic diarrhea virus in the United States: clinical signs, lesions, and viral genomic sequences.",
            "authors": [],
            "year": 2013,
            "venue": "J Vet Diagn Invest",
            "volume": "25",
            "issn": "",
            "pages": "649-54",
            "other_ids": {
                "DOI": [
                    "10.1177/1040638713501675"
                ]
            }
        },
        "BIBREF10": {
            "title": "New variant of porcine epidemic diarrhea virus, United States, 2014.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "917-9",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2005.140195"
                ]
            }
        },
        "BIBREF11": {
            "title": "Distinct characteristics and complex evolution of PEDV strains, North America, May 2013-February 2014.",
            "authors": [],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "",
            "pages": "1620-8",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2010.140491"
                ]
            }
        },
        "BIBREF12": {
            "title": "Investigation into the role of potentially contaminated feed as a source of the first-detected outbreaks of porcine epidemic diarrhea in Canada.",
            "authors": [],
            "year": 2014,
            "venue": "Transbound Emerg Dis",
            "volume": "61",
            "issn": "",
            "pages": "397-410",
            "other_ids": {
                "DOI": [
                    "10.1111/tbed.12269"
                ]
            }
        },
        "BIBREF13": {
            "title": "Comparison of Porcine Epidemic Diarrhea Viruses from Germany and the United States.",
            "authors": [],
            "year": 2015,
            "venue": "Emerg Infect Dis",
            "volume": "21",
            "issn": "",
            "pages": "493-6",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2103.141165"
                ]
            }
        },
        "BIBREF14": {
            "title": "",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}