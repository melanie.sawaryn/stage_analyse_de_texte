{
    "paper_id": "PMC6537730",
    "metadata": {
        "title": "Influenza D Virus Infection in Dromedary Camels, Ethiopia",
        "authors": [
            {
                "first": "Shin",
                "middle": [],
                "last": "Murakami",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Tomoha",
                "middle": [],
                "last": "Odagiri",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Simenew",
                "middle": [
                    "Keskes"
                ],
                "last": "Melaku",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Boldbaatar",
                "middle": [],
                "last": "Bazartseren",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hiroho",
                "middle": [],
                "last": "Ishida",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Akiko",
                "middle": [],
                "last": "Takenaka-Uema",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yasushi",
                "middle": [],
                "last": "Muraki",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Hiroshi",
                "middle": [],
                "last": "Sentsui",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Taisuke",
                "middle": [],
                "last": "Horimoto",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "Influenza D virus (IDV) was first isolated from pigs with respiratory symptoms in the United States in 2011 (1). Epidemiologic analyses revealed that the most likely main host of IDV is cattle, because the seropositivity rate in these animals is higher than that for other livestock (2\u20134). In a recent report, dromedary camels (Camelus dromedaries) exhibited substantially high seroprevalence (99%) for IDV in Kenya (5), suggesting that this animal is a potential reservoir of IDV. We examined seroprevalence of IDV in dromedary camels in Ethiopia and in Bactrian camels (Camelus bactrianus) in Mongolia.",
            "cite_spans": [
                {
                    "start": 109,
                    "end": 110,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 284,
                    "end": 285,
                    "mention": "2",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 286,
                    "end": 287,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 417,
                    "end": 418,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We collected serum samples from dromedary camels (n = 38; average age 4.3 years, range 1\u201313 years), goats (n = 20; average age 3.9 years, range 1\u20138 years), sheep (n = 20; average age 2.7 years, range 1\u20134 years), cattle (n = 15; average age 6.7 years, range, 1\u201311 years), and donkeys (n = 2; ages 1 and 6) from 2 herds in Bati district, Amhara region, and 1 herd in Fafen district, Somali region, Ethiopia. All animals were apparently healthy, shared the same pasturage during the day, and stayed in barns specific for each animal species at night. To detect influenza D infection, we titrated the serum samples by hemagglutination inhibition (HI) assay using 3 antigenically distinct influenza D strains: D/swine/Oklahoma/1334/2011 (D-OK lineage; D/OK) (1), D/bovine/Nebraska/9\u20135/2013 (D/660-lineage; D/NE) (6), and D/bovine/Yamagata/10710/2016 (D/Japan-lineage; D/Yamagata) (7). For the HI test, we treated the samples with receptor-destroying enzyme (RDEII; Denka Seiken, http://www.keyscientific.com) at 37\u00b0C for 16 h, followed by heat inactivation at 56\u00b0C for 30 min. We then reacted serially diluted samples with each virus (4 HAU) at room temperature for 30 min and incubated them with a 0.6% suspension of turkey red blood cells at room temperature for 30 min. The HI titer of each sample was expressed as the reciprocal of the highest sample dilution that completely inhibited HA. We considered samples with HI titer >1:40 positive, to eliminate nonspecific reactions at low dilutions (4,8,9).",
            "cite_spans": [
                {
                    "start": 754,
                    "end": 755,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 808,
                    "end": 809,
                    "mention": "6",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 876,
                    "end": 877,
                    "mention": "7",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1494,
                    "end": 1495,
                    "mention": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1496,
                    "end": 1497,
                    "mention": "8",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1498,
                    "end": 1499,
                    "mention": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Of the 21 dromedary camel samples from Bati, 10 were positive for D/OK, 11 for D/NE, and 19 for D/Yamagata (Figure). Of other animal samples, only 1 goat sample was positive (titer 1:40), indicating that the prevalence rate of influenza D antibodies was higher in dromedary camels than in co-grazing ruminants in the tested herd. The data on the camels\u2019 age indicated that the HI antibodies were not detected due to maternal antibodies, which is only stable for 5\u20136 months in dromedary camels (10). Much closer face-to-face contact may be required for virus transmission among different animal species. The HI titers in camel samples were higher for D/Yamagata (range 1:40\u20131:160) than those for D/OK and D/NE (1:40\u20131:80). Meanwhile, we found several positives in dromedary camel samples from Fafen, albeit at lower positive rates and titers compared with those in Bati (Figure). ",
            "cite_spans": [
                {
                    "start": 494,
                    "end": 496,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "",
            "ref_spans": [
                {
                    "start": 108,
                    "end": 114,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 870,
                    "end": 876,
                    "mention": "Figure",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "We confirmed the specificity of the HI reaction with a viral neutralizing test using HI-positive samples (data not shown). HI titers obtained were not high, suggesting that the infections may have occurred in these animals some time ago or that results might have been due to the variance of HI methods used in each laboratory. For example, turkey red blood cell was used in this study, whereas horse red blood cell was used in a previous study (5). Nonetheless, these data suggest that the virus antigenically related to D/Yamagata was circulating in dromedary camels in this region.",
            "cite_spans": [
                {
                    "start": 446,
                    "end": 447,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "In a previous report, a considerable number of dromedary camels in Kenya were seropositive not only for influenza D but also for influenza C virus (ICV) (5). Thus, we additionally used C/Ann Arbor/1/1950 virus as an antigen for HI assay. The results suggest a limited circulation of ICV in this area because 0 samples in Bati were positive and only 2 in Fafen, which were negative for IDV, were positive (titers 1:40). In addition, we performed the HI test using selected samples following preadsorption with ICV. We did not observe any significant decrease in HI titers to IDV, suggesting no cross-reactivity between IDV and ICV in our samples.",
            "cite_spans": [
                {
                    "start": 154,
                    "end": 155,
                    "mention": "5",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We also collected serum samples of apparently healthy Bactrian camels (n = 40) in Dundgovi, Zavkhan, and Umnugovi Provinces, Mongolia, and tested for HI antibody for IDV. These samples did not test positive for these IDV strains.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "Despite the limited samples tested, this study suggests that dromedary camels in East Africa might play a substantial role in the circulation of IDV. Further studies using additional samples from multiple countries are expected to clarify the role of this animal on the ecology and epidemiology of this virus, including its reservoir potential in nature.",
            "cite_spans": [],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure: Hemagglutination inhibition (HI) antibody titers for influenza D viruses in serum samples from dromedary camels, Bati and Fafen Districts, Ethiopia. Each dot represents 1 camel. HI assay was performed with RDE(II)-treated serum samples and turkey red blood cells (0.6%) against D/swine/Oklahoma/1334/2011 (D/OK), D/bovine/Nebraska/9\u20135/2013 (D/NE), and D/bovine/Yamagata/10710/2016 (D/Yamagata). HI-positive rate for each virus is shown below the virus name.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Isolation of a novel swine influenza virus from Oklahoma in 2011 which is distantly related to human influenza C viruses.",
            "authors": [],
            "year": 2013,
            "venue": "PLoS Pathog",
            "volume": "9",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.ppat.1003176"
                ]
            }
        },
        "BIBREF1": {
            "title": "Time course of MERS-CoV infection and immunity in dromedary camels.",
            "authors": [],
            "year": 2016,
            "venue": "Emerg Infect Dis",
            "volume": "22",
            "issn": "",
            "pages": "2171-3",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2212.160382"
                ]
            }
        },
        "BIBREF2": {
            "title": "Characterization of a novel influenza virus in cattle and Swine: proposal for a new genus in the Orthomyxoviridae family.",
            "authors": [],
            "year": 2014,
            "venue": "MBio",
            "volume": "5",
            "issn": "",
            "pages": "e00031-14",
            "other_ids": {
                "DOI": [
                    "10.1128/mBio.00031-14"
                ]
            }
        },
        "BIBREF3": {
            "title": "Influenza D virus infection in Mississippi beef cattle.",
            "authors": [],
            "year": 2015,
            "venue": "Virology",
            "volume": "486",
            "issn": "",
            "pages": "28-34",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virol.2015.08.030"
                ]
            }
        },
        "BIBREF4": {
            "title": "Serological evidence for high prevalence of Influenza D Viruses in Cattle, Nebraska, United States, 2003-2004.",
            "authors": [],
            "year": 2017,
            "venue": "Virology",
            "volume": "501",
            "issn": "",
            "pages": "88-91",
            "other_ids": {
                "DOI": [
                    "10.1016/j.virol.2016.11.004"
                ]
            }
        },
        "BIBREF5": {
            "title": "Serologic evidence for influenza C and D virus among ruminants and camelids, Africa, 1991\u20132015.",
            "authors": [],
            "year": 2017,
            "venue": "Emerg Infect Dis",
            "volume": "23",
            "issn": "",
            "pages": "1556-9",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2309.170342"
                ]
            }
        },
        "BIBREF6": {
            "title": "Cocirculation of two distinct genetic and antigenic lineages of proposed influenza D virus in cattle.",
            "authors": [],
            "year": 2015,
            "venue": "J Virol",
            "volume": "89",
            "issn": "",
            "pages": "1036-42",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02718-14"
                ]
            }
        },
        "BIBREF7": {
            "title": "Influenza C and D viruses package eight organized ribonucleoprotein complexes.",
            "authors": [],
            "year": 2018,
            "venue": "J Virol",
            "volume": "92",
            "issn": "",
            "pages": "e02084-17",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02084-17"
                ]
            }
        },
        "BIBREF8": {
            "title": "Influenza D virus infection in herd of cattle, Japan.",
            "authors": [],
            "year": 2016,
            "venue": "Emerg Infect Dis",
            "volume": "22",
            "issn": "",
            "pages": "1517-9",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2208.160362"
                ]
            }
        },
        "BIBREF9": {
            "title": "Serological evidence for the presence of influenza D virus in small ruminants.",
            "authors": [],
            "year": 2015,
            "venue": "Vet Microbiol",
            "volume": "180",
            "issn": "",
            "pages": "281-5",
            "other_ids": {
                "DOI": [
                    "10.1016/j.vetmic.2015.09.005"
                ]
            }
        }
    }
}