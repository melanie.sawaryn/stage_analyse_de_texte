{
    "paper_id": "PMC3291385",
    "metadata": {
        "title": "SARS\u2013associated Coronavirus Replication in Cell Lines",
        "authors": [
            {
                "first": "Matthew",
                "middle": [],
                "last": "Kaye",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Julian",
                "middle": [],
                "last": "Druce",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Thomas",
                "middle": [],
                "last": "Tran",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Renata",
                "middle": [],
                "last": "Kostecki",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Doris",
                "middle": [],
                "last": "Chibo",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jessica",
                "middle": [],
                "last": "Morris",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Mike",
                "middle": [],
                "last": "Catton",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Chris",
                "middle": [],
                "last": "Birch",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "An isolate of SARS-CoV, strain HKU 39849, was passaged on 2 occasions in Vero E6 cells to establish a high-titer stock that was used in all infectivity experiments. Because SARS-CoV is classified as a risk group level 4 pathogen in Australia, all procedures performed with the virus, including infecting cell lines and viral lysis before RNA extraction, were carried out in a physical containment level 4 (PC4) laboratory.",
            "cite_spans": [],
            "section": "Virus ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "The cell lines investigated for their susceptibility to SARS-CoV are shown in the Table. They were chosen because they were present in our cell repository and were used either routinely or occasionally for virus isolation attempts as part of diagnostic or research projects. Confluent cells were maintained at 34\u00b0C in 25-mL flasks (Nunc, Roskilde, Denmark) containing 10 mL appropriate maintenance medium supplemented with fetal bovine serum (FBS) (Thermo Trace, Melbourne, Victoria, Australia), 100 U/mL penicillin, and 100 \u03bcg/mL streptomycin (JRH Biosciences, Lenexa, KS, USA). BGM, FRhK, HEK-293, HEL, Hep G2, L20, MA-104, pCMK, and RD-A cell lines were all maintained in modified Eagle medium (MEM) supplemented with 10% FBS. MDCK cells were maintained in MEM supplemented with 5% FBS. HeLa-T cells were maintained in basal medium Eagle supplemented with 10% FBS. COS, Huh-7, Vero, and Vero E6 cell lines were maintained in Dulbecco modified Eagle medium (DMEM) supplemented with 10% FBS. CV-1, Hep-2, LLC-Mk2, MEK, and RK-13 cells were maintained in 199 medium with 5% FBS, and A549 cells were maintained in RPMI 1640 medium supplemented with 10% FBS. Confluent cells were infected with SARS-CoV, which resulted in a multiplicity of infection of 1.7 (results not shown) or were mock-infected with medium only. An additional flask was also prepared in which the original inoculum was incubated under the same experimental conditions but within a cell-free environment.",
            "cite_spans": [],
            "section": "Cell Lines ::: Materials and Methods",
            "ref_spans": [
                {
                    "start": 82,
                    "end": 87,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "On days 4, 7, and 11 after infection, cells were observed for SARS-CoV\u2013specific cytopathic effects (CPE), supernatants were collected for virus detection and quantification by polymerase chain reaction (PCR), and the maintenance medium was replaced. Cells were tested for virus-specific antigens with an indirect immunofluorescence assay 11 days after infection with SARS-CoV if no CPE was observed (or when CPE developed that involved at least 75% of the cell monolayer). Eleven days after infection, cell lines negative for indicators of viral replication were blind-passaged twice for 7 days by adding 100 \u03bcL culture supernatant to the cells in question as well as to the highly susceptible Vero E6 cells. During these passages, cells were observed for SARS-CoV\u2013specific CPE, and after the second passage, supernatants were collected for virus detection and quantification by PCR.",
            "cite_spans": [],
            "section": "Cell Lines ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "A 300-\u03bcL volume of lysis buffer containing guanidinium thiocyanate and Triton X-100 (Roche Diagnostics, Mannheim, Germany) was added to 200 \u03bcL supernatant from cell cultures that had either been infected with SARS-CoV or were mock-infected. These samples were removed from the PC4 laboratory to a PC2 laboratory, where they underwent nucleic acid extraction with a MagNA Pure LC Total Nucleic Acid Isolation Kit with a MagNA Pure LC automated extraction robot (Roche Diagnostics). A 10-\u03bcL volume of eluate was treated for 10 min at 65\u00b0C and added to 12 \u03bcL reverse transcription master mix containing 5.2 A260 U/mL random hexamers (Roche Diagnostics), 0.17 \u03bcmol/L deoxynucleoside triphosphates (Roche Diagnostics), and 7.5 U AMV-RT enzyme (Promega, Madison, WI, USA). After incubation at 42\u00b0C for 30 min, then 100\u00b0C for 10 min, cDNA products were stored at 4\u00b0C until analyzed by PCR.",
            "cite_spans": [],
            "section": "RNA Extraction ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Real-time PCR that amplified an 81-bp fragment of the nucleoprotein gene was used to detect and quantify SARS-CoV by reference to a cycle threshold (Ct). The assay used ABI-7000 Prism instrumentation (Applied Biosystems, Foster City, CA, USA) with primers and probes designed with the associated Primer Express software. The forward primer was SARNP-F: 5\u00b4-CCC AGA TGG TAC TTC TAT TAC CTA GGA-3\u00b4. The reverse primer was SARNP-R: 5\u00b4-CCA TAC GAT GCC TTC TTT GTT AG-3\u00b4. The probe was SARNP-P: 6FAM 5\u00b4-AAG CTT CAC TTC CCT ACG G-3\u00b4 with 3\u00b4 MGB. For real-time PCR, 5 \u03bcL template cDNA was added to ABI TaqMan Universal PCR Master Mix (Applied Biosystems) containing 0.9 \u03bcmol/L each primer and 0.2 \u03bcmol/L probe in a total volume of 45 \u03bcL. The cycling conditions were as follows: 2 min at 50\u00b0C, 10 min at 95\u00b0C, then 45 cycles of 15 s at 95\u00b0C and 1 min at 60\u00b0C. Reference to a standard curve (not shown) demonstrated that negative changes in Ct values of 3.6 represented increases in virus titer of 1.0 log10.",
            "cite_spans": [],
            "section": "Quantitative Real-time PCR for SARS-CoV ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Cells were collected 11 days after infection if no CPE was observed by microscopy or on the day they developed CPE involving at least 75% of the cell monolayer. Cells were manually scraped from monolayers into 1 mL culture medium, then subjected to 50 kGy gamma radiation before being spotted onto a slide, air dried, and fixed in acetone for 10 min. Earlier testing showed that this dose of gamma radiation reduced the titer of SARS-CoV by at least 106 50% tissue culture infectious doses (results not shown). A 10-\u03bcL volume of diluted convalescent-phase serum from a SARS-CoV\u2013infected patient was added to the fixed cells followed by incubation at 37\u00b0C for 30 min in a humidified chamber. The slides were washed twice with phosphate-buffered saline (PBS), dried, and each cell spot overlaid with 10 \u03bcL anti-human fluorescein isothiocyanate\u2013conjugated secondary antibody (BioM\u00e9rieux, Durham, NC, USA) for 30 min at 37\u00b0C. The slides were washed twice with PBS before they were mounted with cover slips. Virus-specific immunofluorescence was read by using an Axioskop UV microscope (Zeiss, Oberkochen, Germany). The final results for the indirect immunofluorescence assay, as shown in the Table, were based on the observations of 2 independent readers.",
            "cite_spans": [],
            "section": "Indirect Immunofluorescence Assay ::: Materials and Methods",
            "ref_spans": []
        },
        {
            "text": "Susceptibilities to SARS-CoV of the cell lines we investigated are shown in the Table. The results obtained on 21 lines are indicated: 14 were tested for the first time, and 7 had been previously reported by others (15). Of the 7 cell lines tested previously, we confirmed previous data that showed that 4 of them could support replication. Of the 14 lines tested for the first time, 10 were shown to support replication of SARS-CoV. In general, cells derived from nonhuman primate kidneys were susceptible. A human liver cell line (Hep G2) and rabbit kidney cells (RK-13) also supported replication.",
            "cite_spans": [
                {
                    "start": 216,
                    "end": 218,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 80,
                    "end": 85,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "SARS-CoV replication in BGM, CV-1, FRhK, LLC-Mk2, MA-104, pCMK, RK-13, and Vero cell lines produced a CPE as early as day 4 after inoculation, with evidence of high levels of virus-specific RNA established by quantitative PCR. CPE was focal, with cell rounding and a refractivity that was soon followed by cell detachment, and CPE quickly spread to involve the entire cell monolayer (Figure 1). In contrast, neither MEK nor COS cells produced a SARS-CoV\u2013specific CPE (Figure 1), despite evidence of rapid (MEK) or limited (COS) replication, as determined by quantitative PCR (Figure 2) and indirect immunofluorescence testing (not shown). For the cell lines capable of supporting SARS-CoV replication, immunofluorescence results confirmed quantitative PCR results in all cases (Table).",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 384,
                    "end": 392,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 468,
                    "end": 476,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 576,
                    "end": 584,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 778,
                    "end": 783,
                    "mention": "Table",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Figure 2 shows the quantitative PCR results for representative cell lines for the 11 days during which isolation was attempted. The results are depicted as Ct values relative to the Ct values of a cell-free preparation. The cell-free preparation had an initial Ct value of 31, obtained when the original inoculum was seeded into a flask containing 10 mL DMEM. This input Ct increased to a Ct value of 40 by day 11 after infection. The supernatants from BGM, CV-1, MEK, Vero, and Vero E6 cell lines yielded Ct values 12\u201317 units lower than the initial cell-free inoculum by day 4 after infection. This number equated to titer increases 3.3\u20134.7 log10/mL above the input virus for these cells. The results for HeLa-T, Hep-2, and MDCK cells, representing cell lines that do not support SARS-CoV replication, are shown in Figure 2. In these cell lines, the Ct values at 4 days after infection were at levels similar to those of the cell-free inoculum. At later times, after successive media changes, Ct values increased in a manner similar to that of the cell-free control preparation, indicating dilution of input virus and absence of any subsequent viral replication. Blind passaging of supernatant fluid from these cell lines confirmed these results (not shown). In contrast, Ct values for COS cells did not change over the course of the experiment, which suggests that viral replication occurred at a low level, sufficient to maintain similar viral titers to those of input levels through several medium changes.",
            "cite_spans": [],
            "section": "Results",
            "ref_spans": [
                {
                    "start": 0,
                    "end": 8,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 817,
                    "end": 825,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ]
        },
        {
            "text": "After the SARS epidemic ended, several cases have occurred as a direct or indirect result of breaches in laboratory biosafety (11\u201313). These breaches highlight the need to safely handle virus in the laboratory, which includes knowing which cell lines may be susceptible to infection. In this study we add to the list of cells known to support replication of SARS-CoV.",
            "cite_spans": [
                {
                    "start": 127,
                    "end": 129,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 130,
                    "end": 132,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Our approach to establishing susceptibility to infection was to use quantitative PCR supported by immunofluorescence testing. The quantitative PCR was used to distinguish ongoing viral production from input virus. Other groups have used alternative strategies to investigate SARS-CoV replication, including using PCR capable of amplifying subgenomic RNA molecules produced during replication (15). Our results show that, in laboratories where reverse-transcription PCR is not available but appropriate reagents are available, immunofluorescence testing is a simple and rapid method of assessing whether cells exposed to respiratory or enteric specimens are infected with the virus.",
            "cite_spans": [
                {
                    "start": 393,
                    "end": 395,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "On the basis of this study and earlier reports (10,14,15), monkey kidney cell lines are particularly susceptible to SARS-CoV infection. African green, cynomolgus, and rhesus monkey kidney cell lines have all been previously shown to be susceptible. We identified for the first time that kidney cells derived from a fourth nonhuman primate species, buffalo green monkey, are productively infected with SARS-CoV, with titers that reach 4.7 log10/mL above input virus, similar to levels in other monkey kidney cells. We found most monkey kidney\u2013derived cell lines, including BGM, CV-1, FRhK, LLC-Mk2, MA-104, pCMK, and Vero E6, supported replication of SARS-CoV, with titers 3.9\u20134.7 log10/mL above input virus titers. High titers of SARS-CoV attainable in these cell lines should be considered when using them for virus isolation purposes, and appropriate safety guidelines should be followed.",
            "cite_spans": [
                {
                    "start": 48,
                    "end": 50,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 51,
                    "end": 53,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 54,
                    "end": 56,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "The ability of SARS-CoV to replicate efficiently in kidney-derived cell lines is not surprising given that its functional receptor, the metalloprotease angiotensin-converting enzyme 2 (ACE-2), is highly expressed in kidney tissue (16). This metalloprotease receptor is widely divergent from the aminopeptidase N receptor of group 1 CoVs (16) but is expressed in lung, heart, kidney, and gastrointestinal tissue, consistent with the pathology of SARS.",
            "cite_spans": [
                {
                    "start": 231,
                    "end": 233,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 338,
                    "end": 340,
                    "mention": "16",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "Generally, close agreement was seen between our results and those previously reported (15), although a difference was seen in CPE. We showed that HEK-293, Huh-7, and pCMK cells supported development of SARS-CoV\u2013specific CPE, whereas no CPE was observed in these cell lines in an earlier study, although replication occurred (15). In that study, cells were observed for CPE for only 2 days after infection, whereas in the present study we observed cells for up to 11 days. In Huh-7 and pCMK cell lines, we observed that CPE often developed slowly and affected a population of cells but did not progress (Figure 1). Neither COS nor MEK cells developed SARS-CoV\u2013specific CPE (Figure 1), despite evidence of replication by PCR and immunofluorescence. COS cells are a derivative of the African green monkey kidney fibroblast cell line CV-1, which is highly susceptible to SARS-CoV. The reason for the decreased level of virus production in related COS cells remains to be determined but may be due to a lower level surface expression of the ACE-2 receptor. Nevertheless, the results for these 2 cell lines highlight the unreliability of CPE as a measure of SARS-CoV replication.",
            "cite_spans": [
                {
                    "start": 87,
                    "end": 89,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 325,
                    "end": 327,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": [
                {
                    "start": 603,
                    "end": 611,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 673,
                    "end": 681,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "Given that primate kidney\u2013derived cell lines are particularly susceptible to infection with SARS-CoV and virus has been isolated from the kidney of an infected human patient (10), we suspect that human kidney\u2013derived cell lines might also support SARS-CoV replication. However, until the study by Gillim-Ross et al. (15), no human cell lines had been shown to be productively infected by SARS-CoV. We found, in agreement with that study that, HEK-293 and Huh-7 cells were susceptible to infection with the virus. In addition, we identified a third human cell line, Hep G2, derived from a hepatocellular carcinoma, that was also susceptible to infection, although it produced lower levels of virus-specific RNA than HEK-293 and Huh-7 cells. Hep G2 and Huh-7 cell lines are used in research laboratories to study hepatitis B and C viruses, which suggests that cell lines used for research purposes need to be considered carefully for their potential to support SARS-CoV replication, and guidelines must be established to prevent simultaneous work on multiple different viruses within the same laboratory.",
            "cite_spans": [
                {
                    "start": 175,
                    "end": 177,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 317,
                    "end": 319,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "Discussion",
            "ref_spans": []
        },
        {
            "text": "This study has shown that SARS-CoV can be isolated in several cell lines commonly used for diagnostic and research purposes and highlights that the virus can achieve high titers in some cell lines, sometimes in the absence of CPE. These findings are particularly relevant to laboratory scientists undertaking virus-isolation procedures on specimens collected from patients with atypical respiratory disease or in research laboratories where the possibility of simultaneously handling more than 1 virus exists.",
            "cite_spans": [],
            "section": "Discussion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Cytopathology of uninfected cells (left column) and the same cells infected in vitro with severe acute respiratory syndrome\u2013associated coronavirus (right column). A) Vero cells day 4 after infection. B) MA-104 cells day 4 after infection. C) Huh-7 cells day 11 after infection. D) pCMK cells day 11 after infection. E) COS cells day 11 after infection. F) MEK cells day 11 after infection.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Cycle threshold (Ct) changes measured by real-time polymerase chain reaction versus days after infection of the indicated cell lines. The cell-free sample had an initial Ct of 31, which rose to 40 by day 11. Reductions in the Ct or flat-line Ct values (e.g., COS cells) indicate replication of the virus. Continued increases in Ct above the initial value of 31 by days 7 and 11 indicate failure to replicate.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Epidemiology and cause of severe acute respiratory syndrome (SARS) in Guangdong, People's Republic of China, in February, 2003.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "362",
            "issn": "",
            "pages": "1353-8",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)14630-2"
                ]
            }
        },
        "BIBREF1": {
            "title": "A novel coronavirus associated with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1953-66",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030781"
                ]
            }
        },
        "BIBREF2": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "",
            "authors": [],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "Coronavirus as a possible cause of severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1319-25",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(03)13077-2"
                ]
            }
        },
        "BIBREF6": {
            "title": "Discovery of novel human and animal cells infected by the severe acute respiratory syndrome coronavirus by replication-specific multiplex reverse transcription-PCR.",
            "authors": [],
            "year": 2004,
            "venue": "J Clin Microbiol",
            "volume": "42",
            "issn": "",
            "pages": "3196-206",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.42.7.3196-3206.2004"
                ]
            }
        },
        "BIBREF7": {
            "title": "Angiotensin-converting enzyme 2 is a functional receptor for the SARS coronavirus.",
            "authors": [],
            "year": 2003,
            "venue": "Nature",
            "volume": "426",
            "issn": "",
            "pages": "450-4",
            "other_ids": {
                "DOI": [
                    "10.1038/nature02145"
                ]
            }
        },
        "BIBREF8": {
            "title": "Identification of a novel coronavirus in patients with severe acute respiratory syndrome.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1967-76",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030747"
                ]
            }
        },
        "BIBREF9": {
            "title": "Isolation and characterization of viruses related to the SARS coronavirus from animals in southern China.",
            "authors": [],
            "year": 2003,
            "venue": "Science",
            "volume": "302",
            "issn": "",
            "pages": "276-8",
            "other_ids": {
                "DOI": [
                    "10.1126/science.1087139"
                ]
            }
        },
        "BIBREF10": {
            "title": "Virus detectives seek source of SARS in China's wild animals.",
            "authors": [],
            "year": 2003,
            "venue": "Nature",
            "volume": "423",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1038/423467a"
                ]
            }
        },
        "BIBREF11": {
            "title": "",
            "authors": [],
            "year": 2001,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF12": {
            "title": "Identification of a new human coronavirus.",
            "authors": [],
            "year": 2004,
            "venue": "Nat Med",
            "volume": "10",
            "issn": "",
            "pages": "368-73",
            "other_ids": {
                "DOI": [
                    "10.1038/nm1024"
                ]
            }
        },
        "BIBREF13": {
            "title": "Characterization and complete genome sequence, coronavirus HKU1, from patients with pneumonia.",
            "authors": [],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "884-95",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.79.2.884-895.2005"
                ]
            }
        },
        "BIBREF14": {
            "title": "Spectrum of clinical illness in hospitalized patients with \"common cold\" virus infections.",
            "authors": [],
            "year": 2000,
            "venue": "Clin Infect Dis",
            "volume": "31",
            "issn": "",
            "pages": "96-100",
            "other_ids": {
                "DOI": [
                    "10.1086/313937"
                ]
            }
        },
        "BIBREF15": {
            "title": "A cluster of cases of severe acute respiratory syndrome in Hong Kong.",
            "authors": [],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1977-85",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa030666"
                ]
            }
        }
    }
}