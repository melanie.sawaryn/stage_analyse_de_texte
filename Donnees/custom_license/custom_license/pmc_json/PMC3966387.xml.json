{
    "paper_id": "PMC3966387",
    "metadata": {
        "title": "Pathology of US Porcine Epidemic Diarrhea Virus Strain PC21A in Gnotobiotic Pigs",
        "authors": [
            {
                "first": "Kwonil",
                "middle": [],
                "last": "Jung",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Qiuhong",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Kelly",
                "middle": [
                    "A."
                ],
                "last": "Scheuer",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Zhongyan",
                "middle": [],
                "last": "Lu",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Yan",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Linda",
                "middle": [
                    "J."
                ],
                "last": "Saif",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "In June 2013, intestinal contents were obtained from a 1-day-old pig with diarrhea on a farm in Ohio, USA. PEDV strain PC21A was detected in the sample by reverse transcription PCR (RT-PCR) selective for the nucleocapsid gene (229\u2013557 nt). The partial nucleocapsid gene sequence of PC21A was identical to that of 2 US PEDV outbreak strains from Colorado, USA: USA/Colorado/2013 (GenBank accession no. KF272920) and 13-019349 (GenBank accession no. KF267450). Only coronavirus-like particles were observed in the fecal sample by electron microscopy (Figure 1). The sample was negative for rotavirus groups A and C and for transmissible gastroenteritis virus/porcine respiratory coronavirus by RT-PCR (7,8). ",
            "cite_spans": [
                {
                    "start": 700,
                    "end": 701,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 702,
                    "end": 703,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 549,
                    "end": 557,
                    "mention": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ]
        },
        {
            "text": "The sample was bacteriologically sterilized by using 0.22-\u03bcm syringe filters and then prepared as inoculum. Near-term gnotobiotic pigs were delivered aseptically by hysterectomy from a specific pathogen\u2013free sow (9). Six 10- to 35-day-old pigs were randomly assigned to a PEDV-infected group (pigs 1\u20135) or a negative control group (pig 6). Information about inoculation and inocula pig-passage number is described in Table 1. Pigs 1\u20133 and 5 were inoculated orally and/or intranasally with 6.3\u20139.0 log10 genomic equivalents (GE) of PEDV strain PC21A; pig 4 was exposed to the virus by indirect contact with inoculated pig 3. For each sample, the quantity of PEDV RNA GE was \u2248106 times higher than plaque assay results for a cell-adapted PEDV strain, PC22A. Clinical signs were monitored hourly. Pig 4 was monitored for longer-term clinical signs and virus shedding. Pigs were euthanized for pathologic examination at 3 stages of infection: acute, mid, and later stages (<24 h, 24\u201348 h, and >48 h, respectively, after onset of clinical signs). The Ohio State University Institutional Animal Care and Use Committee approved all animal-related experimental protocols.",
            "cite_spans": [
                {
                    "start": 213,
                    "end": 214,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 417,
                    "end": 424,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Fecal or rectal swab samples were prepared as described (9). Virus RNA was extracted by using the MagMAX Viral RNA Isolation Kit (Applied Biosystems, Foster City, CA, USA) according to the manufacturer\u2019s instructions. Titers of virus shed in feces were determined by TaqMan real-time RT-PCR using the OneStep RT-PCR Kit (QIAGEN, Valencia, CA, USA) as reported (10), with modifications in the forward primer and probe to provide a 100% match to the US strains: forward 5\u2032-CGCAAAGACTGAACCCACTAAC-3\u2032 and probe FAM-TGYYACCAYYACCACGACTCCTGC-BHQ. A standard curve was generated by using the PCR amplicon (PEDN 229/557) of strain PC21A. The detection limit was 10 GE per reaction, corresponding to 4.8 log10 and 3.8 log10 GE/mL of fecal and serum samples, respectively.",
            "cite_spans": [
                {
                    "start": 57,
                    "end": 58,
                    "mention": "9",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 361,
                    "end": 363,
                    "mention": "10",
                    "ref_id": "BIBREF1"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Small and large intestine tissues, lung, liver, heart, kidney, spleen, and mesenteric lymph node were examined grossly and histologically. Mean jejunal VH:CD was measured by using PAX-it software (PAXcam, Villa Park, IL, USA) as described (11). The frozen tissues were prepared and tested by immunofluorescence staining, as described (12), for the detection of PEDV antigen, using monoclonal antibody 6C8-1 against the spike protein of PEDV strain DR13 (provided by Daesub Song, Korea Research Institute of Bioscience and Biotechnology, Daejeon, Korea).",
            "cite_spans": [
                {
                    "start": 240,
                    "end": 242,
                    "mention": "11",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 335,
                    "end": 337,
                    "mention": "12",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "The Study",
            "ref_spans": []
        },
        {
            "text": "Acute, severe watery diarrhea and vomiting developed in all inoculated pigs. Clinical signs developed 24\u201348 h after inoculation, regardless of the inoculum dose or number of inoculum pig passages (Table 1). Pig 4, which was followed longer, also exhibited dehydration, loss of bodyweight, and lethargy, but it consumed most of the milk that was offered. However, \u2248120 h after onset of clinical signs, pig 4 collapsed after showing signs of disorientation and emaciation. ",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 197,
                    "end": 204,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Immune electron microscopy, using a gnotobiotic pig hyperimmune serum to PEDV, showed only PEDV particles in the intestinal contents. For the pig-passaged PC21A strain, RT-PCR/PCR results were negative for transmissible gastroenteritis virus/porcine respiratory coronavirus (7), rotavirus groups A\u2013C (8), caliciviruses (13,14), astroviruses (15), circoviruses, enterovirus, kobuvirus, and bocavirus. For pigs 1 and 2, the detection of fecal virus shedding 24\u201348 h after inoculation coincided with the onset of clinical signs; for pigs 3 and 4, fecal shedding occurred before the onset of clinical signs (Table 1).",
            "cite_spans": [
                {
                    "start": 275,
                    "end": 276,
                    "mention": "7",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 301,
                    "end": 302,
                    "mention": "8",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 320,
                    "end": 322,
                    "mention": "13",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 323,
                    "end": 325,
                    "mention": "14",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 342,
                    "end": 344,
                    "mention": "15",
                    "ref_id": "BIBREF6"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 604,
                    "end": 611,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "By macroscopic examination, all infected pigs exhibited typical PEDV-like lesions, characterized by thin and transparent intestinal walls (duodenum to colon) and accumulation of large amounts of yellowish fluid in the intestinal lumen (Figure 2, panel A). The stomach was filled with curdled milk, possibly due to reduced intestinal peristalsis. The other internal organs appeared normal. Histologic lesions included acute diffuse, severe atrophic jejunitis (Figure 2, panel B) and mild vacuolation of superficial epithelial cells and subepithelial edema in cecum and colon (Figure 2, panel C). These findings were similar to those in conventional pigs naturally infected with Asian or US strains of PEDV and in caesarean-derived, colostrum-deprived pigs experimentally infected with CV777 (2,3,5,6). The mean jejunal VH:CD of the 5 infected pigs ranged from 1.2 to 3.4, probably depending on the stage of infection (Table 1), and that of the negative control pig was 6.3 (\u00b10.2). VH:CD for pig 4, which was euthanized at a later stage of infection, was 1.5 (\u00b10.2), a ratio indicative of continued cellular necrosis. Neither clinical signs nor lesions developed in the negative control pig during the experiment.",
            "cite_spans": [
                {
                    "start": 791,
                    "end": 792,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 793,
                    "end": 794,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 795,
                    "end": 796,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 797,
                    "end": 798,
                    "mention": "6",
                    "ref_id": "BIBREF11"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 236,
                    "end": 244,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 459,
                    "end": 467,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 575,
                    "end": 583,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 917,
                    "end": 924,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "Immunofluorescence-stained cells were observed mainly in the epithelium of atrophied villi of small (duodenum to ileum) and large intestines (Table 2; Figure 2, panels D\u2013F), as reported in other studies (2,3,5). The immunofluorescence was confined to the villous epithelial cells (Figure 2, panels D\u2013F). A few immunofluorescence-stained cells were detected infrequently in the Peyer patches of pig 4. Lung tissues of the infected pigs did not show immunofluorescence staining, indicating that PEDV does not infect lung tissues under the conditions tested. Although PC21A strain replicated in cecum and colon epithelial cells, cellular necrosis and villous atrophy were not evident. Whether PEDV infection of the large intestine contributes to the severity of PED is unclear.",
            "cite_spans": [
                {
                    "start": 204,
                    "end": 205,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 206,
                    "end": 207,
                    "mention": "3",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 208,
                    "end": 209,
                    "mention": "5",
                    "ref_id": "BIBREF10"
                }
            ],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 151,
                    "end": 159,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 281,
                    "end": 289,
                    "mention": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 142,
                    "end": 149,
                    "mention": "Table 2",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "All infected pigs tested at acute or later stages of infection had viral RNA titers of 4.8\u20137.6 log10 GE/mL in serum samples (Table 1). These titers were similar to those for field samples tested by real-time RT-PCR; 11 (55%) of 20 acute-phase serum samples collected from 13- to 20-week-old pigs with diarrhea from Ohio had viral RNA titers of 4.0\u20136.3 GE/mL. The early, severe diarrhea and vomiting and the PEDV fecal shedding at high titers may be accompanied by viremia. No infected pigs had detectable viral RNA in serum samples obtained before inoculation, and no negative control pig had detectable viral RNA during the experiment. ",
            "cite_spans": [],
            "section": "The Study",
            "ref_spans": [
                {
                    "start": 125,
                    "end": 132,
                    "mention": "Table 1",
                    "ref_id": null
                }
            ]
        },
        {
            "text": "In 2013, the first US outbreaks of the rapidly spreading porcine virus, PEDV, caused a high number of pig deaths and substantial economic losses (1,2); however, little was known about progression of the disease. Our data confirm that US PEDV PC21A is highly enteropathogenic and acutely infects the entire intestine, but the jejunum and ileum are the primary sites of infection. PC21A infection causes severe atrophic enteritis accompanied by viremia that leads to severe diarrhea and vomiting.",
            "cite_spans": [
                {
                    "start": 146,
                    "end": 147,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 148,
                    "end": 149,
                    "mention": "2",
                    "ref_id": "BIBREF7"
                }
            ],
            "section": "Conclusion",
            "ref_spans": []
        }
    ],
    "ref_entries": {
        "FIGREF0": {
            "text": "Figure 1: Electron micrograph of a US porcine epidemic diarrhea virus (PEDV) particle detected in a field fecal sample collected during a 2013 outbreak of PED on a farm in Ohio, USA; the fecal sample from which PEDV strain PC21A in this study was obtained was from a pig on the same farm during the same outbreak. The sample was negatively stained with 3% phosphotungstic acid. Scale bar = 50 nm.",
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Figure 2: Changes seen, by macroscopic examination, histologic examination, or immunofluorescence staining in the intestine of gnotobiotic pigs inoculated with porcine epidemic diarrhea virus (PEDV; US strain PC21A). A) Intestine of pig 1 at postinoculation hour (PIH) 30 (4\u20135 h after onset of clinical signs), showing thin and transparent intestinal walls (duodenum to colon) and extended stomach filled with curdled milk. B) Hematoxylin and eosin (H&E)\u2013stained jejunum of pig 3 at PIH 46 (at onset of clinical signs), showing acute diffuse, severe atrophic jejunitis. Original magnification \u00d7200. C) H&E-stained cecum of noninoculated pig 4 (which was exposed to inoculated pig 3 at PIH 0) at 120 h after onset of clinical signs. Acute diffuse, mild vacuolation of superficial epithelial cells (arrows) and subepithelial edema are seen. Original magnification \u00d7200. D) Immunofluorescence staining of jejunum of pig 5 at PIH 67 (37\u201341 h after onset of clinical signs), indicating that the epithelial cells lining atrophied villi are positive for PEDV. Original magnification \u00d7200. E) Immunofluorescence staining of jejunum of pig 3 at PIH 46 (at onset of clinical signs), showing localization of PEDV antigens in the cytoplasm of enterocytes. Original magnification \u00d7600. F) Immunofluorescence staining of colon of pig 2 at PIH 72 (26\u201328 h after onset of clinical signs), showing large numbers of PEDV-positive cells. Original magnification \u00d7200. CCL, crypt cell layer. Nuclei were stained with blue-fluorescent 4\u2032, 6-diamidino-2-phenylindole, dihydrochloride.",
            "type": "figure"
        }
    },
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Fighting a deadly pig disease: industry, veterinarians trying to contain PED virus, new to the US.",
            "authors": [],
            "year": 2013,
            "venue": "J Am Vet Med Assoc",
            "volume": "243",
            "issn": "",
            "pages": "469-70",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "Multiplex real-time RT-PCR for the simultaneous detection and quantification of transmissible gastroenteritis virus and porcine epidemic diarrhea virus.",
            "authors": [],
            "year": 2007,
            "venue": "J Virol Methods",
            "volume": "146",
            "issn": "",
            "pages": "172-7",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jviromet.2007.06.021"
                ]
            }
        },
        "BIBREF2": {
            "title": "The effects of transplacental porcine circovirus type 2 infection on porcine epidemic diarrhoea virus-induced enteritis in preweaning piglets.",
            "authors": [],
            "year": 2006,
            "venue": "Vet J",
            "volume": "171",
            "issn": "",
            "pages": "445-50",
            "other_ids": {
                "DOI": [
                    "10.1016/j.tvjl.2005.02.016"
                ]
            }
        },
        "BIBREF3": {
            "title": "Porcine reproductive and respiratory syndrome virus modifies innate immunity and alters disease outcome in pigs subsequently infected with porcine respiratory coronavirus: implications for respiratory viral co-infections.",
            "authors": [],
            "year": 2009,
            "venue": "J Gen Virol",
            "volume": "90",
            "issn": "",
            "pages": "2713-23",
            "other_ids": {
                "DOI": [
                    "10.1099/vir.0.014001-0"
                ]
            }
        },
        "BIBREF4": {
            "title": "Prevalence and molecular characterization of porcine enteric caliciviruses and first detection of porcine kobuviruses in US swine.",
            "authors": [],
            "year": 2013,
            "venue": "Arch Virol",
            "volume": "158",
            "issn": "",
            "pages": "1583-8",
            "other_ids": {
                "DOI": [
                    "10.1007/s00705-013-1619-5"
                ]
            }
        },
        "BIBREF5": {
            "title": "Characterization and prevalence of a new porcine calicivirus in swine, United States.",
            "authors": [],
            "year": 2011,
            "venue": "Emerg Infect Dis",
            "volume": "17",
            "issn": "",
            "pages": "1103-6",
            "other_ids": {
                "DOI": [
                    "10.3201/eid/1706.101756"
                ]
            }
        },
        "BIBREF6": {
            "title": "Novel astroviruses in insectivorous bats.",
            "authors": [],
            "year": 2008,
            "venue": "J Virol",
            "volume": "82",
            "issn": "",
            "pages": "9107-14",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.00857-08"
                ]
            }
        },
        "BIBREF7": {
            "title": "Emergence of Porcine epidemic diarrhea virus in the United States: clinical signs, lesions, and viral genomic sequences.",
            "authors": [],
            "year": 2013,
            "venue": "J Vet Diagn Invest",
            "volume": "25",
            "issn": "",
            "pages": "649-54",
            "other_ids": {
                "DOI": [
                    "10.1177/1040638713501675"
                ]
            }
        },
        "BIBREF8": {
            "title": "The pathogenesis of an enteric infection in pigs, experimentally induced by the coronavirus-like agent, Cv-777.",
            "authors": [],
            "year": 1981,
            "venue": "Vet Microbiol",
            "volume": "6",
            "issn": "",
            "pages": "157-65",
            "other_ids": {
                "DOI": [
                    "10.1016/0378-1135(81)90007-9"
                ]
            }
        },
        "BIBREF9": {
            "title": "Pathology of experimental CV777 coronavirus enteritis in piglets. I. Histological and histochemical study.",
            "authors": [],
            "year": 1982,
            "venue": "Vet Pathol",
            "volume": "19",
            "issn": "",
            "pages": "46-56",
            "other_ids": {
                "DOI": [
                    "10.1177/030098588201900108"
                ]
            }
        },
        "BIBREF10": {
            "title": "An immunohistochemical investigation of porcine epidemic diarrhoea.",
            "authors": [],
            "year": 1995,
            "venue": "J Comp Pathol",
            "volume": "113",
            "issn": "",
            "pages": "59-67",
            "other_ids": {
                "DOI": [
                    "10.1016/S0021-9975(05)80069-6"
                ]
            }
        },
        "BIBREF11": {
            "title": "In situ hybridization for the detection and localization of porcine epidemic diarrhea virus in the intestinal tissues from naturally infected piglets.",
            "authors": [],
            "year": 2000,
            "venue": "Vet Pathol",
            "volume": "37",
            "issn": "",
            "pages": "62-7",
            "other_ids": {
                "DOI": [
                    "10.1354/vp.37-1-62"
                ]
            }
        },
        "BIBREF12": {
            "title": "Development of a reverse transcription\u2013nested polymerase chain reaction assay for differential diagnosis of transmissible gastroenteritis virus and porcine respiratory coronavirus from feces and nasal swabs of infected pigs.",
            "authors": [],
            "year": 2000,
            "venue": "J Vet Diagn Invest",
            "volume": "12",
            "issn": "",
            "pages": "385-8",
            "other_ids": {
                "DOI": [
                    "10.1177/104063870001200418"
                ]
            }
        },
        "BIBREF13": {
            "title": "Detection and genetic diversity of porcine group A rotaviruses in historic (2004) and recent (2011 and 2012) swine fecal samples in Ohio: predominance of the G9P[13] genotype in nursing piglets.",
            "authors": [],
            "year": 2013,
            "venue": "J Clin Microbiol",
            "volume": "51",
            "issn": "",
            "pages": "1142-51",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.03193-12"
                ]
            }
        },
        "BIBREF14": {
            "title": "The effects of simvastatin or interferon-alpha on infectivity of human norovirus using a gnotobiotic pig model for the study of antivirals.",
            "authors": [],
            "year": 2012,
            "venue": "PLoS ONE",
            "volume": "7",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": [
                    "10.1371/journal.pone.0041619"
                ]
            }
        }
    }
}