{
    "paper_id": "PMC7110126",
    "metadata": {
        "title": "Reply to van der Hoek and Berkhout, Ebihara et al., and Belay et al",
        "authors": [
            {
                "first": "Frank",
                "middle": [],
                "last": "Esper",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Eugene",
                "middle": [
                    "D."
                ],
                "last": "Shapiro",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Marie",
                "middle": [
                    "L."
                ],
                "last": "Landry",
                "suffix": "",
                "email": null,
                "affiliation": {}
            },
            {
                "first": "Jeffrey",
                "middle": [
                    "S."
                ],
                "last": "Kahn",
                "suffix": "",
                "email": null,
                "affiliation": {}
            }
        ]
    },
    "body_text": [
        {
            "text": "\n To the Editor \u2014The discovery of the human coronavirus (HCoV) we designated the \u201cNew Haven coronavirus\u201d (HCoV-NH) was the result of a deliberate search for previously unknown HCoVs and was made without a priori knowledge of HCoV-NL63 [1] and HCoV-NL [2], the 2 HCoVs from The Netherlands that were recently described, and before the report of the discoveries of these viruses. Unlike HCoV-NL63 and HCoV-NL, which were initially identified in cell cultures, HCOV-NH was discovered by screening respiratory tract specimens with molecular probes. These probes targeted the sequences of the replicase 1a gene that are conserved among human, avian, and mammalian CoVs. Therefore, it is not surprising\u2014perhaps it is predictable\u2014that this region of the replicase 1a gene is highly conserved between HCoV-NH and HCoV-NL63. Until full genomic sequencing of HCoV-NH is complete, we believe that it is premature to state that HCoV-NH, HCoV-NL63, and HCoV-NL are the same virus. Nonetheless, as we clearly acknowledge in our study [3], it is likely that these viruses are closely related",
            "cite_spans": [
                {
                    "start": 236,
                    "end": 237,
                    "mention": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 252,
                    "end": 253,
                    "mention": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1021,
                    "end": 1022,
                    "mention": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "van der Hoek and Berkhout raise the issue of nomenclature [4]. Whether HCoV-NH and HCoV-NL63 are strains of the same species of virus remains to be determined. Sequence comparison between viruses is only one of many criteria used to differentiate between viral species [5]. In fact, the International Committee on Taxonomy of Viruses has established no specific guidelines to address the issues of nomenclature and categorization of potentially closely related viruses that have been identified independently [5, 6]. Undoubtedly, this will continue to be an issue as new viral pathogens are identified",
            "cite_spans": [
                {
                    "start": 59,
                    "end": 60,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 270,
                    "end": 271,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 510,
                    "end": 511,
                    "mention": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 513,
                    "end": 514,
                    "mention": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We disagree with van der Hoek and Berkhout that the naming of HCoV-NH \u201cneedlessly complicates the HCoV literature\u201d [4]. Rather, we believe that the identification of HCoV-NL63 and HCoV-NL by cell culture and genome amplification techniques and the identification of HCoV-NH by use of molecular probes exemplify the recent advances in methods to identify previously unrecognized viruses. As the full genomes of these viruses are described, the clarification of the nomenclature will follow",
            "cite_spans": [
                {
                    "start": 116,
                    "end": 117,
                    "mention": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "section": "",
            "ref_spans": []
        },
        {
            "text": "We believe it is important to attempt to replicate in different populations our finding of the association between HCoV-NH infection and Kawasaki disease, and we appreciate the letters from Ebihara et al. [7] and Belay et al. [8]. We would be interested to know whether proper controls were included to demonstrate the integrity of the RNA that was used in the amplification assays by Ebihara et al. We agree with Belay et al. that the timing of sample collection and the type of sample screened (nasal vs. pharyngeal) may explain the discrepancy in results between their study and ours. Furthermore, more than one etiological agent may be linked to Kawasaki disease. Nonetheless, we are intrigued by the study by Graf that suggests the presence of a peptide, corresponding to the spike protein of HCoV-NL63, in tissue from individuals with Kawasaki disease [9]. These findings further support the association between HCoV-NH and Kawasaki disease",
            "cite_spans": [
                {
                    "start": 206,
                    "end": 207,
                    "mention": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 227,
                    "end": 228,
                    "mention": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 859,
                    "end": 860,
                    "mention": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "section": "",
            "ref_spans": []
        }
    ],
    "ref_entries": {},
    "back_matter": [],
    "bib_entries": {
        "BIBREF0": {
            "title": "Identification of a new human coronavirus",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "van der Hoek",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Pyrc",
                    "suffix": ""
                },
                {
                    "first": "MF",
                    "middle": [],
                    "last": "Jebbink",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat Med",
            "volume": "10",
            "issn": "",
            "pages": "368-73",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF1": {
            "title": "A previously undescribed coronavirus associated with respiratory disease in humans",
            "authors": [
                {
                    "first": "RA",
                    "middle": [],
                    "last": "Fouchier",
                    "suffix": ""
                },
                {
                    "first": "NG",
                    "middle": [],
                    "last": "Hartwig",
                    "suffix": ""
                },
                {
                    "first": "TM",
                    "middle": [],
                    "last": "Bestebroer",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Proc Natl Acad Sci USA",
            "volume": "101",
            "issn": "",
            "pages": "6212-6",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF2": {
            "title": "Evidence of a novel human coronavirus that is associated with respiratory tract disease in infants and young children",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Esper",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Weibel",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ferguson",
                    "suffix": ""
                },
                {
                    "first": "ML",
                    "middle": [],
                    "last": "Landry",
                    "suffix": ""
                },
                {
                    "first": "JS",
                    "middle": [],
                    "last": "Kahn",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "191",
            "issn": "",
            "pages": "492-8",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF3": {
            "title": "Questions concerning the New Haven coronavirus [letter]",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "van der Hoek",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Berkhout",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "192",
            "issn": "",
            "pages": "350-1",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF4": {
            "title": "Emerging issues in virus taxonomy",
            "authors": [
                {
                    "first": "MH",
                    "middle": [],
                    "last": "van Regenmortel",
                    "suffix": ""
                },
                {
                    "first": "BW",
                    "middle": [],
                    "last": "Mahy",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "8-13",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF5": {
            "title": "The 7th ICTV report",
            "authors": [
                {
                    "first": "CM",
                    "middle": [],
                    "last": "Fauquet",
                    "suffix": ""
                },
                {
                    "first": "MA",
                    "middle": [],
                    "last": "Mayo",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Arch Virol",
            "volume": "146",
            "issn": "",
            "pages": "189-94",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF6": {
            "title": "Lack of association between New Haven coronavirus and Kawasaki disease [letter]",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ebihara",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Endo",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Ma",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Ishiguro",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kikuta",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "192",
            "issn": "",
            "pages": "351-2",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF7": {
            "title": "Kawasaki disease and human coronavirus [letter]",
            "authors": [
                {
                    "first": "ED",
                    "middle": [],
                    "last": "Belay",
                    "suffix": ""
                },
                {
                    "first": "DD",
                    "middle": [],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "LJ",
                    "middle": [],
                    "last": "Anderson",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect Dis",
            "volume": "192",
            "issn": "",
            "pages": "352-3",
            "other_ids": {
                "DOI": []
            }
        },
        "BIBREF8": {
            "title": "Identification of peptide epitopes recognized by antibodies in untreated acute Kawasaki disease",
            "authors": [
                {
                    "first": "JD",
                    "middle": [],
                    "last": "Graf",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Program and abstracts of the Eighth International Kawasaki Disease Symposium (San Diego, California, 2005)",
            "volume": "",
            "issn": "",
            "pages": null,
            "other_ids": {
                "DOI": []
            }
        }
    }
}