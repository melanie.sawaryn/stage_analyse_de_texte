{
    "paper_id": "3127736e1681d5f93d9bcdacacca7718a09b3859",
    "metadata": {
        "title": "GLIA EXPRESSION OF MHC DURING CNS INFECTION BY NEUROTROPIC CORONAVIRUS",
        "authors": [
            {
                "first": "Karen",
                "middle": [
                    "E"
                ],
                "last": "Malone",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Chandran",
                "middle": [],
                "last": "Ramakrishna",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J.-M",
                "middle": [],
                "last": "Gonzalez",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Stephen",
                "middle": [
                    "A"
                ],
                "last": "Stohlman",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Cornelia",
                "middle": [
                    "C"
                ],
                "last": "Bergmann",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Interferons are responsible for signaling leading to the initiation of the anti-viral state. Stimulation through the interferon-gamma (IFN\u03b3) receptor primarily follows the JAK/STAT pathway, but interplay between cytokine signaling and type I interferons regulates a large number of genes involved in viral immunity. Of particular interest are genes involved in the processing and presentation of the major histocompatibility complex (MHC) Class I and II pathways. LMP2/7 proteasomal subunits replace normally expressed beta subunits. This assembled immunoproteosome preferentially cleaves peptides for Class I presentation. Also required are Transporters 1/2 (TAP1/2) that translocate peptides into the ER for assembly with Class I and \u00df2-microglobulin. IFN\u03b3 can also induce the primary transactivator controlling Class II gene expression, CIITA. In addition to direct antiviral effects, IFN\u03b3 is crucial for antigen presentation by glial cells and engagement of T-cell effector function.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "The JHMV variant v2.2-1 is a neurotropic MHV that infects microglia, astrocytes, and oligodendroglia. Despite elimination of detectable infectious particles, virus persists in the CNS in the form of viral RNA. Clinical symptoms progress from limited mobility to paralysis during a time frame of 6-10 days postinfection. Primary demyelination occurs most prominently 10-21 days postinfection, but continues throughout persistence. 1 CD8 T cells play a critical role in clearing JHMV from the CNS using perforin and IFN\u03b3-mediated mechanisms. Perforin mediated cytolytic activity by virus specific CD8 T cells is effective in clearing virus from astrocytes and microglia, but is insufficient for clearing virus from oligodendroglia. 2, 3 mice exhibit a more severe disease course and a strong association of viral antigen in oligodendroglia. 3 Infected SCID mice receiving CD8 T cells from immunized wt, perforin knock-out (PKO) or GKO mice support the strict reliance on IFN\u03b3 for viral clearance from oligodendroglia. 4 IFN\u03b3 also induces Class II and enhances Class I surface expression on microglia, coincident with maximal T-cell infiltration during infection.",
            "cite_spans": [
                {
                    "start": 730,
                    "end": 732,
                    "text": "2,",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 733,
                    "end": 734,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 839,
                    "end": 840,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1016,
                    "end": 1017,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "In the normal CNS glial cells express few, if any MHC molecules. Differential MHC regulation by glial cell populations was examined following infection of transgenic mice, in which oligodendroglia are marked by GFP expression. A subset of IFN\u03b3-inducible genes was analyzed at the transcriptional level to identify factors involved in signaling and MHC regulation. The results indicate that both Class I mRNA and mRNA for genes encoding antigen processing machinery (APM) increased shortly after infection in both cell types, prior to surface Class I expression. However, in contrast to microglia, oligodendroglia appear deficient in Class II presentation. Glial cells thus have similar capacity to regulate Class I APM as other somatic cells. However, delayed potential of oligodendroglia to present antigen compared with microglia supports oligodendroglia serve as a reservoir for viral persistence.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "Mice and cell preparation: Transgenic C57BL/6 mice expressing GFP via an oligodendroglia specific PLP promoter (PLP-GFP/B6) ages 6-7 weeks, were infected with 250 pfu JHMV (v2.2-1) intracranially. Brains from 4-6 mice at each time point were collected, subjected to trypsinization and CNS cells enriched by percoll gradients for FACS.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "METHODS"
        },
        {
            "text": "FACS: Cells were stained with anti-CD45, anti-Class I, and anti-Class II and appropriate isotype controls for analysis of surface expression. For sorting, cells were stained with anti-CD45-PE. Two populations were collected: CD45 lo GFPmicroglia and CD45 -GFP + oligodendroglia.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "METHODS"
        },
        {
            "text": "Real-time PCR analysis: Cells were lysed in Trizol and RNA extracted, followed by LMP7 (Psmb8), IRF-1, IRF-2, and GAPDH genes were used to analyze oligo-dT primed cDNA, whereas CIITA promoter specific primers were analyzed in random primed cDNA. SYBER Green PCR reactions were carried out in duplicate on an MJ DNA Engine Opticon and target specificity checked by melting curve. Gene expression was normalized to GAPDH using (2 -(\u2206Ct) )*1000.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "METHODS"
        },
        {
            "text": "Transgenic mice expressing GFP under control of the oligodendroglia-specific PLP promoter were infected to analyze genes associated with MHC presentation based on the easy distinction between CD45 lo microglia and GFP + CD45oligodendroglia by flow cytometry. Resident glial cells expressed few if any MHC molecules in the normal adult murine CNS (Table 1) . By day 5 p.i., expression of Class I was detected on the majority of microglia and a subset of oligodendroglia. The initial delay in oligodendroglia Class I upregulation was overcome by day 7 p.i., when Class I expression peaked in both populations, coincident with maximal T-cell infiltration.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 346,
                    "end": 355,
                    "text": "(Table 1)",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "RESULTS"
        },
        {
            "text": "DNAse I digestion. Reverse transcription used AMV RT with either oligo-dT or random primers. Real-time primers for murine (H-2 b haplotype) TAP1, TAP2, LMP2 (Psmb9), To examine how the delay in Class I surface expression on oligodendroglia is regulated at the mRNA level, glial cell subsets were purified by FACS during acute infection and analyzed for transcripts encoding MHC and genes involved in antigen processing. Real-time PCR analysis indicated that both Class I mRNA and mRNA for genes encoding APM increased shortly after infection, prior to surface Class I expression ( Table 2 ). While Class I and APM mRNAs increased vastly between days 5 and 7 p.i. in oligodendroglia, they remained constant in microglia during acute infection. Interferon regulatory factors 1 (IRF-1) and 2 (IRF-2) are transactivators controlling induction of Class I and APM. IRF-1 mRNA increased in both cell types, albeit more dramatically in oligodendroglia. By contrast, IRF-2 mRNA was constitutively expressed in both cell types and was transiently downregulated during acute infection. In summary, maximal Class I expression coincided with peak mRNA transcription of Class I and APM genes.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 581,
                    "end": 588,
                    "text": "Table 2",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "544"
        },
        {
            "text": "Analysis of Class II expression by flow cytometry demonstrated efficient upregulation on microglia (Table 1) . Only limited expression was detected on oligodendroglia consistent with their inefficient upregulation of CIITA (Table 2) . ",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 99,
                    "end": 108,
                    "text": "(Table 1)",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 223,
                    "end": 232,
                    "text": "(Table 2)",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "544"
        },
        {
            "text": "Analysis of genes necessary for antigen presentation in the CNS revealed several trends unique to distinct glial cell types. Both oligodendroglia and microglia upregulated major components for Class I antigen processing and presentation during infection, supporting the capacity for efficient presentation of virus peptide to CD8 T-cells. Although basal levels of Class I and APM mRNA were lower in oligodendroglia, they increased with similar kinetics in both cell types following infection. Higher relative Class I and APM mRNA levels in oligodendroglia, coincident with peak T-cell infiltration at day 7 p.i., suggests enhanced responsiveness to IFN\u03b3 compared with microglia. Delayed Class I surface expression on oligodendroglia, compared with microglia, appears to reflect levels of IFN inducible APM mRNA. The paucity in CIITA and Class II expression in oligodendroglia remains unknown. Lastly, upregulation of IRF-1 in both cell types during infection was supported by concordant downregulation of IRF-2, an IRF-1 antagonist. In summary, expression of Class I on oligodendroglia during infection, albeit under more stringent regulation compared with microglia, supports oligodendroglia as potential targets for CTL and subsequent pathology. However the CNS employs many protective measures against such activity, particularly to protect cells that proliferate slowly. 5 The demonstration that CTL activity does not control JHMV infection in oligodendroglia, in light of Class I expression, demands further investigation.",
            "cite_spans": [
                {
                    "start": 1375,
                    "end": 1376,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "DISCUSSION"
        },
        {
            "text": "This research was supported by the National Institutes of Health grant NS18146.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ACKNOWLEDGMENTS"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "MHV infection in the CNS: mechanisms of immune-mediated control",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "W"
                    ],
                    "last": "Marten",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Stohlman",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Bergmann",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Viral Immun",
            "volume": "14",
            "issn": "",
            "pages": "1--18",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Antibody prevents virus reactivation within the central nervous system",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "T"
                    ],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "R"
                    ],
                    "last": "Hinton",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "W"
                    ],
                    "last": "Marten",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Bergmann",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Stohlman",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J. Immun",
            "volume": "162",
            "issn": "",
            "pages": "7358--7368",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "IFN-gamma is required for viral clearance from central nervous system oligodendroglia",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Parra",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "R"
                    ],
                    "last": "Hinton",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "W"
                    ],
                    "last": "Marten",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Bergmann",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "T"
                    ],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "S"
                    ],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Stohlmann",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J. Immunol",
            "volume": "162",
            "issn": "",
            "pages": "1641--1647",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Perforin and gamma-interferon mediated control of coronavirus central nervous system infection by CD8 T cells in the absence of CD4 T cells",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Bergmann",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Parra",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "R"
                    ],
                    "last": "Hinton",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Ramakrishna",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Dowdell",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Stohlman",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J. Virol",
            "volume": "78",
            "issn": "",
            "pages": "1739--1750",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Immune responses to RNA-virus infections of the CNS",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "E"
                    ],
                    "last": "Griffin",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Nat. Rev. Immum",
            "volume": "3",
            "issn": "",
            "pages": "493--502",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "Percent microglia and oligodendroglia expressing Class I and Class II.",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Relative gene expression in microglia and oligodendroglia.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}