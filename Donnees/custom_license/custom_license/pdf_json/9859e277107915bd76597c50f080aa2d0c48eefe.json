{
    "paper_id": "9859e277107915bd76597c50f080aa2d0c48eefe",
    "metadata": {
        "title": "OBSERVATION: BRIEF RESEARCH REPORT SARS-CoV-2-Positive Sputum and Feces After Conversion of Pharyngeal Samples in Patients With COVID-19",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Background: The outbreak of coronavirus disease 2019 (COVID-19) has become a global public health problem. In the absence of a specific therapy or vaccine, timely diagnosis and the establishment of a sufficient isolation period for infected individuals are critical to containment efforts. Real-time quantitative fluorescence polymerase chain reaction (RT-qPCR) testing of respiratory specimens for SARS-CoV-2 RNA is currently used for case diagnosis and to guide the duration of patient isolation or hospital discharge (1) . Specimens that are positive on RT-qPCR have, however, also been reported from blood (2), feces (3), and urine (4) . Whether testing of multiple body sites is important when considering patient isolation has not been thoroughly studied.",
            "cite_spans": [
                {
                    "start": 520,
                    "end": 523,
                    "text": "(1)",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 636,
                    "end": 639,
                    "text": "(4)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Objective: To assess the results of RT-qPCR for SARS-CoV2 RNA of sputum and fecal samples from a group of patients after conversion of their pharyngeal samples from positive to negative.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Methods and Findings: We retrospectively identified a convenience sample of patients admitted to Beijing Ditan Hospital, Capital Medical University, with a diagnosis of COVID-19 and paired RT-qPCR testing of pharyngeal swabs with either sputum or feces samples. A diagnosis of COVID-19 required at least 2 RT-qPCR-positive pharyngeal swabs, and patients underwent treatments as well as initial and follow-up testing of pharyngeal, sputum, or fecal samples at the discretion of treating clinicians. Hospital discharge required meeting 4 criteria: afebrile for more than 3 days, resolution of respiratory symptoms, substantial improvement of chest computed tomographic findings, and 2 consecutive negative RT-qPCR tests for SARS-CoV2 in respiratory samples obtained at least 24 hours apart (1). We report the findings of patients with at least 1 initial or follow-up RT-qPCR positive sputum or fecal sample obtained within 24 hours of a follow-up negative RT-qPCR pharyngeal sample. The RT-qPCR assay targeted the open reading frame 1ab (ORF1ab) region and nucleoprotein (N) gene with a negative control. A cycle threshold value of 37 or less was interpreted as positive for SARS-CoV-2, according to Chinese national guidelines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Among 133 patients admitted with COVID-19 from 20 January to 27 February 2020, we identified 22 with an initial or follow-up positive sputum or fecal samples paired with a follow-up negative pharyngeal sample. Of these patients, 18 were aged 15 to 65 years, and 4 were children; 14 were male; and 11 had a history of either travel to or exposure to an individual returning from Hubei Province in the past month. Fever was the most common initial onset symptom. Five patients had at least 1 preexisting medical condition (Table) . All patients met criteria and were discharged from the hospital.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 520,
                    "end": 527,
                    "text": "(Table)",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "We collected 545 specimens from 22 patients, including 209 pharyngeal swabs, 262 sputum samples, and 74 feces samples (Figure) . In these patients, sputum and feces re- mained positive for SARS-CoV2 on RT-qPCR up to 39 and 13 days, respectively, after the obtained pharyngeal samples were negative. Discussion: Pharyngeal swabs are widely used to determine the appropriateness of a patient's discharge from the hospital and whether isolation continues to be required. We observed 22 patients who had positive RT-qPCR results for SARS-CoV-2 in the sputum or feces after pharyngeal swabs became negative. These finding raise concern about whether patients with negative pharyngeal swabs are truly virus-free, or sampling of additional body sites is needed. It is important to emphasize, however, that it is not known whether the positive RT-qPCR results for SARS-CoV2 observed here indicate that a patient continues to pose a risk for infection to others. Related, positive throat samples (after negative samples) after hospital discharge have been reported (5) . ",
            "cite_spans": [
                {
                    "start": 1056,
                    "end": 1059,
                    "text": "(5)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [
                {
                    "start": 118,
                    "end": 126,
                    "text": "(Figure)",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "Infection was confirmed by RT-qPCR assay of pharyngeal swabs, sputum samples, and feces samples. Day 0 is the day of symptom onset for each patient. Patient P2 had RT-qPCR-positive sputum samples after negative pharyngeal samples (although not paired within 24 hours); he was discharged from the hospital on the basis of sequential negative samples. N = negative; NA = not available; P = positive; RT-qPCR = real-time quantitative fluorescence polymerase chain reaction.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Patient"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Chinese Clinical Guidance For COVID-19 Pneumonia Diagnosis and Treatment",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "China. Lancet",
            "volume": "395",
            "issn": "",
            "pages": "497--506",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30183-5"
                ]
            }
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Detection of SARS-CoV-2 in different types of clinical specimens",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Gao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.3786"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "SARS-CoV-2 infection does not significantly cause acute renal injury: an analysis of 116 hospitalized patients with COVID-19 in a single hospital",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Positive RT-PCR test results in patients recovered from COVID-19",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Lan",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Ye",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.2783"
                ]
            }
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Results of nucleic acid testing in 22 patients with confirmed COVID-19 infection, by timing of symptom onset.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Characteristics of 22 Patients With Confirmed COVID-19 Who Had a Positive RT-qPCR Result for SARS-CoV-2 in Fecal and/or Sputum Samples After a Negative RT-qPCR Result on Pharyngeal Swab",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "The authors thank all health care workers involved in the diagnosis and treatment of patients in China. They also thank Professor Ang Li for guidance in study design and coordination. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgment:"
        },
        {
            "text": "Limitations of our study are that it is based on a convenience sample and that serial samples were not obtained from each patient on a defined schedule. These results warrant further study, including the systematic and simultaneous collection of samples from multiple body sites and evaluation of infectious risk. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "annex"
        }
    ]
}