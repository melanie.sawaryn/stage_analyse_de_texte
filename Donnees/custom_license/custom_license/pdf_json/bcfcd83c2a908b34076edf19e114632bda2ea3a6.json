{
    "paper_id": "bcfcd83c2a908b34076edf19e114632bda2ea3a6",
    "metadata": {
        "title": "Journal Pre-proof Echoes of 2009 Pandemic H1N1 Influenza with the COVID Pandemic Echoes of 2009 Pandemic H1N1 Influenza with the COVID Pandemic",
        "authors": [
            {
                "first": "Ravi",
                "middle": [],
                "last": "Jhaveri",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Division of Pediatric Infectious Diseases, Ann & Robert H. Lurie Children's Hospital of Chicago",
                    "institution": "Northwestern University Feinberg School of Medicine",
                    "location": {
                        "addrLine": "225 E. Chicago Ave, Box 20",
                        "postCode": "60611-2991",
                        "settlement": "Chicago, Chicago",
                        "region": "IL, IL"
                    }
                },
                "email": "ravi.jhaveri@northwestern.edu"
            }
        ]
    },
    "abstract": [
        {
            "text": "The SARS CoV-2 pandemic that has engulfed the globe has had incredible effects on health systems and economic activity. Social distancing and school closures have played a central role in public health efforts to counter the COVID-19 pandemic. The most recent global pandemic prior to COVID-19 was with 2009 pandemic H1N1 influenza. The course of events in 2009 offer some rich lessons that could be applied to the current COVID-19 pandemic. This commentary highlights some of the most relevant points and a discussion of possible outcomes with the COVID-19 pandemic.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "The global outbreak of the SARS CoV-2 coronavirus and associated COVID-19 illness has crippled major health systems and economies in a way that no one could have imagined(1). At the time of writing, waves of infected people are being identified across North America and Europe with concerns about explosive growth in developing countries like India(2, 3). Moving quickly from the containment phase, in which widespread testing and isolation of the infected was attempted, countries around the world implemented unprecedented school closures and social distancing(4). After being the epicenter of the outbreak, these measures appear to have helped China slow the spread of COVID-19 infections(5). There is evidence that social distancing and school and group facility closings seem to also be working in parts of the United States impacted earliest by COVID that responded with early implementation and enforcement(6). It is important to highlight the ripple effects that school closures have on a community: children at home means parents missing work; afterschool programs are closed; sports and recreational activities are canceled, teachers and educational staff are out of work; school-based lunch and food programs are impacted. It is also important to note that school Over the summer of 2009, H1N1 cases continued to be identified in the US but spread was more modest. The Southern Hemisphere did experience an influenza season within their traditional months that was dominated by 2009 pandemic H1N1 activity(13). In the US when schools re-opened in late August and early September, there was a surge of new infections (Figure 1b)(9) . As one can see, the timing of school re-opening and frequent close contact of children as vectors for influenza transmission, the 2 nd wave of cases peaked at a level far beyond the initial spring peak. Studies showed that school closures instituted in the fall had no impact on transmission (14) . New infections continued until November and early December before tapering off. The irony of the situation was the massive efforts were put forth to create a pandemic H1N1 vaccine, but the initial strains did not grow well and vaccine production was delayed by several weeks (15) . By the time any vaccine was available in November and early December, the pool of susceptible children had likely been depleted and peak activity had long since passed.",
            "cite_spans": [
                {
                    "start": 1936,
                    "end": 1940,
                    "text": "(14)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 2218,
                    "end": 2222,
                    "text": "(15)",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [
                {
                    "start": 1627,
                    "end": 1641,
                    "text": "(Figure 1b)(9)",
                    "ref_id": "FIGREF3"
                }
            ],
            "section": "Introduction"
        },
        {
            "text": "It is important to acknowledge major differences between the circumstances of 2009 pandemic influenza and the current COVID-19 pandemic. First, a global influenza pandemic was a known commodity, something the international community had experienced in the past and expected to return in the future. While the initial SARS CoV outbreak in 2003 temporarily grabbed the world's attention, it petered out in a few months never to return(16). Therefore, this SARS CoV-2 virus was not an outbreak the world anticipated, which has hampered the rapid response required for better containment. Second, because influenza is a global and consistent seasonal health problem, there is an established infrastructure for surveillance, prevention and treatment that can be applied to any new pandemic strain. Public health officials know they can collaborate with industry partners to create a vaccine within a few months, and indeed this did happen in 2009 albeit too late to impact the 2 nd wave of cases. There is no existing coronavirus vaccine or treatment infrastructure, so all candidates must start with phase 1 studies or the rapidly conducted but highly flawed open-label treatment studies that are being rapidly published and disseminated (17, 18) . Assuming no major adverse events, good efficacy and easy phase 1 and 2 testing, it will be at least a year before a vaccine candidate would be ready for use in the general population. Last, varying levels of influenza immunity exist in the community and help to limit the spread and severity of circulating strains. For example with the 2009 pandemic H1N1 strain, the elderly were relatively protected because they had archived immunity to H1N1 epitopes after being exposed to the similar 1918 strain earlier in life (19) . Aside from those already infected, for SARS-CoV2 the entire population is na\u00efve and at risk for infection. While it is possible there may be some degree of cross-protection with other coronaviruses, the rapid spread of SARS-CoV2 suggests otherwise.",
            "cite_spans": [
                {
                    "start": 1234,
                    "end": 1238,
                    "text": "(17,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1239,
                    "end": 1242,
                    "text": "18)",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1762,
                    "end": 1766,
                    "text": "(19)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Lessons from 2009 that are relevant to the COVID-19 public health response"
        },
        {
            "text": "With all that being said, there are still important lessons that can be drawn from the 2009 pandemic H1N1 experience. The first is the expected impact of social distancing and a potential 2 nd wave of cases. Schools did not close initially in 2009 due to summer being near at hand. The influenza surveillance data from CDC (Figure 1b) demonstrated that there was ongoing person to person transmission during the summer, which is typically a time when there is no detectable activity. Once widespread close contact at school was re-established, children with H1N1 infected other children who subsequently spread influenza to adult contacts and influenza cases surged. Once the fall peak of cases was underway, school closures implemented at that time had no impact on transmission or total flu cases (14) . There is data from the 1918 pandemic that US cities that instituted social distancing and school closures earlier and for longer period of time had fewer overall cases and better economic outcomes in the long term (20, 21) . It is very likely that once social distancing measures are lifted for COVID-19 and school or summer activities resume, there will be a 2 nd wave of cases. At the time of writing, this has already been seen in Hong Kong(5). Given how immune na\u00efve the population is to this virus and given how SARS CoV-2 has proven to be much more contagious than seasonal influenza, a 2 nd wave of cases is a virtual certainty (22, 23) . The goal of social distancing is to blunt the first wave and allow the medical system time to manage cases without being overwhelmed. Over time with widespread testing and prompt diagnosis, social distancing could transition to a more targeted cohorting of infected persons and \"cocooning\" isolation of highly susceptible people like the elderly and those with chronic respiratory and cardiovascular conditions. Sadly, it appears that the social distancing and school closures were implemented too late and in too regional a fashion to allow for optimal containment (24) .",
            "cite_spans": [
                {
                    "start": 799,
                    "end": 803,
                    "text": "(14)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1020,
                    "end": 1024,
                    "text": "(20,",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1025,
                    "end": 1028,
                    "text": "21)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1441,
                    "end": 1445,
                    "text": "(22,",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1446,
                    "end": 1449,
                    "text": "23)",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 2018,
                    "end": 2022,
                    "text": "(24)",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [
                {
                    "start": 323,
                    "end": 334,
                    "text": "(Figure 1b)",
                    "ref_id": "FIGREF3"
                }
            ],
            "section": "Lessons from 2009 that are relevant to the COVID-19 public health response"
        },
        {
            "text": "The 2009 B) The onset of the 2009 pandemic H1N1 strain came in March with an early peak coming in May. Activity declined over the summer with school adjournment, but never fully disappeared.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusion"
        },
        {
            "text": "In late August with school reopening, virus activity reached an even higher fall peak. After widespread infections, influenza activity dropped by negligible activity after December with few infections during the usual peak months of January and February. One will also notice the case ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "World Health Organization. Situation updates -Pandemic (H1N1)",
            "authors": [],
            "year": 2010,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The effect of reactive school closure on community influenza-like illness counts in the state of Michigan during the 2009 H1N1 pandemic",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "M"
                    ],
                    "last": "Davis",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Markel",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Navarro",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Wells",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "S"
                    ],
                    "last": "Monto",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "E"
                    ],
                    "last": "Aiello",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Clin Infect Dis",
            "volume": "60",
            "issn": "12",
            "pages": "90--97",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Learning from SARS: Preparing for the Next Disease Outbreak: Workshop Summary. The National Academies Collection: Reports funded by National Institutes of Health",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Mcneil",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "G"
                    ],
                    "last": "Knobler",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "Mahmoud",
                    "middle": [
                        "A"
                    ],
                    "last": "Lemon",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Mack",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Sivitz",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Oberholtzer",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "",
            "volume": "16",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "A Trial of Lopinavir-Ritonavir in Adults Hospitalized with Severe Covid-19",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Cao",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wen",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Hydroxychloroquine and azithromycin as a treatment of COVID-19: results of an open-label non-randomized clinical trial",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Gautret",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "Lagier",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Parola",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "T"
                    ],
                    "last": "Hoang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Meddeb",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Mailhe",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Int J Antimicrob Agents",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Structural basis of preexisting immunity to the 2009 H1N1 pandemic influenza virus",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "C"
                    ],
                    "last": "Ekiert",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "Krause",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Hai",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "E"
                    ],
                    "last": "Crowe",
                    "suffix": ""
                },
                {
                    "first": "Jr",
                    "middle": [],
                    "last": "Wilson",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [
                        "A"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Science",
            "volume": "328",
            "issn": "5976",
            "pages": "357--60",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Nonpharmaceutical interventions implemented by US cities during the 1918-1919 influenza pandemic",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Markel",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "B"
                    ],
                    "last": "Lipman",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Navarro",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Sloan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "R"
                    ],
                    "last": "Michalsen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "M"
                    ],
                    "last": "Stern",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "JAMA",
            "volume": "298",
            "issn": "6",
            "pages": "644--54",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Social distancing won't just save lives. It might be better for the economy in the long run",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Matthews",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "The reproductive number of COVID-19 is higher compared to SARS coronavirus",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "A"
                    ],
                    "last": "Gayle",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wilder-Smith",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rocklov",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Travel Med",
            "volume": "27",
            "issn": "2",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Estimates of the reproduction number for seasonal, pandemic, and zoonotic influenza: a systematic review of the literature",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Biggerstaff",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Cauchemez",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Reed",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Gambhir",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Finelli",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "BMC Infect Dis",
            "volume": "14",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Trump projects up to 240,000 coronavirus deaths in U.S., even with mitigation efforts. The Washington Post",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Rucker",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wan",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "closures without simultaneous social distancing means children gather in parks, libraries and other facilities and could still transmit to each other and fuels spread in the community. At the time of writing, much of the debate around social distancing has focused on how long these measures need to be implemented and what should be expected in terms of new cases once social distancing recommendations are lifted(4, 7). Many of these questions were posed back in 2009 when the pandemic novel H1N1 influenza strain emerged to cause similar global disruption, albeit with less severe clinical illness(8). The purpose of this commentary is to review the events of the 2009 pandemic H1N1 influenza emergence, examine how only limited school closures were implemented with minimal social distancing and offer a discussion about the likelihood of subsequent waves of COVID cases once these measures lapse. Emergence of 2009 pandemic H1N1 influenza Beyond its rapid global spread and disproportionate impact on children, young adults and pregnant women, the timing of the emergence of the 2009 pandemic H1N1 influenza virus created significant challenges to the response. During a typical influenza season (2015-2016 for example), activity starts in late November and December, peaks in January and February and tapers off by March and April (Figure 1a)(9). In sharp contrast, the onset of 2009 pandemic H1N1 came in late March with many respiratory illnesses being reported in Mexico and then spreading to the US and other parts of the world over the next several weeks(Figure 1b)(9-11).The onset of infections in early spring caused for several school-based outbreaks in New YorkCity and other areas(12). Individual schools did close, but early cases never matched the explosive spread of the current COVID-19 pandemic. Because of the focused geographic activity of 2009 H1N1 and because schools would be adjourning for summer break, national school closures were never seriously considered. While social distancing was discussed as part of a general public health strategy to manage a pandemic, no specific measures were put in place(8).The other issue with emerging in late March was this occurred almost 2 months after global decisions about strain selection for the 2009-2010 seasonal influenza vaccines were made. A pandemic vaccine would need to be prepared in parallel to the seasonal vaccine with a lag of about 2 months assuming no delays. No other community preventive measures were put in place beyond the accelerated pace of trying to produce a pandemic vaccine.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "pandemic H1N1 experience offers a detailed example of what is likely to happen if social distancing or school closures are lifted too soon in the current COVID-19 epidemic. These actions would likely invite a severe 2 nd wave of infections. The lessons are there for those that wish to heed them.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Legend",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Influenza circulation in a Typical Season and in 2009-2010.A) In a typical season (this example is 2015-2016), influenza starts to circulate in late November and December, reaches its peak activity in January and February and then tapers off by late April.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF4": {
            "text": "counts and percent positive tests in 2009-2010 were approximately 5 times that seen in a normal season. Source: Centers for Disease Control and Prevention. https://www.cdc.gov/flu/weekly/pastreports.htm",
            "latex": null,
            "type": "figure"
        },
        "FIGREF5": {
            "text": "Figure 1",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "My thanks to Andi Shane MD, MPH for comments and feedback on this commentary.Best wishes to all colleagues in healthcare around the country who are working incredibly hard under difficult circumstances on behalf of all their patients.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgements"
        }
    ]
}