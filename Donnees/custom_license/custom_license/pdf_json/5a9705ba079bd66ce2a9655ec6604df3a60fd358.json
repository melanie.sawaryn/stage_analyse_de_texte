{
    "paper_id": "5a9705ba079bd66ce2a9655ec6604df3a60fd358",
    "metadata": {
        "title": "Phage display for identifying peptides that bind the spike protein of transmissible gastroenteritis virus and possess diagnostic potential",
        "authors": [
            {
                "first": "Siqingaowa",
                "middle": [],
                "last": "Suo",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Xue",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Dante",
                "middle": [],
                "last": "Zarlenga",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "\u2022",
                "middle": [],
                "last": "Ri-E Bu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Yudong",
                "middle": [],
                "last": "Ren",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "\u2022",
                "middle": [
                    "Xiaofeng"
                ],
                "last": "Ren",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "The spike (S) protein of porcine transmissible gastroenteritis virus (TGEV) is located within the viral envelope and is the only structural protein that possesses epitopes capable of inducing virus-neutralizing antibodies. Among the four N-terminal antigenic sites A, B, C, and D, site A and to a lesser extent site D (S-AD) induce key neutralizing antibodies. Recently, we expressed S-AD (rS-AD) in recombinant form. In the current study, we used the rS-AD as an immobilized target to identify peptides from a phage-display library with application for diagnosis. Among the 9 phages selected that specifically bound to rS-AD, the phage bearing the peptide TLNMHLFPFHTG bound with the highest affinity and was subsequently used to develop a phage-based ELISA for TGEV. When compared with conventional antibody-based ELISA, phagemediated ELISA was more sensitive; however, it did not perform better than semi-quantitative RT-PCR, though phage-mediated ELISA was quicker and easier to set up.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Transmissible gastroenteritis virus (TGEV) is a member of the Coronaviridae family and is a major cause of enteric disease in pigs where it threatens swine production and triggers substantial economic losses in the industry [1] [2] [3] [4] . Its genome is composed of positive-stranded RNA approximately 28.5-kb in length. The virus consists of four structural proteins: envelope (E), membrane (M), spike (S), and nucleocapsid (N) proteins [1, 3, 5] . Non-structural proteins, which comprise two-thirds of the 5 0 -proximal end, are encoded by open reading frames 1a and 1ab as well as the replicase. In contrast, the 3 0 end of the genome encodes both non-structural and structural proteins (5 0 -S-3a-3b-E-M-N-7-3 0 ) [6] .",
            "cite_spans": [
                {
                    "start": 224,
                    "end": 227,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 228,
                    "end": 231,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 232,
                    "end": 235,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 236,
                    "end": 239,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 440,
                    "end": 443,
                    "text": "[1,",
                    "ref_id": null
                },
                {
                    "start": 444,
                    "end": 446,
                    "text": "3,",
                    "ref_id": null
                },
                {
                    "start": 447,
                    "end": 449,
                    "text": "5]",
                    "ref_id": null
                },
                {
                    "start": 720,
                    "end": 723,
                    "text": "[6]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The S protein, which induces neutralizing antibodies, is important in the initiation of infection [7] [8] [9] and has been further delineated into four antigenic sites A, B, C, and D which are located within the N-terminal region of the S protein [8] . Among these, only site A and to a lesser extent site D (herein defined as S-AD) are involved in eliciting neutralizing antibodies. Recent work demonstrated that recombinant S-AD (rS-AD) was able to induce antibodies capable of neutralizing TGEV infection in vitro [10] . Edited Attenuated or inactivated TGEV vaccines are less than optimal because they are capable of reverting back to virulent phenotypes and generally do not prevent viral shedding. Therefore, effective diagnostic tests have become important in virus management and control. Phage display is a proven technology for identifying small peptide ligands that can bind specific target proteins [11] [12] [13] [14] . It has been utilized in antibody engineering [15] , drug discovery [16] , vaccine development [17] , and molecular diagnosis. In virology, phage display has been used to identify peptides that interact with several viruses such as bovine rotavirus [18] , adenovirus type 2 [19], Andes virus [20] , Sin Nombre virus [21] , coronavirus [22] , and Avian H5N1 Virus [23] . Herein, we use similar technology and advance previous work by using the rS-AD as an immobilizing target to select phages from a peptide display library, with diagnostic potential for TGEV. Our results indicate that phages bearing peptide ligands that bind rS-AD can be used to develop a phage-mediated ELISA with high sensitivity and specificity to distinguish TGEV from other common swine viruses.",
            "cite_spans": [
                {
                    "start": 98,
                    "end": 101,
                    "text": "[7]",
                    "ref_id": null
                },
                {
                    "start": 102,
                    "end": 105,
                    "text": "[8]",
                    "ref_id": null
                },
                {
                    "start": 106,
                    "end": 109,
                    "text": "[9]",
                    "ref_id": null
                },
                {
                    "start": 247,
                    "end": 250,
                    "text": "[8]",
                    "ref_id": null
                },
                {
                    "start": 517,
                    "end": 521,
                    "text": "[10]",
                    "ref_id": null
                },
                {
                    "start": 524,
                    "end": 530,
                    "text": "Edited",
                    "ref_id": null
                },
                {
                    "start": 911,
                    "end": 915,
                    "text": "[11]",
                    "ref_id": null
                },
                {
                    "start": 916,
                    "end": 920,
                    "text": "[12]",
                    "ref_id": null
                },
                {
                    "start": 921,
                    "end": 925,
                    "text": "[13]",
                    "ref_id": null
                },
                {
                    "start": 926,
                    "end": 930,
                    "text": "[14]",
                    "ref_id": null
                },
                {
                    "start": 978,
                    "end": 982,
                    "text": "[15]",
                    "ref_id": null
                },
                {
                    "start": 1000,
                    "end": 1004,
                    "text": "[16]",
                    "ref_id": null
                },
                {
                    "start": 1027,
                    "end": 1031,
                    "text": "[17]",
                    "ref_id": null
                },
                {
                    "start": 1181,
                    "end": 1185,
                    "text": "[18]",
                    "ref_id": null
                },
                {
                    "start": 1224,
                    "end": 1228,
                    "text": "[20]",
                    "ref_id": null
                },
                {
                    "start": 1248,
                    "end": 1252,
                    "text": "[21]",
                    "ref_id": null
                },
                {
                    "start": 1267,
                    "end": 1271,
                    "text": "[22]",
                    "ref_id": null
                },
                {
                    "start": 1295,
                    "end": 1299,
                    "text": "[23]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Biopanning Swine testis (ST) cells were purchased from ATCC and used to propagate TGEV strain PUR46-MAD [4] . The rS-AD was produced and purified as described elsewhere [10] . A 12-mer phage-display library was purchased from New England Biolabs for panning according to published protocols [11, 14, 24] using the rS-AD as a target at a concentration of 10 lg/well. The 96-well plates coated with rS-AD, were initially incubated with the phage library (1.5 9 10 11 pfu/ml; 100 ll/well) suspended in TBST (50 mM Tris-HCl [pH 7.5], 150 mM NaCl, 0.05 % Tween-20) for 30 min. Subsequent pannings 2, 3, and 4 were performed using incrementally higher concentrations of Tween-20. The phage titers of the input, output (elution), and amplified phages were determined as defined by the manufacturer.",
            "cite_spans": [
                {
                    "start": 104,
                    "end": 107,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 169,
                    "end": 173,
                    "text": "[10]",
                    "ref_id": null
                },
                {
                    "start": 291,
                    "end": 295,
                    "text": "[11,",
                    "ref_id": null
                },
                {
                    "start": 296,
                    "end": 299,
                    "text": "14,",
                    "ref_id": null
                },
                {
                    "start": 300,
                    "end": 303,
                    "text": "24]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Materials and methods"
        },
        {
            "text": "Indirect ELISA was used to assess the phages that remained after four rounds of biopanning. Either TGEV (0.61 mg/ml) or rS-AD (10 lg/well) in 0.1 M NaHCO 3 pH 8.6 was used to coat 96-well plates at 4\u00b0C for 12 h. The next day, the plates were blocked with 1 % bovine serum albumin (BSA) in TBS (TBSB) for 2 h, washed (39) with TBST, and then incubated with phage (1.5 9 10 12 pfu/ml in 0.1 M NaHCO 3 , pH 8.6; 100 lg/well) for 1 h at 37\u00b0C.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Binding of phage to TGEV or rS-AD protein"
        },
        {
            "text": "The plates were again washed with TBST, then incubated for 1 h at 37\u00b0C with rabbit anti-M13 antibody (1:1000 in TBSB; Abcam), followed by horseradish peroxidase (HRP)-conjugated goat anti-rabbit IgG antibody (GARP; 1:5000 in TBSB, Sigma). The OD 490 nm was determined in triplicate as previously described [24] .",
            "cite_spans": [
                {
                    "start": 306,
                    "end": 310,
                    "text": "[24]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Binding of phage to TGEV or rS-AD protein"
        },
        {
            "text": "Ten phages with the highest affinity for binding rS-AD as determined by ELISA were amplified, precipitated with polyethylene glycol-NaCl, and then used for DNA extraction according to the manufacturer's instructions (New England Biolabs). Amplification of the genes encoding the exogenous peptides was performed using sense (5 0 -TCACCTCGAAAGCAAGCTGA) and anti-sense (5 0 -CCCTCATAGTTAGCGTAACG) M13 primers followed by DNA sequencing [14, 24] . The PCR conditions were as follows: 95\u00b0C for 5 min, 30 cycles of 95\u00b0C for 30 s, 57\u00b0C for 30 s, 72\u00b0C for 30 s, and a final extension at 72\u00b0C for 7 min.",
            "cite_spans": [
                {
                    "start": 434,
                    "end": 438,
                    "text": "[14,",
                    "ref_id": null
                },
                {
                    "start": 439,
                    "end": 442,
                    "text": "24]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Binding of phage to TGEV or rS-AD protein"
        },
        {
            "text": "To compare the sensitivities of phage-mediated ELISA to antibody-mediated ELISA, TGEV serially diluted in 0.1 M NaHCO 3 (pH 8.6) was coated onto duplicate ELISA plates overnight at 4\u00b0C followed by blocking with 5 % skim milk for 3 h at rt. The selected phages or unbound phage complexes (negative control) diluted in PBS (1.5 9 10 12 pfu/ml) were added to one set of plates, followed by anti-M13 antibody (1:1000 in PBS ? BSA). To the second set of duplicate plates, rabbit anti-TGEV polyclonal antiserum serially diluted in PBS ? BSA, and normal rabbit serum were added as the primary and control antibodies, respectively. After incubating both sets of plates for 1 h at 37\u00b0C followed by extensive washing, GARP (1:5000) was added as described above. The OD 490 values were read on all plates; OD 490 ratios where OD 490 (sample-negative standard) (P)/OD 490 (positive control-negative standard) (N) [ 2 were judged as positive. All experiments were performed in triplicate.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Comparison of antibody-and phage-mediated ELISA"
        },
        {
            "text": "The TCID 50 of TGEV was determined using the Reed-Muench method, and TGEV was adjusted to 0.61 mg/ml in PBS. Total RNA was extracted from 300 ll of virus (Fastgene, China) and dissolved in 20 ll of sterile water. Reverse transcription was performed in 20 ll using 2 ll of RNA (550 ng/ll), Oligo dT as primer, and M-MLV reverse transcriptase as recommended by the manufacturer (TaKaRa, China). The resulting cDNA (1 ll) was used as a template for PCR in 20 ll which included 0.2 ll of 109 Easy Taq polymerase (TaKaRa, China), 1 ll of dNTP (2.5 mM), 109 PCR Buffer (1 ll), and 0.2 ll each of sense (5 0 -CTTAGTAGTAATATTTTGCATAC) and antisense (5 0 -TATAGCAGATGATAGAATTAACA) primers. Amplification conditions were as follows: 94\u00b0C for 5 min, then 30 cycles of 95\u00b0C 30 s, 47.6\u00b0C 30 s, and 72\u00b0C 40 s followed by a final extension at 72\u00b0C for 10 min. The amplified fragment was confirmed by DNA sequencing.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "RT-PCR for diagnosing TGEV"
        },
        {
            "text": "Phage specificity was evaluated against the following panel of porcine viruses: TGEV, strain HR/DN1 [25] , porcine epidemic diarrhea virus (PEDV; strain HLJBY) [26] , porcine reproductive and respiratory syndrome virus (PRRSV; strain JilinTN1) [27] , porcine circovirus type II (PCV2; strain PCV2-LJR) [28] , porcine parvovirus (PPV; strain PPV2010) [29] , porcine pseudorabies virus (PrV; strain Kaplan) [23, 30] , and porcine rotavirus (PRoV; isolate DN30209) [31] . All viruses were initially coated at 8 lg/ml then serially diluted in 0.1 M NaHCO 3 (pH 8.6) and subjected to phage-ELISA as described above. Average OD 490 values were obtained from three independent experiments.",
            "cite_spans": [
                {
                    "start": 100,
                    "end": 104,
                    "text": "[25]",
                    "ref_id": null
                },
                {
                    "start": 160,
                    "end": 164,
                    "text": "[26]",
                    "ref_id": null
                },
                {
                    "start": 244,
                    "end": 248,
                    "text": "[27]",
                    "ref_id": null
                },
                {
                    "start": 302,
                    "end": 306,
                    "text": "[28]",
                    "ref_id": null
                },
                {
                    "start": 350,
                    "end": 354,
                    "text": "[29]",
                    "ref_id": null
                },
                {
                    "start": 405,
                    "end": 409,
                    "text": "[23,",
                    "ref_id": null
                },
                {
                    "start": 410,
                    "end": 413,
                    "text": "30]",
                    "ref_id": null
                },
                {
                    "start": 462,
                    "end": 466,
                    "text": "[31]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Phage expressing surface peptides for viral diagnosis"
        },
        {
            "text": "Data were collated and the mean \u00b1 SD values were determined. Arithmetic means were compared between treatment groups using ANOVA (SPSS 15.0; SPSS Inc., Chicago, Illinois, USA) followed by Duncan's multiplerange test. Values of p \\ 0.05 and p \\ 0.01 were defined as statistically significant (''*'') or highly significant (''**''), respectively.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Statistical analyses"
        },
        {
            "text": "In this study, we used phage display to select 12-mer peptides that bind rS-AD [12] and that may function for diagnosing TGEV infections. After four rounds of panning, rS-AD-specific phages increased 119 from 4.7 9 10 4 in the first round to 5.3 9 10 5 in the fourth round (Table 1) . Following the last screen, we selected 10 phage clones from the original 18 that bound both rS-AD and TGEV. This subset was characterized by ELISA with respect to their binding efficiencies (Fig. 1) .",
            "cite_spans": [
                {
                    "start": 79,
                    "end": 83,
                    "text": "[12]",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 273,
                    "end": 282,
                    "text": "(Table 1)",
                    "ref_id": "TABREF1"
                },
                {
                    "start": 475,
                    "end": 483,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Results and discussion"
        },
        {
            "text": "PCR amplification and sequencing indicated that nine distinct 12-mer peptides were identified among the 10 phages that were selected ( Table 2 ). In contrast to previous reports [14, 22, 24] , these peptides exhibited substantial sequence diversity in the number of peptides that bound to rS-AD. It is not known if this relates to the length of the target protein or to changes made in the panning process to enhance binding specificity.",
            "cite_spans": [
                {
                    "start": 178,
                    "end": 182,
                    "text": "[14,",
                    "ref_id": null
                },
                {
                    "start": 183,
                    "end": 186,
                    "text": "22,",
                    "ref_id": null
                },
                {
                    "start": 187,
                    "end": 190,
                    "text": "24]",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 135,
                    "end": 142,
                    "text": "Table 2",
                    "ref_id": null
                }
            ],
            "section": "Results and discussion"
        },
        {
            "text": "As shown in Fig. 2 , we selected four (phTGEV-SAD-15, phTGEV-SAD1/7, phTGEV-SAD11, phTGEV-SAD16) of the ten phages with the highest binding affinity to TGEV for further testing. The lowest detectable quantity of TGEV for the above defined phages was 0.1, 0.3, 0.2, and 0.4 mg, respectively, suggesting that phTGEV-SAD15 was the most sensitive when used in a phage-based ELISA. Binding directly to TGEV was uncharacteristically better than binding to the rS-AD used in the selection process (Figs. 1, 2) . This is likely attributable to more complete folding of the native protein or to better accessibility of the binding epitope in the native form.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 12,
                    "end": 18,
                    "text": "Fig. 2",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 490,
                    "end": 502,
                    "text": "(Figs. 1, 2)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Results and discussion"
        },
        {
            "text": "The minimum quantity of TGEV required for detection via antibody-based ELISA was 0.6 lg (P/N value [ 2) (Fig. 3) , whereas the minimum quantity of TGEV required for phTGEV-SAD15-based ELISA was 0.1 lg. This is consistent with the phage-mediated ELISA being more sensitive than conventional antibody ELISA. A number of ELISA-based assays have been developed over the years for detecting TGEV, many of which have been directed at differentiating TGEV from PRCV-infected animals. Among the earlier ones, Sestak et al. [32] targeted the S glycoprotein of TGEV in a competition ELISA where recombinant S protein was coated onto plates and used to capture host antibodies. Using a monoclonal Ab to epitope D and which is specific for TGEV, the investigators were able to differentiate the infectious agents. Liu et al. [33] cloned and expressed the nucleoprotein (N) to develop an ELISA. Compared to the Virus neutralization assay, they demonstrated 98 % sensitivity and specificity; however, they did not characterize or address the lower level of sensitivity in vitro or in vivo. In 2010, Elia et al. [34] used the recombinant S protein to develop an ELISA to assess swine-like TGEV coronaviruses in canine hosts. Given the novelty of the virus, they were unable to compare it to other assays currently in use. Zou et al. [35] use techniques similar to those developed here, i.e., peptide display, to target the M protein of TGEV in developing an ELISA-based diagnostic test. In this case, the sensitivity of the ELISA exceeded that When the phage-mediated ELISA and antibody ELISA were compared to RT-PCR which targeted a 208-base pair fragment of the S gene, the RT-PCR was most sensitive of all assays tested. This is not unexpected given the higher sensitivity of PCR assays in general. PCR amplification was positive using cDNA equivalents of 0.02 lg of TGEV (data not shown). Real-time PCR and/or nested PCR would clearly have generated even more sensitive results. In addition, phages expressing peptide that bind to TGEV S-AD did not bind to other selected viruses (Fig. 4) . Table 2 Sequences of TGEV rS-AD peptides. Predicted amino acid sequences were generated for ten selected phages In summary, we identified peptides that specifically bind to TGEV and can form the basis of new diagnostic tests where the sensitivity of phTGEV-SAD15 was 0.1 lg of TGEV. This sensitivity fared quite well when compared to the antibody-mediated ELISA which had a sensitivity of 0.6 lg but fell short of the sensitivity of RT-PCR; however, phTGEV-SAD-15 provides a quicker and less costly alternative to RT-PCR. ",
            "cite_spans": [
                {
                    "start": 515,
                    "end": 519,
                    "text": "[32]",
                    "ref_id": null
                },
                {
                    "start": 813,
                    "end": 817,
                    "text": "[33]",
                    "ref_id": null
                },
                {
                    "start": 1097,
                    "end": 1101,
                    "text": "[34]",
                    "ref_id": null
                },
                {
                    "start": 1318,
                    "end": 1322,
                    "text": "[35]",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 104,
                    "end": 112,
                    "text": "(Fig. 3)",
                    "ref_id": "FIGREF4"
                },
                {
                    "start": 2069,
                    "end": 2077,
                    "text": "(Fig. 4)",
                    "ref_id": "FIGREF5"
                },
                {
                    "start": 2080,
                    "end": 2087,
                    "text": "Table 2",
                    "ref_id": null
                }
            ],
            "section": "Results and discussion"
        }
    ],
    "bib_entries": {
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Diseases of swine 7th Ed",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "J"
                    ],
                    "last": "Saif",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "D"
                    ],
                    "last": "Wesley",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "362--386",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "dodecapeptide library was selected for binding to rS-AD. The values are given in plaque forming units (Pfu), representing the number of phage peptides eluted from the antibodytargeting the A-D regions of the S protein. This is likely the result of the location and overall accessibility of the M protein to the host immune response when compared to the S protein.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Phage binding to rS-AD and TGEV. [Eighteen selected phages designated phTGEV-SAD 1-phTGEV-SAD 18 were incubated with rS-AD or TGEV then screened with anti-M13 antibody and visualized with HRP-conjugated secondary antibody. The ELISA OD values represent the binding efficiencies of 18 different phages to rS-AD and TGEV. The experiment was performed in triplicate. Controls are (1) non-binding phage library, (2) secondary antibodyfree, (3) phage-free, (4) porcine circovirus-cap protein, (5) M proteins of TGEV, (6) blocking buffer, and (7) virus dilution buffer.]",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Detection limit of TGEV by phage-mediated ELISA. [TGEV serially diluted in PBS was used to coat ELISA plates. The plates were then incubated with phages (ph) phTGEV-SAD15, phTGEV-SAD1/7, phTGEV-SAD11, or phTGEV-SAD16, followed by anti-M13 antibody and GARP. The OD 490 ratios [(P 490 )/(N 490 )] [ 2 were designated as positive responses. The P/N values were derived from triplicate assays. Virus concentrations are indicated on the x axis.]",
            "latex": null,
            "type": "figure"
        },
        "FIGREF4": {
            "text": "Detection limit of TGEV by antibody-based ELISA. [The TGEV serially diluted in PBS was used to coat ELISA plates. The bound virus was then screened with serially diluted rabbit anti-TGEV or with normal rabbit serum (negative control) followed by GARP. The OD 490 ratios [(P 490 )/(N 490 )] [ 2 were designated as positive responses. The P/N values were derived from triplicate assays. Virus concentrations are indicated on the x axis.]",
            "latex": null,
            "type": "figure"
        },
        "FIGREF5": {
            "text": "Binding specificity of phTGEV-SAD15. [One phage, designated phTGEV-SAD15, was selected from the 10 phages analyzed inFig. 2and tested against other common swine viruses for potential cross-reactivity. Shown in this figure are ELISA values using phTGEV-SAD15 to screen the following porcine viruses: epidemic diarrhea virus (PEDV), respiratory syndrome virus (PRRSV), circovirus (PCV 2 ), reproductive porcine parvovirus (PPV), pseudorabies virus (PrV), and rotavirus (PRoV) and visualized using rabbit anti-M13 antibody and GARP. The OD 490 ratios between individual phages and a control phage are shown on the y axis; p \\ 0.01 is defined by ''**'' relative to other viruses. Bars show the standard deviation from three independent assays.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "by Keizo Tomonaga.",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Enrichment of phage that bind rS-AD after each round of biopanning",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "The authors declare no conflicts of interest.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflict of interest"
        }
    ]
}