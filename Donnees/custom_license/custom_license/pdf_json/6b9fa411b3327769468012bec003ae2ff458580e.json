{
    "paper_id": "6b9fa411b3327769468012bec003ae2ff458580e",
    "metadata": {
        "title": "The camel, new challenges for a sustainable development",
        "authors": [
            {
                "first": "Bernard",
                "middle": [],
                "last": "Faye",
                "suffix": "",
                "affiliation": {
                    "laboratory": "UMR SELMET",
                    "institution": "CIRAD-ES",
                    "location": {
                        "addrLine": "Campus international de Baillarguet TAC/112A",
                        "settlement": "Montpellier",
                        "country": "France"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Among the mammals domesticated by humans for their needs, the camel has a peculiar status: highly adapted to a specific ecosystem (the desert) and it is a multipurpose animal used for production (milk, meat, wool, skin, manure) , leisure (racing, sport such as polo, tourism, beauty contests, festivals), transport (riding, carting, pack carrying), or agricultural work (plowing, weeding, harrowing, noria, water extraction) . No other domestic animal is able to provide such a variety of uses for human populations. In spite of these numerous services to humans, the image of the camel is that of an animal from the past, evoking mythic caravans through the more arid lands of the world and extensive networks managed by nomads from Mauritania to the Middle East, from Mongolia to India. However, the camel population is not declining at the world level, even if its status is variable from one country to another, from its alarming decline in India to its astonishing growth in the Horn of Africa and Sahelian countries. This is in spite of its marginal number (27 million in 2014 according to FAO) compared with other domestic farm animals and the regular decline in the proportion of the human population living in the desert. Whereas the nomad population has decreased from 10 to 1.5 % during the last five decades, the camel population in 2014 has more than doubled since 1961 (first available world annual data), corresponding to an annual growth of more than 2.0 %. By comparing this annual growth with other species at the world level, the camel population has grown faster than the cattle, sheep, horse, and lama populations and has a similar growth rate to that of the buffalo population, with only a growth rate lower than that of the goat population (Faye and Bonnet 2012) . In 1961, the proportion of camels in the Domestic Herbivorous Biomass (DHB) was 1.1 %. In 2014, it was 1.5 %.",
            "cite_spans": [
                {
                    "start": 195,
                    "end": 227,
                    "text": "(milk, meat, wool, skin, manure)",
                    "ref_id": null
                },
                {
                    "start": 370,
                    "end": 424,
                    "text": "(plowing, weeding, harrowing, noria, water extraction)",
                    "ref_id": null
                },
                {
                    "start": 1763,
                    "end": 1785,
                    "text": "(Faye and Bonnet 2012)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "This growth has occurred concomitantly with a change in livestock systems. Nowadays, camels are not only the animals of Bedouins. They are now a part of modern life in the desert (Breulmann et al. 2007) , and they contribute significantly to desert productivity, thanks to their production potential (mainly milk and meat) and the high added value of their products (Tefera and Gebreah 2001; Kurtu 2004; Musaad et al. 2013) . Traditionally based on an extensive system characterized by the use of natural resources, low inputs, and herd mobility, three main driving forces are contributing to the current changes:",
            "cite_spans": [
                {
                    "start": 179,
                    "end": 202,
                    "text": "(Breulmann et al. 2007)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 366,
                    "end": 391,
                    "text": "(Tefera and Gebreah 2001;",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 392,
                    "end": 403,
                    "text": "Kurtu 2004;",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 404,
                    "end": 423,
                    "text": "Musaad et al. 2013)",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "(i) The accentuation of the environmental aridity linked to climatic changes. Since 1900, the Sahara has extended by 250 km to the south and along a 6000-km front in total (Leroux 2004 ). In such a new context, the camel is one of the less sensitive animals in the recurrent droughtaffected Sahelian countries. The observable impacts of the climatic change on the camel stock include the expansion of the geographical distribution of the species, the use of the camel with its higher integration in mixed croplivestock systems and the increased risk of emerging diseases Megersa et al. 2012) , as the recent outbreak of MERS-Coronavirus has shown (Gossner et al. 2016 ); (ii) The globalization of the world economy forcing camelid farming to become more integrated into the market. To date, the contribution of the camel products in the global economy has been marginal, except for camel meat, which is integrated into the regional market between the Horn of Africa and Arabian Peninsula, or for alpaca wool, which integrated into the international textile market. However, in many countries, the emergence of small or large camel dairy plants and the development of milk processing, allowing new camel dairy products such as pasteurized milk, cheese, yogurt, or ice cream, has led to significant changes in the added value chains of camel milk (Faye et al. 2014 ); (iii) The change in the territorial distribution marked by an expansion of traditional farming area and by an increasing risk of emerging diseases. The territorial expansion of the camel is not only related to the expansion of traditional camel breeders but also to the expansion of the species into cattle farming systems. Consequently, the camel is an invading species that groups of settled agro-pastoralists are ready to adopt for agricultural activities. Camels are thus contributing to the intensification process of the farming systems associated with the settlement of the farmers.",
            "cite_spans": [
                {
                    "start": 172,
                    "end": 184,
                    "text": "(Leroux 2004",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 571,
                    "end": 591,
                    "text": "Megersa et al. 2012)",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 647,
                    "end": 667,
                    "text": "(Gossner et al. 2016",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1345,
                    "end": 1362,
                    "text": "(Faye et al. 2014",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Thus, the intensification process of camel farming system is on-going. Nevertheless, this change is not without its effects on the animals. The camelidae family is characteristic of animals occupying remote areas in arid lands or high mountains, as a result of their close adaptation to these ecosystems. Large and small camelids are able to add value to milieu characterized by dispersed resources, low nutritive values, and water scarcity. This explains the interest in this family with respect to the maintenance of rural activities in the most inhospitable places on Earth. Their anatomy, physiology, and behavior are well suited for survival in such harsh environments. The consequences of the intensification process of their management are obviously important issues that need to be approached by scientists.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Indeed, the camel is passing from the status of multipurpose to highly specialized animals for dairy production, fattening, or racing. In pastoral areas, the camel is able to graze the greatest variety of plants, in comparison with other herbivorous ruminants, including halophyte grasses, bushes, and trees, leading to lower pressure on the floristic biodiversity of the arid lands (Laudadio et al. 2009 ). The camel has an ambulatory and low gregarious behavior on pasture, grazing for more than 8 h/day, whereas in intensive farms, camels become settled and are fed with a monotonous diet distributed twice a day. The digestive physiology of camels (nitrogen recycling, slow transit, ruminal flora,\u2026) allows them to make better use of low-quality forages and leads to a more superior feeding efficiency than that of cows, thereby contributing to a better resources/production ratio. However, the consequences of a more limited feeding variability and more widely spaced meals on the rumen flora, on the feeding conversion, on the fattening, or on metabolic disorders of intensively farmed camels are probably not sufficiently studied and require more attention from the camel scientists.",
            "cite_spans": [
                {
                    "start": 383,
                    "end": 404,
                    "text": "(Laudadio et al. 2009",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Camel milk is famous for its medicinal virtues, true or supposed, and the nomads attribute these health effects to the milk composition linked to the desert plants on which free-roaming camels graze. The impact of the changes in feeding practices (low diet variety, spaced meals) on the milk composition and medicinal activities of intensively farmed camels has not really been investigated, and neither has the nutritive value and chemical composition of their meat (Kurtu 2004) . The modernization of management, for example, in milking practices by using milking machines has to be investigated (Atigui et al. 2015) for a better adaptation of this technology to camel because of their specific physiology of lactation (Fig. 1) . Furthermore, the implementation of the biotechnology of reproduction (artificial insemination, embryo transfer) is being strongly developed, especially in the Middle-East (Anouassi and Tibary 2013) , although the spread of those biotechnologies among the camel breeders is still anecdotal.",
            "cite_spans": [
                {
                    "start": 467,
                    "end": 479,
                    "text": "(Kurtu 2004)",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 598,
                    "end": 618,
                    "text": "(Atigui et al. 2015)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 903,
                    "end": 929,
                    "text": "(Anouassi and Tibary 2013)",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [
                {
                    "start": 721,
                    "end": 729,
                    "text": "(Fig. 1)",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "Moreover, intensification leads to higher water requirements. For example, in Saudi Arabia where camel farming moved from the Bedouin system to semi-intensive or even intensive regimes based on feeding by irrigated feedstuffs and settlement, water consumption increased from 3000 to 35,000 m 3 /ha, and according to biomass productivity, water consumption for the feeding of one camel was multiplied by 3.2 contributing to higher pressures on water resources (Faye 2013) . At the national level, compared with the situation in 1961 when almost all camels were reared in the Bedouin style, water consumption in Saudi Arabia has increased approximately from 180,000 to 280,000 m 3 with regard to the Bedouin system, whereas it has risen from 7000 to 860, 000 m 3 in intensive systems during the last 50 years (Faye 2013) . Thus, the intensification process in the camel sector Fig. 1 The modernization of the camel farming system: the implementation of the milking machine requires research into its adaptation to the physiology of camel lactation-Al-Kharj, Saudi Arabia (by courtesy of B. Faye) has a strong effect on water demand, which could be an important constraint for sustainable development.",
            "cite_spans": [
                {
                    "start": 459,
                    "end": 470,
                    "text": "(Faye 2013)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 807,
                    "end": 818,
                    "text": "(Faye 2013)",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [
                {
                    "start": 875,
                    "end": 881,
                    "text": "Fig. 1",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "Thus, the problem of Bsustainable development^in the face of the intensification process in camel farming systems is obviously important for the scientific community and is too often hidden behind a simple search of technical innovation. The commitment to sustainable development implies responsible and proactive decision-making and innovations that minimize negative impacts and maintain balance between social, environmental, and economic growth to ensure a desirable planet for all species, both now and in the future.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In this context, camelids, which are the most important animals domesticated by mankind in the desert or the Andean ecosystem, are faced with important challenges, because they are directly confronted with one of the main hot spots with regard to the interaction of livestock/environment, i.e., the desertification process. As is generally agreed, camelids are an environmentally friendly animal, and camelid farming systems have a low environmental pressure activity. However, how will the current changes in camel farming modify the traditional relationships between the camel and their environment? What is the contribution of camelids to greenhouse gas emissions in relation to camel demography? Can we assess and preserve camelid biodiversity? How can we determine the changes that might occur to camel metabolism and management under conditions of intensification? How can we control transboundary diseases in a population marked by increasing local and international mobility? What is the future of the social role of camelid in an increasingly urbanized world (Fig. 2) ?",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 1068,
                    "end": 1076,
                    "text": "(Fig. 2)",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "These are some of the questions faced by the camel scientists of today. The International Society of Camelid Research and Development (ISOCARD) was created in 2006 at Al-Ain (United Arab Emirates) with the object of giving international scientific status to camelid sciences by promoting research and practice, by organizing regular international conferences, and by encouraging the exchange of information between the members of the Society and various networks and involved organizations. The instigators of ISOCARD hope to give acclaim to the field of Bcamelology^. The present topical collection in Tropical Animal Health Production devoted to camelids is an important step in the recognition of the Bcamelologist community^. The first few papers presented in this collection were presented at the Fourth International Conference of ISOCARD held at Almaty (Kazakhstan) in June 2015. These contributions demonstrate the vitality of this (young) community, even if the interest in camelids by scientists is old. Camelids between tradition and modernity, new questions and advanced answers with regard to the biology of camelids, the products of camelids with uses for the wellbeing of humanity, the emerging diseases and permanent health problems to be solved, the camelids and their environment, the impact of climatic changes, the Camelid economy from local to international market, and the place of camelids in the socio-cultural context and in history were some of the approaches developed by the participants to the last conference of ISOCARD. The present topical collection aims to reflect all these questions.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Obviously, sustainable development is not merely a challenge for camelids, but as Bthe animal of the desert^(camel) or Bthe animal of the Andean highlands^(small camelids), their responsibility is particular high because they contribute strongly to the maintenance of rural activities and the household economy among the more remote places of our planet. The intensification of their farming is not the ineluctable way, but henceforth, camelid breeders have concurrently to contribute to maintain camel diversity, to improve the management of their resources in order to provide products with high added value as expected by the urban population, and to preserve the future of their animals.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Development of a large commercial camel embryo transfer program: 20 years of scientific research",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Anouassi",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Tibary",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Anim. Reprod. Sci",
            "volume": "136",
            "issn": "3",
            "pages": "211--221",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Effects of vacuum level and pulsation rate on milk ejection and milk flow traits in Tunisian dairy camels (Camelus dromedarius)",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Atigui",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "G"
                    ],
                    "last": "Marnet",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Barmat",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Khorchani",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Hammadi",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Trop. Anim. Health Prod",
            "volume": "47",
            "issn": "",
            "pages": "201--206",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "The camel, from tradition to modern times",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Breulmann",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Boer",
                    "suffix": ""
                },
                {
                    "first": "U",
                    "middle": [],
                    "last": "Wernery",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Wernery",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "El-Shaer",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Alhadrami",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Gallacher",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Peacock",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Chaudhary",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Brown",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Norton",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Unesco Doha Publ",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Camel sciences and economy in the world: current situation and perspectives",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Faye",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Bonnet",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Proc. 3rd ISOCARD conference. Keynote presentations",
            "volume": "",
            "issn": "",
            "pages": "2--15",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "From the Bedouin system to intensive farming: what changes are required in the relationship between man and the camel in the urbanized world?",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Integrated impact of climate change and socioeconomic development on the evolution of camel farming systems",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Faye",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Chaibou",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Vias",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "British J. Environ. Clim. Change",
            "volume": "2",
            "issn": "3",
            "pages": "227--244",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Camel Farming Sustainability: The Challenges of the Camel Farming System in the XXIth Century",
            "authors": [
                {
                    "first": "Faye",
                    "middle": [
                        "B"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J. Sustainable Dev",
            "volume": "6",
            "issn": "12",
            "pages": "74--82",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Camel milk value chain in Northern Saudi Arabia",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Faye",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Madani",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "H"
                    ],
                    "last": "El-Rouili",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Emir J. Food Agric",
            "volume": "26",
            "issn": "4",
            "pages": "359--365",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Human-Dromedary Camel Interactions and the Risk of Acquiring Zoonotic Middle East Respiratory Syndrome Coronavirus Infection",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Gossner",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Danielson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Gervelmeyer",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Berthe",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Faye",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kaasik-Aaslav",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Adlhoch",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Zeller",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Penttinen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Coulombier",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "",
            "volume": "63",
            "issn": "",
            "pages": "1--9",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "An assessment of the productivity for meat and the carcass yield of camels (Camelus dromedarius) and of the consumption of camel meat in the Eastern region of Ethiopia",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "Y"
                    ],
                    "last": "Kurtu",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Trop. Anim. Health Prod",
            "volume": "36",
            "issn": "",
            "pages": "65--76",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "A survey of chemical and nutritional characteristics of halophytes plants used by camels in Southern Tunisia",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Laudadio",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Tufarelli",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Dario",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Hammadi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "M"
                    ],
                    "last": "Seddik",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "M"
                    ],
                    "last": "Lacalandra",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Dario",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Trop. Anim. Health Prod",
            "volume": "41",
            "issn": "",
            "pages": "209--215",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "La dynamique de la grande s\u00e9cheresse du sahel",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Leroux",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "La dynamique du temps et du climat, 2e \u00e9dition, DUNOD (publ)",
            "volume": "210",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Epidemic characterization and modeling within herd transmission dynamics of an Bemerging transboundary^camel disease epidemic in Ethiopia",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Megersa",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Biffa",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Abunna",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Regassa",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Bohlin",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Skjerve",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Trop. Anim. Health Prod",
            "volume": "44",
            "issn": "",
            "pages": "1643--1651",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Lactation curves of dairy camels in an intensive system",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Musaad",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Faye",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Abu-Nikhela",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Trop. Anim. Health Prod",
            "volume": "4",
            "issn": "",
            "pages": "1039--1046",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "A study on the productivity and diseases of camels in Eastern Ethiopia",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Tefera",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Gebreah",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Trop. Anim. Health Prod",
            "volume": "33",
            "issn": "",
            "pages": "265--274",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}