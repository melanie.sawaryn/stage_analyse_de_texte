{
    "paper_id": "115c4899a257118e2776fd582d64fd2cf431e30f",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "P",
                "middle": [],
                "last": "Rabinow",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "G",
                "middle": [],
                "last": "Bennett",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "How can animals send signals of future threats to humans? This question has always fascinated humans and anthropologists, but it gains a new meaning in the contemporary world of biosecurity where health issues are considered as security priorities and coming epidemics are the objects of anticipatory knowledge. Understanding how non-humans are enroled in anticipatory knowledge and how this transforms their relations with humans is a key concern for social scientists.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A microbiological article that is of particular interest to this body of scholarship was written in 2003 by Shortridge, Peiris and Guan: \"The Next Influenza Pandemic: Lessons from Hong Kong\". Published in the Journal of Applied Microbiology, it addresses a broader audience and draws lessons from 7 years of mobilisation against influenza as a public health concern. When microbiologists learn from history, as they have done for AIDS (Dodier, 2003) , they write in the language of viruses. Because of the high mutation rate of the flu virus, new forms of the disease constantly emerge and can become pandemic, as was the case in Hong Kong in 1968. The question raised by Shortridge et al is which flu virus was the candidate for the next pandemic and how it could be detected before spreading widely. For a 'global clinic' (King, 2002) it is necessary to know what candidate vaccines must be prepared for a future outbreak. However, we have learned from Hong Kong that stockpiling vaccines is not the only way to prepare for a pandemic.",
            "cite_spans": [
                {
                    "start": 435,
                    "end": 449,
                    "text": "(Dodier, 2003)",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 824,
                    "end": 836,
                    "text": "(King, 2002)",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Shortridge et al's article draws lessons not only from history but also from geography. Being a British colony in 1968, Hong Kong was the place where the last pandemic flu virus was effectively detected, but it may have emerged in Southern China, where little information was available at that time. Trained in Australia, Kennedy Shortridge joined the Department of Microbiology of Hong Kong University in 1972 to provide data on flu viruses to the World Health Organization. He proposed a hypothesis explaining the role of South China as an 'epicentre' of flu pandemics (Shortridge and Stuart-Harris, 1982) . Flu viruses mutate in waterfowl, particularly ducks, where they develop in the digestive tract without killing the animals. They are transmitted to domestic poultry and pigs, which serve as 'mixing vessels' between birds and humans. Influenza is a zoonosis: a disease transmitted from animals with unpredictable effects on humans because of their lack of immunity. South China has a high concentration of waterfowl, poultry, pigs and humans, which speeds up the rate of mutations of flu viruses: it is an \"avian influenza virus melting pot\" (p. 72S). Serving as a hub between East and West, Hong Kong is the place from where these new viruses are transmitted to the rest of the world. Hence, Shortridge's idea is that it should be possible to map flu viruses in their 'animal reservoir' to detect in advance those which will 'jump the species barriers' between birds, pigs and humans. But this raises a new question: How, among all the flu viruses that can be found in animals, to bet on the right 'candidate virus' that will actually cross the species barrier? If this question sounds 'speculative', notice that the only gambling activity allowed in Hong Kong is horse racing. This is where history comes back in. In 1997, a new flu virus emerged, killing 6 persons out of the 18 it infected. It also killed 5000 chickens in poultry farms, and it was estimated that 20 per cent of chickens in the markets had been infected. What was surprising about this virus was its high lethality and the fact that it had jumped directly from birds to humans since it was not found in pigs. This changed the scenario: it was not possible to make a vaccine for this new virus because it killed the chicken embryos used for conventional vaccines (\"recombinant and genetically engineered vaccines are being explored\", Shortridge et al added). But it became possible to imagine a world where this new lethal virus would become pandemic and kill more humans than the 1918 'Spanish flu'. It could spread either through humans (although its inter-human transmission had not been proved), through migratory birds (but can sick birds fly?) or through the poultry industry (a factor emphasised by the article).",
            "cite_spans": [
                {
                    "start": 571,
                    "end": 607,
                    "text": "(Shortridge and Stuart-Harris, 1982)",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Flu viruses are classified by their haemagglutinin (H) and neuraminidase (N) proteins, that allow them to enter and exit from cells. This new virus, called H5N1, was thus considered as the first candidate for a new pandemic. Circulating in birds, H9N2 and H6N1 followed suit. Ordering viruses by the range of their pandemicity was not based on probability of genetic mutation but on their actual capacity to jump the species barrier with catastrophic consequences. \"It might be possible, for the first time, to have influenza pandemic preparedness at the baseline avian level\", wrote Shortridge et al (2003, p. 70 ): this idea is repeated several times in the article. It means that microbiologists can detect mutations at the species barrier and send out 'early warnings' on those that may become pandemic. The image of a fire is evoked when the authors describe the emergence of H5N1 as \"smouldering\": if pandemic viruses are detected early enough, they will be like a fire without a flame.",
            "cite_spans": [
                {
                    "start": 584,
                    "end": 613,
                    "text": "Shortridge et al (2003, p. 70",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "This had powerful political consequences. In 1997, in the context of the handover of the British colony to the People's Republic, the claim that birds coming from China could start a pandemic from Hong Kong stirred up tensions. Shortridge et al noted: \"The incident was the first issue the fledging Special Administrative Region (SAR) had to deal with since Hong Kong's reunification with China. In a sense, the SAR felt vulnerable, acutely aware of its international responsibility and that it was being tested\" (p. 76S). In this context, preparing for the pandemic took a military turn, characteristic of 'biosecurity interventions' (Lakoff and Collier, 2008) . On 29 December 1997, all the live poultry on the territoryaround 1.5 millionwere killed to eradicate the H5N1 virus from its animal reservoir (the term 'incident' may here appear as a euphemism). Commenting on 'the controversial slaughter policy', the authors argue that \"it is reasonable to believe that this intervention prevented an incipient pandemic progressing to an actual pandemic and thus a pandemic was averted\" (p. 72S).",
            "cite_spans": [
                {
                    "start": 635,
                    "end": 661,
                    "text": "(Lakoff and Collier, 2008)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The article thus mixes different registers of anticipatory knowledge: prevention, precaution and preparedness. Shortridge et al attributed the idea that a pathogen is detected before it causes a disease to ancient epidemiological principles of prevention in Chinese medicine. But the 1997 event was analysed as a failure of prevention: it \"gave rise to a precautionary principle, essentially a watching brief of preparedness\". In the absence of certainty on the H5N1 virus, it was necessary to eradicate it in its animal reservoir \"as a preemptive measure\" (p. 73S). If the pandemic had been averted and not prevented, it was certainly imminent: \"each year brings us closer to the next pandemic\" (p. 75S). The failure of prevention (building a vaccine for viruses before they become lethal) forced the Hong Kong government to use precautionary measures, that is, to kill all live poultry, and to strengthen its preparedness by closely monitoring virus mutations in animals.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The question for Shortridge et al was how Hong Kong could act as a 'sentinel post'a vocabulary derived from the militarynot only by gathering information on flu viruses on both sides of the human/animal divide, but also by intervening in a situation of uncertainty. If the public and the media, they argued, \"could not cope with the uncertainty\" (of viral mutations), they would understand public health measures by their impact on food. Hence the stress on live poultry markets as a traditional practice that had a major impact on the risk of flu transmission, even if the virus was killed in the process of cooking. The spectre of a coming pandemic became credible to the public when related to the uncertainties of contact with live poultry. This relation between public health and food safety is one of the main drivers for building 'sentinel posts' for avian influenza.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "If a sentinel post is sensitive to emerging events at the human/animal interface, it can also overreact when betting on the wrong virus (Keck and Lakoff, 2013) .",
            "cite_spans": [
                {
                    "start": 136,
                    "end": 159,
                    "text": "(Keck and Lakoff, 2013)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "It is remarkable that the discussed article was published in 2003 just after the outbreak of Severe Acute Respiratory Syndrome (SARS) in Hong Kong. The two coauthors of Shortridge discovered the human coronavirus responsible for SARS (Peiris) and its animal origins in South China (Guan). But the team initially lost two weeks as it tested for H5N1, which gives rise to the same symptoms as SARS. SARS strengthened the position of Hong Kong as a 'sentinel post' sensitive to diseases emerging from animals and spreading to the rest of the world, but it also showed its vulnerabilities: a biosecurity intervention is a bet on the human/animal interface and its potential outcomes. The article thus concludes: \"Given the genetic unpredictability of the influenza A viruses, there probably will be many influenza battles to be fought, some to lose some hopefully to win, in the foreseeable future\" (p. 77S). The 'lessons from Hong Kong' were that the genetic knowledge accumulated on emerging viruses were not enough to face the uncertainties of epidemic outbreaks: hence the need of new relations between humans and animals as 'sentinel devices' where these uncertainties could be converted into early warning signals.t",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "As an anthropologist of science, nature and culture bookend my reading and my thinking, from L\u00e9vi-Strauss to Ortner and Strathern, from Haraway's naturecultures to Latour's nature-culture hybrids, from nature/culture as a foundational dichotomy in human thought to the destabilizing of universal binaries, and from stable categories to complexity, gray areas and blurred boundaries. Nature/culture has been one of the most provocative conceptual pairings in the social sciences, and STS has challenged the idea of nature versus nurture from the outset. My research focuses on twin studiessometimes described as the 'gold standard' for distinguishing between genetic and environmental influences on health and behaviorso questions about nature and nurture are ever-present, both in my informants' work and my own. Scholarship in STS and anthropology challenged me to think about the ongoing work that goes into separating nature from nurture, but reading the work of Francis Galton, the infamous 'father of eugenics', is what helped me understand the enduring role of twins in the construction of a porous boundary between nature and nurture.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Galton's research took him in many strange directions, and he made contributions in areas as varied as statistics, fingerprints and family resemblance. He was the first researcher to propose studying twins to differentiate between the effects of 'nature and nurture', a phrase he coined in his influential article \"The History of Twins, as a Criterion of the Relative Powers of Nature and Nurture\", first printed in Fraser's Magazine in 1875. He is still cited today by scientists using twins to study the effects of genes and environments, and I first came across his work through tracing Alison Cool is a Mellon Sawyer Postdoctoral Fellow in the Department of Anthropology at New York University. Her research is based in Sweden and focuses on collaborations between life scientists and economists who are using twin studies to investigate genetic influences on economic behavior.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Le\u00e7ons politiques de l'\u00e9pid\u00e9mie de sida",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Dodier",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Security, disease, commerce: Ideologies of postcolonial global health",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "B"
                    ],
                    "last": "King",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Social Studies of Science",
            "volume": "32",
            "issn": "5-6",
            "pages": "763--789",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "An influenza epicentre?",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "F"
                    ],
                    "last": "Shortridge",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "H"
                    ],
                    "last": "Stuart-Harris",
                    "suffix": ""
                }
            ],
            "year": 1982,
            "venue": "Lancet",
            "volume": "2",
            "issn": "",
            "pages": "812--813",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "The history of twins, as a criterion of the relative powers of nature and nurture. Fraser's Magazine 12",
            "authors": [
                {
                    "first": "Francis",
                    "middle": [],
                    "last": "Galton",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "566--576",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Quoted from the reprint in",
            "authors": [],
            "year": null,
            "venue": "Journal",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Keck heads the research department of the Mus\u00e9e du quai Branly. He has translated Paul Rabinow's works into French, edited works of Claude L\u00e9vi-Strauss and published Un monde gripp\u00e9 (Paris, Flammarion, 2010). He is preparing a book on the anticipation of epidemics at the animal level.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}