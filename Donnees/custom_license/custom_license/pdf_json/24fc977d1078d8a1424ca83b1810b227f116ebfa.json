{
    "paper_id": "24fc977d1078d8a1424ca83b1810b227f116ebfa",
    "metadata": {
        "title": "Journal Pre-proof The family caregiving crisis meets an actual pandemic Title: The family caregiving crisis meets an actual pandemic",
        "authors": [
            {
                "first": "Erin",
                "middle": [
                    "E"
                ],
                "last": "Kent",
                "suffix": "",
                "affiliation": {
                    "laboratory": "School of Nursing The University of Alabama at Birmingham Co-Director, Caregiver and Bereavement Support Services UAB Center for Palliative and Supportive Care",
                    "institution": "Health University of North",
                    "location": {
                        "settlement": "Brookdale"
                    }
                },
                "email": ""
            },
            {
                "first": "Katherine",
                "middle": [
                    "A"
                ],
                "last": "Ornstein",
                "suffix": "",
                "affiliation": {
                    "laboratory": "School of Nursing The University of Alabama at Birmingham Co-Director, Caregiver and Bereavement Support Services UAB Center for Palliative and Supportive Care",
                    "institution": "Health University of North",
                    "location": {
                        "settlement": "Brookdale"
                    }
                },
                "email": ""
            },
            {
                "first": "J",
                "middle": [],
                "last": "Nicholas Dionne-Odom",
                "suffix": "",
                "affiliation": {
                    "laboratory": "School of Nursing The University of Alabama at Birmingham Co-Director, Caregiver and Bereavement Support Services UAB Center for Palliative and Supportive Care",
                    "institution": "Health University of North",
                    "location": {
                        "settlement": "Brookdale"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Please cite this article as: Kent EE, Ornstein KA, Dionne-Odom JN, The family caregiving crisis meets an actual pandemic, Journal of Pain and Symptom Management (2020), doi: https://doi.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "The rapid pace of development during the COVID-19 pandemic presents unique challenges to palliative care and other clinicians who work with the seriously ill, including our largest \"hidden\" palliative care workforce: the millions of family caregivers across the world. We have relied on this lay workforce as our global population ages with serious illness, and as complex care continues to transition to the home (1, 2). The professional healthcare workforce is quickly becoming consumed with the deluge of inpatients critically ill with COVID-19. Hence, we are depending on family caregivers now more than ever, as their usual lifelines of medical care and support have been altered or put on hold. Family caregivers continue to deliver complex care to individuals with serious illnesses, including advanced cancer, heart failure, and lung disease, who also happen to be the very individuals most at risk of dying from COVID-19. Social isolation measures to stem virus transmission, such as shelter-in-place orders and family visitation restrictions, present wholly new and stressful, potentially traumatic situations for caregivers. Our current palliative care workforce capacity is strained, and yet it must set the tone for family-centered practice. As crisis standards and protocols are redeveloped and refined during this pandemic, we must consider how decisions will affect not only the care family caregivers are able to provide but also the health and well-being of caregivers themselves.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Three major stressors encapsulate the new challenges COVID-19 has imposed on serious illness family caregivers. The first is the unintended consequences of social distancing, which though necessary for mitigating this crisis, can increase the isolation, loneliness and linked adverse health consequences (3) already experienced by many caregivers and their care recipients. Caregivers already struggle to ask for help and may be even more reluctant now given the fears and anticipatory guilt of having outsiders, who could transmit virus, come into their homes. While telehealth expansion is providing a much needed solution, well-documented disparities in broadband access and digital literacy among large subpopulations of family caregivers, particularly those older and in rural areas, persist (4). Caregivers and their care recipients with limited technological capacity may experience gaps in critical healthcare services over the coming months that greatly limits access to quality care.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Second, caregivers will face new economic stressors due to COVID-19. Approximately 61% of caregivers in the U.S. already report some level of financial strain due to employment loss and out-of-pocket medical expenses (1). COVID-19 will likely further intensify this strain as many working caregivers are at risk of unemployment and by consequence their health insurance coverage, which may also support their care recipient. Many family caregivers have jobs that are not amenable to working remotely and may need to keep working outside the home and risk exposing their households to virus. Some family caregivers may be \"sandwiched\" by having to take on additional childcare and home-schooling responsibilities due to school closures in addition to trying to fulfill work and caregiving responsibilities at home.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Finally, caregivers are now needing to balance an unprecedented landscape of decision-making for their care recipients with maintaining current public health safety practice. Formerly routine healthcare options (eg. primary care and lab visits, follow-up visits for disease surveillance, home health assistance) will be far less available in the months ahead. The need for routine visits may be questioned by patients, caregivers, and providers alike on grounds of safety, urgency, and goals of care. The impact of caregiving stress on caregiver mental and physical health is already considered an urgent public health issue (2). With COVID-19, caregivers face a new set of difficult healthcare decisions that will likely intensify distressing rumination about whether or not they are making the \"right\" or \"best\" decisions for their care recipients.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Both palliative and hospice care have always been patientand family-centered in practice, leading the way for fellow healthcare professionals on how to best support families in times of crisis. As crisis standards and protocols are continually refined, we urge palliative and hospice care clinicians, administrators and leaders to continue to champion support for serious illness family caregivers (and, by extension, patients). Priorities for providing support to families that reinforce current practices under these extreme conditions should include the following: 1. Consider family caregivers in all discussions of personal protective equipment (PPE). First and foremost, family caregivers are essential frontline healthcare workers who are providing daily \"hands on\" care for the most at-risk populations to COVID-19, without direct supervision, support staff, or formal training in COVID-19 safety precautions. Many provide direct personal care to COVID-19 vulnerable or infected patients. Caregivers managing severe pain and breathlessness in the home or caregivers who are themselves older with higher mortality risk due to comorbidities should be considered for distributions of PPE. Healthcare systems should provide guidance about the use and efficacy of homemade masks and other PPE (5) for families in order to promote Centers for Disease Control and Prevention (CDC)-recommended guidance and to avoid sending mixed messages to families from different members of the healthcare team. Clinicians should convey balanced messages about PPE to caregivers: that use alone does not prevent infection and that proper removal and disposal procedures are critical.",
            "cite_spans": [
                {
                    "start": 568,
                    "end": 570,
                    "text": "1.",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "How to best support family caregivers during COVID-19"
        },
        {
            "text": "2. Implement simple risk assessment questions or checklists to assess caregiver capacity for serious illness care in the home, both prior to and after COVID-19 symptom onset of either the patient, caregiver, or both. While there are no validated measures or screening tools for assessing caregiver capacity during a pandemic, the CDC maintains interim guidance for providing home care to guide clinicians (6) . In addition, candidate measures to assess caregiver readiness, burden, and distress such as the Zarit Burden Interview caregiving screening interview (7) and the CancerSupportSource\u00ae-Caregiver distress screening instrument (8) are two options.",
            "cite_spans": [
                {
                    "start": 405,
                    "end": 408,
                    "text": "(6)",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "How to best support family caregivers during COVID-19"
        },
        {
            "text": "3. Develop telehealth capacity to assess and treat patients and include caregivers in these encounters (9) . Recognize that not all will have video access and simpler telephone approaches may be necessary for many individuals. Be aware that caregivers may no longer be able to physically be in same space as their care recipients to assess symptoms and help with communication. If three-way virtual encounters (clinician, patient, caregiver all in separate physical places) are possible in these situations, they should be explored. Telehealth can enable patient symptom monitoring and can also be a valuable way to communicate with and guide family caregivers. (11), and caregiving advocacy organizations, including the Family Caregiver Alliance (13) and National Alliance for Caregiving (14) are continuing to compile COVID-19 specific guidance for caregivers from reputable organizations. Clinicians can use these sites as referral points for caregivers when appropriate. Local community organizations are also engaging vulnerable patients and their caregivers with resources such as meal delivery, resource navigation, and social support calls.",
            "cite_spans": [
                {
                    "start": 103,
                    "end": 106,
                    "text": "(9)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 747,
                    "end": 751,
                    "text": "(13)",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 789,
                    "end": 793,
                    "text": "(14)",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "How to best support family caregivers during COVID-19"
        },
        {
            "text": "Clinical practice guidelines for palliative and hospice care direct us to advocate for patients with serious illness and their families (15). The unprecedented growth in numbers and needs of older adults with serious illness has already necessitated widespread changes in the healthcare system to support caregiving families. The COVID-19 pandemic brings to light how reliant we are on families to support each other during illness. The multi-tasking, uncertainty, and strain that many feel now is emblematic of the pre-existing situation that many caregivers have faced for years. As we share in the public health urgency to contain COVID-19 and care for the most vulnerable, at-risk populations, we must not forget the frontline family caregivers. The care of the seriously ill depends on them, at times of crisis like now, and always.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "National Academies of Sciences Engineering and Medicine. Familes caring for an aging America",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Loneliness and social isolation as risk factors for mortality: a meta-analytic review",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Holt-Lunstad",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "B"
                    ],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Baker",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Harris",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Stephenson",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Perspect Psychol Sci",
            "volume": "10",
            "issn": "",
            "pages": "227--264",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Changes in the Place of Death in the United States",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "H"
                    ],
                    "last": "Cross",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "J"
                    ],
                    "last": "Warraich",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "N Engl J Med",
            "volume": "381",
            "issn": "",
            "pages": "2369--2370",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Questions About Personal Protective Equipment (PPE)",
            "authors": [
                {
                    "first": "U",
                    "middle": [
                        "S"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Interim Guidance for Implementing Home Care of People Not Requiring Hospitalization for Coronavirus Disease 2019 (COVID-19)",
            "authors": [],
            "year": 2020,
            "venue": "Centers for Disease Control and Prevention (CDC)",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "The Zarit Burden Interview: a new short version and screening version",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bedard",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "W"
                    ],
                    "last": "Molloy",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Squire",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Gerontologist",
            "volume": "41",
            "issn": "",
            "pages": "652--659",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Feasibility and Acceptability of Distress Screening for Family Caregivers at a Cancer Surgery Center",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "M"
                    ],
                    "last": "Shaffer",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Benvengo",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "K"
                    ],
                    "last": "Zaleta",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Oncol Nurs Forum",
            "volume": "46",
            "issn": "",
            "pages": "159--169",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "A systematic review of telehealth tools and interventions to support family caregivers",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "C"
                    ],
                    "last": "Chi",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Demiris",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Telemed Telecare",
            "volume": "21",
            "issn": "",
            "pages": "37--44",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "COVID-Ready Communication Skills: A playbook of VitalTalk Tips",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Vitaltalk",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Center to Advance Palliative Care. CAPC COVID-19 Response Resources. 2020",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Dyadic Interventions for Cancer Survivors and Caregivers: State of the Science and New Directions",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Badr",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Bakhshaie",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Chhabria",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Semin Oncol Nurs",
            "volume": "35",
            "issn": "",
            "pages": "337--341",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Family Caregiving Alliance. Coronavirus (COVID-19) Resources and Articles for Family Caregivers",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "National Consensus Project for Quality Palliative Care. Clinical Practice Guidelines for Quality Palliative Care",
            "authors": [],
            "year": 2018,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "Emphasis should be placed on what can be done, not just on what cannot, to encourage feelings of empowerment and activation for caregivers and their loved ones during this crisis (12). 6. Familiarize and check in weekly on guidance from major caregiving organizations on COVID-19 resources. The Center to Advance Palliative Care (CAPC) has detailed response resources for palliative care clinicians during COVID-19",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}