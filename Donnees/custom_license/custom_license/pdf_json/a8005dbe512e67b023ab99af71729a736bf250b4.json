{
    "paper_id": "a8005dbe512e67b023ab99af71729a736bf250b4",
    "metadata": {
        "title": "Comment",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Emergency efforts are underway to find optimum medical products to prevent infection and diagnose and treat patients during the coronavirus disease 2019 (COVID-19) pandemic. Production and supply chains for COVID-19 candidate drugs (such as chloroquine and hydroxychloroquine), and for many other essential medical products, are being impaired by this crisis. 1 Supply chains for vital drugs for other diseases (such as systemic lupus erythematosus) are being disrupted because they are being repurposed to use against COVID-19, without adequate supporting evidence.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "COVID-19 and risks to the supply and quality of tests, drugs, and vaccines"
        },
        {
            "text": "Without preparation for the quality assurance of diagnostic tests, drugs, and vaccines, the world risks a parallel pandemic of substandard and falsified products. Interventions are needed globally to ensure access to safe, quality assured, and effective medical products on which the world's population will depend.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "COVID-19 and risks to the supply and quality of tests, drugs, and vaccines"
        },
        {
            "text": "History provides us with warnings. Quackery was rampant during the Great Plague of the 17th century. When cinchona bark became the treatment for malaria in the 17th century, it was adulterated on a vast scale. After World War 2, penicillin shortages led to widespread falsification. 2 Substandard drugs (because of production or supply chain errors) are driven by cost reduction, whereas falsified agents (because of fraud) thrive on shortages, particularly when buyers depart from regulated supply chains. 3 The COVID-19 pandemic threatens a global surge in substandard and falsified medical products, not just for those directly related to COVID-19. Many products essential for COVID-19 treatment and prevention are at risk, including face masks, hand sanitiser, and diagnostic tests, and false claims have been made for prevention and treatment. 4 Many falsehoods proliferate through illegal websites and social media, 5 and these occurrences will mushroom. Poorly substantiated claims about effectiveness of drugs for treating COVID-19 have led to widespread shortages of chloroquine and hydroxychloroquine and to fatal overdoses. 6 Panicked global populations are desperate to procure products that might prevent and treat COVID-19. When chloroquine was used for malaria treatment, falsified versions were common. 7 Paracetamol is at risk; in the past, nephrotoxic substandard and falsified paracetamol syrup caused hundreds of deaths. 8 The Medicine Quality Monitoring Globe scours the internet for reports of substandard and falsified medical products in many languages, giving the general public early warnings of drug quality problems.",
            "cite_spans": [
                {
                    "start": 283,
                    "end": 284,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 507,
                    "end": 508,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 849,
                    "end": 850,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 922,
                    "end": 923,
                    "text": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1135,
                    "end": 1136,
                    "text": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1319,
                    "end": 1320,
                    "text": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1441,
                    "end": 1442,
                    "text": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19 and risks to the supply and quality of tests, drugs, and vaccines"
        },
        {
            "text": "Multiple diagnostic, therapeutic, and preventive interventions for COVID-19 are being trialed. 9 If products prove to be efficacious against COVID-19, achieving global benefit will require prompt access for all people in need. Drugs must be affordable, quality assured, and not hoarded or diverted from treatment of malaria, autoimmune diseases, or HIV/AIDS. Ineffective interventions, wasting resources, and causing harm should be opposed by robust policies and community-specific public engagement. We need to plan strategically to ensure global manufacture, access, protection, and monitoring of supply chains in the face of unescapable shortages, cost increases, and national hoarding. All our fates are bound together, and any helpful products must be recognised as global assets. The effect on access to other products (eg, HIV diagnostics) must be minimised.",
            "cite_spans": [
                {
                    "start": 95,
                    "end": 96,
                    "text": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19 and risks to the supply and quality of tests, drugs, and vaccines"
        },
        {
            "text": "Coordinated information-sharing among global medicines regulators on authorisations for clinical trials, Monitored Emergency Use of Unregistered and Investigational Interventions, and off-label use, as well as comprehensive and rapid reporting of shortages of active ingredients and finished products by industry and regulators, are essential to optimise global demand and supply. With in-person inspections suspended by many regulators, greater use of reliance mechanisms and full information-sharing among regulators is vital. 10 Effective regulatory supervision, emergency prequalification, robust authentication measures, and procurement policies supporting quality, with abjuring of national export restriction policies, the informal market, and illegal online websites, combined with trusted public engagement campaigns, will be needed to reduce substandard and falsified medical products.",
            "cite_spans": [
                {
                    "start": 529,
                    "end": 531,
                    "text": "10",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19 and risks to the supply and quality of tests, drugs, and vaccines"
        },
        {
            "text": "Few nations have medicine regulatory authorities classed by WHO as well functioning and integrated regulatory systems, rendering most populations For the Medicine Quality Monitoring Globe see https:// www.iddo.org/medicine-qualitymonitoring-globe especially vulnerable to substandard and falsified medical products. Innovative regional mechanisms (eg, the African Vaccine Regulatory Forum) might be part of the solution in this urgency. As efficacious COVID-19 treatments and vaccines are approved, intense global coordinated production, distribution chains, and postmarket surveillance will be needed to protect the general public from manufacturing and supply chain failures, inadequate manufacturing protocols, and criminals selling falsified products. 11 Robust evaluation of diagnostics tests (premarket and postmarket) to ensure accuracy will be vital; bad tests will be worse than no tests.",
            "cite_spans": [
                {
                    "start": 756,
                    "end": 758,
                    "text": "11",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19 and risks to the supply and quality of tests, drugs, and vaccines"
        },
        {
            "text": "If a drug is shown to be efficacious, devices able to detect whether the product contains the stated amount of active ingredient with appropriate dissolution will be important in supporting postmarket surveillance. Many portable screening devices are available but with scant evidence for their effectiveness. Few data exist to show which agents these devices can detect; none has yet been shown to accurately quantify diverse active ingredients. 12 These devices will need to be integrated into national regulatory standards and WHO's Prevent, Detect and Respond frameworks, using public pharmacopeial standards. 9 Drug quality is vulnerable to fear, desperation, and disinformation. While hoping that the efforts of WHO and global coalitions to accelerate COVID-19 research will provide the means to fight this pandemic, we must ensure that access to affordable quality medical products, particularly in low-resource settings, does not become another casualty.",
            "cite_spans": [
                {
                    "start": 447,
                    "end": 449,
                    "text": "12",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 614,
                    "end": 615,
                    "text": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19 and risks to the supply and quality of tests, drugs, and vaccines"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The consequence of COVID-19 on the global supply of medical products: why Indian generics matter for the world",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Guerin",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Singh-Phulgenda",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Strub-Wourgaft",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Fake penicillin, the Third Man, and Operation Claptrap",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "N"
                    ],
                    "last": "Newton",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Timmerman",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "BMJ",
            "volume": "355",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Identifying market risk for substandard and falsified medicines: an analytic framework based on qualitative research in China, Indonesia, Turkey and Romania",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Pisani",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "L"
                    ],
                    "last": "Nistor",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Hasnida",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Wellcome Open Res",
            "volume": "4",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Medical Product Alert N\u00b03/2020: falsified medical products, including in vitro diagnostics, that claim to prevent, detect, treat or cure COVID-19",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Justice Department files its first enforcement action against COVID-19 fraud",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Nigeria reports chloroquine poisonings as Trump keeps pushing drug against coronavirus",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Politi",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Counterfeit anti-infective medicines",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "N"
                    ],
                    "last": "Newton",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Green",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "M"
                    ],
                    "last": "Fern\u00e1ndez",
                    "suffix": ""
                },
                {
                    "first": "Njp",
                    "middle": [],
                    "last": "Day",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "J"
                    ],
                    "last": "White",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Lancet Infect Dis",
            "volume": "6",
            "issn": "",
            "pages": "602--615",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Medication-associated diethylene glycol mass poisoning: a review and discussion on the origin of contamination",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Schier",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Rubin",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Miller",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "J Public Health Policy",
            "volume": "30",
            "issn": "",
            "pages": "127--170",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "A living systematic review protocol for COVID-19 clinical trial registrations",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "J"
                    ],
                    "last": "Maguire",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Gu\u00e9rin",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Wellcome Open Res",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.12688/wellcomeopenres.15821.1"
                ]
            }
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "For medicines regulators, reliance is a global imperative",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Bond",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Huntley-Fenner",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "R\u00e4go",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "World Health Organization. WHO global surveillance and monitoring system for substandard and falsified medical products",
            "authors": [],
            "year": 2017,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Field detection devices for medicines quality screening: a systematic review",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Vickers",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bernier",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zambrzycki",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "M"
                    ],
                    "last": "Fern\u00e1ndez",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "N"
                    ],
                    "last": "Newton",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Caillet",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "BMJ Glob Health",
            "volume": "3",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}