{
    "paper_id": "03baa0ff09213ce5a8ef3da6881998267938e39c",
    "metadata": {
        "title": "The potential of recurrent epidemics and pandemics in a highly mobile global society",
        "authors": [
            {
                "first": "Sabrina",
                "middle": [],
                "last": "Daddar",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "\u2022",
                "middle": [
                    "N"
                ],
                "last": "Nirupama",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "The aim of this study was to demonstrate how latent epidemics can potentially evolve into a pandemic instantaneously due to globally mobile human population in recent times, as can be seen in the ongoing Ebola epidemic in West Africa. Selected cases of current epidemics are used in this study to identify emergent patterns. These cases exemplify the need for a comprehensive analysis of infectious diseases and serve as an initial stage when developing appropriate strategies in improving epidemic management. This study can help better understand the complexities of infectious diseases and assist in developing a specific set of preventative processes from the individual to international levels when developing strategies to reducing the effects of an epidemic outbreak.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Humans have been plagued for as long as we have been in existence with seemingly frequent infectious diseases shown in Table 1 , featuring some notable epidemics and pandemics throughout our history.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 119,
                    "end": 126,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "Introduction"
        },
        {
            "text": "This research highlights the patterns of infectious diseases in two case studies, namely cholera in India and meningococcal meningitis (meningitis) in the Democratic Republic of the Congo (DRC), in order to explore the effects of how devastating epidemics are in developing countries, as well as how they fluctuate over time. The two cases exemplify the need for analyzing infectious disease data and understanding epidemic evolution, and serve as an initial point of reference for creating strategies and suggestions for improving these aspects in communities. Economic costs are atypical in the onset, beginning, and mid-stages of an epidemic occurrence typically as other physical hazards do; thus, resulting in a distinctive pattern of effects and overall spread.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "There are numerous ways in which infectious diseases can be caused and transmitted. According to the Mayo Clinic (2014a), there are four primary causes (agents) for infectious diseases defined and illustrated here: Bacteria. These one-cell organisms are responsible for illnesses such as strep throat, urinary tract infections and tuberculosis; Viruses. Even smaller than bacteria, viruses cause a multitude of diseases-ranging from the common cold to AIDS; Fungi. Many skin diseases, such as ringworm and athlete's foot, are caused by fungi. Other types of fungi can infect lungs or nervous system; and Parasites. A tiny parasite that is transmitted by a mosquito bite causes malaria. Other parasites may be transmitted to humans from animal feces.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Causes and transmission of infectious diseases"
        },
        {
            "text": "These infectious agents can enter the body through: skin contact (e.g., surface, sexual), inhalation, ingestion, and through insect bites. There are four primary mechanisms of transmission: (1) direct contact, (2) indirect contact, (3) insect bites, and iv) food contamination (Mayo Clinic 2014b).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Causes and transmission of infectious diseases"
        },
        {
            "text": "Direct Contact can occur through coming in contact with a person or animal who has the infection, which is the most common transmission. It can occur in three different ways, including person to person, through the exchange of body fluids from sexual contact or blood transfusion, and from the affected person being a carrier without showing obvious symptoms. Animal-to-person infection can occur when handling animals or animal waste and being bitten or scratched by infected animal (Mayo Foundation for Medical Education . The infectious diseases that are transmissible from vertebrate animals to humans and vice versa are classified as zoonosis (WHO 2014a). Infection from the mother to her unborn child, vaginally or through the placenta is also possible.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Causes and transmission of infectious diseases"
        },
        {
            "text": "The most commonly known epidemics and pandemics worldwide are avian influenza, cholera, HIV/AIDS, influenza-like illness, malaria, meningococcal meningitis, SARS, and Ebola (a recent addition). In most cases, the accurate caseload numbers are difficult to determine since many of these infectious diseases are already endemic to developing countries (Infoplease 2007a) . Nearly half of all annual deaths caused by infectious diseases can be attributed to three diseases in particular: AIDS, malaria, and tuberculosis. Combined, they cause over 300 million illnesses annually, including more than 5 million deaths. HIV, the human immunodeficiency virus consists of two types, HIV-1 and HIV-2, where either of the two closely related retroviruses that invade T-helper lymphocytes and are responsible for acquired immune deficiency syndrome (AIDS) . HIV-1 is considered to be responsible for the vast majority of AIDS cases in the USA, whereas HIV-2 is often seen in western Africa and has a slower course than HIV-1. Since there are numerous strains of both types, the virus can mutate rapidly making it challenging to find successful treatments or vaccines. In general, a person's immune system can fight off HIV for many years, especially keeping up with the HIV mutations before it allows obvious signs of AIDS to develop, and lead to death. People infected in advanced stages of AIDS can have symptoms of persistent coughs, fever, and difficulty in breathing, and the virus can also cause brain damage to affected individuals.",
            "cite_spans": [
                {
                    "start": 350,
                    "end": 368,
                    "text": "(Infoplease 2007a)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Common types of infectious diseases"
        },
        {
            "text": "Malaria was almost eliminated about 30 years ago, but now the mosquito-borne disease is found in about 90 countries affecting about 300-500 million people annually. In 2012, the World Health Organization (WHO 2014b) reported an estimated 627,000 deaths worldwide, of which most were children under the age of 5 in sub-Saharan Africa. Malaria consists of three stages once infected: (1) chills and shaking, (2) severe headache and fever, and (3) drop of the temperature and profuse sweats. People affected with malaria can also suffer from anemia, weakness, and a swelling of the spleen.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Common types of infectious diseases"
        },
        {
            "text": "In recent history, avian influenza, commonly known as bird flu is a contagious disease caused by viruses that typically infect only birds and are highly species specific, but have now crossed the species barrier to infect human populations (Hall et al. 2007 ). Avian influenza A (H5N1) virus caused outbreaks that occurred in poultry within Asia from 2003 to 2004 with over 100 million birds that died or were killed from the disease (Infoplease 2007b) . New outbreaks were reported in June 2004, and since then, the virus has geographically spread throughout Europe and Africa. Human affected cases have been reported in ten other countries mostly due to contact with infected poultry or contaminated surfaces of the virus. Currently, person-to-person spread of this infection is rare, but very much possible due to lack of protection from the virus.",
            "cite_spans": [
                {
                    "start": 240,
                    "end": 257,
                    "text": "(Hall et al. 2007",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 434,
                    "end": 452,
                    "text": "(Infoplease 2007b)",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Common types of infectious diseases"
        },
        {
            "text": "Influenza-like illness is an infectious virus that attacks the human respiratory tract and causes symptoms such as fever, nasal congestion, headaches, fatigue, coughing, sore throat, and body aches (Infoplease 2007a ). This epidemic is prevalent throughout human history and causes millions of deaths worldwide. It triggered the worst epidemic in American history, involving H1N1 influenza virus, killing over 50 million people during 1918 -1920 (Wikipedia 2014a . Center for Disaster Control and Prevention (CDC 2014) estimates that from the 1976-1977 season to the 2006-2007 flu season, flu-associated deaths ranged from a low of about 3,000 to a high of about 49,000 people. Death certificate data and weekly influenza virus surveillance information were used to estimate how many flu-related deaths occurred among people whose underlying cause of death was listed as respiratory or circulatory disease on their death certificate.",
            "cite_spans": [
                {
                    "start": 198,
                    "end": 215,
                    "text": "(Infoplease 2007a",
                    "ref_id": null
                },
                {
                    "start": 435,
                    "end": 439,
                    "text": "1918",
                    "ref_id": null
                },
                {
                    "start": 440,
                    "end": 445,
                    "text": "-1920",
                    "ref_id": null
                },
                {
                    "start": 446,
                    "end": 462,
                    "text": "(Wikipedia 2014a",
                    "ref_id": null
                },
                {
                    "start": 508,
                    "end": 518,
                    "text": "(CDC 2014)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Common types of infectious diseases"
        },
        {
            "text": "Cholera is caused by the bacterium Vibrio cholera in the intestines. The symptoms include diarrhea and vomiting, resulting in acute dehydration and causing death. It is an infectious disease that spreads mostly through unsanitary conditions that contaminate drinking water. This endemic is widespread in India, Russia, and sub-Saharan Africa, with about 200,000 annual cases reported to the WHO.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Common types of infectious diseases"
        },
        {
            "text": "Meningococcal meningitis (meningitis) disease is a contagious bacterial disease caused by the meningococcus. It is spread by person-to-person contact through respiratory droplets of infected people. There are three main clinical forms of the disease: the meningeal syndrome, the septic form, and pneumonia. The onset of symptoms is sudden, and death can follow within hours. In as many as 10-15 % of survivors, there are persistent neurological defects, including hearing loss, speech disorders, loss of limbs, mental retardation, and paralysis (WHO 2014c). Typical symptoms of meningitis include fever, severe headache, vomiting, delirium, photophobia, and stiffness in the neck.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Common types of infectious diseases"
        },
        {
            "text": "Severe acute respiratory syndrome (SARS) is a newly identified acute viral respiratory syndrome caused by a novel coronavirus, the SARS coronavirus (SARS-CoV) which is believed to have crossed the species barrier recently from animals to humans (Guan et al. 2003; Zhong et al. 2003; Antia et al. 2003; WHO 2014d) . SARS was recognized at the end of February 2003 as a communicable viral disease that can develop into a potentially fatal pneumonia. The primary symptoms usually consisting of headaches, high fever, body aches, diarrhea, sore throat, and other minor respiratory symptoms PHO 2014) . It spreads through airborne droplets from the affected individual. The spread is controlled by isolating infected patients and quarantining those exposed to them. Two to seven days after the onset of the primary symptoms, a dry cough and shortness of breath can develop, where most affected individuals develop a type of pneumonia in the lungs. The death rate for SARS is higher for older persons, and currently, there is no known vaccine or treatment for the virus that causes this infectious disease.",
            "cite_spans": [
                {
                    "start": 245,
                    "end": 263,
                    "text": "(Guan et al. 2003;",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 264,
                    "end": 282,
                    "text": "Zhong et al. 2003;",
                    "ref_id": "BIBREF21"
                },
                {
                    "start": 283,
                    "end": 301,
                    "text": "Antia et al. 2003;",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 302,
                    "end": 312,
                    "text": "WHO 2014d)",
                    "ref_id": null
                },
                {
                    "start": 586,
                    "end": 595,
                    "text": "PHO 2014)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Common types of infectious diseases"
        },
        {
            "text": "Stochastic and deterministic models prove valuable when studying patterns of infectious diseases at a larger population scale. Stochastic models are based on chance variation in risk of exposure among other factors and allow for the continuation of understanding of the effects for each individual in the population. The model is too laborious to develop and requires many ongoing simulations to produce useful forecasts for infectious diseases (Trottier and Philippe 2001) . Also, it does not explain the dynamics of its variables in relation to other factors due to mathematical complexity involved.",
            "cite_spans": [
                {
                    "start": 445,
                    "end": 473,
                    "text": "(Trottier and Philippe 2001)",
                    "ref_id": "BIBREF15"
                }
            ],
            "ref_spans": [],
            "section": "Types of models used for epidemics and pandemics modeling"
        },
        {
            "text": "Deterministic models allow for better understanding of larger populations by allocating individuals into different subgroups (Trottier and Philippe 2001) . These models do not account for longer period events, such as plague epidemics, disease extinctions, and reemerging cases (Earn 2008) .",
            "cite_spans": [
                {
                    "start": 125,
                    "end": 153,
                    "text": "(Trottier and Philippe 2001)",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 278,
                    "end": 289,
                    "text": "(Earn 2008)",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Types of models used for epidemics and pandemics modeling"
        },
        {
            "text": "SIR models are the most common in epidemic modeling since they are more simplistic in understanding the relationships of how host populations are divided into a small number of subgroups of similar (in terms of their status regarding the disease in question) individuals (Earn 2008) . The three main compartments in a SIR model are (Earn 2008) : Susceptible: individuals who have no immunity to the infectious agent, so might become infected if exposed; Infectious: individuals who are currently infected and can transmit the infection to susceptible individuals who they contact; and Removed: individuals who are immune to the infection, and consequently do not affect the transmission dynamics in any way when they contact other individuals. These three compartments consists of three classes, namely the number of individuals not yet infected with the disease at time t but susceptible, the number of individuals who have been infected and are capable of spreading the disease to those who are susceptible, and individuals who have been infected and then removed from the disease, either due to immunization or due to death-they cannot infect again (Wikipedia 2014b) .",
            "cite_spans": [
                {
                    "start": 271,
                    "end": 282,
                    "text": "(Earn 2008)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 332,
                    "end": 343,
                    "text": "(Earn 2008)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1152,
                    "end": 1169,
                    "text": "(Wikipedia 2014b)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Types of models used for epidemics and pandemics modeling"
        },
        {
            "text": "Real-time modeling, as a variant of traditional deterministic models, is the most commonly used method for epidemic prediction. This type of model is capable of providing the justification as to why certain epidemics do not reach a pandemic stage and mass outbreak levels. Real-time forecasting model allows one to calculate the early stages of a future outbreak showing the timing of the illness, with its affects and the time that it persists in a country comparable with surveillance systems. This variation takes on the assumption that an influenza case is latent for 2 days, infectious for 2.5 days, with viral shedding starting 1 day before symptoms develop in symptomatic cases (Hall et al. 2007 ). For the application of this model, there are nine specified parameters, including size and frequency, duration of time (i.e., 2 days, 2.5 days, and 1 day), growth rate, and the ratio of cases reported for the dataset. These parameters are used to create the relationship curves of comparing and estimating the effects of an epidemic.",
            "cite_spans": [
                {
                    "start": 685,
                    "end": 702,
                    "text": "(Hall et al. 2007",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Types of models used for epidemics and pandemics modeling"
        },
        {
            "text": "The two case studies analyzed in this paper are cholera in India and meningococcal meningitis (referred to as meningitis) in the DRC. These cases were selected because the available WHO data sets were the most complete, spanning at least 30 years. The statistics for cases were investigated to understand the effects of how devastating epidemics can be in developing countries. The analyses demonstrate the patterns of fluctuation over a period of time, considering the possibility for problematic data source collection. Moreover, the cases of cholera and meningitis include descriptive and linear statistical analyses in order to comprehend the need for analyzing infectious diseases data, epidemic evolution, and initial suggestions for improving these aspects in developing countries (Fig. 1) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 788,
                    "end": 796,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Case studies"
        },
        {
            "text": "In India, during the 30-year period of 1982-2010, the total number of reported cholera cases was 164,256, out of which the reported number of deaths was 2,453. The 30 years of data collection is statistically significant for drawing linear regression as shown in Figs. 2 and 3. Figure 2 shows a linear regression between the number of reported cholera cases and the deaths during the 30-year period. Figure 3 illustrates a linear regression between the numbers of cases reported versus the deaths.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 278,
                    "end": 286,
                    "text": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 400,
                    "end": 408,
                    "text": "Figure 3",
                    "ref_id": "FIGREF2"
                }
            ],
            "section": "Cholera"
        },
        {
            "text": "Every year, there are about 1.2 million cases of bacterial meningitis that occur globally; over a tenth of which are fatal (MRF 2014). There has been a remarkable 244.15 % increase in the DRC's human population from 1980 to 2010 (World Bank 2013). Here, the relationship between the number of suspected meningitis cases, the number of suspected meningitis deaths, and the number of epidemic districts from 1980 to 2010 have been 30-year (1980-2010) data for cholera cases and deaths reported according to World Health Organization examined for the DRC using the data from WHO (2005) . Four statistical tests were performed to identify patterns in the available meningitis data as illustrated in Figs. 4 and 5. The number of meningitis cases reported was 95,826, and the resulting deaths were 11,834. in India between the number of suspected cholera cases and deaths reported according to WHO (2014a) The present economic and industrial setup in the world is referred to as the global village, and as a consequence, people are highly mobile not only in their surrounding but across the entire globe. While there are several positive aspects to this highly mobile society, there are also some negative features, among which probably the most serious issue is the recurrence of epidemics and pandemics. This fact is demonstrated by the spread of AIDS, SARS, and the recent Ebola pandemic. In this study, we examined two particular epidemics: cholera in India and meningitis in DRC. For these studies, the data record is 30 years long from 1980 to 2010. While the length of data record is not entirely ideal from statistical purposes, nevertheless it is long enough to provide some useful regression relations.",
            "cite_spans": [
                {
                    "start": 429,
                    "end": 448,
                    "text": "30-year (1980-2010)",
                    "ref_id": null
                },
                {
                    "start": 572,
                    "end": 582,
                    "text": "WHO (2005)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Meningococcal meningitis"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The role of evolution in the emergence of infectious diseases",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Antia",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "R"
                    ],
                    "last": "Rogoes",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "Koella",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "T"
                    ],
                    "last": "Bergstrom",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Nature",
            "volume": "426",
            "issn": "",
            "pages": "658--661",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "A light introduction to modelling recurrent epidemics",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Earn",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "3--8",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Isolation and characterization of viruses related to the SARS coronavirus from animals in southern China",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "J"
                    ],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "Q"
                    ],
                    "last": "He",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [
                        "L"
                    ],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "L"
                    ],
                    "last": "Zhuang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "W"
                    ],
                    "last": "Cheung",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Science",
            "volume": "302",
            "issn": "",
            "pages": "276--278",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Real-time epidemic forecasting for pandemic influenza",
            "authors": [
                {
                    "first": "I",
                    "middle": [
                        "M"
                    ],
                    "last": "Hall",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Gani",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "E"
                    ],
                    "last": "Hughes",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Leach",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Epidemiol Infect",
            "volume": "135",
            "issn": "3",
            "pages": "372--384",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Linear regression plotted for the 30-year dataset (1980-2010) in the DRC between the number of suspected meningitis cases and deaths reported according to WHO",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "An overview of Avian flu",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Infoplease",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Columbia electronic encyclopedia 6th",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Infoplease",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Mayo Foundation (2015) Diseases and conditions. Mayo Foundation for Medical Education and Research",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "What is a pandemic? What is an epidemic?",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Nordqvist",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Medical News Today. MediLexicon International. Accessed",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Deterministic modeling of infectious diseases: theory and methods",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Trottier",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Philippe",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Internet J Infect Dis",
            "volume": "1",
            "issn": "2",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "Avian influenza frequently asked questions, epidemic and pandemic alert and response (EPR)",
            "authors": [],
            "year": 2005,
            "venue": "WHO",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF20": {
            "ref_id": "b20",
            "title": "The World Bank group. Data: population, total. Eurostat: demographic statistics, Secretariat of the Pacific Community",
            "authors": [
                {
                    "first": "World",
                    "middle": [],
                    "last": "Bank",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF21": {
            "ref_id": "b21",
            "title": "Epidemiology and cause of severe acute respiratory syndrome (SARS) in Guangdong, People's Republic of China",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "S"
                    ],
                    "last": "Zhong",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "J"
                    ],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "M"
                    ],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Llm",
                    "middle": [],
                    "last": "Poon",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "H"
                    ],
                    "last": "Xie",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "H"
                    ],
                    "last": "Chan",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "362",
            "issn": "9393",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Types of infectious agents. Bacteria and protozoa are microscopic one-celled organisms, while viruses are even smaller. Fungi grow like plants, and helminthes resemble worms (Mayo Foundation for Medical Education and Research 2015)",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Linear regression plotted for the",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Linear regression plotted for the 30-year dataset",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "DRC meningitis case-linear regression between the number of suspected meningitis cases, deaths, and epidemic districts reported during 1980-2010 according to WHO (",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}