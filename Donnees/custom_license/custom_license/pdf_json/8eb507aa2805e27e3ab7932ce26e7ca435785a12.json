{
    "paper_id": "8eb507aa2805e27e3ab7932ce26e7ca435785a12",
    "metadata": {
        "title": "COVID-19 in Children: More than meets the eye",
        "authors": [
            {
                "first": "Stefan",
                "middle": [
                    "H F"
                ],
                "last": "Hagmann",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Northwell Health",
                    "location": {
                        "addrLine": "New Hyde Park",
                        "settlement": "New York",
                        "country": "United States"
                    }
                },
                "email": "shagmann@northwell.edu"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The novel Severe Acute Respiratory Syndrome Coronavirus 2 (SARS-CoV-2) has so far resulted in more than 300,000 reported confirmed cases of Coronavirus virus disease 2019 and about 15,000 deaths. Today's very high degree of international interconnectedness and mobility has favored the truly rapid global spread of this novel virus as COVID-19 cases have been so far reported from almost every country on earth (190 out of 195 countries recognized by the United Nations) [1] . Severe respiratory illness and acute respiratory distress syndrome (ARDS), mostly observed in older adults, that have in many instances in several countries overloaded hospital capacities have so far dominated the media reports and the clinical literature on COVID-19 [2] .",
            "cite_spans": [
                {
                    "start": 471,
                    "end": 474,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 745,
                    "end": 748,
                    "text": "[2]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Infections with SARS-CoV-2 in children were indeed absent or rarely noted in the early reports, and the associated morbidity in COVID-19 was deemed as mostly mild [3] . In particular in comparison to adults, children with COVID-19 were deemed to have milder illness and a better prognosis [4] . Moreover, vertical transmission of SARS-CoV-2 has so far not been documented, and disease in newborns has been considered as occurring only very rarely [5, 6, 7] .",
            "cite_spans": [
                {
                    "start": 163,
                    "end": 166,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 289,
                    "end": 292,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 447,
                    "end": 450,
                    "text": "[5,",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 451,
                    "end": 453,
                    "text": "6,",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 454,
                    "end": 456,
                    "text": "7]",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "As a result, it was even wondered if children might be less susceptible to this novel infectious disease [8] . This first glance at COVID-19 in children is of course reassuring on the surface especially for those providing health care for children, yet it begs the question if there is more to see and to learn about pediatric COVID-19 associated clinical disease and the epidemiology of",
            "cite_spans": [
                {
                    "start": 105,
                    "end": 108,
                    "text": "[8]",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Forthcoming literature with more detailed data on pediatric morbidity during the progressing and accelerating COVID-19 pandemic allow now for a more nuanced picture [9, 10] .",
            "cite_spans": [
                {
                    "start": 165,
                    "end": 168,
                    "text": "[9,",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 169,
                    "end": 172,
                    "text": "10]",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "SARS-CoV-2 infections in children?"
        },
        {
            "text": "In the hitherto largest pediatric COVID-19 study that analyzed 2143 children with laboratory-3 confirmed or suspected cases most pediatric patients (94.1%) were diagnosed as asymptomatic, or with mild or moderate disease [10] . In fact, 13% of laboratory-confirmed cases were asymptomatic which is notable as this is certainly an underestimation of the true rate of asymptomatic infection, since many children without symptoms are unlikely to be tested for obvious reasons. Recent virologic data demonstrated that such asymptomatic or oligosymptomatic individuals carry potentially infectious SARS-CoV-2 particles in their nasopharyngeal secretions thereby very likely contributing to early transmission to close contacts [11, 12] . In addition, the evidence of fecal shedding of SARS-CoV-2 in the stool of an asymptomatic child for a prolonged period raises the concern that infants and children who are not toilet-trained could facilitate fecal-oral transmission of the virus [13] . The potentially prolonged shedding of the virus in nasal secretions and stool of children and infants has substantial implications for spread of the virus in daycare centers, schools, and in the home.",
            "cite_spans": [
                {
                    "start": 221,
                    "end": 225,
                    "text": "[10]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 722,
                    "end": 726,
                    "text": "[11,",
                    "ref_id": null
                },
                {
                    "start": 727,
                    "end": 730,
                    "text": "12]",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 978,
                    "end": 982,
                    "text": "[13]",
                    "ref_id": "BIBREF14"
                }
            ],
            "ref_spans": [],
            "section": "SARS-CoV-2 infections in children?"
        },
        {
            "text": "Consequently, children and infants may play a pivotal role in community-based transmission of SARS-CoV-2.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "SARS-CoV-2 infections in children?"
        },
        {
            "text": "Severe pediatric COVID-19 infections are seemingly rare, in this so-far largest pediatric COVID-19 analysis less than 1 in 10 children diagnosed with COVID-19 developed a severe and critical illness and only one child died [10] . However, it has recently been questioned if such numbers reflect the real burden of severe COVID-19 disease in children [14] . Using the ratio of COVID-19 to influenza cases identified in an early retrospective surveillance study in China [3], it was estimated that the actual number of severe pediatric cases and mildly infected children may have been significantly larger (by a factor of several hundreds and several thousands, respectively) compared to what had been registered suggesting that the force of infection from children may have been grossly underestimated [14] . In regards of clinical impact, the available 4 data suggest that the proportion of severe and critical cases seems to be inversely related to age suggesting that young children, and in particular infants and pre-school children, could be more vulnerable to COVID-19 related morbidity [10] . Thus, clinicians caring for children should be wary of subgroups of children who can be at an increased risk for more significant illness, as particularly younger age, underlying pulmonary pathology, and many immunocompromising conditions have also been associated with more severe outcomes with other coronavirus infections in children [15] .",
            "cite_spans": [
                {
                    "start": 223,
                    "end": 227,
                    "text": "[10]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 350,
                    "end": 354,
                    "text": "[14]",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 801,
                    "end": 805,
                    "text": "[14]",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 1092,
                    "end": 1096,
                    "text": "[10]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1436,
                    "end": 1440,
                    "text": "[15]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "SARS-CoV-2 infections in children?"
        },
        {
            "text": "Much has been learnt about SARS-CoV-2 and COVID-19 in a very short time, yet there is still much that we need to learn about the impact of this virus on children, as well as the impact of children on viral spread. While the focus during pandemics is frequently largely determined by the pandemic's impact on the individuals who utilize the highest resources or on the economically productive age groups, rigorously gauging the impact of COVID-19 on children will be important to accurately model the pandemic and to ensure that appropriate resources are allocated to children requiring care.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "SARS-CoV-2 infections in children?"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "World Health Organization (WHO), Coronavirus disease (COVID-19) Pandemic",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Hospitalized Patients With 2019 Novel Coronavirus-Infected Pneumonia in Wuhan, China",
            "authors": [],
            "year": 2020,
            "venue": "JAMA",
            "volume": "323",
            "issn": "11",
            "pages": "1061--1069",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Detection of Covid-19 in Children in Early",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Xiang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Systematic review of COVID-19 in children show milder cases and a better prognosis than adults",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "F"
                    ],
                    "last": "Ludvigsson",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Acta Paediatr",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1111/apa.15270"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Clinical characteristics and intrauterine vertical transmission potential of COVID-19 infection in nine pregnant women: a retrospective review of medical records",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Guo",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Luo",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "809--824",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Clinical analysis of 10 neonates born to mothers with 2019-nCoV pneumonia",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Fang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Peng",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Chang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Transl Pediatr",
            "volume": "9",
            "issn": "1",
            "pages": "51--60",
            "other_ids": {
                "DOI": [
                    "10.21037/tp.2020.02.06"
                ]
            }
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Coronavirus disease (COVID-19) and neonate: What neonatologist need to know",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Med Virol",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1002/jmv.25740"
                ]
            }
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Are children less susceptible to COVID-19?",
            "authors": [
                {
                    "first": "P-I",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "Y-L",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "P-Y",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "Y-C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "P-R",
                    "middle": [],
                    "last": "Hsueh",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Journal of Microbiol Immunol Infect",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jmii.2020.02.011"
                ]
            }
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "SARS-CoV-2 Infection in Children",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Du",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "Y"
                    ],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Qu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMc2005073"
                ]
            }
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Epidemiological characteristics of 2143 pediatric patients with 2019 coronavirus disease in China",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Dong",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Mo",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Qi",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Jiang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Jiang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Pediatrics",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1542/peds.2020-07026"
                ]
            }
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Virological assessment of hospitalized cases of coronavirus disease 2019",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1101/2020.03.05.20030502"
                ]
            }
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "SARS-CoV-2 Viral Load in Upper Respiratory Specimens of Infected Patients",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zou",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Ruan",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Liang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Hong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "",
            "pages": "1177--1179",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Detection of novel coronavirus by RT-PCR in stool specimen from asymptomatic child, China. Emerg Infect Dis",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Tang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "D"
                    ],
                    "last": "Tong",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "L"
                    ],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "X"
                    ],
                    "last": "Dai",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "N"
                    ],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "26",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.3201/eid2606.200301"
                ]
            }
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Hundreds of severe pediatric COVID-19 infections in Wuhan prior to the lockdown",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Du",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Nugent",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "J"
                    ],
                    "last": "Cowling",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "A"
                    ],
                    "last": "Meyers",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1101/2020.03.16.20037176"
                ]
            }
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Characteristics and outcomes of coronavirus infection in children: role of viral factors and an immunocompromised state",
            "authors": [],
            "year": 2019,
            "venue": "J Pediatr Infect Dis Soc",
            "volume": "8",
            "issn": "1",
            "pages": "21--28",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}