{
    "paper_id": "3ccc4f8ef7c92e873d76764ee5a1fbc0c26f8f7a",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Email: walgate@scienceanalysed.com",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "The outbreak of SARS (Severe Acute Respiratory Syndrome) that originated in China is, with \"95-97% certainty,\" caused by a completely new type of coronavirus, according to Julie Hall, who is responsible for the World Health Organization's Global Alert, Response and Operations Network.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "The new virus diverges by 50-60% from the three known groups of coronavirus, but that is typical of the variation between coronavirus groups, according to Stephan G\u00fcnther of the Bernhard Nocht Institute of Tropical Medicine in Hamburg. G\u00fcnther works with Christian Drosten, who along with researchers in several other laboratories has identified the coronavirus in SARS patients. We asked G\u00fcnther if it could be a previously unknown animal virus. \"The origin is completely unclear because no other coronavirus is closely related,\" he said.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "The coronavirus has the largest known genome of all RNA viruses, and frequently recombines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "Two other viruses, both paramyoxoviruses, seem also to be associated with the disease. One of these is a metapneumovirus -a subclass of the paramyxoviruses. Canadian scientists have identified the latter in five of six SARS patients. They also found coronavirus in five of six patients. It's thought the paramyxoviruses might represent opportunistic co-infections.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "Hall said it was possible the paramyxoviruses were producing the really severe symptoms of SARS; most people recover but 3-4% die. \"But these viruses are circulating all the time, particularly at this season. It may be background noise,\" she said.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "\"There are still one or two things needed to really prove the coronavirus is the key. We'd like to see this virus create the disease in an animal model. In patients, we'd also like to see the curves of IgM [specific to this virus], to show people have an acute response, and then IgG, which takes two to three weeks to level up, and stays with you, to show that it wasn't just a passing extraneous infection.\" Moreover \"We have lots of blood samples to test this in now,\" Hall said, so the results should not be long coming.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "James Hughes, director of the US Centers for Disease Control's National Center for Infectious Diseases, announced Wednesday (April 2) that CDC had developed fluorescent antibody and ELISA tests for antibodies to the coronavirus, and \"they look promising in that they appear to perform well in suspect cases, particularly a subset of those that are relatively more severe.\" Crucially, Hughes added \"We have looked for evidence of this antibody in roughly 400 sera collected recently from people in this country without any suggestive evidence of SARS, and they're negative in all of those people.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "The virus has been identified in several of the network of 12 laboratories investigating SARS for WHO, including at CDC -where, according to director Julie Gerberding, 200 scientists are at work on the problem -and at Drosten and G\u00fcnther's laboratory in Hamburg.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "G\u00fcnther said he and Drosten had isolated three short DNA sequences from the coronavirus in a tissue sample from the \"index case\" (first case) of SARS in Germany, by a random amplification, lowstringency PCR approach.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "They first showed that an agent from tissue samples from the index case could be grown in a Vero cell line (a laboratory line of monkey cells), and that it was cytotoxic. Then they applied PCR. G\u00fcnther explained \"PCR is usually very specific for the agent you want to target, but you can run it under conditions where it detects any genetic material. Then you get random fragments of nucleic acid, present in the cells. We got about 20 fragments of nucleic acid, and sequenced those. Most were from the Vero cells. But three turned out to be specific for the coronavirus.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "None of these were the same as the 300-nucleotide sequence obtained by CDC from another patient, said G\u00fcnther. But when they searched specifically for the CDC sequence, they found an exact match. Drosten and G\u00fcnther concluded this indicated an epidemiological link between the patients, although G\u00fcnther admitted to us that it could simply mean they'd hit a conserved or slowly varying region of a widespread virus.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "Hall said the medical importance of identifying the SARS agent was to create a diagnostic test. In addition to the CDC antibody tests, G\u00fcnther and Drosten have now created a protocol for a PCR diagnostic test, which will be posted on their website. But all these tests need validation before they can be used in patients, Hall said. PCR will be most important, as the antibody tests can only detect infection well into the course of the disease, \"The earliest antibody tests can work is day ten after the onset of symptoms,\" she said. \"But WHO is facilitating the sharing of all these techniques.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "The transmission routes of the disease remain unclear. In Hong Kong there is infection in a block of flats \"without clear clustering of people who are likely to have coughed on each other,\" said Hall. So there must be another route of transmission -perhaps via particles left on a solid surface, such as stair rails or lift doors, where coronavirus could survive for hours. The greatest fear is that it could be airborne, \"But it were airborne we'd have many, many more cases.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        },
        {
            "text": "A WHO team has now been admitted to mainland China, where the epidemic seems to have begun in Guandong province. Hall said the team left Beijing for Guandong Thursday. \"China has agreed to do reports of all the regions that have been affected. The reports will appear on the WHO website.\" But the greatest fear at WHO is for Africa and other poor regions. \"We're extremely worried. That was why we put out the global alert,\" Hall said. \"People say: 'Only a 4% mortality rate, so few people have died, why worry?' But as we saw in Vietnam, when it gets into a hospital, within a week 50% of the health care staff are sick. Then it starts to strike at the very infrastructure of society. And if it got into the slums of a third world city, it would decimate the population.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Robert Walgate"
        }
    ],
    "bib_entries": {
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Identification of Severe Acute Respiratory Syndrome in",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "M"
                    ],
                    "last": "Poutanen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "E"
                    ],
                    "last": "Low",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Henry",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Finkelstein",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Rose",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Green",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Tellier",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Draker",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Adachi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ayers",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "National Center for Infectious Diseases",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "SARS -worldwide (16): etiology",
            "authors": [
                {
                    "first": "Promed-Mail",
                    "middle": [],
                    "last": "Post",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}