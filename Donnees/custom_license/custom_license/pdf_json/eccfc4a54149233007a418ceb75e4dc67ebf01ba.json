{
    "paper_id": "eccfc4a54149233007a418ceb75e4dc67ebf01ba",
    "metadata": {
        "title": "THREE-DIMENSIONAL RECONSTRUCTION OF THE NUCLEOLUS USING META-CONFOCAL MICROSCOPY IN CELLS EXPRESSING THE CORONAVIRUS NUCLEOPROTEIN",
        "authors": [
            {
                "first": "Jae-Hwan",
                "middle": [],
                "last": "You",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Mark",
                "middle": [
                    "L"
                ],
                "last": "Reed",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Brian",
                "middle": [
                    "K"
                ],
                "last": "Dove",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Julian",
                "middle": [
                    "A"
                ],
                "last": "Hiscox",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The coronavirus nucleoprotein (N protein) is one of the most abundantly expressed viral proteins in an infected cell, with the principal function of binding the viral RNA genome to form the ribonucleocapsid structure (RNP) and forming the viral core. N protein also has roles in viral replication, transcription, and translation as well as modulating cellular processes. We and others have shown that some coronavirus and arterivirus N proteins can localize to a dynamic subnuclear structure called the nucleolus and interact with nucleolar proteins. [1] [2] [3] The nucleolus is involved in ribosome subunit biogenesis, RNA processing, cell cycle control, and acts as a sensor for cell stress. 4 Morphologically the nucleolus can be divided into an inner fibrillar center (FC), a middle dense fibrillar component (DFC), and an outer granular component (GC). A directed proteomic analysis followed by subsequent bioinformatic analysis revealed that the nucleolus is composed of at least 400 proteins.",
            "cite_spans": [
                {
                    "start": 551,
                    "end": 554,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 555,
                    "end": 558,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 559,
                    "end": 562,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 695,
                    "end": 696,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "Coronavirus N proteins have the potential to be phosphorylated at multiple serine residues. However, mass spectroscopic analysis of both the avian infectious bronchitis virus (IBV) 5 and porcine transmissible gastroenteritis virus (TGEV) 6 N proteins have shown that phosphorylation occurs at only three or four residues. In the case of IBV N protein, these map to predicted casein kinase II sites. 5 Based on amino acid sequence comparisons, three conserved regions have been identified in the murine coronavirus, mouse hepatitis virus (MHV) N protein. 7 In general, other coronavirus N proteins would appear to follow this pattern.",
            "cite_spans": [
                {
                    "start": 181,
                    "end": 182,
                    "text": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 399,
                    "end": 400,
                    "text": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 554,
                    "end": 555,
                    "text": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "We investigated the three-dimensional structure of the nucleolus and sub-nuclear bodies within cells expressing IBV and severe acute respiratory syndrome coronavirus (SARS-CoV) N protein. In many cases, viral proteins localize to discrete regions of the nucleolus and their specific localization can inform as to what effect they may be having on the host cell. For example, proteins that localize to, and disrupt the GC, can affect cellular transcription. Therefore, we use coronavirus N proteins as a model to test our working hypothesis that disruption of nucleolar proteins and/or alterations in nucleolar architecture can perturb cellular functions.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "SARS-CoV N protein was N-terminally tagged with ECFP, creating pECFP-SARS-CoV-N. Briefly, the SARS-CoV N gene was amplified (from a clone containing the SARS-CoV N gene-kindly provided by Dr. Maria Zambon) using gene-specific primers to the 5' and 3' 20 nucleotides of the SARS-CoV N gene, but incorporating 5' restriction enzyme sites (in the case of the forward primer this was BspEI and in the reverse primer BamHI) and then TOPO cloned into pCR2.1 (Invitrogen). The insert was subcloned into pECFPC1 (Clontech) such that SARS-CoV N protein would be C-terminal and in frame to ECFP. EGFP was added N-terminally to IBV N protein, creating pEGFP-IBV-N. The cloning strategy was identical to that described for SARS-CoV N gene, except the forward primer restriction site was BamHI and the reverse primer restriction site was EcoRI. The gene was ligated into pEGFPC2 (Clontech), such that IBV N protein would be C-terminal and in frame to EGFP. All clones were verified by sequencing and expression of fusion proteins by Western blot (data not shown).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Expression Constructs"
        },
        {
            "text": "Confocal sections of fixed samples were captured on an LSM510 META microscope (Carl Zeiss Ltd.) equipped with a 63x, NA 1.4, oil immersion lens. Pinholes were set to allow optical sections of 1 \u00b5m to be acquired. In singly transfected cells, ECFP was excited with the 458-nm argon laser line running at 10%, and emission was collected through a BP435-485 emission filter. EGFP was excited with the 488-nm argon laser line running at 2%, and emission was collected through a LP505 filter. Propidium iodide (PI) was excited with the helium:neon 543-nm laser line in all cases, and emission was collected through a LP560 filter. Due to excitation of the EGFP molecule by the 458-nm argon laser line, co-transfected samples were linearly unmixed using the META detector. Lambda plots of EGFP and ECFP were generated from singly transfected reference samples excited with the 458-nm argon laser line and collected with the META detector between 461 and 536-nm, in 10.7-nm increments. These lambda plots were then utilized to separate, or unmix, overlapping emission signal from co-transfected samples. Zsections of cells expressing EGFP, counterstained with PI, were generated by a two-step methodology. Firstly, serial confocal sections of EGFP were acquired with the META detector. PI was then collected as described using the same z-settings. Z-steps were collected 0.5 \u00b5m apart to allow over sampling of the data. The two sets of z-stacks were then pseudo-coloured and merged using the 'copy' facility within the LSM510 META software. Three-dimensional reconstruction and orthogonal views were also generated in the LSM510 META software.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Meta-Confocal Microscopy"
        },
        {
            "text": "Studying the nucleolar localisation of proteins can be problematic. Previous reports raised the possibility that charged proteins could migrate through cells postfixation and become localized to the nucleus and nucleolus. 8 Although in this instance the specific example of VP22 has been challenged, the possibility arises that localization of coronavirus N proteins to discrete subcellular structures could be an artifact of fixation conditions. In addition, the successful detection of nucleolar proteins using antibodies can be related to the concentration of the protein within the nucleolus, in that the nucleolus, because of the high protein concentration, is not always amenable to antibody staining. 9 To address these concerns and also to investigate the subcellular localization of SARS-CoV N protein, we generated vectors that expressed fluorescent tagged SARS-CoV and IBV N proteins and determined the subcellular localization of these proteins first by live cell imagery, followed by fixation and confocal microscopy. SARS-CoV N gene was cloned downstream of ECFP (from vector pECFPC1, Clontech), creating vector pECFP-SARSCoV-N, and IBV N gene was cloned downstream of EGFP (from vector pEGFPC2, Clontech), creating pEGFP-IBV-N, and when expressed in cells, resulted in fluorescent fusion proteins ECFP-SARS-CoV-N and EGFP-IBV-N, respectively. Cos-7 cells were transfected with pECFPC1, pEGFPC2, pEGFP-IBV-N or pECFP-SARSCoV-N (the former two as controls), and imaged 24 hr later by live cell imaging (Fig. 1a) or co-transfected with both pEGFP-IBV-N and pECFP-SARSCoV-N, fixed 24 hr post-transfection and analyzed by META-confocal microscopy (which unmixes ECFP from EGFP (Fig. 1b) .",
            "cite_spans": [
                {
                    "start": 222,
                    "end": 223,
                    "text": "8",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 708,
                    "end": 709,
                    "text": "9",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [
                {
                    "start": 1515,
                    "end": 1524,
                    "text": "(Fig. 1a)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1687,
                    "end": 1696,
                    "text": "(Fig. 1b)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Expression of IBV N Protein and SARS-CoV Protein"
        },
        {
            "text": "Live cell imaging data indicated that both EGFP and ECFP, when expressed as individual proteins, had no distinct distribution pattern and were present in both the cytoplasm and nucleus but not nucleolus (Fig. 1A) . However, EGFP tagged IBV N protein localized to both the cytoplasm and nucleus while ECFP tagged SARS-CoV N protein localized to the cytoplasm only. As ECFP tagged SARS-CoV N has a molecular weight lower than the size exclusion limit for the nuclear pore complex, the lack of any SARS-CoV N protein within the nucleus suggests this protein contains a cytoplasmic retention signal. Because these images were taken from live cells, the localization of IBV N protein to the nucleolus could not have been due to an artifact of fixation.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 203,
                    "end": 212,
                    "text": "(Fig. 1A)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Expression of IBV N Protein and SARS-CoV Protein"
        },
        {
            "text": "Confocal microscopy data (Fig. 1B) reflected the localization patterns observed using live cell imaging. In contrast to EGFP-tagged IBV N protein, ECFP-tagged SARS-CoV N protein localized to the cytoplasm and, in a minority of cells, to what appeared to be a nuclear body (arrowed). Based upon morphology this structure cannot be identified but it does not have the appearance of the nucleolus.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 25,
                    "end": 34,
                    "text": "(Fig. 1B)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Expression of IBV N Protein and SARS-CoV Protein"
        },
        {
            "text": "To investigate whether IBV N protein localized to a specific part of the nucleolus, Cos-7 cells were transfected with pEGFP-IBV-N, fixed 24 hr post-transfection, stained with PI to visualize the nucleus and nucleolus, then sectioned by confocal microscopy (Fig. 2) . The data indicated that IBV N protein localized to a discrete area of the nucleolus. For example, compare the distribution of IBV N protein to the PI stained nucleolus in optical section 2.64 \u00b5m (Fig. 2, arrows) , N protein would appear to occupy less nucleolar volume. To investigate this further, we utilized these Z-sections to construct a threedimensional representation focusing specially on the nuclear region of the cell (Fig. 3) . As can be observed, the volume taken up by IBV N protein in the nucleolus (Fig. 3A) is less than total nucleolar volume (Fig. 3B) . From this data we hypothesize that IBV N protein localizes to the DFC but not the GC. In conclusion, our data demonstrate that IBV N protein localizes to the cytoplasm and nucleolus and is not an artifact of fixation conditions. In contrast, SARS-CoV N protein remains localized in the cytoplasm and does appear to cross the nuclear pore complex, despite being below the size exclusion limit for entry into the nucleus. We hypothesize that SARS-CoV N protein contains a dominant cytoplasmic retention motif. META-confocal analysis and three-dimensional reconstructions of cells expressing IBV N protein revealed that N protein does not localize throughout the nucleolus and may be confined to the DFC.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 256,
                    "end": 264,
                    "text": "(Fig. 2)",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 462,
                    "end": 478,
                    "text": "(Fig. 2, arrows)",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 695,
                    "end": 703,
                    "text": "(Fig. 3)",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 780,
                    "end": 789,
                    "text": "(Fig. 3A)",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 826,
                    "end": 835,
                    "text": "(Fig. 3B)",
                    "ref_id": "FIGREF2"
                }
            ],
            "section": "Three-Dimensional Reconstruction of the Nucleolus in IBV N-Expressing Cells"
        },
        {
            "text": "This work was funded by the BBSRC, project grant number BBSB03416 and studentship BBSSP200310434 to J.A.H. The confocal microscope facility in the Astbury Centre for Structural Molecular Biology was funded by the Wellcome Trust and SRIF, and we would like to thank Gareth Howell for his help in using this facility.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ACKNOWLEDGMENTS"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The interaction of animal cytoplasmic RNA viruses with the nucleus to facilitate replication",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Hiscox",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Virus Res",
            "volume": "95",
            "issn": "",
            "pages": "13--22",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The nucleolus--a gateway to viral infection?",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Hiscox",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Arch. Virol",
            "volume": "147",
            "issn": "",
            "pages": "1077--1089",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Nucleolar-cytoplasmic shuttling of PRRSV nucleocapsid protein: a simple case of molecular mimicry or the complex regulation by nuclear import, nucleolar localization and nuclear export signal sequences",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "R"
                    ],
                    "last": "Rowland",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Yoo",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Virus Res",
            "volume": "95",
            "issn": "",
            "pages": "23--33",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Bioinformatic analysis of the nucleolus",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "K"
                    ],
                    "last": "Leung",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Andersen",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Mann",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "I"
                    ],
                    "last": "Lamond",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Biochem. J",
            "volume": "376",
            "issn": "",
            "pages": "553--569",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Mass spectroscopic characterization of the coronavirus infectious bronchitis virus nucleoprotein and elucidation of the role of phosphorylation in RNA binding by using surface plasmon resonance",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Gill",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "K"
                    ],
                    "last": "Dove",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "R"
                    ],
                    "last": "Emmett",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "F"
                    ],
                    "last": "Kemp",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Ritchie",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Dee",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Hiscox",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Virol",
            "volume": "79",
            "issn": "",
            "pages": "1164--1179",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Phosphorylation and subcellular localization of transmissible gastroenteritis virus nucleocapsid protein in infected cells",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Calvo",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Escors",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Lopez",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Gonzalez",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Alvarez",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Arza",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Enjuanes",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Gen. Virol",
            "volume": "86",
            "issn": "",
            "pages": "2255--2267",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Sequence comparison of the N genes of five strains of the coronavirus mouse hepatitis virus suggests a three domain structure for the nucleocapsid protein",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "M"
                    ],
                    "last": "Parker",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "S"
                    ],
                    "last": "Masters",
                    "suffix": ""
                }
            ],
            "year": 1990,
            "venue": "Virology",
            "volume": "179",
            "issn": "",
            "pages": "463--468",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Positively charged DNA-binding proteins cause apparent cell membrane translocation",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Lundberg",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Johansson",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Biochem. Biophys. Res. Commun",
            "volume": "291",
            "issn": "",
            "pages": "367--371",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "A higher concentration of an antigen within the nucleolus may prevent its proper recognition by specific antibodies",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "V"
                    ],
                    "last": "Sheval",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Polzikov",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "O"
                    ],
                    "last": "Olson",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [
                        "V"
                    ],
                    "last": "Zatsepina",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Eur. J. Histochem",
            "volume": "49",
            "issn": "",
            "pages": "117--123",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "(A). Live cell imaging of cells expressing EGFP, ECFP, EGFP-IBV-N, and ECFP-SARS-CoV N protein. (B) META-confocal image of the same cells co-expressing EGFP-IBV-N (indicated) and ECFP-SARS-CoV N (indicated) protein; subnuclear structures are indicated by an arrow in these cells.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "META-confocal image showing sections (indicated in the top left-hand corner of each image) through a Cos-7 cells expressing IBV N protein. The upper panel shows the distribution of IBV N protein and the lower panel shows the same sections but showing the signal from PI, which highlights the nucleus and nucleolus.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Three-dimensional reconstruction of a nucleus showing the distribution of IBV N protein (A) and the total nucleolar volume (B). Images were reconstructed from the data shown inFig. 2.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}