{
    "paper_id": "d9e872e109b564ba67e9528c968bd3432d08b0b0",
    "metadata": {
        "title": "Viral Diarrhoea in a Rural Coastal Region of Karnataka India",
        "authors": [
            {
                "first": "Mamatha",
                "middle": [],
                "last": "Shetty",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kasturba Medical College",
                    "location": {
                        "region": "Manipal",
                        "country": "India"
                    }
                },
                "email": ""
            },
            {
                "first": "Alfonse",
                "middle": [],
                "last": "Thclma",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kasturba Medical College",
                    "location": {
                        "region": "Manipal",
                        "country": "India"
                    }
                },
                "email": ""
            },
            {
                "first": "Mohan",
                "middle": [],
                "last": "Brown",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kasturba Medical College",
                    "location": {
                        "region": "Manipal",
                        "country": "India"
                    }
                },
                "email": ""
            },
            {
                "first": "P",
                "middle": [
                    "G"
                ],
                "last": "Kotian",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kasturba Medical College",
                    "location": {
                        "region": "Manipal",
                        "country": "India"
                    }
                },
                "email": ""
            },
            {
                "first": "",
                "middle": [],
                "last": "Shivananda",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kasturba Medical College",
                    "location": {
                        "region": "Manipal",
                        "country": "India"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "A total of 106 children below 5 years of age admitted to the Kasturba Medical College Hospital Manipal Karnataka (South India) were investigated over a period of 6 months to determine the aetiological role of viruses in acute diarrhoea. Viral aetiological agents isolated were Rotaviruses in 12 (11 per cent) cases, Adenoviruses in 3 (3 per cent) cases, corona virus and astroviruses in two (2 per cent) cases each. Non-viral isolates were Cryptosporidium and Salmonella typhimurium in two cases each, and Entamoeba histolytica and Shigella flexneri in one case each.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "In many developing countries nearly two-thirds of diarrhoea used to be of unknown aetiology. The introduction of electron microscopy for the examination of faecal samples led in the 1970s to the discovery of a number of viruses which may cause diarrhoeal disease in man and animals. 1 \" 3 In view of the recent recognition of some viral aetiological agents of acute infantile diarrhoea, we conducted the present study to identify viruses as the causative agents of infantile diarrhoea in Manipal, a place in Coastal Karanataka (South India).",
            "cite_spans": [
                {
                    "start": 283,
                    "end": 284,
                    "text": "1",
                    "ref_id": null
                },
                {
                    "start": 285,
                    "end": 288,
                    "text": "\" 3",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "One-hundred-and-six children aged below 5 years, suffering from acute watery diarrhoea of less than 4 days' duration who attended the out patient clinic of paediatric dept of the Kasturba Medical College Hospital, Karanataka, South India were included in the study. Of the 106 children, 59 (56 per cent) were less than 2 years old, 42 (40 per cent) were below 1 year, and the remaining five (5 per cent) were between 3 and 5 years old. The stool specimens were also tested for cryptosporidium; other intestinal parasites, and for bacterial pathogens like Salmonella, Shigella, and Vibrio cholerae by previously described methods. 4 ' 3 The stool samples were frozen and stored at -70\u00b0C for viral testing by electron microscopy at the Liverpool University, London.",
            "cite_spans": [
                {
                    "start": 630,
                    "end": 631,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 632,
                    "end": 635,
                    "text": "' 3",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "The stool samples were processed for the detection of cryptosporidium by modified Ziehl-Neelsen staining, Sheathers sugar flotation technique, and also by phenol-auramine staining.* Detection of rota virus was done by slide latex agglutination test as per the method advocated by the manufacturers (Mercia Diagnostic Ltd, England).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "Cultures showing typical biochemical reactions favouring Salmonella, Shigella, and Vibrio cholerae were confirmed by agglutination with specific antisera. The specimens were also examined by both direct and concentration method for parasitic ova and for fungal isolation.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "Of the total 106 stool samples received, viruses were detected in 19 samples (18 per cent). They were identified as Rotavirus in 12 (11 per cent) cases, coronavirus in two (2 per cent) cases, adenovirus in three (3 per cent) cases, and astrovirus in two (2 per cent) cases each.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results"
        },
        {
            "text": "Fifty control infants, without diarrhoea during last 3 weeks served as controls.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results"
        },
        {
            "text": "Among the control group, a single adenovirus was seen in the stool sample of a 1-year-old child.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results"
        },
        {
            "text": "Clinical picture of viral diarrhoea was characterized by a high frequency of vomiting, fever, and respiratory symptoms. In six infants, vomiting was the first symptom preceding diarrhoea (Table 1) . Table 2 shows the enteric pathogens (viral and non-viral) isolated from cases of suspected viral diarrhoea. In our study, Rotavirus was isolated in children in the 7-12-month age groups. Coronavirus, adenovirus and astrovirus were isolated in children between 1 and 3 years of age group. Salmonella typhimurium was isolated from two infants below 6 months and a single isolate of Shigella flexneri in a 5-year-old school-going child.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 187,
                    "end": 196,
                    "text": "(Table 1)",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 199,
                    "end": 206,
                    "text": "Table 2",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Results"
        },
        {
            "text": "Frequency of detection of viruses in stool samples were high in winter months (December to February) or in the cold wet seasons than in the dry.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results"
        },
        {
            "text": "Diarrhoeal disease is perhaps one of the most important causes of sickness and death among infants and children in developing countries like India. 6 Seasonal characters such as prevalence in winter months supported the diagnosis of viral disease.",
            "cite_spans": [
                {
                    "start": 148,
                    "end": 149,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Rotaviruses are reported as the commonest cause of acute non-bacterial gastroenteritis. 7 \" 9 Rotavirus enteritis is generally a disease of infants and young children and appears to have a worldwide distribution. It is common in children of 6-24 months old with a peak incidence at 9-12 months.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Rotavirus is responsible for 30-60 per cent of all cases of severe watery diarrhoea in young children. In the past two decades, the importance of Rotavirus as a cause of illness and mortality has been clearly documented and substantial progress has been made towards developing vaccines to control this agent.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Enteric adenoviruses are well established as respiratory viruses and are second to Rotavirus as the most common cause of pediatric viral gastroenteritis. It is found to be common below 2 years of age, particularly during the first year of life. Serotypes of adenovirus responsible for diarrhoea are 2, 3, 5, 40, and 41. Diarrhoea is often protracted, but vomiting and fever are less prominent than with Rotavirus. 10 Astrovirus were first associated with gastroenteritis in 1975 in a report by Macheley & Cosgrove who visualized astroviruses on the electron microscope. Astroviruses are found to produce clinical findings similar to those caused by Rotavirus infection, but dehydration is uncommon here. The recent development of an enzyme-linked immunoassay using monoclonal antibodies has enabled the rapid detection of antigen common to all five serotypes in the stool. They have not been firmly established as a cause of gastroenteritis in humans because of the lack of controlled studies and the small number of patients studied. Astroviral disease is most frequent in children from infancy to 7 years of age.\" 12 Coronavirus have also been associated with diarrhoeal illness with the electron microscopic demonstration in faeces. The virus is seen both in ill and well patients. Its causal relationship to GE is still questioned by many investigators.",
            "cite_spans": [
                {
                    "start": 414,
                    "end": 416,
                    "text": "10",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1116,
                    "end": 1118,
                    "text": "12",
                    "ref_id": "BIBREF11"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Appreciation of role of viruses in childhood diarrhoea should lead to a decrease in the wasteful use of antibiotics and a greater emphasis being placed on oral rehydration in the management of the condition.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Viral diarrhoea is not uncommon in India, but very few reports have been published so far. This may be attributed to the fact that diagnostic centres possessing an electron microscope for detection of viral aetiological agents are very few in India. Despite the large amount of investigative work carried out in viral gastroenteritis, an understanding of the natural history and epidemiology of this disease is still lacking.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Practically all the patients in our study are from rural areas where the people have been in close contact with nature and animals. Moreover, this rural population is exposed to unprotected drinking water obtained from open wells, puddles, and streams. Sporadic outbreaks of gastroenteritis and diarrhoea along with other water-borne diseases have been reported in this geographical area. 15 ' 16 As regards viral diarrhoea, no reports have been so far published from coastal Karnataka in India, and further studies are indicated.",
            "cite_spans": [
                {
                    "start": 389,
                    "end": 391,
                    "text": "15",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 392,
                    "end": 396,
                    "text": "' 16",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Shigellosis is one of the commonest causes of morbidity and mortality due to dysenteric illness in developing countries. It is evident from the epidemic of shigelia dysentry reported from West Bengal, India. 1 -2 The disease is worldwide in distribution and affects all the age groups. The reported incidence of shigellosis varies from 5-30 per cent. 3 \" 5 Sigmoidoscopic and histological features have been studied in adults, but not in the pediatric age group.",
            "cite_spans": [
                {
                    "start": 208,
                    "end": 212,
                    "text": "1 -2",
                    "ref_id": null
                },
                {
                    "start": 351,
                    "end": 356,
                    "text": "3 \" 5",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        }
    ],
    "bib_entries": {
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Epidemic viral gastroenteritis",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "K"
                    ],
                    "last": "Estes",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "Y"
                    ],
                    "last": "Graham",
                    "suffix": ""
                }
            ],
            "year": 1979,
            "venue": "Am J Med",
            "volume": "66",
            "issn": "",
            "pages": "1001--1008",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Viral gastroenteritis",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "R"
                    ],
                    "last": "Blacklow",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "B"
                    ],
                    "last": "Greenberg",
                    "suffix": ""
                }
            ],
            "year": 1991,
            "venue": "N Engl J Med",
            "volume": "325",
            "issn": "",
            "pages": "252--64",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Identification of Enterobacteriaceae",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "R"
                    ],
                    "last": "Edwards",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "H"
                    ],
                    "last": "Ewing",
                    "suffix": ""
                }
            ],
            "year": 1972,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "146--207",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Manual for Identification of Medical Bacteria",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "T"
                    ],
                    "last": "Cowan",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "J"
                    ],
                    "last": "Steel",
                    "suffix": ""
                }
            ],
            "year": 1974,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "45--122",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Comparison of sedimentation and floatation techniques for identification of cryptosporidium species oocytes in a large outbreak of human diarrhoea",
            "authors": [
                {
                    "first": "Jnm",
                    "middle": [],
                    "last": "Scott",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "H"
                    ],
                    "last": "Diane",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "W"
                    ],
                    "last": "David",
                    "suffix": ""
                }
            ],
            "year": 1985,
            "venue": "J Clin Microbiol",
            "volume": "22",
            "issn": "",
            "pages": "587--596",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Bacteria, parasitic agents and rotavirus associated with acute diarrhoea in hospital inpatient Indonesian children",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Sennarte",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Sebode",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Suryantere",
                    "suffix": ""
                }
            ],
            "year": 1983,
            "venue": "Trans Roy Soc Trop Med Hyg",
            "volume": "77",
            "issn": "",
            "pages": "724--730",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Viral diarrhoeas in childhood",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "J"
                    ],
                    "last": "Elliott",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Br Med J",
            "volume": "8",
            "issn": "",
            "pages": "1006--1013",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Rotavirus, the first 5 years",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Steinhoffmc",
                    "suffix": ""
                }
            ],
            "year": 1980,
            "venue": "J Paediat",
            "volume": "96",
            "issn": "",
            "pages": "611--633",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Importance of enteric adenovirus 40 and 41 in acute GE in infants and young children",
            "authors": [
                {
                    "first": "I",
                    "middle": [],
                    "last": "Uhnoo",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Wadell",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Vensson",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "E"
                    ],
                    "last": "Johanson",
                    "suffix": ""
                }
            ],
            "year": 1984,
            "venue": "J Clin Microbiol",
            "volume": "20",
            "issn": "",
            "pages": "365--373",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Astrovirus as a cause of GE in children",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "H"
                    ],
                    "last": "John",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "T"
                    ],
                    "last": "David",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "G"
                    ],
                    "last": "Peter",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Neil",
                    "suffix": ""
                }
            ],
            "year": 1991,
            "venue": "N Engl J Med",
            "volume": "324",
            "issn": "",
            "pages": "1757--60",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Astrovirus associated GE in children",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "R"
                    ],
                    "last": "Ashley",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "O"
                    ],
                    "last": "Caul",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "K"
                    ],
                    "last": "Paver",
                    "suffix": ""
                }
            ],
            "year": 1978,
            "venue": "J din Pathol",
            "volume": "31",
            "issn": "",
            "pages": "939--982",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Human viral gastroenteritis",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Cukor",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "R"
                    ],
                    "last": "Blacklow",
                    "suffix": ""
                }
            ],
            "year": 1984,
            "venue": "MicrobRev",
            "volume": "48",
            "issn": "",
            "pages": "157--79",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Viral diarrhoea",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "G"
                    ],
                    "last": "Fairchild",
                    "suffix": ""
                }
            ],
            "year": 1987,
            "venue": "Infect Dis Clin N Am",
            "volume": "1",
            "issn": "",
            "pages": "501--505",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Rotavirus and bacterial enteropathogens causing acute diarrhoea",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Mamatha",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Jyothirlatha",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Subbannayya",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Venkatesh",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "G"
                    ],
                    "last": "Sivananda",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "Int J Paediat",
            "volume": "59",
            "issn": "",
            "pages": "203--210",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "An investigation of cholera outbreak in Raipur district",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "S"
                    ],
                    "last": "Durbari",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Sudarshan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "N"
                    ],
                    "last": "Babar",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Agarwal",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "Ind J Med Res",
            "volume": "66",
            "issn": "",
            "pages": "562--568",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Correspondence: Dr. P. G. Shivananda, MD, PhD, Department of Microbiology, Kasturba Medical College, Manipal-576 119, Karnataka, India.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Clinical features of 19 infants with viral diarrhoea",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Enteric pathogens isolated from suspected cases of viral diarrhoea",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}