{
    "paper_id": "ecdca42f05a92b1b2bcac04e7bdb291831c0a410",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Middle East respiratory syndrome (MERS), an emerging infectious disease first identified in June 2012, is caused by the novel MERS-coronavirus (MERS-CoV) [1] . MERS-CoV, a zoonotic virus, has bats as its natural reservoir and possibly dromedary camels as its intermediate host [2, 3] . MERS-CoV infects humans, especially the elderly, people with diabetes and chronic lung diseases, and immunocompromised persons, causing severe disease with high mortality (~35%). The MERS outbreak has resulted in 1474 laboratory-confirmed cases, including 515 deaths as of August 27, 2015 [4] . There is still a possibility for MERS-CoV to spread within healthcare facilities, to which a number of cases have been linked [5, 6] . Most MERS cases have been reported from Saudi Arabia, but South Korea is the second largest country with travel-associated MERS cases, which can all be traced back to Saudi Arabia. The continuous spread of MERS-CoV possibly through camel-to-human and human-to-human transmission has raised worldwide concerns, calling for immediate steps to develop effective and safe vaccines.",
            "cite_spans": [
                {
                    "start": 154,
                    "end": 157,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 277,
                    "end": 280,
                    "text": "[2,",
                    "ref_id": null
                },
                {
                    "start": 281,
                    "end": 283,
                    "text": "3]",
                    "ref_id": null
                },
                {
                    "start": 559,
                    "end": 578,
                    "text": "August 27, 2015 [4]",
                    "ref_id": null
                },
                {
                    "start": 707,
                    "end": 710,
                    "text": "[5,",
                    "ref_id": null
                },
                {
                    "start": 711,
                    "end": 713,
                    "text": "6]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The genome of MERS-CoV encodes at least four unique accessory proteins, such as 3, 4a, 4b and 5, two replicase proteins (open reading frame 1a and 1b), and four major structural proteins, including spike (S), envelope (E), nucleocapsid (N), and membrane (M) proteins [7] . The accessory proteins play nonessential roles in MERS-CoV replication, but they are likely structural proteins or interferon antagonists, modulating in vivo replication efficiency and/or pathogenesis, as in the case of SARS-CoV [7] [8] [9] . The other proteins of MERS-CoV maintain different functions in virus replication. The E protein, for example, involves in virulence, and deleting the E-coding gene results in replication-competent and propagation-defective viruses or attenuated viruses [7] . The S protein is particularly essential in mediating virus binding to cells expressing receptor dipeptidyl peptidase-4 (DPP4) through receptor-binding domain (RBD) in the S1 subunit, whereas the S2 subunit subsequently mediates virus entry via fusion of the virus and target cell membranes [10, 11] . Therefore, these viral structural proteins, particularly S protein, may serve as targets for vaccine development. It is demonstrated that all circulating human MERS-CoV strains represent one single serotype and that virus isolates from other parts of the outbreaks have no differences from EMC2012, the prototype strain, in replication, interferon escape responses and serum neutralization, suggesting that vaccines developed based on the prototype virus strain unlikely affect their success against other virus strains [12,13].",
            "cite_spans": [
                {
                    "start": 267,
                    "end": 270,
                    "text": "[7]",
                    "ref_id": null
                },
                {
                    "start": 502,
                    "end": 505,
                    "text": "[7]",
                    "ref_id": null
                },
                {
                    "start": 506,
                    "end": 509,
                    "text": "[8]",
                    "ref_id": null
                },
                {
                    "start": 510,
                    "end": 513,
                    "text": "[9]",
                    "ref_id": null
                },
                {
                    "start": 769,
                    "end": 772,
                    "text": "[7]",
                    "ref_id": null
                },
                {
                    "start": 1065,
                    "end": 1069,
                    "text": "[10,",
                    "ref_id": null
                },
                {
                    "start": 1070,
                    "end": 1073,
                    "text": "11]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "No MERS vaccines are available for human use. Vaccines against MERS-CoV thus far developed in the laboratory can be categorized as those based on viral vectors, such as adenovirus (Ad) and Modified Vaccinia virus Ankara (MVA), or those based on recombinant viral proteins, DNAs, nanoparticles, and recombinant virus ( Table 1) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 318,
                    "end": 326,
                    "text": "Table 1)",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "Current status of MERS vaccines"
        },
        {
            "text": "Most viral vector-based MERS vaccines use the full-length S or S1 protein of MERS-CoV as the coding antigens and exhibit immunogenicity in vaccinated animals. Reports have indicated that recombinant Ad5 vectors, which encode the full-length or S1 extracellular domain, of MERS-CoV S protein induced MERS-CoV S-specific antibody responses in immunized mice, neutralizing MERS-CoV infection in vitro [14] . Ad5 or Ad41 vectors expressing full-length S protein of MERS-CoV have elicited MERS-CoV-specific antibody responses, neutralizing antibodies and T-cell responses in immunized mice [15] . Moreover, a viral vector MVA-based MERS vaccine expressing viral S protein, termed MVA-MERS-S, demonstrated efficacy against MERS-CoV infection in Ad/DPP4-transduced mice [16, 17] . These studies suggest the potential of applying MERS-CoV S protein as a vaccine target. The German Center for Infection Research (DZIF) has supported the Phase I clinical trial of MVA-MERS-S vaccine candidate in humans. About USD $1.66 million was awarded to initiate this trial [18] .",
            "cite_spans": [
                {
                    "start": 398,
                    "end": 402,
                    "text": "[14]",
                    "ref_id": null
                },
                {
                    "start": 585,
                    "end": 589,
                    "text": "[15]",
                    "ref_id": null
                },
                {
                    "start": 763,
                    "end": 767,
                    "text": "[16,",
                    "ref_id": null
                },
                {
                    "start": 768,
                    "end": 771,
                    "text": "17]",
                    "ref_id": null
                },
                {
                    "start": 1053,
                    "end": 1057,
                    "text": "[18]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Current status of MERS vaccines"
        },
        {
            "text": "In addition to viral vector-based vaccines, vaccines based on the recombinant MERS-CoV S protein, in particular, RBD, have demonstrated efficacy in protecting immunized animals from MERS-CoV infection [19, 20] . Several fragments, including residues 350 --588, 358 --588, 367 --588, 367 --606, 377 --588, and 377 --662, in the RBD of MERS-CoV S are shown to induce MERS-CoV neutralizing antibody responses in mice and/or rabbits [21, 22] . Thus, similar to the RBD of severe acute respiratory syndrome coronavirus (SARS-CoV), the MERS-CoV RBD also contains a critical neutralizing domain (CND) capable of eliciting highly potent neutralizing antibodies and protective immunity against infection from MERS-CoV. Particularly, a fragment containing residues 377 --588 of MERS-CoV RBD has been identified as a CND able to protect Ad5/hDPP4-transduced and hDPP4transgenic mice against MERS-CoV. Notably, S377 --588 is a very effective immunogen, as 1 \u00b5g of S377 --588 protein could induce potent neutralizing antibody responses similar to those raised by high doses, such as 5 and 20 \u00b5g, respectively [23, 24] . These reports all confirm that the RBD/CND of MERS-CoV in the S protein is an important target for the development of MERS subunit vaccines.",
            "cite_spans": [
                {
                    "start": 201,
                    "end": 205,
                    "text": "[19,",
                    "ref_id": null
                },
                {
                    "start": 206,
                    "end": 209,
                    "text": "20]",
                    "ref_id": null
                },
                {
                    "start": 429,
                    "end": 433,
                    "text": "[21,",
                    "ref_id": null
                },
                {
                    "start": 434,
                    "end": 437,
                    "text": "22]",
                    "ref_id": null
                },
                {
                    "start": 1096,
                    "end": 1100,
                    "text": "[23,",
                    "ref_id": null
                },
                {
                    "start": 1101,
                    "end": 1104,
                    "text": "24]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Current status of MERS vaccines"
        },
        {
            "text": "DNA vaccines are proven to be effective against MERS-CoV infection. An optimized DNA vaccine encoding full-length S protein of MERS-CoV was able to elicit antigen-specific neutralizing antibodies in mice, camels and rhesus macaques (non-human primates, NHPs), with six of the eight vaccinated macaques showing no radiographic evidence of infiltration after MERS-CoV challenge. Interestingly, potent antigen-specific cellular immune responses were induced in the immunized macaques, suggesting that T cell responses may also play a role in MERS-CoV protection [25] . Inovio Pharmaceuticals Inc., in collaboration with GeneOne Life Science, will perform a Phase I clinical trial for this DNA-based vaccine [26] . In addition to the DNA-only strategy, DNA-priming and protein-boosting could be an alternative vaccine approach for MERS-CoV. It is revealed that full-length S DNA priming and S1 subunit protein boosting immunization of mice and NHPs induced robust neutralizing antibody responses against several MERS-CoV strains, protecting NHPs from MERS-CoV challenge [27] .",
            "cite_spans": [
                {
                    "start": 559,
                    "end": 563,
                    "text": "[25]",
                    "ref_id": null
                },
                {
                    "start": 704,
                    "end": 708,
                    "text": "[26]",
                    "ref_id": null
                },
                {
                    "start": 1066,
                    "end": 1070,
                    "text": "[27]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Current status of MERS vaccines"
        },
        {
            "text": "Other potential candidates have been suggested as MERS vaccines. For example, purified MERS-CoV full-length S nanoparticles in combination with appropriate adjuvants could elicit neutralizing antibodies in immunized mice [28] . Using reverse genetics, recombinant MERS-CoV may efficiently replicate in cell culture of human cell lines with broad tissue tropism, allowing an engineered mutant MERS-CoV lacking the structural E protein to be rescued and propagate in cells expressing the E protein in trans, thus providing a platform to develop live-attenuated, or recombinant, MERS-CoV-based vaccines [7, 9] .",
            "cite_spans": [
                {
                    "start": 221,
                    "end": 225,
                    "text": "[28]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 600,
                    "end": 603,
                    "text": "[7,",
                    "ref_id": null
                },
                {
                    "start": 604,
                    "end": 606,
                    "text": "9]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Current status of MERS vaccines"
        },
        {
            "text": "The induction of neutralizing antibodies is the key to the prevention of MERS-CoV infection, as supported by the fact that the reduction of lung pathogenesis and protection from MERS-CoV infection has been correlated with neutralizing antibody levels in the animal models [23, 29] . In addition, vaccines that enjoy a high safety profile, combined with the ability to induce broad-spectrum immune responses and strong protective neutralizing antibodies, should have top priority for further development.",
            "cite_spans": [
                {
                    "start": 272,
                    "end": 276,
                    "text": "[23,",
                    "ref_id": null
                },
                {
                    "start": 277,
                    "end": 280,
                    "text": "29]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Expert opinion"
        },
        {
            "text": "Efficacy and protective immunity of MERS candidate vaccines are, of necessity, evaluated in appropriate animal models. Thus, before moving to clinical trials, NHP rhesus macaque and common marmoset models [30, 31] and camel model [32] , have been established and utilized for such purposes. However, the use of NHP and camel models is financially and facility restrictive for many researchers. Fortunately, recent advancements in establishing small animal models, including Ad5/hDPP4-transduced mouse model [33] and hDPP4-transgenic mouse models [34] , have provided a portable and economical platform for assessing the efficacy of MERS candidate vaccines, despite the fact that each of these animal models has its advantages and disadvantages.",
            "cite_spans": [
                {
                    "start": 205,
                    "end": 209,
                    "text": "[30,",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 210,
                    "end": 213,
                    "text": "31]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 230,
                    "end": 234,
                    "text": "[32]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 507,
                    "end": 511,
                    "text": "[33]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 546,
                    "end": 550,
                    "text": "[34]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Expert opinion"
        },
        {
            "text": "Although very promising, the recombinant MERS-CoVbased vaccines will need to be further characterized in suitable MERS-CoV animal models to confirm their efficacy and safety. Viral vector-based vaccines, on the other hand, may show protective immunity in challenged mouse animals. However, these vaccines might have safety concerns by the presence of pre-existing immunity in humans (in the case of Ad) or the potential to induce harmful immune responses. Thus, safety tests are strongly recommended. Also, this vaccine type might lead to incomplete protection or fail to protect aged groups, as shown in the case of SARS-CoV vaccines [35] . By comparison, recombinant protein-based subunit vaccines possess the highest safety profile because they utilize full-length spike protein, or its subunit (e.g., S1) or fragment (e.g., RBD) as the antigen. In the presence of suitable adjuvants, this type of vaccine is generally able to induce higher titers of neutralizing antibody responses than those elicited by other vaccine types, and they can be easily injected via different pathways, including intramuscular injection, an adopted route for human vaccines. Importantly, some of these MERS subunit vaccines are shown to be protective against MERS-CoV infection in challenged mouse and/or NHP models [23, 27] , demonstrating their ability for further scaleup and/or human clinical trials.",
            "cite_spans": [
                {
                    "start": 635,
                    "end": 639,
                    "text": "[35]",
                    "ref_id": null
                },
                {
                    "start": 1299,
                    "end": 1303,
                    "text": "[23,",
                    "ref_id": null
                },
                {
                    "start": 1304,
                    "end": 1307,
                    "text": "27]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Expert opinion"
        },
        {
            "text": "Among these recombinant protein-based subunit vaccines, we believe that RBD-based vaccines are more effective and safer than those based on the full-length S protein. RBD contains the CND in the S protein, and most of the highly potent neutralizing antibodies target the RBD [21,36-38]. Recombinant proteins containing RBD induce strong neutralizing antibody responses and protection in the vaccinated animals against MERS-CoV infection [21, 23] . In contrast, the full-length S protein contains some non-neutralizing immunodominant domains, which may compromise the immunogenicity with the CND in RBD, or even induces virus-enhancing or harmful immune responses, as demonstrated in the S protein of SARS-CoV [39] . This may be one of the reasons that the development of full-length S protein-based SARS vaccine has been discontinued. One may criticize that the vaccines based on the RBD sequences of the current MERS-CoV strains might be not effective against future emerged MERS-CoV strains with mutations in RBD. This should not be a problem for RBD-based vaccines because RBD contains several conformational neutralizing epitopes, and thus mutation(s) in one epitope may not significantly affect the neutralizing activity of antibodies elicited by other epitopes. For example, the RBD-specific mAb F11 could not neutralize the Bisha1 strain of MERS-CoV, whose RBD contains 509 mutation, but susceptible to neutralization of D12, another MERS-CoV RBDspecific mAb [27] . Previous studies have demonstrated that RBD of SARS-CoV Tor2 strain, which contains six different conformational neutralizing epitopes, induces antibodies in the vaccinated animals with neutralizing activity against all SARS-CoV strains tested, including those caused by the early SARS outbreaks (e.g., GD03 strain) and late SARS pandemics (e.g., Urbani strain), as well as the SARS-like CoV strain from civets (SZ strain) [40] . Although some epitopes outside MERS-CoV RBD may have neutralizing activity, such ability is significantly lower than that of the RBD. For example, neutralizing mAbs G2 and G4 target epitopes at MERS-CoV S1 and S2, respectively, both of which are outside the RBD, their neutralizing ability is not as potent as RBD-specific mAbs F11 and D12 [27] . Nevertheless, these non-RBD neutralizing epitopes, if being clearly identified, could be potentially included in vaccine design to increase the breadth and strength of MERS-CoV vaccines.",
            "cite_spans": [
                {
                    "start": 437,
                    "end": 441,
                    "text": "[21,",
                    "ref_id": null
                },
                {
                    "start": 442,
                    "end": 445,
                    "text": "23]",
                    "ref_id": null
                },
                {
                    "start": 709,
                    "end": 713,
                    "text": "[39]",
                    "ref_id": null
                },
                {
                    "start": 1466,
                    "end": 1470,
                    "text": "[27]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1896,
                    "end": 1900,
                    "text": "[40]",
                    "ref_id": null
                },
                {
                    "start": 2243,
                    "end": 2247,
                    "text": "[27]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Expert opinion"
        },
        {
            "text": "It is common knowledge that the process of vaccine approval is long and that it is further complicated by regulatory restrictions and a shortage of funds. As a case in point, Ad5.MERS-S/Ad5.MERS-S1 Viral vector Full-length S/S1 Mice Preclinical [14] MVA-MERS-S Viral vector Full-length S hDPP4-transduced mice Preclinical [16] S1 --358 --588-Fc Recombinant protein S1-RBD (358 --588) Rabbits Preclinical [19] MERS-CoV rRBD Recombinant protein S1-RBD (367 --606) Mice Preclinical [41] S-RBD-Fc Recombinant protein S1-RBD (377 --662) Mice Preclinical [20] S377 --588-Fc Recombinant protein S1-RBD (377 --588) Mice, rabbits, hDPP4-mice Preclinical [21, 23] MERS DNA Full-length S Mice, camels, NHPs Preclinical [25] S-DNA/S1 protein DNA + Recombinant protein Full-length S/S1 Mice, NHPs Preclinical [27] MERS-S Nanoparticles Full-length S Mice Preclinical [28] rMERS-CoV/rMERS- MERS vaccines are urgently needed, yet Big Pharma has consistently refused to invest funds for manufacturing or developing in pre-clinical and clinical studies based on the uncertainty of immediate return on such investments. In addition, adequate financial support from government entities is not guaranteed by the general lack of funds. As noted above, however, some firms in U.S. and DZIF Academy in Germany have taken the first step toward clinical trials of the two MERS vaccines based on DNA and viral vector, respectively. Big Pharma and governments of these countries and other parts of the world should take note of this signal and act accordingly.",
            "cite_spans": [
                {
                    "start": 245,
                    "end": 249,
                    "text": "[14]",
                    "ref_id": null
                },
                {
                    "start": 322,
                    "end": 326,
                    "text": "[16]",
                    "ref_id": null
                },
                {
                    "start": 404,
                    "end": 408,
                    "text": "[19]",
                    "ref_id": null
                },
                {
                    "start": 479,
                    "end": 483,
                    "text": "[41]",
                    "ref_id": null
                },
                {
                    "start": 549,
                    "end": 553,
                    "text": "[20]",
                    "ref_id": null
                },
                {
                    "start": 645,
                    "end": 649,
                    "text": "[21,",
                    "ref_id": null
                },
                {
                    "start": 650,
                    "end": 653,
                    "text": "23]",
                    "ref_id": null
                },
                {
                    "start": 708,
                    "end": 712,
                    "text": "[25]",
                    "ref_id": null
                },
                {
                    "start": 796,
                    "end": 800,
                    "text": "[27]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 853,
                    "end": 857,
                    "text": "[28]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Expert opinion"
        },
        {
            "text": "The authors were funded by NIH grants R21AI109094, R21AI111152 and R01AI098775, as well as intramural funds from the New York Blood Center, grant NYB000348. The authors have no other relevant affiliations or financial involvement with any organization or entity with a financial interest in or financial conflict with the subject matter or materials discussed in the manuscript. This includes employment, consultancies, honoraria, stock ownership or options, expert testimony, grants or patents received or pending, or royalties.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Declaration of interest"
        },
        {
            "text": "Papers of special note have been highlighted as either of interest () or of considerable interest () to readers. This was the one of the papers showing the protective immunity induced by MERS-CoV full-length S-based vaccines in NHPs.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Bibliography"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "2015/Inovio-Pharmaceuticals-Partners-with-GeneOne-Life-Science-for-MERS-Immunotherapy-Clinical-Development/default.aspx",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Evaluation of candidate vaccine approaches for MERS-CoV",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "G"
                    ],
                    "last": "Joyce",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "This was the first paper demonstrating the protective immunity induced by MERS-CoV full-length S DNA priming and S1 protein boosting in NHPs",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Purified coronavirus spike protein nanoparticles induce coronavirus neutralizing antibodies in mice",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Coleman",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "V"
                    ],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Mu",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Vaccine",
            "volume": "32",
            "issn": "26",
            "pages": "3169--74",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Passive immunotherapy with dromedary immune serum in an experimental animal model for Middle East respiratory syndrome coronavirus infection",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Perera",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Kayali",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Virol",
            "volume": "89",
            "issn": "11",
            "pages": "6117--6137",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Infection with MERS-CoV causes lethal pneumonia in the common marmoset",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Falzarano",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "De Wit",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Feldmann",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "PLoS Pathog",
            "volume": "10",
            "issn": "8",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Middle East respiratory syndrome coronavirus (MERS-CoV) causes transient lower respiratory tract infection in rhesus macaques",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "De Wit",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "L"
                    ],
                    "last": "Rasmussen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Falzarano",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Proc Natl Acad Sci",
            "volume": "110",
            "issn": "41",
            "pages": "16598--603",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Replication and shedding of MERS-CoV in upper respiratory tract of inoculated dromedary camels",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "R"
                    ],
                    "last": "Adney",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "N"
                    ],
                    "last": "Van",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "R"
                    ],
                    "last": "Brown",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Emerg Infect Dis",
            "volume": "20",
            "issn": "12",
            "pages": "1999--2005",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Rapid generation of a mouse model for Middle East respiratory syndrome",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wohlford-Lenane",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Proc Natl Acad Sci",
            "volume": "111",
            "issn": "13",
            "pages": "4970--4975",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "MERS vaccines under development.",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "13. Drosten C, Muth D, Corman VM, et al. An observational, laboratory-based study of outbreaks of middle East respiratory syndrome coronavirus in Jeddah and Riyadh, kingdom of Saudi Arabia, 2014. This paper demonstrates the protection of MERS-CoV infection by a viral vector-based vaccine. 17. Song F, Fux R, Provacia LB, et al. Middle East respiratory syndrome coronavirus spike protein delivered by modified vaccinia virus Ankara efficiently induces virus-neutralizing antibodies. J Virol 2013;87(21):11950-4 18. Paddock C. MERS vaccine 'ready for human trials'. Medical News Today. Available from: http://www. medicalnewstoday.com/articles/296023. php [Last accessed 29 June 2015] 19. Mou H, Raj VS, van Kuppeveld FJ, et al. The receptor binding domain of the new MERS coronavirus maps to a 231-residue region in the spike protein that efficiently elicits neutralizing antibodies. J Virol 2013;87(16):9379-83 20. Du L, Zhao G, Kou Z, et al. Identification of a receptor-binding domain in the S protein of the novel human coronavirus Middle East respiratory syndrome coronavirus as an essential target for vaccine development. J Virol 2013;87(17):9939-42 21. Ma C, Wang L, Tao X, et al. Searching for an ideal vaccine candidate among different MERS coronavirus receptorbinding fragments -the importance of immunofocusing in subunit vaccine design. Vaccine 2014;32(46):6170-6 This was one of the papers demonstrating MERS-CoV RBD as an important vaccine target. 22. Ma C, Li Y, Wang L, et al. Intranasal vaccination with recombinant receptor-binding domain of MERS-CoV spike protein induces much stronger local mucosal immune responses than subcutaneous immunization: Implication for designing novel mucosal MERS vaccines. Vaccine 2014;32(18):2100-8 23. Zhang N, Channappanavar R, Ma C, et al. Identification of an ideal adjuvant for receptor-binding domain-based subunit vaccines against Middle East respiratory syndrome coronavirus. Reuschel EL, et al. A synthetic consensus anti-spike protein DNA vaccine induces protective immunity against Middle East respiratory syndrome coronavirus in nonhuman primates. Sci Transl Med 2015;7(301):301ra132",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "This was the first paper to demonstrate the generation of a MERS-CoV mouse model.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "annex"
        },
        {
            "text": "Agrawal AS, Garron T, Tao X, et al. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "34."
        }
    ]
}