{
    "paper_id": "b6645ab156fa99f54d86a917f054f4748a79035b",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "Brendon",
                "middle": [],
                "last": "Sen-Crowe",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kendall Regional Medical Center",
                    "location": {
                        "settlement": "Miami",
                        "region": "FL",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Mark",
                "middle": [],
                "last": "Mckenney",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kendall Regional Medical Center",
                    "location": {
                        "settlement": "Miami",
                        "region": "FL",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Dessy",
                "middle": [],
                "last": "Boneva",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kendall Regional Medical Center",
                    "location": {
                        "settlement": "Miami",
                        "region": "FL",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Adel",
                "middle": [],
                "last": "Elkbuli",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Kendall Regional Medical Center",
                    "location": {
                        "settlement": "Miami",
                        "region": "FL",
                        "country": "USA"
                    }
                },
                "email": "adel.elkbuli@hcahealthcare.com"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The current doubling time in the US for the SARS-CoV-2, virus, is 3 days 5 . However, the doubling time is currently 6 days for King County in Washington state 6 . In mid-March Washington state took measures to limit the spread of infection, by closing educational facilities, closing non-essential services, and a stay at home order (SAHO) 7 . These efforts have been associated with the percent increase in cases and fatalities decreasing ( Figure 2 ). According to the Institute for Disease Modeling (IDM), the spread of infection into Seattle and Eastside decreased by about 90% and has continuously decreased since March 2 nd9 . In late February, the reproductive number was about 2.7, whereas it was approximately 1.4 on March 18 th 9 .",
            "cite_spans": [
                {
                    "start": 160,
                    "end": 161,
                    "text": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 341,
                    "end": 342,
                    "text": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [
                {
                    "start": 443,
                    "end": 451,
                    "text": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "Likewise, in California, with strict physical distancing measures in effect in the Bay Area and Sacramento County, the doubling time is now 6 days 6 . On March 19 th California closed nonessential services and educational facilities, and a SAHO was enacted 10 . The percent increase in cases in California also decreased after this period ( Figure 2 ).",
            "cite_spans": [
                {
                    "start": 257,
                    "end": 259,
                    "text": "10",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [
                {
                    "start": 341,
                    "end": 349,
                    "text": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "Additionally, a similar effect was observed after Idaho issued a SAHO and closed non-essential services on March 25 th 11 . After March 25 th the fatality rate increased slightly, and then showed a decreasing trend and lower percent increase in new cases ( Figure 2 ). The lack of testing does not appear to play a role in the decrease in fatality rate because the percentage of those testing positive continues to increase ( Figure 2 ).",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 257,
                    "end": 265,
                    "text": "Figure 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 426,
                    "end": 434,
                    "text": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "Florida issued a SAHO on April 3rd, late in comparison to many other states 12, 13 . However, their percent increase in cases over the past two weeks has decreased comparably to states in which a SAHO was in effect earlier ( Figure 2 ). This highlights the importance of other interventions.",
            "cite_spans": [
                {
                    "start": 76,
                    "end": 79,
                    "text": "12,",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 80,
                    "end": 82,
                    "text": "13",
                    "ref_id": "BIBREF11"
                }
            ],
            "ref_spans": [
                {
                    "start": 225,
                    "end": 233,
                    "text": "Figure 2",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "Florida mandated isolation orders of those at most risk, including senior citizens and those with underlying medical conditions 12 . Furthermore, travel was limited to that necessary to obtain or provide essential services or to conduct essential activities, and businesses/organizations were encouraged to provide delivery, carry-out or curbside service 12 .",
            "cite_spans": [],
            "ref_spans": [],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "New York holds the greatest number of cases and deaths (Figure 1) 1 . This forced the state to consider extraordinary measures aimed at increasing hospital capacity and decreasing density of cases. To address the first issue, an executive order was issued, allowing the state to increase J o u r n a l P r e -p r o o f hospital capacity 14 . In addition, there was deployment of a 1,000-bed hospital ship to New York Harbor 15 . Moreover, an executive order signed on March 19 th mandated all businesses requiring in-office personnel to decrease their in-office workforce by 75% 16 . Another executive order on March 20 th , enforced the closure of all non-essential businesses 17 . The percent increase in cases dropped after these interventions (Figure 2 ).",
            "cite_spans": [
                {
                    "start": 337,
                    "end": 339,
                    "text": "14",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 424,
                    "end": 426,
                    "text": "15",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 579,
                    "end": 581,
                    "text": "16",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 678,
                    "end": 680,
                    "text": "17",
                    "ref_id": "BIBREF15"
                }
            ],
            "ref_spans": [
                {
                    "start": 747,
                    "end": 756,
                    "text": "(Figure 2",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "The relative increase in cases appears to be abating, but the fatality rate is increasing (Figure 2 ).",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 90,
                    "end": 99,
                    "text": "(Figure 2",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "These trends may suggest that there is inadequate healthcare capacity and medical supplies 18 . In response, the Centers for Medicare & Medicaid Services (CMS) granted changes to provide some flexibility for hospitals, physicians and healthcare organizations 19 . CMS allowed hospitals to provide services for patients transferred outside of their building 19 . The new rules allow for patient transfers to ambulatory surgery centers, freeing hospital beds for the critically ill 19 There is an association between implementing social distancing and a lowering of the percent increase of cases. Some states implemented interventions earlier than others did, which may be responsible for the differing fatality rates. Encouraging states to implement more widespread distancing interventions at an earlier time has the potential to act on multiple levels in preventing the spread of COVID-19, and could make all the difference in preserving thousands if not hundreds of thousands of lives in the United States. Now is not the time to return to normalcy; we can win this fight together.",
            "cite_spans": [
                {
                    "start": 259,
                    "end": 261,
                    "text": "19",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 357,
                    "end": 359,
                    "text": "19",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 480,
                    "end": 482,
                    "text": "19",
                    "ref_id": "BIBREF16"
                }
            ],
            "ref_spans": [],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "J o u r n a l P r e -p r o o f",
            "cite_spans": [],
            "ref_spans": [],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "The total cases and total deaths plotted as a function of time for Washington State (WA), California (CA), New York (NY), Florida (FL) and Idaho (ID). As of April 1, 2020, the total number of cases in descending order is NY > CA > FL > WA > ID. The total number of deaths in descending order is NY > WA > CA > FL > ID. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Figure 1: Total Cases of COVID-19 and Total Deaths due to COVID-19 in 5 States"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "COVID-19 Map. Johns Hopkins Coronavirus Resource Center",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Lung Cancer Facts: 29 Statistics and Facts: LCFA. Lung Cancer Foundation of America",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "COVID-19)",
            "authors": [],
            "year": 2019,
            "venue": "Centers for Disease Control and Prevention",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "US COVID-19 cases surge past 82,000, highest total in world",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Soucheray",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Center for Infectious Disease Research and Policy",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Reported COVID-19 Cases Over Time",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Proclamation by the Governor Amending Proclamation 20-05. Office of the Governor",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Understanding the Impact of COVID-19 Policy Change in the Greater Seattle Area using Mobility Data",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Burstein",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Thakkar",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Schroeder",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Famulare",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Klein",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Social distancing and mobility reductions have reduced COVID-19 transmission in King County",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Thakkar",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Burstein",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Selvaraj",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Klein",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Executive Department -State of California",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Executive Order Number 20-91. Office of the Governor",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Department of Health Announces Two Presumptive Positive COVID-19 Cases in Florida. Department of Health Announces Two Presumptive Positive COVID-19 Cases in Florida | Florida Department of Health",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "During Coronavirus Briefing, Governor Cuomo Issues Executive Order Allowing State to Increase Hospital Capacity. Governor Andrew M",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Amid Ongoing COVID-19 Pandemic, Governor Cuomo Announces Deployment of 1,000-Bed Hospital Ship 'USNS Comfort' to New York Harbor",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Governor Cuomo Signs Executive Order Mandating Businesses That Require In-Office Personnel to Decrease In-Office Workforce by 75 Percent",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "IHME: COVID-19 Projections",
            "authors": [],
            "year": 2020,
            "venue": "Governor Cuomo Signs the 'New York State on PAUSE' Executive Order. Governor Andrew M",
            "volume": "18",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "Trump Administration Makes Sweeping Regulatory Changes to Help U.S. Healthcare System Address COVID-19 Patient Surge. Centers for Medicare & Medicaid Services",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "COVID-19 Hospital Capacity Estimates 2020. Harvard Global Health Institute",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "US Department of Health and Human Services; Assistant Secretary for Preparedness and Response",
            "authors": [],
            "year": 2019,
            "venue": "Strategic National Stockpile. Last reviewed November",
            "volume": "18",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF19": {
            "ref_id": "b19",
            "title": "Administrator for the Centers for Medicare and Medicaid Services and member of White House Coronavirus Task Force Seema Verma interviewed on U.S. preparedness for ongoing coronavirus pandemic",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF20": {
            "ref_id": "b20",
            "title": "FIGURE LEGENDS",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": ". Many states are still projected to have shortages in hospital and ICU beds and ventilators (Figure 4) 18,20 . Of the 5 selected states, Washington and Florida are projected to have a shortage of ICU beds; Idaho and New York are projected to have a shortage of hospital and ICU beds, and all 5 states are projected to have a shortage of ventilators (Figure 4) 18,20 . There is a national stockpile of ventilators that can be deployed 21 , but may be insufficient to supply all of the hospitals' needs 18,20,22 .",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fatality Rate, Percent Increase in Cases and Percentage of Individuals who Tested Positive for COVID-19 across 5 States The fatality rate (blue), percent increase in COVID-19 cases (orange) and Percentage of those who tested positive (gray) reported in each state. 2a. New York; 2b. Washington State; 2c. Idaho; 2d. California; 2e. Florida.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Timeline of Major Interventions in 5 StatesThe relative time points of major interventions implemented by Washington State (WA), California (CA), New York (NY), Idaho (ID) and Florida (FL).",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Hospital Resources and Projected Need across 5 StatesThe total number of hospital beds in the State (Blue), the number of available hospital beds (Orange), projected number of hospital beds needed (Gray), total number of ICU beds (Yellow), the number of available ICU Beds (Light Blue), projected number of ICU beds needed (Green), and projected number of ventilators needed (Purple) were reported in Washington state, California, New York, Idaho, and Florida. Washington and Florida are projected to have a shortage of ICU beds; Idaho and New York are projected to have a shortage of hospital and ICU beds, and all 5 states are projected to have a shortage of ventilators.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF4": {
            "text": "Total",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Total Cases of COVID-19 and Total Deaths due to COVID-19 in 5 States",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}