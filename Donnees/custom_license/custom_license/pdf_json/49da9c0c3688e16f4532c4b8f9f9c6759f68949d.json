{
    "paper_id": "49da9c0c3688e16f4532c4b8f9f9c6759f68949d",
    "metadata": {
        "title": "Antibody Avidity Maturation during Severe Acute Respiratory Syndrome-Associated Coronavirus Infection",
        "authors": [
            {
                "first": "Paul",
                "middle": [
                    "K S"
                ],
                "last": "Chan",
                "suffix": "",
                "affiliation": {},
                "email": "paulkschan@cuhk.edu.hk."
            },
            {
                "first": "Pak-Leong",
                "middle": [],
                "last": "Lim",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Prince of Wales Hospital",
                    "location": {
                        "settlement": "Shatin"
                    }
                },
                "email": ""
            },
            {
                "first": "Esther",
                "middle": [
                    "Y M"
                ],
                "last": "Liu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Jo",
                "middle": [
                    "L K"
                ],
                "last": "Cheung",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Danny",
                "middle": [
                    "T M"
                ],
                "last": "Leung",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Prince of Wales Hospital",
                    "location": {
                        "settlement": "Shatin"
                    }
                },
                "email": ""
            },
            {
                "first": "Joseph",
                "middle": [
                    "J Y"
                ],
                "last": "Sung",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "K",
                "middle": [
                    "S"
                ],
                "last": "Paul",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "",
                "middle": [],
                "last": "Chan",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "The maturation of virus-specific immunoglobulin G avidity during severe acute respiratory syndrome-associated coronavirus infection was examined. The avidity indices were low (mean \u202b\u05e2\u202c SD, 30.8% \u202b\u05e2\u202c 11.6%) among serum samples collected \u044050 days after fever onset, intermediate (mean \u202b\u05e2\u202c SD, 52.1% \u202b\u05e2\u202c 14.1%) among samples collected between days 51 and 90, and high (mean \u202b\u05e2\u202c SD, 78.1% \u202b\u05e2\u202c 8.0%) among samples collected after day 90. Avidity indices of 40% and 55% could be considered as cutoff values for determination of recent (\u044050 days) and past (165 days) infection, respectively. Measurement of antibody avidity can be used to differentiate primary infection from reexposure and to assess humoral responses to candidate vaccines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "Since the worldwide outbreak of severe acute respiratory syndrome (SARS) between November 2002 and June 2003 [1], subsequent smaller outbreaks have occurred as a result of laboratory negligence or reemergence of SARS-associated coronavirus (SARS-CoV) from the natural reservoir [2] [3] [4] [5] . These incidences bear witness to the fact that reemergence of SARS-CoV infection in humans is a real concern. Experience from the Guandong outbreak (which occurred between December",
            "cite_spans": [
                {
                    "start": 278,
                    "end": 281,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 282,
                    "end": 285,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 286,
                    "end": 289,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 290,
                    "end": 293,
                    "text": "[5]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "2003 and January 2004) suggests that the clinical presentation of disease and the transmission behavior of the reemerged SARS-CoV strain can be different from what was known before [4] . When a SARS outbreak occurs again, it is mandatory that a serological survey be conducted, to define the epidemiological character of the outbreak. Since these outbreaks may happen in places where a proportion of the population was exposed to the virus during a previous outbreak of SARS, a reliable method for differentiating between recent infection and past exposure is vital if a meaningful interpretation is to result from such investigations [4] . The avidity (functional affinity) of an antibody is a measure of the overall strength of interaction between antibody and antigen. The avidity of virus-specific IgG antibody is low during primary viral infection and increases with time [6] [7] [8] . However, exceptions to this rule have been observed for some viruses [9, 10] . Here, we report the maturation pattern of anti-SARS-CoV nucleocapsid protein-specific IgG antibody (hereafter, \"anti-SARS-CoV IgG antibody\") avidity over the course of a 10-month period after primary infection and discuss the potential applications of our findings.",
            "cite_spans": [
                {
                    "start": 181,
                    "end": 184,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 635,
                    "end": 638,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 877,
                    "end": 880,
                    "text": "[6]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 881,
                    "end": 884,
                    "text": "[7]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 885,
                    "end": 888,
                    "text": "[8]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 960,
                    "end": 963,
                    "text": "[9,",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 964,
                    "end": 967,
                    "text": "10]",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Patients, materials, and methods. Sixty-one patients with SARS were recruited into the present study; they ranged in age from 21 to 81 years (mean \u202b\u05e2\u202c SD, 37.0 \u202b\u05e2\u202c 14.9 years), and 54.1% were female. These patients presented with acute-onset fever that progressed to pneumonia, which was otherwise unexplained. Nine patients (14.8%) required intensive care, all of whom eventually recovered. All 61 patients fulfilled the US Centers for Disease Control and Prevention criteria for SARS [11] and had serological evidence of SARS-CoV infection, as determined by the anti-SARS-CoV IgG antibody immunofluorescence assay described elsewhere [12] . Forty-one patients (67.2%) seroconverted, and 20 (32.8%) developed a \u04434-fold increase in antibody titer. A total of 90 serum samples were available from the 61 patients, of whom 26 provided 11 sample for the study.",
            "cite_spans": [
                {
                    "start": 486,
                    "end": 490,
                    "text": "[11]",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 636,
                    "end": 640,
                    "text": "[12]",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Anti-SARS-CoV IgG antibody avidity was measured by a 2step approach. The first step was to assess the concentrations of anti-SARS-CoV IgG antibody in samples, so that samples that required further dilution for testing during the second step could be identified. On the basis of our serial-dilution experiments, samples with ODs of 12.5 needed to be further diluted to provide a linear range for measurement of avidity during the second step. Concentrations of anti-SARS-CoV IgG antibody were measured by a recombinant nucleocapsid proteinbased enzyme immunoassay, ELISARS (IgGENE), in accor- dance with the manufacturer's instructions. Briefly, samples were diluted to a concentration of 1:50 by mixing 22 mL of serum with 1.1 mL of sample diluent. An 100-mL aliquot of the prediluted serum was added to an antigen-coated well. After incubation at room temperature (\u223c25\u040aC) for 30 min, the wells were washed 3 times with the washing buffer provided. Antihuman IgG antibodies conjugated with horseradish peroxidase were added and were incubated at room temperature for 15 min. After a second washing step, 3,3 ,5,5 -tetramethylbenzidine was added as a substrate for color development. Optical density was measured at 450 nm.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The second step was also based on the ELISARS assay but included a urea-elution procedure. Briefly, samples were further diluted, if necessary, according to the results obtained during the first step. The neat or prediluted serum samples were mixed with sample diluent as described above. The sample mixtures were added in duplicate to 2 antigen-coated wells. After the first incubation step, 300 mL of urea was added to one of the wells, whereas the same volume of washing buffer was added to the other well, which served as a reference. On the basis of our initial optimization experiments using 5 early and 5 late samples (collected !20 and 1250 days after fever onset, respectively), a soaking step at room temperature for 10 min with 4 mol/L urea diluted in washing buffer was found to be most suitable and, thus, was used in the present study. The ureasoaking step was followed by washing 3 times with the washing buffer provided. The subsequent conjugate-addition and colordevelopment steps were conducted in accordance with the manufacturer's protocol. The antibody avidity index was calculated as OD urea /OD reference and is expressed as a percentage.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Samples collected \u044050 days after fever onset were also tested for anti-SARS-CoV IgM antibody, so that IgM antibody detection and IgG antibody avidity measurement could be compared with respect to demonstrating a recent infection. Anti-SARS-CoV IgM antibody was also detected by the ELISARS assay. Briefly, samples were treated by use of a rheumatoid factor removal kit (Chemicon) and then mixed with sample diluent to a final concentration of 1:50. A 100-mL aliquot of the diluted sample was added to an antigen-coated well and subjected to the incubation, wash, and color-development steps described above, except that anti-human IgM antibody was used as the conjugate.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Anti-SARS-CoV IgG antibody titers for paired serum sam- Figure 2 . Changes in severe acute respiratory syndrome-associated coronavirus-specific IgG antibody avidity in paired serum samples ples were measured by an in-house indirect immunofluorescence assay that has been described elsewhere [12] . This was done so that the value of using changes in IgG antibody titers and that in antibody avidity could be compared with respect to demonstrating a recent infection.",
            "cite_spans": [
                {
                    "start": 291,
                    "end": 295,
                    "text": "[12]",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [
                {
                    "start": 56,
                    "end": 64,
                    "text": "Figure 2",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "For the 90 serum samples, the optical-density values obtained during the first step ranged from 0.639 to 3.509 (mean \u202b\u05e2\u202c SD, 2.057 \u202b\u05e2\u202c 0.862); 31 samples had an OD of 12.5 and, thus, required further dilution for the second step. Of these, 19 required further dilution of 1:2, and 12 required further dilution of 1:5, to achieve a reference OD of \u04402.5 before they were subjected to the second step for measurement of antibody avidity. Figure 1 shows the pattern of maturation of anti-SARS-CoV IgG antibody avidity after infection. The avidity indices were low (mean \u202b\u05e2\u202c SD, 30.8% \u202b\u05e2\u202c 11.6%; range, 7.4%-51.8%) among the 45 samples collected \u044050 days after fever onset and increased to intermediate levels (mean \u202b\u05e2\u202c SD, 52.1% \u202b\u05e2\u202c 14.1%; range, 25.9%-73.1%) among the 11 samples collected between days 51 and 90. The avidity indices were high (mean \u202b\u05e2\u202c SD, 78.1% \u202b\u05e2\u202c 8.0%; range, 61.0%-94.7%) among the 34 samples collected after day 90.",
            "cite_spans": [
                {
                    "start": 167,
                    "end": 171,
                    "text": "12.5",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 435,
                    "end": 443,
                    "text": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Results."
        },
        {
            "text": "Of the 45 samples collected \u044050 days after fever onset, only 18 (40.0%) were positive for anti-SARS-CoV IgM antibody, as determined by the ELISARS assay. For the 18 IgM-positive samples, the avidity indices ranged from 13.3% to 46.8%, with a median of 32.3% and an interquartile range of 23.1%-37.6%. For the 27 IgM-negative samples, the avidity indices ranged from 7.4% to 51.8%, with a median of 31.9% and an interquartile range of 21.8%-40.8%. There was no significant difference in avidity level between these 2 groups of samples (P p .746, Mann-Whitney U test).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results."
        },
        {
            "text": "All together, 1 sample was available from 35 patients, 2 samples were available from 23 patients, and 3 samples were available from 3 patients. The results for the 26 patients with at least 2 samples were further analyzed (the third samples from the 3 patients with 3 samples were not considered). Their first samples were collected between days 17 and 54 (mean \u202b\u05e2\u202c SD, 32.3 \u202b\u05e2\u202c 9.6 days) after fever onset, and the time span between collection of the first and second samples ranged from 18 to 253 days (mean \u202b\u05e2\u202c SD, 128.8 \u202b\u05e2\u202c 73.6 days). Of the 26 paired samples, only 6 (23.1%) showed a significant (\u04434-fold) increase in anti-SARS-CoV IgG antibody titer (as determined by an in-house indirect immunofluorescence assay) from the first to the second sample, a result that could be regarded as evidence of recent infection. When the antibody avidity indices for the 26 paired samples were analyzed, they all showed an increase in avidity index with time. The changes in avidity levels for the paired samples are shown by collection time interval in figure 2.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results."
        },
        {
            "text": "Discussion. Our data show that anti-SARS-CoV IgG antibody avidity is low during primary infection and increases with time in a unidirectional manner. On the basis of this phenomenon, measurement of antibody avidity can be used to resolve certain difficulties that may be encountered in assessment of SARS-CoV infection. First, it can be used to differentiate between primary infection and reexposure. Although it was not possible to include patients who had been reexposed in the present study, on the basis of experience with other viral infections that have a similar pattern of antibody avidity maturation [13] , it is reasonable to infer that patients reexposed to SARS-CoV will mount a humoral memory immune response that includes the production of antibodies with high avidity within a short period of time. Second, the presence of antibodies with low avidity could provide alternative evidence for demonstrating a primary infection when the IgM assay result is in doubt. This is important, given that viral serological testing based solely on the determination of the presence of IgM can lead to false conclusions, because IgM responses last for only a very short period of time and could be missed if serum samples are collected too early or too late [14] . On the other hand, IgM can persist for months or even years after primary infection and reappear during secondary infection [15] . When interpreting our IgM results, one should be aware that the IgM assay used in the present study was based on the indirect enzyme immunoassay format, the sensitivity of which might be inferior to that of the IgM capture format, and that our omission of the IgG antibody removal step might have decreased the assay's sensitivity. Nevertheless, our IgM assay results for the 45 samples collected \u044050 days after fever onset support the view that low antibody avidity could be a valuable alternative marker for defining primary infection, in particular when serum sample availability is limited in terms of collection time points. Our data show that all 36 samples with an avidity index of !40% were collected before day 50, whereas all 39 samples with an avidity index of 155% were collected after day 65. Avidity indices between 40% and 55% could be considered to represent \"the maturation zone,\" in which the correlation between avidity and time since infection is less strong. Third, our comparison of the avidity indices for the 26 paired samples indicated that this approach could provide a helpful alternative to the use of increasing antibody concentration as serological evidence of recent infection. This is particularly important if convalescent samples are collected when antibody concentrations are no longer increasing, as was the case for most of our paired samples. Other possible applications of measurement of antibody avidity to SARS-CoV infection include use of the technique to assess humoral responses to vaccine candidates and to discriminate between primary and secondary vaccine failures.",
            "cite_spans": [
                {
                    "start": 609,
                    "end": 613,
                    "text": "[13]",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 1259,
                    "end": 1263,
                    "text": "[14]",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 1390,
                    "end": 1394,
                    "text": "[15]",
                    "ref_id": "BIBREF15"
                }
            ],
            "ref_spans": [],
            "section": "Results."
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "World Health Organization. Summary of probable SARS cases with onset of illness from 1",
            "authors": [],
            "year": 2002,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Laboratory-acquired severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "L"
                    ],
                    "last": "Lim",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kurup",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Gopalakrishna",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "N Engl J Med",
            "volume": "350",
            "issn": "",
            "pages": "1740--1745",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Severe acute respiratory syndrome (SARS) in Taiwan, China",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Update 4: review of probable and laboratory-confirmed SARS cases in southern China",
            "authors": [],
            "year": 2004,
            "venue": "World Health Organization",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Investigation into China's recent SARS outbreak yields important lessons for global public health",
            "authors": [],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Changes in antibody avidity after virus infections: detection by an immunosorbent assay in which a mild protein-denaturing agent is employed",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Inoue",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Hasegawa",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Matsuno",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Katow",
                    "suffix": ""
                }
            ],
            "year": 1984,
            "venue": "J Clin Microbiol",
            "volume": "20",
            "issn": "",
            "pages": "525--534",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Avidity of IgG in serodiagnosis of infectious diseases",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Hedman",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Lappalaninen",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Soderlund",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Hedman",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "Rev Med Microbiol",
            "volume": "4",
            "issn": "",
            "pages": "123--132",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Comparative evaluation of the use of immunoblots and of IgG avidity assays as confirmatory tests for the diagnosis of acute EBV infections",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Schubert",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Zens",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Weissbrich",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "J Clin Virol",
            "volume": "11",
            "issn": "",
            "pages": "161--72",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Differential maturation of avidity of IgG antibodies to gp41, p24 and p17 following infection with HIV-1",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "I"
                    ],
                    "last": "Thomas",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Wilson",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "O&apos;toole",
                    "suffix": ""
                }
            ],
            "year": 1996,
            "venue": "Clin Exp Immunol",
            "volume": "103",
            "issn": "",
            "pages": "185--91",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "The relative functional affinity of specific anti-core IgG in different categories of hepatitis B virus infection",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "I"
                    ],
                    "last": "Thomas",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "J Med Virol",
            "volume": "51",
            "issn": "",
            "pages": "189--97",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Severe acute respiratory syndrome",
            "authors": [],
            "year": 2004,
            "venue": "Centers for Disease Control and Prevention",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Immunofluorescence assay for serologic diagnosis of SARS",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "K"
                    ],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "C"
                    ],
                    "last": "Chan",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Emerg Infect Dis",
            "volume": "10",
            "issn": "",
            "pages": "530--532",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Differentiation of primary from nonprimary genital herpes infections by a herpes simplex virus-specific immunoglobulin G avidity assay",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Hashido",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Inouye",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kawana",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "J Clin Microbiol",
            "volume": "35",
            "issn": "",
            "pages": "1766--1774",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Hepatitis A virus infection among the hemophilia population at the Bonn Hemophilia Center",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "H"
                    ],
                    "last": "Brackmann",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Oldenburg",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "M"
                    ],
                    "last": "Eis-Hubinger",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Gerritzen",
                    "suffix": ""
                },
                {
                    "first": "U",
                    "middle": [],
                    "last": "Hammerstein",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Hanfland",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Vox Sang",
            "volume": "67",
            "issn": "1",
            "pages": "3--7",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Serological diagnosis of tick-borne encephalitis by determination of antibodies of the IgM class",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Roggendrof",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Heinz",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Deinhardt",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Kunz",
                    "suffix": ""
                }
            ],
            "year": 1981,
            "venue": "J Med Virol",
            "volume": "7",
            "issn": "",
            "pages": "41--50",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Severe acute respiratory syndrome-associated coronavirus-specific IgG antibody avidity in relation to time after fever onset",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}