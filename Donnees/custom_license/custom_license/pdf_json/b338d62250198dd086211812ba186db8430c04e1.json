{
    "paper_id": "b338d62250198dd086211812ba186db8430c04e1",
    "metadata": {
        "title": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore",
        "authors": [
            {
                "first": "J",
                "middle": [
                    "S E"
                ],
                "last": "Chan",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J",
                "middle": [
                    "J L"
                ],
                "last": "Ithnin",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "F",
                "middle": [
                    "W L"
                ],
                "last": "Goy",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "R",
                "middle": [
                    "W L"
                ],
                "last": "Sng",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "B",
                "middle": [
                    "L"
                ],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "We would like to report our experience from the restructuring of our obstetric anaesthesia training program in order to mitigate the impact of infection control measures during the COVID-19 outbreak in Singapore. Our institution, KK Women's and Children's Hospital, is responsible for obstetric anaesthesia training for the Singhealth Anaesthesiology Residency Program and has about 90 residents rotating through three different institutions under the Singhealth cluster of healthcare institutions.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore"
        },
        {
            "text": "Our national 'Disease Outbreak Response System Condition' alert level was escalated to the second highest of 'Orange' on February 7 2020, signifying severe disease with limited community spread. 1 From that time, a team-based segregation roster was implemented. Specialists and residents were divided into two teams, with each team rostered to clinical areas that were located on two separate levels within the hospital. Staff performing overnight duties were also from the same team. The purpose was to minimise staff interactions and reduce the impact of mandatory staff quarantine orders in the event of an outbreak in our healthcare system. These measures had a significant impact on the delivery of curricular training, which required interactions between learners and consultants. These included classroombased teaching, clinical supervision, procedural skills and simulation-based training, as well as trainee assessment and feedback. In the absence of routine face-to-face meetings, we anticipated and experienced difficulties in each of these educational elements.",
            "cite_spans": [
                {
                    "start": 195,
                    "end": 196,
                    "text": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore"
        },
        {
            "text": "Consequently, our department drew on the challenges learnt with respect to providing continuing medical education during the SARS-virus crisis in 2003 and modified and implemented some of these measures. 2 We hope that describing these measures will be useful to the readership in times of current and future outbreaks. With the suspension of classroom-based teaching, we selected a video-conferencing platform to conduct the webcast lectures. 3, 4 The real-time display of presentation slides, ease of internet connectivity, ease of use on laptops and mobile phones as well as the ability to record for subsequent playback all enhanced the learning experience of the residents and consultants. The latter was essential as the shift roster and clinical duties did not always allow for protected teaching time for residents to join in the teaching session. Reflective questions (including those about clinical management, resource management, communications and interprofessional collaborations) were posed to the residents during these sessions and were discussed with the consultant facilitating the session over the webcast. Unfortunately, procedural skills training on epidural trainers, and simulation sessions involving obstetric difficult airway management on mannequins in clinical areas, could not be replaced with webcast lectures. To overcome this, we looked into using a pilot virtual reality difficult airway gaming scenario to practice decision-making at critical junctures of the crisis scenario. 5, 6 Virtual reality sets were readily available and portable but needed disinfection after each use. The results from this form of learning are being analysed.",
            "cite_spans": [
                {
                    "start": 204,
                    "end": 205,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 444,
                    "end": 446,
                    "text": "3,",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 447,
                    "end": 448,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1511,
                    "end": 1513,
                    "text": "5,",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1514,
                    "end": 1515,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore"
        },
        {
            "text": "In addition to staff segregation, there were other educational impacts. Clinical learning decreased due to cancellation of non-essential surgery. Increased clinical and psychological stress from managing 'high infection risk' cases superseded the clinical teaching that might otherwise occur in a non-outbreak setting. 7 It was also natural for our consultants to manage 'high infection risk' patients intra-operatively themselves, so as to minimize resident exposure. 2 Nonetheless, we encouraged the consultant-resident pair, assigned to the same clinical area, to do a clinical and wellbeing debrief at the end of every shift.",
            "cite_spans": [
                {
                    "start": 319,
                    "end": 320,
                    "text": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 469,
                    "end": 470,
                    "text": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore"
        },
        {
            "text": "The robustness of resident assessment and feedback was significantly impacted. As only consultants in the same team could asses the resident, the number of available assessors was reduced, which may have resulted in less robust or suboptimal feedback on performance. Moreover, performance could be affected in the direct observation of procedural skills competency assessments by the added psychological stress of additional infection control measures, such as the need for personal protective equipment. Thus, we limited the performance of the direct observation of procedural skills assessments to patients who had been assessed using our institutional guidelines as of 'low infection risk'. This required co-ordination between the surgical, anaesthetic and infection control teams.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore"
        },
        {
            "text": "Movement of residents within the different institutions was restricted and thus their planned rotation in obstetric anaesthesia was extended. This necessitated curricular adjustments, as some residents had already completed the requisite training and achieved the competencies required at their level of residency training. We put in place learning and developmental programmes for the residents that allowed them to progress to the next level of obstetric anaesthesia training. The impact on the COVID-19 outbreak on training and mitigating measures is summarised in Table 1 .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 568,
                    "end": 575,
                    "text": "Table 1",
                    "ref_id": null
                }
            ],
            "section": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore"
        },
        {
            "text": "During infective outbreaks, the main priority of healthcare systems is to manage the disease outbreak and, inevitably, residency training is not the main priority. Residents' learning is likely to be affected during this period due to diminished clinical and didactic teaching, so we instituted measures to minimise the impact on their learning. However, as the outbreak situation is evolving continually, the training program needs to be reviewed regularly to adapt to any new changes and restrictions. We understand that different practices and differing severity of the outbreak overseas might limit the transferability of our measures, but we hope that readers will be able to adapt our experience to their local context. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Resilience of the restructured obstetric anaesthesia training program during the COVID-19 outbreak in Singapore"
        },
        {
            "text": "Email address: john.lee.s.e@singhealth.com.sg",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Department of Women's Anaesthesia, KK Women's and Children's Hospital, Singapore"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "What do the different DORSCON levels mean? Available at",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Government Of Singapore",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "SARS-Ed\": severe acute respiratory syndrome and the impact on medical education",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Sherbino",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Atzema",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Ann Emerg Med",
            "volume": "44",
            "issn": "",
            "pages": "229--260",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Teleconferencing in medical education: A useful tool",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lamba",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Aus Med J",
            "volume": "4",
            "issn": "",
            "pages": "442--449",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "The effectiveness of webcast compared to live lectures as a teaching tool in medical school",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "P"
                    ],
                    "last": "Vaccani",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Javidnia",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Humphrey-Murto",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Med Teach",
            "volume": "38",
            "issn": "",
            "pages": "59--63",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Teaching difficult airway management: is virtual reality real enough?",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "L"
                    ],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "K"
                    ],
                    "last": "Menon",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Intensive Care Med",
            "volume": "31",
            "issn": "",
            "pages": "504--509",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Overview of serious gaming and virtual reality",
            "authors": [
                {
                    "first": "T",
                    "middle": [
                        "P"
                    ],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Sherman",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Gerard",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "2019",
            "issn": "",
            "pages": "29--38",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "COVID-19 in Singapore -current experience: critical global issues that require attention and action",
            "authors": [
                {
                    "first": "Jel",
                    "middle": [],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "S"
                    ],
                    "last": "Leo",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Tan",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.2467"
                ]
            }
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "J.S.E. Lee, J.J.L. Chan, F. Ithnin, R.W.L. Goy, B.L. Sng",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}