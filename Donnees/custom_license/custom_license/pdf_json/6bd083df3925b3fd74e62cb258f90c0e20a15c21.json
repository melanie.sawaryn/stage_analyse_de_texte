{
    "paper_id": "6bd083df3925b3fd74e62cb258f90c0e20a15c21",
    "metadata": {
        "title": "A study on the psychological needs of nurses caring for patients with coronavirus disease 2019 from the perspective of the existence, relatedness, and growth theory *",
        "authors": [
            {
                "first": "Xue",
                "middle": [],
                "last": "Yin",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Tongji Medical College of Huazhong University of Science and Technology",
                    "location": {
                        "settlement": "Wuhan",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Lingdan",
                "middle": [],
                "last": "Zeng",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Tongji Medical College of Huazhong University of Science and Technology",
                    "location": {
                        "settlement": "Wuhan",
                        "country": "China"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Objective: This study aimed to explore the psychological needs of nurses caring for patients with coronavirus disease 2019 and to propose corresponding interventions. Methods: In-depth interviews were conducted with 10 nurses who cared for patients with COVID-19. Interview data were analyzed by category analysis from the perspective of the existence, relatedness, and growth theory (ERG). Results: The existence needs were mainly reflected in health and security needs, whereas the relatedness needs consisting mainly of interpersonal needs, humanistic concern needs, and family needs; further, the growth needs were mainly reflected as a strong need for knowledge. Existence needs were the main needs during the epidemic, with health and security needs influencing each other. Humanistic concern needs were the most important of the relatedness needs; interpersonal and family needs were also growing. Conclusion: It is found that the existence, relatedness, and growth needs coexist in clinical nurses. It is helpful to take effective interventions to meet their needs if the needs of nurses caring for COVID-19 patients could be perceived well. \u00a9 2020 Chinese Nursing Association. Production and hosting by Elsevier B.V. This is an open access article under the CC BY-NC-ND license (http://creativecommons.org/licenses/by-nc-nd/4.0/).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "In the exploration of nursing staff incentive mechanism, nurses' professional needs and patients' psychological needs, the existence, relatedness, and growth (ERG) theory has been used to establish a better nursing staff incentive mechanism, improve nurses' professional status and analyze the patient's psychological needs, more importantly, it promotes the development and implementation of medical humanistic care.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "It is found that the existence, relatedness, and growth needs coexist in clinical nurses. The existence needs were mainly reflected in health and security needs, whereas the relatedness needs consisting mainly of interpersonal needs, humanistic concern needs, and family needs; further, the growth needs consisting mainly of knowledge needs.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "In December 2019, an epidemic of coronavirus disease 2019(COVID-19) was discovered in Wuhan, Hubei Province, China [1] . The virus is highly transmissible, but the source and route of transmission have yet to be determined [2] . Clinical nurses experienced great stress when they were fighting against COVID-19 with needs for health, safety, interpersonal relationships, and related knowledge. Alderfer [3] proposed the existence, relatedness, and growth (ERG) theory of humanistic needs on the basis of Maslow's hierarchy of needs. He believed that people have three core needs, namely a need for survival, a need for relationships, and a need for growth and development. The purpose of this study is to use in-depth interviews to understand the psychological needs of front-line nurses working in extraordinary epidemic situations, and to analyze the main content of their psychological needs from the lens of the ERG theory and to provide a perspective for interventions to alleviate the psychological stress of nurses at the front-line.",
            "cite_spans": [
                {
                    "start": 115,
                    "end": 118,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 223,
                    "end": 226,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 403,
                    "end": 406,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "In this study, a purposive sampling method was used to select nurses from a tertiary general hospital in Wuhan who had cared for patients with COVID-19. Inclusion criteria: registered nurses at the front-line; having been caring for COVID-19 patients for more than one week; voluntary participation in the study. A total of 10 nurses were interviewed; the sample size was based on the saturation of information [4] . Among the interviewed nurses, nine were female and one was male, aged 25e38 (29.9 \u00b1 3.62) years. Three of them had 5-year or less of nursing experience, five had 5e10 years of nursing experience, and two had 10 or more years of nursing experience. Three of them had a junior college diploma, six with a bachelor's degree and one with a master's degree. Seven of them had experience in critical care nursing. Before they began to care for COVID-19 patients, they worked in the neurology department, the emergency department, the cardiac surgery department, the hematology department, and the infectious diseases department, respectively.",
            "cite_spans": [
                {
                    "start": 411,
                    "end": 414,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Participants"
        },
        {
            "text": "A semi-structured, personal, in-depth interview method was adopted. The outline of the interview was formulated based on the format of interviews from previous literature reviews. Two nurses were pre-interviewed to adjust and refine the outline of the interview. The outline of the interview was as follows: (1) Please tell us your feelings about caring for COVID-19 patients over the past few days. (2) What are your biggest needs or expectations right now? (3) How has taking care of COVID-19 patients affected your personal life? (4) Do you have any lived experiences and feelings about this job differ from before? Interviews were conducted by researchers who had been trained in qualitative research interviewing techniques. Before the interview, the interviewees were told about the purpose, content, and significance of the study in order to earn their trust and cooperation. Interviews were conducted in a quiet and well-ventilated room, which would be disinfected after completing an interview. Both the interviewer and interviewee wore protective masks with a distance of at least 1 meter from each other, and the duration of the interview time was kept within 30 min as much as possible. A voice recorder was used to record during interviews with the consent of the interviewees. In addition, non-verbal cues by the interviewee, such as eye movements and gestures, were noticed and recorded. The interviewees were coded as N1eN10 and their privacy was protected in accordance with the principles of confidentiality [5] . This study was approved by the hospital ethics committee.",
            "cite_spans": [
                {
                    "start": 1526,
                    "end": 1529,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Data collection"
        },
        {
            "text": "Within 24 hours after each interview, the researchers carefully read the interview notes and transcribed the interview record verbatim under the recollection of the interview scene. QSR Nvivo-8 [6] was used for data encoding and analysis. A category analysis method was used to classify data with the same attributes into a theme and a name was given to it. Based on the encoding of the interview text, the number of individuals that initially mentioned each encoding and the number of times mentioned was counted. The main encoding was then extracted and integrated to form an encoding system. Finally, we extracted the main content of the psychological needs of clinical nurses from the encoding system based on the ERG theory.",
            "cite_spans": [
                {
                    "start": 194,
                    "end": 197,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Data analysis"
        },
        {
            "text": "Encoding and analysis of the interview data showed that, from the perspective of the ERG theory, among the psychological needs of clinical nurses, the existence needs primarily manifested as needs for health and safety. The need for health refers to the nurse's attention to their own physical and mental health, and the need for safety refers to the hope for adequate personal protective equipment (PPE) and the emotional stability of patients' family members. The relatedness need is primarily manifested as a need for interpersonal relationships, a need for community concern, and a need for affection. During the period of lockdown in Wuhan, the need for interpersonal relationships specially reflected the desire of clinical nurses to communicate face-to-face with family members, colleagues, and friends. The need for community concern manifests as nurses' need for care, help, and support from department heads, the hospital, and the outside world. The need for affection reflected their desire for family affection is stronger than usual. The growth need is manifested as a strong need for knowledge about COVID-19 prevention and control, especially from authoritative reports. Specific categories and narrative examples are shown in Table 1 .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 1242,
                    "end": 1249,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "Summary of psychological needs reflected in nurses"
        },
        {
            "text": "Our analysis showed that 80% of the respondents exhibited existence, relatedness, and growth needs, and these three needs affected each other. N1: \"I hope my own immune system can eliminate the virus.\"; \"It's been a long time since I've seen any colleagues that I used to work with in the past.\"; \"There is too much information about COVID-19 every day. Much of it is rumors, and I wish there were more official reports from the authorities.\" N4: \"I hope that I won't become infected by the virus.\"; \"I miss the days when we could talk to each other without face masks\"; \"I hope Chinese scientists can find the source of infection and develop a vaccine as soon as possible.\" N8: \"I hope that personal protective equipment is available every day so that I don't have to worry as much about myself or my colleagues getting infected.\"; \"I hope that the community hospital at home also provides sufficient medical services so that I feel more at ease at work and less worried about my family.\" N10: \"There is a shortage of personal protective equipment in some hospitals right now. The virus still can't be treated with specific drugs. I feel really anxious and scared.\" It is apparent that most of the interviewees had existence, relatedness, and growth needs simultaneously, and interacting effects were found among these needs. When any of these needs were fulfilled well, others may decrease correspondingly.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Existence, relatedness, and growth needs coexist and influence each other"
        },
        {
            "text": "The interviews showed that the need for existence is currently the primary need in nurses. Physical health is a basic necessity required to overcome the epidemic, and all respondents exhibited a strong need for maintaining health. N2: \"Now, I eat fruit and take vitamin C supplements every day to strengthen my immunity.\" N5: \"I now practice yoga and do aerobic exercises every day at home follow the guide on TV in order to get rid of toxins and strengthen my immunity. Now is the time to work hard to build my immunity.\" N6: \"I haven't been on my special diet to lose weight during this time. The nursing department issued a notice saying that I should maintain a ketogenic diet to enhance immunity and I think that having more chicken soup can alleviate some common cold symptoms. Now I eat a lot every day.\" It is also found that the needs for health and safety among clinical nurses interacted with each other; when the need for safety was fulfilled, the need for health may decrease. When asked about the current biggest needs or expectations for clinical nurses, both N3 and N5 responded: \"I recently saw news reports that a continuous stream of personal protective equipment was being shipped to Wuhan, and I felt less worried [about my health].\" N7: \"Although the virus can survive in the air for a period, I'm not too worried about getting infection as long as I wear a mask, have fresh air, and take proper safety precautions.\" 3.2.3. The need for community concern is foremost among relatedness needs, and the needs for interpersonal relationships and affection are continuously increasing Through coding and analysis, it showed that the need for community concern was foremost among the relatedness need. All interviewees emphasized the need for community concern, hoping to get the care, help, and support from leaders of department and hospitals, and the outside world. N6: \"To be honest, I was very apprehensive before coming to the infectious department assupport staff, but on the first day here, the head nurse personally explained relevant knowledge such as disinfection and quarantine, and that helped me calm down a lot.\" N7: \"I hope that the hospital sets up a psychological support task force to ease our tension and fears.\" N4: \"I hope that our society and government pay more attention to lack of personal protective equipment.\" 80% of the interviewees indicated needs for interpersonal relationships and affection. Due to the impact of the epidemic, the needs for interpersonal relationships and affection are temporarily suppressed, thereby causing these needs to increase continuously. N2: \"Now, I go straight to my rented apartment after work every day. I can't see my family or go shopping with my classmates. I hope we overcome the epidemic and return to normal life as soon as possible.\" N9: \"In the past, it was no big deal when everyone worked together. Now everyone needs to wear personal protective equipment to avoid cross-infection. Everyone is afraid to go out with each other too much after work. I miss the days when we could just talk and laugh together.\" N5: \"I stay at a hotel every day and am afraid of getting my family sick. I'm afraid to go home and haven't seen my mom and dad in a long time.\" N7: \"There are already cases of children becoming infected. I'm afraid to go home and transmit the virus to my daughter, but I miss her terribly and just want to hug and kiss her.\" N9: \"I haven't been home for a year. I plan to go back to my hometown to see my parents this year. But now I can't, and I don't know what their health is like.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The need for existence is currently the primary need in clinical nurses"
        },
        {
            "text": "Because the current outbreak is caused by a novel coronavirus never been found in humans before, knowledge of the virus itself and the disease it is causing is still under constant investigation. Our interviews showed that clinical nurses have a strong need for knowledge about novel coronavirus that runs through all of their psychological needs. Around 90% of respondents said that they would like to know more about the novel coronavirus. N1: \"I hope to ease my own fears by knowing more correct information about the virus.\" N2: \"Because I can't meet with my colleagues, friends, or relatives every day, I basically spend the free time browsing the web, hoping to get useful information about novel coronavirus pneumonia prevention.\" N5: \"There is too much information about novel coronavirus every day. Much of it is rumors, and I wish there were more official reports from the authorities.\" N9: \"I see a few hundred new confirmed cases every day, and I feel panicked, but seeing that researchers have developed diagnostic kits, I feel that we are not far from overcoming the epidemic.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The need for knowledge within growth need is very strong"
        },
        {
            "text": "The psychological needs of nurses caring for COVID-19 patients were investigated from the perspective of the ERG theory. It was found that needs for existence, relatedness, and growth coexisted among clinical nurses and affected each other. This is also consistent with the idea proposed by the ERG theory that \"an individual may have more than one need at the same time\". The interviews showed that clinical nurses have needs at different levels. This is consistent with the idea that \"Even if a person's needs for existence and relatedness have not been fully met, he can still work toward developing the need for growth\", as stated by the ERG theory.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Although there are currently no specific drugs or vaccines effective against COVID-19, the chance of infection can be reduced by following scientific protective measures [7] . Currently, governmental agencies and hospital managers are making every effort to collect and distribute personal protective equipment to ensure the \"I hope that personal protective equipment is available every day.\" (N8) \"I hope we never have another incident where medical personnel are put at risk.\" (N6) Relatedness Need for interpersonal relationships 8 1 5 \"It's been a long time since I've seen any colleagues that I used to work with in the past.\" (N1) \"I miss the days when we could talk to each other without face masks.\" (N4)",
            "cite_spans": [
                {
                    "start": 170,
                    "end": 173,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Need for community concern 10 20 \"I hope that our society and government pay attention to our lack of personal protective equipment.\" (N4) \"I hope that the hospital sets up a psychological support task force to ease our tension and fears.\" (N7) Need for affection 8 14 \"I stay at a hotel every day and am afraid of getting my family sick. I'm afraid to go home and haven't seen my mom and dad for a long time.\" (N5) Growth Need for knowledge 9 19 \"There is too much information about novel coronavirus every day. Much of it is rumors, and I wish there were more official reports from the authorities.\" (N5) safety needs of the front-line. Adequate supplies, Scientific and reasonable utilization of personal protective equipment would fulfill the needs for health and safety among clinical nurses. The interviews showed that humanistic community concern for nurses is necessary, especially in the extraordinary circumstances during the epidemic. Establishing psychological coping task forces with help of the nursing department and psychological experts, and setting up psychological support platforms to provide community support for healthcare professionals, may contribute to fulfill the needs of nurses and protect their mental health. Though face-to-face communication reduced during the epidemic, the needs for interpersonal relationships and affection in clinical nurses can be enhanced through other ways for emotional expression, such as colleagues encouraging each other during work shifts, writing good-luck messages on personal protective equipment, enjoying lunches provided by social volunteers, and so on.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The interviews showed that clinical nurses have a strong need for knowledge, which may bring a lack of security. This is also consistent with the concept of \"frustration-regression\" proposed in ERG theory, which states that when higher-level need is not met, lower-level needs may be increased as a substitute. Therefore, under the direction of the Chinese Center for Disease Control and Prevention and the guidance of the National Health Commission, promptly training of knowledge on prevention and control of COVID-19 would help reduce psychological panic and insecurity caused by inadequate knowledge.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Limitations exist in this study include the short time of each interview and the tight time to analyze the data. More valuable information may be found if plenty of time is engaged.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "During extraordinary epidemic situations, needs for existence, relatedness, and growth coexisted among clinical nurses and affected each other. The existence needs were mainly reflected in health and security needs, whereas the relatedness needs consisted mainly of interpersonal needs, humanistic concern needs, and family needs; further, the growth needs consisted mainly of knowledge needs. More attention should be paid to clinical nurses' needs to protect their health.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusions"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "National Health Commission and National Administration of Traditional Chinese Medicine of the People's Republic of China. Protocols for diagnosis and treatment of COVID-19",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Special expert group for control of the epidemic of novel coronavirus pneumonia of the Chinese Preventive Medicine Association. An update on the epidemiological characteristics of novel coronavirus pneumonia",
            "authors": [],
            "year": 2020,
            "venue": "Chin J Vitral Dis",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.16505/j.2095-0136.2020.0015"
                ]
            }
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "An empirical test of a new theory of human needs",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "P"
                    ],
                    "last": "Alderfer",
                    "suffix": ""
                }
            ],
            "year": 1969,
            "venue": "Organ Behav Hum Perform",
            "volume": "4",
            "issn": "2",
            "pages": "142--75",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Beijing: People's Medical Publishing House",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "A qualitative study on the community concern behaviors of head nurses at a tertiary hospital in Wuhan",
            "authors": [
                {
                    "first": "Y",
                    "middle": [
                        "L"
                    ],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "F"
                    ],
                    "last": "Liao",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "Y"
                    ],
                    "last": "Hu",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Med Soc",
            "volume": "9",
            "issn": "",
            "pages": "37--46",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Qualitative research on the psychological needs of the elderly from the perspective of ERG theory",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Cao",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "An",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Chin J Clin Psychol",
            "volume": "2",
            "issn": "",
            "pages": "343--348",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "5 novel coronavirus pneumonia patients with mild disease and nursing care Academic Journal of Chinese PLA Medical School",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "H"
                    ],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "M"
                    ],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "X"
                    ],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "Main types and examples of psychological needs of nurses.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "The authors would like to thank the nurses who participated in the interview and express high respect for nurses' hard work during the outbreak of COVID-19.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgment"
        },
        {
            "text": "Supplementary data to this article can be found online at https://doi.org/10.1016/j.ijnss.2020.04.002.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Appendix A. Supplementary data"
        }
    ]
}