{
    "paper_id": "b50b938a9439979f0b39bd6c2f5a6193b5d3aec4",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "T",
                "middle": [],
                "last": "Atefehzandifar",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Alborz University of Medical Sciences",
                    "location": {
                        "settlement": "Karaj"
                    }
                },
                "email": ""
            },
            {
                "first": "Rahimbadrfam",
                "middle": [],
                "last": "B\u204e",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Historically, it has been nearly a hundred years since the first time in scientific literature that the temporal relationship between a viral infection and schizophrenia was discussed (Graves, 1928) . In a retrospective study, the highest birth rate among schizophrenic patients, after the 1957 influenza epidemic, was about 5 months after the peak of the A2 influenza outbreak. The study was conducted about 30 years after the epidemic and the time of birth assessment was in the range from two years before to two years after the epidemic peak (O'Callaghan et al., 1991) . However, there has been some controversy regarding the causal relationship between these two issues .The most important reason is the set of other factors that affect the disease (Selten et al., 2010) .",
            "cite_spans": [
                {
                    "start": 184,
                    "end": 198,
                    "text": "(Graves, 1928)",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 546,
                    "end": 572,
                    "text": "(O'Callaghan et al., 1991)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 754,
                    "end": 775,
                    "text": "(Selten et al., 2010)",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Dear Editor,"
        },
        {
            "text": "The link between the effects of the time of birth and schizophrenia is based on the theory of the influence of environmental factors on the formation of psychiatric disorders and in particular schizophrenia. It has been thought for many years that both individual and environmental factors are effective in the development of schizophrenia (Allardyce and Boydell, 2006) . The question that arises is how respiratory viruses can be effective in the etiopathology of neuropsychiatric disorders. According to some studies, some respiratory viruses have neuroinvasive capacities (Desforges et al., 2020) . In a study that examined human coronavirus anti-strains antibodies (229E, HKU1, NL63, and OC43) in patients with new psychotic symptoms compared to the control group, a higher level of antibodies was found in this group(especially anti-NL63 antibodies in patients with schizophrenia spectrum) (Severance et al., 2011) .",
            "cite_spans": [
                {
                    "start": 340,
                    "end": 369,
                    "text": "(Allardyce and Boydell, 2006)",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 575,
                    "end": 599,
                    "text": "(Desforges et al., 2020)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 895,
                    "end": 919,
                    "text": "(Severance et al., 2011)",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "Dear Editor,"
        },
        {
            "text": "Human respiratory coronaviruses have the ability to infect nerve cells and remain in the human brain. In some animal models, it has been shown that the virus can lead to direct neurological involvement. In addition, prominent spongiform-like degradation has been observed that can initiate underlying neuropathology (Jacomy and Talbot, 2003) . In some studies, schizophrenia has been described as a pathogenic autoimmune disease that is caused by the interaction of viruses, pathogens, and the immune system (Carter, 2011) . Also there are some reports about the coexistence of a genetically modified im-mune system with the activity of microorganisms such as viruses that have deleterious consequences for the central nervous system (Severance and Yolken, 2019) .",
            "cite_spans": [
                {
                    "start": 316,
                    "end": 341,
                    "text": "(Jacomy and Talbot, 2003)",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 508,
                    "end": 522,
                    "text": "(Carter, 2011)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 734,
                    "end": 762,
                    "text": "(Severance and Yolken, 2019)",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Dear Editor,"
        },
        {
            "text": "Accordingly, the increasing spread of COVID-19 has raised serious concerns that, in addition to the acute psychiatric problems associated with the present condition (Li et al., 2020) , the psychiatric consequences of the disorder particularly in the context of the increasing prevalence of schizophrenia, may become apparent in subsequent years. In this regard, health policy makers, while seeking more accurate epidemiological information and identifying different aspects of the activity of the virus, should pay attention to the different aspects of the psychiatric status of those affected.",
            "cite_spans": [
                {
                    "start": 165,
                    "end": 182,
                    "text": "(Li et al., 2020)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Dear Editor,"
        },
        {
            "text": "There is no conflict of interest.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Declaration of Competing Interest"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Environment and schizophrenia: review: the wider social environment and schizophrenia",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Allardyce",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Boydell",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Schizophr. Bull",
            "volume": "32",
            "issn": "",
            "pages": "592--598",
            "other_ids": {
                "DOI": [
                    "10.1093/schbul/sbl008"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Schizophrenia: a pathogenetic autoimmune disease caused by viruses and pathogens and dependent on genes",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Carter",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J. Pathog",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.4061/2011/128318"
                ]
            }
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Human coronaviruses and other respiratory viruses: underestimated opportunistic pathogens of the central nervous system? Viruses",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Desforges",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Le Coupanec",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Dubeau",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Bourgouin",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Lajoie",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Dub\u00e9",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Talbot",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "12",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.3390/v12010014"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Influenza in relation to the onset of acute psychoses",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Graves",
                    "suffix": ""
                }
            ],
            "year": 1928,
            "venue": "J. Neurol. Psychopathol",
            "volume": "9",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1136/jnnp.s1-9.34.97"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Vacuolating encephalitis in mice infected by human coronavirus OC43",
            "authors": [
                {
                    "first": ".",
                    "middle": [
                        "H"
                    ],
                    "last": "Jacomy",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Talbot",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Virology",
            "volume": "315",
            "issn": "",
            "pages": "323--327",
            "other_ids": {
                "DOI": [
                    "10.1016/S0042-6822(03)00323-4"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Progression of mental health services during the COVID-19 outbreak in China",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "- H"
                    ],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "- J"
                    ],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Cheung",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "- T"
                    ],
                    "last": "Xiang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Int. J. Biol. Sci",
            "volume": "16",
            "issn": "",
            "pages": "1732--1738",
            "other_ids": {
                "DOI": [
                    "10.7150/ijbs.45120"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Schizophrenia after prenatal exposure to 1957 A2 influenza epidemic",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "O&apos;callaghan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Sham",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Takei",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Murray",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Glover",
                    "suffix": ""
                }
            ],
            "year": 1991,
            "venue": "Lancet",
            "volume": "337",
            "issn": "",
            "pages": "1248--1250",
            "other_ids": {
                "DOI": [
                    "10.1016/0140-6736(91)92919-S"
                ]
            }
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Schizophrenia and 1957 pandemic of influenza: meta-analysis",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "- P"
                    ],
                    "last": "Selten",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Frissen",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Lensvelt-Mulders",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "A"
                    ],
                    "last": "Morgan",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Schizophr. Bull",
            "volume": "36",
            "issn": "",
            "pages": "219--228",
            "other_ids": {
                "DOI": [
                    "10.1093/schbul/sbp147"
                ]
            }
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Coronavirus immunoreactivity in individuals with a recent onset of psychotic symptoms",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "G"
                    ],
                    "last": "Severance",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "B"
                    ],
                    "last": "Dickerson",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "P"
                    ],
                    "last": "Viscidi",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Bossis",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "R"
                    ],
                    "last": "Stallings",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "E"
                    ],
                    "last": "Origoni",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Sullens",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "H"
                    ],
                    "last": "Yolken",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Schizophr. Bull",
            "volume": "37",
            "issn": "",
            "pages": "101--107",
            "other_ids": {
                "DOI": [
                    "10.1093/schbul/sbp052"
                ]
            }
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "From Infection to the Microbiome: An Evolving Role of Microbes in Schizophrenia",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "G"
                    ],
                    "last": "Severance",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "H"
                    ],
                    "last": "Yolken",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1007/7854_2018_84"
                ]
            }
        }
    },
    "ref_entries": {},
    "back_matter": [
        {
            "text": "None.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgement"
        }
    ]
}