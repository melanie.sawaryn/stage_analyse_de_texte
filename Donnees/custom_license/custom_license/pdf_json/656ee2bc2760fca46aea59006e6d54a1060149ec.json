{
    "paper_id": "656ee2bc2760fca46aea59006e6d54a1060149ec",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "Jonathan",
                "middle": [],
                "last": "Depierro",
                "suffix": "",
                "affiliation": {},
                "email": "jonathan.depierro@mssm.edu"
            },
            {
                "first": "Sandra",
                "middle": [],
                "last": "Lowe",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Craig",
                "middle": [],
                "last": "Katz",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "and Health System Design & Global Health",
                    "location": {
                        "addrLine": "World Trade Center Health Program Phone: 212-241",
                        "postBox": "Box 1230",
                        "postCode": "8462, 10029, 1305",
                        "settlement": "New York",
                        "region": "New York, NY",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Mount",
                "middle": [],
                "last": "Sinai",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "The COVID-19 pandemic will likely lead to high rates of PTSD, depression, and substance misuse among survivors, victims' families, medical workers, and other essential personnel. The mental health response to the 9/11/01 terrorist attacks, culminating in a federally-funded health program, provides a template for how providers may serve affected individuals. Drawing on the",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "9/11 experience, we highlight effective prevention measures, likely short and long-term treatment needs, vulnerable subgroups, and important points of divergence between 9/11 and the COVID-19 pandemic. Mental health monitoring, early identification of at-risk individuals, and treatment irrespective of financial barriers is essential for minimizing chronic distress.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Keywords: disaster mental health; PTSD; emergency responders At the time of this writing (April 14, 2020), over 1.9 million people across the world have been infected with COVID-19 and there have been approximately 120,000 deaths (Johns Hopkins University, 2020). In New York State, which has become one of the ground zeroes for this pandemic, there have been more than 196,000 infections and 10,000 deaths, far exceeding the number of lives lost on 9/11/01. However, several lessons learned from the mental health response to 9/11 should inform how providers and institutions meet the treatment need of those affected by COVID-19.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The mental health risk posed to 9/11 responders by the level of trauma to which they were exposed was likely worsened by lack of adequate monitoring of that level of exposure, especially the duration of their time at Ground Zero. Efforts should be made to ensure that the exposure of healthcare workers responding in the many ground zeros that our hospitals have become is monitored, even as the demand for their services rises with the infection rates.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Prevention"
        },
        {
            "text": "It is also important to note that 9/11 responders who were part of traditional first responder populations such as the police and fire service also likely had the mental health benefit of robust social networks provided by their affiliation with firehouses, stationhouses, and unions.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Prevention"
        },
        {
            "text": "Such social support offered them the protection that the many thousands of non-traditional responders, including unaffiliated volunteers who spontaneously came forward to help, had available to them less reliably (Pietrzak et al., 2014) . In the COVID-19 crisis, hospitals in particular should ensure that all possible efforts are made to provide social support to their staff, whether through hospital communications, supervisors, or formalized peer support groups.",
            "cite_spans": [
                {
                    "start": 213,
                    "end": 236,
                    "text": "(Pietrzak et al., 2014)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Prevention"
        },
        {
            "text": "Social support is a largely free, natural, and abundant resource that only needs to be channeled.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Prevention"
        },
        {
            "text": "Mental health support for 9/11 responders and survivors started soon after the attacks, as many volunteer mental health professions flooded makeshift family support centers and responder aid stations. This initial support has been described as a \"trauma tent\", a safe, supportive space surrounding affected individuals (Katz and Nathaniel, 2002) . These efforts became formalized within hospital-based clinics and charity-subsidized private treatment. Now, in the midst of COVID-19, again thousands of providers in New York have volunteered for mental health support services, many participating in city and state-organized efforts.",
            "cite_spans": [
                {
                    "start": 319,
                    "end": 345,
                    "text": "(Katz and Nathaniel, 2002)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Short-term treatment needs and barriers"
        },
        {
            "text": "With the benefit of technological advances, this aid can be deployed as effectively via video telehealth platforms. Significant issues were encountered securing space to see patients as the initial Mount Sinai 9/11 mental health treatment and monitoring program in New York was housed within the confines of an already bustling medical center, and modern advances in telepsychiatry can remedy this in the case of COVID-19. To address immediate COVID-19 related mental health needs for staff, the Mount Sinai Hospital System is currently deploying a range of mostly telehealth-based supportive mental health services, including support groups, individual therapy, and crisis lines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Short-term treatment needs and barriers"
        },
        {
            "text": "All of these efforts are likely to be fruitful and will alleviate much distress. However, several barriers should be noted. Many affected individuals may not make use of these shortterm services, due to mental health stigma, denial, dissociative defenses, conflicting time demands, or general discomfort with emotional disclosure (particularly in group support/debriefing). Many people with the highest level of need may not make use of services offered in the short-term. The Mount Sinai-based mental health program for 9/11 responders lowered these barriers by partnering and co-locating with medical colleagues who were addressing 9/11 related physical health issues, and finding comparable integration will likely be essential to ensure COVID-19 related mental health programming reaches the greatest possible number of affected people.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Short-term treatment needs and barriers"
        },
        {
            "text": "The mental health treatment of World Trade Center rescue, recovery and cleanup workers has highlighted the need for long-term monitoring of COVID-19 pandemic survivors, health care workers, other essential personnel (e.g. delivery, postal, and grocery store workers) and surviving family members. Health monitoring and treatment efforts for 9/11 survivors and responders were established soon after the attacks and continue to this day, and this work is now funded through the James Zadroga Act. Without similarly unified health registry and treatment services, it is likely that many individuals, particularly those from underserved groups, will experience chronic long-term mental health consequences while being unable to access highquality healthcare services.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Long-term health monitoring and treatment"
        },
        {
            "text": "Two vulnerable populations should be highlighted. First, high rates of post-traumatic stress disorder (PTSD), clinical depression, and recurrent alcohol use problems are anticipated among health care workers, now on the front lines of the COVID-19 pandemic, and serving as the most \"visible\" responder group during this crisis. Concerns have been raised around moral injury among healthcare workers given that hard choices may need to be made regarding rationing care, including ventilators (Williamson et al., 2020) . Moral injury has been associated with increased risk for psychiatric disorders and suicidal ideation (Bryan et al., 2018) . This issue recalls the identity conflicts faced by 9/11 uniformed responders: while trained to serve and protect others, many felt that they failed in this mission because so many people were killed and so few were rescued from the \"pile\" at Ground Zero. Just world assumptions were shattered, as they likely will be for medical personnel now.",
            "cite_spans": [
                {
                    "start": 491,
                    "end": 516,
                    "text": "(Williamson et al., 2020)",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 620,
                    "end": 640,
                    "text": "(Bryan et al., 2018)",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Vulnerable Populations"
        },
        {
            "text": "Second, special attention should be paid to the mental health outcomes of non-medical \"essential personnel,\" including governmental employees, healthcare administrators and support staff, and food delivery workers. This concern arises from striking parallels to the 9/11 experience. Non-traditional 9/11 responders (e.g. construction, cleanup, asbestos workers; city employees and volunteers) have consistently higher rates of chronic PTSD than uniformed responders (e.g. police), potentially related to having greater pre and post-9/11 life stressors, higher rates of pre-9/11 psychiatric diagnoses, and lower social support around the time of 9/11 (Pietrzak et al., 2014) . Further, this group mostly lacked disaster response experience and found themselves taking on tasks well outside the scope of their jobs, often not by choice but due to economic necessity.",
            "cite_spans": [
                {
                    "start": 650,
                    "end": 673,
                    "text": "(Pietrzak et al., 2014)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Vulnerable Populations"
        },
        {
            "text": "It is often said of disaster experience, \"If you have seen one disaster, you have seen one,\" reflecting the uniqueness of every disaster. Despite the many similarities between 9/11 and the COVID-19 pandemic, they must be seen through the lens of differences between two events, differences which suggest the possibility of even greater psychiatric fallout for the pandemic's responders. First, while the daily exposure of responders at Ground Zero took place over months, the acute impact proved to be more or less limited to 9/11/01 itself whereas the acute impact of COVID-19 is unfolding over at least months. Second, the militaristic conceptualization of the COVID-19 response as a \"war\" wherein healthcare professionals have been \"(re)deployed\" to work with COVID-19 patients underscores how obligation and immediate personal jeopardy may color this response far more than 9/11 ultimately did. Third, to the extent that problematic pre-disaster psychosocial circumstances are a risk factor for survivors' future risk of psychopathology, the heightened levels of political and social discord in the United States and elsewhere in the last several years is of concern.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Contrasts"
        },
        {
            "text": "The reverberations of the 2020 COVID-19 pandemic will likely be felt for many decades, as the impact of 9/11 is still felt by many, including New Yorkers, nearly 20 years after the attacks. The tremendous loss of life will invariably lead to adverse mental health effects in the population, including family members of victims, workers who tried valiantly to save them, and essential personnel that continued to serve the public in the face of ongoing, invisible life threat.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Concluding Thoughts"
        },
        {
            "text": "Rather than the sudden jolt of fear and horror that accompanied the 9/11 attacks, the COVID-19 pandemic will likely bring a more insidious wave of anxiety, anger, and grief as casualties increase. Widely-accessible short-term and long-term mental health treatment services, such as those provided following 9/11, may prove necessary to respond to this need. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Concluding Thoughts"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Moral injury, posttraumatic stress disorder, and suicidal behavior among National Guard personnel",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "J"
                    ],
                    "last": "Bryan",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "O"
                    ],
                    "last": "Bryan",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Roberge",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "R"
                    ],
                    "last": "Leifker",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "C"
                    ],
                    "last": "Rozek",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Psychological Trauma: Theory, Research, Practice, and Policy",
            "volume": "10",
            "issn": "",
            "pages": "36--45",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Coronavirus Global Cases by the Center for Systems Science and Engineering",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Disasters, Psychiatry, and Psychodynamics",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "L"
                    ],
                    "last": "Katz",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Nathaniel",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Journal of the American Academy of Psychoanalysis",
            "volume": "30",
            "issn": "",
            "pages": "519--529",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Trajectories of PTSD risk and resilience in World Trade Center responders: an 8-year prospective cohort study",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Moline",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Stellman",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [
                        "G"
                    ],
                    "last": "Udasin",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Landrigan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "M"
                    ],
                    "last": "Southwick",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Psychol. Med",
            "volume": "44",
            "issn": "",
            "pages": "205--219",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "COVID-19 and experiences of moral injury in front-line key workers",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Williamson",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Murphy",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Greenberg",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Occupational Medicine",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Lowe and DePierro are supported by CDC/NIOSH Contract 200-2017-93428. The contents of this article are solely the responsibility of the authors and do not necessarily represent the official views of CDC/NIOSH.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "The authors would like to thank Drs. Adriana Feder and Robert Pietrzak, who provided comments on an earlier version of this manuscript. The authors would also like to thank all of the rescue, recovery and cleanup workers and survivors who participate in the World Trade Center Health Program.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgements"
        },
        {
            "text": "The authors declare the following financial interests/personal relationships which may be considered as ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Declaration of Interest Statement"
        }
    ]
}