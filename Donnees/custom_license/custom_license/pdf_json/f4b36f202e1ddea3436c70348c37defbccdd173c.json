{
    "paper_id": "f4b36f202e1ddea3436c70348c37defbccdd173c",
    "metadata": {
        "title": "Regional Diversification of Influenza Activity in Poland During the 2015/16 Epidemic Season",
        "authors": [
            {
                "first": "K",
                "middle": [],
                "last": "Szyma\u0144",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health-National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska Street 24",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            },
            {
                "first": "D",
                "middle": [],
                "last": "Kowalczyk",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health-National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska Street 24",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            },
            {
                "first": "K",
                "middle": [],
                "last": "Cie\u015blak",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health-National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska Street 24",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            },
            {
                "first": "L",
                "middle": [
                    "B"
                ],
                "last": "Brydak Abstract",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health-National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska Street 24",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Influenza viruses are the most common cause of respiratory infections, resulting in high morbidity and mortality (Taubenberger and Morens 2008) . Influenza is an infectious disease caused by influenza virus type A or B. The occurrence of influenza, whose viruses are characterized by great seasonal antigenic changeability, may vary in different districts of a country. In Poland, epidemiological surveillance of influenza was implemented in collaboration with primary care physicians and laboratories of 16 Voivodeship (province) Sanitary Epidemiological Station (VSES) as of the 2004/05 epidemic season. The information on influenza and influenza-like infections has been gathered in all districts by means of both Sentinel and non-Sentinel systems. Since the 2013/14 epidemic season, data consisting of the number of samples tested, suspected and confirmed cases of infection, hospitalizations, and deaths in each province are stratified into seven age-groups (Hallmann-Szeli\u0144ska et al. 2016a, b; Bednarska et al. 2015) . These data are then further elaborated and randomly checked for correctness of viral identification in the National Influenza Center (NIC) at the National Institute of Public Health-National Institute of Health (NIPH-NIH 2016). The NIC serves as a reference center for individual virological VSES laboratories of the country. The aim of the present study was to determine the regional differences of influenza activity in Poland in the 2015/16 epidemic season. The savvy of such differences might be of help in setting apart resources for preventive and therapeutic actions to combat the epidemic. ",
            "cite_spans": [
                {
                    "start": 113,
                    "end": 143,
                    "text": "(Taubenberger and Morens 2008)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 963,
                    "end": 999,
                    "text": "(Hallmann-Szeli\u0144ska et al. 2016a, b;",
                    "ref_id": null
                },
                {
                    "start": 1000,
                    "end": 1022,
                    "text": "Bednarska et al. 2015)",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The Q-RT-PCR was used for a molecular examination of the presence of influenza viral material.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time Quantitative Reverse Transcription Polymerase Chain Reaction (Q-RT-PCR)"
        },
        {
            "text": "The analysis was carried out using a Light Thermocycler 2.0 System (Roche Diagnostics; Rotkreuz, Switzerland). The reaction proceeded in capillaries of 20 \u03bcL volume. Primers and probes necessary to carry out the reaction were obtained through the Influenza Reagent Resource (IRR) program from the US Center for Disease Control (CDC) The reaction mixture contained MgSO 4 , reaction buffer -bovine serum albumin (BSA), RNase free water, and a SuperScript \u00ae III/platinum Taq mix (Invitrogen Life Technologies-Thermo Fisher Scientific; Carlsbad, CA), with the addition of 5 \u03bcL of the previously isolated RNA for each sample. The positive control constituted the RNA strains that were the following vaccine components for the 2015/16 epidemic season/2016: A/H1N1/pdm09 (A/California/7/2009), A/H3N2/Switzerland/ 9715293/2013, and B/Phuket/3073/2013. As for the negative, RNase-free water was used. Before the start of amplification, the RNA was rewritten to obtain cDNA, using the enzyme reverse transcriptase at 50 C for 30 min. Then, samples were analyzed as follows: initialization at 95 C for 2 min and 45 cycles of amplification consisting of denaturation at 95 C for 15 s, annealing at 55 C for 30 s, and elongation at 72 C for 2 min.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time Quantitative Reverse Transcription Polymerase Chain Reaction (Q-RT-PCR)"
        },
        {
            "text": "In the 2015/2016 epidemic season in Poland over eight thousand five hundred specimens for influenza and influenza-like viruses were examined. The presence of influenza virus was found in over three thousand cases. The number of confirmed cases and share of various types of virus are presented in Fig. 1 . The majority of confirmations of influenza were found in the Pomeranian (72%), Masovian (62%), and Subcarpathian (55%) provinces. In other provinces, influenza was confirmed in less than half specimens investigated ( Fig. 1 and Table 1 ).",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 297,
                    "end": 303,
                    "text": "Fig. 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 523,
                    "end": 529,
                    "text": "Fig. 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 534,
                    "end": 541,
                    "text": "Table 1",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Influenza"
        },
        {
            "text": "We further found that the predominant subtype of influenza A virus in the 2015/16 season was A/H1N1/pdm09 that was detected in high percentage of influenza infection in several provinces, such as Pomerania (80%), Podlaskie (76%), Subcarpathia (74%), Lubuskie (71%), Silesia, and Warmia-Masuria (69%), with a smaller contribution from other provinces. In (Table 2) . Type B virus predominated in West Pomerania (69% of infections) and also accounted for a substantial share of more than one third of the confirmed cases in Lesser Poland (Table 2) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 354,
                    "end": 363,
                    "text": "(Table 2)",
                    "ref_id": "TABREF2"
                },
                {
                    "start": 536,
                    "end": 545,
                    "text": "(Table 2)",
                    "ref_id": "TABREF2"
                }
            ],
            "section": "Influenza"
        },
        {
            "text": "Poland provinces. In other regions of the country these infections were rather sporadic (Fig. 1) . The respiratory syncytial virus was the most common cause of ILI. Other viruses, such as human metapneumovirus, adenovirus, coronavirus 229E/NL63, parainfluenzavirus 1-3, coronavirus OC43, and rhinovirus A/B were identified in individual cases. (Brydak 2016) . This subtype was recommended as a component of influenza vaccine in the 2015/16 epidemic season (WHO 2015) and it also is going to be included into the 2016/17 vaccine (WHO 2016). Influenza virus type B was the most frequent in the West Pomerianian and Swietokrzyskie provinces, accounting for more than half of the confirmed infections (Table 1) . A similar number of cases of influenza B and influenza-like infections was observed in the Opole and Greater Poland provinces, amounting to about 30% each. The majority of deaths occurred in the region of Silesia (33), in which the most frequently detected virus was influenza type A. There also were 25 deaths reported in Greater Poland, where virus type A (39%), B (32%), and viruses causing influenza-like illness (29%) were at a grossly comparable level. The WHO estimates that 1 million people die each year due to influenza infection (Brydak 2012) .",
            "cite_spans": [
                {
                    "start": 344,
                    "end": 357,
                    "text": "(Brydak 2016)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 456,
                    "end": 466,
                    "text": "(WHO 2015)",
                    "ref_id": null
                },
                {
                    "start": 1249,
                    "end": 1262,
                    "text": "(Brydak 2012)",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [
                {
                    "start": 88,
                    "end": 96,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 697,
                    "end": 706,
                    "text": "(Table 1)",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Influenza-like infections (ILI) accounted for over 20% of all infections in the Opole and Greater"
        },
        {
            "text": "The present virological data indicate the predominance of infection with influenza virus type A in the eastern Polish provinces: Subcarpathian (91%), Lublin (80%), and Podlaskie (74%), lying near the border with Russia. These infection may be related to border trafficking between the two countries as type A virus was confirmed as predominant from week 3 of 2016 in Russia (Kimissarov et al. 2016 ). However, predominance of type A virus in 2015/16 was also confirmed in Slovakia, Ukraine, Belarus, and Lithuania (Flu News Europe 2016).",
            "cite_spans": [
                {
                    "start": 374,
                    "end": 397,
                    "text": "(Kimissarov et al. 2016",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Influenza-like infections (ILI) accounted for over 20% of all infections in the Opole and Greater"
        },
        {
            "text": "A decline in the number of confirmed cases of influenza virus subtype A/H3N2/, which accounted for only 1% in the Kuyavian-Pomerianian and Masovian provinces, was observed in 2015/16 compared with that in 2014/15 (Bednarska et al. 2015) ; a situation similar to that also noted in Russia where this subtype caused only sporadic infections (Kimissarov et al. 2016) . In contrast, predominance of influenza virus type B was observed the West Pomerania that lies west of the provinces above mentioned near Germany where type B virus was found to predominate in the 2015/16 influenza season (Flu News Europe 2016). Therefore, it seems that the regional differences in the type of influenza virus causing infections in a given season have in all probability to do with the epidemiological situation in neighboring countries.",
            "cite_spans": [
                {
                    "start": 213,
                    "end": 236,
                    "text": "(Bednarska et al. 2015)",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 339,
                    "end": 363,
                    "text": "(Kimissarov et al. 2016)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Influenza-like infections (ILI) accounted for over 20% of all infections in the Opole and Greater"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Evaluation of the activity of influenza and influenza-like viruses in the epidemic season",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kondratiuk",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Adv Exp Med Biol",
            "volume": "857",
            "issn": "",
            "pages": "1--7",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Influenza -an age old problem",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Hygeia Public Health",
            "volume": "47",
            "issn": "1",
            "pages": "1--7",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Prophylaxis of influenza in general practice",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Article in Polish) Flu News Europe",
            "volume": "1",
            "issn": "",
            "pages": "13--15",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Virological characteristics of the 2014/2015 influenza season based on molecular analysis of biological material derived from I-MOVE study",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Korczy\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Paradowska-Stankiewicz",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Adv Exp Med Biol",
            "volume": "921",
            "issn": "",
            "pages": "81--85",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Viral infections in children in the 2014/2015 epidemic season in Poland",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kondratiuk",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Rabczenko",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Adv Exp Med Biol",
            "volume": "912",
            "issn": "",
            "pages": "51--56",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Rapid spread of influenza A(H1N1)pdm09 viruses with a new set of specific mutations in the international genes in the beginning of 2015/2016 epidemic season in Moscow and Saint Petersburg",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kimissarov",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Fadeev",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Sergeeva",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Petrov",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Sintsova",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Egorova",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Pisareva",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Buzitskaya",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Musaeva",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Danilenko",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Konovalova",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Petrova",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Stolyarov",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Smorodintseva",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Burtseva",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Krasnoslobodtsev",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Kirillova",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Eropkin",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Sominina",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Grudinin",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Influenza Other Respir Viruses",
            "volume": "10",
            "issn": "",
            "pages": "247--253",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Influenza and influenza-like illness in Poland",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Niph-Nih",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "",
            "volume": "22",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "The pathology of influenza virus infections",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "K"
                    ],
                    "last": "Taubenberger",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "M"
                    ],
                    "last": "Morens",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Annu Rev Pathol",
            "volume": "3",
            "issn": "",
            "pages": "499--522",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Percentage distribution of influenza and influenza-like infections in the Polish provinces in the 2015/16 epidemic season. The province number, placed under each set of bars, corresponds to the numbered names of provinces provided in the tables two provinces, Swietokrzyskie and Greater Poland, there was an equal frequency of type A and B viruses. Subtype A/H3N2/ accounted for one percent in the Kuyavian-Pomeranian and Masovian provinces. Apart from the two subtypes, there were unsubtyped cases of influenza infection (60%). These infections occurred most frequently in the province of Opole",
            "latex": null,
            "type": "figure"
        },
        "TABREF1": {
            "text": "",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "Percentage distribution (%) of influenza virus infections in the Polish provinces in the 2015/16 epidemic season H1N1/pdm09 may stem from a low level of vaccination coverage in Poland amounting to about 3.6% of the population",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "Acknowledgements This work was funded by NIPH-NIH thematic subject 5/EM.1. The authors would like to acknowledge physicians and employees of VSESs participating in Sentinel and non-Sentinel programs for their input into the influenza surveillance in Poland.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        },
        {
            "text": "The authors declare no conflicts of interests in relation to this article.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflicts of Interest"
        }
    ]
}