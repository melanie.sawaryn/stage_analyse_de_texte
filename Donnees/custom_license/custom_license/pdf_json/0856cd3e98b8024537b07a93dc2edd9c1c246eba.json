{
    "paper_id": "0856cd3e98b8024537b07a93dc2edd9c1c246eba",
    "metadata": {
        "title": "ARTICLE IN PRESS +Model",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The world is in war against the pandemic of SARS-CoV-2 (severe acute respiratory syndrome coronavirus 2, the cause of COVID-19) and casualties (like in war: casualties) are increasing dramatically throughout the world. This disease may cause massive diffuse alveolar damage resulting in hypoxaemic acute respiratory failure (ARF) requiring, in a high percentage of cases, mechanical ventilation. 1---3 Behaviours and lifestyles are changing all over the world due to restrictions and lockdowns imposed by national governments to contain the diffusion.",
            "cite_spans": [
                {
                    "start": 396,
                    "end": 401,
                    "text": "1---3",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "This editorial is written in the middle of the war and only final and complete data will allow to draw solid conclusions. However, even with partial information the scientific community has the moral duty to raise questions and to try to provide interdisciplinary analyses and a prospective vision based on societal, clinical, economical, organisational, technological and ethical challenges which arise from this worldwide emergency.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "With respect to the organisation of the healthcare services, in most countries the available ICU beds seem not to be properly sized to face the dramatic requirements. In the years, many governments around the world have reduced the economic resources to their healthcare systems and the current organisation of our hospitals may not fit present needs. The huge prevalence of the disease requires the distribution of patients according to different levels of interventions from simple medical supervision to oxygen (supplementation or High Flow), to non-invasive ventilation (NIV), to intubation while preserving safety of doctors and nurses. 4,5 Do we have the infrastructure for these purposes? Furthermore, the shortage of ICU beds and the higher mortality among oldest and weakest people pose dramatic ethical questions regarding the triage of the patients with possible need of ''Sophie's Choice''. 6 Moreover, it is not only a matter of infrastructure, but also of competences. Health authorities launch appeals for more troops (doctors and nurses) and weapons (ventilators, masks and other tools). The physiology of COVID-19 induces ARF which requires high skills (e.g. to appropriately set the ventilator either for NIV or invasive ventilation). Do we have such skilled troops?",
            "cite_spans": [
                {
                    "start": 903,
                    "end": 904,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Furthermore, how to equip our troops? Other than to provide the healthcare professionals with the proper safety equipment, an appropriate use of available technologies is required. 7 Robots, Artificial Intelligence, Big Data Analytics, mobile apps and tele-medicine can be effective resources also in fighting pandemics. In fact, robots have a high potential in different infection-related applications, such as environment disinfection, medications and food delivery, assessment of clinical parameters and security checks. 8 For disease prevention autonomous or remote-controlled robots can be deployed for noncontact ultraviolet surface disinfection. 9 Mobile robots embedded with temperature measurement sensors (i.e., thermal sensors) and vision algorithms (for facial recognition) may lead to cost-effective, fast and effective screening of population in hospitals and public areas (airports, railway stations, ports, underground stations, etc.). 10 Robots may also be deployed to optimise nasopharyngeal and oropharyngeal swabbing in terms of increase of the process speed and protection of healthcare professionals. Another potential area of application is represented by delivery of samples and medicines to infected persons by means of drones or autonomous ground robots. 11 Data collected by robots and mobile apps may be analysed by means of dedicated machine learning algorithms able to extract meaningful information for infection prevention and prediction, diffusion control, and diagnosis and treatment of infected persons. 12 Data can be transmitted to hospital information and security systems and can be matched with data collected by mobile applications to support contact tracing efforts amid the outbreak to limit the infection diffusion: however, data protection and data privacy regulations and laws must be guaranteed. 13, 14 Which lessons from the middle of the storm we are still fighting against? We highlight at least three precious lessons:",
            "cite_spans": [
                {
                    "start": 181,
                    "end": 182,
                    "text": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 524,
                    "end": 525,
                    "text": "8",
                    "ref_id": null
                },
                {
                    "start": 653,
                    "end": 654,
                    "text": "9",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1281,
                    "end": 1283,
                    "text": "11",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1539,
                    "end": 1541,
                    "text": "12",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1843,
                    "end": 1846,
                    "text": "13,",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1847,
                    "end": 1849,
                    "text": "14",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "1. The expense in the healthcare field must be considered an investment and not a cost. We must build strong healthcare systems, able to work well in normal and to promptly react in extraordinary times. Let us think of the economic impact produced by the pandemic. How much of the economic losses might have been avoided by previous major investments in healthcare? What is the opportunity cost today of not having done so? We must not repeat the same mistake! Let's invest in research, innovation, technology, organisational models. 2. In healthcare we must adopt a strategic approach.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "There are some answers that even in emergency conditions can be provided (mask production through the reconversion of some production activities, etc.), others not. How to make available in a few days, skills that take years to be trained? We need to redesign the academic programmes for specializations we may require most in the next years (such as respiratory medicine). We need to scan the horizon, trying to forecast ---analysing trends in the population, in the behaviours, in the lifestyles, and in the society, and taking into account the potential large-scale events (although unlikely) ---future scenarios. 3. We must have a common systematic approach in the response. The international community must share ways of reacting and common protocols for managing these emergencies. A global effort is required in terms of organisation in collaborative networks, and of implementation of international emergency policies. Our countries seem unprepared: (a) absence of codified procedures and predefined plans to manage events like COVID-19 outbreak; (b) different behaviours between country and country in the way of managing the emergency.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "As said, we are still in the middle of the storm, more lessons will be taught from this emergency before it is over. It will only depend on us if we will be able to learn them and to turn the ''black swan'' into a growth opportunity for the entire international community. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "And now for something completely different: from 2019-nCoV and COVID-19 to 2020-nMan",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Froes",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Pulmonology",
            "volume": "26",
            "issn": "",
            "pages": "114--119",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "A novel coronavirus from patients with pneumonia in China",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "",
            "pages": "727--760",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan China",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Ren",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "497--506",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Surviving Sepsis Campaign: guidelines on the management of critically ill adults with Coronavirus Disease 2019 (COVID-19)",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Alhazzani",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "H"
                    ],
                    "last": "M\u00f8ller",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "M"
                    ],
                    "last": "Arabi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Loeb",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "N"
                    ],
                    "last": "Gong",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Intensive Care Med",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "file:/localhost/opt/grobid/grobid-home/tmp/dx.doi.org/10.1007/s00134-020-06022-5"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "More awareness is needed for severe acute respiratory syndrome coronavirus 2019 transmission through exhaled air during non-invasive respiratory support: experience from China",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Peng",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Eur Respir J",
            "volume": "55",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Fair allocation of scarce medical resources in the time of Covid-19",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "J"
                    ],
                    "last": "Emanuel",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Persad",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Upshur",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Thome",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Parker",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Glickman",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "file:/localhost/opt/grobid/grobid-home/tmp/dx.doi.org/10.1056/NEJMsb2005114"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Critical Care Utilization for the COVID-19 outbreak in Lombardy, Italy: early experience and forecast during an emergency response",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Grasselli",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pesenti",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Cecconi",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "JAMA",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "file:/localhost/opt/grobid/grobid-home/tmp/dx.doi.org/10.1001/jama.2020.4031"
                ]
            }
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Evaluation of an ultraviolet room disinfection protocol to decrease nursing home microbial burden, infection and hospitalization rates",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "R"
                    ],
                    "last": "Kovach",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Taneli",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Neiman",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "M"
                    ],
                    "last": "Dyer",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Arzaga",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "T"
                    ],
                    "last": "Kelber",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "BMC Infect Dis",
            "volume": "17",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Use of drones in clinical microbiology and infectious diseases: current status, challenges and barriers",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Poljak",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "\u0160terbenc",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Clin Microbiol Infect",
            "volume": "",
            "issn": "19",
            "pages": "30499--30508",
            "other_ids": {
                "DOI": [
                    "file:/localhost/opt/grobid/grobid-home/tmp/dx.doi.org/10.1016/j.cmi.2019.09.014"
                ]
            }
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Identification of COVID-19 can be quicker through artificial intelligence framework using a mobile phone-based survey in the populations when cities/towns are under quarantine",
            "authors": [
                {
                    "first": "Asrs",
                    "middle": [],
                    "last": "Rao",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Vazquez",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Infect Control Hosp Epidemiol",
            "volume": "3",
            "issn": "",
            "pages": "1--18",
            "other_ids": {
                "DOI": [
                    "10.1017/ice.2020.61"
                ]
            }
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Tele-monitoring of ventilator-dependent patients: a European Respiratory Society Statement",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Ambrosino",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Vitacca",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Dreher",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Isetta",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Montserrat",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Tonia",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Eur Respir J",
            "volume": "48",
            "issn": "",
            "pages": "648--63",
            "other_ids": {
                "DOI": [
                    "file:/localhost/opt/grobid/grobid-home/tmp/dx.doi.org/10.1017/ice.2020.61"
                ]
            }
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Artificial intelligence for infectious disease Big Data Analytics",
            "authors": [
                {
                    "first": "Zsy",
                    "middle": [],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhoub",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Zhangb",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Infect Dis Health",
            "volume": "24",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "https://doi.org/10.1016/j.pulmoe.2020.03.002 2531-0437/\u00a9 2020 Sociedade Portuguesa de Pneumologia. Published by Elsevier Espa\u00f1a, S.L.U. This is an open access article under the CC BY-NC-ND license (http://creativecommons.org/licenses/by-nc-nd/4.0/).",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Stefano Mazzoleni a,b, * , Giuseppe Turchetti c , Nicolino Ambrosino d a The BioRobotics Institute, Scuola Superiore Sant'Anna, Italy b Department of Excellence in Robotics & AI, Scuola Superiore Sant'Anna, Italy c Institute of Management, Scuola Superiore Sant'Anna, Italy d Istituti Clinici Scientifici Maugeri IRCCS, Istituto di Montescano, Italy",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}