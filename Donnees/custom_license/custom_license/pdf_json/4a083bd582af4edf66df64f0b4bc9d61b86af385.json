{
    "paper_id": "4a083bd582af4edf66df64f0b4bc9d61b86af385",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The ''helmet bundle\" in COVID-19 patients undergoing non invasive ventilation Dear Editor, The COVID-2019 outbreak represents a new challenge for intensive care (ICU) nurses. In this epidemic, limiting the virus aerosolisation during ICU procedures (e.g. intubation, bronchoscopy, non-invasive ventilation) is one of the main challenges for critical care nurses. More than 50% of the patients treated in China required High Flow Nasal Cannula (HFNC) or Non-Invasive Ventilation (NIV) (Guan et al., 2020) . During the current pandemic, HFNC use has been required for 11% of critically ill patients in Wuhan (Guan et al., 2020) . The high flow rates, however are likely to increase virus aerosolisation.",
            "cite_spans": [
                {
                    "start": 484,
                    "end": 503,
                    "text": "(Guan et al., 2020)",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 606,
                    "end": 625,
                    "text": "(Guan et al., 2020)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "Substantial exposure to exhaled air occurs within one metre from patients receiving NIV via face-mask (Hui et al., 2009) . Large air leaks affect the efficacy of NIV and should be avoided, whereas small air leaks can be compensated for by ventilators designed for NIV and are usually tolerated.",
            "cite_spans": [
                {
                    "start": 102,
                    "end": 120,
                    "text": "(Hui et al., 2009)",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "Recently Cabrini and Colleagues (2020) suggest the use of the helmet device for Continuous Airway Pressure (CPAP) and Pressure Support Ventilation (PSV) to limit virus spread into the ambient air. The number of available ICU beds during COVID-2019 outbreak, is less than the total number of COVID-19 patients requiring NIV or CPAP. In order to prevent ICU admission, the use of helmets in general wards could be implemented (Bellani et al., 2008) .",
            "cite_spans": [
                {
                    "start": 9,
                    "end": 38,
                    "text": "Cabrini and Colleagues (2020)",
                    "ref_id": null
                },
                {
                    "start": 424,
                    "end": 446,
                    "text": "(Bellani et al., 2008)",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "The Helmet is a reusable single patient interface, made of a clear plastic hood on a hard-plastic ring with a multi size silicon-polyvinyl chloride soft collar, to fit a wide range of necks' dimensions. With this device, the patient's exhalate can be filtered by applying a high efficiency particulate (HEPA) filter at the helmet outlet.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "During Helmet CPAP or NIV, ICU nurses should focus their attention on the interventions that contribute to increase patient's comfort, to maximise the acceptability of the device (Lucchini et al., 2010) . The longer the treatment cycle, the lower the environmental dispersion and the risk for healthcare providers. Key areas for nursing when caring for patients with helmet treatment include noise reduction, helmet anchorage and humidification of the gas supply (i.e.: a ''helmet bundle\").",
            "cite_spans": [
                {
                    "start": 179,
                    "end": 202,
                    "text": "(Lucchini et al., 2010)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "The gas airflow generates turbulence and consequently noise. We suggest the use of a Heat and Moisture Exchanger (HME) filter on the helmet gas inspiratory limb. Basically, the inner filter membrane works like an engine exhaust muffler, resulting in a significant noise reduction inside the helmet (Lucchini et al., 2020) .",
            "cite_spans": [
                {
                    "start": 298,
                    "end": 321,
                    "text": "(Lucchini et al., 2020)",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "The choice of fixing system to anchor the helmet during CPAP significantly affects patient comfort, (Lucchini et al, 2019a) . We suggest to avoid armpit straps during helmet CPAP, as they can cause pain and device-related pressure ulcers. On the contrary, the counterweights system (Fig. 1) seems to be the best approach to minimise the risks of pressure sores and pain during this treatment.",
            "cite_spans": [
                {
                    "start": 100,
                    "end": 123,
                    "text": "(Lucchini et al, 2019a)",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [
                {
                    "start": 282,
                    "end": 290,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Letter to the Editor"
        },
        {
            "text": "In the absence of active humidification during high flow Helmet-CPAP, under-humidification will occur (Chiumello et al., 2008; Lucchini et al., 2019b) . The problem is more prevalent with Venturi systems with a high inspiratory oxygen fraction and when only medical gases are employed. The modern active heated humidifiers, through NIV software, are able to deliver an absolute humidity above 10 mgH2O/L. The use of an active humidifier set at 26\u00b0C, with a temperature gradient increasing towards the patient (+2\u00b0/28\u00b0at the helmet gas inlet port) improves absolute and relative humidity inside the helmet, while avoiding under-humidification in healthy subjects. These settings provide a proportional amount of water for the helmet inner temperature, due to a rising of temperature inside the hosing line and a reduction in moisture build-up before the helmet inlet. If an HME filter is used as noise reduction system, it must be placed between the medical gas source and the heater chamber inlet.",
            "cite_spans": [
                {
                    "start": 102,
                    "end": 126,
                    "text": "(Chiumello et al., 2008;",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 127,
                    "end": 150,
                    "text": "Lucchini et al., 2019b)",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "In conclusion, in patients who need non-invasive CPAP we suggest the ''the helmet CPAP bundle\" (noise reduction, counterweights fixing system and heated wire tube with active humidification), to improve patient's comfort.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "https://doi.org/10.1016/j.iccn.2020.102859 0964-3397/\u00d3 2020 Elsevier Ltd. All rights reserved. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Letter to the Editor"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The use of helmets to deliver non-invasive continuous positive airway pressure in hypoxemic acute respiratory failure",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Bellani",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Patroniti",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Greco",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Foti",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pesenti",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Minerva Anestesiol",
            "volume": "74",
            "issn": "11",
            "pages": "651--656",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Minimise nosocomial spread of 2019-nCoV when treating acute respiratory failure",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Cabrini",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Landoni",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Zangrillo",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Effect of a heated humidifier during continuous positive airway pressure delivered by a helmet",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Chiumello",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Chierichetti",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Tallarini",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Cozzi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Cressoni",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Polli",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Crit Care",
            "volume": "12",
            "issn": "2",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Clinical characteristics of coronavirus disease 2019 in China",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "J"
                    ],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "Y"
                    ],
                    "last": "Ni",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "H"
                    ],
                    "last": "Liang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "Q"
                    ],
                    "last": "Ou",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "X"
                    ],
                    "last": "He",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N. Engl. J. Med. Epub ahead of print",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Exhaled air dispersion distances during noninvasive ventilation via different Respironics face masks",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "S"
                    ],
                    "last": "Hui",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "K"
                    ],
                    "last": "Chow",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "S"
                    ],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "C Y"
                    ],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "D"
                    ],
                    "last": "Hall",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Gin",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Chest",
            "volume": "136",
            "issn": "4",
            "pages": "998--1005",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "The comfort of patients ventilated with the Helmet Bundle",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Lucchini",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Valsecchi",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Elli",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Doni",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Corsaro",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Tundo",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Assist. Inferm. Ric",
            "volume": "29",
            "issn": "4",
            "pages": "174--183",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Water content of delivered gases during Helmet Continuous Positive Airway Pressure in healthy subjects",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Lucchini",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Bambi",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Elli",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bruno",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Dallari",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Puccio",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Acta Biomed",
            "volume": "90",
            "issn": "11-S",
            "pages": "65--71",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "How different helmet fixing options could affect patients' pain experience during helmet-continuous positive airway pressure",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Lucchini",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Elli",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Bambi",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "De Felippis",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Vimercati",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Minotti",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Nurs. Crit. Care",
            "volume": "24",
            "issn": "6",
            "pages": "369--374",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Noise level and comfort in healthy subject undergoing high flow helmet CPAP",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Lucchini",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Bambi",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gurini",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Di Francesco",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Pace",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Rona",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "General Intensive Care Unit -San Gerardo Hospital",
            "volume": "33",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "E-mail addresses: alberto.lucchini@unimib.it, a.lucchini@asst-monza",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Available online xxxx",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "CPAP with counterweights system.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "The authors declare that they have no known competing financial interests or personal relationships that could have appeared to influence the work reported in this paper.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        }
    ]
}