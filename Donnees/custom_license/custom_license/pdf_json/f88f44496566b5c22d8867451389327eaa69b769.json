{
    "paper_id": "f88f44496566b5c22d8867451389327eaa69b769",
    "metadata": {
        "title": "Incidence of Circulating Antibodies Against Hemagglutinin of Influenza Viruses in the in Poland",
        "authors": [
            {
                "first": "K",
                "middle": [],
                "last": "Bednarska",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health -National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska St",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            },
            {
                "first": "M",
                "middle": [
                    "A"
                ],
                "last": "Nowak",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health -National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska St",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            },
            {
                "first": "K",
                "middle": [],
                "last": "Kondratiuk",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health -National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska St",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            },
            {
                "first": "E",
                "middle": [],
                "last": "Hallmann-Szeli\u0144 Ska",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health -National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska St",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            },
            {
                "first": "L",
                "middle": [
                    "B"
                ],
                "last": "Brydak Abstract",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institute of Public Health -National Institute of Hygiene",
                    "location": {
                        "addrLine": "24 Chocimska St",
                        "postCode": "00-791",
                        "settlement": "Warsaw",
                        "country": "Poland"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Influenza is an infectious disease caused by influenza viruses from the Orthomyxoviridae family. The continuing evolution of the virus is the cause of seasonal epidemics and, from time to time, of pandemics in the human population. Furthermore, due to appearance of influenza-like illnesses, diagnosis of this disease on the basis of clinical symptoms is possible only during the outbreak. It should be noted that in case of respiratory infections various clinical symptoms can be caused by the same virus and, on the other hand, the same set of symptoms can be caused by more than 200 different viruses (e.g. parainfluenza, adenovirus, rhinovirus, and coronavirus). For this reason, the laboratory confirmation of influenza virus infection is crucial in the influenza surveillance and is essential for evaluating the effectiveness of vaccines and antiviral drugs. Laboratory diagnosis of influenza includes confirmation of the presence of influenza virus antigen in the material collected from the patient and serological evidence of infection with influenza virus by detecting the increase of specific antibodies in patient's serum (Brydak 2008) . The major line of defense from influenza infection are antibodies directed against two glycoproteins exposed on the surface of the virion -hemagglutinin (HA) and neuraminidase (NA) (Johansson et al. 1989) . Furthermore, evolution of influenza virus is most evident in the case of these surface proteins. HA and NA antigens are constantly changing due to antigenic pressure. They both are highly immunogenic, and antibodies produced in response to viral infection are specific for a particular subtype of hemagglutinin (H1-H18) and neuraminidase (N1-N11) and do not give complete protection against all influenza virus strains (Nicholson et al. 1998) . The presence of hemagglutinin antibodies (anti-HA) provides not only protection against infection with specific strains of influenza virus, but also might alleviate symptoms of the disease in case of infection with another variant of the virus. This is caused by the occurrence of so-called cross-reactive antibodies. It has been shown that subtypespecific anti-HA antibodies can decrease the infectivity of the virus of other subtype by disruption of proliferation and release of viral particles during infection (Ekiert et al. 2011; Epstein and Price 2010; Thorsby et al. 2008) . For this reason, regular vaccinations are essential for reducing the impact of seasonal influenza and influenza prevention. Seasonal vaccinations protect from infection with currently circulating viruses and give cross-protection, which can reduce viral replication, accelerate viral clearance, and thus reduce the severity of disease. The World Health Organization leads the global influenza surveillance and gives seasonal recommendations for influenza virus strains included in the vaccine for the upcoming epidemic season.",
            "cite_spans": [
                {
                    "start": 1134,
                    "end": 1147,
                    "text": "(Brydak 2008)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1331,
                    "end": 1354,
                    "text": "(Johansson et al. 1989)",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1776,
                    "end": 1799,
                    "text": "(Nicholson et al. 1998)",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 2316,
                    "end": 2336,
                    "text": "(Ekiert et al. 2011;",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 2337,
                    "end": 2360,
                    "text": "Epstein and Price 2010;",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 2361,
                    "end": 2381,
                    "text": "Thorsby et al. 2008)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "An anti-HA antibody titers of 1:40 has been determined as corresponding to a 50 % reduction in the risk of contracting influenza in population (Hobson et al. 1972) and it is assumed that high titers of anti-HA antibodies (!1:40) provide protection against influenza infection. Furthermore, this value of titer, after vaccination, is required by European Agency for the Evaluation of Medicinal Products for assessment of vaccines (Committee for Proprietary Medicinal Products 1997). On the other hand, the anti-NA antibodies, even at high titers, only support the resistance because they do not prevent influenza infection themselves. However, it has been shown that the anti-NA antibodies block the replication process, alleviate the severity of infection, and reduce the incidence of disease (Brydak 2008) .",
            "cite_spans": [
                {
                    "start": 143,
                    "end": 163,
                    "text": "(Hobson et al. 1972)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 793,
                    "end": 806,
                    "text": "(Brydak 2008)",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Serological methods for detection of antiinfluenza antibodies include, among others, hemagglutination inhibition (HAI) test and neuraminidase inhibition (NI) test. Serological tests are used not only for diagnostic purposes, but also to evaluate the resistance of the population resulting from both, natural infection and vaccination against influenza. Most laboratories use the hemagglutination inhibition test for serological diagnosis. The method is based on the ability of anti-HA antibodies to inhibit virus-induced agglutination of erythrocytes (WHO -Global Influenza Surveillance Network 2011). It is a simple, inexpensive, and rapid test, during which a small amount of reagents is used. Due to the high cost and labor-consumption, not many laboratories determines the level of antibodies with neuraminidase inhibition test (Brydak 2008) .",
            "cite_spans": [
                {
                    "start": 832,
                    "end": 845,
                    "text": "(Brydak 2008)",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The recorded number of cases of influenza and influenza-like illnesses depends on reporting by health care workers. In Poland, this is done by Local Sanitary Epidemiological Stations. Thereafter, these data in the form of weekly reports are sent to the National Institute of Public Health -National Institute of Hygiene by Voivodeship Sanitary Epidemiological Stations. There are two types of influenza vaccines, which are currently available in Poland: inactivated split virion and subunit. Split type vaccine contains inactivated split virus, whereas subunit type contains only viral glycoproteins, hemagglutinin and neuraminidase. It should be noted that already for many influenza seasons, numerous local governments offer free influenza vaccinations for people over 50 years of age, who often belong to high-risk groups of health. Despite this, the proportion of people vaccinated is diminishing season by season. In the epidemic season 2013/2014 only 3.75 % of the population was vaccinated against influenza (Brydak 2014) .",
            "cite_spans": [
                {
                    "start": 1015,
                    "end": 1028,
                    "text": "(Brydak 2014)",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The epidemic season 2013/2014 was mild in intensity in comparison to the previous season. A total number of 2,036,215 cases of influenza and influenza-like illness were registered in Poland in this season and the incidence was 5,284 (per 100,000 population), causing 15 deaths as a result of complications from influenza (GIS 2014). Our paper describes determination of the level of antibodies against hemagglutinin of influenza viruses present in the sera of people in different age groups, during the epidemic season 2013/2014.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The serum samples of people in age groups: 0-3, ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "The geometric mean titers of anti-HA antibodies in sera collected from people in different age groups in the epidemic season 2013/2014 are shown in Fig. 1 . The highest level of antibodies against hemagglutinin H1 was found in the age group 4-7 (GMT-27.51). The GMT of anti-H1 antibodies was slightly lower in the age group 8-14 (15.60) and low in the age groups 0-3, 15-25, 26-44 and 45-64, where there was almost the same level of antibodies (12.09-13.44). The lowest level of anti-H1 antibodies was observed in the age group !65 (GMT-10.05). In the case of the H3 hemagglutinin, the highest geometric mean titers were in the 4-7 (21.14) and 8-14 (20.37) years old groups. In other age groups, these values were at similar levels from 11.97 to 15.58. For type B hemagglutinin the observed level of antibodies was the highest in group of 15-25 years (GMT-24.85), lower in group of 8-14 years (GMT-12.14), and the lowest in groups of 0-3 and 4-7 years (GMT: 9.55 and 10.67, respectively). Almost identical geometric mean titers were observed in the age groups 26-44 (17.01), 45-64 (16.99), and !65 (16.17) .",
            "cite_spans": [
                {
                    "start": 1060,
                    "end": 1105,
                    "text": "26-44 (17.01), 45-64 (16.99), and !65 (16.17)",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 148,
                    "end": 154,
                    "text": "Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Results and Discussion"
        },
        {
            "text": "In conclusion, the highest GMT titers were observed in the age groups: 4-7, 8-14, and 15-25, and a different type of anti-HA antibody had dominated in each of these groups: H1, H3 and B, respectively. The lowest geometric mean titer of antibody directed against the HA was observed in children aged 0-3 years.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results and Discussion"
        },
        {
            "text": "The protection rate is the percentage of people with the protective titer of anti-HA antibodies of at least 40 after vaccination and, depending on age, should achieve different values: !70 % for people aged 18-60 years and !60 % for people over 60 years of age (Brydak 2008) . However, the protection rate did not exceed 60 % in any age group (Fig. 2) . The highest level of protection rate was observed for the hemagglutinin H1 of A/California/7/2009pdm09 strain in the age group 4-7 (53.33 %). An analysis of the protection rate with respect to the age group, including all three types of hemagglutinin, revealed that in the 0-3 years old group the level of protection exhibited similar values: 16.00 % (B), 20.67 % (H3), and 26.67 % (H1) without the domination of particular hemagglutinin type. In the age groups 4-7 and 8-14, the protection rate for H1 and H3 hemagglutinins was much higher than for hemagglutinin type B. However, in 15-25, 26-44 and 45-64 years old groups significantly higher protection rates were observed for type B hemagglutinin compared to the other two types.",
            "cite_spans": [
                {
                    "start": 261,
                    "end": 274,
                    "text": "(Brydak 2008)",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [
                {
                    "start": 343,
                    "end": 351,
                    "text": "(Fig. 2)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Results and Discussion"
        },
        {
            "text": "In the last three epidemic seasons: 2011/2012, 2012/2013 and 2013/2014 , the values of protection rate were different in particular age groups ( Table 2 ). The comparison of protection rates for hemagglutinin H1 of A/California/7/2009pdm09 strain is especially interesting. This type of A influenza virus has been circulating in the population since 2009. In the age groups of 4-7, 26-44, 45-64, and !65 years there has been a significant increase in the protection rate compared to the previous two seasons. In the youngest age group this value is higher in comparison to the last season, but lower than in the 2011/ 2012 season. However, in groups of 8-14 and 15-25 years old the protection rates remain at a similar level in all analyzed seasons. In the case Despite the apparent increase of protection rate in some age groups, this value did not achieve 60 %. These results are truly alarming as it shows that the value of protection rate against influenza in the Polish population is very low. The complications of influenza may be hazardous to health and life-threatening to persons who are not vaccinated, especially in the very young, the elderly, and those with other serious medical conditions. Influenza vaccination is the most effective way to prevent infection, but this fact is ignored by many people. It can be concluded, on the basis of serological screening of sera from people at different age groups in the epidemic season 2013/2014, that: 1. our results confirm the circulation of three antigenically different influenza strains: two subtypes of influenza A virus -A/California/ 7/2009(H1N1)pdm09 and A/Victoria/361/ 2011(H3N2) -and type B, B/Massachusetts/ 2/2012; 2. protection rate did not exceed 60 % in any age group, which demonstrates that the proportion of people vaccinated against influenza in Poland is very low. This situation can be changed when more people will vaccinate against influenza.",
            "cite_spans": [
                {
                    "start": 36,
                    "end": 70,
                    "text": "2011/2012, 2012/2013 and 2013/2014",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 145,
                    "end": 152,
                    "text": "Table 2",
                    "ref_id": "TABREF2"
                }
            ],
            "section": "Results and Discussion"
        },
        {
            "text": "The authors declare no conflicts of interest in relation to this article.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflicts of Interest"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Incidence of circulation antibodies against influenza viruses (hemagglutinins) in epidemic season 2012/2013 in Poland",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Bednarska",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Hallmann-Szeli\u0144ska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Tomczuk",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Problemy Higieny i Epidemiologii",
            "volume": "95",
            "issn": "2",
            "pages": "268--272",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Grypa. Pandemia grypy -mit czy realne zagro\u017cenie",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "1--492",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Grypa -problem zdrowia publicznego",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Top Medical Trends Przewodnik Lekarza",
            "volume": "1",
            "issn": "",
            "pages": "13--15",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Committee for Proprietary Medicinal Products (1997) Note for guidance on harmonisation of requirements for influenza vaccines",
            "authors": [],
            "year": null,
            "venue": "CPMP",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "A highly conserved neutralizing epitope on group 2 influenza A viruses",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "C"
                    ],
                    "last": "Ekiert",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "H"
                    ],
                    "last": "Friesen",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Bhabha",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kwaks",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Jongeneelen",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Science",
            "volume": "333",
            "issn": "6044",
            "pages": "843--850",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Cross-protective immunity to influenza A viruses",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "Epstein",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "E"
                    ],
                    "last": "Price",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Expert Rev Vaccines",
            "volume": "9",
            "issn": "11",
            "pages": "1325--1341",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Meldunek G\u0142wnego Inspektora Sanitarnego dot. sytuacji epidemiologicznej grypy za okres 1 -7 kwietnia",
            "authors": [],
            "year": 2014,
            "venue": "GIS",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "The role of serum haemagglutination-inhibiting antibody in protection against challenge infection with influenza A2 and B viruses",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Hobson",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "L"
                    ],
                    "last": "Curry",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "S"
                    ],
                    "last": "Beare",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ward-Gardner",
                    "suffix": ""
                }
            ],
            "year": 1972,
            "venue": "J Hyg (Lond)",
            "volume": "70",
            "issn": "4",
            "pages": "767--777",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Purified influenza virus hemagglutinin and neuraminidase are equivalent in stimulation of antibody response but induce contrasting types of immunity to infection",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "E"
                    ],
                    "last": "Johansson",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Bucher",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "D"
                    ],
                    "last": "Kilbourne",
                    "suffix": ""
                }
            ],
            "year": 1989,
            "venue": "J Virol",
            "volume": "63",
            "issn": "",
            "pages": "1239--1246",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Textbook of Influenza",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "G"
                    ],
                    "last": "Nicholson",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "G"
                    ],
                    "last": "Webster",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Hay",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "1--592",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Heterosubtypic neutralizing monoclonal antibodies cross-protective against H5N1 and H1N1 recovered from human IgM + memory B cells",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Throsby",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Van Den Brink",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Jongeneelen",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "L"
                    ],
                    "last": "Poon",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Alard",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Cornelissen",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "PLoS ONE",
            "volume": "3",
            "issn": "12",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "A procedure which eliminates nonspecific inhibitor from human serum but does not affect specific antibodies against influenza viruses",
            "authors": [
                {
                    "first": "Daj",
                    "middle": [],
                    "last": "Tyrell",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "L"
                    ],
                    "last": "Horsfall",
                    "suffix": ""
                }
            ],
            "year": 1952,
            "venue": "J Immunol",
            "volume": "69",
            "issn": "",
            "pages": "563--574",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "WHO -Global Influenza Surveillance Network (2011) Manual for the laboratory diagnosis and virological surveillance of influenza",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "1--153",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Recommended composition of influenza virus vaccines for use in the 2013-14 northern hemisphere influenza season",
            "authors": [],
            "year": 2013,
            "venue": "WHO",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Geometric mean titers of anti-HA antibodies in sera of people in different age groups in the epidemic season 2013/2014 of H3 hemagglutinin of A/Victoria/361/2011 strain the protection rate increased in the age group 4-7, decreased in the age group 26-44, and did not change significantly in the group of 8-14 years, compared to the previous two epidemiological seasons. There has been a significant increase in the values of this factor in groups 15-25, 45-64, and !65 years old as compared to the epidemic season 2012/2013. With regard to the last season, value of protection rate substantially increased in the youngest group. A significant increase in protection rates was observed in four age groups: 0-3, 4-7, 15-25, and 26-44 for influenza B virus strain B/Massachusetts/2/2012 in epidemic season 2013/2014. In other age groups these values were similar to 2011/2012 season and lower than in previous season.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "The percentage of people with protective level of anti-HA antibodies with titers of at least 40 in the epidemic season 2013/2014",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "The antigens listed inTable 1were used in hemagglutination inhibition (HAI) tests, in agreement with the WHO recommendations (WHO 2013). All antigens were prepared in National Influenza Center, Department of Influenza Research, National Institute of Public Health -National Institute of Hygiene. Preparation and dilution of antigens for HAI tests were performed in accordance to WHO protocols (WHO -Global Influenza Surveillance Network 2011).",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Influenza virus strains used in hemagglutination inhibition tests (HAI)(WHO 2013)",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "Protection rate values (%) in the epidemic seasons 2011/2012, 2012/2013 and 2013/2014 (Bednarska et al. 2014)",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}