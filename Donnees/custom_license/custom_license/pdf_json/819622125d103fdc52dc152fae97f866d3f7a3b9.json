{
    "paper_id": "819622125d103fdc52dc152fae97f866d3f7a3b9",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The search for universal truths underpins all scientific research. The capability to be generalized is an important yardstick of the strength of a scientific output; concepts that are limited either geographically, or subject to special conditions, are viewed as being less powerful than those of broader coverage. This same principle holds true in the field of sustainability science. In order to meet the most urgent environmental challenges of the 21st Centuryissues such as climate change, biodiversity loss, and depletion of abiotic resources, among othersit is important to leverage state-of-the-art scientific knowledge to generate rational and evidence-based policies (Bednarek et al., 2018) . To achieve this goal, we believe it is important to understand the distinction between research based on universal physical principles, in contrast to research based on localized socio-economic conditions. For instance, there is a clear qualitative difference between engineering tools such as Process Integration, which have firm basis in the Laws of Thermodynamics and thus provide a universally rigorous methodological framework for planning efficient use of energy and resources (Kleme\u0161 et al., 2018) , in comparison to sociological constructs such as the Theory of Planned Behavior (TPB), which in a recent review is found to yield inconsistent results (Yuriev et al., 2020) . In addition, socio-economic aspects such as public acceptability of technologies is usually subject to geographically localized cultural norms, as illustrated by a recent paper on mining in Australia (Lacey et al., 2019) . More recently, cultural variations have been evident in people's reactions to state control measures in response to the growing COVID-19 pandemic.",
            "cite_spans": [
                {
                    "start": 676,
                    "end": 699,
                    "text": "(Bednarek et al., 2018)",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1185,
                    "end": 1206,
                    "text": "(Kleme\u0161 et al., 2018)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1360,
                    "end": 1381,
                    "text": "(Yuriev et al., 2020)",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1584,
                    "end": 1604,
                    "text": "(Lacey et al., 2019)",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Nevertheless, it is only possible to generate effective solutions and policies by considering aspects that are best elucidated from the lens of the Social Sciences. For example, radical dietary shifts (e.g., reduced meat consumption) and reduction of supply chain losses from spoilage are widely accepted by expert opinion as key solutions to reducing the carbon, water, land, nitrogen, and phosphorus footprints of food systems; and yet consumer behavior, economic developmental stage, and cultural preferences act as very real constraints to the implementation of known solutions (Tseng et al., 2019) . Various socio-economic aspects remain an integral part of any sustainability issue, even if the details vary across different locations and over time. These principles must be integrated with those arising from the Natural Sciences, and should then be \"engineered\" into actual workable solutions consisting of technology systems, management practices, and public policy. Thus, the question arises: If insights from the Social Sciences arise from fundamentally different roots in contrast to those generated via Natural Sciences, how do we best combine these domains to gain maximum benefits?",
            "cite_spans": [
                {
                    "start": 582,
                    "end": 602,
                    "text": "(Tseng et al., 2019)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Fortunately, the philosophical gap between these two schools of thought is not insurmountable. There are three important principles that we believe to be critical to managing the nuances that exist at the science-policy interface. Firstly, researchers must always be keenly aware of the geographic, temporal, or other sociological limitations of their findings. Reports of such findings must thus be qualified with appropriate caveats to minimize the risk of inappropriate generalization. To do otherwise constitutes an irresponsible overreach on the part of the researchers. Secondly, researchers must also reflect on which aspects of their contributions might be useful beyond the scope of the case study investigated. A section discussing broader implications of such work will strengthen their scientific value and social impact. This point is complementary to the first one, but it is important to point out given that many researchers are under increasing pressure from stakeholders (e.g., national government, university officials, funding agencies, and the general public) to create social impact within a short span of time. This pressure often results in a narrowing of thought processes towards highly visible local issues, at the expense of the loss of generalized insights. This type of myopic outlook also reduces the capacity of the scientific community to generate research output that can address unexpected issues. Thirdly, even as specific results may vary from case to case, it is essential to put a premium on the development and use of methodologies to ensure that the investigation of socio-economic aspects is done in a systematic and transparent manner. A good example is the Analytic Hierarchy Process, a well-known technique for decomposing complex decision problems into discrete sub-problems, and for integrating expert or stakeholder opinions in a mathematically transparent manner. Use of such tools enables the reader to not just take the results of articles at face value; it also empowers them to replicate (or modify) these results through duplicate studies. A promising development in this area is the partnership of key sustainability journals such as Resources, Conservation and Recycling with other journals dedicated to enhancing scientific transparencyfor example, Elsevier's MethodsX (for methodologies) and Data in Brief (for data).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In summary, the \"wicked problems\" of sustainability science will certainly require a multi-disciplinary as well as transdisciplinary approach in order to bridge the gap at the science-policy interface. Although researchers seldom perform key roles on both sides of this interface, it is important that they report any policy implications of their findings clearly and unambiguously. Solutions can be found by combining rigorous physical basis in Natural Sciences with managing real-life complications best seen via the lens of the Social Sciences. Understanding these points can pave the way towards the translation of scientific knowledge into effective and generalizable sustainability policies.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Boundary spanning at the science-policy interface: the practitioners' perspectives",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "T"
                    ],
                    "last": "Bednarek",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wyborn",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Cvitanovic",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Meyer",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "M"
                    ],
                    "last": "Colvin",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "F E"
                    ],
                    "last": "Addison",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "Close",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Curran",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farooque",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Goldman",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Hart",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Mannix",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Mcgreavy",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Parris",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Posner",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Robinson",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ryan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Leith",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Sustain. Sci",
            "volume": "13",
            "issn": "",
            "pages": "1175--1183",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "New directions in the implementation of Pinch Methodology (PM)",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "J"
                    ],
                    "last": "Kleme\u0161",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "S"
                    ],
                    "last": "Varbanov",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "G"
                    ],
                    "last": "Walmsley",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Jia",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Renew. Sustain. Energy Reviews",
            "volume": "98",
            "issn": "",
            "pages": "439--468",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Public perceptions of established and emerging mining technologies in Australia",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Lacey",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Malakar",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Mccrea",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Moffat",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Resour. Conserv. Recycl",
            "volume": "62",
            "issn": "",
            "pages": "125--135",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Pathways and barriers to circularity in food systems",
            "authors": [
                {
                    "first": "M.-L",
                    "middle": [],
                    "last": "Tseng",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "S F"
                    ],
                    "last": "Chiu",
                    "suffix": ""
                },
                {
                    "first": "C.-F",
                    "middle": [],
                    "last": "Chien",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "R"
                    ],
                    "last": "Tan",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Resour. Conserv. Recycl",
            "volume": "143",
            "issn": "",
            "pages": "236--237",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Pro-environmental behaviors through the lens of the theory of planned behavior: a scoping review",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Yuriev",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Dahmen",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Paill\u00e9",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Boiral",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Guillaumie",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Resources. Conserv. Recycl",
            "volume": "155",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "Contents lists available at ScienceDirect Resources, Conservation & Recycling journal homepage: www.elsevier.com/locate/resconrec Perspective On general principles at the sustainability science-policy interface A.S.F. Chiu a,b, \u204e , K.B. Aviso a,c , R.R. Tan a,c a Center for Engineering and Sustainable Development Research, De La Salle University, 2401 Taft Avenue, 0922 Manila, Philippines b Industrial Engineering Department, De La Salle University, 2401 Taft Avenue, 0922 Manila, Philippines c Chemical Engineering Department, De La Salle University, 2401 Taft Avenue, 0922 Manila, Philippines",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "The authors declare that they have no known competing financial interests or personal relationships that could have appeared to influence the work reported in this paper.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        }
    ]
}