{
    "paper_id": "f57b5250e80ff8c2fecd3c5e7e2fa43600e9b99e",
    "metadata": {
        "title": "Shiny Framework Based Visualization and Analytics Tool for Middle East Respiratory Syndrome",
        "authors": [
            {
                "first": "Maya",
                "middle": [],
                "last": "John",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Sree Buddha College of Engineering",
                    "location": {
                        "settlement": "Pathanamthitta",
                        "region": "Kerala",
                        "country": "India"
                    }
                },
                "email": ""
            },
            {
                "first": "Hadil",
                "middle": [],
                "last": "Shaiba",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Abdulrahman University",
                    "location": {
                        "settlement": "Riyadh"
                    }
                },
                "email": "hashaiba@pnu.edu.sa"
            }
        ]
    },
    "abstract": [
        {
            "text": "People in the Middle East have been affected by the Middle East Respiratory Syndrome CoronaVirus (MERS Co-V) since 2012. New cases are continuously reported especially in the Kingdom of Saudi Arabia, and the risk of exposure remains an issue. Data visualization plays a vital role in effective analysis of the data. In this paper, we introduce an interactive visualization application for MERS data collected from the Control and Command Centre, Ministry of Health website of Saudi Arabia. The data corresponding to the period from January 1, 2019 to February 28, 2019 was used in the present work. The attributes considered include gender, age, date of reporting, city, region, camel contact, description and status of the patient. The visualization tool has been developed using Shiny framework of R programming language. The application presents information in the form of interactive plots, maps and tables. The salient feature of the tool is that users can view and download data corresponding to the period of their choice. This tool can help decision makers in the detailed analysis of data and hence devise measures to prevent the spread of the disease.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "MERS is a viral disease that was first discovered in 2012 when a patient in Saudi Arabia was diagnosed with critical respiratory distress and kidney trouble [1] . The infection can either be asymptomatic or show symptoms such as cough, fever etc. along with difficulty in breathing [2] . The disease has been brought under control in all Middle Eastern countries except Saudi Arabia where each month, new cases are still reported. Studies have confirmed that this disease is zoonotic and camels are its significant reservoir [3, 4] . People in close contact with infected camels and health care professionals who care for infected patients have a high risk of acquiring the infection. Many cases of community and household acquired infections have been reported from Saudi Arabia [5] . The Ministry of Health, Saudi Arabia has issued guidelines for infection prevention, control and management.",
            "cite_spans": [
                {
                    "start": 157,
                    "end": 160,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 282,
                    "end": 285,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 525,
                    "end": 528,
                    "text": "[3,",
                    "ref_id": null
                },
                {
                    "start": 529,
                    "end": 531,
                    "text": "4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 780,
                    "end": 783,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "To the best of our knowledge, there is a lack of interactive visualization tools for MERS data visualization in Saudi Arabia. This work deals with developing an application where users can interactively view information about the infection in the form of plots, tables and maps. The data used in this study was obtained from the Saudi Ministry of Health website and it includes the cases reported in Saudi Arabia from January 1, 2019 to February 28, 2019. By viewing the data visualizations, users can analyze MERS cases better, find trends, monitor the disease and help authorities set detection and prevention guidelines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The MERS data analyzed consists of 82 cases reported during the first two months of the year 2019. The information is present in the form of a pdf file corresponding to the cases reported each week. The cases reported contains details which include the date of reporting and patient information such as personal information, demographic information, camel contact details, description of infection and status of the disease. The latitude and longitude of the regions and cities are stored in separate .csv files. The details of the attributes present in the database are shown in Table 1 . ",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 580,
                    "end": 587,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "Data Description"
        },
        {
            "text": "Shiny is an R package used to create interactive web applications [6] . The Shiny framework is a reactive programming model where browser refreshing is not required to instantiate the output changes when the user modifies the input. It can be used to build web applications without using Javascript coding. Shiny combines the computational power of R programming language with the highly interactive nature of the web.",
            "cite_spans": [
                {
                    "start": 66,
                    "end": 69,
                    "text": "[6]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Shiny Framework"
        },
        {
            "text": "Leaflet package is used to create interactive maps in R based on the Javascript library leaflet [7] . These interactive maps can be rendered in R markdown, Shiny apps, RStudioIDE and R console as it was developed in association with htmlwidgets. The leaflet( ) function is used to create the map widget. The different types of layers which can be included in the map widget are map tile, markers, lines, polygon, popups, raster images, color legends, layer groups, layer control etc. The maps created can be zoomed or downsized interactively.",
            "cite_spans": [
                {
                    "start": 96,
                    "end": 99,
                    "text": "[7]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Leaflet Package"
        },
        {
            "text": "The googleviz package in R facilitates the use of Google Chart APIs [8] . Interactive charts can be incorporated into web pages using Google Charts. The data stored in R data frames can be visualized in the form of Google Charts without uploading the information to Google. An internet connection is required to view the output rendered by this package.",
            "cite_spans": [
                {
                    "start": 68,
                    "end": 71,
                    "text": "[8]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Googleviz Package"
        },
        {
            "text": "The MERS data visualization tool was developed using R programming language. The tool consists of three sections (tabs) namely \"Different Cases Analysis\", \"Miscellaneous Analysis\" and \"Summary\" section. The users can choose the period for which they would like to visualize the MERS data.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results and Discussion"
        },
        {
            "text": "In the case of different cases analysis, the user can view the information as pie charts and maps, or tables. The users can view details about either all the cases together or recovered cases, death cases or hospitalized cases separately. The details regarding the cases can be viewed with respect to all cities, all regions or cities within a region. In the case of the option \"cities within a region\", the user has to choose a region from the drop down list provided. Unlike conventional pie charts, the pie charts created using googleviz package are interactive in nature. By pointing the cursor over a portion in the pie chart, one can understand the actual number of cases in that particular part of the chart. In the map, places are marked based on longitude and latitude of that place. On clicking the marker displayed in the map, the name and number of cases in that place will be displayed as a pop-up. Based on the number of cases, markers are assigned colors such as red, orange or yellow. The colors red, orange and yellow represent \"large number of cases\", \"moderate number of cases\" and \"few number of cases\" respectively. The information in Figs. 1 and 2 is presented in the form of pie chart and map. The screen shot of the page corresponding to different cases analysis is shown in Fig. 1 . The figure depicts the analysis of all MERS cases reported for the first two months of the year 2019 with regard to cities. It can be inferred from Fig. 1 that during that period 76.8% of the patients recovered from the disease and the maximum number of cases (61%) were reported from the city of Wadi Aldawasir. The analysis based on all cases reported in \"all cities within Riyadh region\" during January to February 2019 is shown in Fig. 2 . It can be observed from the figure that majority of cases in Riyadh region were reported from the city of Wadi Aldawasir. Nearly 78% of the infected people in Riyadh region recovered from the disease. The application page with table output is shown in Fig. 3 . It can be observed from the table that 12 people died due to MERS in Riyadh region during the first two months of the year 2019. The table which displays the information is interactive in nature. When the user clicks a column name in the table, the records in the table are sorted based on values in the clicked column and displayed accordingly. The table also has provision for searching values and selecting the number Application page corresponding to \"Different Cases Analysis\" tab for cities within a region of records to be displayed in a page. The users can also download the details contained in the table. The different cases analysis will help decision makers in gathering information regarding the cases reported in different places. This type of analysis is essential to identify disease prone areas and hence take measures to curtail the spread of infection.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 1299,
                    "end": 1305,
                    "text": "Fig. 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1456,
                    "end": 1462,
                    "text": "Fig. 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1743,
                    "end": 1749,
                    "text": "Fig. 2",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 2004,
                    "end": 2010,
                    "text": "Fig. 3",
                    "ref_id": "FIGREF2"
                }
            ],
            "section": "Different Cases Analysis"
        },
        {
            "text": "The miscellaneous analysis consists of analysis based on age, camel contact and months. The users can analyze the data for all cases, death cases or recovered cases. Depending on the user's choice, the analysis corresponding to all cities, all regions or cities within a region are displayed.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Miscellaneous Analysis"
        },
        {
            "text": "Analysis Based on Age. The infected people are divided into three age categories namely below 25 years, 26-60 years and greater than 60 years. In the case of age based analysis, the information is displayed both in the form of pie chart and stacked bar chart. The pie chart represents the number of cases corresponding to each age group. The stacked bar chart depicts the number of cases in each age group corresponding to the location type selected. The screen shot corresponding to age category wise analysis is shown in Fig. 4 . The figure corresponds to the analysis corresponding to all cases reported with respect to cities for the first two months of the year 2019. It is evident from the pie chart Fig. 4 that the percentage of cases reported is 4.9%, 65.9% and 29.3% for the age group below 25 years, 26-60 years and above 60 years respectively. The age category wise helps in understanding the number of cases reported among people of different age groups. This type of analysis will help in identifying which group has more mortality rate and health authorities may conduct extensive analysis of causes leading to death.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 523,
                    "end": 529,
                    "text": "Fig. 4",
                    "ref_id": null
                },
                {
                    "start": 706,
                    "end": 712,
                    "text": "Fig. 4",
                    "ref_id": null
                }
            ],
            "section": "Miscellaneous Analysis"
        },
        {
            "text": "Analysis Based on Camel Contact. Camels have been identified as a carrier of the disease, and hence it is essential to perform analysis of patient cases involving camel contact. Depending on the choice of the user, the information is displayed for \"all cases\", \"death cases\" or \"recovery cases\". The analysis is carried out for \"all cities\", \"all regions\" or \"cities within a region\". The details are displayed as pie chart, map and table. The pie chart represents the numbers of cases corresponding to the location type selected by the user. The places where camel contact cases are reported are marked in the map. This can help in analyzing whether the cases pertaining to people with camel contact are clustered in a region or not. The table displays the details of infected people who had contact with camels. The data visualization based on camel contact is shown in Fig. 5 . The figure depicts the analysis corresponding to all cases with regard to cities in Saudi Arabia. It can be observed from Fig. 5 that 56% of the camel contact based cases were reported from the city of Wadi Aldawasir. Camel contact based analysis of MERS patients is highly essential to identify places where people in contact with camels have been infected with the disease. This will help health authorities in taking more measures to spread awareness about the precautionary methods to be taken when handling camels. The infected camels in such regions may be identified and isolated to prevent the spread of the disease. Month-wise comparison of the data can be carried out for \"all cases\", \"death cases\" and \"recovery cases\". The stacked bar chart for cases reported in different months is plotted based on the location type specified by the user. When the cursor is moved to a region in the bar chart, the number of cases in the particular month will be displayed corresponding to the location. The screenshot corresponding to month-wise comparison is shown in Fig. 6 . The figure portrays the analysis corresponding to death cases reported in various regions. The maximum number of death cases were reported in Riyadh region during February. Monthwise analysis is useful in identifying whether the infection is related to climate.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 872,
                    "end": 878,
                    "text": "Fig. 5",
                    "ref_id": "FIGREF3"
                },
                {
                    "start": 1003,
                    "end": 1009,
                    "text": "Fig. 5",
                    "ref_id": "FIGREF3"
                },
                {
                    "start": 1948,
                    "end": 1954,
                    "text": "Fig. 6",
                    "ref_id": "FIGREF4"
                }
            ],
            "section": "Fig. 4. Data visualization on the basis of age category"
        },
        {
            "text": "Summary based visualization gives a graphical summary of the count of the \"status of the patients\" for different attributes. The information is depicted in the form of stacked bar charts where the charts are plotted based on the frequency of status of patients corresponding to different values of the attributes. This visualization will help the users in getting an overall idea regarding the distribution of data with regard to the status of the patients. Figures 7 and 8 depict the screenshots of the visual summary of data for the first two months of the year 2019. Figure 7 confirms the earlier findings that Riyadh region and specifically Wadi Aldawasir city has the highest number of MERS cases and death cases. Figure 8 shows that most MERS patients are male and that people below 25 years are less likely to get infected by MERS. Elderly patients are more prone to die of MERS. The majority of patients were not in contact with camels and death rate was low in the case of patients in contact with camels. Many patients acquired the infection from healthcare facilities and high death rate is reported among this group.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 458,
                    "end": 473,
                    "text": "Figures 7 and 8",
                    "ref_id": "FIGREF5"
                },
                {
                    "start": 570,
                    "end": 578,
                    "text": "Figure 7",
                    "ref_id": "FIGREF5"
                },
                {
                    "start": 719,
                    "end": 727,
                    "text": "Figure 8",
                    "ref_id": "FIGREF6"
                }
            ],
            "section": "Summary-Based Visualization"
        },
        {
            "text": "The users can view information corresponding to their period of interest. The interactive feature of the pie chart prevents the chart from being cluttered with description. The use of maps to represent the information will give an idea regarding the spatial distribution of MERS cases. These maps will help the health authorities to identify the areas with large number of cases and hence alert the hospitals and the general public regarding that. The interactive nature of the table helps the users in analyzing the data as per their requirement. A salient feature of the application is that the users can easily download details corresponding to the period of their choice. Analyzing the data based on camel contact will aid health authorities to identify the areas where many such cases are present and hence intensify the awareness programs to reduce the rate of infection.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Benefits of the Application"
        },
        {
            "text": "In this paper, we have created an interactive visualization tool for MERS Co-V infection cases based on details of cases reported in Saudi Arabia. The attributes used include the date of reporting, city, region, age, gender, description of the disease, camel contact and status of the patient. The user can view details regarding all cases, recovered cases, death cases or hospitalized cases for all the cities, all regions or cities within a region. Our tool provides the flexibility to view information in the form of charts and maps, or downloadable tables. The analysis is also carried out based on age group, camel contact and month-wise cases. By viewing the maps, users can easily differentiate between places based on the number of cases. Moreover, the users can view a visual summary of the number of cases for different values of attributes based on the status of the patients. Understanding and analyzing the disease related information can help decision makers in setting guidelines for preventing and controlling the spread of disease. Location-based analysis of the infection is highly essential in formulating region specific awareness programs to reduce the rate of infection.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Isolation of a novel coronavirus from a man with pneumonia in Saudi Arabia",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "M"
                    ],
                    "last": "Zaki",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Van Boheemen",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "M"
                    ],
                    "last": "Bestebroer",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "D M E"
                    ],
                    "last": "Osterhaus",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "A M"
                    ],
                    "last": "Fouchier",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "N. Engl. J. Med",
            "volume": "367",
            "issn": "",
            "pages": "1814--1820",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Main factors influencing recovery in MERS Co-V patients using machine learning",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "John",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Shaiba",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jiph.2019.03.020"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "MERS-CoV infection of alpaca in a region where MERS-CoV is endemic",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "B"
                    ],
                    "last": "Reusken",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Emerg. Infect. Dis",
            "volume": "22",
            "issn": "6",
            "pages": "1129--1131",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Middle east respiratory syndrome coronavirus: risk factors and determinants of primary, household, and nosocomial transmission",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "S"
                    ],
                    "last": "Hui",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "I"
                    ],
                    "last": "Azhar",
                    "suffix": ""
                },
                {
                    "first": "Y.-J",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "A"
                    ],
                    "last": "Memish",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Oh",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Zumla",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Lancet Infect. Dis",
            "volume": "18",
            "issn": "8",
            "pages": "217--227",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Application page corresponding to Different Cases Analysis",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Fig. 2. Application page corresponding to \"Different Cases Analysis\" tab for cities within a region",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Application page corresponding to information displayed as table in",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Data visualization based on cases involving camel contact Analysis Based on Month-Wise Comparison.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF4": {
            "text": "Application page corresponding to month-wise comparison of cases",
            "latex": null,
            "type": "figure"
        },
        "FIGREF5": {
            "text": "Summary of attributes based on status of patients",
            "latex": null,
            "type": "figure"
        },
        "FIGREF6": {
            "text": "Summary of attributes based on status of patients",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Description of data used.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}