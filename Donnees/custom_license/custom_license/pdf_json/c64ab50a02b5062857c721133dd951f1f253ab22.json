{
    "paper_id": "c64ab50a02b5062857c721133dd951f1f253ab22",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Manifestations and Potential Fecal-Oral Transmission T he outbreak of novel coronavirus (2019-nCoV) pneumonia initially developed in one of the largest cities, Wuhan, Hubei province of China, in early December 2019 and has been declared the sixth public health emergency of international concern by the World Health Organization, and subsequently named coronavirus disease 2019 (COVID-19). As of February 20, 2020, a total of >75,000 cumulative confirmed cases and 2130 deaths have been documented globally in 26 countries across 5 continents. Current studies reveal that respiratory symptoms of COVID-19 such as fever, dry cough, and even dyspnea represent the most common manifestations at presentation similar to severe acute respiratory syndrome (SARS) in 2003 and Middle East respiratory syndrome in 2012, which is firmly indicative of droplet transmission and contact transmission. However, the incidence of less common features like diarrhea, nausea, vomiting, and abdominal discomfort varies significantly among different study populations, along with an early and mild onset frequently followed by typical respiratory symptoms. 1 Mounting evidence from former studies of SARS indicated that the gastrointestinal tract (intestine) tropism of SARS coronavirus (SARS-CoV) was verified by the viral detection in biopsy specimens and stool even in discharged patients, which may partially provide explanations for the gastrointestinal symptoms, potential recurrence, and transmission of SARS from persistently shedding human as well. 2 Notably, the first case of 2019-nCoV infection confirmed in the United States reported a 2-day history of nausea and vomiting on admission, and then passed a loose bowel movement on hospital day 2. The viral nucleic acids of loose stool and both respiratory specimens later tested positive. 3 In addition, 2019-nCoV sequence could be also detected in the self-collected saliva of most infected patients even not in nasopharyngeal aspirate, and serial saliva specimens monitoring showed declines of salivary viral load after hospitalization. 4 Given that extrapulmonary detection of viral RNA does not mean infectious virus is present, further positive viral culture suggests the possibility of salivary gland infection and possible transmission. 4 More recently, 2 independent laboratories from China declared that they have successfully isolated live 2019-nCoV from the stool of patients (unpublished). Taken together, a growing number of clinical evidence reminds us that digestive system other than respiratory system may serve as an alternative route of infection when people are in contact with infected wild animals or sufferers, and asymptomatic carriers or individuals with mild enteric symptoms at an early stage must have been neglected or underestimated in previous investigations. Clinicians should be careful to promptly identify the patients with initial gastrointestinal symptoms and explore the duration of infectivity with delayed viral conversion.",
            "cite_spans": [
                {
                    "start": 1137,
                    "end": 1138,
                    "text": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1538,
                    "end": 1539,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1831,
                    "end": 1832,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 2081,
                    "end": 2082,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 2286,
                    "end": 2287,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19: Gastrointestinal"
        },
        {
            "text": "To date, molecular modelling has revealed by the next-generation sequencing technology that 2019-nCoV shares about 79% sequence identify with SARS-CoV, indicative of these 2 lineage B b-coronaviruses highly homologous, and angiotensinconverting enzyme II (ACE2), previously known as an entry receptor for SARS-CoV, was exclusively confirmed in 2019-nCoV infection despite amino acid mutations at some key receptorbinding domains. 5, 6 It is widely accepted that coronavirus human transmissibility and pathogenesis mainly depend on the interactions, including virus attachment, receptor recognition, protease cleaving and membrane fusion, of its transmembrane spike glycoprotein (S-protein) receptor-binding domain, specific cell receptors (ACE2), and host cellular transmembrane serine protease (TMPRSS), with binding affinity of 2019-nCoV about 73% of SARS-CoV. 7 Recent bioinformatics analysis on available single-cell transcriptomes data of normal human lung and gastrointestinal system was carried out to identify the ACE2-expressing cell composition and proportion, and revealed that ACE2 was not only highly expressed in the lung AT2 cells, but also in esophagus upper and stratified epithelial cells and absorptive enterocytes from ileum and colon. 8 With the increasing gastrointestinal wall permeability to foreign pathogens once virus infected, enteric symptoms like diarrhea will occur by the invaded enterocytes malabsorption, which in theory indicated the digestive system might be vulnerable to COVID-19 infection. In contrast, because ACE2 and TMPRSS especially TMPRSS2 are co-localized in the same host cells and the latter exerts hydrolytic effects responsible for S-protein priming and viral entry into target cells, further bioinformatics investigation renders additional evidence for enteric infectivity of COVID-19 in that the high coexpression ratio was found in absorptive enterocytes and upper epithelial cells of esophagus besides lung AT2 cells. However, the exact mechanism of COVID-19-induced gastrointestinal symptom largely remains elusive. Based on these considerations, ACE2based strategies against COVID-19 such as ACE2 fusion proteins and TMPRSS2 inhibitors should be accelerated into clinical research and development for diagnosis, prophylaxis, or treatment.",
            "cite_spans": [
                {
                    "start": 430,
                    "end": 432,
                    "text": "5,",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 433,
                    "end": 434,
                    "text": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 863,
                    "end": 864,
                    "text": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1256,
                    "end": 1257,
                    "text": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19: Gastrointestinal"
        },
        {
            "text": "Last, mild to moderate liver injury, including elevated aminotransferases, hypoproteinemia, and prothrombin time prolongation, has been reported in the existing clinical investigations of COVID-19, whereas up to 60% of patients suffering from SARS had liver impairment. The presence of viral nucleic acids of SARS in liver tissue confirmed the coronavirus direct infection in liver, and percutaneous liver biopsies of SARS showed conspicuous mitoses and apoptosis along with atypical features such as acidophilic bodies, ballooning of hepatocytes, and lobular activities without fibrin deposition or fibrosis. 9 It is believed that SARS-associated hepatotoxicity may be likely with viral hepatitis or a secondary effect associated with drug toxicity owing to high-dose consumption of antiviral medications, antibiotics, and steroids, as well as immune system overreaction. However, little is known about 2019-nCoV infection in liver. Surprisingly, recent single cell RNA sequencing data from 2 independent cohorts revealed a significant enrichment of ACE2 expression in cholangiocytes (59.7% of cells) instead of hepatocytes (2.6% of cells), suggesting that 2019-nCoV might lead to direct damage to the intrahepatic bile ducts. 10 Altogether, substantial effort should be made to be alert on the initial digestive symptoms of COVID-19 for early detection, early diagnosis, early isolation, and early intervention. ",
            "cite_spans": [
                {
                    "start": 610,
                    "end": 611,
                    "text": "9",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1228,
                    "end": 1230,
                    "text": "10",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19: Gastrointestinal"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Clinical characteristics of 138 hospitalized patients with 2019 novel coronavirus-infected pneumonia in Wuhan, China",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Enteric involvement of severe acute respiratory syndromeassociated coronavirus infection",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "K"
                    ],
                    "last": "Leung",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "F"
                    ],
                    "last": "To",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "K"
                    ],
                    "last": "Chan",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Gastroenterology",
            "volume": "125",
            "issn": "",
            "pages": "1011--1017",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "First case of 2019 novel coronavirus in the United States",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Holshue",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Debolt",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Lindquist",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "",
            "pages": "929--936",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Consistent detection of 2019 novel coronavirus in saliva",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "K"
                    ],
                    "last": "To",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [
                        "T"
                    ],
                    "last": "Tsang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Chik-Yan Yip",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Clin Infect Dis",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Genomic characterisation and epidemiology of 2019 novel coronavirus: implications for virus origins and receptor binding",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "565--574",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "A pneumonia outbreak associated with a new coronavirus of probable bat origin",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [
                        "L"
                    ],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [
                        "G"
                    ],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Nature",
            "volume": "579",
            "issn": "",
            "pages": "270--273",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Fast assessment of human receptorbinding capability of 2019 novel coronavirus (2019-nCoV)",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Herrmann",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "The digestive system is a potential route of 2019-nCov infection: a bioinformatics analysis based on single-cell transcriptomes",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "J"
                    ],
                    "last": "Kang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "Y"
                    ],
                    "last": "Gong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "SARS-associated viral hepatitis caused by a novel coronavirus: report of three cases",
            "authors": [
                {
                    "first": "T",
                    "middle": [
                        "N"
                    ],
                    "last": "Chau",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Yao",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Hepatology",
            "volume": "39",
            "issn": "",
            "pages": "302--310",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Specific ACE2 expression in cholangiocytes may cause liver damage after 2019-nCoV infection",
            "authors": [
                {
                    "first": "X",
                    "middle": [
                        "Q"
                    ],
                    "last": "Chai",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "F"
                    ],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}