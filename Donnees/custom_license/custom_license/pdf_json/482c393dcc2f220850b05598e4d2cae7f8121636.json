{
    "paper_id": "482c393dcc2f220850b05598e4d2cae7f8121636",
    "metadata": {
        "title": "Rapid-response vaccines-does DNA offer a solution?",
        "authors": [
            {
                "first": "Gareth",
                "middle": [
                    "M"
                ],
                "last": "Forde",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Monash University",
                    "location": {
                        "postCode": "3800",
                        "settlement": "Clayton, Melbourne",
                        "country": "Australia"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The world is teetering on the verge of a massive influenza outbreak 1 . In the face of such a pandemic, existing manufacturers are unlikely to have sufficient production capacity to produce new vaccines at the scale and speed required to protect public health. I argue here that countries with established vaccine production infrastructure must invest greater effort and resources into developing innovative DNA vaccine technology, which ultimately may empower more countries to produce their own vaccines.",
            "cite_spans": [
                {
                    "start": 68,
                    "end": 69,
                    "text": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The World Health Organization (WHO) has stated that avian flu has the potential to kill many more people than severe acute respiratory syndrome (SARS), which to date has accounted for 774 deaths. As of June 28, the WHO has confirmed 54 deaths in total from infection with avian influenza A subtype H5N1 (hemagglutinin 5/neuraminidase 1) 2 . H5N1 is of particular concern because it mutates rapidly and has a documented capacity to acquire genes from viruses infecting other animal species. Most of the cases of transmission have been traced to contact with animals, but if the virus evolves to cross the species barrier with ease, health officials warn of catastrophic consequences for human health (and indirectly the global economy).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "A looming health and manufacturing crisis"
        },
        {
            "text": "As a response, the United States has contracted for 4 million doses of influenza vac-cine, with Italy and France anticipating the stockpiling of 2 million doses each and more recently the United Kingdom ordered 14 million doses of an anti-viral drug. The words 'contracted,' 'anticipating' and 'ordered' must be stressed as, according to a WHO report 3 , the ability of current manufacturers to produce vaccines and drugs in response to a pandemic is woefully inadequate-estimated at ~300 million shots per year 4 .",
            "cite_spans": [
                {
                    "start": 351,
                    "end": 352,
                    "text": "3",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 512,
                    "end": 513,
                    "text": "4",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "A looming health and manufacturing crisis"
        },
        {
            "text": "The lack of manufacturing capacity has arisen because only five major manufacturers (London-based GlaxoSmithKline, Parisheadquartered Sanofi-Aventis/Pasteur, Merck of Rahway, New Jersey, Wyeth in Madison, New Jersey and Emeryville-based Chiron) now focus on producing vaccines. In the 2004-2005 flu season, only GlaxoSmithKline, Sanofi-Aventis/Pasteur and Chiron were manufacturing and distributing a flu vaccine, with Chiron again running into manufacturing problems, this time at its plant in Germany.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "A looming health and manufacturing crisis"
        },
        {
            "text": "There are several reasons why only 5 companies manufacture vaccines today compared with more than 25 companies in 1967 (ref. 5).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "A looming health and manufacturing crisis"
        },
        {
            "text": "One contributor is the sector's relatively poor profit margins and high fixed costs for manufacture. Today, the entire vaccine market is estimated to be only $8 billion 5 ; in contrast, a single blockbuster drug may make a company around $3 billion annually (the biggest seller, New York-based Pfizer's Lipitor (atorvastatin), brought in $9.2 billion in 2003; ref. 6). Overall, the size of the vaccine market is estimated to be less than 2% that of the pharmaceutical market 7 . Government bulk buying and price controls have not helped. Nor has the increased risk of liability exposure for vaccines compared with therapeutics.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "A looming health and manufacturing crisis"
        },
        {
            "text": "On the other hand, increasingly stringent regulatory conditions for vaccine testing, licensure and production have markedly increased the outlay needed for manufacturing facilities, personnel and technology. Vaccines have long processing times and are difficult (thus expensive) to produce. The failure of a production batch results in extreme financial ramifications for the manufacturer. Technological issues, stemming from production difficulties, are crippling the vaccine industry and leading to vaccine shortages and financial woes for companies. Last year, for example, the US Food and Drug Administration (FDA) was caught off guard by a shortage of flu vaccine caused by a contamination in a UK plant of Chiron, one of the United States' two major suppliers (the other was Aventis Pasteur) contracted for the 2003-2004 flu season 8 . Added to this, waning corporate interest in the sector has diminished commercial incentives for innovation and technology transfer that could address these problems.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "A looming health and manufacturing crisis"
        },
        {
            "text": "In the case of an influenza pandemic, vaccination will be one of the key interventions. A crucial stage in the control of a pandemic will be to provide as many vaccine doses against the new viral strain in the shortest possible time; however, the shots currently licensed for use are based on ancient technology (in a biotechnology time frame). Current chemically inactivated trivalent influenza vaccines are produced by coinfecting batches of eggs with the epidemic influenza strains and another strain of influenza (A/Puerto Rico/8/34) that grows well in eggs. The hit-and-miss process of screening infected eggs for recombined vaccine strains that not only can multiply efficiently in ova, but also provide protection against the epidemic strains takes time (often months) and luck. In the face of an H5N1 influenza pandemic, such an approach is unlikely to be tenable (Fig. 1) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 872,
                    "end": 880,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Current influenza vaccine technology"
        },
        {
            "text": "The advent of reverse genetics has enabled precise manipulation and assembly of flu virus genomes in vitro to create new vaccines 9, 10 . To create an H5N1 vaccine, for example, the hemagglutinin gene can be altered to remove motifs associated with virulence and then stitched together with other parts of the influenza genome to make a replicating vaccine strain, which can subsequently be used as a live preparation or chemically inactivated 9 . It is estimated that a vaccine strain can be made by this approach in just four weeks from a wild-type viral isolate 11 . Yet, the efficacy and safety of such vaccines are only just beginning to be tested in the clinic 4 and the production of vaccine batches, quality control, product finishing, control agency testing and distribution all significantly prolong the time needed before a vaccine can be administered to susceptible populations ( Fig. 1) .",
            "cite_spans": [
                {
                    "start": 130,
                    "end": 132,
                    "text": "9,",
                    "ref_id": null
                },
                {
                    "start": 133,
                    "end": 135,
                    "text": "10",
                    "ref_id": null
                },
                {
                    "start": 444,
                    "end": 445,
                    "text": "9",
                    "ref_id": null
                },
                {
                    "start": 565,
                    "end": 567,
                    "text": "11",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 892,
                    "end": 899,
                    "text": "Fig. 1)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Current influenza vaccine technology"
        },
        {
            "text": "Clearly, in a pandemic the demand for influenza vaccine will significantly surpass the available supply from existing manufacturing capacities 12 . Once a pandemic strain emerges, researchers will be in a frantic race against time to create an effective vaccine. Although the stockpiling of flu vaccine might help, there is always the possibility that the new epidemic strain could be sufficiently different from human vaccines against H5N1 and H9N2 currently in testing to render stockpiles ineffective.",
            "cite_spans": [
                {
                    "start": 143,
                    "end": 145,
                    "text": "12",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Current influenza vaccine technology"
        },
        {
            "text": "As a response, the WHO has put forward a range of ideas. These include exchanging clinical trial data, antigen-sparing (e.g., using adjuvants to enhance antigen effectiveness thus stretching limited supplies), increased funding for a pandemic vaccine, tax/other incentives for industry and waiving regulatory licensing fees. But the solution is likely to be a lot more technically complex. As alluded to briefly in the report 3 , the greatest need of all in addressing a pandemic is vaccine production, which leads to a demand for forward-looking innovations to address production issues.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Current influenza vaccine technology"
        },
        {
            "text": "Vaccines based on plasmid DNA offer a potential solution to the looming influenza vaccine crisis. The platform is now at a sufficient stage of development, after extensive experimental work and clinical trials [13] [14] [15] [16] [17] [18] , to be considered seriously as a vaccine option.",
            "cite_spans": [
                {
                    "start": 210,
                    "end": 214,
                    "text": "[13]",
                    "ref_id": null
                },
                {
                    "start": 215,
                    "end": 219,
                    "text": "[14]",
                    "ref_id": null
                },
                {
                    "start": 220,
                    "end": 224,
                    "text": "[15]",
                    "ref_id": null
                },
                {
                    "start": 225,
                    "end": 229,
                    "text": "[16]",
                    "ref_id": null
                },
                {
                    "start": 230,
                    "end": 234,
                    "text": "[17]",
                    "ref_id": null
                },
                {
                    "start": 235,
                    "end": 239,
                    "text": "[18]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "The rise of DNA vaccines"
        },
        {
            "text": "In a typical DNA vaccination protocol, an individual is not given the protein antigen, but DNA encoding the antigen. The DNA segment that encodes the protein antigen is incorporated into plasmid DNA that may be administered in the same way as conventional vaccines. The plasmid is taken up by the relevant cell types (usually dendritic cells in skin or muscle), where it is capable of replicating independently of chromosomal DNA and can transcribe the gene encoding the antigen of interest. Fragments of the expressed protein are degraded into peptides that can be presented by both major histocompatibility complex (MHC) class I and MHC class II molecules, depending upon the cell type and method of administration (gene gun or injection). Despite the production of very small amounts of antigen 13 , both antibody and cellular responses can be induced.",
            "cite_spans": [
                {
                    "start": 798,
                    "end": 800,
                    "text": "13",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "The rise of DNA vaccines"
        },
        {
            "text": "Compared with the practice of variolation-which Edward Jenner used in 1796 to produce the first smallpox vaccine in the United Kingdom, but was documented in China as early as the 10 th century-plasmid DNA vaccination is a very new approach. Indeed, its technical feasibility was first demonstrated only 15 years ago by Wolff et al. 13 , who reported that intramuscular inoculation with plasmid DNA encoding reporter genes could induce expression without the need for a special delivery system (e.g., a viral vector). This work laid the foundation for the first observations by Ulmer et al. 19 in 1993 that a DNA vaccine could protect animals from influenza infection. The first reported use of a plasmid DNA vaccine outside of trial conditions or an experimental setting was in 2003 for the immunization of California condors against West Nile Virus 18 . In an almost last ditch attempt to save the remaining 200 endangered California Condors, the US Centers for Disease Control and Prevention (CDC) in Atlanta, Georgia, rushed through a 5.3-kb plasmid DNA vaccine containing a piece of West Nile Virus genome that encodes the prM and prE proteins.",
            "cite_spans": [
                {
                    "start": 333,
                    "end": 335,
                    "text": "13",
                    "ref_id": null
                },
                {
                    "start": 591,
                    "end": 593,
                    "text": "19",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "The rise of DNA vaccines"
        },
        {
            "text": "The entire process from initial development to the final product took North Dakota-based Aldevron, the company contracted by the CDC to manufacture the vaccine, approximately one month. The creation of a cell bank and optimized manufacturing system meant that large quantities of the plasmid could be produced significantly faster than would have been possible with a conventional vaccine.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The rise of DNA vaccines"
        },
        {
            "text": "But most importantly, did it work? According to Cynthia Stringfield, a veterinarian at the Los Angeles Zoo, the condors had absolutely zero negative effects and blood tests suggest that the birds \"had a fantastic immune response\" 20 . This is a promising result.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The rise of DNA vaccines"
        },
        {
            "text": "Several features of DNA vaccines make them more appealing than conventional and even recombinant subunit vaccines. Firstly, DNA's amenability of to manipulation and its expression in a recipient's own cells allows greater control over the immunization process than with other platforms. An investigator can determine which antigens and costimulants to use, where to elicit the response (e.g., skin or muscle), which cytokines (if any) need to be coexpressed and whether to use co-stimulatory DNA sequences to modulate the type of immune response (T helper (Th) cells: Th1 or Th2). The ability of plasmid DNA to hold multiple genes, for example, hemagglutinins of multiple influenza strains, offers an easy way to mix-and-match antigens in new combinations.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "What do DNA vaccines offer?"
        },
        {
            "text": "Secondly, a plasmid DNA vaccine will be designed to be noninfectious and encode only the antigen(s) of interest as opposed to live attenuated vaccines or viral carrier systems. Where required, several antigens can be encoded simultaneously.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "What do DNA vaccines offer?"
        },
        {
            "text": "Thirdly, in addition to providing the coding sequences for the antigen(s) of interest, plasmid DNA serves as the physical vector and can also possibly function as an adjuvant to improve the immune response; indeed, DNA vaccination is increasingly being tested in prime-boost strategies aiming to ameliorate immune responses to many types of vaccine. It may be possible to manipulate DNA to increase its adjuvant effects by the addition of multiple immunostimulatory sequences.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "What do DNA vaccines offer?"
        },
        {
            "text": "Finally, DNA vaccines have excellent safety profiles; they display no toxicity or immunogenicity, drawbacks often associated with viral vectors 21 . Unlike other types of vaccine (including recombinant subunit vaccines using viral vectors), DNA vaccines do not contain heterologous protein components to which the host may respond. Thus, they can, like live vaccine immunization, induce both MHC class I and II responses, without the risks inherent in live virus immunization 22 .",
            "cite_spans": [
                {
                    "start": 144,
                    "end": 146,
                    "text": "21",
                    "ref_id": null
                },
                {
                    "start": 476,
                    "end": 478,
                    "text": "22",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "What do DNA vaccines offer?"
        },
        {
            "text": "But it is perhaps the advantages to be gained in terms of production time that make DNA vaccines of particular relevance in combating emerging infectious agents, such as a new virulent influenza strain. The current development time to turn raw ingredients into a finished vaccine in the surgery fridge ready for administration to a patient is almost 20 months (Fig. 1) 23 . This includes 4-9 months for production, 3 months for quality control, 1-2.5 months for product finishing, up to 2 months for control agency testing and potentially over 2 months for distribution.",
            "cite_spans": [
                {
                    "start": 369,
                    "end": 371,
                    "text": "23",
                    "ref_id": "BIBREF21"
                }
            ],
            "ref_spans": [
                {
                    "start": 360,
                    "end": 368,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Streamlining manufacture"
        },
        {
            "text": "It has been shown that a DNA vaccine could reduce the initial production period to one month 24 , reducing the total process time by up to eight months (or 40%). If plasmid DNA technology were to be applied to influenza vaccines, how many lives could this save in the first eight months of a global pandemic?",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Streamlining manufacture"
        },
        {
            "text": "In total, 50-80% of the total production costs of vaccine manufacture are incurred during downstream purification 25 . Here again, advances in purification processes for DNA, such as targeted affinity chromatography, are enabling highly selective and facile purification of DNA vaccines, even from complex feed stocks 26, 27 . By enabling the capture and purification of vastly different types of DNA in one chromatographic step, without the copurification of contaminants such as genomic DNA, RNA, protein and endotoxins, these processes could potentially eliminate the need for further purification or polishing stages that are required for traditional vaccines.",
            "cite_spans": [
                {
                    "start": 114,
                    "end": 116,
                    "text": "25",
                    "ref_id": "BIBREF22"
                },
                {
                    "start": 318,
                    "end": 321,
                    "text": "26,",
                    "ref_id": null
                },
                {
                    "start": 322,
                    "end": 324,
                    "text": "27",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Streamlining manufacture"
        },
        {
            "text": "There remain, however, some areas of concern with the use of DNA as a vaccine. These include poor translation from mouse models. Whereas in some cases, DNA vaccines have elicited strong immune responses in mice, such responses have been more disappointing in primates and humans. It is becoming clear, however, that immune responses can be ameliorated via the inclusion in the plasmid of genes encoding various immunostimulatory molecules. An additional concern is the possibility that DNA vaccines could elicit the production of antibodies not to their encoded antigens but instead to double-stranded plasmid DNA molecules themselves. This could not only compromise repeated use of DNA vaccines as boosters, but also, more seriously, prompt the development of autoimmune diseases.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "What's the catch?"
        },
        {
            "text": "It also appears that DNA molecules containing certain bases (e.g., unmethylated C\u2022G dinucleotides in specific base sequence contexts; CpG motifs) have the ability to activate Toll-like receptors 28 and also to preferentially activate T-helper cell 1(Th1)-type immune responses. As more is learned about the underlying mechanisms, it is possible, of course, that DNA vaccines may be designed to exploit these effects for adjuvant activity or to promote certain types of immune protection.",
            "cite_spans": [
                {
                    "start": 195,
                    "end": 197,
                    "text": "28",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "What's the catch?"
        },
        {
            "text": "Another generic problem is the low efficiency of cell transformation by plasmid DNA, which is affected by the manner of administration, dosage and formulation of plasmid DNA. Physical (e.g., injection, gene gun, electroporation or aerosol delivery), chemical (e.g., cationic lipid or polymer condensing agents) and biological (e.g., use of the cellular transport mechanism) approaches are all being explored to overcome low target cell specificity.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "What's the catch?"
        },
        {
            "text": "Current influenza vaccine production methods are based on 50-year old technology. Production issues associated with this outmoded technology (e.g., the requirement for 100% inactivation or attenuation of a vaccine or delays in delivery of batches of appropriate quality hen's eggs) compromise its utility for responding to a public health crisis such as a flu pandemic.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The future"
        },
        {
            "text": "In contrast, plasmid DNA can be reverse engineered using recombinant techniques, and manufactured and purified quickly, allowing vaccine production in a much shorter time; indeed, production times could be reduced from up to nine months for conventional vaccines to as little as one month for a DNA vaccine. Notwithstanding their production advantages, plasmid DNA vaccines also offer greater flexibility (as several different antigens can be prepared using standard recombinant techniques), greater control over the immunization process, an excellent safety profile from animal and early phase human studies, and stability and resistance to extremes of temperature, thus facilitating storage, transportation and distribution to remote areas.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The future"
        },
        {
            "text": "Currently several DNA vaccines are in development for veterinary purposes and around a dozen DNA vaccines are in human development worldwide, but none has yet proceeded beyond phase 2 trials. Gene therapy data 29 show that not only is the total number of trials using plasmid DNA increasing, but so also is the percentage of trials (now up to 16% of all gene therapy trials) (Fig. 2) . This indicates that the interest in plasmid DNA as a vector is gaining momentum relative to most other vectors (e.g., retrovirus and adenovirus).",
            "cite_spans": [
                {
                    "start": 210,
                    "end": 212,
                    "text": "29",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 375,
                    "end": 383,
                    "text": "(Fig. 2)",
                    "ref_id": "FIGREF2"
                }
            ],
            "section": "The future"
        },
        {
            "text": "But for DNA vaccines to realize their potential, several challenges lie ahead. Firstly, and perhaps most important, a product must pass through late-stage trials, regulatory scrutiny and be approved for marketing in the human population. Secondly, plasmid contract manufacturers will need to ramp up their production (which is already experiencing rapid growth) and introduce patentable, high-resolution purification procedures that can speed processing and manufacture. From an economic standpoint, the cost per dose of a DNA vaccine must be brought down to levels that are economically competitive with conventional vaccines. Wider use of nucleic acids in other pharmaceutical applications, such as gene therapy or gene correction, will likely lead to processing advances and technology transfer that reduce costs. Such advances may include the development of a process to create much shorter fragments of DNA that can elicit the same response as the much larger plasmid DNA molecules, leading to high transformation efficiencies and improved dosage time yields (doses created per hour) from processing equipment.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The future"
        },
        {
            "text": "If all of the above issues can be addressed, it is likely not only that the barriers of entry into the vaccine development market could be lowered but also that more countries would be able to produce their own vaccines, rather than relying on a small number of producers. For this to happen, it becomes painfully clear that research centers, public health institutions, governments and vaccine manufacturers throughout the world, and particularly in countries that have strengths in vaccine production, will need to collaborate in taking action towards solving the current vaccine production problem to confront the looming pandemic.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The future"
        },
        {
            "text": "The countries and areas named by WHO in January 30 as having manufacturing capacity for influenza vaccines are Australia, Europe, Japan and North America. It is up to all groups with traditional strengths in vaccine production to lead the way in developing forward-looking innovations to address woefully inadequate production capacities. For the short-term and long-term global health of the world, it is time to break out of the traditional molds. DNA vaccines need to be proactively explored and knowledge transferred and shared so that their clinical application can be accelerated. The alternative is to rely on a vaccine manufacturing capacity that will be hopelessly outdated and inadequate in the face of an influenza pandemic.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The future"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Board on Global Health. The Threat of Pandemic Influenza: Are We Ready?",
            "authors": [],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Epidemic Alert and Response: Vaccines for Pandemic Influenza",
            "authors": [],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Global Alliance for Vaccines and Immunization. Immunization Financing Options",
            "authors": [],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Flu vaccine production system shaky",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wahlberg",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Seabrook",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "New York Times Syndicate",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "Nile's widening toll",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Weiss",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "West",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Washington Post",
            "volume": "01",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF21": {
            "ref_id": "b21",
            "title": "Production: How are Vaccines Made? (Association of the British Pharmaceutical Industry",
            "authors": [],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF22": {
            "ref_id": "b22",
            "title": "Method and device for isolating and purifying a polynucleotide of interest on a manufacturing scale",
            "authors": [],
            "year": 2003,
            "venue": "Boehringer Ingelheim",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF27": {
            "ref_id": "b27",
            "title": "World Health Organization. Influenza Pandemic Preparedness and Response (WHO",
            "authors": [],
            "year": 2005,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF1": {
            "text": "Schematic of the time scale for the development of a vaccine. DNA vaccine may provide time savings, reducing the production stage from 4-9 months to only 1 month.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Plot showing increasing usage of naked/plasmid DNA in gene therapy trials throughout the world29 . Both the number of trials and the percentage of total trials using naked or plasmid DNA has been increasing since 2000. No data were available later than 2003.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Testing as above plus correct volumes Control agency testing: up to 2 months May include further toxicology, efficacy and clinical trial testing by one of the official European and/or US medicine control agencies",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}