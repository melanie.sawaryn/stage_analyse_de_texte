{
    "paper_id": "8da0813db7e4722a2c682412429676c64afd5aba",
    "metadata": {
        "title": "Operating During the COVID-19 Pandemic: How to Reduce Medical Error",
        "authors": [
            {
                "first": "R",
                "middle": [],
                "last": "Ellis",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Royal Derby Hospital",
                    "location": {
                        "postCode": "DE22 3NE",
                        "settlement": "Derby",
                        "country": "UK"
                    }
                },
                "email": ""
            },
            {
                "first": "A",
                "middle": [
                    "G C"
                ],
                "last": "Hay-David",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Elizabeth University Hospital",
                    "location": {
                        "postCode": "G51 4TF",
                        "settlement": "Glasgow",
                        "region": "Queen",
                        "country": "UK"
                    }
                },
                "email": ""
            },
            {
                "first": "P",
                "middle": [
                    "A"
                ],
                "last": "Brennan",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Queen Alexandra Hospital",
                    "location": {
                        "postCode": "PO6 3LY",
                        "settlement": "Portsmouth",
                        "country": "UK"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Our professional and private lives changed on March 11 2020 when the coronavirus disease 2019 (COVID-19) was declared a pandemic by the WHO. By March 16, surgical training was suspended, MRCS and FRCS examinations cancelled and all courses postponed.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "In theory, essential cancer surgery, emergency and trauma operating will continue. All elective, non-essential cases are currently cancelled. While we adapt to our new ways of working, we remind ourselves that surgeons are flexible, resilient and, ultimately, we are doctors in the first instance.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "We present a short article on operating during the COVID-19 pandemic.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "The COVID-19 pandemic is likely to be the biggest public health crisis that we will encounter in our lifetimes. The scale of viral spread worldwide, coupled with the subsequent burden on healthcare systems, makes tackling the virus a monumental task. Governments around the world are employing strategies to minimise loss of life while trying to maintain functioning economies. The changes in healthcare in the United Kingdom, seen over the last few weeks, are unprecedented. Intensive care capacity has been increased exponentially, resources including personal protection equipment (PPE) and ventilators are being manufactured at an impressive rate, and, the workforce is being mobilised or redeployed. It is estimated that 2,660 doctors are among the * Corresponding author: Maxillofacial Unit, Queen Alexandra Hospital, Portsmouth, PO6 3LY, UK.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "E-mail address: peter.brennan@porthosp.nhs.uk (P.A. Brennan).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "12,000 retired NHS staff returning to clinical practice. 1 Medical and nursing students are volunteering their services. Final year medical students will have their graduation date brought forward in order to join the workforce. Since its launch on March 24 2020, 405,000 people applied to be a NHS volunteer responder within the first 24 hours. This number now stands at 700,000. 2 The response from the general public is mixed: one of fear and frustration. Initially manifested as panic-buying from supermarkets (soap, antibacterial hand gel and toilet rolls), there is now a gradual acceptance that social distancing and self-isolation will stop the spread of covid-19 and thereby reduce the strain on the NHS. At time of writing 203 countries / areas are affected with 754,948 confirmed cases and 36,571 confirmed deaths. 3 ",
            "cite_spans": [
                {
                    "start": 57,
                    "end": 58,
                    "text": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 381,
                    "end": 382,
                    "text": "2",
                    "ref_id": null
                },
                {
                    "start": 826,
                    "end": 827,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Updates from the four Royal Surgical Colleges have been delivered on a regular basis, providing support by keeping dialogue open (emails and webinars) and by providing educational material.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Adapting to our new roles"
        },
        {
            "text": "It is unsurprising that some clinicians find this an unfamiliar and unnerving time with loss of routine and a rota that changes weekly (if not daily). A reminder by Dame Clare Marx, GMC Chair, that we must be flexible: to stick to basic principles; to adhere to GMC guidance; work intelligently; and, minimise risks to our own health. 4 With the cancellation of elective operating and clinics, clinicians are being redistributed throughout hospitals to assist colleagues in other specialties. These may include roles outside our comfort zones / normal remit. Many surgical colleagues are being trained in managing unwell patients suffering with coronavirus on the wards, others have been supporting emergency departments and intensive care units.",
            "cite_spans": [
                {
                    "start": 335,
                    "end": 336,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Adapting to our new roles"
        },
        {
            "text": "Despite covering an increasing number of patients admitted with COVID-19, surgeons will still be dealing with emergency surgical admissions and will continue to operate on emergency cases. Many of these patients will be COVID-19 positive when tested and some may be acutely unwell as a result of the infection. Emergency operating during the COVID-19 crisis can be challenging for a wide variety of reasons. Additionally, we have started to see patients presenting late to hospital with advanced surgical pathology often due to a fear of hospital admission during this pandemic or due to a reduction in outpatient activity.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Adapting to our new roles"
        },
        {
            "text": "In light of these new and unfamiliar challenges, there are resources available to help surgeons revise their knowledge of acute medicine and receive updates on COVID-19 via webinars 5, 6 ; critical care websites with up to date guidelines and handbooks (such as the intensive care society: www.ics.ac.uk/ICS/handbooks.aspx); Systemic Training in Acute Illness Recognition and Treatment for Surgery(START) course, that includes a human factors presentation, on the RCS website; Non-Technical Skills for Surgeons course (NOTSS) presentations are available on the RCS Edinburgh website (www.rcsed.ac.uk) to both members and non-members.",
            "cite_spans": [
                {
                    "start": 182,
                    "end": 184,
                    "text": "5,",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 185,
                    "end": 186,
                    "text": "6",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "What We Can Do Now"
        },
        {
            "text": "A greater understanding and appreciation for human factors (HF) can have a significant impact in the reduction of surgical error in such challenging circumstances. 7 Surgeons will find themselves operating in different theatres to normal and with unfamiliar (or different) equipment. Theatre teams may be unfamiliar with a particular specialty and we may find ourselves operating out of hours more frequently on acutely unwell patients. Never has there been a more important time to utilise our non-technical skills to reduce the risk of surgical error.",
            "cite_spans": [
                {
                    "start": 164,
                    "end": 165,
                    "text": "7",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Human Factors and Non-Technical skills"
        },
        {
            "text": "We must take care to introduce ourselves and our role to all new team members during the briefing, to outline the proposed operation and its steps and to ensure all of the required equipment has been sourced before proceeding. Clarify any expected or potential concerns from a surgical and an anaesthetic perspective and ask the team if they have any questions. In some situations, surgical options may be limited by the patient's general health or other factors, it is therefore important that these are considered before the planned procedure begins. The patient's COVID-19 status must also be identified to ensure the appropriate PPE is used in line with the trust's and Public Health England (PHE) advice.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Human Factors and Non-Technical skills"
        },
        {
            "text": "At time of writing, PHE advise full PPE (FFP3 mask, visor or safety glasses, gown and gloves) for aerosol generating procedures (AGPs) for patients with known or symptomatic COVID-19. There is currently variation in practise between trusts as to whether all patients should be treated as having COVID-19.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "Masks require training to ensure correct fit. Some trusts use the N95 mask that you must \"pass\" the test in order to use safely. If you are able to taste the aerosolised bittersolution sprayed you have \"failed\" the test and it is ineffective in providing protection. Training is also required in the donning and doffing process to ensure this is done correctly and minimises your own risk of exposure.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "Operating can be challenging: uncomfortable and unfamiliar. Full PPE can be distracting when first worn, therefore where possible operate with other senior team members to reduce the risk of error. Furthermore, as we have experienced, FFP3 masks can make some procedures such as operating down a microscope more challenging as it can affect the usual comfortable eye positions (Fig. 1) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 377,
                    "end": 385,
                    "text": "(Fig. 1)",
                    "ref_id": null
                }
            ],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "Wearing face masks and hoods can also significantly reduce the clarity of verbal communication between theatre staff. Additionally, it may be more difficult than usual to read non-verbal cues. Therefore, care must be taken to ensure open, focussed channels of communication are established between all team members throughout the entire procedure. We must use names to address individual team members and ask people to repeat our requests back to us to ensure a shared understanding.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "Situational awareness may be impaired given the circumstances; therefore care should be taken to ensure all team members are aware of our expectations for the procedure including volume of blood loss and duration. Consider allocating roles for various members of the team to raise the alarm if these exceed what is expected. Fig. 1 . FFP3 mask and visor can impede certain procedures such as getting close to an operating microscope. 12 Operating in full PPE can also be exhausting, when performing long procedures, breaks may be required to prevent fatigue. Dehydration and hunger are also linked to an increased risk of surgical error; therefore it is imperative that we look after ourselves at work. 8 Lack of sleep has been attributed to slower cognitive processing and decision making, increasing the risk of error. 9 Despite increasing workloads during this pandemic it is essential that clinicians take care of their own health to optimise their ability to care for others.",
            "cite_spans": [
                {
                    "start": 434,
                    "end": 436,
                    "text": "12",
                    "ref_id": null
                },
                {
                    "start": 703,
                    "end": 704,
                    "text": "8",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 821,
                    "end": 822,
                    "text": "9",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [
                {
                    "start": 325,
                    "end": 331,
                    "text": "Fig. 1",
                    "ref_id": null
                }
            ],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "Burn out amongst clinicians may become more common in such a demanding environment. It is important that we look out for this in ourselves and our colleagues, recognising and dealing with this early before it can lead to sub-optimal performance. 10 Many clinicians are away from families in order to avoid risk to loved ones. There are also more widespread concerns causing stress and anxiety: COVID-19 testing (patients and healthcare workers); distrust of governmental decisions; unnecessary \"fake\" news and scaremongering; concern over inconsistent policies by different trusts in the UK on PPE use (WHO standards vs PHE advice); concern for an overstretched health service that cannot deliver care; and, normal concerns for their own heath and that of their families. Trusts have access to support for healthcare workers suffering from burn out which should be utilised at the earliest opportunity.",
            "cite_spans": [
                {
                    "start": 246,
                    "end": 248,
                    "text": "10",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "We have entered a period of unease and uncertainty due to the COVID-19 pandemic and are seeing unprecedented changes in healthcare: junior members of staff training their seniors on how to complete ward jobs and other administrative tasks; intensive care nurses supervising non-intensive care unit doctors looking after patients in over-flow ICU spaces; surgeons working alongside physicians; and, senior team members taking bloods and cannulating patients.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusion"
        },
        {
            "text": "Overall, this public health crisis has united us in our pursuit of a common goal -flattening the work hierarchy and provoking a considerable change in the way we work in the UK. 11 When we emerge from this crisis, we hope that we will continue to work together as valued team members.",
            "cite_spans": [
                {
                    "start": 178,
                    "end": 180,
                    "text": "11",
                    "ref_id": "BIBREF11"
                }
            ],
            "ref_spans": [],
            "section": "Conclusion"
        },
        {
            "text": "None.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflict of interest"
        },
        {
            "text": "N/A. Q3",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Ethics statement/confirmation of patient permission"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Concerns over death-in-service benefits keeping doctors from NHS frontline. The Guardian",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Booth",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Campbell",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "World Health Organisation. Coronavirus disease (COVID-19) outbreak situation",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Our message to the profession",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Marx",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Gmc",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "stop-at-nothing-to-provide-care-in-this-crisis-our-job-is-to-supportthem)",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "COVID-19): Essential knowledge for surgeons undertaking acute medical care",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Rcsed",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Coronavirus",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "European Centre for Disease Prevention and Control",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Webinars On Covid19",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Leading article: What can we do to improve individual and team situational awareness to benefit patient safety? Br J Oral Maxillofac Surg 2020",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Brennan",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Holden",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Shaw",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Morris",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Oeppen",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "30034--30040",
            "other_ids": {
                "DOI": [
                    "10.1016/j.bjoms.2020.01.030"
                ]
            }
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Impact of hydration and nutrition on personal performance in the clinical workplace",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Parry",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Oeppen",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Gass",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Brennan",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Br J Oral Maxillofac Surg",
            "volume": "55",
            "issn": "",
            "pages": "995--1003",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Sleep: its importance and the effects of deprivation on surgeons and other healthcare professionals",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "A"
                    ],
                    "last": "Parry",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Oeppen",
                    "suffix": ""
                },
                {
                    "first": "Msa",
                    "middle": [],
                    "last": "Amin",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Brennan",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Br J Oral Maxillofac Surg",
            "volume": "56",
            "issn": "",
            "pages": "663--669",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "The impact and effect of emotional resilience on performance: an overview for surgeons and other healthcare professionals",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Murden",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Bailey",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Mackenzie",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Oeppen",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Brennan",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Br J Oral Maxillofac Surg",
            "volume": "56",
            "issn": "",
            "pages": "786--90",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Challenging hierarchy in healthcare teams -ways to flatten gradients to improve teamwork and patient care",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Green",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Oepen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "W"
                    ],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Brennan",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "British Journal of Oral and Maxillofacial Surgery",
            "volume": "55",
            "issn": "",
            "pages": "449--53",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "Conference centres are being converted into temporary hospitals to treat the less critical Covid-19 cases: Excel Centre, London (Nightingale Hospital) and with further plans for Manchester Central Conference Centre, Birmingham and Glasgow's SEC Centre. https://doi.org/10.1016/j.bjoms.2020.04.002 0266-4356/\u00a9 2020 The British Association of Oral and Maxillofacial Surgeons. Published by Elsevier Ltd. All rights reserved.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}