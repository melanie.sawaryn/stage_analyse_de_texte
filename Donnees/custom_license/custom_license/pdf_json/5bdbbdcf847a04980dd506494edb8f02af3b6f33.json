{
    "paper_id": "5bdbbdcf847a04980dd506494edb8f02af3b6f33",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "Antonio",
                "middle": [],
                "last": "Pisano",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Cardiac Anesthesia and Intensive Care Unit",
                    "institution": "Monaldi Hospital",
                    "location": {
                        "settlement": "Naples",
                        "country": "Italy"
                    }
                },
                "email": ""
            },
            {
                "first": "Giovanni",
                "middle": [],
                "last": "Landoni",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Vita-Salute San Raffaele University",
                    "location": {
                        "settlement": "Milan",
                        "country": "Italy"
                    }
                },
                "email": ""
            },
            {
                "first": "Alberto",
                "middle": [],
                "last": "Zangrillo",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Vita-Salute San Raffaele University",
                    "location": {
                        "settlement": "Milan",
                        "country": "Italy"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Protecting High-Risk Cardiac Patients During the COVID-19 Outbreak",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "To the Editor:",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "In the effort to face the ongoing Coronavirus Disease 2019 (COVID-19) epidemic, which caused severe pneumonia requiring intensive care unit (ICU) admission in up to 15% of confirmed cases so far, many hospitals in Italy are setting up new ICUs, stopping nonurgent admissions, limiting the access to emergency rooms and wards, and providing separate pathways for suspected COVID-19 and other diseases. In parallel, it is mandatory to continue ensuring the provision of nonpostponable treatments (eg, primary percutaneous coronary interventions or urgent/emergency cardiac surgical procedures). The particularly high mortality rates recorded in Italy among COVID-19 patients (apparently more than 9% at the time of writing) 1 suggest that the actual number of people infected with severe acute respiratory syndrome coronavirus-2 (SARS-CoV-2) may be much higher than that of confirmed cases, with a substantial number of asymptomatic or minimally/mildly symptomatic infections. 2 Indeed, a high percentage of asymptomatic infections (likely contributing to rapid dissemination of the contagion) recently was confirmed in a retrospective investigation in China 3 and among the population of one of the first outbreak villages in Italy (data not yet published). Moreover, it has been suggested that person-toperson transmission can occur from individuals with an asymptomatic course and in the prodromal phase of disease, 4,5 or even after recovery. 6 Accordingly, every patient admitted to the hospital with urgency/emergency criteria (eg, acute myocardial infarction, cardiogenic shock, aortic dissection) potentially might be infected and, once transferred to either a coronary unit or ICU, may disseminate the contagion among patients already admitted to these units and among health care personnel working therein, who in turn may become (or already be) subclinically infected, and further contribute to the spread of infection among patients with a very high risk of a fatal outcome from SARS-CoV-2 (eg, cardiac transplantation recipients, patients with mechanical circulatory support, patients with major complications after cardiovascular surgery). If, as suggested, health care providers should be protected from subclinical SARS-CoV-2 infection, 4 high-risk patients also should be protected from new patients admitted to ICUs (who may have become infected during their previous social contacts or during admission to emergency departments in the same or other hospitals) and from asymptomatic or minimally/mildly symptomatic health care providers. During the ongoing health emergency, all new patients admitted to hospital units hosting immunocompromised; complex; critical; and, more generally, acutely ill patients probably should be isolated initially and screened for SARS-CoV-2 infection, and separate pathways should be provided until the virological test results are obtained. Moreover, because routine use of high-level personal protective equipment outside the management of suspected cases in the emergency departments and of confirmed cases within COVID-19\u00c0dedicated units probably is not feasible, health care providers working in other (acute care) units should be turned away and screened immediately in the presence of minimal symptoms of respiratory infection, if not routinely screened regardless of the presence of symptoms.",
            "cite_spans": [
                {
                    "start": 975,
                    "end": 976,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1445,
                    "end": 1446,
                    "text": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 2251,
                    "end": 2252,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Letter to the Editor"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Available at: www.protezionecivile.gov.it",
            "authors": [
                {
                    "first": "Protezione",
                    "middle": [],
                    "last": "Civile",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Covid-19 -navigating the uncharted",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "S"
                    ],
                    "last": "Fauci",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "C"
                    ],
                    "last": "Lane",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "R"
                    ],
                    "last": "Redfield",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "328",
            "issn": "",
            "pages": "1268--1277",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Substantial undocumented infection facilitates the rapid dissemination of novel coronavirus (SARS-CoV2)",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Pei",
                    "middle": [
                        "S"
                    ],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Science",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1126/science.abb3221"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Protecting health-care workers from subclinical coronavirus infection",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Rebaza",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet Respir Med",
            "volume": "8",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "A familial cluster of pneumonia associated with the 2019 novel coronavirus indicating person-to-person transmission: A study of a family cluster",
            "authors": [
                {
                    "first": "Jf-W",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Yuan",
                    "suffix": ""
                },
                {
                    "first": "K-H",
                    "middle": [],
                    "last": "Kok",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "514--537",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Positive RT-PCR test results in patients recovered from COVID-19",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Lan",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Ye",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.2783"
                ]
            }
        }
    },
    "ref_entries": {},
    "back_matter": []
}