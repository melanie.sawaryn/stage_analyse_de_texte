{
    "paper_id": "9e8479f3f693499f502384fd441ea492768775c6",
    "metadata": {
        "title": "Real-Time Reverse Transcription Polymerase Chain Reaction for Rapid Detection of Transmissible Gastroenteritis Virus",
        "authors": [
            {
                "first": "Ramesh",
                "middle": [],
                "last": "Vemulapalli",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Transmissible gastroenteritis (TGE) is a highly contagious disease of pigs caused by the TGE virus (TGEV). Rapid detection of the virus in the affected pigs' feces is critical for controlling the disease outbreaks. The real-time RT-PCR assay described in this chapter can quickly detect the presence of TGEV in fecal samples with high sensitivity and specifi city.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Transmissible gastroenteritis (TGE) is a highly contagious, acute viral disease of pigs [ 1 ] . TGE can affect pigs of all ages, but the disease severity and mortality rate are high in piglets under 2 weeks of age. The causative agent is TGE virus (TGEV), a coronavirus that primarily infects and replicates in the epithelial cells of pig intestines. Affected animals shed the virus in their feces . The disease transmission occurs primarily via fecal-oral route [ 2 ] . During an outbreak, rapid detection of TGEV in feces is very useful for implementing the disease management practices in a timely manner. Any TGEV-specifi c diagnostic assay must be able to differentiate it from porcine respiratory coronavirus (PRCV), a natural mutant of TGEV with truncated spike protein and altered cell tropism towards respiratory epithelial cells [ 3 ] . PRCV mostly causes mild or subclinical respiratory disease. However, in some PRCV-infected pigs, the virus can be shed in the feces [ 4 ] . Realtime PCR -based assays are well suited for rapid, specifi c, and sensitive detection of viruses such as TGEV. The real-time RT-PCR assay described here is based on amplifi cation of a conserved region of the spike protein gene of TGEV strains and detection of the amplifi ed products using a TaqMan probe [ 5 ] . The assay, along with the RNA extraction method described here, can be established in any molecular diagnostic laboratory for detection of TGEV in pig fecal samples [ 6 -8 ] .",
            "cite_spans": [
                {
                    "start": 88,
                    "end": 93,
                    "text": "[ 1 ]",
                    "ref_id": null
                },
                {
                    "start": 463,
                    "end": 468,
                    "text": "[ 2 ]",
                    "ref_id": null
                },
                {
                    "start": 839,
                    "end": 844,
                    "text": "[ 3 ]",
                    "ref_id": null
                },
                {
                    "start": 979,
                    "end": 984,
                    "text": "[ 4 ]",
                    "ref_id": null
                },
                {
                    "start": 1296,
                    "end": 1301,
                    "text": "[ 5 ]",
                    "ref_id": null
                },
                {
                    "start": 1469,
                    "end": 1477,
                    "text": "[ 6 -8 ]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Preventing contamination of samples and reagents with nucleic acids and nucleases is critical to obtaining accurate and reproducible results with any PCR -based diagnostic assay. It is recommended that the nucleic acid extraction, preparation of master mix, and real-time PCR amplifi cation are performed in three physically separated areas. Each of these areas should have separate set of laboratory instruments and supplies that are to be used only in their",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "assigned location [ 9 ] . Including a positive extraction control and a negative extraction control along with each batch of clinical samples is recommended to monitor the effi ciency of RNA extraction and potential cross contamination of the samples [ 9 ] .",
            "cite_spans": [
                {
                    "start": 18,
                    "end": 23,
                    "text": "[ 9 ]",
                    "ref_id": null
                },
                {
                    "start": 251,
                    "end": 256,
                    "text": "[ 9 ]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "PCR inhibitors are often co-extracted with nucleic acids from fecal samples. In our experience, RNA extracted from pig fecal samples using the following method is suitable for TGEV detection using the real-time RT-PCR assay. Other extraction methods that produce PCR inhibitor-free RNA can also be used ( see Note 1 ). The presence of PCR inhibitors in the extracted can be monitored by using an internal control ( see Note 2 ).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "1. Prepare a 20 % (w/v) suspension of feces in DNase/RNasefree distilled water. Feces in liquid form can be used directly.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "2. Transfer 250 \u03bcl of the fecal suspension into a 1.5 ml microcentrifuge tube.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "3. Add 750 \u03bcl of TRIzol LS reagent, briefl y vortex the tube for 10 s, and incubate at room temperature for 5 min.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "4. Add 200 \u03bcl of chloroform to the tube. Vortex the tube for 5 s and incubate at room temperature for 3 min.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "5. Centrifuge the tube at 12,000 \u00d7 g for 10 min. 11. Repeat step 10 for a second wash of the spin column with RPE buffer.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "12. Place the spin column into a new collection tube and centrifuge at 10,000 \u00d7 g for 1 min to dry the membrane of the column.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "13. Place the spin column in a 1.5 ml microcentrifuge tube and add 30 \u03bcl of DNase/RNase-free water to the column. Incubate at room temperature for 1 min.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "14. Centrifuge at 10,000 \u00d7 g for 1 min.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        },
        {
            "text": "15. Discard the spin column and store the eluted RNA at 4 \u00b0C if it is used in the real-time RT-PCR assay within 12 h or at \u221220 \u00b0C if it is used after 12 h ( see Note 4 ).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "RNA Extraction"
        },
        {
            "text": "1. Turn on the real-time PCR machine. Follow the software directions of the machine manufacturer to confi rm that the FAM signal data will be gathered during the amplifi cation. Program the thermal cycle and data collection conditions according to Table 1 . 2. Prepare a master mix suffi cient for the intended number of samples in a sterile 1.5 ml microcentrifuge tube according to Table 2 .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 248,
                    "end": 257,
                    "text": "Table 1 .",
                    "ref_id": null
                },
                {
                    "start": 383,
                    "end": 390,
                    "text": "Table 2",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "RNA Extraction"
        },
        {
            "text": "Add at least a no-template control (NTC) and a positive amplifi cation control (PAC) to the number required reactions.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "RNA Extraction"
        },
        {
            "text": "3. Close the cap of the microcentrifuge tube. Vortex and centrifuge the tube briefl y.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "RNA Extraction"
        },
        {
            "text": "4. Aliquot 20 \u03bcl of the master mix into each PCR reaction tube. Table 1 Thermal cycling conditions",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 64,
                    "end": 71,
                    "text": "Table 1",
                    "ref_id": null
                }
            ],
            "section": "RNA Extraction"
        },
        {
            "text": "Step Temperature (\u00b0C) Time ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Real-Time RT-PCR"
        }
    ],
    "bib_entries": {
        "BIBREF4": {
            "ref_id": "b4",
            "title": "DNase/RNase-free distilled water",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Feces samples from suspected pigs",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "RNasin Ribonuclease Inhibitor (Promega) or equivalent",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "mM MgCl 2 solution for PCR (Sigma-Aldrich) or equivalent",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "DNase/RNase-free distilled water",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "\u03bcM Forward primer",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "CCA(BHQ1)-3\u2032(FAM, 6-carboxyfl uorescein; BHQ1, black hole quencher 1; Eurofi ns MWG Operon)",
            "authors": [],
            "year": null,
            "venue": "\u03bcM Probe 5\u2032-(FAM)YAAGGGCTCACCACCTACTACCA",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Real-time PCR machine, such as Smart Cycler II (Cepheid), 7300 Real-Time PCR System (Applied Biosystems), or equivalent",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "PCR reaction tubes suitable for the real-time PCR machine platform",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Transmissible gastroenteritis",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Garwes",
                    "suffix": ""
                }
            ],
            "year": 1988,
            "venue": "Vet Rec",
            "volume": "122",
            "issn": "",
            "pages": "462--463",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Immunity to transmissible gastroenteritis virus and porcine respiratory coronavirus infections in swine",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "J"
                    ],
                    "last": "Saif",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "L"
                    ],
                    "last": "Van Cott",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "A"
                    ],
                    "last": "Brim",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Vet Immunol Immunopathol",
            "volume": "43",
            "issn": "",
            "pages": "89--97",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "Porcine respiratory coronavirus differs from transmissible gastroenteritis virus by a few genomic deletions",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Rasschaert",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Duarte",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Laude",
                    "suffix": ""
                }
            ],
            "year": 1990,
            "venue": "J Gen Virol",
            "volume": "71",
            "issn": "",
            "pages": "2599--2607",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Respiratory and fecal shedding of porcine respiratory coronavirus (PRCV) in sentinel weaned pigs and sequence of the partial S-gene of the PRCV isolates",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Costantini",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lewis",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Alsop",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Templeton",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Arch Virol",
            "volume": "149",
            "issn": "",
            "pages": "957--974",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "A real-time TaqMan \u00ae RT-PCR assay with an internal amplifi cation control for rapid detection of transmissible gastroenteritis virus in swine fecal samples",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Vemulapalli",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Gulani",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Santrich",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "J Virol Methods",
            "volume": "162",
            "issn": "",
            "pages": "231--235",
            "other_ids": {}
        },
        "BIBREF19": {
            "ref_id": "b19",
            "title": "Attempted experimental reproduction of porcine periweaning-failure-to-thrive syndrome using tissue homogenates",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Jcs",
                    "middle": [],
                    "last": "Harding",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "PLoS One",
            "volume": "9",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF20": {
            "ref_id": "b20",
            "title": "The fi rst case of porcine epidemic diarrhea in Canada",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ojkic",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Hazlett",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Fairles",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Marom",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Can Vet J",
            "volume": "56",
            "issn": "",
            "pages": "149--152",
            "other_ids": {}
        },
        "BIBREF21": {
            "ref_id": "b21",
            "title": "A novel watery diarrhea caused by the co-infection of neonatal piglets with Clostridium perfringens type A and Escherichia coli (K88, 987P)",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Ren",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Nie",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Cheng",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Vet J",
            "volume": "197",
            "issn": "",
            "pages": "812--816",
            "other_ids": {}
        },
        "BIBREF22": {
            "ref_id": "b22",
            "title": "Quality assurance and quality control in the routine molecular diagnostic laboratory for infectious diseases",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "H"
                    ],
                    "last": "Kessler",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "B"
                    ],
                    "last": "Raggam",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Clin Chem Lab Med",
            "volume": "50",
            "issn": "",
            "pages": "1153--1159",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Transfer 600 \u03bcl of the top aqueous phase to a new 1.5 ml microcentrifuge tube containing 600 \u03bcl of 70 % ethanol and mix by inverting 4-5 times. 7. Transfer 700 \u03bcl of the mix to an RNeasy spin column and centrifuge at 8000 \u00d7 g for 30 s. 8. Transfer the spin column to a new collection tube, and add the remaining mix from step 6 . Centrifuge at 8000 \u00d7 g for 30 s. 9. Place the spin column into a new collection tube. Add 700 \u03bcl of RW1 buffer to the spin column and centrifuge at 8000 \u00d7 g for 30 s. 10. Place the spin column into a new collection tube. Add 500 \u03bcl of RPE buffer to the spin column and centrifuge at 8000 \u00d7 g for 30 s ( see Note 3 ).",
            "latex": null,
            "type": "figure"
        },
        "TABREF1": {
            "text": "\u03bcl of the extracted sample RNA. Add 5 \u03bcl of DNase/ RNase-free water to the NTC tube. Add 5 \u03bcl of RNA extracted from TGEV to the PAC tube. 6. Centrifuge the PCR tubes briefl y. 7. Insert the PCR tubes into the real-time PCR machine and start the thermal cycling program. 8. At the completion of the amplifi cation program, examine the amplifi cation curves and the threshold cycles (Ct) of the reactions. The NTC reaction and any other negative control reactions should not generate a Ct value. The PAC reaction and any other expected positive reactions should generate a Ct value as expected based on the template RNA concentration. Notes 1. Magnetic bead-based manual or high-throughput RNA extraction methods (e.g., MagMax Viral RNA Isolation Kit, Life Technologies) are used in our laboratory to extract TGEV and other viral RNA from pig fecal samples. 2. Xeno RNA (Life Technologies) can be used as internal RNA control to monitor the presence of PCR inhibitors and efficiency of RNA extraction. 3. Buffer RPE of the RNeasy Kit is supplied as a concentrate. Follow the manufacturer's direction to prepare the buffer by adding the appropriate volume of 100 % ethanol. 4. For long-term storage, it is recommended that the extracted viral RNA be kept at \u221280 \u00b0C.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}