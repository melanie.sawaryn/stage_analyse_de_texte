{
    "paper_id": "b86e52c58a992f4abb77f72b68c5a822383565a0",
    "metadata": {
        "title": "Commentary Limiting spread of COVID-19 from cruise ships -lessons to be learnt from Japan",
        "authors": [
            {
                "first": "Toyoaki",
                "middle": [],
                "last": "Sawano",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Sendai City Medical Center",
                    "location": {
                        "settlement": "Sendai",
                        "region": "Miyagi",
                        "country": "Japan"
                    }
                },
                "email": "toyoakisawano@gmail.com"
            },
            {
                "first": "Akihiko",
                "middle": [],
                "last": "Ozaki",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Fukushima Medical University School of Medicine",
                    "location": {
                        "settlement": "Fukushima",
                        "region": "Fukushima",
                        "country": "Japan"
                    }
                },
                "email": "ozakiakihiko@gmail.com"
            },
            {
                "first": "Alfonso",
                "middle": [
                    "J"
                ],
                "last": "Rodriguez-Morales",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Public Health and Infection Research Group",
                    "institution": "Jyoban Hospital of Tokiwa Foundation",
                    "location": {
                        "settlement": "Iwaki, Shinagawa, Tokyo",
                        "region": "Fukushima",
                        "country": "Japan, Japan"
                    }
                },
                "email": ""
            },
            {
                "first": "Tetsuya",
                "middle": [],
                "last": "Tanimoto",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Universidad Tecnologica de Pereira",
                    "location": {
                        "settlement": "Pereira, Shinagawa, Tokyo",
                        "region": "Risaralda",
                        "country": "Colombia., Japan"
                    }
                },
                "email": "tetanimot@yahoo.co.jp"
            },
            {
                "first": "Ranjit",
                "middle": [],
                "last": "Sah",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Public Health Laboratory",
                    "location": {
                        "postCode": "44600",
                        "settlement": "Kathmandu",
                        "country": "Nepal"
                    }
                },
                "email": "ranjitsah@iom.edu.np"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Japan's response to the novel coronavirus (COVID-19) infection has been problematic since the outbreak was first reported in China. A typical example was the case of a cruise ship \"Diamond Princess,\" anchored in Yokohama, Japan. As of March, 6, the WHO officially reported that there were 17,481 laboratory-confirmed cases of Covid-19 infection globally outside China. In Japan, there were 1,045 such cases, but there were 696 laboratory-confirmed cases on the Diamond Princess. 1 The Japanese government have quarantined the ship's 3,711 passengers and crew members since February 5, 2020 until February 19, under the Japan's Quarantine Act enacted in 1951, effectively prohibiting the disembarkation of anyone onto Japanese soil. We have three primary concerns about this quarantine action.",
            "cite_spans": [
                {
                    "start": 479,
                    "end": 480,
                    "text": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Spread of COVID-19 infection on a Cruise Ship in Yokohama, Japan"
        },
        {
            "text": "First, the quarantine in the cruise ship may not be effective enough to prevent the contagion of the virus in Japan. Although the disease has a relatively low case fatality rate (currently 0.3% in China), each COVID-19 infected individual is reported to infect 2.2 people, similarly to the influenza virus, a basic reproductive number of which is 1.4-4.0. 2 Given the fact that Japan has received hundreds of thousands of Chinese tourists since the start of the outbreak in early-December, several of whom may have been from the Wuhan region, it is obvious that COVID-19 is already in Japan, as evidenced by the 13 locally-transmitted individuals, including a taxi driver and bus driver who had transported Chinese tourists at the time when the quarantine was initiated.",
            "cite_spans": [
                {
                    "start": 356,
                    "end": 357,
                    "text": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Spread of COVID-19 infection on a Cruise Ship in Yokohama, Japan"
        },
        {
            "text": "Second, quarantine in the cruise ship could accelerate a contagion of the virus in the ship since it has been reported that cruise ships can become incubators of the viruses because a large number of people are packed in the semi-enclosed space, with limited sanitation as we as restricted water and food supply. 3 Indeed, the infection on the cruise ship appears not to have been well-controlled, and the number of infected people has been steadily rising, with 137 new confirmed cases on February 16, according to WHO's daily update.",
            "cite_spans": [
                {
                    "start": 313,
                    "end": 314,
                    "text": "3",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Spread of COVID-19 infection on a Cruise Ship in Yokohama, Japan"
        },
        {
            "text": "Third, care for the passengers and crew members has been poor. The majority of the passenger of the ship are the elderly, with more than 200 people over the age of 80. As Japan has learned from past emergencies, the elderly, especially those with existing with comorbidities, are at grave risk not just from the emerging Covid-19 infection, but from the physical and psychological stress that may make their existing ailments worse, actually jeopardizing their health and well being. With increased international criticism of the poor management of the those on board, the Japanese government allowed some elderly passengers to leave the ship on February 14, rather than wait until the quarantine ends on February 19. As of February 23, 36 severe cases of the COVID-19 have been reported among the passengers, requiring treatment in land-based intensive care units. After this quarantine on the cruise ship, hundreds of others on board had been repatriated to their home countries, where they will start another 14 days of quarantine.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Spread of COVID-19 infection on a Cruise Ship in Yokohama, Japan"
        },
        {
            "text": "There is growing global concern about how illegal immigrants and individuals such as the potentially infected on the \"Diamond Princess,\" are being treated. We believe that the response of the authorities to the cruise ship dilemma has not been patient-oriented with respect to those on board. While it is important to respect the concept of quarantining people who could pose a significant danger to the nation's entire population, which is the government's overriding responsibility, alternative solutions may be possible. The reported development of a 15-minute blood-based Covid-1-test kit by Chinese scientists may help in future, if it is accurate and does not produce repeated false-negatives as the existing test does. Similarly, creation of a purposebuilt mass quarantine facility somewhere in Japan would be useful. We hope learned from this experience in Japan would be help improve the situation on a cruise ship \"Grand Princess\" off the coast of California, where 21 Covid-19 cases were confirmed on March 5.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Spread of COVID-19 infection on a Cruise Ship in Yokohama, Japan"
        },
        {
            "text": "We declare that we have no competing interests.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Spread of COVID-19 infection on a Cruise Ship in Yokohama, Japan"
        },
        {
            "text": "Toyoaki Sawano, MD Department of Surgery, Sendai City Medical Center, Sendai, Miyagi, Japan toyoakisawano@gmail.com",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Spread of COVID-19 infection on a Cruise Ship in Yokohama, Japan"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "WHO (2020) Coronavirus disease 2019 (COVID-19) Situation Report -27",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Early Transmission Dynamics in Wuhan, China, of Novel Coronavirus-Infected Pneumonia",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Tong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}