{
    "paper_id": "973176b2271e62022702244fe3e0313f8aaa8f7c",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "Joseph",
                "middle": [],
                "last": "Campbell",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Reuters",
                "middle": [],
                "last": "Comment",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The current response to the coronavirus disease 2019 (COVID-19) pandemic involves aggressive implementation of suppression strategies, such as case identification, quarantine and isolation, contact tracing, and social distancing. However, models developed by the Imperial College COVID-19 Response Team suggest that \"transmission will quickly rebound if interventions are relaxed\". 1 WHO warns of multiple simultaneous outbreaks of COVID-19 worldwide. 2 The development of COVID-19 vaccines that can be used globally is therefore a priority for ending the pandemic. This vaccine effort should be guided by three imperatives: speed, manufacture and deployment at scale, and global access. In February, 2020, the World Bank and the Coalition for Epidemic Preparedness Innovations (CEPI), which funds development of epidemic vaccines, co-hosted a global consultation on these goals. 3 This consultation led to the launch of a COVID-19 Vaccine Development Taskforce that is now working on how to finance and manufacture vaccines for global access.",
            "cite_spans": [
                {
                    "start": 452,
                    "end": 453,
                    "text": "2",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 880,
                    "end": 881,
                    "text": "3",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Ensuring global access to COVID-19 vaccines"
        },
        {
            "text": "CEPI estimates that developing up to three vaccines in the next 12-18 months will require an investment of at least US$2 billion. 4 This estimate includes phase 1 clinical trials of eight vaccine candidates, progression of up to six candidates through phase 2 and 3 trials, completion of regulatory and quality requirements for at least three vaccines, and enhancing global manufacturing capacity for three vaccines. This estimate does not include the costs of manufacture or delivery. Progress has been rapid. A phase 1 trial of a vaccine candidate, supported by the US National Institutes of Health and CEPI, began on March 16, 2020, 5 and 2 days later a clinical trial began in China. 6 Clinical trials for other candidates will start soon.",
            "cite_spans": [
                {
                    "start": 130,
                    "end": 131,
                    "text": "4",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 688,
                    "end": 689,
                    "text": "6",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Ensuring global access to COVID-19 vaccines"
        },
        {
            "text": "Use of existing financing systems to support this work offers the benefits of speed and lower transaction costs than for new financing approaches. CEPI is supported by a World Bank financial intermediary fund that brings together public, philanthropic, and private funding to respond to global priorities. 7 Through this fund, CEPI can act as a global mechanism for funding vaccine development until vaccines can be licensed or used under emergency use provisions. Mobilising $2 billion in funding will require funding from all sources. Given the enormous health, social, and economic consequences of COVID-19, there is a strong case for all governments to invest in vaccines.",
            "cite_spans": [
                {
                    "start": 306,
                    "end": 307,
                    "text": "7",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "Ensuring global access to COVID-19 vaccines"
        },
        {
            "text": "In addition to direct government contributions, innovative finance mechanisms have been successful in raising funds for vaccines in the past and should be used to fund the development of COVID-19 vaccines. 8, 9 The International Finance Facility for Immunisation (IFFIm) raises funds with vaccine bonds, which turn long-term contributions by donors into available cash. 8 IFFIm was created to support Gavi, the Vaccine Alliance, but could be used to finance CEPI's COVID-19 vaccine efforts. With advanced market commitments, donors make funding commitments to vaccine manufacturers and, in exchange, companies sign a legally binding commitment to provide the vaccines at a price affordable to low-income and middle-income countries. Gavi's board expressed support for the use of Gavi's IFFIm and advanced market commitments to improve COVID-19 vaccine development and access. 10 The need for COVID-19 vaccines is global, although the need is differentially distributed within populations. Vaccines would likely be prioritised for health-care workers and people at greatest risk of severe illness and death. High-income countries must not monopolise the global supply of COVID-19 vaccines. This risk is real: during the 2009 influenza A/H1N1 pandemic, rich countries negotiated large advance orders for the vaccine, crowding out poor countries. 11 Such an outcome would result in a suboptimal allocation of an initially scarce resource.",
            "cite_spans": [
                {
                    "start": 206,
                    "end": 208,
                    "text": "8,",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 209,
                    "end": 210,
                    "text": "9",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 370,
                    "end": 371,
                    "text": "8",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 876,
                    "end": 878,
                    "text": "10",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1344,
                    "end": 1346,
                    "text": "11",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "Ensuring global access to COVID-19 vaccines"
        },
        {
            "text": "A far better solution would be for governments to ensure there is a globally fair allocation system. With sufficient political will and public sector financing, such a system could be established using existing instruments and institutions. The rudiments of the system would require a global purchasing agent or agents, a substantial but limited-term advanced purchase commitment, and access through the system to financial instruments such as concessional loans or grants and indemnification from liability to offset the risks taken by participating private sector partners.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Ensuring global access to COVID-19 vaccines"
        },
        {
            "text": "Vaccines purchased through the system should be free at the point of care worldwide for prioritised populations, with national allocations determined through a fair and objective process.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Ensuring global access to COVID-19 vaccines"
        },
        {
            "text": "On March 16, 2020, the G7 committed to supporting the launch of joint research projects for COVID-19 treatments and vaccines. 12 High-level dialogue is needed on ways to ensure complementarity of efforts and global access to COVID-19 vaccines. Investments should proceed in tandem to build national systems for delivery of potential vaccines-eg, using domestic financing and external financing from the World Bank Group's $14 billion COVID-19 Fast Track Facility 13 and reallocations from the Global Fund to Fight AIDS, Tuberculosis and Malaria, Gavi, and Global Financing Facility grants for service delivery.",
            "cite_spans": [
                {
                    "start": 126,
                    "end": 128,
                    "text": "12",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [],
            "section": "Ensuring global access to COVID-19 vaccines"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Coalition for Epidemic Preparedness Innovations",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Impact of non-pharmaceutical interventions (NPIs) to reduce COVID-19 mortality and healthcare demand",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "World's most vulnerable in \"third wave\" for COVID-19 support, experts warn. The Guardian",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Ahmed",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Funding the development and manufacturing of COVID-19 vaccines: background paper for the World Bank/CEPI financing COVID-19 vaccine development consultation on",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Yamey",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Sch\u00e4ferhoff",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Pate",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "The Coalition for Epidemic Preparedness Innovations. $2 billion required to develop a vaccine against the COVID-19 virus",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "National Institutes of Health. NIH clinical trial of investigational vaccine for COVID-19 begins",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "China announces first human trials of COVID-19 vaccine",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "Cansino"
                    ],
                    "last": "Garc\u00eda",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Biologics",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Market Screener",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "World Bank",
            "authors": [],
            "year": 2020,
            "venue": "Financial Intermediary Funds (FIFs). 2020",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Banking for health: opportunities in cooperation between banking and health applying innovation from other sectors",
            "authors": [
                {
                    "first": "I",
                    "middle": [],
                    "last": "Kickbusch",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Krech",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Franz",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Wells",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "BMJ Glob Health",
            "volume": "3",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Ebola vaccine purchasing commitment from Gavi to prepare for future outbreaks",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Gavi",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "The Vaccine Alliance",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Gavi Board calls for bold engagement to respond to COVID-19",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Gavi",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "The Vaccine Alliance",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Negotiating equitable access to influenza vaccines: global health diplomacy and the controversies surrounding avian influenza H5N1 and pandemic influenza H1N1",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "P"
                    ],
                    "last": "Fidler",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "PLoS Med",
            "volume": "7",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "The White House. G7 leaders' statement",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "World Bank Group increases COVID-19 response to $14 billion to help sustain economies, protect jobs",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Comment www.thelancet.com Published online March 31, 2020 https://doi.org/10.1016/S0140-6736(20)30763-7 1",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "GY, KKM, and MS have received research funding from Gavi ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        }
    ]
}