{
    "paper_id": "8b354996f37c408d22b7702bf400edb796358b0e",
    "metadata": {
        "title": "Seroprevalence of Antibody to Severe Acute Respiratory Syndrome (SARS)- Associated Coronavirus among Health Care Workers in SARS and Non-SARS Medical Wards",
        "authors": [
            {
                "first": "Margaret",
                "middle": [],
                "last": "Ip",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Chinese University of Hong Kong",
                    "location": {
                        "settlement": "Shatin, Hong Kong",
                        "country": "People's Republic of China"
                    }
                },
                "email": "margaretip@cuhk.edu.hk."
            },
            {
                "first": "Paul",
                "middle": [
                    "K S"
                ],
                "last": "Chan",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Chinese University of Hong Kong",
                    "location": {
                        "settlement": "Shatin, Hong Kong",
                        "country": "People's Republic of China"
                    }
                },
                "email": ""
            },
            {
                "first": "Nelson",
                "middle": [],
                "last": "Lee",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Alan",
                "middle": [],
                "last": "Wu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Tony",
                "middle": [
                    "K C"
                ],
                "last": "Ng",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Louis",
                "middle": [],
                "last": "Chan",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Albert",
                "middle": [],
                "last": "Ng",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "H",
                "middle": [
                    "M"
                ],
                "last": "Kwan",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Lily",
                "middle": [],
                "last": "Tsang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Ida",
                "middle": [],
                "last": "Chu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Jo",
                "middle": [
                    "L K"
                ],
                "last": "Cheung",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Joseph",
                "middle": [
                    "J Y"
                ],
                "last": "Sung",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "John",
                "middle": [
                    "S"
                ],
                "last": "Tam",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Chinese University of Hong Kong",
                    "location": {
                        "settlement": "Shatin, Hong Kong",
                        "country": "People's Republic of China"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "HCWs on these wards were highest among nurses (11.6%) and health care assistants (11.8%), indicating that these occupations are associated with the highest risks for exposure. Severe acute respiratory syndrome (SARS) was first recognized in late February 2003 in Hanoi, Vietnam [1]. Shortly thereafter, an outbreak of SARS occurred in Hong Kong, People's Republic of China, and 22% of the patients were health care workers (HCWs) in Hong Kong hospitals [2]. At Prince of Wales Hospital, a 1350-bed teaching hospital in Hong Kong, the first wave of SARS-associated coronavirus (SARS-CoV) infection affected close to 80% of the staff on the medical ward. Within the first 2 weeks of this outbreak, which began on 10 March 2003, 69 HCWs were admitted for probable or suspected SARS [3]. Isolation wards for probable and suspected SARS cases were established, and infection-control measures involving droplet precautions was implemented [4]. In the next few weeks, 444 patients with probable SARS were admitted to these wards. The mean number of patients with SARS at any one time was 86.6 (range, 41-168 patients; interquartile range, 59.0-98.8 patients), occupying a mean of 4.4 SARS wards (range, 3-8 wards) each day during the 10-week period between 10 March and 20 May 2003. During this time, at least 259 HCWs were documented to have worked in the SARS wards at Prince of Wales Hospital and were directly or indirectly exposed to patients with SARS and their body fluids. Infection-control measures during this time essentially involved contact and droplet precautions [5] and use of N95 particulate respirators (3M).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "In a previous study, we revealed that subclinical or mild infection with SARS-CoV was rare among HCWs in the hospital and that none of the 674 HCWs examined had seroconversion to SARS-CoV antibody [6] . The objective of this study was to examine the specific groups of HCWs who were involved in the care of patients with SARS on the medical wards, with documentation of their period of exposure on the SARS and non-SARS medical wards. We examined the seroprevalence of anti-SARS-CoV IgG antibody in 2 cohorts of healthy HCWs, 1 of which was comprised of HCWs who worked in the SARS wards, with the other comprising HCWs from non-SARS medical wards. In addition, the attack rates of clinical SARS-CoV in HCWs from these wards who required hospitalization were also identified.",
            "cite_spans": [
                {
                    "start": 197,
                    "end": 200,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "Subjects and methods. Healthy HCWs were recruited on a voluntary basis, and the 2 cohorts were identified via a questionnaire. The names of volunteers were counterchecked against a predetermined staff list of 742 HCWs identified as having worked on these wards. The questionnaire included the nature of the HCW's job, the length of time they had worked on the SARS wards, signs and symptoms of any illness suggestive of SARS during this time, and the practice of infectioncontrol measures. The nature of the HCW's job was categorized into 1 of the following 5 groups: doctors, nurses, allied health professionals (including physiotherapists, occupational therapists, radiographers, and phlebotomists), health care/general service assistants, and ancillary staff. The health care/general service assistants were directly involved with the daily needs of patients, including feeding, washing, and dressing, whereas ancillary staff did not have direct contact with patients, in general.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "Blood samples were obtained at the end of the 10-week outbreak period (i.e., after 21 May 2003) for antibody detection. Anti-SARS-CoV IgG antibody was detected by means of an indirect immunofluorescence assay using Vero cells infected with a strain of CoV (GenBank accession no. AY278554) iso-",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "HCWs on these wards were highest among nurses (11.6%) and health care assistants (11.8%), indicating that these occupations are associated with the highest risks for exposure.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The seroprevalence of antibody to severe acute respiratory syndrome (SARS)-associated coronavirus (SARS-CoV) in cohorts of health care workers (HCWs) with subclinical infection in SARS and non-SARS medical wards was 2.3% (3 of 131 HCWs) and 0% (0 of 192 HCWs), respectively. Rates for clinical SARS-CoV infection among 742"
        },
        {
            "text": "Severe acute respiratory syndrome (SARS) was first recognized in late February 2003 in Hanoi, Vietnam [1] . Shortly thereafter, an outbreak of SARS occurred in Hong Kong, People's Republic of China, and 22% of the patients were health care workers (HCWs) in Hong Kong hospitals [2] . At Prince of Wales Hospital, a 1350-bed teaching hospital in Hong Kong, the first wave of SARS-associated coronavirus (SARS-CoV) infection affected close to 80% of the staff on the medical ward. Within the first 2 weeks of this outbreak, which began on 10 March 2003, 69 HCWs were admitted for probable or suspected SARS [3] . Isolation wards for probable and suspected SARS cases were established, and infection-control measures involving droplet precautions was implemented [4] .",
            "cite_spans": [
                {
                    "start": 102,
                    "end": 105,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 278,
                    "end": 281,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 605,
                    "end": 608,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 760,
                    "end": 763,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "The seroprevalence of antibody to severe acute respiratory syndrome (SARS)-associated coronavirus (SARS-CoV) in cohorts of health care workers (HCWs) with subclinical infection in SARS and non-SARS medical wards was 2.3% (3 of 131 HCWs) and 0% (0 of 192 HCWs), respectively. Rates for clinical SARS-CoV infection among 742"
        },
        {
            "text": "In the next few weeks, 444 patients with probable SARS were admitted to these wards. In a previous study, we revealed that subclinical or mild infection with SARS-CoV was rare among HCWs in the hospital and that none of the 674 HCWs examined had seroconversion to SARS-CoV antibody [6] . The objective of this study was to examine the specific groups of HCWs who were involved in the care of patients with SARS on the medical wards, with documentation of their period of exposure on the SARS and non-SARS medical wards. We examined the seroprevalence of anti-SARS-CoV IgG antibody in 2 cohorts of healthy HCWs, 1 of which was comprised of HCWs who worked in the SARS wards, with the other comprising HCWs from non-SARS medical wards. In addition, the attack rates of clinical SARS-CoV in HCWs from these wards who required hospitalization were also identified.",
            "cite_spans": [
                {
                    "start": 282,
                    "end": 285,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "The seroprevalence of antibody to severe acute respiratory syndrome (SARS)-associated coronavirus (SARS-CoV) in cohorts of health care workers (HCWs) with subclinical infection in SARS and non-SARS medical wards was 2.3% (3 of 131 HCWs) and 0% (0 of 192 HCWs), respectively. Rates for clinical SARS-CoV infection among 742"
        },
        {
            "text": "Subjects and methods. Healthy HCWs were recruited on a voluntary basis, and the 2 cohorts were identified via a questionnaire. The names of volunteers were counterchecked against a predetermined staff list of 742 HCWs identified as having worked on these wards. The questionnaire included the nature of the HCW's job, the length of time they had worked on the SARS wards, signs and symptoms of any illness suggestive of SARS during this time, and the practice of infectioncontrol measures. The nature of the HCW's job was categorized into 1 of the following 5 groups: doctors, nurses, allied health professionals (including physiotherapists, occupational therapists, radiographers, and phlebotomists), health care/general service assistants, and ancillary staff. The health care/general service assistants were directly involved with the daily needs of patients, including feeding, washing, and dressing, whereas ancillary staff did not have direct contact with patients, in general.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The seroprevalence of antibody to severe acute respiratory syndrome (SARS)-associated coronavirus (SARS-CoV) in cohorts of health care workers (HCWs) with subclinical infection in SARS and non-SARS medical wards was 2.3% (3 of 131 HCWs) and 0% (0 of 192 HCWs), respectively. Rates for clinical SARS-CoV infection among 742"
        },
        {
            "text": "Blood samples were obtained at the end of the 10-week outbreak period (i.e., after 21 May 2003) for antibody detection. Anti-SARS-CoV IgG antibody was detected by means of an indirect immunofluorescence assay using Vero cells infected with a strain of CoV (GenBank accession no. AY278554) iso- lated from a patient with SARS. This assay has been used to confirm the diagnosis of SARS in patients with antibody titers of 80-5120 twenty-one days after the onset of symptoms [7] . Screening tests were performed as described elsewhere [6] . Serum specimens were tested at a dilution of 1:40, and, if they were positive at titers \u044340, testing was repeated and specimens were titrated to the end point using 2-fold serial dilutions. For patients with a titer of \u044340, a second serum sample and all previously obtained serum samples were also examined for SARS-CoV IgG. All HCWs with SARS had titers of \u0443160 in this study.",
            "cite_spans": [
                {
                    "start": 472,
                    "end": 475,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 532,
                    "end": 535,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "The seroprevalence of antibody to severe acute respiratory syndrome (SARS)-associated coronavirus (SARS-CoV) in cohorts of health care workers (HCWs) with subclinical infection in SARS and non-SARS medical wards was 2.3% (3 of 131 HCWs) and 0% (0 of 192 HCWs), respectively. Rates for clinical SARS-CoV infection among 742"
        },
        {
            "text": "Overall, 323 (44%) of 742 HCWs from these wards completed the questionnaire and had serum specimens obtained for testing, which included 131 (51%) of 259 HCWs from the SARS wards and 192 (40%) of 483 HCWs from the non-SARS wards. The characteristics of the 2 cohorts are summarized in table 1. Three HCWs (2.3%) who worked on the SARS wards were positive for antibody to the SARS CoV: one had seroconversion from titers of !40 to 160, the second had persistent titers of 160 at 1-month intervals, and the third had titers as high as 640. Stools subsequently obtained from 2 of the 3 HCWs yielded no CoV on culture, but RT-PCR for detection of SARS-CoV was not performed.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results."
        },
        {
            "text": "In the cohort of 192 HCWs from the non-SARS general medical wards, none tested positive for anti-SARS-CoV IgG antibody. The mean days of exposure for the staff working on the SARS wards was 45 days (median, 50 days; range, 1-70 days). Many HCWs from both groups experienced mild symptoms that mimicked those of SARS [3] during the period of study (table 1) , but the percentages of HCWs with such symptoms were similar in the 2 groups ( , by x 2 analysis). For P 1 .05 the 3 HCWs who developed antibodies to SARS-CoV, 1 was a registered nurse and the other 2 were health care/general service assistants. One HCW experienced fever and chills for 1 day, which resolved with symptomatic relief after receipt of an antipyretic, and did not require hospitalization. The other 2 only complained of a headache and a sore throat at some stage during this period.",
            "cite_spans": [
                {
                    "start": 316,
                    "end": 319,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [
                {
                    "start": 347,
                    "end": 356,
                    "text": "(table 1)",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Results."
        },
        {
            "text": "The attack rates for SARS infection among HCWs from the medical wards are listed in table 2. The highest rates were again in nurses and health care/general service assistants, indicating that the risks of exposure to SARS are greatest in these occupations. These HCWs were likely to spend most of the time on the wards and to be in direct contact with patients for prolonged periods. The 3 HCWs with IgG antibodies to SARS-CoV claimed to have complied with infection-control precau- [2] . Twenty-two percent of the cases involved HCWs, and 8 died of SARS [1, 2] . Among all HCWs who were infected with SARS-CoV in Hong Kong, nurses accounted for 55% of the cases, followed by 27% for support staff and 15% for doctors [10] . In an infection-control program for the prevention of SARS, health care occupations associated with the highest risk for SARS must be emphasized-perhaps through educational activities or investigation of the procedures commonly performed by HCWs in such occupations-to modify practice such that the risk of exposure is minimized. In addition, guidelines and isolation precautions must be adopted and complied with [5] to control SARS. Preliminary data suggest that only a few HCWs will develop subclinical or mild infections due to SARS-CoV. Whether this is an infective dose-dependent phenomenon or a factor of the host remains to be determined.",
            "cite_spans": [
                {
                    "start": 483,
                    "end": 486,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 555,
                    "end": 558,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 559,
                    "end": 561,
                    "text": "2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 718,
                    "end": 722,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1139,
                    "end": 1142,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Results."
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Update: outbreak of acute respiratory syndrome-worldwide",
            "authors": [],
            "year": 2003,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "52",
            "issn": "",
            "pages": "241--249",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "World Health Organization. Summary of probable SARS cases (revised 26",
            "authors": [],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "A major outbreak of severe acute respiratory syndrome in Hong Kong",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Hui",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1986--94",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Chan-Yeung",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "H"
                    ],
                    "last": "Seto",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "J"
                    ],
                    "last": "Sung",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "BMJ",
            "volume": "326",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Effectiveness of precautions against droplets and contact in prevention of nosocomial transmission of severe acute respiratory syndrome (SARS)",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "H"
                    ],
                    "last": "Seto",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Tsang",
                    "suffix": ""
                },
                {
                    "first": "Rwh",
                    "middle": [],
                    "last": "Yung",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1519--1539",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Severe acute respiratory syndromeassociated coronavirus infection",
            "authors": [
                {
                    "first": "Pks",
                    "middle": [],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ip",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Ng",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Emerg Infect Dis",
            "volume": "9",
            "issn": "",
            "pages": "1453--1457",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Evaluation of WHO criteria for identifying patients with severe acute respiratory syndrome out of hospital: prospective observational study",
            "authors": [
                {
                    "first": "T",
                    "middle": [
                        "H"
                    ],
                    "last": "Rainer",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Cameron",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Smit",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "BMJ",
            "volume": "326",
            "issn": "",
            "pages": "1354--1362",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Asymptomatic severe acute respiratory syndrome-associated coronavirus infection",
            "authors": [
                {
                    "first": "Hkk",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "Eyk",
                    "middle": [],
                    "last": "Tso",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "N"
                    ],
                    "last": "Chau",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Emerg Infect Dis",
            "volume": "9",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Prevalence of IgG antibody to SARS-associated coronavirus in animal traders",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "52",
            "issn": "",
            "pages": "986--993",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "SARS reference-07/2003. Flying Publisher. Available at",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF1": {
            "text": "Characteristics of 131 health care workers (HCWs) in severe acute respiratory syndrome (SARS) wards and 192 HCWs in non-SARS medical wards at Prince of Wales Hospital, Hong Kong, People's Republic of China.",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "Attack rates of clinical severe acute respiratory syndrome (SARS) among health care workers (HCWs) in medical wards at Prince of Wales Hospital, Hong Kong, People's Republic of China. in the SARS wards. A literature search revealed 1 case report of a nurse who was found to have asymptomatic SARS-CoV antibody seroconversion during screening involving 101 HCWs at another major infectious diseases hospital[8]. A seroprevalence study of SARS-CoV conducted by the Guangdong Center for Disease Control and Prevention (Guangdong, People's Republic of China) indicated a prevalence of 2.9% among hospital workers (4 of 137) in 2 Guangdong city hospitals[9]. Our finding of 3 (2.3%) of 131 patients in the group of HCWs who worked in the SARS wards suggests that subclinical infection is an uncommon event in the hospital setting, despite large outbreaks of SARS.Discussion. In Hong Kong, 1755 probable SARS cases had been diagnosed up to 11 July 2003, with a mortality rate of 17%",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}