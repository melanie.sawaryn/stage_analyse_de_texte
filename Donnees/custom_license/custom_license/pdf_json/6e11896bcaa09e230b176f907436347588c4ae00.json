{
    "paper_id": "6e11896bcaa09e230b176f907436347588c4ae00",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "Kyla",
                "middle": [
                    "N"
                ],
                "last": "Price",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Laboratory of Investigative Dermatology",
                    "institution": "The Rockefeller Uni-versity",
                    "location": {
                        "settlement": "New York",
                        "region": "New York"
                    }
                },
                "email": ""
            },
            {
                "first": "John",
                "middle": [
                    "W"
                ],
                "last": "Frew",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Univer-sity of California",
                    "location": {
                        "settlement": "Los Angeles, Los Angeles",
                        "region": "California"
                    }
                },
                "email": ""
            },
            {
                "first": "Jennifer",
                "middle": [
                    "L"
                ],
                "last": "Hsiao",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Arizona",
                    "location": {
                        "settlement": "Tucson",
                        "region": "Arizona. d"
                    }
                },
                "email": ""
            },
            {
                "first": "Vivian",
                "middle": [
                    "Y"
                ],
                "last": "Shi",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "To the Editor: We read with great interest the Commentary by Lebwohl et al 1 recently published in the Journal of the American Academy of Dermatology. The authors provided a pertinent overview of infection risk associated with commonly used biologics to treat psoriasis in light of the current coronavirus disease 2019 (COVID-19) outbreak. We agree that this time has been particularly concerning for patients taking immunomodulators/immunosuppressants who are unsure of their risk for severe disease.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "COVID-19 and immunomodulator/ immunosuppressant use in dermatology"
        },
        {
            "text": "In response to the previous commentary, the goal of this letter is to expand and provide the latest information about COVID-19 along with considerations for addressing patient concerns surrounding dermatology-related immunomodulator/immunosuppressant use.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "COVID-19 and immunomodulator/ immunosuppressant use in dermatology"
        },
        {
            "text": "Theoretical data from previous coronavirus outbreaks has suggested a strong role for type I interferon, B-cellereleased antibodies, tumor necrosis factor-, and other cytokines in the viral immune response (Fig 1) . [2] [3] [4] Interleukin (IL) 17 cytokines are important for immune cell recruitment to infection sites to promote clearance, while also activating downstream cascades of cytokines and chemokines. 4 IL-1 promotes fever and the differentiation of T-helper cells to IL-17eproducing T cells. Tumor necrosis factor-promotes dendritic cell differentiation, leukocyte recruitment, and mediates fever. 4 Antibodies produced by plasma cells help to neutralize the virus, limit infection, and prevent future infections. Disruption of B-cell differentiation into plasma cells could limit antibody production. 3 Broad immunosuppression across multiple cytokine axes with immunosuppressants has the potential to increase susceptibility, persistence, and reactivation of viral infections. Immunosuppressants decrease cytokines that recruit and differentiate immune cells needed to clear the infection. In addition, inflammatory mediators can become hyperactivated, resulting in a ''cytokine storm,'' which is the primary cause of death in severe disease. 3 Whether withdrawal of broadly immunosuppressive therapies may increase the risk of precipitating cytokine storm is unknown. Therefore, classic immunosuppressants may present the most concerning risk for those affected by COVID-19 (Table I) . Immunomodulators, such as biologics, that do not target vital domains within the viral immune response may dampen, but not significantly affect viral clearance.",
            "cite_spans": [
                {
                    "start": 215,
                    "end": 218,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 219,
                    "end": 222,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 223,
                    "end": 226,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 411,
                    "end": 412,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 609,
                    "end": 610,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 813,
                    "end": 814,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1256,
                    "end": 1257,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [
                {
                    "start": 205,
                    "end": 212,
                    "text": "(Fig 1)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1488,
                    "end": 1497,
                    "text": "(Table I)",
                    "ref_id": null
                }
            ],
            "section": "COVID-19 and immunomodulator/ immunosuppressant use in dermatology"
        },
        {
            "text": "Currently, there are no data describing the benefits or risks of stopping immunomodulators/ immunosuppressants during the COVID-19 outbreak. However, each medication's mechanism of action, administration method/frequency, and pharmacokinetics/pharmacodynamics are important to consider. Nonbiologic medications, including small molecule inhibitors and immunosuppressants, are typically easier to stop and restart within days to weeks due to shorter half-life. Meanwhile, biologics generally have a longer half-life and include a risk of antidrug antibody formation with treatment cessation and subsequent continuation. However, biologics also tend to be more targeted and less involved in the previously mentioned components of the viral immune response. General medication considerations are included in Table I . Although patient dependent, clinicians may consider weaning patients with stable disease off of immunosuppressants.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 805,
                    "end": 812,
                    "text": "Table I",
                    "ref_id": null
                }
            ],
            "section": "COVID-19 and immunomodulator/ immunosuppressant use in dermatology"
        },
        {
            "text": "Shared decision making is needed when deciding on a treatment plan that includes immunomodulators/immunosuppressants during the COVID-19 outbreak. Patients with existing comorbidities may require more conservative measures. 5 Physicians should continue to consult with the Centers for Disease Control and Prevention Information for Healthcare Providers, which is updated daily (https://www.cdc.gov/coronavirus/2019-nCoV/hc p/index.html). Once again, we thank the authors for raising awareness of patient concerns during this evolving outbreak. ",
            "cite_spans": [
                {
                    "start": 224,
                    "end": 225,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "COVID-19 and immunomodulator/ immunosuppressant use in dermatology"
        },
        {
            "text": "The resulting inflammatory cytokines and antibodies continue to stimulate the production of additional cytokines and antibodies, which may contribute to the ''cytokine storm'' noted in those with severe disease. (8) The inflammatory cytokines and antibodies also promote the influx of neutrophils, monocytes, and macrophages along with additional inflammatory cytokines. (Right) The drug targets for common dermatologic immunomodulators and immunosuppressants have also been included in this diagram. FGF, Basic fibroblast growth factor; GCSF, granulocyte-colony stimulating factor; GMCSF, granulocyte-macrophage colonystimulating factor; IL, interleukin; IP10, interferon -induced protein 10; IRF, interferon regulatory factor; MCP1, monocyte chemoattractant protein 1; MIP1A, macrophage inflammatory protein 1-; NFAT, nuclear factor of activated T cells; NF-B, nuclear factor-B; PDE4, phosphodiesterase 4; PDGF, platelet-derived growth factor; PKA, protein kinase A; T H , T-helper cell; TNF, tumor necrosis factor; VEGFA, vascular endothelial growth factor A. Created with Biorender.com.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ") Once the virus is identified, macrophages present viral components to activate and induce (5) differentiation of T cells and B cells. (6) Activated B cells differentiate into plasma cells that produce antibodies important for neutralizing viruses. (7)"
        },
        {
            "text": "Bees, GpSkin, Altus Labs, and Skin Actives Scientific. There were no incentives or transactions, financial or otherwise, relevant to this manuscript. Kyla N. Price and Drs Frew and Hsiao have no conflicts of interest to declare. IRB approval status: Not applicable.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ") Once the virus is identified, macrophages present viral components to activate and induce (5) differentiation of T cells and B cells. (6) Activated B cells differentiate into plasma cells that produce antibodies important for neutralizing viruses. (7)"
        },
        {
            "text": "Accepted for publication March 20, 2020.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ") Once the virus is identified, macrophages present viral components to activate and induce (5) differentiation of T cells and B cells. (6) Activated B cells differentiate into plasma cells that produce antibodies important for neutralizing viruses. (7)"
        },
        {
            "text": "Reprints not available from the authors. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": ") Once the virus is identified, macrophages present viral components to activate and induce (5) differentiation of T cells and B cells. (6) Activated B cells differentiate into plasma cells that produce antibodies important for neutralizing viruses. (7)"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Should biologics for psoriasis be interrupted in the era of COVID-19?",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Lebwohl",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Rivera-Oyola",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "F"
                    ],
                    "last": "Murrell",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Am Acad Dermatol",
            "volume": "82",
            "issn": "5",
            "pages": "1217--1218",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The epidemiology and pathogenesis of coronavirus disease (COVID-19) outbreak",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "A"
                    ],
                    "last": "Rothan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "N"
                    ],
                    "last": "Byrareddy",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "J Autoimmun",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Immune responses in COVID-19 and potential vaccines: lessons learned from SARS and MERS epidemic",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Prompetchara",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Ketloy",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Palaga",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Asian Pac J Allergy Immunol",
            "volume": "38",
            "issn": "1",
            "pages": "1--9",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Coronavirus infections and immune responses",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Lai",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Med Virol",
            "volume": "92",
            "issn": "4",
            "pages": "424--432",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Centers for Disease Control and Prevention. Coronavirus (COVID-19). Available at",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jaad.2020.03.046"
                ]
            }
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "COVID-19 viral immune response and targets of common dermatologic immunomodulators and immunosuppressants. (Left) (1) Person-to-person transmission of COVID-19 occurs though direct contact with respiratory secretions of infected individuals.2 The virus invades host cells by binding to their receptors and fusing with the cell membrane. (2) It is hypothesized that once inside the body, the lung epithelial cells become the primary target, where the receptor binding domain of the virus spikes bind to angiotensin-converting enzyme 2 (ACE2) receptors of ACE2-expressing target cells. (3) Although not confirmed, it is believed the virus dampens the initial type 1 interferon (IFN ) responses, which contributes to uncontrolled viral replication.(4",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "IL, Interleukin; NF-B, nuclear factor B; PDE4, phosphodiesterase 4. *General considerations only, medication use should be considered based on each individual patient's risk and disease profile.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}