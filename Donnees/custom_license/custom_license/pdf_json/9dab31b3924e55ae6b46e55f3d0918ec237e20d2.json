{
    "paper_id": "9dab31b3924e55ae6b46e55f3d0918ec237e20d2",
    "metadata": {
        "title": "Chapter 4 Crystallization and Structural Determination of the Receptor-Binding Domain of MERS-CoV Spike Glycoprotein",
        "authors": [
            {
                "first": "Haixia",
                "middle": [],
                "last": "Zhou",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Shuyuan",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Xinquan",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Three-dimensional structures of the receptor-binding domain (RBD) of MERS-CoV spike glycoprotein bound to cellular receptor and monoclonal antibodies (mAbs) have been determined by X-ray crystallography, providing structural information about receptor recognition and neutralizing mechanisms of mAbs at the atomic level. In this chapter, we describe the purification, crystallization, and structure determination of the MERS-CoV RBD.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "The first three-dimensional structure of the MERS-CoV spike glycoprotein receptor-binding domain (RBD), providing the molecular basis of viral attachment to host cells, was determined in the complex with it cellular receptor dipeptidyl peptidase 4 (DPP4, also called CD26) by X-ray crystallography [1] . Because of the significance in receptor recognition and specific pathogenesis, RBD became a hot spot in the study of MERS-CoV. A number of structures of RBD bound by monoclonal antibodies (mAbs) have also been determined and deposited in the Protein Data Bank (PDB, http://www.rcsb.org/pdb/) [2] [3] [4] [5] [6] [7] [8] . Our group determined the RBD structures in complex with DPP4 and the mAbs MERS-27, MERS-4 and MERS-GD27, respectively [2] [3] [4] 9] . All the three-dimensional structures of MERS-CoV RBD have been determined by X-ray crystallography, which is a powerful method for determining molecular structures at atomic resolution. Briefly, the ordered and repeated atoms in a single protein crystal can diffract the incident X-ray beam into many specific directions. The angles and intensities of these diffracted X-rays can be collected and measured in an X-ray diffraction experiment. After obtaining the phases of these diffracted X-rays by heavy-atom derivative, anomalous scattering or molecular replacement methods, a protein crystallographer then calculates the density of electrons with the protein crystal and builds a structural model based on the density map. For details on the principles and methodology of protein crystallography, please refer to the range of other excellent textbooks.",
            "cite_spans": [
                {
                    "start": 298,
                    "end": 301,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 596,
                    "end": 599,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 600,
                    "end": 603,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 604,
                    "end": 607,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 608,
                    "end": 611,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 612,
                    "end": 615,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 616,
                    "end": 619,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 620,
                    "end": 623,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 744,
                    "end": 747,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 748,
                    "end": 751,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 752,
                    "end": 755,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 756,
                    "end": 758,
                    "text": "9]",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "In this chapter, an overview of the standard method of protein crystallography is briefly introduced, focusing on crystallization and structural determination of MERS-CoV RBD using the molecular replacement method.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Prepare all solutions using ultrapure water (prepared by purifying deionized water, to attain a resistivity of 18 M\u03a9 cm at 25 C) and analytical grade reagents. When dealing with waste, we strictly follow all waste disposal regulations.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials"
        },
        {
            "text": "1. pFastBac vector containing the MERS-CoV RBD gene.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Expression"
        },
        {
            "text": "3. LB liquid medium: 10 g tryptone, 5 g yeast extract, 10 g NaCl, and 1 L ultrapure water; sterilize by high-pressure steam.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DH10Bac competent cells."
        },
        {
            "text": "4. Liquid LB selection medium: LB liquid medium, 50 \u03bcg/mL kanamycin, 7 \u03bcg/mL gentamicin, and 10 \u03bcg/mL tetracycline.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DH10Bac competent cells."
        },
        {
            "text": "5. Bacmid selection LB agar plate:10 g tryptone, 5 g yeast extract, 10 g agar powder, 10 g NaCl, and 1 L ultrapure water. Sterilization at high-pressure steam. 50 \u03bcg/mL kanamycin, 7 \u03bcg/mL gentamicin, 10 \u03bcg/mL tetracycline, 100 \u03bcg/mL x-gal, 40 \u03bcg/ mL IPTG Mix and pout into sterile plates (see Note 1). ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DH10Bac competent cells."
        },
        {
            "text": "MER-CoV RBD can be expressed using the Bac-to-Bac baculovirus expression system (Fig. 1 ), collected and captured using NTA Sepharose (GE Healthcare) and then further purified by gel filtration chromatography using a Superdex 200 High Performance column (GE Healthcare). Crystallization trials are set up using the hanging-drop or sitting-drop vapor diffusion method in conjunction with the sparse-matrix crystal screening kits. The structure of MERS-CoV RBD in complex with MERS-4scFv was determined using the molecular replacement method. When the total volume remaining is reduced to 50 mL, add 100 mL HBS buffer to collect all the liquid in the system in a beaker. Then dispense into high-speed centrifuge tubes, centrifuge at 3000 \u00c2 g for 1 h at 4 C.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 80,
                    "end": 87,
                    "text": "(Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Methods"
        },
        {
            "text": "4. The supernatant after centrifugation is loaded onto the nickel-NTA beads equilibrated with 30 mL of HBS buffer. MERS-CoV RBD with a His tag could be captured by nickel-NTA beads. Repeat loading the sample once more.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "5. Add wash buffer to the beads to remove the nonspecifically bound proteins until the flow-through is not able to discolor the Coomassie Brilliant Blue G250 solution.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "6. After adding elution buffer, the target protein will dissociate from the beads; collect it in a 10 kDa Millipore concentrating tube. Similarly, detect protein with Coomassie Brilliant Blue G250. The concentrating tube containing the protein sample is centrifuged at 3000 \u00c2 g to concentrate the sample to less than 7. MERS-CoVRBD is further purified by gel filtration chromatography. The sample is loaded onto the Superdex 200 column pre-equilibrated with HBS buffer. Fractions containing RBD are collected and the protein's purity is confirmed by SDS-PAGE (Fig. 2) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 559,
                    "end": 567,
                    "text": "(Fig. 2)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Methods"
        },
        {
            "text": "8. Dilute the protein to 1 mg/mL, and digest with endoglycosidase F1 and F3 (F1/F3: RBD at the ratio of 1:100) at 18 C overnight. The digested protein is concentrated and purified by gel filtration chromatography same as above (optional).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "9. After preparing the MERS-CoV RBD protein and MERS-4scFv protein (see Note 11) detect the absorption of the protein sample at 280 nm (A280). According to the molecular weight and extinction coefficient, the molar concentration can be calculated. The two proteins were mixed at molar ratio of 1:1, incubated on ice for 1 h, and purified using a Superdex 200 column. Collect the fractions containing the complex and confirm the protein purity by SDS-PAGE.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "Sartorius centrifugal concentrator to concentrate to 10-15 mg/mL. After mixing and aspirating, centrifuge at 10,000 \u00c2 g for 10 min at 4 C. 3. Use TTP LabTech's mosquito crystallization setup for automated crystallography. Absorb 3 \u03bcL protein on the 8-well 5 \u03bcL micro-reservoir strip (Fig. 3a) . Then the needles aspirate the protein from the strip onto the SWISSCI plate with 200 nL of protein per well (Fig. 3b) , using the sitting-drop vapor diffusion method by mixing 200 nL reservoir and 200 nL reservoir (Fig. 3c,d) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 283,
                    "end": 292,
                    "text": "(Fig. 3a)",
                    "ref_id": "FIGREF3"
                },
                {
                    "start": 403,
                    "end": 412,
                    "text": "(Fig. 3b)",
                    "ref_id": "FIGREF3"
                },
                {
                    "start": 509,
                    "end": 520,
                    "text": "(Fig. 3c,d)",
                    "ref_id": "FIGREF3"
                }
            ],
            "section": "Collect the purified MERS-CoV RBD protein and use a 2 mL"
        },
        {
            "text": "4. Seal the plate with tape and gently place it in an 18 C room.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Collect the purified MERS-CoV RBD protein and use a 2 mL"
        },
        {
            "text": "5. Check the sample drops under a microscope at 20-100\u00c2 magnifications after 3 and 7 days (and if necessary, after 1 and 2 weeks, and 1, 3, and 6 months). 6 . A week later, we found crystal growth in the PEG/Ion, PEGRX and JCSG+ kits. Specific conditions were as follows (Fig. 4a,b) 2. The diffraction images should be collected on the BL17U beamline (Fig. 4c) . Rotate the mounted crystal and the X-ray diffraction patterns should be recorded at 1 per image, and collected for 360 .",
            "cite_spans": [
                {
                    "start": 155,
                    "end": 156,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [
                {
                    "start": 271,
                    "end": 282,
                    "text": "(Fig. 4a,b)",
                    "ref_id": "FIGREF4"
                },
                {
                    "start": 351,
                    "end": 360,
                    "text": "(Fig. 4c)",
                    "ref_id": "FIGREF4"
                }
            ],
            "section": "Collect the purified MERS-CoV RBD protein and use a 2 mL"
        },
        {
            "text": "3. The diffraction images in a dataset should be processed with HKL2000 [10] including auto-index, refinement, integration, and scaling steps. After data processing, the crystal unit cell parameters, crystal space group, Miller indexes of reflections, intensities, and error estimates of reflections should be determined and stored in a * .sca file, which provides the dataset applicable to structure determination.",
            "cite_spans": [
                {
                    "start": 72,
                    "end": 76,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Collect the purified MERS-CoV RBD protein and use a 2 mL"
        },
        {
            "text": "4. Using CCP4 suite solve the structure as follows: Export the * . sca file to a * .mtz file using the program SCALPACK2MTZ. Use the * .mtz file to calculate the solvent via MATTHEWS_-COEF. Run with PHASER MR (see Note 12) with the MERS-CoV RBD structure (PDB ID: 4l72) and the structures of the variable domain of the heavy and light chains available in the PDB with the highest sequence identities as search models (see Note 12) [11] . When the phases are determined, the electron density map can be calculated, from which the molecular model can be constructed. 5. Subsequent model building and refinement were performed using COOT and PHENIX, respectively (see Note 13) [12, 13] .",
            "cite_spans": [
                {
                    "start": 431,
                    "end": 435,
                    "text": "[11]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 674,
                    "end": 678,
                    "text": "[12,",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 679,
                    "end": 682,
                    "text": "13]",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "Collect the purified MERS-CoV RBD protein and use a 2 mL"
        },
        {
            "text": "6. Many validation programs are used to check the structure, until the investigator is satisfied, and then the structure can be deposited in the PDB.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Collect the purified MERS-CoV RBD protein and use a 2 mL"
        },
        {
            "text": "1. Weigh 10 g tryptone, 5 g yeast extract, 10 g agar powder, and 10 g NaCl, and add ultrapure water to 1 L. After sterilization by high-pressure steam, wait until the temperature of the medium drops to about 60 C, add the required antibiotics, inducers etc. (50 \u03bcg/mL kanamycin,7 \u03bcg/mL gentamicin, 10 \u03bcg/mL tetracycline, 100 \u03bcg/mL x-gal, 40 \u03bcg/mL IPTG). Mix evenly and then pour into sterile plates. When the culture medium has cooled and solidified, store the bacmid selection LB agar plate at 4 C. 6. Endoglycosidase F1 and F3 are expressed and purified from E. coli by our laboratory. The endoglycosidase was added into the reaction system according to the mass ratio of 1:100.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Notes"
        },
        {
            "text": "7. The coding sequence of the MERS-CoV RBD (EMC strain, spike residues 367-588) was ligated into the pFastBac-Dual vector (Invitrogen) with a N-terminal gp67 signal peptide to enable the protein secreting outside the cell and a C-terminal His-tag to facilitate further purification processes.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "TIANprep Mini"
        },
        {
            "text": "8. Allow the pellet to dissolve for at least 10 min at room temperature. To avoid shearing the DNA, pipet only 1-2 times to resuspend. Store the bacmid at 4 C and use it as soon as possible, usually within 1 week. Aliquot the bacmid DNA into separate tubes and store at \u00c020 C (not in a frost-free fridge). Avoid multiple freeze/thaw cycles as this decreases the transfection efficiency.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "TIANprep Mini"
        },
        {
            "text": "9. Characteristics of infected cells: A 25-50% increase in cell diameter can be seen and the size of cell nuclei increases at the early stage. Cells release from the plate and appear lysed, showing signs of clearing in the monolayer. 10 . P0 virus can be stored for years, adding 2% (v/v) FBS at 4 C, protected from light.",
            "cite_spans": [
                {
                    "start": 234,
                    "end": 236,
                    "text": "10",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "TIANprep Mini"
        },
        {
            "text": "11. The expression of MERS-4scFv protein was conducted in 293F cells transiently transfected with plasmid DNA. After 72 h, the supernatant was collected and concentrated. The purified MERS-4scFv protein was obtained by Ni-NTA affinity chromatography and Superdex 200 size-exclusion chromatography. The purification method is the same as that of RBD protein.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "TIANprep Mini"
        },
        {
            "text": "12. If the crystal structure of the same protein or a similar protein has been solved, the molecular replacement method can be applied. After obtaining the solutions of the rotation and translation functions, initial phases can be calculated from the reference model, after which the electron density can be calculated.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "TIANprep Mini"
        },
        {
            "text": "13. The accuracy of the constructed model is confirmed by the crystallographic R-factor and R-free, which indicate the discrepancy between the calculated and observed amplitudes. The stereo-chemical parameters of the model can also be checked using programs such as MOLPROBITY, PROCHECK, or RAMPAGE.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "TIANprep Mini"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Molecular basis of binding between novel human coronavirus MERS-CoV and its receptor CD26",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Nature",
            "volume": "500",
            "issn": "7461",
            "pages": "227--231",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Structural basis for the neutralization of MERS-CoV by a human monoclonal antibody MERS-27",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Sci Rep",
            "volume": "5",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Structural definition of a unique neutralization epitope on the receptorbinding domain of MERS-CoV spike glycoprotein",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Cell Rep",
            "volume": "24",
            "issn": "2",
            "pages": "441--452",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Ultrapotent human neutralizing antibody repertoires against Middle East respiratory syndrome coronavirus from a recovered patient",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Niu",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "J Infect Dis",
            "volume": "218",
            "issn": "8",
            "pages": "1249--1260",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Evaluation of candidate vaccine approaches for MERS-CoV",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Nat Commun",
            "volume": "6",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Junctional and allelespecific residues are critical for MERS-CoV neutralization by an exceptionally potent germline-like antibody",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ying",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Nat Commun",
            "volume": "6",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Introduction of neutralizing immunogenicity index to the rational design of MERS coronavirus subunit vaccines",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Du",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Nat Commun",
            "volume": "7",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Importance of neutralizing monoclonal antibodies targeting multiple antigenic sites on MERS-CoV spike to avoid neutralization escape",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "J Virol",
            "volume": "92",
            "issn": "10",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.02002-17"
                ]
            }
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Structure of MERS-CoV spike receptor-binding domain complexed with human receptor DPP4",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Cell Res",
            "volume": "23",
            "issn": "8",
            "pages": "986--993",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Processing of X-ray diffraction data collected in oscillation mode",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Otwinowski",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Minor",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Methods Enzymol",
            "volume": "276",
            "issn": "",
            "pages": "307--326",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Phaser crystallographic software",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Mccoy",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "J Appl Crystallogr",
            "volume": "40",
            "issn": "",
            "pages": "658--674",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Coot: modelbuilding tools for molecular graphics",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Emsley",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Cowtan",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Acta Crystallogr D Biol Crystallogr",
            "volume": "60",
            "issn": "",
            "pages": "2126--2132",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "PHENIX: building new software for automated crystallographic structure determination",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "D"
                    ],
                    "last": "Adams",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Acta Crystallogr D Biol Crystallogr",
            "volume": "58",
            "issn": "",
            "pages": "1948--1954",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Schematic diagram of bac-to-bac expression system",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Gel filtration profile of MERS-CoV RBD protein and digested by endoglycosidase F1 and F3 MERS-CoV RBD protein, confirmed by SDS-PAGE. The first two lanes are MERS-CoV RBD protein and the last two lanes are digested RBD protein 1 mL. Transfer the sample to a 1.5 mL EP tube and centrifuge at 3000 \u00c2 g for 10 min.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "All crystals should be flash-frozen in liquid nitrogen after being incubated in the reservoir solution containing 20% (v/v) glycerol.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Crystallization by mosquito. Panel a-d shows the operation diagram of the TTP LabTech's setups: blue dots represent proteins, and square lattices represent reservoir. Panel e shows the TTP LabTech's Mosquito machine. Panel f shows the SWISSCI plate",
            "latex": null,
            "type": "figure"
        },
        "FIGREF4": {
            "text": "Crystals of MERS-CoV RBD and MERS-4scFv complex (a, b) and the X-ray diffraction pattern of the crystal complex (c)",
            "latex": null,
            "type": "figure"
        },
        "TABREF1": {
            "text": "Centrifuge the mixture at 15,000 \u00c2 g at 4 C for 20 min. Carefully discard the supernatant. Resuspend the DNA pellet in 1 mL of 70% ethanol. Centrifuge at 15,000 \u00c2 g at 4 C for 5 min. Carefully discard the supernatant. Air-dry the pellet for 30 min. Resuspend the DNA pellet in 50 \u03bcL of sterile ultrapure water (see Note 8).3. Confirm the bacmid quality by separating the DNA on a 0.5% agarose gel. The bacmid is in the position nearest the gel hole. Verify the presence of the MERS-CoV RBD gene in the recombinant bacmid by PCR, using M13 forward and reverse primers (M13 F: CCCAGTCACGACGTTGTAAAACG; M13 R: AGCGGATAACAATTTCACACAGG; optional). 4. Transfection of Sf9 cells: verify that the Sf9 cells are in the log phase (1.5-2.5 \u00c2 10 6 cells/mL) with greater than 95% viability. Add 2 \u00c2 10 6 cells into a 10 cm dish with 2 mL of culture medium (Sf-900 II Serum Free). Allow the cells to attach themselves for 30-60 min at room temperature in the hood. For the transfection sample, dilute 10 \u03bcL Cellfectin II Reagent in 250 \u03bcL of culture medium (Sf-900 II Serum Free). Mix by inverting 5-10 times (do not vortex), and incubate for 5 min at room temperature. Add 1 \u03bcg of bacmid DNA. Mix by inverting 5-10 times (do not vortex), and incubate for 20 min at room temperature. Add the entire DNA-lipid mixture dropwise onto the cells. Incubate the cells at 27 C for 5 h and then remove the supernatant and add 7 mL of fresh culture medium (Sf-900 II Serum Free).",
            "latex": null,
            "type": "table"
        },
        "TABREF4": {
            "text": "Plasmid Kit includes Buffers P1, P2, and P3. We only use the Buffers P1, P2, and P3 for the preceding steps of isolating recombinant bacmid DNA. 3. Method for preparing HBS buffer: Prepare 10\u00c2 HBS buffer (100 mM HEPES, 1500 mM NaCl). Weigh 23.8 g HEPES (Sigma), 87.66 g NaCl (Amresco) dissolve with water to 1 L, adjust pH to 7.2 with NaOH. Then, dilute 100 mL of 10\u00c2 HBS buffer with 900 mL of water. 4. Method of preparing wash buffer: First prepare 5 M imidazole. Weigh 340 g imidazole (Sigma-Aldrich, USA), add water to 1 L and use HCl to adjust pH to 8.0. Add 100 mL 10\u00c2 HBS buffer, 30 mL 5 M imidazole with water to 1 L. 5. Method of preparing Coomassie brilliant blue G-250: Weigh 1 g G-250 (Solarbio), add 100 mL 85% phosphate, 100 mL anhydrous ethanol and water to 1 L. The G-250 needs to be stored away from light.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}