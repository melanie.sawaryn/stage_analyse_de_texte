{
    "paper_id": "bb6a4823b9fa757dc3ceda4d18e2bbdfc296db4d",
    "metadata": {
        "title": "COVID-19 and the Renin- Angiotensin System",
        "authors": [
            {
                "first": "Line",
                "middle": [],
                "last": "Malha",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Medical College of Cornell University",
                    "location": {
                        "settlement": "New York",
                        "region": "New York",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Franco",
                "middle": [
                    "B"
                ],
                "last": "Mueller",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Medical College of Cornell University",
                    "location": {
                        "settlement": "New York",
                        "region": "New York",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Mark",
                "middle": [
                    "S"
                ],
                "last": "Pecker",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Medical College of Cornell University",
                    "location": {
                        "settlement": "New York",
                        "region": "New York",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Samuel",
                "middle": [
                    "J"
                ],
                "last": "Mann",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Medical College of Cornell University",
                    "location": {
                        "settlement": "New York",
                        "region": "New York",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Phyllis",
                "middle": [],
                "last": "August",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Medical College of Cornell University",
                    "location": {
                        "settlement": "New York",
                        "region": "New York",
                        "country": "USA"
                    }
                },
                "email": ""
            },
            {
                "first": "Peter",
                "middle": [
                    "U"
                ],
                "last": "Feig",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Medical College of Cornell University",
                    "location": {
                        "settlement": "New York",
                        "region": "New York",
                        "country": "USA"
                    }
                },
                "email": "pufeig@gmail.com"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "I n late 2019, a coronavirus disease leading to severe acute respiratory syndrome (SARS) started in China and has become a pandemic. The responsible virus has been designated SARS-CoV-2. The cellular receptor for SARS-CoV-2 is angiotensin-converting enzyme 2 (ACE2), a mostly membrane-bound homologue of angiotensin-converting enzyme (ACE) that has generated great interest in the interaction between COVID-19 and the renin-angiotensin system (RAS), 1 as well as in the medicines commonly used to block the RAS. These agents, ACE inhibitors (ACEIs) and angiotensin II receptor blockers (ARBs) are widely used in the treatment of hypertension, heart failure, chronic kidney disease, and diabetes.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "ACE2 and ACE share 42% of amino acid identity, but differ in enzymatic selectivity and, more importantly, in function. ACE2 is expressed primarily in lung, heart, and kidneys. The 2 enzymes play key roles in the RAS. Angiotensin I (AngI), whose formation from angiotensinogen is catalyzed by renin, is then converted into 2 downstream hormones with opposing hemodynamic effects: the vasoconstricting angiotensin II (AngII) and the vasodilating angiotensin-(1-7) (Ang [1] [2] [3] [4] [5] [6] [7] ). AngII and Ang(1-7) have opposing vascular effects via their separate receptors, ATR1 and MAS, respectively. In addition to its role as an antagonist of AngII-mediated vasoconstriction, Ang(1-7) also has been studied as an antifibrotic agent in non-SARS lung disease and is thought to afford protection from lung injury. 2 As depicted in Figure 1 , ACE increases circulating AngII by increasing conversion of AngI into AngII. In contrast, ACE2 decreases circulating AngII, both by diverting some of AngI into formation of Ang(1-9) and by converting some AngII into Ang(1-7).",
            "cite_spans": [
                {
                    "start": 467,
                    "end": 470,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 471,
                    "end": 474,
                    "text": "[2]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 475,
                    "end": 478,
                    "text": "[3]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 479,
                    "end": 482,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 483,
                    "end": 486,
                    "text": "[5]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 487,
                    "end": 490,
                    "text": "[6]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 491,
                    "end": 494,
                    "text": "[7]",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [
                {
                    "start": 835,
                    "end": 843,
                    "text": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "Both AngI and AngII regulate the formation of ACE and ACE2. 3 AngII upregulates ACE and downregulates ACE2 expression under hypertensive conditions both in vivo and in vitro. 3 The major entry of SARS-CoV-2 is via the respiratory system, where the ACE2:ACE ratio is 1:20, in contrast with the kidneys where the ACE2:ACE ratio is approximately 1:1. S1 Given that other viruses that enter via the lung might use ACE2 as receptors, low levels of ACE2 in the lung might help to reduce entry and provide an evolutionary advantage in survival. Despite the higher levels of ACE2 observed in the kidney, the incidence of acute kidney injury in COVID-19 is relatively low (29%), in contrast to 71% incidence of severe lung injury, which requires mechanical ventilation. Observed injuries in kidneys might be secondary to sepsis, hypotension and/ or pre-infection kidney pathology, whereas ACE2 in the kidney might still exert protective effects. 4 Alternatively, any intervention that increases ACE2 in the lung might be detrimental to patients by facilitating the entry of the virus into cells.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "SARS-CoV-2 enters cells by binding its Spike (S) protein to ACE2, 5 a step that is essential for lung injury 6 in COVID-19. Internalization of the virus depletes membrane-bound ACE2, possibly increasing AngII, which could potentiate lung injury. 1,6 AngII stimulation of ATR1, the target receptor for ARBs, contributes significantly to lung damage in experimental models. 1 Blockade of the classic RAS pathway by either ACEIs or ARBs has similar downstream effects from AngII on vasoconstriction, sodium reabsorption, and aldosterone secretion, but the mechanism of blockade has very different effects on AngI and AngII levels. ACEIs increase AngI levels and (perhaps) Ang(1-7) and decrease AngII, whereas ARBs increase levels of AngI, Ang(1-7) , and AngII. Other agents that act on the RAS, such as sympatholytics, betablockers, and direct renin inhibitors, lower the formation of AngI, and therefore of AngII and Ang(1-7), but they have not been the focus of the current controversy about the impact of RAS agents on the severity of infection and subsequent prognosis of COVID-19.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 736,
                    "end": 744,
                    "text": "Ang(1-7)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "In animal models, responses to ARBs or ACEIs are heterogeneous depending on the agent used, dosing, and organ studied. Overall, it appears that ARBs tend to increase ACE2 activity. 7 In an experimental model of SARS-Coronavirus lung injury, despite elevated tissue levels of AngII, ATR1 blockade with the ARB losartan attenuated severe acute lung injury and pulmonary edema. 6 This is in line with findings in hypertensive humans, which showed that the ARB olmesartan increased urinary ACE2 levels. 8 Taken together, however, the current understanding of the effects of ACEIs and ARBs on ACE2, and on viral-mediated lung or cardiac injury is preliminary and insufficient to inform definitive statements on how changes in ACE2 in response to these drugs would affect COVID-19.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The urgent need for effective therapeutic interventions for the ongoing COVID-19 pandemic has led to considerable speculation regarding the impact of RAS blockade. It has been hypothesized that use of ARBs increases ACE2, which could potentially be beneficial in treating COVID-19 1 because the increased ACE2 could prevent lung damage by preserving ACE2 despite SARS-CoV-2 entry into the cells. Alternatively, increased ACE2 could potentially have the opposite result by promoting viral ports of entry into cells.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A case series from China of 12 hospitalized patients with confirmed COVID-19 found that circulating levels of AngII were significantly elevated and negatively correlated with viral load and lung injury as determined by oxygenation. 9 The significance of this observation is unclear given the measurement difficulty and high variability in AngII measurements in humans. It is also possibly confounded by the hemodynamic alterations associated with critical illness, such as hypotension, which stimulates the RAS. At present, the interpretation of any clinical observations regarding the RAS in patients with COVID-19 are difficult to interpret in the context of sepsis, viral infection, and pharmacologic effects of treatment. Nevertheless, the notion that ARBs may mitigate a potential low ACE2/high AngII milieu is of considerable interest.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Given the lack of clarity about the effect of the ACEI and ARB on ACE2 and SARS-CoV-2 interaction and the development of COVID-19, it is not currently possible to recommend using either ACEIs or ARBs with the purpose of treating COVID-19. If needed, short-term discontinuation or delay of initiating ACEI or ARB in patients with otherwise controllable or mild hypertension, stable heart failure, or stable chronic kidney disease is unlikely to have negative consequences. However, longer-term effects will need to become clarified with time. Currently, the authors continue to administer these drugs to patients unless they are critically ill and at high risk for hypotension and kidney injury. Thus, at present, the effect of either ACEIs or ARBs in patients with known COVID-19 infection on the severity of illness during the virus infection remains unknown and it is unclear whether the differences between ACEI and ARB are clinically relevant in these patients.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Clarification on the role of ACE2 expression in COVID-19 and the prognostic implications of ACEIs and ARBs on clinical outcomes are urgently needed. Observational studies 1 and a big data approach evaluating the association of RAS blockade and COVID-19 severity are potential starting points. Additional experimental studies examining the complex relationships between COVID-19 and the RAS would greatly benefit future treatment approaches. De-identified patient data and outcomes on the use of RAS agents and COVID-19 infections should be placed on the Web and made publicly available to help further research. Development of drugs that target ACE2 may have the potential to treat COVID-19 and assist in understanding the role of ACE2 in the disease. As of March 2020, there are 94 registered clinical trials studying COVID-19, with only 1 targeting the RAS and appears to be withdrawn. S2",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "All the authors declared no competing interests.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DISCLOSURE"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Angiotensin receptor blockers as tentative SARS-CoV-2 therapeutics",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Gurwitz",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Angiotensin-converting enzyme 2/angiotensin-(1-7)/Mas axis protects against lung fibrosis by inhibiting the MAPK/ NF-kappaB pathway",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Meng",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "H"
                    ],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Am J Respir Cell Mol Biol",
            "volume": "50",
            "issn": "",
            "pages": "723--736",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Angiotensin II up-regulates angiotensin I-converting enzyme (ACE), but down-regulates ACE2 via the AT1-ERK/p38 MAP kinase pathway",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Koka",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [
                        "R"
                    ],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "C"
                    ],
                    "last": "Chung",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Am J Pathol",
            "volume": "172",
            "issn": "",
            "pages": "1174--1183",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Clinical course and outcomes of critically ill patients with SARS-CoV-2 pneumonia in Wuhan, China: a single-centered, retrospective, observational study",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Lancet Respir Med",
            "volume": "",
            "issn": "20",
            "pages": "30079--30084",
            "other_ids": {
                "DOI": [
                    "10.1016/S2213-2600(20)30079-5"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Angiotensin-converting enzyme 2 is a functional receptor for the SARS coronavirus",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Moore",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Vasilieva",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Nature",
            "volume": "426",
            "issn": "",
            "pages": "450--454",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "A crucial role of angiotensin converting enzyme 2 (ACE2) in SARS coronavirusinduced lung injury",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kuba",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Imai",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Rao",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Nat Med",
            "volume": "11",
            "issn": "",
            "pages": "875--879",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Angiotensin II mediates angiotensin converting enzyme type 2 internalization and degradation through an angiotensin II type I receptor-dependent mechanism. Hypertension",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "R"
                    ],
                    "last": "Deshotels",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Xia",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Sriramula",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "",
            "volume": "64",
            "issn": "",
            "pages": "1368--1375",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Urinary angiotensin-converting enzyme 2 in hypertensive patients may be increased by olmesartan, an angiotensin II receptor blocker",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Furuhashi",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Moniwa",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Mita",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Am J Hypertens",
            "volume": "28",
            "issn": "",
            "pages": "15--21",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Clinical and biochemical indexes from 2019-nCoV infected patients linked to viral loads and lung injury",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Sci China Life Sci",
            "volume": "63",
            "issn": "",
            "pages": "364--374",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Renin-angiotensin ACE2 pathway. ACE, angiotensin-converting enzyme; ACE2, angiotensin-converting enzyme 2; AngI, angiotensin I; AngII, angiotensin II; AngIII, angiotensin III; ATR1, angiotensin receptor 1; ATR2, angiotensin receptor 2; MAS1, proto-oncogene, G protein-coupled receptor.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}