{
    "paper_id": "221fba7a72f9c7870c6397e9ef670f0a2c4d60a6",
    "metadata": {
        "title": "Fighting the Coronavirus Outbreak",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "\u25a0 COVID-19 AND THE CHEMICAL BIOLOGY COMMUNITY Laura Kiessling, ACS Chemical Biology Editor-in-Chief, and Peng Chen, ACS Chemical Biology Associate Editor: The recent global outbreak of the novel coronavirus COVID-19 is changing the research focus of scientists and the ways in which we practice science. To limit the virus spread, those in the USA are canceling both smaller (\u223c100 people) and large meetings, researchers are limiting their travel both overseas and within the country, and student college and graduate school visit weekends are being postponed or stopped altogether. Still, there is much uncertainty about how to proceed and what other measures should be taken. Since the outbreak began in China, Chinese scientists have already faced many of the questions that those of us in other locations are now asking. We therefore asked several Chinese scientists to discuss the strategies that they are pursuing to limit the impact of the virus both on their research and on society. Some of the actions taken by the scientists and the government under this extreme situation may have a long-term influence and shift our future research and funding paradigm. We anticipate that their perspectives will be valuable to the community. global lifethreatening concern in recent days. The war against COVID-19 there has now reached the most critical moment, as confirmed COVID-19 cases have peaked for several days, and almost all the people in Wuhan City have been screened for suspected case identification (as of late February). We cannot imagine how people there are sacrificing to control the spread of COVID-19. As alumni of Wuhan University that is located in the center of Wuhan city, we became deeply saddened and worried as we watched the entire city cope with an unprecedented emergency situation. We wanted to provide a glimpse of what chemical biologists have been working on, from the perspective of scientists in China. In the past 40 days, we have heard more stories of hope and resilience than of worry and grief in China, where 1.4 billion people are doing their best to bring this serious outbreak under control. Right now, the epidemic situation outside Hubei is gradually improving, and the situation inside Hubei has shown signs of stabilization. It is noteworthy that although it is mandated that most people have to stay at home as a quarantine procedure, scientists in China have been increasingly devoted to combating COVID-19, with several clinical trials currently underway. As for scientists throughout the world, researchers in China are making extraordinary efforts toward rapid advancement of diagnostic and treatment approaches, and are doing so under highly challenging personal circumstances with extensive restrictions on not only travel, but day-to-day activities. As mentioned by WHO officers, Chinese scientists have been extremely professional in the face of this epidemic and have made their findings rapidly available to researchers around the world to coordinate global efforts to characterize, treat, and mitigate the spread of COVID-19. One team led by Yongzhen Zhang from Fudan University completed the deep sequencing of COVID-19 and made its whole genome sequence publicly available on January 10, 2020. 1 Medical experts, virologists, structural biologists, and medicinal chemists have made significant contributions in areas such as epidemic prevention and control, patient treatment, virus classification and traceability, structure analysis, and clinical drug development. The current progress relied on the rapid development of science and technology, as well as the dedication of scientific researchers. During the epidemic, almost all industries not related to any antiepidemic efforts were halted due to the lock down of transportation and lack of manpower. It has been extremely difficult to complete highlevel scientific research under such an extreme situation.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Here, we hope to provide a glimpse into current efforts and progress made by Chinese scientists during the outbreak, especially those who work at the interface of chemistry and biology, and to discuss how the community can work together to prevent and control coronavirus infection now, and in the future.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "On the basis of the crystal structure and computational chemistry of 2019-nCoV hydrolase (Mpro), a research team led by Prof. Hualiang Jiang at the Shanghai Institute of Materia Medica of the Chinese Academy of Sciences performed virtual screening of marketed drugs and the self-built compound library with \"high druggability.\" The hit compounds are currently subject to further experimental tests for identifying potential drug candidates. 2 Another expert in chemical biology and structural biology\ue0d5Prof. Wenshe Liu from Texas A&M University\ue0d5performed a docking study and suggested the use of remdesivir, a clinical trial III antiviral drug developed by Gilead, as a safe and potentially effective drug against 2019-nCoV 3,4 ( Figure 1 ). He posted his independent findings as a preprint on January 27, which was supported by the \"sympathetic use\" of the first patient in the United States around the same time. 5 Gilead is now working with hospitals in China to carry out large-scale clinical trials. 6 The anti-malarial drug chloroquine is also in clinical trials and showing promising preliminary results. Other potential therapeutics, such as a plant microRNA (MIR2911), found by Prof. Chenyu Zhang from Nanjing University, are under preclinical or clinical evaluations. In order to speed up the discovery process of the neutralizing antibody, a team led by Prof. Sunney Xie at Peking University adopted a method for high-throughput single-cell sequencing of B cells (only VDJ region) from the blood samples in patients during rehabilitation. 7 In collaboration with the You'an Hospital of Beijing Medical University, they collected blood samples from 12 convalescent patients, performed B-cell single cell sorting and library construction, and detected 15 000 IgG antibody sequences in 143 000 B cells. A total of 138 IgG antibodies with the highest enrichment were selected, and the ELISA-based affinity screening confirmed the nanomolar level of binding to the surface protein S and RBD of the virus. 7 The ongoing neutralizing antibody screening is expected to facilitate virus prevention and treatment, as well as diagnostics.",
            "cite_spans": [
                {
                    "start": 441,
                    "end": 442,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 914,
                    "end": 915,
                    "text": "5",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1004,
                    "end": 1005,
                    "text": "6",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [
                {
                    "start": 729,
                    "end": 737,
                    "text": "Figure 1",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "As a key component in epidemic prevention and control, the diagnosis of COVID-19 determines the classification and treatment of patients. In the early stage of the epidemic, clinicians relied on the traditional nucleic acid detection kit as the most precise method for diagnosis; however, it is timeconsuming with an inherent low positive detection rate. As an alternative approach, a team led by Prof. Xueji Zhang and Prof. Xinchun Chen at Shenzhen University developed a chem-iluminescence-based antibody detection kit, which was successfully tested and put into use on February 4, 2020. 8 The kit is based on the enrichment and detection of IgM in serum and is suitable for clinical samples from patients with fevers in the past 7\u221214 days. It can complete the rapid diagnosis of coronavirus infection in as fast as 22 min, which greatly reduced the diagnosis time. Many other scientists in China have also been dedicated to the development of diagnostic methods. For example, a joint team led by Prof. Weihong Tan from Renji Hospital of Shanghai Jiaotong University and Hunan University has developed a new diagnostic method via portable RT-PCR-based molecular diagnostics. 9 At the center of the outbreak, a joint team led by Prof. Tiangang Liu at Wuhan University and Prof. Yan Li from the Renmin Hospital of Wuhan University developed a nanopore target sequencing (NTS) method to detect COVID-19 and other potential respiratory viruses simultaneously. By combining the advantages of long-read, real-time nanopore sequencing and target amplification, this approach allowed more sensitive detection than qPCR kits and could also monitor for mutation or other respiratory virus infections in test samples. 10 In addition to the academic community, scientists from industry with a background in chemical biology are also actively participating in the war against the epidemic. On January 25th, the first day of the Chinese New Year, an R&D team from a biotech company announced successful development of a rapid and sensitive detection system that only takes 9 min for pretreatment of 32 samples (Figure 2 ). With the combination of an automatic nucleic acid extraction instrument, the daily test capacity for a single technician can reach 1000 samples. The diagnosis kits have been utilized in over 10 Chinese provinces and used to test more than 100 000 clinical samples in the first 2 weeks of February alone. Dr. Lu Dong and Dr. Haiping Song are two leading scientists in this team, who previously graduated from the Department of Chemical Biology, College of Chemistry at Peking University. Scientists in Hong Kong, one of the busiest international transportation hubs in China, are also dedicated to protecting their city through everything they can do. For example, Prof. Chi-Ming Che and Vivian Yam, two of the most famous It should be noted that the aforementioned work is only a part of the growing list of examples in fighting the virus on an invisible battlefield, and the global medical and scientific communities have been working tirelessly to put forth solutions to bring the current outbreak under control. Facing such a sudden and serious epidemic outbreak, we feel that scientific research is difficult to accomplish overnight, and many scientific studies cannot keep up with the fast spread of the virus. Global efforts from research institutions and pharmaceutical companies with a \"record breaking\" speed are highly desired in the arms race against the virus. To our delight, it only took a few days for remdesivir to enter clinical trials. A coherent effort from every section of the drug development chain made this fast track possible, which we hope can eventually turn into a life-saving therapy.",
            "cite_spans": [
                {
                    "start": 590,
                    "end": 591,
                    "text": "8",
                    "ref_id": null
                },
                {
                    "start": 1177,
                    "end": 1178,
                    "text": "9",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1709,
                    "end": 1711,
                    "text": "10",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [
                {
                    "start": 2098,
                    "end": 2107,
                    "text": "(Figure 2",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "After this epidemic, we have further realized that human beings need more technical reserves when facing major infectious diseases, and many directions need to be urgently explored. From the chemical biology perspective, below are some directions that need the attention from scientists working at the interface of chemistry, life sciences, and medicine:",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "(i) Development of rapid, accurate, safe, and convenient detection and diagnosis technologies. Early stage screening is to quickly isolate confirmed patients. Intermediate stage screening can provide basis for precise treatment, and later screening is to establish reliable standards for discharge and desegregation. Specific diagnostic methods should be developed for each of the different stages. (ii) Fast screening of safe and effective drugs. In the face of sudden public health safety incidents, the fastest way is to establish treatments to identify drugs already available in drug libraries since their safety has been proved in clinical trials. At the same time, drug efficacy needs to be screened for different stages of the disease. For example, for advanced patients, reducing the cytokine storm may be a major direction. (iii) Development of neutralizing antibodies and vaccines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Interdisciplinary participation, including chemical biology, is expected to accelerate the research and development process, for example, how to speed up the adaptive immune response to develop specific IgG antibodies, how to efficiently enrich antibodies from serum, and how to quickly and cost-effectively isolate human B cells and perform single BCR sequencing to facilitate the identification of neutralizing antibodies.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The outbreak of COVID-19 may trigger many scientists to reconsider and reshape their research interests toward infectious-disease-related fields and encourage research funders to commit more long-term resources to these efforts, even after the current outbreak has been contained. Indeed, we should speed up our scientific advancement so that we can build the defense line more quickly when combating a mysterious and rapidly evolving virus in the future. Coherent and interdisciplinary efforts among the entire scientific community will play important roles to equip ourselves with a more powerful arsenal in the war against virus outbreak.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Laura Kiessling, ACS Chemical Biology Editor-in-Chief Peng Chen, ACS Chemical Biology Associate Editor Jie Wang Jie P. Li",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Complete contact information is available at: https://pubs.acs.org/10.1021/acschembio.0c00175",
            "cite_spans": [],
            "ref_spans": [],
            "section": "\u25a0 AUTHOR INFORMATION"
        },
        {
            "text": "Views expressed in this editorial are those of the authors and not necessarily the views of the ACS.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Notes"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "A new coronavirus associated with human respiratory disease in China",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "Y.-M",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Z.-G",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "Z.-W",
                    "middle": [],
                    "last": "Tao",
                    "suffix": ""
                },
                {
                    "first": "J.-H",
                    "middle": [],
                    "last": "Tian",
                    "suffix": ""
                },
                {
                    "first": "Y.-Y",
                    "middle": [],
                    "last": "Pei",
                    "suffix": ""
                },
                {
                    "first": "M.-L",
                    "middle": [],
                    "last": "Yuan",
                    "suffix": ""
                },
                {
                    "first": "Y.-L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "F.-H",
                    "middle": [],
                    "last": "Dai",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "Q.-M",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "J.-J",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "C"
                    ],
                    "last": "Holmes",
                    "suffix": ""
                },
                {
                    "first": "Y.-Z",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Nature",
            "volume": "579",
            "issn": "",
            "pages": "265--269",
            "other_ids": {
                "DOI": [
                    "10.1038/s41586-020-2008-3"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The Chinese Academy of Sciences and the University of Science and Technology recently found that these old drugs may be effective for new pneumonia",
            "authors": [],
            "year": 2020,
            "venue": "Small Tech News",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Learning from the Past: Possible Urgent Prevention and Treatment Options for Severe Acute Respiratory Infections Caused by 2019-nCoV",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Morse",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Lalonde",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "R"
                    ],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "ChemBioChem",
            "volume": "21",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1002/cbic.202000047"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Learning from the Past: Possible Urgent Prevention and Treatment Options for Severe Acute Respiratory Infections Caused by",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Morse",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Lalonde",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "R"
                    ],
                    "last": "Liu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.26434/chemrxiv.11728983.v1"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "First Case of 2019 Novel Coronavirus in the United States",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Holshue",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Debolt",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Lindquist",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "H"
                    ],
                    "last": "Lofy",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wiesman",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Bruce",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Spitters",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Ericson",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Wilkerson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Tural",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Diaz",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Cohn",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Fox",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Patel",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "I"
                    ],
                    "last": "Gerber",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Tong",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Lindstrom",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Pallansch",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "C"
                    ],
                    "last": "Weldon",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "M"
                    ],
                    "last": "Biggs",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "M"
                    ],
                    "last": "Uyeki",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "K"
                    ],
                    "last": "Pillai",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N. Engl. J. Med",
            "volume": "382",
            "issn": "",
            "pages": "929--936",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001191"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": ") Resource: from the website of school of basic medical science",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Zhihao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": ") Resource: from the website of Biomedical Pioneering Innovation Center (BIOPIC), Peking University",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Quick virus detection kits may be suitable for home use. Shine",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Meiping",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Nanopore target sequencing for accurate and comprehensive detection of SARS-CoV-2 and other respiratory viruses. medRvix",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Ming",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Remdesivir can be metabolized into its active form GS-441524 and target RdRp of coronavirus. (The picture of RdRp is regenerated from the structure of SARS-CoV nsp12 polymerase, PDB: 6NUS.) Scientists were developing the COVID-19 diagnostic kit during the Chinese New Year. Hong Kong, even provided self-made disinfectants to Hong Kong citizens according to the protocol of the WHO.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "\u25a0 FIGHTING THE COVID-19 OUTBREAK: PERSPECTIVES FROM CHEMICAL BIOLOGISTS IN CHINA Jie Wang, Synthetic and Functional Biomolecules Center (SFBC), College of Chemistry and Molecular Engineering, Peking University, Beijing 100871, China, and Jie P. Li, State Key Laboratory of Coordination Chemistry, Chemistry and Biomedicine Innovation Center (ChemBIC), School of Chemistry and Chemical Engineering, Nanjing University, Nanjing, 210023, China: The outbreak of Coronavirus 2019 (COVID-19) has become a",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}