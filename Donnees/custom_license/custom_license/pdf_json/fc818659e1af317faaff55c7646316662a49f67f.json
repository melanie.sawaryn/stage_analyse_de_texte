{
    "paper_id": "fc818659e1af317faaff55c7646316662a49f67f",
    "metadata": {
        "title": "Journal Pre-proof A call to action : the need for autopsies to determine the full extent of organ involvement associated with COVID-19 infections",
        "authors": [
            {
                "first": "Xinyang",
                "middle": [],
                "last": "Xu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Rolf",
                "middle": [
                    "F"
                ],
                "last": "Barth",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "L",
                "middle": [
                    "Maximilian"
                ],
                "last": "Buja",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "A call to action : the need for autopsies to determine the full extent of organ involvement associated with COVID-19 infections Letter to the Editor A call to action : the need for autopsies to determine the full extent of organ involvement associated with COVID-19 infections Xinyang Xu [1] ; Rolf F. Barth [2] ; L. Maximilian Buja [3] [1] Kansas City University of Medicine and Biosciences Kansas City, Missouri 64106 [2] The Ohio State University, Department of Pathology, Columbus, Ohio 43210 *Corresponding Author [3] The University of Texas Health Science Center at Houston, Department of Pathology, Houston, TX 77030",
            "cite_spans": [
                {
                    "start": 288,
                    "end": 291,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 308,
                    "end": 311,
                    "text": "[2]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 333,
                    "end": 336,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 420,
                    "end": 423,
                    "text": "[2]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 519,
                    "end": 522,
                    "text": "[3]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The purpose of this letter very simply put is a \"Call to Action\" for complete, detailed autopsies in patients who have succumbed to COVID-19 infections. There have been a number of reports in the Chinese medical literature [1] and recently in Lancet [2] describing large case series of patients who have Medicine on their findings in an 85 year old Chinese male who died following COVID-19 infection. [3] Briefly summarized, the gross autopsy revealed heavy lungs with copious amounts of gray-white viscous fluid, but otherwise the heart, liver and kidneys were unremarkable. However, no pathologic diagnoses were made and unfortunately histopathologic examination was not reported by Liu, et al [3] . Therefore, it is difficult at this time to make any more specific diagnoses than the gross findings summarized above.",
            "cite_spans": [
                {
                    "start": 223,
                    "end": 226,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 250,
                    "end": 253,
                    "text": "[2]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 401,
                    "end": 404,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 696,
                    "end": 699,
                    "text": "[3]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In contrast to the report of Liu, et al, [3] Xu, et al [4] performed \"biopsies\" of tissue from a 50 year old man who had died as a result of COVID-19 infection, and detailed information on the histopathologic, but not the gross changes seen in the lungs, heart, and liver of the decedent were described. Based on these \"biopsies\" both lungs demonstrated changes consistent with diffuse alveolar damage (DAD) and a clinical diagnosis of acute respiratory distress syndrome (ARDS). Interstitial lymphocytic infiltrates were seen in both lungs and atypical large pneumocytes and cytopathic changes consistent with a viral etiology were identified. A few interstitial mononuclear inflammatory infiltrates were seen in the heart and moderate microvesicular steatosis with mild lobular and portal activity were seen in the liver, which may not have been related to COVID-19 infection. Based on their autopsy of one decedent Liu, et al [5] stated that the fibrotic and consolidative changes were less severe than that seen in patients who had died to Sudden Acute Respiratory Syndrome (SARS) and the Middle East Respiratory Syndrome (MERS).",
            "cite_spans": [
                {
                    "start": 41,
                    "end": 44,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 55,
                    "end": 58,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 929,
                    "end": 932,
                    "text": "[5]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "However, the short course of the disease in the decedent (15 days, from diagnosis to death) does not rule out the involvement of other organ systems. As suggested by Chen, et al, [4] who reported that diarrhea was seen in COVID-19 patients, there also might have been significant GI pathology.",
            "cite_spans": [
                {
                    "start": 179,
                    "end": 182,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Gu and Korteweg have described in detail the pathologic findings associated with SARS and this could serve as a future model for a similar review for the pathology associated with COVID-19",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "infections. [5] The diverse pathology associated with SARS infections suggests that multiple organ systems probably are involved in a subset of patients who succumb to COVID-19 infections. Despite the fact that fewer and fewer autopsies are being done throughout the world, they still remain a powerful tool [4] to better understand the full scope of new and emerging diseases such as COVID-19. Based on the increasing number of deaths of patients who have succumbed to COVID-19 infections, we hope that there will be one or more reports describing in detail the autopsy findings in these decedents.",
            "cite_spans": [
                {
                    "start": 12,
                    "end": 15,
                    "text": "[5]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 308,
                    "end": 311,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The OSHA recommendation notwithstanding, the CDC has provided Interim Guidance (https://cdc.gov/coronavirus/2019-ncov/hcp/guidance-postmortem-specimens.html).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Epidemiological and clinical characteristics of 99 cases of 2019 novel corona virus pneumonia in Wuhan, China: A descriptive study",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Lancet",
            "volume": "2020",
            "issn": "10223",
            "pages": "507--513",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30211-7"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Pathological findings of COVID-19 associated with acute respiratory distress syndrome",
            "authors": [
                {
                    "first": ".",
                    "middle": [
                        "Z"
                    ],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet Respir Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/S2213-2600(20)30076-X"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Pathology and pathogenesis of severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Gu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Korteweg",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Am J Pathology",
            "volume": "170",
            "issn": "",
            "pages": "1136--1147",
            "other_ids": {
                "DOI": [
                    "10.2353/ajpath.2007.061088"
                ]
            }
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "had COVID-19 infections, and a subset of which have died as a result of their infections. A number of these decedents clinically had involvement of other organ systems besides the lungs. However, to the best of our knowledge, at this point in time, there is only one report describing pathologic findings in a single decedent who had a \"complete\" autopsy. Liu et al recently reported in the Journal of Forensic",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Kansas City University of Medicine and Biosciences Ringgold standard institution Kansas City, Missouri United States Louis Maximilian Buja University of Texas Health Science Center at Houston Ringgold standard institution Houston, Texas United States The authors have no conflicts of interest.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "We thank Mrs. Delisa Watkins for her expert assistance in the preparation of this Invited Commentary.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgement"
        }
    ]
}