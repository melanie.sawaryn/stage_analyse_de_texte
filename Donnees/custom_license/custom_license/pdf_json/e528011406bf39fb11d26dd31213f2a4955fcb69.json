{
    "paper_id": "e528011406bf39fb11d26dd31213f2a4955fcb69",
    "metadata": {
        "title": "Letter to the Editor Any possible role of phosphodiesterase type 5 inhibitors in the treatment of severe COVID19 infections? A lesson from urology",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "One of the most challenging characteristics of the novel pandemic of COVID19, is that there are various degrees of severity of symptoms, from asymptomatic to life-threatening forms.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "As recently reported by Zhang et al., considering the biochemical parameters, elevated levels of infection-related biomarkers and inflammatory cytokines (such as IL-6), neutrophilia and lymphocytopenia (as well as low CD3 + and CD4 + T-cell counts) seem to be correlated to the most severe cases of the infection. [1] In an attempt to identify some prognostic parameters, there are some clinical factors significantly correlated with higher risks of acute respiratory distress syndrome (ARDS) and death, such as being in an older age group, high fever, and comorbidities. Focusing attention on the last groups of factors, recent studies confirmed a strong correlation between the severity of infection caused by COVID19 and the presence of hypertension [2] . In our experience almost all COVID19 patients with severe ARDS are hypertensive (unpublished data).",
            "cite_spans": [
                {
                    "start": 314,
                    "end": 317,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 753,
                    "end": 756,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Several studies demonstrated a strong correlation between hypertension and Nitric Oxide (NO): a high level of blood pressure is a pathological condition characterized by endothelial dysfunction, in which NO availability is impaired with concomitant increased release of IL-6 by the dysfunctional endothelium [3, 4] Extrapolating from the previous points, COVID19 virus could determine a more severe cytokines storm (with very high levels of IL-6, but low T cells) in those patients where the basal levels of cytokines (i.e. IL-6) are higher, and NO levels are lower. This hypothesis could explain why hypertensive patients seem to be more susceptible than healthy patients to COVID19 infection.",
            "cite_spans": [
                {
                    "start": 308,
                    "end": 311,
                    "text": "[3,",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 312,
                    "end": 314,
                    "text": "4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Moreover, considering the (preliminary) reported positive effects of the off-label treatment with Tocilizumab (a humanized monoclonal antibody against IL-6 receptor adopted in the therapy of rheumatoid arthritis) in improving outcomes of severe COVID19-patients [5] , we could indicate a role of this drug in reducing the severe inflammation through the lowering of IL6. Nevertheless, the efficacy of Tocilizumab seems to be partial.",
            "cite_spans": [
                {
                    "start": 262,
                    "end": 265,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Starting from these considerations and focusing attention on the role of IL-6 and NO, we can postulate a possible role for the phosphodiesterase type 5 inhibitors (PDE5-i), such as Sildenafil Citrate or Tadalafil, in increasing the levels of NO.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Indeed, PDE5-i represents the standard medical treatment for erectile dysfunctions; nevertheless, these drugs are currently used in other diseases, such as in the management of pulmonary fibrosis or arterial pulmonary hypertension [6] . Several animal tests confirmed that NO mitigates lung injury, decreasing concentrations of proin-flammatory cytokines and reducing the migration of polymorphonuclears into the lungs [7] A role of PDE5-I in anti-viral therapy has been already tested, demonstrating an inhibiting role in the Coronavirus replication [8] .",
            "cite_spans": [
                {
                    "start": 231,
                    "end": 234,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 419,
                    "end": 422,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 551,
                    "end": 554,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Moreover, in the recent guidelines on the management of COVID19patients [9] there is no mention of this kind of drug. Considering the biochemical mechanisms involved in COVID19 infection and previous experiences justifying the off-label use of Sildenafil Citrate (or similar) as anti-viral drugs, a possible synergic role of PDE5-i as early complimentary drug in the treatment of COVID19 infection should be considered.",
            "cite_spans": [
                {
                    "start": 72,
                    "end": 75,
                    "text": "[9]",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Once the therapy efficacy has been demonstrated, more useful prophylaxis should modify the evolution of this novel pandemic disease.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "None.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Declaration of Competing Interest"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The use of anti-inflammatory drugs in the treatment of people with severe coronavirus disease 2019 (COVID-19): The experience of clinical immunologists from China",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Clin. Immunol",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/j.clim.2020.108393"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Are patients with hypertension and diabetes mellitus at increased risk for COVID-19 infection?",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Fang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Karakiulakis",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Roth",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet Respir. Med",
            "volume": "",
            "issn": "20",
            "pages": "30116--30124",
            "other_ids": {
                "DOI": [
                    "10.1016/S2213-2600(20)30116-8"
                ]
            }
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Cyclosporin-induced endothelial dysfunction and hypertension: are nitric oxide system abnormality and oxidative stress involved?",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Cal\u00f2",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Semplicini",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Davis",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Transpl. Int",
            "volume": "13",
            "issn": "",
            "pages": "413--418",
            "other_ids": {
                "DOI": [
                    "10.1007/s001470050374"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Nitric oxide and its putative role in hypertension",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "F"
                    ],
                    "last": "Dominiczak",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "F"
                    ],
                    "last": "Bohr",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Hypertension",
            "volume": "25",
            "issn": "6",
            "pages": "1202--1211",
            "other_ids": {
                "DOI": [
                    "10.1161/01.hyp.25.6.1202"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "National Institute for the Infectious Diseases \"L. Spallanzani\", IRCCS. Recommendations for COVID-19 clinical management",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Nicastri",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Petrosillo",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "A"
                    ],
                    "last": "Bartoli",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "8543. Pharmacother",
            "volume": "12",
            "issn": "1",
            "pages": "182--186",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Is There Value in Repeating Inhaled Nitric Oxide Vasoreactivity Tests in Patients with Pulmonary Arterial Hypertension?",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Tooba",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Almoushref",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Tonelli",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lung",
            "volume": "198",
            "issn": "1",
            "pages": "87--94",
            "other_ids": {
                "DOI": [
                    "10.1007/s00408-019-00318-0"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Effects of phosphodiesterase 5 inhibitor sildenafil on the respiratory parameters, inflammation and apoptosis in a saline lavage-induced model of acute lung injury",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Kosutova",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Mikolka",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Balentova",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "J. Physiol. Pharmacol",
            "volume": "69",
            "issn": "5",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.26402/jpp.2018.5.15"
                ]
            }
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Nitric oxide inhibits the replication cycle of severe acute respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Akerstr\u00f6m",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Mousavi-Jazi",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Klingstr\u00f6m",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Leijon",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Lundkvist",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mirazimi",
                    "suffix": ""
                }
            ],
            "year": 1966,
            "venue": "J. Virol",
            "volume": "79",
            "issn": "3",
            "pages": "1966--1969",
            "other_ids": {
                "DOI": [
                    "10.1128/JVI.79.3.1966-1969"
                ]
            }
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Surviving Sepsis Campaign: guidelines on the management of critically ill adults with Coronavirus Disease 2019 (COVID-19)",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Alhazzani",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "H"
                    ],
                    "last": "M\u00f8ller",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "M"
                    ],
                    "last": "Arabi",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Intensive Care Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1007/s00134-020-06022-5"
                ]
            }
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Piazzale Santa Maria della Misericordia 15",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "33100",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": [
        {
            "text": "None.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgements"
        }
    ]
}