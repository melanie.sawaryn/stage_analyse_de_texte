{
    "paper_id": "a13d0cfa3e6ff5850483c2e1a6e0d22d47064fa2",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [
        {
            "text": "publicly funded repositories, such as the WHO COVID database with rights for unrestricted research re-use and analyses in any form or by any means with acknowledgement of the original source. These permissions are granted for free by Elsevier for as long as the COVID-19 resource centre remains active. 0.75 as antigenically similar (Qiu et al., 2018) , these results indicate the existence of potential CREs between 2019-nCoV and SARS virus.",
            "cite_spans": [
                {
                    "start": 333,
                    "end": 351,
                    "text": "(Qiu et al., 2018)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Letter to the editor Identification of potential cross-protective epitope between a new type of coronavirus (2019-nCoV) and severe acute respiratory syndrome virus Recently, a new type of unknown virus causing severe acute respiratory infection was reported in Wuhan city, Hubei province, China (WHO, 2020b) . Infection of this virus was first reported in December 2019, and origin of the virus was traced back to a large seafood/wide animal market in Wuhan city. The serious clinical symptoms of the viral infection, including fever, dry cough, dyspnea, and pneumonia, may result in progressive respiratory failure and even death. Moreover, the quick spread of the virus has caused an epidemic in China, as well as infection cases worldwide. The whole-genome sequence of Wuhan new virus (WH-Human_1) was first released on January 10, 2020 (Zhang, 2020) , followed by additional ones released in Global Initiative on Sharing All Influenza Data (GISAID) (Shu and McCauley, 2017) . Later, this new virus was determined and announced as a new type of coronavirus (CoV; 2019-nCoV) by the World Health Organization (WHO, 2020a).",
            "cite_spans": [
                {
                    "start": 295,
                    "end": 307,
                    "text": "(WHO, 2020b)",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 840,
                    "end": 853,
                    "text": "(Zhang, 2020)",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 953,
                    "end": 977,
                    "text": "(Shu and McCauley, 2017)",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "CoVs are single-stranded RNA viruses that belong to the order Nidovirales, family Coronaviridae, and subfamily Coronavirinae (Schoeman and Fielding, 2019) and have been classified into four major groups: a-CoVs, b-CoVs, g-CoVs, and d-CoVs with 17 subtypes (Saminathan et al., 2014) . CoVs primarily infect wild animals including mammals and birds. They also infect humans and cause various diseases such as upper and lower respiratory tract infections and respiratory syndromes. Among them, severe acute respiratory syndrome (SARS) CoV and Middle Eastern respiratory syndrome (MERS) CoV can cause serious respiratory syndrome in humans (Schoeman and Fielding, 2019) . For instance, the outbreak of SARS in 2003 led to a pandemic with 8906 infected cases and 774 deaths reported worldwide (WHO, 2003) . Meanwhile, the outbreak of MERS confirmed 2229 cases globally, including 791 associated deaths (WHO, 2018) .",
            "cite_spans": [
                {
                    "start": 125,
                    "end": 154,
                    "text": "(Schoeman and Fielding, 2019)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 256,
                    "end": 281,
                    "text": "(Saminathan et al., 2014)",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 636,
                    "end": 665,
                    "text": "(Schoeman and Fielding, 2019)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 788,
                    "end": 799,
                    "text": "(WHO, 2003)",
                    "ref_id": null
                },
                {
                    "start": 897,
                    "end": 908,
                    "text": "(WHO, 2018)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The coronaviral genome normally encodes four structural proteins including spike (S) protein, nucleocapsid (N) protein, membrane (M2) protein, and envelope (E) protein (Schoeman and Fielding, 2019) . S protein contains the receptor-binding domain (RBD) and mediates the attachment of viruses to the surface receptors in host cells, as well as subsequent fusion between the viral and host cell membranes, to facilitate viral entry into host cells (Kirchdoerfer et al., 2016) . Multiple binding and neutralization epitopes have been identified in the S proteins of CoVs (Hwang et al., 2006; Prabakaran et al., 2006; Reguera et al., 2012) , which makes S protein an essential antigen for vaccine design.",
            "cite_spans": [
                {
                    "start": 168,
                    "end": 197,
                    "text": "(Schoeman and Fielding, 2019)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 446,
                    "end": 473,
                    "text": "(Kirchdoerfer et al., 2016)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 568,
                    "end": 588,
                    "text": "(Hwang et al., 2006;",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 589,
                    "end": 613,
                    "text": "Prabakaran et al., 2006;",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 614,
                    "end": 635,
                    "text": "Reguera et al., 2012)",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Latest bioinformatic analysis indicated that 2019-nCoV is phylogenetically close to SARS-CoV and bat CoV (BCoV) (Xu et al., 2020) . The genomes of 2019-nCoV and SARS-CoV share more than 79% sequence similarity on average , and their S proteins share 76.47% identity (Xu et al., 2020 ). Yet the antigenicity similarity between them remains unknown and is urgently needed for vaccine design. Cross-reactive epitopes (CREs) are shared or similar epitope regions on the antigen surface among viruses that can be bound or neutralized by the same antibodies. Desirably, if any CREs were identified, previous antibodies for other CoVs might be reused to facilitate 2019-nCoV intervention.",
            "cite_spans": [
                {
                    "start": 112,
                    "end": 129,
                    "text": "(Xu et al., 2020)",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 266,
                    "end": 282,
                    "text": "(Xu et al., 2020",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Previously, we have developed an algorithm, namely, Conformational Epitope (CE)-BLAST, which enables antigenic similarity computation for new emerging pathogens, and have used it to successfully identify CREs between the dengue and Zika viruses (Qiu et al., 2018) . In this study, we investigated the antigenic similarity of 2019-nCoV to other CoVs based on their spike antigens.",
            "cite_spans": [
                {
                    "start": 245,
                    "end": 263,
                    "text": "(Qiu et al., 2018)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Sequences of S protein were downloaded for known CoVs from UniProt, and 2019-nCoV sequences were obtained from Shanghai Public Health Clinical Center and GISAID (Supplementary data). After data processing, a total of 53 unique S proteins were selected and structure modeled which represent different subtypes of CoVs, including 2019-nCoV (WH-Human_1), 3 SARS strains, 2 BCoV strains, and 47 strains from other CoVs (Table S1 ). S proteins of 2019-nCoV and SARS strains share high structural similarity with a root-mean-square deviation of 1.21 \u00c5 according to modeling structures. Individual epitope residues were derived from immune complexes of CoV S protein and further merged into 6 epitope regions (Table S2) . Mapping the epitope regions to the 3D structure of S protein of 2019-nCoV demonstrated that five epitope regions are located in the RBD (Fig. S1 ). For each region, CE-BLAST calculates the similarity score of antigenicity between CoV pairs by comparing the physicochemical difference in 3D adjacent structural regions (Qiu et al., 2018) . The higher the score, the better the potential for cross-reaction between the paired antigens.",
            "cite_spans": [
                {
                    "start": 1033,
                    "end": 1051,
                    "text": "(Qiu et al., 2018)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [
                {
                    "start": 415,
                    "end": 424,
                    "text": "(Table S1",
                    "ref_id": null
                },
                {
                    "start": 702,
                    "end": 712,
                    "text": "(Table S2)",
                    "ref_id": null
                },
                {
                    "start": 851,
                    "end": 859,
                    "text": "(Fig. S1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "The antigenic clustering of the 53 CoV strains was made according to the score matrix for each epitope region (Figs. 1 and S2 ). It can be seen that, although the clustering results may vary slightly for different regions, the antigenicity of CoV S proteins can be generally divided into two major groups. Detailed results for the 6th epitope region shown in Fig. 1A and B displayed the detailed clustering of the 2019-nCoV group, as well as the similarity score between them. Antigenically, 2019-nCoV is most similar to SARS-CoV, followed by BCoV (Fig. 1B) . Similarity scores higher than 0.80 were detected between 2019-nCoV and SARS-CoV strains. Considering the default cutoff of Further structural mapping showed the potential CREs in the S protein of 2019-nCoV (Fig. 1C) and SARS virus (Fig. 1D ). Fig. 1E shows the multiple sequence alignment (MSA) for 2019-nCoV and SARS strains. The MSA results of full sequences can be found in Fig. S3 . The potential CREs are conformational epitopes, in which the component residues are close in 3D space but disconnected in protein sequence (Fig. 1E) . Compared with SARS virus, most mutations at CRE sites are residual substitutions of amino acids with similar properties, such as changes of alkaline residue Lys (K) to Arg (R) and aromatic residue Phe (F) to Trp (W).",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 110,
                    "end": 125,
                    "text": "(Figs. 1 and S2",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 359,
                    "end": 366,
                    "text": "Fig. 1A",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 548,
                    "end": 557,
                    "text": "(Fig. 1B)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 766,
                    "end": 775,
                    "text": "(Fig. 1C)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 791,
                    "end": 799,
                    "text": "(Fig. 1D",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 803,
                    "end": 810,
                    "text": "Fig. 1E",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 937,
                    "end": 944,
                    "text": "Fig. S3",
                    "ref_id": null
                },
                {
                    "start": 1086,
                    "end": 1095,
                    "text": "(Fig. 1E)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "Recently, the binding sites of S protein to human ACE2 were identified as residues 455, 486, 493, 501, and 505 (numbered according to the 2019-nCoV S protein sequence), which are located near the potential CRE positions (Fig. 1E) . Interestingly, although no direct overlapping, the potential CRE residues are highly adjacent to essential ACE2-binding sites in the 3D structure Xu et al., 2020) , suggesting that neutralization antibodies targeting CRE may also interfere or even block the interaction between the new CoV and ACE2 receptor because of steric hindrance (Fig. 1F ). As such, the CRE region recommended here may likely become the cross-protective epitope between 2019-nCoV and SARS-CoV.",
            "cite_spans": [
                {
                    "start": 378,
                    "end": 394,
                    "text": "Xu et al., 2020)",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [
                {
                    "start": 220,
                    "end": 229,
                    "text": "(Fig. 1E)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 568,
                    "end": 576,
                    "text": "(Fig. 1F",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "In this study, we used the computational method CE-BLAST to identify the potential CREs between 2019-nCoV and SARS-CoV. CE-BLAST requires epitope structures as the input file. Epitopes are usually referred to as highly specific and continuous areas on the surface of an antigen which can be recognized and bound by corresponding antibodies. Previous analysis on antibody-antigen complexes found that more than 90% of epitope residues appeared conformational (Van Regenmortel, 1996) . As conformational epitopes can be influenced by various factors such as mutation/insertion/deletion, structure change, or neighboring mutations, examining sequence conservation alone is usually not enough to evaluate the similarity of epitope regions among viruses.",
            "cite_spans": [
                {
                    "start": 458,
                    "end": 481,
                    "text": "(Van Regenmortel, 1996)",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In addition to the epitope similarity between 2019-nCoV and SARS-CoV, we also examined the location and residual distribution for each epitope region in S protein of 2019-nCoV. As Fig. S1 indicates, epitope region 3 is located in S1 domain, which is far from the important ACE2-binding site. For the rest 5 regions, although located in the RBD, some epitope residues become spatially scattered, rather than maintaining the epitope conformation, such as the case in region 1. Thus, region 6 was recommended here for CRE candidate because of its high score, surface continuity, and proximity to essential binding sites of ACE2. Possibility of an additional CRE may also exist in other regions of S protein or other antigens between 2019-nCoV and SARS-CoV. It is noted that these potential CREs are highly conformational. Following antibody design and screening is suggested to consider the whole domain as antibodies derived from linear epitopes might have difficulty in fully recognizing the CRE structures.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 180,
                    "end": 187,
                    "text": "Fig. S1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "In summary, a highly similar epitope was identified computationally between the 2019-nCoV and SARS virus, in the region of the binding site of the S proteins to the human ACE2 receptor. This timely work may shed light on the vaccine intervention for the emergent 2019-nCoV. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Structural basis of neutralization by a human anti-severe acute respiratory syndrome spike protein antibody, 80R",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "C"
                    ],
                    "last": "Hwang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Santelli",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Sui",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Jaroszewski",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Stec",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farzan",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "A"
                    ],
                    "last": "Marasco",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J. Biol. Chem",
            "volume": "281",
            "issn": "",
            "pages": "34610--34616",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Pre-fusion structure of a human coronavirus spike protein",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "N"
                    ],
                    "last": "Kirchdoerfer",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "A"
                    ],
                    "last": "Cottrell",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Pallesen",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "M"
                    ],
                    "last": "Yassine",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "L"
                    ],
                    "last": "Turner",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "S"
                    ],
                    "last": "Corbett",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "S"
                    ],
                    "last": "Graham",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Mclellan",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "B"
                    ],
                    "last": "Ward",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Nature",
            "volume": "531",
            "issn": "",
            "pages": "118--121",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Structure of severe acute respiratory syndrome coronavirus receptorbinding domain complexed with neutralizing antibody",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Prabakaran",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Gan",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Feng",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Choudhry",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Xiao",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Ji",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "S"
                    ],
                    "last": "Dimitrov",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J. Biol. Chem",
            "volume": "281",
            "issn": "",
            "pages": "15829--15836",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "CE-BLAST makes it possible to compute antigenic similarity for newly emerging pathogens",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Qiu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Qiu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Xiao",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Tang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Cao",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Nat. Commun",
            "volume": "9",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Structural bases of coronavirus attachment to host aminopeptidase N and its inhibition by neutralizing antibodies",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Reguera",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Santiago",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Mudgal",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ordo\u00f1o",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Enjuanes",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Casasnovas",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "PLoS Pathog",
            "volume": "8",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Coronavirus infection in equines: a review",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Saminathan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Chakraborty",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Tiwari",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Dhama",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Verma",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Asian J. Anim. Vet. Adv",
            "volume": "9",
            "issn": "",
            "pages": "164--176",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Coronavirus envelope protein: current knowledge",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Schoeman",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "C"
                    ],
                    "last": "Fielding",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Virol. J",
            "volume": "16",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "GISAID: global initiative on sharing all influenza datafrom vision to reality",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Shu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Mccauley",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Euro Surveill",
            "volume": "22",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Mapping epitope structure and activity: from onedimensional prediction to four-dimensional description of antigenic specificity",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "H V"
                    ],
                    "last": "Van Regenmortel",
                    "suffix": ""
                }
            ],
            "year": 1996,
            "venue": "Methods",
            "volume": "9",
            "issn": "",
            "pages": "465--472",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "WHO, 2020a. Novel Coronavirus-China",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Pneumonia of Unknown Cause in China",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Who",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Summary of Probable SARS Cases with Onset of Illness from 1",
            "authors": [],
            "year": 2002,
            "venue": "WHO",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "WHO MERS-CoV Global Summary and Assessment of Risk",
            "authors": [],
            "year": 2018,
            "venue": "WHO",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Evolution of the novel coronavirus from the ongoing Wuhan outbreak and modeling of its spike protein for risk of human transmission",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Feng",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Zhong",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Hao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Sci. China Life Sci. 1e4",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1007/s11427-020-1637-5"
                ]
            }
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Initial Genome Release of Novel Coronavirus",
            "authors": [
                {
                    "first": "Y",
                    "middle": [
                        "Z"
                    ],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "A pneumonia outbreak associated with a new coronavirus of probable bat origin",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1038/s41586-020-2012-7"
                ]
            }
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Chinese Academy of Medical Science & Peking Union Medical College, Beijing, 100730, China Jianqing Xu ** Shanghai Public Health Clinical Center",
            "authors": [
                {
                    "first": "Tiantian",
                    "middle": [],
                    "last": "Mao 1",
                    "suffix": ""
                },
                {
                    "first": "Yuan",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Mengdi",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Biology of Pathogens and Christophe M erieux Laboratory, IPB, CAMS-Fondation M erieux, Institute of Pathogen Biology (IPB)",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Antigenic clustering and potential CREs between 2019-nCoV and SARS. A: Antigenic clustering of S proteins for 53 representative CoVs based on the 6th epitope regions. One cluster is marked in cyan, and the other is marked in pink. The 2019-nCoV is marked as WH Human1-2019-nCoV here. B: Details in the 2019-nCoV cluster with score matrix of antigenic similarity. C: Position mapping of potential CREs on the modeled structure of 2019-nCoV (WH-Human1-2019-nCoV). D: Position mapping of potential CREs on the modeled structure of SARS-CoV B039 (Q4JDN3). E: Sequence alignment of S proteins for 2019-nCoV and SARS-CoVs. Epitope regions are highlighted in red, and five essential human ACE2-binding sites are highlighted in blue. Sites are numbered based on the S protein sequence of 2019-nCoV. Dot in the sequence of SARS-CoV represents gap insertion. F: Surface view of CREs and ACE2-binding sites. CREs are marked in red, and ACE2-binding sites are marked in cyan. CRE, cross-reactive epitope; CoV, coronavirus; SARS, severe acute respiratory syndrome.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "We would like to thank Dr. Yongzhen Zhang from Shanghai Public Health Clinical Center, Fudan University, Shanghai, China, and Dr. Zhengli Shi from the Wuhan Institute of Virology, Chinese Academy of Sciences, Wuhan, China, for providing the genome sequence of 2019-nCoV collected from Wuhan, China. This work is supported in part by grants from National Key R&D Program of China (2017YFC0908400, 2017YFC1700200) and National Natural Science Foundation of China (31900483).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgments"
        },
        {
            "text": "Supplementary data to this article can be found online at https://doi.org/10.1016/j.jgg.2020.01.003.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Supplementary data"
        }
    ]
}