{
    "paper_id": "49a26ea1e9f6e2bae20ffddf17774d696114fff8",
    "metadata": {
        "title": "Epidemie\u00ebn zijn moeilijk beheersbaar",
        "authors": [
            {
                "first": "",
                "middle": [],
                "last": "Galama",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Arts-microbioloog, hoogleraar virologie, afdeling Medische microbiologie, UMC St Radboud, Nijmegen",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "Halverwege de vorige eeuw had men een groot vertrouwen in de beheersbaarheid van infectieziekten. Maar nu, veertig jaar later, is het optimisme omgeslagen in bezorgdheid voor de toekomst. Hoe terecht is dat? In dit inleidend hoofdstuk wordt kort ingegaan op begrippen als 'uitbraak' en zo\u00f6nose, de historie van pestilentie\u00ebn, de risicofactoren en de manier waarop risicofactoren zouden kunnen worden aangepakt. Ziekten als pest, pokken en mazelen worden genoemd als voorbeeld van pestilentie\u00ebn die min of meer onder controle zijn gebracht, en dengue als opkomende ziekte waartegen nog geen passend antwoord is gevonden.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Toen ruim dertig jaar geleden aids uitbrak, was ik nog in opleiding. Er ontspon zich op het lab een levendige discussie over de mogelijke oorzaak. In eerste instantie werd gedacht aan drugs; de poppers die toen 'in' waren en mogelijk de afweer zouden aantasten. Sommige onderzoekers binnen het lab meenden dat een bijzondere variant van het cytomegalovirus in het spel was. Het cytomegalovirus was immers naast Pneumocystis jirovecii een van de eerst herkende verwekkers van een opportunistische infectie. Dat het om een geheel nieuw virus zou gaan, kwam niet direct bij ons op. Het pokvirus was uit-geroeid en het poliovirus zou weldra volgen. Dankzij het Rijksvaccinatieprogramma behoorden ook bof, mazelen en rubella tot het verleden. Er was groot vertrouwen in de beheersbaarheid van infectieziekten. Maar nu, 30-40 jaar later, is het optimisme omgeslagen in bezorgdheid voor de toekomst. Maar hoe terecht is dat? In dit inleidend hoofdstuk wil ik kort ingaan op begrippen als 'uitbraak' en zo\u00f6 nose, de historie van pestilentie\u00ebn, de risicofactoren en de manier waarop deze zouden kunnen worden aangepakt.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Inleiding"
        },
        {
            "text": "Het woord 'uitbraak' of in het Engels 'outbreak' staat voor 'a sudden occurrence of war or disease' (Oxford Dictionary). Het begrip komt gedeeltelijk overeen met het begrip epidemie: 'een besmettelijke ziekte die zich snel uitbreidt'. Dit woord zal hier verder worden gebruikt. Of er sprake is van een epidemie, wordt voor verschillende ziekten anders gedefinieerd: voor influenza geldt een incidentie van vijf nieuwe gevallen per 10,000 personen per week als limiet waarboven sprake is van een epidemie. Voor de aidsepidemie, die veel geleidelijker verloopt, is een dergelijk criterium niet zinvol.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Begrippen"
        },
        {
            "text": "Naast de epidemie kennen we de endemische situatie, waarbij een infectie in een gemeenschap op een laag niveau aanwezig blijft, maar ook deze situatie kan gevolgd worden door een plotselinge toename. Men spreekt dan van een endemische ziekte met epidemische verheffingen. Mazelen is hiervan het klassieke voorbeeld. Of een infectieziekte endemisch is, hangt samen met de populatiegrootte, de geboorteaanwas en de besmettelijkheid van de ziekte. Mazelen is zeer besmettelijk en kan zich al handhaven bij een populatiegrootte van circa 200.000 inwoners. De ziekte werd pas endemisch toen er steden van een dergelijke omvang ontstonden. 1 Voordien was mazelen een importziekte, zoals ook nu in Nederland weer het geval is; mazelen is in Nederland door inenting niet langer endemisch.",
            "cite_spans": [
                {
                    "start": 634,
                    "end": 635,
                    "text": "1",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Begrippen"
        },
        {
            "text": "Een zo\u00f6 nose is een infectie bij de mens, waarbij een dier als natuurlijke gastheer en reservoir fungeert -in feite betreft het transmissie tussen soorten en ook transmissie van mens naar dier is een zo\u00f6 nose. De humane infectie kan daarbij als incident worden opgevat. Gevaarlijker wordt het wanneer de infectie vervolgens van mens op mens overdraagbaar is. Dan is er niet langer sprake van een zo\u00f6 nose in engere zin. 2 Een voorbeeld van een zo\u00f6 nose in engere zin is de huidige Q-koortsepidemie, waarbij nauwelijks of geen transmissie van mens op mens wordt waargenomen, terwijl het SARS-coronavirus tijdens de epidemie in 2003 wel door mensen werd overgedragen.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Begrippen"
        },
        {
            "text": "Ten slotte zijn er nog twee actuelere begrippen: opkomende infecties veroorzaakt door nieuwe pathogenen en bekende infecties die opnieuw opduiken (emerging and re-emerging infections). Van het laatste zijn tuberculose en dengue voorbeelden (zie later in dit artikel).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Begrippen"
        },
        {
            "text": "Van de bijbelse plagen in Egypte onder het regime van Ramses II (1297-1213 voor Christus) is weinig bekend. Deels betrof het natuurrampen en geen infecties. E\u00e9n van de vroegst opgetekende epidemie\u00ebn is de plaag van Athene in 430-426 voor Christus. 3 Deze plaag zou zijn oorsprong hebben in Ethiopi\u00eb en zich stroomafwaarts langs de Nijl tot aan de Middellandse zee verspreid hebben, waarna de ziekte vermoedelijk per schip Piraeus bereikte. De omstandigheden waren zeer ongunstig: de Peloponnesische oorlog leidde tot grote mensenmassa's binnen de muren en een slechte hygi\u00ebne. Een derde van de bevolking stierf. Volgens Thucydides was iedereen ontvankelijk voor de ziekte, liet deze blijvende immuniteit na en verspreidde de ziekte zich ook naar andere Griekse steden. De Atheners waren door de epidemie zo verzwakt dat zij de verloren hegemonie niet meer hebben kunnen heroveren. Hiermee vormt deze verslaglegging de eerste specifieke beschrijving van een epidemie, waarbij bovendien geen rol werd toebedeeld aan de goden. Ook illustreert dit verslag de betekenis van pestilentie\u00ebn voor de loop van de geschiedenis. Ondanks vele speculaties is de verwekker nog onbekend. 4 In China is een groot aantal (290) epidemieen opgetekend over een periode van ruim 2000 jaar. 5 Voor sommige van deze epidemieen was een schatting van de sterfte vermeld, soms met hoge sterftecijfers tot boven de 50%. Een hoge sterfte duidt doorgaans op introductie van een nieuwe verwekker waartegen de bevolking geen weerstand heeft opgebouwd, maar vaak wordt de sterfte sterk be\u00efnvloed door sociale factoren als oorlog, hongersnood en overbevolking. 5, 6 De pest en de pokken genieten de twijfelachtige eer van meest dodelijke pestilentie\u00ebn ooit. In ieder geval waren ze het meest gevreesd. Dit heeft geleid tot indrukwekkende monografie\u00ebn. [7] [8] [9] De pest is waarschijnlijk afkomstig uit China, waar het ziektebeeld rond 1330 was gerapporteerd. De ziekte heeft het westen bereikt via Mongoolse invasies en via handelsroutes, zoals de zijderoute en de vele scheepvaartroutes. De periode 1347-1350 vormde het hoogtepunt.",
            "cite_spans": [
                {
                    "start": 1172,
                    "end": 1173,
                    "text": "4",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1268,
                    "end": 1269,
                    "text": "5",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1627,
                    "end": 1629,
                    "text": "5,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1630,
                    "end": 1631,
                    "text": "6",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1818,
                    "end": 1821,
                    "text": "[7]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1822,
                    "end": 1825,
                    "text": "[8]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1826,
                    "end": 1829,
                    "text": "[9]",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Epidemie\u00ebn in het verleden"
        },
        {
            "text": "De pest is een infectieziekte van overvolle steden, en slechte hygi\u00ebnische omstandigheden die gunstig zijn voor ratten. De zwarte rat was de verspreider van de zwarte dood. Nadat de zwarte rat door de succesvollere bruine rat werd verdrongen, werd de pest steeds zeldzamer en meer en meer een ziekte van havensteden, waar de zwarte rat zich beter kon handhaven. De pest veroorzaakte geen pandemie, maar bleef beperkt tot Europa en delen van Azi\u00eb.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "De pest"
        },
        {
            "text": "Het pokvirus is afkomstig van apen of runderen in Afrika of Azi\u00eb en is meer dan 5000 jaar geleden met succes naar de mens overgestapt. Rond 12.000 jaar geleden werd voor het eerst vee gedomesticeerd; ziekten als de pokken en mazelen (zie hierna) beschouwt men als een rechtstreeks gevolg van het samenleven van mens en dier. Ramses V (jong gestorven in 1157 voor Christus) wordt beschouwd als \u00e9\u00e9n van de vroegst bekende slachtoffers van pokken (figuur 1).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Pokken"
        },
        {
            "text": "De term vaccinatie is afgeleid van inenting met het vacciniavirus (koepokvirus). Het vormde de vroegste vorm van inenting met een verminderd virulent virus (Jenner 1798). Door scheepvaart zijn de pokken uiteindelijk over de gehele wereld verspreid. Omgekeerd is de ziekte dankzij vaccinatie uitgeroeid, maar tot nu toe is dit de enige ernstige infectieziekte -voor andere infectieziekten is uitroeien nog niet gelukt. In 1978 heeft de WHO de wereld officieel pokkenvrij verklaard.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Pokken"
        },
        {
            "text": "Van de pest en de pokken zijn tevens de oudste voorbeelden van bioterrorisme bekend: tijdens de belegering van steden door de mongolen (de pest) en tijdens de verovering van Amerika op de indianen (de pokken). 6 Mazelen Minder bekend is de ernst van mazelen. Het mazelenvirus is verwant aan het runderpestvirus en waarschijnlijk, evenals de pokken, na domesticatie van vee op de mens overgegaan. Mazelen is \u00e9\u00e9n van de besmettelijkste ziekten die we kennen, grofweg tienmaal besmettelijker dan influenza. Na een incubatietijd van twee weken en een korte duur van besmette-lijkheid volgt de ziekte, waarna men levenslang immuun is. Hierdoor kan mazelen zich binnen een kleine gemeenschap maar kort handhaven. In de geschiedenis is mazelen dan ook lang een importziekte geweest die met grote intervallen terugkeerde onder een inmiddels weer grotendeels immunologisch na\u00efeve populatie. In het Londen van de 18e eeuw was mazelen even gevreesd als de pokken. 10 Voorbeelden van recenter datum vormen mazelenepidemie\u00ebn op de Fiji-eilanden (1875) en op Groenland (1951). Tijdens deze epidemie\u00ebn raakte meer dan 99% van de bevolking ge\u00efnfecteerd en liep de sterfte op tot meer dan 40%. 6, 11 Nog steeds is mazelen in de tropen een gevreesde ziekte die tot het eind van de vorige eeuw in de top-10 van dodelijke infectieziekten stond. Mazelen is dan ook de belangrijkste dodelijke infectieziekte die door inenting te voorkomen is.",
            "cite_spans": [
                {
                    "start": 210,
                    "end": 211,
                    "text": "6",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 953,
                    "end": 955,
                    "text": "10",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1177,
                    "end": 1179,
                    "text": "6,",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1180,
                    "end": 1182,
                    "text": "11",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Pokken"
        },
        {
            "text": "Met de groei van de steden werd mazelenin westerse landen -geleidelijk aan endemisch, met regelmatig (eens in de 2-4 jaar) terugkerende epidemische verheffingen die ervoor zorgden dat kinderen al op jonge leeftijd de ziekte opliepen. Een groeiende welvaart, gezondere voeding, betere leefomstandigheden en de jonge leeftijd waarop mazelen werd doorgemaakt, hebben bijgedragen aan het relatief milde karakter van de kinderziekte, zoals de ouderen onder ons zich dat herinneren. Maar dat is niet altijd zo geweest.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Pokken"
        },
        {
            "text": "De keuze voor deze drie infectieziekten is betrekkelijk willekeurig. Er zijn tal van andere epidemie\u00ebn waarvan de geschiedenis boeiend is, maar deze komen in dit bestek niet aan de orde. Een aantal ervan staat vermeld in tabel 1.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Pokken"
        },
        {
            "text": "De mens speelt bij epidemie\u00ebn een centrale rol, welke onder meer wordt beschreven in het boek The social ecology of infectious diseases. 12 De belangrijkste door menselijk gedrag veroorzaakte risicofactoren staan opgesomd in tabel 2.",
            "cite_spans": [
                {
                    "start": 137,
                    "end": 139,
                    "text": "12",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "Risicofactoren"
        },
        {
            "text": "Twee factoren zijn hierbij dominant: de snelle bevolkingsgroei die min of meer samenvalt met welvaartsgroei en afname van kindersterfte, en het optimisme over de beheersbaarheid van infectieziekten dat in de zestiger en zeventiger jaren van de vorige eeuw heeft geleid tot een drastische ombuiging van preventieve naar curatieve zorg. Hierdoor werd een zorgvuldig opgebouwd systeem van infectiepreventie ernstig verwaarloosd en deels afgebroken. Dit geldt in het bijzonder voor de grote steden in de derde wereld. Door de globalisering is het probleem dichter bij huis dan men zich doorgaans realiseert. Een derde belangrijke factor is gelegen in contact met dieren: zo\u00f6 nosen vormen de belangrijkste bron voor opduikende nieuwe infecties, maar voor dit laatste aspect verwijs ik naar de literatuur. 2, 5, 13 In het boek Plagues and people beschrijft de historicus William McNeill hoe culturele ontwikkelingen het patroon van infectieziekten sterk hebben be\u00efnvloed. 5 Zo heeft een omschakeling plaatsgevonden van het bestaan als zwerver-verzamelaar naar een sedentair bestaan als boer en vervolgens als handwerksman in centra van nijverheid en handel: de steden. Bij iedere bestaanswijze hoorde een patroon van infectieziekten. De steden waren overbevolkt en de hygi\u00ebnische omstandigheden slecht. De steden raakten onderling verbonden door handelsroutes (zijderoute, scheepvaartroutes), waarlangs uitwisseling van goederen tot stand kwam. Dit leidde ook tot uitwisseling van infectieziekten. Hierdoor werden steden vaak de brandhaard van waaruit infecties zich in de omgeving konden verspreiden, hetgeen ook nu nog actueel is. 1 Reistijden en reisomstandigheden bepaalden vaak welke infectieziekten effici\u00ebnt verspreid werden: acute infectieziekten met een korte incubatietijd en een korte periode van besmettelijkheid verspreidden zich minder ver dan bijvoorbeeld het pokvirus dat langdurig besmettelijk blijft. De korte reistijden van nu hebben dergelijke belemmeringen nagenoeg opgeheven. Inmiddels woont meer dan de helft van de wereldbevolking in steden en verstedelijkte gebieden (tabellen 3 en 4), waarvan de grootste met meer dan tien miljoen inwoners doorgaans eigen vliegvelden hebben. Een expansieve groei van deze steden treft men vooral aan in arme landen, waar armoede op het platteland de migratie naar steden bevordert. Dit leidt veelal tot het ontstaan van sloppenwijken zonder infrastructuur, watervoorziening en sanitair, kortom tot hygi\u00ebnische misstanden.",
            "cite_spans": [
                {
                    "start": 800,
                    "end": 802,
                    "text": "2,",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 803,
                    "end": 805,
                    "text": "5,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 806,
                    "end": 808,
                    "text": "13",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 966,
                    "end": 967,
                    "text": "5",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Risicofactoren"
        },
        {
            "text": "Dengue is een uitstekend voorbeeld van een opkomende infectieziekte die gedijt bij urbanisatie en die buiten verstedelijkte gebieden relatief weinig problemen veroorzaakt. 14 Dengue wordt veroorzaakt door het denguevirus (DENV), waarvan vier serotypen bestaan (DENV 1-4) welke afzonderlijk in staat zijn om een infectie te veroorzaken. DENV wordt overgebracht door de Aedes aegypti, waarvan het leefpatroon verregaand is aangepast aan een stedelijke omgeving en aan de mens als bloedbron. Andere aedesspecies, zoals de tijgermug, kunnen eveneens als vector fungeren, maar het specifieke levenspatroon maakt dat de A. aegypti de belangrijkste vector is en dat dengue een ziekte is van verstedelijkte gebieden. Een vochtig tropisch of subtropisch klimaat, overbevolking en stilstaand water, in autobanden, potten en pannen, vormen de ideale biotoop waarbij de mug gedijt (figuur 2). In de 17e eeuw kwam dengue sporadisch voor, bijvoorbeeld in Batavia (1779) en doofden de haarden vervolgens weer uit. Tijdens de Tweede Wereldoorlog hebben troepen-en goederentransporten bijgedragen aan een mondiale verspreiding van diverse DENV-serotypen, onder andere naar Zuid-Amerika. De ongekend snelle economische groei en bevolkingsaanwas in Azi\u00eb hebben ertoe geleid dat de vier DENV-serotypen zich vanaf de zeventiger jaren over heel Azi\u00eb hebben verspreid, waardoor nu op veel plaatsen gelijktijdig meerdere serotypen circuleren. Verstedelijking, bevolkingsgroei en migratie naar verstedelijkte gebieden hebben gemaakt dat dengue nu op veel plaatsen endemisch is geworden. Wanneer meerdere serotypen gelijktijdig circuleren, spreekt men van een hyperendemische situatie. Herinfectie met een tweede serotype kan een ernstig beloop veroorzaken met bloedingen en shock: dengue hemorrhagic fever (DHF) en het dengue shock syndrome (DSS). In hyperendemische gebieden is dengue dan ook een gevaarlijke ziekte. Inmiddels is dengue op vele plaatsen in de wereld hyperendemisch en vormt het de belangrijkste vectoroverdraagbare virusinfectie, met 50-100 miljoen infecties per jaar, waarvan 500.000 verlopen als DHF/DSS met ongeveer scherming een averechts effect kan hebben doordat het juist hemorragische complicaties bevordert. In een welvarende stad als Singapore heeft men dan ook lange tijd geprobeerd om dengue te bestrijden door een strikte controle op het v\u00f3 \u00f3 rkomen van stilstaand water waarin de A. aegypti kan broeden. Zo werden platte daken, houders voor vlaggenstokken, tuinsproeiers enzovoort gecontroleerd. Overtredingen werden zwaar beboet. Uiteindelijk bleek het succes van tijdelijke aard en is na verloop van jaren dengue opnieuw in Singapore uitgebroken, zelfs in een ernstiger vorm dan tevoren. De tijdelijke onderbreking van viruscirculatie bleek een averechtse werking te hebben, waarvoor een sluitende verklaring nog ontbreekt.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Risicofactoren"
        },
        {
            "text": "In welvarende landen wordt de preventie veelal nog opgetuigd op landelijk niveau: de genomen maatregelen blijken tot aan de grens geldend te zijn, maar niet eroverheen te reiken. 15 De oprichting van een Europees centrum voor ziektepreventie (ECDC) is een stap voorwaarts, maar in feite zijn de problemen mondiaal en vergen ze dus een mondiale aanpak. Waar armoede regeert, is die aanpak een zaak van solidariteit van rijke met arme landen. Solidariteit is een soft begrip, zeker zolang het water niet over de dijk slaat, of zoals de politicus Hans van Mierlo het stelde: wanneer je een probleem niet kunt oplossen, moet je wachten tot het groter wordt. Het lijkt een kwestie van wachten.",
            "cite_spans": [
                {
                    "start": 179,
                    "end": 181,
                    "text": "15",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [],
            "section": "Risicofactoren"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Waving goodbye to measles",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "M"
                    ],
                    "last": "Strebel",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "Cochi",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Nature",
            "volume": "414",
            "issn": "",
            "pages": "695--701",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Zijn opduikende zo\u00f6nosen beheersbaar?",
            "authors": [
                {
                    "first": "Jmd",
                    "middle": [],
                    "last": "Galama",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Ned Tijdschr Geneeskd",
            "volume": "149",
            "issn": "",
            "pages": "680--684",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "The Thucydides syndrome: another view",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Holladay",
                    "suffix": ""
                }
            ],
            "year": 1986,
            "venue": "N Eng J Med",
            "volume": "315",
            "issn": "",
            "pages": "1170--1172",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Plagues and people",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "H"
                    ],
                    "last": "Mcneill",
                    "suffix": ""
                }
            ],
            "year": 1976,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Emerging infections: a perpetual challenge",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "M"
                    ],
                    "last": "Morens",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "K"
                    ],
                    "last": "Folkers",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "S"
                    ],
                    "last": "Fauci",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Lancet Infect Dis",
            "volume": "8",
            "issn": "",
            "pages": "710--719",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "The black death. Londen; Penguin Books",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Ziegler",
                    "suffix": ""
                }
            ],
            "year": 1982,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Parijs: Gallimard",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "La"
                    ],
                    "last": "Camus",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Peste",
                    "suffix": ""
                }
            ],
            "year": 1947,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Smallpox, the greatest killer in history",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "R"
                    ],
                    "last": "Hopkins",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Handleiding tot de kennis en geneezing van de ziekten der kinderen. 's-Gravenhage",
            "authors": [
                {
                    "first": "Ros\u00e9n",
                    "middle": [],
                    "last": "Van Rosenstein",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Viruses, plagues and history",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "A"
                    ],
                    "last": "Oldstone",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "The social ecology of infectious diseases",
            "authors": [],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Host range and emerging and reemerging pathogens",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "E"
                    ],
                    "last": "Woolhouse",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gowtage-Sequeria",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Infect Dis",
            "volume": "11",
            "issn": "",
            "pages": "1842--1849",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Urbanization and the social ecology of emerging infectious diseases",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "A"
                    ],
                    "last": "Wilcox",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Gubler",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "F"
                    ],
                    "last": "Pizer",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "How prepared is Europe for pandemic influenza? Analysis of national plans",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Mounier-Jack",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "J"
                    ],
                    "last": "Coker",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Lancet",
            "volume": "367",
            "issn": "",
            "pages": "1405--1416",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Mummie van Ramses V.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Pestilentie\u00ebn door de eeuwen heen.* pokken (circa 10.000 voor Chr.) mazelen (circa 10.000 voor Chr.) de pest (1346 in Caffa nabij de Zwarte Zee, rond 1330 al in Azi\u00eb) syfilis (1495, import in Europa vanuit Zuid-Amerika) tuberculose (17e en 18e eeuw, tijdens industri\u00eble revolutie in steden)dengue (3e eeuw in China, mondiaal in 17e-18e eeuw) de meeste epidemie\u00ebn uit het verre verleden zijn de verwekkers onbekend. Naast de grote pandemie\u00ebn bestaan nog talloze uitbraken van beperkter omvang, zoals SARS, BSE, hemorragische koortsen, Nipah-virusencefalitis, enterovirus-71-infecties, Q-koorts, lymeborreliose, vogelgriep en de relatief mild verlopen, latere grieppandemie\u00ebn.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Dengue is een goed voorbeeld van een infectieziekte van verstedelijkte gebieden, maar niet de enige. Chikungunya en gele koorts volgen eenzelfde patroon en zijn ook in opmars.Bestrijding van infectieziekten vergt een gecontroleerde aanpak van surveillance, bronopsporing en het nemen van effectieve maatregelen. Het ruimen van de veestapel, zoals recent werd gedaan bij de bestrijding van Q-koorts, is zo'n maatregel. Het illustreert dat sommige maatregelen enorme kosten met zich meebrengen en vaak stuiten op tegengestelde belangen. Dit laatste maakt dat het probleem eerst in omvang zal groeien voordat men bereid is de prijs te betalen. In een situatie van armoede, zoals in de mega-cities van de derde wereld wordt aangetroffen, is het uiterst moeilijk om effectieve maatregelen door te voeren. Soms bestaat er een effectief vaccin en biedt vaccinatie van de bevolking een oplossing, maar het handhaven van een hoge vaccinatiegraad stuit vaak op weerstand, zeker wanneer de infectie onder controle lijkt en men vanuit het oogpunt van schaarste andere politieke keuzes maakt. Dit probleem doet zich op dit moment voor bij de eradicatie van polio. Volgens de WHO zou polio in het jaar 2000 de wereld uit zijn, een slogan die destijds haalbaar leek. Echter, nu polio door vaccinatie in grote delen van de wereld is verdwenen, wordt in arme landen veelal gestopt met vaccineren en duikt polio na verloop van jaren in deze gebieden telkens weer op.De ontwikkeling van een vaccin voor dengue wordt belemmerd door het bestaan van vier serotypen en het gevaar dat parti\u00eble be-Figuur 2 Semaring (Indonesi\u00eb), benedenstad tijdens het regenseizoen.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}