{
    "paper_id": "a6946039ab0f28efa8184f33fcbb4941222e3c94",
    "metadata": {
        "title": "SARS-CoV, BUT NOT HCoV-NL63, UTILIZES CATHEPSINS TO INFECT CELLS Viral entry",
        "authors": [
            {
                "first": "I-Chueh",
                "middle": [],
                "last": "Huang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Jan",
                "middle": [],
                "last": "Bosch",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Wenhui",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Michael",
                "middle": [],
                "last": "Farzan",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Peter",
                "middle": [
                    "M"
                ],
                "last": "Rottier",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Hyeryun",
                "middle": [],
                "last": "Choe",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Three genetic and serologic groups of coronaviruses have been described. Group 1 human coronavirus HCoV-229E utilizes aminopeptidase N (APN; CD13) as its cellular receptor, 1, 2 whereas SARS-CoV, a group 2 virus, uses angiotensin-converting enzyme 2 (ACE2). 3, 4 A recently identified novel group 1 coronavirus, HCoV-NL63, utilizes the SARS-CoV receptor ACE2, 5 despite its close similarity to other group 1 coronaviruses.",
            "cite_spans": [
                {
                    "start": 258,
                    "end": 260,
                    "text": "3,",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 261,
                    "end": 262,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 360,
                    "end": 361,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "Some coronavirus S proteins are cleaved into 2 domains by a furin-like protease in virus-producing cells. The resulting S1 domain mediates receptor binding, 6, 7 and the Cterminal S2 domain mediates fusion between viral and cellular membranes. This producer-cell processing of fusion protein is essential for the infection of HIV-1 and influenza virus. 8 Although the S1 and S2 domains can be identified by their similarity with other S proteins, SARS-CoV and HCoV-NL63 S proteins are not processed in the producer cells.",
            "cite_spans": [
                {
                    "start": 353,
                    "end": 354,
                    "text": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "Cathepsins are a diverse group of endosomal and lysosomal proteases. The role of cathepsins in reovirus infection is well established. 9-12 After receptor-mediated endocytosis, degradation of outer capsid protein \u03c33 by cathepsins is essential for reovirus infection. Recently, it has been demonstrated that infection mediated by the GP protein of the Zaire Ebola virus depends on cathepsin B. 13 Here we show that cathepsins play an important role in SARS-CoV infection. In contrast, HCoV-NL63 infection is not dependent on cathepsin activities. Thus variations in cellular proteases can serve as an additional determinant of viral tropism.",
            "cite_spans": [
                {
                    "start": 393,
                    "end": 395,
                    "text": "13",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "Inhibitors for several classes of proteases were assayed for their ability to modulate SARS-CoV infection. HIV-1 is included as a control. Murine leukemia viruses carrying the gene for green fluorescent protein (GFP) were pseudotyped with the SARS-CoV S protein or HIV-1 gp160 (SARS/MLV and HIV-1/MLV, respectively). 293T cells expressing ACE2 or CD4/CXCR4 were incubated with these pseudotyped viruses in the presence of the aspartic protease inhibitor pepstatin A, serine protease inhibitor AEBSF, cysteine protease inhibitor E64d, and metalloprotease inhibitor phosphoramidon. As shown in Fig. 1A , only E64d, a general inhibitor of cysteine proteases, blocked SARS/MLV infection. Because SARS-CoV infection is sensitive to NH 4 Cl, an inhibitor of lysosomal acidification, 14 and roles for the lysosomal cysteine protease cathepsins in reovirus infection have been described, [9] [10] [11] [12] The ability of cathepsin inhibitors to block infection was then examined using replication-competent SARS-CoV or HCoV-NL63, which also utilizes ACE2 as its receptor. Sindbis virus and vesicular stomatitis virus (VSV) were included as controls. Infection was assessed within 8 and 24 hours of incubation with SARS-CoV and HCoV-NL63, respectively, thereby minimizing any potential effects of inhibitors on post-entry steps in viral replication. As shown in Fig. 2 ",
            "cite_spans": [
                {
                    "start": 777,
                    "end": 779,
                    "text": "14",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 880,
                    "end": 883,
                    "text": "[9]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 884,
                    "end": 888,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 889,
                    "end": 893,
                    "text": "[11]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 894,
                    "end": 898,
                    "text": "[12]",
                    "ref_id": "BIBREF11"
                }
            ],
            "ref_spans": [
                {
                    "start": 592,
                    "end": 599,
                    "text": "Fig. 1A",
                    "ref_id": null
                },
                {
                    "start": 1354,
                    "end": 1360,
                    "text": "Fig. 2",
                    "ref_id": null
                }
            ],
            "section": "RESULTS"
        },
        {
            "text": "This difference between HCoV-NL63 and SARS-CoV was confirmed using pseudotyped viruses. Inhibitors of cathepsin B, L, K, and S-the only available specific cathepsin inhibitors-were assessed. None of these inhibitors had a detectable effect on the infection of MLV pseudotyped with the HCoV-NL63 S protein (NL63/MLV), or with the VSV G protein (VSV-G/MLV), whereas cathepsin L and S inhibitors efficiently blocked SARS/MLV entry (Fig. 3A) . None of the protease inhibitors tested in Fig. 1A had any effect on NL63/MLV infection (data not shown), nor did E64d, which again potently inhibited SARS/MLV (Fig. 3B) . Because cathepsin inhibitors can cross-react, the roles of specific cathepsins were studied by introducing exogenous cathepsins into 293T cells. To ensure comparable ACE2 expression levels in cells transfected with various cathepsins, varying amounts of ACE2-expressing plasmid were used in transfection together with fixed amount of cathepsin plasmids. Also, ACE2 cell-surface expression was assessed by flow cytometry in each experiment. As shown in Fig. 4A , cathepsin L markedly increased infection of SARS/MLV but had no effect on NL63/MLV or VSV-G/MLV. Cathepsin S also modestly enhanced SARS/MLV infection, but, surprisingly, reduced NL63/MLV infection. In parallel, these cathepsins were immunoprecipitated from aliquots of the same cells that were metabolically labeled, and analyzed by SDS-PAGE. Figure 4B shows that exogenous cathepsin L expression was lower than that of cathepsin B or S, despite its greater effect on SARS-CoV infection. Similarly, as shown in Fig. 4C , introduction of cathepsin L into mouse embryonic fibroblasts derived from mice lacking cathepsin L resulted in enhanced infection by SARS/MLV and MLV pseudotyped with Marburg virus or Ebola virus GP proteins, while no enhancement of infection was observed with NL63/MLV or VSV-G/MLV. Collectively, these data show that cathepsins L and S contribute to SARS-CoV infection but not to that of HCoV-NL63. ",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 428,
                    "end": 437,
                    "text": "(Fig. 3A)",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 482,
                    "end": 489,
                    "text": "Fig. 1A",
                    "ref_id": null
                },
                {
                    "start": 599,
                    "end": 608,
                    "text": "(Fig. 3B)",
                    "ref_id": "FIGREF2"
                },
                {
                    "start": 1063,
                    "end": 1070,
                    "text": "Fig. 4A",
                    "ref_id": "FIGREF3"
                },
                {
                    "start": 1417,
                    "end": 1426,
                    "text": "Figure 4B",
                    "ref_id": "FIGREF3"
                },
                {
                    "start": 1585,
                    "end": 1592,
                    "text": "Fig. 4C",
                    "ref_id": "FIGREF3"
                }
            ],
            "section": ", SARS-CoV infection was effectively blocked by cathepsin L inhibitor and less significantly by cathepsin B inhibitor. Neither"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Human aminopeptidase N is a receptor for human coronavirus 229E",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "L"
                    ],
                    "last": "Yeager",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Ashmun",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "K"
                    ],
                    "last": "Williams",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "B"
                    ],
                    "last": "Cardellichio",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "H"
                    ],
                    "last": "Shapiro",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "T"
                    ],
                    "last": "Look",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "V"
                    ],
                    "last": "Holmes",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "Nature",
            "volume": "357",
            "issn": "",
            "pages": "420--422",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Feline aminopeptidase N is a receptor for all group I coronaviruses",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "B"
                    ],
                    "last": "Tresnan",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "V"
                    ],
                    "last": "Holmes",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Adv. Exp. Med. Biol",
            "volume": "440",
            "issn": "",
            "pages": "69--75",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Angiotensin-converting enzyme 2 is a functional receptor for the SARS coronavirus",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Moore",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Vasilieva",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Sui",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "K"
                    ],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Berne",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Somasundaran",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "L"
                    ],
                    "last": "Sullivan",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Luzeriaga",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "C"
                    ],
                    "last": "Greenough",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Choe",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farzan",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Nature",
            "volume": "426",
            "issn": "",
            "pages": "450--454",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Expression cloning of functional receptor used by SARS coronavirus",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Nie",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Luo",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Tan",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Yin",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Qu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Qing",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ding",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Deng",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Biochem. Biophys. Res. Commun",
            "volume": "315",
            "issn": "",
            "pages": "439--444",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Human coronavirus NL63 employs the severe acute respiratory syndrome coronavirus receptor for cellular entry",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Hofmann",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Pyrc",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Van Der Hoek",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Geier",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Berkhout",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Pohlmann",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "16",
            "issn": "",
            "pages": "7988--7993",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "A 193-amino acid fragment of the SARS coronavirus S protein efficiently binds angiotensin-converting enzyme 2",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "K"
                    ],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Moore",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Choe",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farzan",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J. Biol. Chem",
            "volume": "279",
            "issn": "",
            "pages": "3197--3201",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Identification of a receptorbinding domain of the spike glycoprotein of human coronavirus HCoV-229E",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Bonavia",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "D"
                    ],
                    "last": "Zelus",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "E"
                    ],
                    "last": "Wentworth",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Talbot",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "V"
                    ],
                    "last": "Holmes",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J. Virol",
            "volume": "77",
            "issn": "",
            "pages": "2530--2538",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Virus entry: molecular mechanisms and biomedical applications",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "S"
                    ],
                    "last": "Dimitrov",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat. Rev. Microbiol",
            "volume": "2",
            "issn": "",
            "pages": "109--122",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Addition of exogenous protease facilitates reovirus infection in many restrictive cells",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "W"
                    ],
                    "last": "Golden",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Linke",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Schmechel",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Thoemke",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "A"
                    ],
                    "last": "Schiff",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J. Virol",
            "volume": "76",
            "issn": "",
            "pages": "7430--7443",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Cathepsin L and cathepsin B mediate reovirus disassembly in murine fibroblast cells",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "H"
                    ],
                    "last": "Ebert",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Deussing",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Peters",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "S"
                    ],
                    "last": "Dermody",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J. Biol. Chem",
            "volume": "277",
            "issn": "",
            "pages": "24609--24617",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Sites and determinants of early cleavages in the proteolytic processing pathway of reovirus surface protein sigma 3",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Jane-Valbuena",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "A"
                    ],
                    "last": "Breun",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "A"
                    ],
                    "last": "Schiff",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Nibert",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "J. Virol",
            "volume": "76",
            "issn": "",
            "pages": "5184--5197",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Mutant cells selected during persistent reovirus infection do not express mature cathepsin L and do not support reovirus disassembly",
            "authors": [
                {
                    "first": "G",
                    "middle": [
                        "S"
                    ],
                    "last": "Baer",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "H"
                    ],
                    "last": "Ebert",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "J"
                    ],
                    "last": "Chung",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "H"
                    ],
                    "last": "Erickson",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "S"
                    ],
                    "last": "Dermody",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J. Virol",
            "volume": "73",
            "issn": "",
            "pages": "9532--9543",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Endosomal proteolysis of the Ebola virus glycoprotein is necessary for infection",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Chandran",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "J"
                    ],
                    "last": "Sullivan",
                    "suffix": ""
                },
                {
                    "first": "U",
                    "middle": [],
                    "last": "Felbor",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "P"
                    ],
                    "last": "Whelan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Cunningham",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Science",
            "volume": "308",
            "issn": "",
            "pages": "1643--1645",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Inhibitors of cathepsin L prevent severe acute respiratory syndrome coronavirus entry",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Simmons",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "D"
                    ],
                    "last": "Reeves",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Rennekamp",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "M"
                    ],
                    "last": "Amberg",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Piefer",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Bates",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "101",
            "issn": "",
            "pages": "4240--4245",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "the ability of cathepsin inhibitors to block SARS/MLV infection was assessed. As shown in Fig. 1B, cathepsin L inhibitor (Z-FY(t-Bu)-DMK) potently blocked SARS/MLV infection. Cathepsin B inhibitor (CA-074 methyl ester) also showed consistent but less significant inhibition of infection. HIV-1/MLV infection was rather enhanced by these inhibitors, consistent with other reports indicating that lysosomal degradation interferes with productive HIV-1 infection.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "SARS coronavirus S-protein-mediated (SARS/MLV) entry is blocked by cathepsin L inhibitor. of the cathepsin inhibitors showed significant suppression of HCoV-NL63 replication. SARS-CoV, BUT NOT HCoV-NL63, UTILIZES CATHEPSINS Cathepsin L inhibitor suppresses the infection of SARS-CoV, but not that of HCoV-NL63.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "HCoV-NL63 S-protein-mediated (NL63/MLV) entry is not affected by cathepsin inhibitors.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Exogenous cathepsin L enhances the entry of SARS/MLV, but not that of NL63/MLV.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}