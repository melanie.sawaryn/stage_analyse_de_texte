{
    "paper_id": "03451de3ac8646f267bb79ac8fefe7fe878dc838",
    "metadata": {
        "title": "DEUBIQUITINATING ACTIVITY OF THE SARS-CoV PAPAIN-LIKE PROTEASE",
        "authors": [
            {
                "first": "Naina",
                "middle": [],
                "last": "Barretto",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Dalia",
                "middle": [],
                "last": "Jukneliene",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Kiira",
                "middle": [],
                "last": "Ratia",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Zhongbin",
                "middle": [],
                "last": "Chen",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Andrew",
                "middle": [
                    "D"
                ],
                "last": "Mesecar",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Susan",
                "middle": [
                    "C"
                ],
                "last": "Baker",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "(nsps) by two viral proteases; a 3C-like protease (3CLpro) and a papain-like protease (PLpro). PLpro processes the amino-terminal end of the replicase polyprotein to release nsp1, nsp2, and nsp3. In this study, we identified a 316 amino acid core catalytic domain for SARS-CoV PLpro that is active in trans-cleavage assays. Interestingly, bioinformatics analysis of the SARS-CoV PLpro domain suggested that this protease may also have deubiquitinating activity because it is predicted to have structural similarity to a cellular deubiquitinase, HAUSP (herpesvirus-associated ubiquitin-specific-protease). Using a purified preparation of the catalytic core domain in an in vitro assay, we demonstrate that PLpro has the ability to cleave ubiquitinated substrates. We also established a FRETbased assay to study the kinetics of proteolysis and deubiquitination by SARS-CoV PLpro. This characterization of a PLpro catalytic core will facilitate structural studies as well as high-throughput assays to identify antiviral compounds.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "Proteolytic processing of the coronavirus replicase polyprotein by 3CLpro and the papain-like proteases is essential for the formation of the mature replicase complex. In light of its role in processing the amino-terminal end of the replicase polyprotein, PLpro is a potential target for the development of antiviral drugs. To identify a catalytically active core domain for PLpro, we performed deletion analysis from the Cand N-terminii of a region of SARS-CoV nsp3, which we had previously shown to be proteolytic active. We generated four N-terminal, and four C-terminal constructs and tested them for proteolytic activity in a trans-cleavage assay. The region from amino acids 1541-1855 was the smallest fragment with proteolytic activity. 3 To identify the important features of this segment, we aligned the amino acid sequences of 15 papain-like protease domains from 8 different coronaviruses with that of SARS-CoV PLpro1541-1855. While there is only an 18-32% identity between the sequences, there are some important conserved features, including the catalytic cysteine and histidine residues (identified as C1651 and H1812 for SARS-CoV PLpro) 2 and a conserved Zn-binding finger, which has been shown to be important for proteolytic activity of HCoV-229E PLP1. 4 In addition, we identified an aspartic acid residue (D1826 in SARS-CoV PLpro), which is conserved amongst all the coronavirus papain-like proteases. We found that PLpro in which D1826 was replaced by an alanine had no detectable proteolytic activity in the cellular trans-cleavage assay, 3 indicating that this residue may play an important role in proteolytic activity. We predict that this residue forms a part of a catalytic triad with the catalytic cysteine and histidine residues, similar to the active sites of other papain-like proteases. 5 Ultimately, crystallographic data will be critical for a complete understanding of the coronavirus PLpro active site.",
            "cite_spans": [
                {
                    "start": 744,
                    "end": 745,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1270,
                    "end": 1271,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1560,
                    "end": 1561,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1818,
                    "end": 1819,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "IDENTIFYING A PLpro CATALYTIC CORE DOMAIN"
        },
        {
            "text": "Interestingly, an independent bioinformatics study predicted that SARS-CoV PLpro may have structural similarity to a cellular deubiquitinating enzyme, HAUSP (herpesvirus-associated ubiquitin-specific protease). 6, 7 On the basis of this similarity in molecular architecture and the predicted specificity of PLpro for cleavage after a diglycine residue, Sulea and co-workers predicted that PLpro has deubiquitinating (DUB) activity.",
            "cite_spans": [
                {
                    "start": 211,
                    "end": 213,
                    "text": "6,",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 214,
                    "end": 215,
                    "text": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "DEUBIQUITINATING ACTIVITY OF SARS-CoV PLpro."
        },
        {
            "text": "To test this prediction we used our experimentally determined catalytic core domain in an in vitro assay for deubiquitination. 7 Briefly, wild-type PLpro and mutated versions where C1651 or D1826 was replaced by alanine, were expressed in E. coli BL21 and purified to homogeneity. 3 In the assay, purified WT PLpro was incubated alone (Fig. 1 , lane 2) or with di-ubiquitin substrate (Boston Biochem) (lane 3) for 1 hour at 37\u00b0C in a buffer containing BSA. In lanes 4 and 5, purified mutant PLpro D1826A and PLpro C1651A respectively were incubated with the substrate. WT PLpro cleaved the substrate completely, while the C1651A mutant had no activity. As expected, the D1826A mutant showed a small amount of processing activity. This is the first evidence that SARS-CoV PLpro is able to cleave a ubiquitinated substrate.",
            "cite_spans": [
                {
                    "start": 127,
                    "end": 128,
                    "text": "7",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 281,
                    "end": 282,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [
                {
                    "start": 335,
                    "end": 342,
                    "text": "(Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "DEUBIQUITINATING ACTIVITY OF SARS-CoV PLpro."
        },
        {
            "text": "To study the kinetics of proteolysis and deubiquitination by PLpro, we established a FRET-based assay using a substrate [E-EDANS]RELNGGAPI-[KDABCYL]S for proteolysis. This substrate is based on the naturally occurring nsp1/nsp2 and nsp2/nsp3 cleavage sites for PLpro. A similar assay using the commercially available substrate, ubiquitin-AMC (Boston Biochem) was used to measure the kinetics of deubiquitination. The substrates were incubated with the purified proteins and the reaction was allowed to go to completion. The fluorescence released was measured on a Cary Eclipse fluorescence spectrophotometer. Since the enzyme could not be saturated with substrate, pseudo-first rate order kinetics were used to calculate the apparent k cat / k m (k app ), indicated in Table 1 . WT PLpro1541-1855 cleaved the ubiquitin substrate with faster kinetics than the peptide substrate, though this might reflect in vitro conditions and the substrates used. However in a similar assay, HAUSP cleaved ubiquitin-AMC with a k cat /K m of 13 min 1 mM 1 (Hu deubiquitinase.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 769,
                    "end": 776,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "DEUBIQUITINATING ACTIVITY OF SARS-CoV PLpro."
        },
        {
            "text": "The mutant D1826A hydrolyzed peptide substrate at approximately 1% of the efficiency of the WT, which is consistent with an assisting role for this residue in a catalytic triad with the catalytic cysteine and histidine residues. The C1651A mutant showed no cleavage activity with either substrate, demonstrating the essential role of the catalytic cysteine residue (Johnston et al., 1997) .",
            "cite_spans": [
                {
                    "start": 365,
                    "end": 388,
                    "text": "(Johnston et al., 1997)",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "DEUBIQUITINATING ACTIVITY OF SARS-CoV PLpro."
        },
        {
            "text": "Sulea and co-workers noted \"structural signatures for strict specificity\" (Sulea et al., 2005) present in HAUSP and also in some of the coronavirus papain-like proteases, which form part of the substrate-binding site. In the case of SARS-CoV PLpro the residues Y1804 and Y1813 occlude the substrate-binding site, imposing the requirement for the small diglycine residues at the cleavage site (Sulea et al., 2005) . Our alignment of the amino acid sequences of 16 papain-like proteases from nine coronaviruses shows that the \"signature sequence\" is present in twelve of these sequences. This suggests that these proteases may also have deubiquitinating activity, though this remains to be experimentally verified. ",
            "cite_spans": [
                {
                    "start": 74,
                    "end": 94,
                    "text": "(Sulea et al., 2005)",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 392,
                    "end": 412,
                    "text": "(Sulea et al., 2005)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "A \"SIGNATURE SEQUENCE\" FOR DEUBIQUITINATION ACTIVITY"
        },
        {
            "text": "Here, we have identified a core catalytic domain for SARS-CoV PLpro that is capable of processing both the amino-terminal end of the replicase polyprotein and ubiquitinated substrates. The role of this deubiquitinating activity in SARS-CoV-infected cells is unknown. Future experiments will focus on determining if this SARS-CoV PLpro DUB is active in virus-infected cells, investigating the significance of DUB activity in the viral life cycle, as well as identifying possible viral and cellular targets.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "FUTURE DIRECTIONS"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Coronavirus protein processing and RNA synthesis is inhibited by the cysteine proteinase inhibitor E64d",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Spence",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "F"
                    ],
                    "last": "Currier",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "R"
                    ],
                    "last": "Denison",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Virology",
            "volume": "208",
            "issn": "",
            "pages": "1--8",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Identification of severe acute respiratory syndrome coronavirus replicase products and characterization of papain-like protease activity",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "H"
                    ],
                    "last": "Harcourt",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Jukneliene",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kanjanahaluethai",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Bechill",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "M"
                    ],
                    "last": "Severson",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Rota",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "C"
                    ],
                    "last": "Baker",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J. Virol",
            "volume": "78",
            "issn": "",
            "pages": "13600--13612",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "The papain-like protease of Severe Acute Respiratory Syndrome (SARS) coronavirus has deubiquitinating activity",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Barretto",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Jukneliene",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Ratia",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "D"
                    ],
                    "last": "Mesecar",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "C"
                    ],
                    "last": "Baker",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "J. Virol",
            "volume": "79",
            "issn": "",
            "pages": "15189--15198",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "A human RNA viral cysteine proteinase that depends on a unique Zn 2+ binding finger connecting the two domains of a papain-like fold",
            "authors": [
                {
                    "first": "Herold",
                    "middle": [
                        "J"
                    ],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "G"
                    ],
                    "last": "Siddell",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "E"
                    ],
                    "last": "Gorbalenya",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J. Biol. Chem",
            "volume": "274",
            "issn": "",
            "pages": "14918--14925",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Crystal structure of a deubiquitinating enzyme (human UCH-L3) at 1.8 A resolution",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "C"
                    ],
                    "last": "Johnston",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "N"
                    ],
                    "last": "Larsen",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "J"
                    ],
                    "last": "Cook",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "D"
                    ],
                    "last": "Wilkinson",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "P"
                    ],
                    "last": "Hill",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "EMBO J",
            "volume": "16",
            "issn": "",
            "pages": "3787--3796",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Deubiquitination, a new function of the severe acute respiratory syndrome coronavirus papain-like protease?",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Sulea",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "A"
                    ],
                    "last": "Lindner",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "O"
                    ],
                    "last": "Purisima",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Menard",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Virol",
            "volume": "79",
            "issn": "",
            "pages": "4550--4551",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Crystal structure of a UBP-family deubiquitinating enzyme in isolation and complex with ubiquitin aldehyde",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Yao",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "W"
                    ],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Gu",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "E"
                    ],
                    "last": "Cohen",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Cell",
            "volume": "111",
            "issn": "",
            "pages": "1041--1054",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Deubiquitinating activity of SARS-CoV PLpro. Purified WT or mutant PLpro was incubated with the di-ubiquitin substrate for 1 hour at 37\u00b0C. Products were resolved by electrophoresis on a 10-20% gradient polyacrylamide gel, which was stained with Coomassie blue.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "PLp1804 YT---GNYQCGH-Y-T-HITAKETLYR----<32>IKP-------------%ID MHVJ P2 1867 FT---GG-SVGH-Y-T-HVKCKPKYQL----<35?>YY--------------32 BCoV P2 1822 FK---GD-KVGH-Y-V-HVKCEQSYQL----<35>YY--------------31 0C43 P2 1822 FI---GD-KVGH-Y-V-HVKCEQSYQL----<35>YY--------------30 HKU P2 1907 FM---G-VGVGH-Y-T-HLKCGSPYQH----<32>LTNY------------28 229E P2 1857 FS---GPVDKGH-Y-TVYDTAKKSMY-----<30>VK--------------22 TGEV P2 1736 YS---GSNRNGH-Y-T-YYDNRNGLV-----<25>KKPQAEERPKNCAFNK 22 IBV PLp1490 FV---GSTNSGHCY-T-QAAGQ----A----<30>SLPV------------22 NL63 P2 1825 YTTFSGSFDNGH-Y-VVYDAANNAVY-----<28>VPTIVSEK--------21 TGEV P1 1237 YT---GTTQNGH-Y-M--VDDIEHGYC----<28>EKPKQEFKVEKVEQQ-21 229E P1 1197 FR---GAVSCGH-YQT-NIYSQNLC------<37>IKNTVD----------20 NL63 P1 1204 YL---GVKGSGH-Y-------QTNLYSFNKA<33>VKPFAVYKNVK-----19 HKU P1 1306 VD---VNVC--H-S-V-AVIGDE---Q----<36>ITPNVCF---------20 MHVJ P1 1267 VN------DCHS-M-A-VVDGKQ--------<38>ITPNVCF---------18 OC43 P1 1199 KR---IVYKAAC-V-V-DVNDSHSMAV----<43>ITPNVCF---------18 BCoV P1 1199 KR---SVYKAAC-V-V-DVNDSHSMAV----<43>ITPNVCF--------Multiple sequence alignment of DUB signature sequences from coronavirus papain-like proteases. The papain-like protease domain amino acid sequences (two domains termed as P1 and P2; one domain termed as PLpro) of nine coronaviruses were aligned using the ALIGN program (SciEd). The residues proposed to be part of the substrate binding site for deubiquitination are boxed. Identical residues are highlighted in light gray. The catalytic histidine residue is boxed in thick black or highlighted in gray. Papain-like proteases which are predicted to lack deubiquitinating activity are indicated by a thick vertical line. Abbreviationas are as follows: SARS CoV-Severe acute respiratory syndrome coronavirus, Urbani strain (AY278741); MHVJ-Mouse hepatitis virus, strain JHM (NC_001846); BCoV-bovine coronavirus (NC_003045); HCoV-OC43-Human coronavirus OC43 (AY585228); HCoV-229E-Human coronavirus 229E (X69721); HCoV-NL63-Human coronavirus NL63 (NC_005831); TGEV-Transmissible gastroenteritis virus of pigs (Z34093); aIBV-avian infectious bronchitis virus (NC_001451); HCoV-HKU1-Hong Kong University coronavirus 1 ., 2002), indicating that PLpro has higher deubiquitinating activity than this cellular N. BARRETTO ET AL.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Apparent kcat/Km (kapp) values obtained in FRET-based assays for peptide hydrolysis or deubiquitination for SARS CoV PLpro1541-1855 and mutants. Assay as described in Ref. 3.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "This work was funded by Public Health Service research grant AI45798 (to S.C.B.) and P01AI060915 (to S.C.B. and A.D.M). N.B. was supported by Training Grant T32 AI007508.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ACKNOWLEDGMENTS"
        }
    ]
}