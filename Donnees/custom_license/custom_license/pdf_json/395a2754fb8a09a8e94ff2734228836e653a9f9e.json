{
    "paper_id": "395a2754fb8a09a8e94ff2734228836e653a9f9e",
    "metadata": {
        "title": "Klebsiella pneumoniae pharyngitis mimicking malignancy: a diagnostic dilemma",
        "authors": [
            {
                "first": "C.-F",
                "middle": [],
                "last": "Yeh",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "W.-Y",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "\u2022 Y.-B",
                "middle": [],
                "last": "Hsu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Acute pharyngitis is a common disease. However, acute pharyngitis caused by Klebsiella pneumoniae with a gross appearance mimicking hypopharyngeal malignancy has never previously been reported. We report the case of a 57-year-old man with a right hypopharyngeal tumor which was disclosed by fiberoptic laryngoscopy and computed tomography scan. However, both the frozen and final pathologies showed no evidence of malignant cells, and a bacterial culture revealed the growth of K. pneumoniae. The hypopharyngeal lesion completely regressed after 2 weeks of antibiotic treatment. Clinicians should perform biopsy along with tissue culture for tumor-like lesions because infectious agents can lead to lesions with malignancy-like appearance.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Acute pharyngitis is one of the most common diseases for which patients seek medical assistance [1] . It refers to an acute inflammatory process involving nasopharynx, oropharynx or hypopharynx. Viral infections are the most common cause of acute pharyngitis and they are usually benign and self-limited [2] . Bacterial organisms are also capable of inducing pharyngitis and they usually involve more severe disease processes. The most important form of bacterial pharyngitis is group A streptococcal pharyngitis for which antimicrobial therapy is definitely indicated [2] . However, bacterial pharyngitis caused by Klebsiella pneumoniae is rare. The most common clinical findings of acute pharyngitis of the pharynx include erythema, ulcers, exudates and vesicles, but not malignant tumor-like mass lesions. Herein, we report a case of bacterial pharyngitis caused by K. pneumoniae with the gross appearance surprisingly mimicking hypopharyngeal malignancy. The clinical chart of this patient was reviewed following Institutional Review Board guidelines.",
            "cite_spans": [
                {
                    "start": 96,
                    "end": 99,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 304,
                    "end": 307,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 569,
                    "end": 572,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "A 57-year-old man presented to our out-patient department with a sudden onset of hoarseness and dysphagia. His past medical history included liver cirrhosis, and he had smoked half a pack of cigarettes per day for 30 years. No fever, tachycardia, stridor or dyspnea was noted. On physical examination, the oral cavity appeared normal and no neck lymphadenopathy was identified. Fiberoptic laryngoscopy disclosed a large hypopharyngeal tumor with limited movement of bilateral vocal cords and critical airway (Fig. 1) . Laboratory examinations revealed leukopenia (3,400/mm 3 ) and thrombocytopenia (86,000/mm 3 ), and biochemistry analysis showed hypoalbuminemia (3.2 g/ dL), hyperbilirubinemia (total bilirubin 1.77 mg/dL), elevated aspartate aminotransferase (AST) (80 U/L) and normal alanine aminotransferase (ALT) (40 U/L). Prothrombin time and activated partial thromboplastin time were within normal limits. C-reactive protein (CRP) was 4.12 mg/dL (reference 0-0.5 mg/dL). A computed tomography (CT) scan revealed an increased amount of soft tissue at the right pyriform sinus with a maximal diameter of 5.6 cm, compatible with hypopharyngeal cancer ( Fig. 2) . Tracheostomy was performed to establish an adequate airway, and a tumor biopsy was performed via direct laryngoscopy. However, the pathology report of the frozen specimen showed no evidence of malignancy, and the final pathology revealed ulceration with necrosis, inflammation and granulation tissues (Fig. 3 ). To rule out Wegener's granulomatosis, we checked the erythrocyte sedimentation rate (ESR), antinuclear antibodies (ANA), p-antineutrophil cytoplasmic antibodies (ANCA), c-ANCA and urine analysis and all results were normal. Seven days later, a bacterial culture of the tumor tissue revealed growth of K. pneumoniae. Concentrated acid-fast stain of the tumor was negative. A mycobacterial culture of the tumor tissue kept for up to 8 weeks also revealed negative result. Amoxicillin/clavulanic acid was prescribed according to antibacterial susceptibility testing, and dramatic regression of the tumor was noted after 2 weeks of treatment. ",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 508,
                    "end": 516,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1158,
                    "end": 1165,
                    "text": "Fig. 2)",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 1469,
                    "end": 1476,
                    "text": "(Fig. 3",
                    "ref_id": "FIGREF2"
                }
            ],
            "section": "Case report"
        },
        {
            "text": "Acute pharyngitis involves both infectious and noninfectious causes although the former are more common. The most common viral organisms inducing pharyngitis include rhinovirus (20 %), coronavirus (5 %), adenovirus (5 %), herpes simplex virus (4 %) [1, 2] . The most common bacterial organisms inducing pharyngitis include group A b-hemolytic streptococci (GABHS) (15-30 %) and group C b-hemolytic streptococci (5 %) [1, 2] . Fungi are also capable of inducing pharyngitis in immunosuppressed patients, with Candida albicans being the most common organism.",
            "cite_spans": [
                {
                    "start": 249,
                    "end": 252,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 253,
                    "end": 255,
                    "text": "2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 417,
                    "end": 420,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 421,
                    "end": 423,
                    "text": "2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Klebsiella pneumoniae is a Gram-negative, facultative anaerobic, rod-shaped bacterium. Klebsiella pneumoniae infections are more common in patients with impaired immunity such as those with alcoholism, liver disease, diabetes or cancer patients receiving chemotherapy. Such infections can lead to pneumonia, urinary tract infections, biliary tract infections, or deep neck infections [3] . Klebsiella pneumoniae bacteremia also had been reported in cancer patients, especially those with hematologic malignancies [4] . However, bacterial pharyngitis caused by K. pneumoniae is not common.",
            "cite_spans": [
                {
                    "start": 384,
                    "end": 387,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 513,
                    "end": 516,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Due to the inflammatory nature of pharyngitis, sore throat and pharyngeal erythema are common presentations in acute pharyngitis, and pharyngeal exudates and neck lymphadenopathy may also be seen. Some types of pharyngitis have characteristic symptoms and signs such as hepatosplenomegaly in infectious mononucleosis, and erythematous rash and strawberry tongue in GABHS pharyngitis [5] . However, the coexistence of hoarseness, dysphagia and malignant tumor-like mass lesions is extremely rare in acute pharyngitis.",
            "cite_spans": [
                {
                    "start": 383,
                    "end": 386,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The common diagnostic tests for acute pharyngitis include throat culture, rapid antigen test in GABHS [5] , and heterophile antibody test for infectious mononucleosis [2] . Biopsies are rarely used for the diagnosis of acute pharyngitis [1] , however, we performed a biopsy to make the diagnosis in our case due to the impression of malignancy indicated by both fiberoptic laryngoscopy and CT scan.",
            "cite_spans": [
                {
                    "start": 102,
                    "end": 105,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 167,
                    "end": 170,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 237,
                    "end": 240,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Other pharyngeal infections have been described that mimic malignancy including actinomycosis [6] , syphilis [7] , candidiasis [8] , histoplasmosis [9] , blastomycosis [10] and tuberculosis [11] . However, there are no reports of K. pneumoniae pharyngitis mimicking hypopharyngeal malignancy in the literature.",
            "cite_spans": [
                {
                    "start": 94,
                    "end": 97,
                    "text": "[6]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 109,
                    "end": 112,
                    "text": "[7]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 127,
                    "end": 130,
                    "text": "[8]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 148,
                    "end": 151,
                    "text": "[9]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 168,
                    "end": 172,
                    "text": "[10]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 190,
                    "end": 194,
                    "text": "[11]",
                    "ref_id": "BIBREF11"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "In contrast to the usual appearance of mucosal erythema in bacterial pharyngitis, the patient's initial presentation was a large hypopharyngeal tumor. The key factor in making the correct diagnosis for this patient was the frozen pathology. Because it showed no evidence of malignancy, we further collected tissue cultures for various pathogens in the same surgical procedure, thereby avoiding the need for general anesthesia for a second time.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The pharynx is colonized with a lot of normal bacterial flora. Bacteria growing from pharyngeal culture are not necessarily pathogenic. To prevent the development of drug resistance, antibiotic treatment should be considered carefully. In this case, the tissue culture disclosed K. pneumoniae growth, which is not a common pathogen causing pharyngitis. However, the patient's liver cirrhosis might result in an immunocompromised status and further predisposed him getting infected with some uncommon pathogens including Klebsiella pneumonia. Since the pathologic report revealed no malignancy but only pictures of inflammation, clinically, bacterial pharyngitis was the most possible reason for the tumor-like lesion. We used amoxicillin/clavulanic acid according to the antibacterial susceptibility testing, and the lesion responded well to the treatment. The final diagnosis was acute K. pneumoniae pharyngitis.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "We report a case of severe acute pharyngitis caused by K. pneumoniae mimicking hypopharyngeal malignancy. This case is unique, and such a case has not been reported previously in the related literature. Clinicians should keep in mind that a biopsy is an essential diagnostic approach for tumor-like lesions, and avoid aggressive or irreversible treatment such as emergent total laryngopharyngectomy before a definite diagnosis of a malignant tumor is made. Frozen section and tissue cultures for bacteria, mycobacteria, and fungi should be considered and samples collected when performing the tumor biopsy, especially in patients with an immunocompromised status as these pathogens can lead to the growth of malignant tumor-like lesions. In so doing, the use of general anesthesia for a second time to obtain tissue cultures can be avoided.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Acute pharyngitis",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "L"
                    ],
                    "last": "Bisno",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "N Engl J Med",
            "volume": "344",
            "issn": "",
            "pages": "205--216",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Pharyngitis and epiglottitis",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Alcaide",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "L"
                    ],
                    "last": "Bisno",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Infect Dis Clin North Am",
            "volume": "21",
            "issn": "",
            "pages": "449--69",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Klebsiella pneumoniae fascial space infections of the head and neck in Taiwan: emphasis on diabetic patients and repetitive infections",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Chang",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "H"
                    ],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "R"
                    ],
                    "last": "Guo",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "C"
                    ],
                    "last": "Ko",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Infect",
            "volume": "50",
            "issn": "",
            "pages": "34--40",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Factors associated with mortality in bacteremic patients with hematologic malignancies",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Tumbarello",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Spanu",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Caira",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "M"
                    ],
                    "last": "Trecarichi",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Laurenti",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Montuori",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Diagn Microbiol Infect Dis",
            "volume": "64",
            "issn": "",
            "pages": "320--326",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Principles of appropriate antibiotic use of acute pharyngitis in adults",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Snow",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Mottur-Pilson",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "J"
                    ],
                    "last": "Cooper",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "R"
                    ],
                    "last": "Hoffman",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Ann Intern Med",
            "volume": "134",
            "issn": "",
            "pages": "506--514",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Klebsiella pneumoniae pharyngitis 1049",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Actinomycosis of tonsil masquerading as tumour in a 12-year old child",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "P"
                    ],
                    "last": "Yadav",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Chanda",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Gathwala",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "K"
                    ],
                    "last": "Yadav",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Int J Pediatr Otorhinolaryngol",
            "volume": "63",
            "issn": "",
            "pages": "73--78",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Syphilitic tonsillitis presenting as an ulcerated tonsillar tumor with ipsilateral lymphadenopathy",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Odd\u00f3",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Carrasco",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Capdeville",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "F"
                    ],
                    "last": "Ayala",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Ann Diagn Pathol",
            "volume": "11",
            "issn": "",
            "pages": "353--360",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Laryngeal candidiasis mimicking malignancy",
            "authors": [
                {
                    "first": "F",
                    "middle": [
                        "P"
                    ],
                    "last": "Nunes",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Bishop",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Prasad",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Madison",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "Y"
                    ],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Laryngoscope",
            "volume": "118",
            "issn": "",
            "pages": "1957--1966",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Epiglottic histoplasmosis presenting in a nonendemic region: a clinical mimic of laryngeal carcinoma",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "D"
                    ],
                    "last": "O&apos;hara",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "W"
                    ],
                    "last": "Allegretto",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "D"
                    ],
                    "last": "Taylor",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Isotalo",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Arch Pathol Lab Med",
            "volume": "128",
            "issn": "",
            "pages": "574--581",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Laryngeal blastomycosis: a commonly missed diagnosis: report of two cases and review of the literature",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Hanson",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Spector",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "K"
                    ],
                    "last": "El-Mofty",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Ann Otol Rhinol Laryngol",
            "volume": "109",
            "issn": "",
            "pages": "281--287",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Laryngeal tuberculosis masquerading as carcinoma",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "J"
                    ],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "H"
                    ],
                    "last": "Kang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "W"
                    ],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2002,
            "venue": "Eur Arch Otorhinolaryngol",
            "volume": "259",
            "issn": "",
            "pages": "521--524",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Fiberoptic laryngoscopy disclosed a large hypopharyngeal tumor with a critical airway",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Computed tomography scan revealed an increased amount of soft tissue at the right pyriform sinus (arrow) with a maximal diameter of 5.6 cm, compatible with hypopharyngeal cancer; a axial view, b coronal view",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Pathology of the hypopharyngeal tumor revealed ulceration with necrosis, inflammation and granulation tissues, but no evidence of malignancy (hematoxylin-eosin stain, original magnification, 9100)",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "The authors declare that they have no conflicts of interest.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflict of interest"
        }
    ]
}