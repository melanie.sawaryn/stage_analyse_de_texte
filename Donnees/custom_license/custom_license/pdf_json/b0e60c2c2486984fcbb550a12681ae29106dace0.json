{
    "paper_id": "b0e60c2c2486984fcbb550a12681ae29106dace0",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Vaccines are predominantly used for prevention; that means they should establish a protection in immunized people or animals which will protect them from a possible infection and the subsequent illness when they come into contact with the respective pathogens. Fundamentally, there are two kinds of immunization: active and passive. The latter is based on the administration of immunoglobulin preparations that can neutralize a specific virus. Therefore, passive vaccination is applied only in special cases, such as when the person to be protected recently had verifiable contact with a specific virus (postexposure prophylaxis), or if the risk of exposure to pathogens cannot be ruled out in the following weeks and an active vaccination is not possible, as in short-term planned trips to Third World countries (exposure prophylaxis). An example is the administration of antibodies specific for hepatitis B virus in cases of contamination with blood from people who have an acute or chronically persistent infection with this virus, and thus have high concentrations of infectious particles in the blood. Such accidents occur primarily in medical personnel by needlestick injury (\u25b6 Sect. 19 .1). In certain cases, the administration is performed in combination with an active vaccination (active-passive immunization). Specific immunoglobulin preparations are also administered when people have been bitten by animals that may be infected with the rabies virus (\u25b6 Sect. 15.1). In the case of early application (together with an active vaccination), the antibodies can neutralize the virus, and impede its spread in the body. Since the time between contact with the virus and its spread in the organism is often very short, passive immunization is limited to a period shortly before or after exposure to the infective agent (usually within 4 days). Therefore, it is reserved for cases in which the contact with the potential pathogen is well documented and the type of infection is known, and when an appropriate immunoglobulin preparation is available. The protection afforded by antibody preparations lasts just a few weeks, as immunoglobulins are rapidly degraded in the organism. Therefore, postexposure administration of active vaccines is increasingly preferred, e.g. in the context of outbreak-control vaccination. In veterinary medicine, passive immunization is employed occasionally in young animals which were born in a flock with high infection pressure. This approach is applied, for example, in kennels when infections occur with canine parvovirus (\u25b6 Sect. 20.1.6). However, its value is controversial, as the immunoglobulins administered hinder the more advantageous active immunization.",
            "cite_spans": [
                {
                    "start": 1181,
                    "end": 1192,
                    "text": "(\u25b6 Sect. 19",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "On the other hand, the active vaccine produces a long-lasting protection against infection, which can even last for life. In this case, a protecting immune response is induced. Ideally, it consists of a combination of neutralizing antibodies and cytotoxic T cells (\u25b6 Chap. 7). The active immunization can be done in two ways: with live vaccines or by using inactivated vaccines. The various methods which are now used to develop vaccines or which are being attempted are illustrated schematically in Fig. 10 .1.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 500,
                    "end": 507,
                    "text": "Fig. 10",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "Live vaccines contain attenuated, replication-competent pathogens that can replicate in the vaccinated person, i.e. they are able to infect certain cells and initiate the synthesis of viral proteins and particles, but without triggering the respective clinical picture. These viral components are recognized by the immune system of the vaccinee as foreign (exogenous), which induces the production of specific neutralizing antibodies and cytotoxic T lymphocytes (\u25b6 Chap. 7). Neutralizing antibodies are predominantly directed against viral surface structures, and can bind to the surface of the virus, thus preventing both attachment of the virus to target cells and infection. The immune complexes of antibodies and virus particles activate the complement system, or are phagocytosed by macrophages and neutrophils. Cytotoxic T cells recognize infected cells by the T-cell receptor on their surface; these infected cells synthesize viral (foreign) proteins and present peptide fragments derived from them in complex with MHC class I antigens. Recognition leads to the destruction of infected cells, and thus to the elimination of the virus from the organism. Live vaccines can develop a very effective protection against a number of viral pathogens, as they effectively activate both the humoral and the cellular mechanisms of the immune system. Therefore, live vaccines are generally the ideal vaccine candidates. However, the implementation of their application requires refrigeration systems for storage, which can lead to difficulties in some developing countries.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "How Do Live Vaccines Work?"
        },
        {
            "text": "Attenuated viruses are similar to pathogenic viruses with respect to structure, protein composition and infection behaviour. However, they clearly differ from them with regard to virulence (\u25b6 Chap. 4). In comparison with the wild-type virus, they usually cause just a limited or weakened infection, which in most cases does not cause clinical symptoms, and can easily be controlled by the immune response of the organism. The proteins produced during the infection are ideally identical to those of the virulent virus strains, or they are at least very similar to them. The fact that viral polypeptides are synthesized in the cells during an attenuated infection leads to the emergence of virus particles which induce the generation of neutralizing antibodies and cytotoxic T cells. Therefore, the immune response that is triggered by attenuated viruses is suitable to induce a long-lasting, effective protection against infections with the respective pathogen.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Attenuated Viruses"
        },
        {
            "text": "The molecular bases of attenuation are mutations in the genome of wild-type viruses. Different genes may be affected in the various vaccine viruses. Frequently, it is not even known why they generate an attenuation of the wild type. One way to attenuate virus strains is their continuous cultivation and passaging in cell culture. In this way, virus mutants are selected which are optimally adapted to the cell culture conditions. Occasionally, they lose their virulence during this process. This method made possible, for example, the isolation of attenuated polio, measles and yellow fever viruses, which do not cause diseases in humans, in contrast to wildtype infections (Table 10 .1, \u25b6 Sects. 14.1, \u25b6 14.5 and \u25b6 15.3). Another example is the vaccine that was successfully applied to protect pigs against infections by classical swine fever virus (\u25b6 Sect. 14.5). In this case, the virulent virus was attenuated by continuous passages in rabbit cells. Subsequently, these lapinized viruses were no longer able to trigger diseases in swine. Meanwhile, the application of this originally very successful vaccine has been banned. The justification for this lies particularly in trade. On the one hand, infections with the attenuated vaccine virus could not be distinguished from infections with classical swine fever virus in blood tests; on the other hand, following a vaccination, an export ban is imposed for a limited period, which, however, has economically adverse effects. Instead, it is attempted to interrupt the infection chain and to eliminate the virus from the population by culling all herds in which classical swine fever occurs (\u25b6 Chap. 11, \u25b6 Sect. 14.2). By transgressing the species barrier, the infection can occasionally adopt a weakened character. Similarly, vaccinia viruses that were originally used to produce a protective immune response against smallpox virus induced local infections in humans, which in very rare cases had a generalized or fatal course. Because of this problem, further attenuation of vaccinia virus was sought by repeated passages in chicken cells. In fact, this approach resulted in modified vaccinia Ankara virus (\u25b6 Sect. 19.6), which exhibits fewer adverse side effects, and is currently being tested as a vector system for expression of recombinant vaccines which are to be used, for example, in human immunodeficiency virus (HIV) patients in the context of therapeutic vaccinations.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 675,
                    "end": 684,
                    "text": "(Table 10",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Attenuated Viruses"
        },
        {
            "text": "Vaccinations with attenuated live viruses usually confer very good protection, which remains for a long time. Booster vaccinations at relatively long intervals of up to 10 years can provide continuous protection. However, attenuated viruses entail the risk that they can lose their reproduction capability and thus their efficiency because of inadequate refrigeration systems, or they can mutate back to the wild type during the weakened infection. Therefore, attention is now paid to the fact that attenuation should be based on multiple independent mutations, which largely excludes back mutation to the pathogenic wild type. However, attenuated vaccine viruses should be used only in immunologically healthy people. In immunocompromised individuals, these types of viruses can persist for a long time, and result in symptomatic infections. Since attenuated live vaccine viruses can sometimes be transmitted from vaccinees to other people in their immediate environment (e.g. polio vaccination; \u25b6 Sect. 14.1), this restriction also applies to immunosuppressed people in the family and the surroundings. Furthermore, live vaccines should not be used during pregnancy. It is also important to emphasize that attenuation is defined only for the affected host species in which it was tested, and application of a live vaccine in species other than that in which the evaluation occurred is not permitted. This has to be considered for vaccinations of zoo and wild animals, and the unquestioned use of live vaccines should be avoided as far as possible.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Attenuated Viruses"
        },
        {
            "text": "In particular, the use of live vaccines is prohibited when infections with the pathogenic wild type are absent or occur very rarely in a population. This status has been achieved by successful vaccination programmes, e.g. worldwide for smallpox virus and in Europe and North America for poliovirus. In these cases, the risk associated with vaccination is greater than that of being infected with the wild-type virus and of becoming ill. Therefore, in the case of smallpox virus, vaccination has been stopped, and to protect against poliovirus, an inactivated vaccine is being used today (\u25b6 Sects. 14.1 and \u25b6 19.6). Every active vaccination entails a certain rate of known harmless side effects. However, real complications (such as permanent nerve damage) are very rare events that occur with a prevalence of approximately one in a million vaccinations in adults. Therefore, every vaccination must be accompanied by corresponding obligatory medical information and documentation.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Attenuated Viruses"
        },
        {
            "text": "Today, recombinant viruses represent a controversially discussed variant of live vaccines. It is being attempted to modify well-explored, less pathogenic viruses (e.g. adenoviruses) and vaccine viruses that were used successfully in the past (usually vaccinia viruses) by using genetic engineering methods in such a way that they encode proteins of other viral species, in addition to their own gene products necessary for infection and replication (\u25b6 Sects. 19.4 and \u25b6 19.6). After viral inoculation, these foreign genes are expressed by recombinant viruses along with their own genes in an organism during infection. This induces an immune response against both the vaccinia virus or adenovirus proteins and the heterologous polypeptides. These recombinant, replication-competent viruses offer all the advantages of live vaccines (antibodies and cell-mediated immune response) and are, therefore, also increasingly used in therapeutic immunizations, i.e. in cases where an infection has already occurred. Vaccination should stimulate the immune response to influence the progression of the disease favourably.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Recombinant Viruses"
        },
        {
            "text": "Large DNA fragments cannot be cloned into the genome of vaccinia virus despite its size. Therefore, it is essential to know exactly which proteins of the virus are important for the elicitation of a protective immune response in order to produce a vaccine against the viral infection. The pertinent gene is cloned into the genome of the vaccinia virus in such a way that it is under the control of an early vaccinia virus specific promoter. Such a vaccine cannot generate the entire spectrum of an immune response that arises during an infection with the wild-type virus or its attenuated variant. In recombinant vaccine viruses, the immune response is confined to a specific protein. Such vaccines have been extensively employed in North America (USA, Canada) and Europe (Belgium, France, Spain) to protect raccoons and foxes from rabies. The genome of the recombinant vaccinia virus harbours the genetic information coding for the G protein of rabies virus (\u25b6 Sect. 15.1). With use of these recombinant viruses, meat-based artificial baits are prepared and scattered for foxes. The animals bite the virus-containing plastic capsule when devouring the bait, whereby they become infected with the vaccine virus, and develop an immune protection against rabies virus infections. A vaccine based on recombinant vaccinia viruses or adenoviruses has not been approved for human use yet, but is being discussed for prevention or treatment of infections with HIV, and is being tested experimentally.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Recombinant Viruses"
        },
        {
            "text": "According to their definition, inactivated vaccines are not able to proliferate in the vaccinated organism. They induce predominantly antibody responses. Because of the lack of active viral protein synthesis, a cytotoxic T-cell response occurs rarely. When inactivated vaccines are used, an initial immunization (two or three vaccinations) and booster vaccinations are necessary to maintain the immune protection.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "How Do Inactivated Vaccines Stimulate the Immune System, and what Types Are in Use or in Clinical Trials?"
        },
        {
            "text": "To enhance the immune response, these vaccines have to be applied with an adjuvant, which facilitates the migration of macrophages, monocytes, and B and T lymphocytes to the inoculation site. Aluminium hydroxide, aluminium hydroxyphosphate sulphate in combination with deacylated monophosphoryl lipid A components and certain toxoids (e.g. tetanus toxoid) are adjuvants approved for use in humans. Other adjuvants are also used in animals. A widely used adjuvant is Quil-A, a saponin that is also used for the production of immune-stimulating complexes. However, fibrosarcomas have recently been observed at the injection site in the application of inactivated vaccines (rabies virus and feline leukaemia virus) in cats. Whether this is elicited by the adjuvant or by the trauma of injection has not been conclusively elucidated. For experimental immunizations, only incomplete Freund's adjuvant is used in animals.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "How Do Inactivated Vaccines Stimulate the Immune System, and what Types Are in Use or in Clinical Trials?"
        },
        {
            "text": "The simplest form of an inactivated vaccine is a preparation of wild-type viruses that have been effectively killed by treatment with chemicals. This inactivation is usually done with aldehydic or alcoholic agents. Frequently, b-propiolactone is also used. The protein components must not be denatured to the extent that they lose their native configuration and are no longer similar to the genuine viral structures.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Inactivated Pathogens"
        },
        {
            "text": "Since the viral nucleic acid is per se frequently infectious and can lead to the production of progeny viruses, methods must be used to eliminate the infectivity which result in the degradation of the nucleic acid of the virus. For example, the currently commonly used vaccines against influenza virus or hepatitis A virus infections (Table 10 .1) are based on inactivated pathogens.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 334,
                    "end": 343,
                    "text": "(Table 10",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Inactivated Pathogens"
        },
        {
            "text": "Relatively new vaccines are based solely on a selected protein component of the virus. A fundamental prerequisite for the development of such a vaccine is detailed knowledge of the immunologically important components of a pathogen. If it is known against which of the viral proteins (usually surface proteins) a protective immune response is induced, the respective coding gene can be cloned into a eukaryotic expression system. From this vector, the protein can be synthesized, purified and finally administered with an adjuvant. Structural particle proteins are especially suitable for the induction of a protective immune response. An example of this is HBsAg, the surface protein of hepatitis B virus, which aggregates to vesicular particles when it is artificially expressed in yeast or other eukaryotic cells.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Use of Selected Proteins of Pathogens"
        },
        {
            "text": "HBsAg particles can induce, largely independently of adjuvants, the production of hepatitis B virus neutralizing antibodies and even cytotoxic T cells (\u25b6 Sect. 19.1). Similarly, the capsid proteins L1 and L2 of papillomaviruses assemble together into particles that resemble the infectious viruses. The newly approved vaccines against infections with oncogenic human papillomaviruses (against human papillomaviruses 16 and 18) are based on such virus-like L1 particles, which -like the aforementioned HBsAg particles -are produced in yeast cells using genetic engineering methods (\u25b6 Sect. 19.3). The particle-forming Gag proteins of HIV also seem to be very appropriate for immunization. Another variant of this type of a subunit vaccine has been in use in veterinary medicine for many years. It is a vaccine against feline leukaemia virus, a retrovirus (\u25b6 Sect. 18.1). It contains only the external glycoprotein (gp70) of subtype A of this virus as an immunologically relevant component, which is produced and purified in Escherichia coli cells in a non-glycosylated form by genetic engineering methods.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Use of Selected Proteins of Pathogens"
        },
        {
            "text": "Vaccines consisting of synthetic peptides with a length of 15-30 amino acids represent an additional form of vaccine which is currently being tested. In this case, individual epitopes of viral proteins, which induce the production of neutralizing antibodies and also activation of T cells, are selected and chemically synthesized. One advantage is that these vaccines are devoid of nucleic acids and that they can be produced in large quantities with relatively little effort. In animal studies, the protective effect of synthetic peptide vaccines has been demonstrated against infections both with foot-and-mouth disease virus (\u25b6 Sect. 14.1) and with canine parvovirus (\u25b6 Sect. 20.1). Prerequisite for the development of such a vaccine is also in this case detailed knowledge of the protein regions that can elicit a virus-neutralizing immune response. However, it seems rather doubtful that a single epitope is able to do that in the long term, as most viruses have high genetic variability. Furthermore, single individuals have different abilities for immunological recognition of specific protein regions. This is interrelated with the distinct MHC phenotype of each individual. In a vaccine based on synthetic peptides, several different epitopes ought to be combined and applied with a suitable adjuvant. A vaccine based on synthetic peptides has not been approved yet.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Synthetic Peptide Vaccines"
        },
        {
            "text": "Another new vaccine type is administered as DNA. The nucleic acid contains the genes of a virus that are capable of inducing a protective immune response, i.e. usually the regions that encode surface components of a pathogen. They are cloned into a vector system together with promoter elements, which regulate their expression, and are injected intramuscularly as purified DNA. Particularly in muscle cells, the DNA can be detected over long periods as an episome. Apparently, it is degraded very slowly there. If the corresponding genes are expressed, the organism can apparently develop both a humoral and a cellular immune response. This type of vaccine has been tested predominantly in animal systems. In animal studies, a protective effect of these vaccines has been demonstrated against a variety of different animal pathogenic viruses, such as paramyxovirus, parvovirus and herpesvirus. However, large amounts of DNA and a number of booster injections are necessary. A faster degradation of the nucleic acid has also been observed. However, the inherent possibility of integration of DNA sequences which were applied with the vaccine into the cellular genome and the resulting endangerment to the vaccinee are additional problems to consider in the discussion of the harmlessness of DNA vaccines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DNA Vaccines"
        },
        {
            "text": "Viruses with RNA genomes present a special challenge for the development of vaccines using genetic engineering methods. Inasmuch as the techniques for targeted introduction of mutations and expression of foreign genes in prokaryotic and eukaryotic systems are based on DNA as the initial nucleic acid, the genetic information of RNA viruses must first be transcribed into double-stranded DNA. Only then does it become accessible for genetic engineering manipulations. Then, targeted mutations can be selectively introduced into the nucleic acid sequence of such DNA constructs, e.g. to generate attenuated vaccine viruses. In this case, the genome of RNA viruses, which were previously transcribed into DNA, must be present in a form that subsequently allows the synthesis of infectious viruses. This process describes the methods of reverse genetics, by which it is being attempted, for example, to construct attenuated live vaccines against respiratory syncytial virus and some other paramyxoviruses (\u25b6 Sect. 15.3). Viruses with a segmented RNA genome, such as influenza viruses, represent a particular challenge. In this case, the genetic information of all segments of the viral RNA genome must be transcribed into DNA and cloned into appropriate eukaryotic expression vectors. After the combined synthesis of all viral proteins and the production of new genome segments, recombinant viruses can subsequently be produced and cultivated in appropriate cell culture systems. An attenuation of the virus is possible by targeted mutagenesis. In the future, this will facilitate faster adaptation of new vaccine strains to new influenza viruses (\u25b6 Sect. 16.3). In this case, it is also being attempted to develop novel vaccines rapidly by using prefabricated genomic libraries or universal vaccines in order to cover all new influenza virus subtypes before they have pandemic dimensions.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Reverse Genetic Methods: An Innovation in Vaccine Development"
        },
        {
            "text": "Marker vaccines are of great importance in veterinary medicine. They allow the distinction of vaccinated animals from field-virus-infected animals (wild-type virus) by using simple serological methods (\u25b6 Chap. 13). These vaccines are also referred to as DIVA vaccines (for \"differentiating between infection and vaccination\"). In principle, a differentiation is made between negative and positive marker vaccines. In the first case, the vaccine virus lacks a gene and the respective protein, so the vaccinated animals do not develop immunological reactions against the respective protein. The distinction of field-virus-infected animals is performed by determining the specific antibodies directed against this protein. Prerequisites for the success of this approach are that the missing protein does not exert essential functions during viral replication, and that sufficient specific antibodies against the relevant protein are produced during an infection with wild-type viruses. In veterinary medicine, such negative marker vaccines are administered in the control of two economically important herpesvirus infections, namely Aujeszky's disease in swine and infectious bovine rhinotracheitis in cattle (Table 10 .1, \u25b6 Sect. 19.5). Positive marker vaccines are vaccine viruses that are characterized by a unique marker (nucleic acid sequence). In cases of vaccination failure, i.e. in cases in which an infection is established in spite of vaccination, positive marker vaccines facilitate the easy identification of vaccine viruses. ",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 1206,
                    "end": 1215,
                    "text": "(Table 10",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Marker Vaccines"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Recombinant and synthetic vaccines",
            "authors": [
                {
                    "first": "W ;",
                    "middle": [],
                    "last": "Jilg",
                    "suffix": ""
                },
                {
                    "first": "Landsberg",
                    "middle": [],
                    "last": "Ecomed",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Plotkin",
                    "suffix": ""
                },
                {
                    "first": "Ea ;",
                    "middle": [],
                    "last": "Mortimer",
                    "suffix": ""
                },
                {
                    "first": "Philadelphia",
                    "middle": [],
                    "last": "Saunders",
                    "suffix": ""
                },
                {
                    "first": "U",
                    "middle": [],
                    "last": "Quast",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Thilo",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Fescharek",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Impfreaktionen. Bewertung und Differentialdiagnose",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "(1994) Strategies in vaccine design. Landes, Austin Bankston J (2001) Jonas Salk and the polio vaccine. Mitchell Lane, Bear Day MJ, Schoon H-A, Magnol JP, Saik J, Devauchelle P, Truyen U, Gruffydd-Jones TJ, Cozette V, Jas D, Poulet H, Pollmeier M, Thibault JC (2007) A kinetic study of histopathological changes in the subcutis of cats injected with nonadjuvanted and adjuvanted multi-component vaccines. Vaccine 25:4073-4084 Ellis RW (1992) Vaccines: new approaches to immunological problems. Butterworth-Heinemann, Boston",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Fig. 10.1 Different ways to develop vaccines 10.1 How Do Live Vaccines Work?",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Important vaccines for prevention of viral infections. The vaccines listed are licensed and distributed worldwide. In addition, several vaccines exist for use in humans or animals that are exclusively used in some countries. These vaccines are able to induce protection against infectious agents that not ubiquitous but are restricted to defined regions and states. For further information, the respective public health authorities should be contacted",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "Smallpox vaccination was stopped worldwide on the recommendation of the World Health Organization in 1979Rinderpest vaccination was stopped world wide on the recommendation of the World Health Organisation for Animal Health in 2006",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}