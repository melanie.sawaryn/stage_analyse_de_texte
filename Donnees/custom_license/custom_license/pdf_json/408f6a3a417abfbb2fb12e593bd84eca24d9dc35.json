{
    "paper_id": "408f6a3a417abfbb2fb12e593bd84eca24d9dc35",
    "metadata": {
        "title": "Online Supplementary Material Pacing a small cage: mutation and RNA viruses Quasispecies modelling",
        "authors": [
            {
                "first": "Robert",
                "middle": [],
                "last": "Belshaw",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Oxford",
                    "location": {
                        "postCode": "OX1 3PS",
                        "settlement": "Oxford",
                        "country": "UK"
                    }
                },
                "email": "r.robert.belshaw@zoo.ox.ac.uk"
            },
            {
                "first": "Andy",
                "middle": [],
                "last": "Gardner",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Edinburgh",
                    "location": {
                        "postCode": "EH9 3JT",
                        "settlement": "Edinburgh",
                        "country": "UK"
                    }
                },
                "email": ""
            },
            {
                "first": "Andrew",
                "middle": [],
                "last": "Rambaut",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Edinburgh",
                    "location": {
                        "postCode": "EH9 3JT",
                        "settlement": "Edinburgh",
                        "country": "UK"
                    }
                },
                "email": ""
            },
            {
                "first": "Oliver",
                "middle": [
                    "G"
                ],
                "last": "Pybus",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Oxford",
                    "location": {
                        "postCode": "OX1 3PS",
                        "settlement": "Oxford",
                        "country": "UK"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Let \u03b1 be the wildtype replication rate, \u03b2 the mutant replication rate (we assume \u03b1 > \u03b2), \u03b4 the per capita death rate (we assume \u03b1 > \u03b4, so that the unmutated wildtype population has a net positive rate of increase), and Q the probability of error-free replication (the probability that copies of the wildtype are also of the wildtype form rather than a mutated form). Then, denoting time as t, growth in numbers of wildtype (x) and mutants (y) can be written as:",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "= \"Qx t # $ x t , and ! dy dt = \" 1# Q",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "( ) x t + $y t # % y t .",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "It is helpful to define new variables: (1) the total population size, n t = x t + y t ; (2) the frequency of the wildtype, p t = x t /n t . The change in these variables is given by:",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The error threshold describes the minimum degree of replication fidelity that is required in order for the frequency of the wildtype to be maintained above zero (p > 0). To determine this, we find the equilibrium frequency of the wildtype by setting dp/dt = 0 and solve for pt = p*. This obtains p* = (\u03b1 Q\u03b2)/(\u03b1\u03b2), which is positive only if Q > \u03b2/\u03b1. Thus, the error threshold is at Q = \u03b2/\u03b1, and if the proportion of replication that is error-free is less than this ratio of mutant and wildtype replication rates, the wildtype cannot be maintained at a nonzero frequency.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The idea of lethal mutagenesis is that the probability of error-free replication may be too low to support a net positive growth rate for the population so that, irrespective of whether or not the wildtype can be maintained at a nonzero frequency, the population comprising both wildtype and mutants will be driven to extinction. If \u03b2 > \u03b4 then there can be no lethal mutagenesis since even a population comprising only mutants still maintains a net positive growth rate. If \u03b2 < \u03b4 and if the error threshold is crossed, so that Q < \u03b2/\u03b1, then population inevitably loses the wildype, the population will become extinct because the mutant population has a net negative growth. However, if \u03b2 < \u03b4 but the error threshold is not crossed, so that Q > \u03b2/\u03b1, then the wildtype may be maintained at a positive frequency yet the population could have a negative growth rate and become extinct, or the population could have a positive growth rate and hence be maintained. We determine the outcome by substituting the equilibrium wildtype frequency p* = (\u03b1 Q\u03b2)/(\u03b1\u03b2) into the equation for change in population size, and this yields:",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Hence, the net growth of the population is positive if \u03b1Q\u03b4 > 0, i.e. if Q > \u03b4/\u03b1. Thus, even if the error threshold is not crossed, the population will go extinct when Q < \u03b4/\u03b1. When the mutant replication rate is insufficient to maintain a wholly-mutated population (i.e. \u03b2\u03b4 < 0), then lethal mutagenesis imposes a stricter requirement (Q > \u03b4/\u03b1) for error-free replication than does the error threshold (Q > \u03b2/\u03b1).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Regarding the relationship between genome size and mutation rate, we assume that Q can be represented by the proportion of progeny that have no mutations in a Poisson distribution:",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "where p is the probability of mutation per base and m is the sequence length in bases.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "At the Error Threshold, the replication advantage of the wild-type (fittest) sequence just compensates for its loss through mutation (i.e. Q = \u03b2/\u03b1). Thus:",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "We can rewrite this equation as follows:",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Given that the experimental data suggests that ln(\u03b1/\u03b2) approximates to one (Sanjuan et al. 2004) , the predicted maximum length of the virus is the inverse of its mutation rate -which is approximately what we observe.",
            "cite_spans": [
                {
                    "start": 75,
                    "end": 96,
                    "text": "(Sanjuan et al. 2004)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "With only two exceptions, families of RNA viruses attack eukaryote hosts, whose ribosomes typically enable only one polypeptide to be produced from each mRNA molecule. Evolving a large, complex genome might thus be difficult for RNA viruses because they also have to evolve mechanisms to allow additional genes to be translated (they lack the 'automatic' separation of genome replication and gene transcription/translation found in the DNA world). RNA viruses appear to have acquired varied strategies to cope with this (Belshaw et al. 2007 ). The negative strand mononegavirales have multiple internal transcription starts (Pringle & Easton 1997) and some positive strand viruses also produce varied sub-genomic RNAs by truncation with internal transcription sites (togaviruses) or polymerase skipping (coronavirus). Another method is segmentation of the genome, so that it is composed of multiple RNAs. Consistent with the number of transcription start points affecting genome size, larger viruses are more likely to be segmented ( Figure S1 ; Mann-Whitney U test; P = 0.036), and among segmented viruses, larger viruses tend to have more segments (Spearman Rank Correlation; P < 0.001).",
            "cite_spans": [
                {
                    "start": 520,
                    "end": 540,
                    "text": "(Belshaw et al. 2007",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 624,
                    "end": 647,
                    "text": "(Pringle & Easton 1997)",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [
                {
                    "start": 1034,
                    "end": 1043,
                    "text": "Figure S1",
                    "ref_id": null
                }
            ],
            "section": "Limited transcription starts"
        },
        {
            "text": "There is another, older explanation for segmentation in RNA viruses, namely a mechanism for coping with their high mutation rate in the way that recombination in higher organisms was thought to be a mechanism for reducing mutational load (Pressing & Reanney 1984) . Figure S1 . Tendency of segmentation to be associated with RNA viruses of longer (total) genome length. Each data point is the mean of an RNA family or unassigned genus or species. Data from our website at http://virus.zoo.ox.ac.uk/virus/index.html.",
            "cite_spans": [
                {
                    "start": 238,
                    "end": 263,
                    "text": "(Pressing & Reanney 1984)",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [
                {
                    "start": 266,
                    "end": 275,
                    "text": "Figure S1",
                    "ref_id": null
                }
            ],
            "section": "Limited transcription starts"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The evolution of genome compression and genomic novelty in RNA viruses",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Belshaw",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Genome Res",
            "volume": "17",
            "issn": "",
            "pages": "1496--1504",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Divided genomes and intrinsic noise",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Pressing",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "C"
                    ],
                    "last": "Reanney",
                    "suffix": ""
                }
            ],
            "year": 1984,
            "venue": "J. Mol. Evol",
            "volume": "20",
            "issn": "",
            "pages": "135--146",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Monopartite negative strand RNA genomes",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "R"
                    ],
                    "last": "Pringle",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Easton",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "Semin. Virol",
            "volume": "8",
            "issn": "",
            "pages": "49--57",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "The distribution of fitness effects caused by single-nucleotide substitutions in an RNA virus",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Sanjuan",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Proc. Natl. Acad. Sci. USA",
            "volume": "101",
            "issn": "",
            "pages": "8396--8401",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}