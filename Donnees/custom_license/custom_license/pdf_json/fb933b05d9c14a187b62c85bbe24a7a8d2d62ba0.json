{
    "paper_id": "fb933b05d9c14a187b62c85bbe24a7a8d2d62ba0",
    "metadata": {
        "title": "The Journal of Infectious Diseases Discovery of T-Cell Infection and Apoptosis by Middle East Respiratory Syndrome Coronavirus",
        "authors": [
            {
                "first": "Tianlei",
                "middle": [],
                "last": "Ying",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Fudan University",
                    "location": {
                        "country": "China;"
                    }
                },
                "email": ""
            },
            {
                "first": "Wei",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institutes of Health",
                    "location": {
                        "addrLine": "131 Dong An Rd",
                        "postCode": "200032",
                        "settlement": "Frederick, Shanghai",
                        "region": "Maryland",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Dimiter",
                "middle": [
                    "S"
                ],
                "last": "Dimitrov",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Institutes of Health",
                    "location": {
                        "addrLine": "131 Dong An Rd",
                        "postCode": "200032",
                        "settlement": "Frederick, Shanghai",
                        "region": "Maryland",
                        "country": "China"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The links between human immunity and MERS-CoV infection and progression have been well established. Like SARS-CoV infections, MERS-CoV infections occur more frequently in immunocompromised individuals, and patients who survive MERS-CoV infection usually have better immune responses than those who die [4] . MERS-CoV can occasionally be detected in patients' blood and urine, and MERS-CoV-infected patients have substantial abnormal hematological findings, including elevated leukocyte numbers and lymphopenia, thrombocytopenia, and coagulopathy [5] , suggesting virus infiltration of circulating blood and lymphoid cells. Dysregulation of cytokines and chemokines can also be observed in MERS-CoV-infected patients [3] . These findings suggest that invasion of the human immune system, followed by the dysregulation of cytokines, might aggravate MERS-CoV infection.",
            "cite_spans": [
                {
                    "start": 302,
                    "end": 305,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 546,
                    "end": 549,
                    "text": "[5]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 716,
                    "end": 719,
                    "text": "[3]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A well-orchestrated innate and adaptive immune response is a prerequisite for effective defense against most viral infections. MERS-CoV uses various methods to inhibit host antiviral innate immune responses. First, MERS-CoV papain-like nsp3, accessory proteins 4a, 4b, 5, and M protein could antagonize interferon \u03b1/\u03b2 (IFN-\u03b1/\u03b2) expression by either binding to cytosolic pattern-recognition receptors or interfering with the downstream transcription factors [6] [7] [8] . MERS-CoV markedly decreases antiviral IFN levels in primary human lower respiratory tract cell lines and bronchial epithelium [9] . Patients with fatal MERS usually express fewer type I IFNs than those who survive [10] . Second, MERS-CoV can directly infect and replicate productively in macrophages [11] and dendritic cells [12] , which results in dysregulations in the cytokine and antigen-presentation pathways. Finally, MERS-CoV could persistently induce the expression of proinflammatory cytokines which are associated with chemotaxis and activation of neutrophils associated with peripheral damage to the surrounding or distant uninfected tissues [10] . Therefore, MERS-CoV could antagonize or dysregulate human innate immunosurveillance on multiple levels.",
            "cite_spans": [
                {
                    "start": 457,
                    "end": 460,
                    "text": "[6]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 461,
                    "end": 464,
                    "text": "[7]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 465,
                    "end": 468,
                    "text": "[8]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 597,
                    "end": 600,
                    "text": "[9]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 685,
                    "end": 689,
                    "text": "[10]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 771,
                    "end": 775,
                    "text": "[11]",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 796,
                    "end": 800,
                    "text": "[12]",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1124,
                    "end": 1128,
                    "text": "[10]",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "For adaptive immunity, the important roles of T cells in the surveillance and clearance of MERS-CoV have been well demonstrated by using Ad5-hDPP4transduced mice deficient in T cells [13] and by using an immunosuppressed rhesus macaques model [14] . Indeed, the MERS-CoV infection site could recruit T cells by secretion of monocyte chemoattractant protein 1, CXCL10, and interleukin 10 stimulated by type I IFN. However, the expression of these cytokines is uncontrolled, and their persistent expression will inhibit the expression of interleukin 12 and IFN-\u03b3, which are required for the activation of T-helper cells [10] . This, along with the downregulation of antigenpresentation pathways (decreased levels of major histocompatibility complex class I and II costimulatory molecules) as demonstrated in MERS-CoV-infected Calu-3 cells [15] , would strongly inhibit the activation of T cells. Therefore, T cells are sequestered in the infected tissues but fail to be activated to target the virus.",
            "cite_spans": [
                {
                    "start": 183,
                    "end": 187,
                    "text": "[13]",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 243,
                    "end": 247,
                    "text": "[14]",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 618,
                    "end": 622,
                    "text": "[10]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 837,
                    "end": 841,
                    "text": "[15]",
                    "ref_id": "BIBREF15"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "These T cells, according to the elegant study by Hin Chu et al, are highly susceptible to MERS-CoV infection and the subsequent MERS-CoV-induced apoptosis. By elaborate in vitro, ex vivo, and animal studies, the authors unambiguously showed that MERS-CoV but not SARS-CoV could effectively infect both CD4 + and CD8 + T cells from human peripheral blood mononuclear cells, human lymphoid organs (tonsil and spleen), and the spleen of the infected common marmosets. Consequently, the infiltrated T cells undergo substantial apoptosis involving intrinsic and extrinsic apoptotic pathways, although their infection by MERS-CoV seems to be abortive. Their seminal findings could give a novel perspective for the immunopathogenesis of MERS-CoV and could partly explain the lymphopenia observed in MERS-CoVinfected patients. The inability of the SARS-CoV to infect T cells may be ascribed to the lower angiotensin-converting enzyme 2 expression in T cells, which may provide a mechanism that helps explain why MERS-CoV infection causes moresevere immunological manifestations, a poorer clinical prognosis, and a higher mortality rate than SARS-CoV infections.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "An interesting observation by Hin Chu et al is the significant downregulation of DPP4 by MERS-CoV infection, which may further impair T-cell functions, since DPP4 is believed to play important roles in T cells signaling, transduction, activation, and costimulation [16] . In addition, this result may hint that MERS-CoV infection of T cells probably involves endocytic pathways via the endosome but not via direct plasma membrane fusion. The endocytic pathway will cointernalize DPP4 with MERS-CoV S protein into the endosome, which could explain the decreased surface associated DPP4 after infection. This endosome-dependent pathway for T cells contrasts with other cell types, such as human lung epithelial cells, in which MERS-CoV could directly transport itself across the cell membrane after binding of the viral spike glycoprotein to DPP4 [17] . These different pathways for viral entry may be associated with different IFN secretion profiles [18] .",
            "cite_spans": [
                {
                    "start": 265,
                    "end": 269,
                    "text": "[16]",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 845,
                    "end": 849,
                    "text": "[17]",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 949,
                    "end": 953,
                    "text": "[18]",
                    "ref_id": "BIBREF18"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Another interesting finding is that the intrinsic and extrinsic apoptotic pathways are both activated in MERS-CoV-infected T cells. For Vero E6 cells, which are used as effective producers of MERS-CoV progeny, the MERS-CoV-induced apoptosis is gradual and dependent on effective MERS-CoV replication. In contrast to the apoptosis of Vero E6 cells, MERS-CoVinduced T-cell apoptosis seems to be independent of virus replication. The apoptosis involves activation of the extrinsic and intrinsic apoptosis pathways, which might be important in the pathogenesis of MERS.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The study by Hin Chu et al highlights several important areas for future research. First, to what extent does apoptosis of T cells contribute to increased mortality? This question is difficult to answer in the absence of animal models that closely resemble the pathology seen in humans, although common marmosets could be useful in this regard. Second, the detailed apoptotic pathways could be further investigated, which would facilitate the development of antiapoptotic therapeutic reagents. Third, since T cells could be a major source of cytokines and chemokines, the cytokine profiles of T cells during MERS-CoV infection could be scrutinized and the results correlated with the severe cytokine release syndrome observed in patients with MERS. Finally, the study showed that CD4 + helper T cells are more susceptible to MERS-CoV infection, which could be related to impairment of B-cell function. An interesting question is whether the number of CD4 + T cells declines. A certain parallel could be made with acute human immunodeficiency virus type 1 infection, in which the number of CD4 + T cells in the blood declines, likely owing to their killing by the virus and cell redistribution. Clarification of these questions would allow further dissection of the complex MERS-CoV pathogenesis, with important implications for the development of therapeutics and vaccines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Financial support. This work was supported by the Center for Cancer Research, National Cancer Institute, National Institutes of Health (intramural program), and the National Science and Technology Major Project of China (2012ZX10002002).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Notes"
        },
        {
            "text": "Potential conflicts of interest. All authors: No reported conflicts. All authors have submitted the ICMJE Form for Disclosure of Potential Conflicts of Interest. Conflicts that the editors consider relevant to the content of the manuscript have been disclosed.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Notes"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "World Health Organization. Middle East respiratory syndrome coronavirus (MERS-CoV)-Republic of",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Molecular pathology of emerging coronavirus infections",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "E"
                    ],
                    "last": "Gralinski",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Baric",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Pathol",
            "volume": "235",
            "issn": "",
            "pages": "185--95",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Pathogenesis of Middle East respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Van Den Brand",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "Smits",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "L"
                    ],
                    "last": "Haagmans",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Pathol",
            "volume": "235",
            "issn": "",
            "pages": "175--84",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Clinical course and outcomes of critically ill patients with Middle East respiratory syndrome coronavirus infection",
            "authors": [
                {
                    "first": "Y",
                    "middle": [
                        "M"
                    ],
                    "last": "Arabi",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "A"
                    ],
                    "last": "Arifi",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "H"
                    ],
                    "last": "Balkhy",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Ann Intern Med",
            "volume": "160",
            "issn": "",
            "pages": "389--97",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Epidemiological, demographic, and clinical characteristics of 47 cases of Middle East respiratory syndrome coronavirus disease from Saudi Arabia: a descriptive study",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Assiri",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Al-Tawfiq",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "A"
                    ],
                    "last": "Al-Rabeeah",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Lancet Infect Dis",
            "volume": "13",
            "issn": "",
            "pages": "752--61",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Middle east respiratory syndrome coronavirus 4a protein is a doublestranded RNA-binding protein that suppresses PACT-induced activation of RIG-I and MDA5 in the innate antiviral response",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "L"
                    ],
                    "last": "Siu",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Yeung",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "H"
                    ],
                    "last": "Kok",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "J Virol",
            "volume": "88",
            "issn": "",
            "pages": "4866--76",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "The structural and accessory proteins M, ORF 4a, ORF 4b, and ORF 5 of Middle East respiratory syndrome coronavirus (MERS-CoV) are potent interferon antagonists",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Geng",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Protein Cell",
            "volume": "4",
            "issn": "",
            "pages": "951--61",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Middle East respiratory syndrome coronavirus accessory protein 4a is a type I interferon antagonist",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Niemeyer",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Zillinger",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Muth",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Virol",
            "volume": "87",
            "issn": "",
            "pages": "12489--95",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Human cell tropism and innate immune system interactions of human respiratory coronavirus EMC compared to those of severe acute respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zielecki",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Weber",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Eickmann",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Virol",
            "volume": "87",
            "issn": "",
            "pages": "5300--5304",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Distinct immune response in two MERS-CoV-infected patients: can we go from bench to bedside?",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Faure",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Poissy",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Goffard",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "PLoS One",
            "volume": "9",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Active replication of Middle East respiratory syndrome coronavirus and aberrant induction of inflammatory cytokines and chemokines in human macrophages: implications for pathogenesis",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "J Infect Dis",
            "volume": "209",
            "issn": "",
            "pages": "1331--1373",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Productive replication of Middle East respiratory syndrome coronavirus in monocyte-derived dendritic cells modulates innate immune response",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "H"
                    ],
                    "last": "Wong",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Virology",
            "volume": "",
            "issn": "",
            "pages": "454--455",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Rapid generation of a mouse model for Middle East respiratory syndrome",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wohlford-Lenane",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "111",
            "issn": "",
            "pages": "4970--4975",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Defining the effects of immunosuppression in the rhesus model of Middle East respiratory syndrome (MERS)",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Prescott J Dwe",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Falzarano",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "P"
                    ],
                    "last": "Scott",
                    "suffix": ""
                },
                {
                    "first": "Feldmann",
                    "middle": [],
                    "last": "Hmunster",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "J"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Presented at: 33rd Annual Meeting of the American Society for Virology",
            "volume": "",
            "issn": "",
            "pages": "21--25",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Cell host response to infection with novel human coronavirus EMC predicts potential antivirals and important differences with SARS coronavirus",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Josset",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "D"
                    ],
                    "last": "Menachery",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "E"
                    ],
                    "last": "Gralinski",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "mBio",
            "volume": "4",
            "issn": "",
            "pages": "165--00113",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "CD26-mediated signaling for T cell activation occurs in lipid rafts through its association with CD45RO",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ishii",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Ohnuma",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Murakami",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "98",
            "issn": "",
            "pages": "12138--12181",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Middle East respiratory syndrome coronavirus infection mediated by the transmembrane serine protease TMPRSS2",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Shirato",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Kawase",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Matsuyama",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Virol",
            "volume": "87",
            "issn": "",
            "pages": "12552--61",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "High secretion of interferons by human plasmacytoid dendritic cells upon recognition of Middle East respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "V",
                    "middle": [
                        "A"
                    ],
                    "last": "Scheuplein",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Seifried",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "H"
                    ],
                    "last": "Malczyk",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "J Virol",
            "volume": "89",
            "issn": "",
            "pages": "3859--69",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}