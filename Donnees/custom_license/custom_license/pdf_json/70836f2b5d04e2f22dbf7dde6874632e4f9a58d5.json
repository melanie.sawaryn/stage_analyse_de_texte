{
    "paper_id": "70836f2b5d04e2f22dbf7dde6874632e4f9a58d5",
    "metadata": {
        "title": "IDENTIFICATION AND CHARACTERIZATION OF SEVERE ACUTE RESPIRATORY SYNDROME CORONAVIRUS SUBGENOMIC RNAs",
        "authors": [
            {
                "first": "Snawar",
                "middle": [],
                "last": "Hussain",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Ji",
                "middle": [
                    "&apos;"
                ],
                "last": "An Pan",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Jing",
                "middle": [],
                "last": "Xu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Yalin",
                "middle": [],
                "last": "Yang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Yu",
                "middle": [],
                "last": "Chen",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Yu",
                "middle": [],
                "last": "Peng",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "first recognized in Guangdong Province, China, in November 2002, and its causative agent was identified as novel coronavirus (SARS-CoV). [1] [2] [3] Coronaviruses are the largest RNA viruses, containing a single-stranded, plus-sense RNA ranging from 27 kb to 31.5 kb in size. The two large open reading frames (ORFs) (1a and 1b) at the 5 -end of the genome encode the viral replicase and are translated directly from the genomic RNA, while 1b is expressed by -1 ribosomal frameshifting. 4 The 3 -one third of the genome comprises the unknown function. These proteins are translated through 6-9 nested and 3 -coterminal subgenomic RNAs (sgRNAs). approximately 29,700 nucleotides. [5] [6] [7] Fourteen ORFs have been identified, of which 12 are located in the 3 -proximal one-third of the genome. 4, 5 The exact mechanisms of expression of the 3 -proximal ORFs are unknown but on the analogy with other coronaviruses, these ORFs are predicted to be expressed through a set of sgRNAs. 6 Identification of SARS-CoV sgRNAs in infected cells and characterization of molecular details of the leader-body fusion in the sgRNAs will help elucidate the regulatory mechanism of SARS-CoV transcription and replication. This knowledge will be useful in the development of antiviral therapeutic agents and vaccine for treatment and prevention of this newly emerged disease. ",
            "cite_spans": [
                {
                    "start": 137,
                    "end": 140,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 141,
                    "end": 144,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 145,
                    "end": 148,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 487,
                    "end": 488,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 679,
                    "end": 682,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 683,
                    "end": 686,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 687,
                    "end": 690,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 795,
                    "end": 797,
                    "text": "4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 798,
                    "end": 799,
                    "text": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 982,
                    "end": 983,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "Northern blot, RT-PCR, and DNA sequencing revealed the existence of ten subgenomic RNAs including two novel subgenomic RNAs named 2-1 and 3-1. The leader-body fusion site (ACGAgC) of subgenomic RNA 2-1 has one nucleotide mismatch (lowercase) with SARS-CoV leader core sequence (CS-L ) ACGAAC and is located inside the S gene, 384 nucleotides downstream from the authentic CS (ACGAAC) for mRNA 2/S. The second novel subgenomic RNA (3-1) corresponded to the 3b ORF that was predicted to be expressed from mRNA 3. The leader-body fusion site (AaGAAC) for subgenomic mRNA 3-1 is 10 nucleotides upstream of AUG start codon of ORF 3b and has a mismatch (lowercase) with the SARS-CoV leader core sequence (ACGAAC).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "IDENTIFICATION OF NOVEL SUBGENOMIC RNAS"
        },
        {
            "text": "To determine whether these new subgenomic RNAs are functional messages, the 5\u00b4-end (containing leader sequence plus downstream 150 nucleotides in case of sgRNA 2-1 and 400 nucleotides in case of sgRNA 3-1) were fused with green fluorescent protein (GFP) gene, and sgRNA codon usage was indirectly determined by the expression of the reporter gene. Strong fluorescence was observed in cells transfected with the construct in which the ORF2b was fused in-frame with GFP ( Figure 1A ). Western blot with anti-GFP downstream AUG by leaky scanning was also detected in cells transfected with in-frame and out-frame construct ( Figure 1B, upper panel) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 470,
                    "end": 479,
                    "text": "Figure 1A",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 622,
                    "end": 645,
                    "text": "Figure 1B, upper panel)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "IDENTIFICATION OF NOVEL SUBGENOMIC RNAS"
        },
        {
            "text": "Cells transfected with the ORF3b in-frame construct displayed weak fluorescence while no fluorescence was observed in cells transfected with the ORF3b out-frame construct ( Figure 1A) . Western blot analysis confirmed the existence of a 42, kDa band corresponding to 3b-GFP fusion protein ( Figure 1B, lower panel) . These results indicated that sgRNA 2-1 and 3-1 could function in the environment of the cell. A single band of fusion protein was detected by Western blot in cells transfected with constructs containing the 5 -end of sgRNA 2, 2-1, 3, 3-1, 4, 6 and 7, whereas, in cells weight band was also detected that may result from a downstream AUG codon ( Figure  2B ).",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 173,
                    "end": 183,
                    "text": "Figure 1A)",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 291,
                    "end": 314,
                    "text": "Figure 1B, lower panel)",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 662,
                    "end": 672,
                    "text": "Figure  2B",
                    "ref_id": "FIGREF3"
                }
            ],
            "section": "IDENTIFICATION OF NOVEL SUBGENOMIC RNAS"
        },
        {
            "text": "We also detected some minor low molecular weight bands in some cases, possibly subgenomic RNA8-GFP fusion construct displayed fair amount of fluorescence but in showed that nine out of ten subgenomic RNAs could be functional messages in vivo; however, existence of proteins encoded by these subgenomic in SARS-CoV infected cells is yet to be determined. In summary, ten subgenomic RNAs including two novel subgenomic RNAs (2-1/3-1) were identified in SARS-CoV infected cells by Northern blot, RT-PCR, and DNA sequencing. Nine out of 10 subgenomic RNAs were potentially functional messages in SARS-CoV infected cells. The initiator AUG codon of subgenomic RNA 8 (ORF8b) was inactive in our experimental set-up.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "IDENTIFICATION OF NOVEL SUBGENOMIC RNAS"
        },
        {
            "text": "transfected with mRNA 5 and 9, beside the expected fusion protein band, a low molecular resulting from downstream AUG codons by leaky scanning. Cells transfected with Western blot only a 27 kDa band of wild-type GFP was detected. Taken together-we ,",
            "cite_spans": [],
            "ref_spans": [],
            "section": "IDENTIFICATION OF NOVEL SUBGENOMIC RNAS"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Aetiology: Koch's postulates fulfilled for SARS virus",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Fouchier",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Kuiken",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Schutten",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Nature",
            "volume": "423",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "A novel coronavirus associated with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "T",
                    "middle": [
                        "G"
                    ],
                    "last": "Ksiazek",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "S"
                    ],
                    "last": "Goldsmith",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N. Engl. J. Med",
            "volume": "348",
            "issn": "",
            "pages": "1953--1966",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Clinical progression and viral load in a community outbreak of coronavirus-associated SARS pneumonia: a prospective study",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Peiris",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "C"
                    ],
                    "last": "Cheng",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1767--1772",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Mechanisms and enzymes involved in SARS coronavirus genome expression",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Thiel",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "A"
                    ],
                    "last": "Ivanov",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Putics",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J. Gen. Virol",
            "volume": "84",
            "issn": "",
            "pages": "2305--2315",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "The genome sequence of the SARS-associated coronavirus",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Marra",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "J"
                    ],
                    "last": "Jones",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "R"
                    ],
                    "last": "Astell",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": "1399--1404",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Characterization of a novel coronavirus associated with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Rota",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "S"
                    ],
                    "last": "Oberste",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "S"
                    ],
                    "last": "Monroe",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": "1394--1399",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Comparative full-length genome sequence analysis of 14 SARS coronavirus isolates and common mutations associated with putative origins of infection",
            "authors": [
                {
                    "first": "Y",
                    "middle": [
                        "J"
                    ],
                    "last": "Ruan",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "L"
                    ],
                    "last": "Wei",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "L"
                    ],
                    "last": "Ee",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1779--1785",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "85 Severe acute respiratory syndrome (SARS) is an atypical form of pneumonia that was Ying Wu, Zhaoyang Li, Ying Zhu, Po Tien, and Deyin Guo* Wuhan University, Wuhan, People's Republic of China. * genes encoding structural proteins S, E, M, and N and a number of auxiliary proteins of The genomes of many SARS-CoV isolates have been sequenced,",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Translatability of novel subgenomic RNAs 2-1 and 3-1. (A) The ORF 2b (sgRNA 2-1) and 3b (sgRNA 3-1) were fused in-frame and out of frame at the 5\u00b4-end of GFP gene. The expression of GFP-fusion protein was qualitatively assessed by fluorescent microscopy (magnification: X200). (B) Proteins were extracted from transfected cells and separated by 12% SDS-PAGE. GFP-Fusion proteins were detected by anti-GFP monoclonal antibody.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "confirmed the existence of a 32 kDa band corresponding to the fusion protein. A 27 kDa band corresponding to the wild-type GFP resulting from 3. EXPRESSION PROFILE OF SARS-COV SUBGENOMIC RNAS The 5'-ends (including leader sequence and 200-400 nucleotides of the 5'-end of body sequence) of all 10 (were amplified by RT-PCR and fused to the 5'-end of the GFP reporter gene. The AUG initiator codon usage of subgenomic RNAs was indirectly assessed by level of expression of reporter gene. Strong fluorescence was observed in the cells transfected with fusion constructs containing the 5'-ends of subgenomic RNAs 2-1, 3, 4/E, 5/M, 6 and 9/N, whereas relatively weak fluorescence was observed in cells transfected with fusion constructs containing 5'-ends of subgenomic RNAs 2, 3-1, 7 and 8 (Figure 2A).",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Expression of GFP-fusion protein in BHK cells. (A) The ORFs of all ten subgenomic were fused in-frame with GFP gene. The expression of GFP-fusion proteins was qualitatively assessed by fluorescent microscopy (magnification: X200). (B) Proteins were extracted from transfected cells and separated by 12% SDS-PAGE. GFP-Fusion proteins were detected by anti-GFP monoclonal antibody. Names of the individual subgenomic RNAs are marked on top and molecular weight (kDa) is marked on right side of image.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}