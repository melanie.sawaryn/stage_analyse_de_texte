{
    "paper_id": "fb8b0363cd19a76b24985929d68c92335fd1057f",
    "metadata": {
        "title": "Journal Pre-proof Personal protective equipment and Covid 19-a risk to healthcare staff? Personal protective equipment and Covid 19-a risk to healthcare staff?",
        "authors": [
            {
                "first": "J",
                "middle": [
                    "B T"
                ],
                "last": "Herron",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "P",
                "middle": [
                    "A"
                ],
                "last": "Brennan",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Wellbeing Sunderland University",
                    "location": {
                        "addrLine": "Chester Road",
                        "postCode": "SR1 3SD",
                        "settlement": "Sunderland",
                        "country": "UK"
                    }
                },
                "email": ""
            },
            {
                "first": "Jbt",
                "middle": [],
                "last": "Herron",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Wellbeing Sunderland University",
                    "location": {
                        "addrLine": "Chester Road",
                        "postCode": "SR1 3SD",
                        "settlement": "Sunderland",
                        "country": "UK"
                    }
                },
                "email": ""
            },
            {
                "first": "Agc",
                "middle": [],
                "last": "Hay-David",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Elizabeth University Hospital",
                    "location": {
                        "postCode": "G51 4TF",
                        "settlement": "Glasgow",
                        "region": "Queen",
                        "country": "UK"
                    }
                },
                "email": ""
            },
            {
                "first": "Gilliam",
                "middle": [],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "In four months, coronavirus has spread around the world causing deaths in many countries as well as anxiety, panic, economic instability and unprecedented demands on healthcare systems. Healthcare staff are exposed to viral transmission and there have already been some deaths in both doctors and nurses.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "In this article we discuss the current legislation and requirements for personal protective equipment (PPE), particularly given the distance of up to 4.5m that the virus can travel during coughing and sneezing. Surgical facemasks provide very little protection for particle sizes 10 to 80 nm while FFP2 masks are over 100 times more effective providing at least 95% protection when performing an aerosol generating procedure. We emphasise the importance of ensuring all healthcare staff are adequately protected during this crisis.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "None J o u r n a l P r e -p r o o f occupational health. Scand J Work Environ Health. 2020; Epub ahead of print. 9. Tran K, Cimon K, Severn M, Pessoa-Silva CL, Conly J. Aerosol generating procedures and risk of transmission of acute respiratory infections to healthcare workers: A systematic review. PLoS ONE. 2012; 7(4):e35797. J o u r n a l P r e -p r o o f",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "The novel Coronavirus, COVID-19 (SARS-CoV-2) (1) has recently created a worldwide pandemic. With a death rate that is climbing rapidly, the disease has been declared a global emergency (2) . A substantial number of healthcare workers tested positive for the disease in Italy, the epicentre of the European outbreak, particularly in its early stages. The number of positive healthcare workers was 10,627 with 34 deaths to date, representing a mortality rate of 0.3% (3) , but the Italian authorities acknowledged that healthcare workers were over tested which may account for a lower mortality rate. The most worrying statistic is the rising number of deaths amongst healthcare professionals (4).",
            "cite_spans": [
                {
                    "start": 185,
                    "end": 188,
                    "text": "(2)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 465,
                    "end": 468,
                    "text": "(3)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "There have already been seven healthcare staff deaths in the UK (5) as of 5 April 2020. This has been postulated to be for a number of reasons and may be multifactorial.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The initial viral load is thought to be a predictor of severity as is the case with influenza (6). There is a known poorer prognosis in patient with COVID-19 whom have a prolonged virus shedding (7) .",
            "cite_spans": [
                {
                    "start": 195,
                    "end": 198,
                    "text": "(7)",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Viral Load"
        },
        {
            "text": "Healthcare professionals are at a higher risk of catching the disease due to their exposure to higher viral loads (8) . It is also known that if the virus is aerosolised then it becomes more infectious to healthcare staff (9). In 2007, the WHO lists Intubation and extubation, manual ventilation, open suctioning, cardiopulmonary resuscitation, bronchoscopy, surgery and post-mortem procedures involving high-speed devices, some dental procedures (e.g. using dental burs), non-invasive ventilation (NIV) e.g. bi-level positive airway pressure (BiPAP) and continuous positive airway pressure ventilation (CPAP) as aerosol generating procedures (AGP's) (10). This guidance has not been adopted by all UK regions with Northern Ireland for example delineating only intubation, manual ventilation, non-invasive ventilation (e.g., BiPAP, BPAP) and tracheostomy insertion as AGP's which reflects the most recent WHO guidance in 2014 (10). However, since this reduced list there have been incidences where items from the old list have been implicated in transmission of Mers-Co-V, such as open suction (11). Additionally, bronchoscopy has been shown in several studies to be implicated in aerosolised transmission (12) . If a healthcare worker is exposed to a higher viral load especially aerosolised rather than droplet form then their outcome may be significantly worse.",
            "cite_spans": [
                {
                    "start": 114,
                    "end": 117,
                    "text": "(8)",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1206,
                    "end": 1210,
                    "text": "(12)",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [],
            "section": "Viral Load"
        },
        {
            "text": "The personal protective equipment (PPE) at Work Regulations 1992 legislates that an employer will provide suitable protection for an employee in their work (13) . The employee must also receive adequate training in the use of the equipment. The WHO has recommended that when dealing with patients whom are performing any AGP on a suspected COVID-19 positive patient must wear an N95 or FFP2 mask (14). There is also a recommendation that a medical mask, gown, gloves, and eye protection (goggles or face shield) is sufficient (14). The WHO also recommends that other staff on a J o u r n a l P r e -p r o o f ward not providing direct care require no PPE. Public health England (PHE) have recommended that an FFP3 mask (Figure 1 ) should be used if available but an FFP2/N95 mask can be used when FFP3",
            "cite_spans": [
                {
                    "start": 156,
                    "end": 160,
                    "text": "(13)",
                    "ref_id": "BIBREF14"
                }
            ],
            "ref_spans": [
                {
                    "start": 719,
                    "end": 728,
                    "text": "(Figure 1",
                    "ref_id": null
                }
            ],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "are not available for AGP (15). Otherwise there is very little divergence between the two guidance articles.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "Electron microscopy has measured the COVID-19 virus is between 70-90nm in diameter (16).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "However, Fl\u00fcgge droplets less than 5 \u03bcm in size are typically produced by coughing and sneezing during which the virus can travel up to 4.5m, representing a risk to healthcare staff that are not directly involved in patient care (17) . This is particularly relevant when staff are ward based with no additional PPE. Surgical facemasks were found to provide very little protection for particle sizes 10 to 80 nm (18) . N95/FFP2 masks are at least 95% effective for particle sizes 0.1 to 0.3 \u00b5m which increases to 99.5% or higher for particles that are 0.75 \u00b5m or bigger (19) . Therefore over 95% protection is provided with an FFP2/N95 mask when performing an AGP.",
            "cite_spans": [
                {
                    "start": 229,
                    "end": 233,
                    "text": "(17)",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 411,
                    "end": 415,
                    "text": "(18)",
                    "ref_id": "BIBREF19"
                },
                {
                    "start": 569,
                    "end": 573,
                    "text": "(19)",
                    "ref_id": "BIBREF20"
                }
            ],
            "ref_spans": [],
            "section": "Personal Protective Equipment"
        },
        {
            "text": "There has been considerable concern in the UK that front line clinicians are not getting the correct PPE (20). A BBC article raised concerns that Chief Nurse Ruth May held, stating that more staff were likely to die and that there are PPE shortages not only in the frontline NHS but also in communities, but the Government are actively addressing this issue (20). However, at least one NHS staff member has resigned as she was unable to wear a facemask she purchased herself (4) . With up to 14% of staff absent from work, stretching an already busy workforce stress, emotions and fatigue are running high (21) , increasing the interplay of human factors, and chance of error and potentially exposing staff to the virus (22) . Asymptomatic carriers who are potentially part of the healthcare workforce present a significant challenge as they can be the vectors to others (23) . Until a reliable antibody test is developed and there is widespread RNA testing of staff this will remain a challenge.",
            "cite_spans": [
                {
                    "start": 475,
                    "end": 478,
                    "text": "(4)",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 606,
                    "end": 610,
                    "text": "(21)",
                    "ref_id": "BIBREF23"
                },
                {
                    "start": 720,
                    "end": 724,
                    "text": "(22)",
                    "ref_id": "BIBREF25"
                },
                {
                    "start": 871,
                    "end": 875,
                    "text": "(23)",
                    "ref_id": "BIBREF26"
                }
            ],
            "ref_spans": [],
            "section": "Workforce concerns"
        },
        {
            "text": "Any healthcare staff that are required to self-isolate due to co-morbidities should seek to engage with occupational health at the first opportunity to be managed effectively (8) .",
            "cite_spans": [
                {
                    "start": 175,
                    "end": 178,
                    "text": "(8)",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "Workforce concerns"
        },
        {
            "text": "There are also concerns over staff members' use of facemasks as not all staff have had a fit test, and little training has been provided in some establishments for donning a surgical mask (24) . An illfitting mask due to poor donning or prolonged use can increase the risk of infection (24) . This is a potential area for further spread of the disease and easy to implement. A lesson the UK military learned from deployment on Operation Gritrock (Humanitarian assistance to the Ebola epidemic Sierra Leone 2014-2015) was to have a donning and doffing supervisor, and when this was not possible a buddy-buddy system (25) . This can reduce self-infection/cross contamination and provides automaticity of safe and efficient donning and doffing of PPE amongst healthcare workers.",
            "cite_spans": [
                {
                    "start": 188,
                    "end": 192,
                    "text": "(24)",
                    "ref_id": "BIBREF27"
                },
                {
                    "start": 286,
                    "end": 290,
                    "text": "(24)",
                    "ref_id": "BIBREF27"
                },
                {
                    "start": 615,
                    "end": 619,
                    "text": "(25)",
                    "ref_id": "BIBREF28"
                }
            ],
            "ref_spans": [],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "In order for healthcare professionals to deliver safe care they need adequate protection and training in its use. Where employers are failing to provide adequate PPE safe healthcare cannot be delivered.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusions"
        },
        {
            "text": "This poses both moral and ethical dilemmas to healthcare professionals who are patient focused, thereby creating a sense of inadequacy and undervaluation resulting in workforce stress. The elected government should be held accountable to the public on its promises. 'Rationing' and prioritisation of care has already been seen in the cancellation of all elective surgery, endoscopy lists and clinics.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusions"
        },
        {
            "text": "Healthcare staff want to care for their patients and the government need to put in place a system for future pandemics that safeguards and preserves the NHS workforce.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusions"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The proximal origin of SARS-CoV-2",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "G"
                    ],
                    "last": "Andersen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Rambaut",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "I"
                    ],
                    "last": "Lipkin",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Nat Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Epub ahead of print",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "World Health Organization declares global emergency: A review of the 2019 novel coronavirus (COVID-19)",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Sohrabi",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Alsafi",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Neill",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Int J Surg",
            "volume": "76",
            "issn": "",
            "pages": "71--76",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "COVID-19 epidemic. 2 April 2020 national update",
            "authors": [
                {
                    "first": "Istituto Superiore Di",
                    "middle": [],
                    "last": "Sanit\u00e0",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Coronavirus deaths of two nurses lead to calls for more protection",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Siddique",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Parveen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Topping",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "The Guardian. 2020",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "UK minister gives coronavirus update",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Nagesh",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Snowdon",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Rannard",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Influenza infectious dose may explain the high mortality of the second and third wave of 1918 1919 influenza pandemic",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "C"
                    ],
                    "last": "Paulo",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Correia-Neves",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Domingos",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "PLoS One",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Clinical course and risk factors for mortality of adult inpatients with COVID-19 in Wuhan, China: a retrospective cohort study",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Du",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "398",
            "issn": "",
            "pages": "1054--1062",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "The COVID-19 (Coronavirus) pandemic: consequences for 10",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Burdorf",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Porru",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Rugulies",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Aerosol Generating Procedures (AGPs)",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Boswell",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Longstaff",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Healthcare worker infected with Middle East Respiratory Syndrome during cardiopulmonary resuscitation in Korea",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "S"
                    ],
                    "last": "Nam",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "Y"
                    ],
                    "last": "Yeon",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "W"
                    ],
                    "last": "Park",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Epidemiol Health",
            "volume": "39",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Characterization of aerosols generated during patient care activities",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "A"
                    ],
                    "last": "O&apos;neil",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Leavey",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Clin Infect Dis",
            "volume": "65",
            "issn": "8",
            "pages": "1342--1348",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "The Personal Protective Equipment at Work Regulations",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Uk Government",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Geneva: World Health Organisation. 2020. 15. Public health England. COVID-19 personal protective equipment (PPE",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Identification of coronavirus isolated from a patient in Korea with covid-19",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "S"
                    ],
                    "last": "Chung",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "J"
                    ],
                    "last": "Jo",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Osong Public Heal Res Perspect",
            "volume": "11",
            "issn": "1",
            "pages": "3--7",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "The impact of high-flow nasal cannula (HFNC) on coughing distance: implications on its use during the novel coronavirus disease outbreak",
            "authors": [
                {
                    "first": "Nhw",
                    "middle": [],
                    "last": "Loh",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Tan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Taculod",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Gorospe",
                    "suffix": ""
                },
                {
                    "first": ".",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Can J Anaesth",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF19": {
            "ref_id": "b19",
            "title": "Do N95 respirators provide 95% protection level against airborne viruses, and how adequate are surgical masks?",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ba\u0142azy",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Toivola",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Adhikari",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Am J Infect Control",
            "volume": "34",
            "issn": "2",
            "pages": "51--58",
            "other_ids": {}
        },
        "BIBREF20": {
            "ref_id": "b20",
            "title": "Performance of N95 respirators: Filtration efficiency for airborne microbial and inert particles",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Qian",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Willeke",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Grinshpun",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Donnelly",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Coffey",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Am Ind Hyg Assoc J",
            "volume": "59",
            "issn": "2",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF21": {
            "ref_id": "b21",
            "title": "Nurse deaths \"inevitable\" from coronavirus",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Bbc",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF23": {
            "ref_id": "b23",
            "title": "Coronavirus: Almost half of NHS Scotland staff absences Covid-19 related",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Walker",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF25": {
            "ref_id": "b25",
            "title": "Good people who try their best can have problems: Recognition of human factors and how to minimise error",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Brennan",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "A"
                    ],
                    "last": "Mitchell",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Holmes",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Br J Oral Maxillofac S",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF26": {
            "ref_id": "b26",
            "title": "Presumed Asymptomatic Carrier Transmission of COVID-19",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Bai",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Yao",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA -Journal of the American Medical Association",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF27": {
            "ref_id": "b27",
            "title": "Do theatre staff use face masks in accordance with the manufacturers' guidelines of use?",
            "authors": [
                {
                    "first": "Jbt",
                    "middle": [],
                    "last": "Herron",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Kuht",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "Z"
                    ],
                    "last": "Hussain",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "J Infect Prev",
            "volume": "20",
            "issn": "2",
            "pages": "99--106",
            "other_ids": {}
        },
        "BIBREF28": {
            "ref_id": "b28",
            "title": "Operation GRITROCK: the Defence Medical Services' story and emerging lessons from supporting the UK response to the Ebola crisis",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bricknell",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Hodgetts",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Beaton",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "J R Army Med Corps",
            "volume": "162",
            "issn": "3",
            "pages": "169--75",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}