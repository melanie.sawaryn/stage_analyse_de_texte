{
    "paper_id": "c78a152566f57c00668f5a7d4949515f4f3a5883",
    "metadata": {
        "title": "When a Diagnosis Is Reportable The Patient",
        "authors": [
            {
                "first": "Scott",
                "middle": [],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J",
                "middle": [
                    "E"
                ],
                "last": "Snyder",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "C",
                "middle": [
                    "C"
                ],
                "last": "Gauthier",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "O. is a 37-year-old male with no significant past medical history who presented to the Emergency Department with a one week history of worsening dysphagia and odynophagia. He reported he was usually in good health until the current symptoms began. Scott said that he had been unable to swallow solid foods over the past week, largely due to increasing pain on swallowing, and was now unable to swallow liquids as well. Although he stated that his appetite was unchanged, he estimated a fivepound weight loss over the last week due to poor oral intake. He otherwise denied fever, respiratory or gastrointestinal symptoms, or any other complaints. He takes no medications at home except for rare acetaminophen. He is married, has a twoyear-old son, and works as an attorney. He denied use of tobacco, alcohol, or illicit drugs. Family history was noncontributory.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        },
        {
            "text": "On physical exam, he was afebrile and his vital signs were within normal limits. Cardiovascular, respiratory, abdominal, extremity, and neurologic exam were unremarkable. There was some mild oropharyngeal thrush, palpable bilateral inguinal lymphadenopathy, and a 1 cm painless ulceration on the penile shaft. Laboratory evaluation was unremarkable.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Scott was admitted for further evaluation and treatment by the medical team. His wife was asked to step out of the room while the medical resident asked Scott some further questions. The resident told Scott that she was concerned that he had primary syphilis based on his ulceration and adenopathy and stated he should be tested for that as well as for HIV, given his thrush. When she pressed Scott for information about his sexual history he initially hesitated, but then acknowledged that he had in fact had sexual relations with a female prostitute, \"but only once.\" He consented to tests for HIV-1 antibodies and syphilis, but begged the resident \"please don't tell my wife.\" Both tests came back positive shortly thereafter. When Scott learned the news, he admitted to the medical resident that he has also had sexual relations with a woman at his office. He again implored that his test results not be shared with his wife or his other sexual partners.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethical Dilemma"
        },
        {
            "text": "Questions for thought and discussion: What are the obligations of the medical resident for reporting the positive HIV and syphilis test results? Who do these results get reported to? Who notifies the sexual partners of patients who test positive for sexually transmitted infections?",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethical Dilemma"
        },
        {
            "text": "Question for thought and discussion: When should patient confidentiality be broken, even if it is against the patient's wishes?",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethical Dilemma"
        },
        {
            "text": "The advent of antiretroviral therapy has led to improved survival rates for those infected with HIV. Despite national campaigns designed to encourage people to get tested for HIV, around one-quarter of those infected are unaware of their diagnosis and many persons are diagnosed at an advanced stage of disease. In a 2005 report from the Centers for Disease Control and Prevention (CDC) it was estimated that 39 percent of patients diagnosed with HIV are subsequently diagnosed with AIDS in less than 12 months. Among AIDS-defining illnesses (see Table 12 .1), esophageal Candidiasis is one of relatively high incidence. Identifying patients with HIV infection prior to their developing an HIV-related illness would be beneficial and allow antiretroviral treatment to be initiated earlier. One way in which early diagnosis of HIV infection may be better achieved is through more aggressive screening at the primary care provider level. In one study by Gao, et al. it was found that only 28 percent of the over 3,000 patients surveyed were asked about sexually transmitted infections (STIs) at their last routine health visit.",
            "cite_spans": [
                {
                    "start": 952,
                    "end": 966,
                    "text": "Gao, et al. it",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 547,
                    "end": 555,
                    "text": "Table 12",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "The Medicine"
        },
        {
            "text": "From 1990 to 2000, rates of primary and secondary syphilis declined a sharp 89.7 percent; however, a resurgence has been seen in the subsequent years. The greatest increases have been in men, particularly those having sex with other men (MSM). According to one study by Ciesielski and Boghani, MSM who have syphilis are 10 times more likely than heterosexual males with syphilis to be coinfected with HIV. The chancres caused by syphilis disrupt the natural barriers against HIV infection and increase susceptibility to HIV infection by up to five times. STIs such as syphilis are indicative of risk-taking behavior that is associated with increased transmission of HIV.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Medicine"
        },
        {
            "text": "Based on 2005 data, the CDC reports that, in adult and adolescent males, AIDS is transmitted via sexual contact in men who have sex with men (MSM) in about 59 percent of cases. About 8 percent is due to high risk heterosexual contact. Although Scott is married to a woman and claims to have had high risk heterosexual encounters, he should be specifically questioned about having past sexual relations with other men. MSM is a grouping which includes persons who self-identify as gay, bisexual, or heterosexual and, hence, includes men that may also have sexual relations with women. Sociocultural stigmatization of homosexuality, particularly in certain minority groups, may prevent some MSM from self-identifying as gay or bisexual. Some men who proclaim themselves to be heterosexual may admit to being \"on the down low,\" an expression referring to those men who occasionally also have sex with other men. Nonjudgmentally asking a patient \"do you have sex with men, women, or both?\" is probably the best way to obtain such sexual history information during the medical interview.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Medicine"
        },
        {
            "text": "By law licensed physicians and other health care providers must report cases, or even suspected cases, of certain medical conditions and communicable diseases that they come upon in practice to their local health department (for those in North Carolina, please see Table 12 .2). Cases must be reported in a timely fashion to help prevent epidemic spread of infectious diseases or agents of biological warfare. The list of reportable diseases, mechanism for reporting, and timing of the filed report are established by state and local public health departments, in association with the United States Department of Health and Human Services (HHS). Health care practitioners are encouraged to familiarize themselves with the individual laws and guidelines in their state(s) of practice.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 265,
                    "end": 273,
                    "text": "Table 12",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "The Law"
        },
        {
            "text": "In many states, once the HHS is notified of a reportable disease such as confirmed HIV infection, a health investigator representing that state's HHS will become The Law involved in the case. The investigator will meet with the patient and encourage them to seek medical attention and to use abstinence or barrier methods to help prevent spread of the virus. They will also attempt to determine who the patient may have, through sufficient contact, exposed to the disease. Although they are strongly encouraged to do so, the patient is not required by law to divulge all sexual contacts to the investigator. If a patient has not personally notified their partners of their risk for HIV infection, the investigator may then contact all divulged sexual partners for the need to pursue testing and potential treatment for HIV. This is done in a way that does not disclose who may have exposed them to the virus.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Law"
        },
        {
            "text": "Under the Health Insurance Portability and Accountability Act of 1996 (HIPAA), patient confidentiality laws mandate that Scott's diagnoses of HIV and syphilis must be kept confidential, per his request, from all people who are not at risk for contracting these diseases from him. However, when a patient's known disease presents an imminent threat to the health or life of others, then confidentiality must be broken. Similar to the precedent established in the case of Tarasoff v. Regents of the University of California, there is a duty to protect the safety of an intended victim that supersedes the duty to maintain a patient's right to confidentiality. Anyone who has sexual relations with or engages in other risk behaviors with Scott is at risk for contracting HIV and syphilis, and protecting the health and welfare of those contact persons takes precedent over maintaining Scott's confidentiality. Additionally, if Scott knowingly exposes others to HIV, this may be considered a crime in some states, although individual state laws are variable regarding what qualifies as criminal activity in this regard. Health care practitioners should specifically learn the law of their state of practice regarding this issue. Due to variability in state laws regarding mandatory reporting of HIV status by patients to persons with occupational exposure to bodily fluids (e.g., dental hygienists, nurses, physicians, etc.), the use of universal contact precautions is always prudent.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Law"
        },
        {
            "text": "The Principle of Respect for Dignity requires respect for Scott's emotions, his relationships, and his privacy. Scott is naturally upset about the possibility that his diagnosis of syphilis and HIV will be shared with his wife and he is worried about the effect this knowledge may have on their relationship. To protect Scott's relationship with his wife and his privacy, it appears that Scott's diagnosis should remain confidential and his wife should not be informed.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethics"
        },
        {
            "text": "However, none of the principles of health care ethics is absolute and there are situations in which each of the principles may be overridden by more important considerations. In this case the Principle of Respect for Dignity and the medical confidentiality that it requires are outweighed by the need to protect others in society from grave and foreseeable harm. This conclusion can be justified by comparing the harms that would be done and the harms that would be prevented by violating confidentiality in this case. Scott will be harmed emotionally. He may feel angry and betrayed. His relationship with his wife may be ruined and may even end. On the other hand, if Scott's wife is informed, then she can get tested for syphilis and HIV, seek treatment if she has been infected, and if not infected, she can take steps to protect herself from becoming so. The harm prevented, then, is the harm of serious and life-threatening illness.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethics"
        },
        {
            "text": "The same conclusion may be reached using the Principles of Beneficence and Non-Maleficence, considering the harms done and the harms prevented for Scott, alone. Informing Scott's wife of her exposure to syphilis and HIV means the loss of Scott's privacy and possibly his marriage, but it also prevents the emotional pain of knowing he has infected his wife with a deadly disease. Comparing these harms, it could be argued that notifying Scott's wife is in his best interests, as well as hers.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethics"
        },
        {
            "text": "The same applies to notifying Scott's other sexual partners. Communicable diseases present a case in which individual privacy and medical confidentiality are overridden by public health concerns. This is the moral basis for the current reporting, counseling, and notification program carried out by local public health departments. Once Scott's diagnosis is reported, he will meet with a health investigator who will record the names of his contacts and notify them of their exposure to syphilis and HIV. Most states use \"confidential\" HIV testing, which means that contacts will not be told who has been diagnosed as HIV seropositive and has subsequently named them as contacts at risk for infection.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethics"
        },
        {
            "text": "Scott should be informed that his diagnosis will be reported to the public health department and that he will be asked to meet with an investigator and give the names of his sexual contacts. Ideally, Scott would name his wife as a sexual contact and she would be notified along with his other contacts. However, it could be argued that it would be better for Scott's wife (in terms of time for tests and treatment or protection) and better for him (in terms of the future of their relationship) if Scott told her himself about his diagnosis and her risk. With this in mind, the medical resident could counsel Scott to inform his wife as soon as possible. Scott needs to be aware that, if they stay together for any length of time, his wife will likely come to know that he is very sick and she may guess the cause of his illness based on his symptoms and treatments.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethics"
        },
        {
            "text": "If Scott refuses to tell his wife, the medical resident could intervene on the wife's behalf by informing the public health department that Scott is married, thereby guaranteeing she will be notified by the health investigator.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Ethics"
        },
        {
            "text": "Now that the evidence-based medicine, legal precedent, and relevant ethical principles for this case have been reviewed, formulate a strategy to address the ethical conflicts in this case. If necessary, perform additional research into local and state laws and hospital regulations. Consider delving further into the background medical literature to assist with making sound therapeutic decisions. Devise a treatment approach that addresses the needs of the patient and his family, that is both ethically and medically sound, and that is culturally competent. Ensure that the strategy employs fair and appropriate utilization of medical resources, and that the approach is practical and feasible within the limits of the medical system at large. Work out a clear and professional way to communicate the proposal to the patient and his family. Attempt to foresee challenges that may arise in conveying or implementing the plan. Determine what follow-up will be necessary to ensure that the chosen strategy remains successful for the patient in the long-term. Reflect on how the knowledge and skills learned from this case can be used to improve the care of patients that may be encountered in future practice.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "The Formulation"
        },
        {
            "text": "In this case a patient is diagnosed with two reportable infectious diseases and patient confidentiality must be broken to maintain the health and safety of those who have come into contact with him. How much does fear of \"exposure\" via broken confidentiality impact a patient's initiative to get tested for diseases such as HIV? Should HIV testing ever be anonymous or confidential? Is public health more important than an individual's health and rights?",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Afterthoughts"
        }
    ],
    "bib_entries": {
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Ethics and Human Rights Committee, American College of Physicians",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Snyder",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "D"
                    ],
                    "last": "Leffler",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "D"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Annals of Internal Medicine",
            "volume": "142",
            "issn": "7",
            "pages": "560--582",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Estimated HIV prevalence in the United States at the end of 2003. National HIV Prevention Conference",
            "authors": [
                {
                    "first": "Glynn",
                    "middle": [],
                    "last": "Rhodes",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Atlanta. Abstract",
            "volume": "595",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "The incidence of AIDS-defining illnesses in 4,883 patients with human immunodeficiency virus infection",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mocroft",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Archives of Internal Medicine",
            "volume": "158",
            "issn": "",
            "pages": "491--497",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Cases of HIV infection and AIDS in the United States",
            "authors": [],
            "year": 2004,
            "venue": "HIV/AIDS Surveillance Report",
            "volume": "16",
            "issn": "",
            "pages": "16--45",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Missed opportunities to assess sexually transmitted diseases in U.S. adults during routine medical checkups",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Tao",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "American Journal of Preventive Medicine",
            "volume": "18",
            "issn": "2",
            "pages": "109--114",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "HIV infection among men with infectious syphilis in Chicago",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "A"
                    ],
                    "last": "Ciesielski",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Boghani",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "Program and abstracts of the 9th Conference on Retroviruses and Opportunistic Infections",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "The most common AIDS-defining illnesses, from highest to lowest incidence",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Reportable communicable diseases in North Carolina (may vary by state) Shiga Toxin-producing infection (including E.coli O157:H7) Ehrlichiosis, granulocytic Ehrlichiosis, monocytic (E. chaffeensis) Encephalitis, Arboviral (CAL, EEE, WNV, other) Enterococci, Vancomycin-resistant (\"VRE\"), from normally sterile site Foodborne Disease (C. perfringens, Staphylococcal, Other/Unknown)",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "North Carolina Communicable Disease Report Card. Acquired from: http://www.epi.state.nc.us/epi/gcdc/manual/CDReportCard.pdf. Accessed October 15, 2007.",
            "latex": null,
            "type": "table"
        },
        "TABREF3": {
            "text": "",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}