{
    "paper_id": "35706d8148f1b232f92aaff1fbd1aca76ad5241f",
    "metadata": {
        "title": "Correlating Cell Line Studies With Tissue Distribution of DPP4/TMPRSS2 and Human Biological Samples May Better Define the Viral Tropism of MERS-CoV",
        "authors": [
            {
                "first": "Phd",
                "middle": [],
                "last": "Msc",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "",
                "middle": [],
                "last": "Fams",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "",
                "middle": [],
                "last": "Face",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "",
                "middle": [],
                "last": "Facp",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "",
                "middle": [],
                "last": "Frcp",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "TO THE EDITOR-A decade after the severe acute respiratory syndrome (SARS) epidemic caused by the notorious SARS-coronavirus (CoV), it is disconcerting that a novel human coronavirus, Middle East respiratory syndrome (MERS)-CoV, has eerily emerged in the Middle East with a threat to exact yet another grim toll on humankind. It is indeed timely to find a paper published in the recent issue of the Journal of Infectious Diseases by Chan et al describing the wide tissue tropism of MERS-CoV across a range of human and nonhuman cell lines and its rapidity of induction of cytopathic effects in an attempt to explain the apparently high fatality rate encountered [1] .",
            "cite_spans": [
                {
                    "start": 661,
                    "end": 664,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "At least 3 issues should be considered in the implications of their findings. First, endocrine tissues such as adrenal or pituitary cell lines were conspicuously unrepresented in their list. Previously, we showed that a sizeable proportion of SARS patients had evidence of disordered cortisol secretion that had conceivably contributed to mortality [2] . Thus, it is of paramount importance to suspect that MERS-CoV could share a similar predilection for the hypothalamus-pituitary-adrenal axis.",
            "cite_spans": [
                {
                    "start": 349,
                    "end": 352,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Second, the authors did not address the mechanisms of cell entry by MERS-CoV into tissue cultures. As the specificity of the coronavirus-host tissue receptors governs cell invasion and determines the sites of organ pathogenicity, research in this area in the wake of their findings should be a priority. Notably, a group has recently established a hitherto uncharacterized surface receptor targeted by the viral spike-protein of the Human Coronavirus-Erasmus Medical Center (HCoV-EMC) for protease-activated cellular entry mediated by type II transmembrane serine proteases (TMPRSS2) potentially exploitable for antiviral intervention [3] . Close on the heel of this discovery came the findings of Raj et al [4] who independently unraveled yet another critical lead on the same puzzle. Apparently, MERS-CoV also capitalizes on dipeptidyl-peptidase 4 (DPP4) as a key portal of entry into cells. Interestingly, DPP4 has an uncanny dual nature, being an immunologic signaling glycoprotein component cluster of differentiation-26 on T cells [5] as well as an enzyme best known for its catalytic affinity on incretins from which DPP4-inhibitor therapy was developed as an additional sword in the arsenal against the diabetes \"pandemic\" [6] .",
            "cite_spans": [
                {
                    "start": 635,
                    "end": 638,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 708,
                    "end": 711,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1037,
                    "end": 1040,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1231,
                    "end": 1234,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Although the data are compelling, it is premature to assume that this new villain capitalizes on no other receptors, given that much remains unknown in the face of this looming outbreak. It would bode us well to remember the cautionary tale that even though the most poignant assault of SARS-CoV was on the lungs, other organs including the endocrine system bore the hallmarks of collateral damage [7] . SARS-CoV employs a variant of the angiotensin-converting enzyme known as ACE2 for cell entry [7] . However, ACE2 was not the sole receptor, as the organ distribution of SARS-CoV provided clues to a second receptor called CD209L that was also shown to facilitate its cellular invasion [8] . While both TMPRSS2 and DPP4 are functionally serine proteases, they are unequivocally different molecules encoded by distinct genes on separate chromosomal loci (21q22.3 and 2q24.3, respectively). TMPRSS2 exhibits tissue distributions (aerodigestive tract) [9] that are different from those of DPP4 (endothelial membranes of most organs including the liver, pancreas, and kidneys) [10] . Because viral tropism is ultimately constrained by engagement of the virus with specific cellular receptors/coreceptors, it is prudent to investigate for the existence of other receptors co-utilized by MERS-CoV, which is hinted at by the broad array of cell types shown by Chan et al to be affected before our new enemy gains further ground.",
            "cite_spans": [
                {
                    "start": 398,
                    "end": 401,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 497,
                    "end": 500,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 688,
                    "end": 691,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 951,
                    "end": 954,
                    "text": "[9]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1075,
                    "end": 1079,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Finally, the extrapolation of data from such human cell lines to tissues of the intact human host is often fraught with difficulties. Hence, tissue culture tropism may depart significantly from the actual sites of organ involvement and correlate imperfectly with clinical manifestation in afflicted patients. Whenever possible, tissue samples from bodily fluids, organ biopsies of patients, and even necropsies obtained from a thorough autopsy study of those who succumbed to MERS-CoV infection will be insightful to better define the repertoire and tissue distribution of relevant target receptors involved. After all, there is no wisdom in hindsight from the SARS epidemic if it does not extend our foresight further in the face of this present novel MERS-CoV outbreak.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Potential conflicts of interest. Author certifies no conflicts of interest.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Note"
        },
        {
            "text": "The author has submitted the ICMJE Form for Disclosure of Potential Conflicts of Interest. I do not have a commercial or other association that might pose a conflict of interest.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Note"
        },
        {
            "text": "Department of Endocrinology, Tan Tock Seng Hospital, Singapore",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Melvin Khee-Shing Leow"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Differential cell line susceptibility to the emerging novel human betacoronavirus 2c EMC/2012: Implications for disease pathogenesis and clinical manifestation",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "F"
                    ],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "H"
                    ],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "K"
                    ],
                    "last": "Choi",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Infect Dis",
            "volume": "207",
            "issn": "",
            "pages": "1734--52",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Hypocortisolism in severe acute respiratory syndrome (SARS)",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "K"
                    ],
                    "last": "Leow",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "S"
                    ],
                    "last": "Kwek",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "W"
                    ],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Ong",
                    "suffix": ""
                },
                {
                    "first": "Kaw",
                    "middle": [],
                    "last": "Gjlee",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "S"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Clinical Endocrinol (Oxf )",
            "volume": "63",
            "issn": "",
            "pages": "197--202",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "The spikeprotein of the emerging betacoronavirus EMC uses a novel coronavirus receptor for entry, can be activated by TMPRSS2 and is targeted by neutralizing antibodies",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gierer",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Bertram",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Kaup",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Virol",
            "volume": "87",
            "issn": "",
            "pages": "5502--5513",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Dipeptidyl peptidase 4 is a functional receptor for the emerging human coronavirus-EMC",
            "authors": [
                {
                    "first": "V",
                    "middle": [
                        "S"
                    ],
                    "last": "Raj",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Mou",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "Smits",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Nature",
            "volume": "495",
            "issn": "",
            "pages": "251--255",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "CD26, adenosine deaminase, and adenosine receptors mediate costimulatory signals in the immunological synapse",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Pacheco",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Martinez-Navio",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Lejeune",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "102",
            "issn": "",
            "pages": "9583--88",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Dipeptidyl peptidase inhibitors as new drugs for the treatment of type 2 diabetes",
            "authors": [
                {
                    "first": "H",
                    "middle": [
                        "J"
                    ],
                    "last": "Mest",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Mentlein",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Diabetologia",
            "volume": "48",
            "issn": "",
            "pages": "616--636",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Tissue distribution of ACE2 protein, the functional receptor for SARS coronavirus. A first step in understanding SARS pathogenesis",
            "authors": [
                {
                    "first": "I",
                    "middle": [],
                    "last": "Hamming",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Timens",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Bulthuis",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "T"
                    ],
                    "last": "Lely",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Navis",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Van Goor",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Pathol",
            "volume": "203",
            "issn": "",
            "pages": "631--638",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "CD209L (L-SIGN) is a receptor for severe acute respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Jeffers",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "M"
                    ],
                    "last": "Tusell",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Gillim-Ross",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "101",
            "issn": "",
            "pages": "15748--53",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Influenza and SARS-coronavirus activating proteases TMPRSS2 and HAT are expressed at multiple sites in human respiratory and gastrointestinal tracts",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Bertram",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Heurich",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Lavender",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "PLoS One",
            "volume": "7",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Dipeptidyl peptidase IV and related enzymes in cell biology and liver disorders",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Gorrell",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Clin Sci (Lond)",
            "volume": "108",
            "issn": "",
            "pages": "277--92",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}