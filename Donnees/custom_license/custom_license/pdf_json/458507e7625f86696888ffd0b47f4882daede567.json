{
    "paper_id": "458507e7625f86696888ffd0b47f4882daede567",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Pandemics rarely affect all people in a uniform way. The Black Death in the 14th century reduced the global population by a third, with the highest number of deaths observed among the poorest populations. 1 Densely populated with malnourished and overworked peasants, medieval Europe was a fertile breeding ground for the bubonic plague. Seven centuries on-with a global gross domestic product of almost US$100 trillion-is our world adequately resourced to prevent another pandemic? 2 Current evidence from the coronavirus disease 2019 (COVID-19) pandemic would suggest otherwise.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Why inequality could spread COVID-19"
        },
        {
            "text": "Estimates indicate that COVID-19 could cost the world more than $10 trillion, 3 although considerable uncertainty exists with regard to the reach of the virus and the efficacy of the policy response. For each percentage point reduction in the global economy, more than 10 million people are plunged into poverty worldwide. 3 Considering that the poorest populations are more likely to have chronic conditions, this puts them at higher risk of COVID-19-associated mortality. Since the pandemic has perpetuated an economic crisis, unemployment rates will rise substantially and weakened welfare safety nets further threaten health and social insecurity.",
            "cite_spans": [
                {
                    "start": 78,
                    "end": 79,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 323,
                    "end": 324,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Why inequality could spread COVID-19"
        },
        {
            "text": "Working should never come at the expense of an individual's health nor to public health. In the USA, instances of unexpected medical billings for uninsured patients treated for COVID-19 and carriers continuing to work for fear of redundancy have already been documented. 4 Despite employment safeguards recently being passed into law in some high-income countries, such as the UK and the USA, low-income groups are wary of these assurances since they have experience of long-standing difficulties navigating complex benefits systems, 4 and many workers (including the self-employed) can be omitted from such contingency plans. The implications of inadequate financial protections for lowwage workers are more evident in countries with higher levels of extreme poverty, such as India.",
            "cite_spans": [
                {
                    "start": 271,
                    "end": 272,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Why inequality could spread COVID-19"
        },
        {
            "text": "In recent pandemics, such as the Middle East respiratory syndrome, doctors were vectors of disease transmission due to inadequate testing and personal protective equipment. 5 History seems to be repeating itself, with clinicians comprising more than a tenth of all COVID-19 cases in Spain and Italy. With a projected global shortage of 15 million health-care workers by 2030, governments have left essential personnel exposed in this time of need.",
            "cite_spans": [
                {
                    "start": 173,
                    "end": 174,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Why inequality could spread COVID-19"
        },
        {
            "text": "Poor populations lacking access to health services in normal circumstances are left most vulnerable during times of crisis. Misinformation and miscommunication disproportionally affect individuals with less access to information channels, who are thus more likely to ignore government health warnings. 6 With the introduction of physical distancing measures, household internet coverage should be made ubiquitous.",
            "cite_spans": [
                {
                    "start": 302,
                    "end": 303,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Why inequality could spread COVID-19"
        },
        {
            "text": "The inequitable response to COVID-19 is already evident. Healthy life expectancy and mortality rates have historically been markedly disproportionate between the richest and poorest populations. The full effects of COVID-19 are yet to be seen, while the disease begins to spread across the most fragile settings, including conflict zones, prisons, and refugee camps. As the global economy plunges deeper into an economic crisis and government bailout programmes continue to prioritise industry, scarce resources and funding allocation decisions must aim to reduce inequities rather than exacerbate them.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Why inequality could spread COVID-19"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "What caused the black death?",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "J"
                    ],
                    "last": "Duncan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Scott",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Postgrad Med J",
            "volume": "81",
            "issn": "",
            "pages": "315--335",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The short history of global living conditions and why it matters that we know it",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Roser",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "How much will poverty increase because of COVID-19?",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Update on federal surprise billing legislation: new bills contain key differences",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Hoadley",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Fuchs",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Lucia",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "COVID-19: towards controlling of a pandemic",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Bedford",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Enria",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Giesecke",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30673-5"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Low health literacy prevents equal access to care",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pirisi",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Lancet",
            "volume": "356",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Comment www.thelancet.com/public-health Published online April 2, 2020 https://doi.org/10.1016/ S2468-2667(20)30085-2 1",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "We declare no competing interests. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        }
    ]
}