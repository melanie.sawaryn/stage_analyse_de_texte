{
    "paper_id": "2620711ca6b696ded3b6a891305daea8cf3a562c",
    "metadata": {
        "title": "Characterization of Human Metapneumoviruses Isolated from Patients in North America",
        "authors": [
            {
                "first": "Teresa",
                "middle": [
                    "C T"
                ],
                "last": "Peret",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Guy",
                "middle": [],
                "last": "Boivin",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Laval University",
                    "location": {
                        "settlement": "Quebec City"
                    }
                },
                "email": ""
            },
            {
                "first": "Yan",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "affiliation": {
                    "laboratory": "National Microbiology Laboratory",
                    "institution": "Canadian Science Center for Human and Animal Health",
                    "location": {
                        "settlement": "Winnipeg",
                        "country": "Canada"
                    }
                },
                "email": ""
            },
            {
                "first": "Michel",
                "middle": [],
                "last": "Couillard",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Public Health Institute of Quebec",
                    "location": {
                        "settlement": "Montreal"
                    }
                },
                "email": ""
            },
            {
                "first": "Charles",
                "middle": [],
                "last": "Humphrey",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "National Center for Infectious Diseases",
                    "location": {
                        "settlement": "Atlanta",
                        "country": "Georgia"
                    }
                },
                "email": ""
            },
            {
                "first": "Albert",
                "middle": [
                    "D M E"
                ],
                "last": "Osterhaus",
                "suffix": "",
                "affiliation": {
                    "laboratory": "National Center for In-fectious Diseases, Centers for Disease Control and Prevention",
                    "institution": "Erasmus Medical Center",
                    "location": {
                        "addrLine": "1600 Clifton Rd., MS A34",
                        "postCode": "30333",
                        "settlement": "Rotterdam, Atlanta",
                        "region": "GA",
                        "country": "The Netherlands"
                    }
                },
                "email": ""
            },
            {
                "first": "Dean",
                "middle": [
                    "D"
                ],
                "last": "Erdman",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Larry",
                "middle": [
                    "J"
                ],
                "last": "Anderson",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Human metapneumovirus (HMPV) was recently identified in The Netherlands and was linked to acute respiratory tract illness. In this study, 11 isolates from 10 patients with respiratory disease from Quebec, Canada, were tested by a reverse-transcriptase polymerase chain reaction based on the fusion protein gene. Identified sequences were consistent with HMPV. The patients were 2 months to 87 years of age (median age, 58 years) and presented with acute respiratory tract illness during the winter season. Sequence studies of the nucleocapsid, fusion, and polymerase genes identified 2 main lineages of HMPV and cocirculation of both lineages during the same year. These findings support a previous finding that HMPV is a human respiratory pathogen that merits further study.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Human metapneumovirus (HMPV) was recently identified in The Netherlands and was linked to acute respiratory tract illness. In this study, 11 isolates from 10 patients with respiratory disease from Quebec, Canada, were tested by a reverse-transcriptase polymerase chain reaction based on the fusion protein gene. Identified sequences were consistent with HMPV. The patients were 2 months to 87 years of age (median age, 58 years) and presented with acute respiratory tract illness during the winter season. Sequence studies of the nucleocapsid, fusion, and polymerase genes identified 2 main lineages of HMPV and cocirculation of both lineages during the same year. These findings support a previous finding that HMPV is a human respiratory pathogen that merits further study.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A new paramyxovirus associated with respiratory illness, human metapneumovirus (HMPV), was described recently [1] . Data from that report suggest that HMPV is similar to respiratory syncytial virus (RSV), in that infection usually occurs during winter months and is common during childhood (most children have serologic evidence of infection by age 5 years). The genomic organization of HMPV resembles that of the avian pneumovirus [2] and has been tentatively assigned to the subfamily Pneumovirinae, genus Metapneumovirus. In the present article, we describe polymerase chain reaction (PCR) and sequencing studies done on 11 isolates from respiratory specimens from 10 Canadian patients with acute respiratory tract illness.",
            "cite_spans": [
                {
                    "start": 110,
                    "end": 113,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 432,
                    "end": 435,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Isolates and patients. We studied 11 unidentified isolates from 10 patients with acute respiratory tract illness that were recovered from 1997 through 1999 in Quebec City, Quebec, Canada. The specimens from which the isolations were made were from endobronchial lavage (1 specimen), pharyngeal swabs (2 specimens), and nasopharyngeal aspirates (5 specimens); the origin of 3 specimens was not determined. Patients were 2 months to 87 years of age (median age, 58 years). Of the 10 patients, 7 were hospitalized for respiratory illness in an acute care hospital, and 3 were residing in a long-term care facility at the time of illness. All 11 isolates were recovered in LLC-MK2 cells (Rhesus monkey kidney cells) and had focal rounding and cell destruction without apparent syncytia formation. No cytopathic effect was noted in MDCK or NCI-H292 cells. The original specimens had negative results of testing for influenza viruses when they were inoculated into embryonated eggs. The isolates did not adsorb erythrocytes and had negative results of testing by indirect immunofluorescence assays for influenza viruses A and B; parainfluenza viruses 1, 2, and 3; adenovirus; and RSV (Bartels; Chemicon). Results of reverse-transcriptase (RT) PCR or PCR assays for adenovirus, coronavirus, influenza viruses A, B, and C, parainfluenza viruses 1, 2, and 3, rhinovirus, and RSV were negative for all 11 isolates.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Patients, Materials, and Methods"
        },
        {
            "text": "Electron microscopy (EM). The isolation material was prepared for negative-stain EM examination by use of 2% phosphotungstic acid negative staining. We adjusted phosphotungstic acid pH to 6.5 with 1 N potassium hydroxide and used formvar-carbon grids pretreated with glow discharge [3] . Samples were mixed 1:1 with catalase crystals and prepared for negative-stain EM examination, as described elsewhere [4] , to determine the dimensions of virus particles and nucleocapsid structures.",
            "cite_spans": [
                {
                    "start": 282,
                    "end": 285,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 405,
                    "end": 408,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Patients, Materials, and Methods"
        },
        {
            "text": "Oligonucleotide primer design for HMPV. Published nucleocapsid (N) and fusion (F) gene sequences of HMPV and avian pneumovirus were used to develop primers for detection and sequencing of HMPV at the Respiratory Virus Section (Centers for Disease Con- trol and Prevention, Atlanta). The F primer set was used for HMPV identification by RT-PCR. Polymerase (L) gene primers, used in the initial HMPV studies [1] , were later provided to corroborate our findings. Primer pair MPVF1f (5 0 -CTTTGGACTTAATGACA-GATG-3 0 )-MPVF1r (5 0 -GTCTTCCTGTGCTAACTTTG-3 0 ) and primer pair MPVN3f (5 0 -GAGAAGAGCTGGGTAGAAG-3 0 )-MPVN3r (5 0 -CAAACAAACTTTCTGCT-3 0 ) were used to amplify regions in the F (450 bp) and N (377 bp) genes, respectively.",
            "cite_spans": [
                {
                    "start": 406,
                    "end": 409,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Patients, Materials, and Methods"
        },
        {
            "text": "RT-PCR and nucleotide sequencing. Total RNA was extracted by use of a guanidium-isothiocyanate-phenol method (RNAzol LS; Tel-Test). Viral RNA was amplified in a 1-step RT-PCR (Access RT-PCR; Promega). The PCR assay was carried out in a mix containing 10 mL of 5\u00a3 reaction buffer, 10 mM dNTPs, 1.25 mM forward and reverse primers, 2 mL of 25 mM MgSO 4 , 5 U of avian myeloblastosis virus RT, 5 U of Thermus flavus DNA polymerase, and 5 mL of RNA; nuclease-free water was added until the volume of the mix was 50 mL. Amplification conditions consisted of 45 min at 42 C; 2 min at 94 C; 35 cycles of PCR for 1 min each at 94 C, 54 C, and 72 C; and a final extension at 72 C for 7 min. Each RNA sample was run against a housekeeping gene to verify RNA integrity. The PCR products were purified with either the QIAquick gel extraction kit or QIAquick PCR purification kit (Qiagen). Both strands were sequenced on an ABI 377 sequencer, using a fluorescent dye terminator kit (Applied Biosystems). The nucleotide sequences were edited with Sequencher version 3.1.1 for the Power Macintosh (Gene Codes).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Patients, Materials, and Methods"
        },
        {
            "text": "Phylogenetic analysis. Partial nucleotide sequences of N, F, and L genes were aligned by use of CLUSTAL W, version 1.7, for Unix [5] . N and L sequences were aligned with published HMPV partial and full gene sequences. F sequences were aligned with the single full HMPV F gene sequence available. Phylogenetic trees for group A and B alignments were computed by maximum parsimony-, distance-, and maximum likelihood-based criteria analysis with PAUP* version 4.0.d8 [6] . For the bootstrap analysis, sequences were added randomly, and 1 tree was held at each step (100 bootstrap replicates). Nucleotide sequences were translated with TRANSLATE in the Wisconsin Package, version 10.2 for Unix (Genetics Computer Group). Pairwise nucleotide and amino acid distances were calculated, respectively, as the proportion of differences (uncorrected P value) and mean character difference, using MEGA (Molecular Evolutionary Genetics Analysis; MEGA Software) [7] .",
            "cite_spans": [
                {
                    "start": 129,
                    "end": 132,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 466,
                    "end": 469,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 950,
                    "end": 953,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "Patients, Materials, and Methods"
        },
        {
            "text": "EM studies. The 11 isolates initially recovered in LLC-MK2 cells were also successfully passaged in VCI-H292 cells. EM examination showed that all 11 specimens contained viruses with morphologic characteristics that were consistent with paramyxovirus. The particles were pleomorphic, spherical, and filamentous (figure 1). The mean length of the projections on the particles was 15 nm (SE, 0.27), the nucleocapsid diameter was 17 nm (SE, 0.36), and the nucleocapsid pitch spacing was 7 nm (SE, 0.24). The nucleocapsid length was , 200 nm to 1000 nm. Spherical particles varied considerably in size, but the mean diameter was 209 nm (SE, 27.8). Filamentous particles averaged 282 \u00a3 62 nm in size, but too few were available for us to obtain a satisfactory statistical representation. The measure-ments are in accordance with the dimensions of members of the Metapneumovirus and Pneumovirus genera [2, 8] .",
            "cite_spans": [
                {
                    "start": 896,
                    "end": 899,
                    "text": "[2,",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 900,
                    "end": 902,
                    "text": "8]",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Results"
        },
        {
            "text": "Sequencing studies. Phylogenetic analyses based on the N (300 nt), F (405 nt), and L (102 nt) genes gave comparable results (i.e., they identified 2 major groups or lineages; figure 2 ). An overall nucleotide comparison for the isolates revealed 93%-100% similarity between isolates in the same group and 83%-85% similarity between the 2 groups. The predicted amino acid sequences were less distinct: they showed 95%-97% similarity between the 2 distinct groups and 97%-100% similarity between isolates in the same group. Of note, several sequences from isolates from patients in The Netherlands (designated \"NLD\"), reported elsewhere [1] , clustered in those 2 main groups, along with the 11 isolates from patients in Canada (designated \"CAN\") described in the present article. Despite the limited data available, we observed cocirculation of both groups (CAN97-82 and CAN97-83) during the same year. Some sequences clustered with published HMPV sequences identified in different years, as seen on subclusters (CAN97-82, NLD99- . Of note, 2 HMPV isolates, CAN98-75 and CAN99-80, each belonging to a different group, came from the same child. The second isolate was found 10 months after the first. This demonstrates that the child was reinfected, rather than persistently infected, with HMPV.",
            "cite_spans": [
                {
                    "start": 635,
                    "end": 638,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [
                {
                    "start": 175,
                    "end": 183,
                    "text": "figure 2",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Results"
        },
        {
            "text": "Our data support the findings of van den Hoogen et al.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "[1] that a new virus, tentatively named \"human metapneumovirus,\" is associated with acute respiratory disease. In addition, our data demonstrate that this virus is present in North America and likely worldwide. Our findings are also consistent with the aforementioned findings [1] : we identified 2 major groups or lineages and sequence diversity within these 2 major groups. Human RSV, a paramyxovirus in the same taxonomic subfamily, has been classified into 2 major groups, A and B. Concurrent circulation of both human RSV groups and variants has been identified [8] . One could speculate that HMPV might also follow these trends. We found some HMPV isolates that, despite close genetic relatedness, did not originate from a single outbreak but were from discrete and unrelated cases of respiratory illness, as also occurs with human RSV. Further sequencing studies that examine more isolates and different HMPV genes, including the surface glycoprotein G gene, should be conducted to confirm and refine these observations. We detected virus in isolates from children with acute respiratory tract infection, as described in the first report of HMPV [1] . We also detected virus in isolates from adults with acute respiratory tract infection. If the serologic studies from The Netherlands [1] are indicative of the epidemiologic features of infection in Canada (i.e., most children are infected by age 5 years), then HMPV likely can reinfect an individual later in life, possibly repeatedly, as does human RSV.",
            "cite_spans": [
                {
                    "start": 277,
                    "end": 280,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 567,
                    "end": 570,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1153,
                    "end": 1156,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1292,
                    "end": 1295,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "In fact, like human RSV infection, HMPV reinfection may well occur in the first years of life, as is illustrated by the isolation of 2 HMPVs from the same child in 2 consecutive winter seasons.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "It appears that the virus isolated in the second year was able to evade, at least partially, immunity induced 10 months earlier.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The ability to detect HMPV in children and in adults suggests that HMPV disease should be studied in all age groups. Given the very limited data available on infection with this virus in humans, etiologic studies should include testing of specimens from appropriate control patients. In conclusion, HMPV presents some exciting new opportunities and challenges in our efforts to understand human respiratory tract disease.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "A newly discovered human pneumovirus isolated from young children with respiratory tract disease",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "G"
                    ],
                    "last": "Van Den Hoogen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "De Jong",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Groen",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Nat Med",
            "volume": "7",
            "issn": "",
            "pages": "719--743",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Characterization of a virus associated with turkey rhinotracheitis",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "S"
                    ],
                    "last": "Collins",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "E"
                    ],
                    "last": "Gough",
                    "suffix": ""
                }
            ],
            "year": 1988,
            "venue": "J Gen Virol",
            "volume": "69",
            "issn": "",
            "pages": "909--925",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "A glow discharge unit to render electron microscope grids and other surfaces hydrophilic",
            "authors": [
                {
                    "first": "U",
                    "middle": [],
                    "last": "Aebi",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "D"
                    ],
                    "last": "Pollard",
                    "suffix": ""
                }
            ],
            "year": 1987,
            "venue": "J Electron Microsc Tech",
            "volume": "7",
            "issn": "",
            "pages": "29--33",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "An accurate measurement of the catalase crystal and its use as an internal marker for electron microscopy",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Luftig",
                    "suffix": ""
                }
            ],
            "year": 1967,
            "venue": "J Ultrastruct Res",
            "volume": "20",
            "issn": "",
            "pages": "91--102",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "CLUSTAL W: improving the sensitivity of progressive multiple sequence alignment through sequence weighting, position-specific gap penalties and weight matrix choice",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "D"
                    ],
                    "last": "Thompson",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "G"
                    ],
                    "last": "Higgins",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "J"
                    ],
                    "last": "Gibson",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "Nucleic Acids Res",
            "volume": "22",
            "issn": "",
            "pages": "4673--80",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "phylogenetic analysis using parsimony (*and other methods)",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "L"
                    ],
                    "last": "Swofford",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Paup*",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "MEGA: molecular evolutionary genetics analysis",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Kumar",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Tamura",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Nei",
                    "suffix": ""
                }
            ],
            "year": 1993,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Respiratory syncytial virus",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "L"
                    ],
                    "last": "Collins",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "M"
                    ],
                    "last": "Chanock",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "R"
                    ],
                    "last": "Murphy",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Fields virology. 4th ed. Philadelphia: Lippincott Williams & Wilkins",
            "volume": "",
            "issn": "",
            "pages": "1443--85",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Negative-stain electron micrographs of human metapneumovirus (HMPV). Center image shows 5 pleomorphic HMPV particles; note the projections along the periphery of the viruses. The images in the upper right and lower left corners are, respectively, of the nucleocapsid and filamentous rod-like particle. Staining was done with 2% phosphotungstic acid. Bar markers represent 100 nm.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Phylogenetic relationships observed in maximum likelihood analysis of the human metapneumovirus nucleocapsid (A), polymerase (B), and fusion (C)nucleotide sequences (GenBank accession nos. AF371330-38, AF371356-63, AF371365, and AF37137). Bootstrap proportions, obtained from a 50% majority rule consensus tree, were plotted at the main internal branches of the phylogram to show the support values. For the bootstrap analysis under the maximum likelihood assumption, sequences were added randomly, and 1 tree was held at each step (100 bootstrap replicates) by applying the tree bisection-reconnection branch-swapping algorithm. Trees were midpoint rooted using minimum F value optimization.The partial sequence for CAN97-82 (polymerase gene) could not be obtained. Trees were drawn to scale. CAN, Canada; NLD, The Netherlands.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Reprints or correspondence: Dr. Larry J. Anderson, Respiratory and Enteric Viruses Branch, Div. of Viral and Rickettsial Diseases, National Center for Infectious Diseases, Centers for Disease Control and Prevention, 1600 Clifton Rd., MS A34, Atlanta, GA 30333 (lja2@cdc.gov).",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "We thank Brian Holloway and staff (Centers for Disease Control and Prevention Biotechnology Core Facility, DNA Chemistry Section) for the oligonucleotide synthesis.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgments"
        }
    ]
}