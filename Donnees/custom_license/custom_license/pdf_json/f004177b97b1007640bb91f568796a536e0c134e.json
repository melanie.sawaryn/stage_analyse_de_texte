{
    "paper_id": "f004177b97b1007640bb91f568796a536e0c134e",
    "metadata": {
        "title": "Detection of human bocavirus in Asturias, Northern Spain",
        "authors": [
            {
                "first": "L",
                "middle": [],
                "last": "Villa",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "S",
                "middle": [],
                "last": "Mel\u00f3n",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "S",
                "middle": [],
                "last": "Su\u00e1rez",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "M",
                "middle": [
                    "E"
                ],
                "last": "Alvarez-Arg\u00fcelles",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "D",
                "middle": [],
                "last": "G\u00f3nzalez",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "A",
                "middle": [],
                "last": "Morilla",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J",
                "middle": [
                    "A"
                ],
                "last": "Boga",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J",
                "middle": [],
                "last": "Rodr\u00edguez",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "M",
                "middle": [],
                "last": "De O\u00f1a",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The aim of this study was to detect HBoV prospectively in samples obtained from children who visited the Pediatric Emergency Service, and to evaluate its relative contribution as a cause of different respiratory infections with respect to other respiratory viruses.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "From 20th April to 24th November 2006, 366 samples (209 pharyngeal, 148 nasal, five nasopharyngeal swabs, two bronchial aspirates, and two bronchoalveolar lavage fluids) from 339 children (190 males and 149 females) with a mean age of 2.9\u00b13.4 years, (range 3 days-13 years) and with ARI were collected for viral diagnostic evaluation. Of them, 118 (34.8%) showed symptoms of lower respiratory tract infections (LRTI), such as bronchiolitis and pneumonia, 115 (33.9%) came to hospital with symptoms of upper respiratory tract infections (URTI), such us pharyngitis, laryngitis, or rhinitis, and the remaining 106 (31.3%) showed only general symptoms of respiratory infection (SRI), such as fever, myalgia, headache, or cough.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The samples were tested with monoclonal antibodies to RSV; IA and IB viruses; PIV type 1, 2, and 3; and human adenovirus. The samples were also inoculated into MRC-5, LLC-MK2, and MDCK cell monolayers following standard protocols. Also, they were processed for rapid cultures (\"shell-vial\") in MRC-5 monolayer cells for above mentioned viruses (RSV, IA, IB, PIV, and adenovirus). Viral genomes were purified by using an automated nucleic acid purifier (AmpliPrep, Roche Diagnostics, USA). The routine detection of respiratory viruses comprised nested reverse transcription polymerase chain reaction (RT-PCR) for RSV, IA, IB, IC, PIV, hMPV, and coronaviruses. In 106 pharyngeal swabs, a PCR to detect primers Boca1N (5\u2032-GAAGACACCGAGCCTGAGAC-3\u2032, position 2189-2208 in the BoV genome DQ00045) and Boca2N (5\u2032-GCTGATTGGGTGTTCCTGAT-3\u2032, position 2516-2497), and the second round with inner primers Boca3N (5\u2032-AAACGTCGTCTAACTGCTCCA -3\u2032, position 2237-2257) and Boca4N (5\u2032-ATATGAGCCCGAG CCTCTCT-3\u2032, position 2495-2476). The expected product of 259 bp was excised from agarose gels, purified, and sequenced using the reverse inner primer to confirm that they were specific for HBoV.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In 161 (43.98%) samples belonging to 151 (44.54%) children, a virus was detected: RSV was found in 56 (16.52%), HBoV in 26 (7.67%), adenovirus in 25 (7.37%), PIV in 25 (7.37%), hMPV in 14 (4.13%), enterovirus in 13 (3.83%), herpes simplex virus (HSV) type 1 in three (0.88%), cytomegalovirus (CMV), coronavirus, and IA in one (0.29%), and EBV in nine (8.49%) of the 106 samples tested.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Although RSV was the most frequently identified virus, supporting its role as the main virus associated with respiratory infections in young children [10] , our data indicate that HBoV was the second most frequently detected in the viral screening of respiratory samples. It is in accordance with the values reported in other studies [1, 3, 4, 7] . In a retrospective screening performed from October 2005 to April 2006 in our laboratory, a similar rate of HBoV was observed (8.9%). When classic unspecific methods, such as cells lines, are used, viruses like CMV, enteroviruses, or HSV are also identified. It is also surprising to observe the detection of EBV in children with SRI.",
            "cite_spans": [
                {
                    "start": 150,
                    "end": 154,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 334,
                    "end": 337,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 338,
                    "end": 340,
                    "text": "3,",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 341,
                    "end": 343,
                    "text": "4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 344,
                    "end": 346,
                    "text": "7]",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Coinfections were found in 12 cases (3.54%). HBoV was detected in ten mixed infections (38.46% of all HBoVassociated cases), followed by RSV in five mixed infections (8.92% of all RSV-associated cases), and PIV in four (16% of all PIV-associated cases). A triple infection involving HBoV, PIV, and hMPV was detected.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In several studies, the coinfection with other respiratory viruses is a frequent feature of HBoV infection in infants. Rates between 18-56% have been reported [4, 6, 7, 9] . This variation may reflect differences in the viral incidence, the range of viruses screened, and the methods used. The fact that HBoV was present in 10 of 12 coinfections suggests the possibility that HBoV infection may increase the severity of other viruses, such as RSV or PIV [5] .",
            "cite_spans": [
                {
                    "start": 159,
                    "end": 162,
                    "text": "[4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 163,
                    "end": 165,
                    "text": "6,",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 166,
                    "end": 168,
                    "text": "7,",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 169,
                    "end": 171,
                    "text": "9]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 454,
                    "end": 457,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "HBoV was detected in 15 (10.13%) nasal swabs and in 11 (5.26%) pharyngeal swabs, but this difference was not significant. However, RSV was significantly more frequent in nasal swabs (27% vs. 5.74%, p<0.0001), such as PIV (11.5% vs. 3.34%, p=0.005) and hMPV (7.43% vs 1.43%, p=0.009). Adenovirus was found in 10.5% of pharyngeal swabs and in 2.7% nasal swabs (p=0.009).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The 26 HBoV-infected children (15 males [7.84%] and 11 females [7.38%]) had a mean age of 1.63\u00b11.37 years (range 3 days-12 years). Similar detection rates of RSV and HBoV were observed in each age group: in 17 (18.8%) and seven (8.8%) children younger than 6 months; in ten (28.6%) and nine (25%) children between 6 to 12 months; in ten (17.2%) and seven (12%) children between 1 and 2 years; and in nine (5.3%) and three (1.8%) children older than 2 years, respectively. PIV was detected in 21 cases (84%) in children younger than 1 year old and hMPV in 12 (85%). However, adenovirus was observed in 23 (88%) children older than 1 year.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Several studies [1, 4, 10] have reported that HBoV is most frequently detected in infants between 6 months and 2 years of age. In this study, the mean age of HBoVinfected children was 19 months, according to this hypothesis. The pattern of HBoV incidence in each age group was similar to RSV. This distribution can be compatible with protection from infection by maternal antibodies in the first year of life, but studies of seroprevalence of HBoV antibodies in different age groups are needed to validate this hypothesis.",
            "cite_spans": [
                {
                    "start": 16,
                    "end": 19,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 20,
                    "end": 22,
                    "text": "4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 23,
                    "end": 26,
                    "text": "10]",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The clinical characteristics of the 26 HBoV-infected children were bronchiolitis in 19 and pneumonia in three (18.64% of all cases of LRTI; in 14 cases as a unique virus), symptoms of upper respiratory tract infections in two (1.7% of all cases of URTI), and unspecific symptoms of respiratory infection, such as fever, in two (1.8% of all cases of SRI) (p<0.0001; Fig. 1 ). Then, HBoV infections were mainly identified in children with LRTI, which suggests that HBoV shares clinical characteristics with viruses such as RSV or hMPV [1, 4, 5, 7, 9, 10] , However, these [2] , whose individuals displayed symptoms consistent with URTI.",
            "cite_spans": [
                {
                    "start": 533,
                    "end": 536,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 537,
                    "end": 539,
                    "text": "4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 540,
                    "end": 542,
                    "text": "5,",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 543,
                    "end": 545,
                    "text": "7,",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 546,
                    "end": 548,
                    "text": "9,",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 549,
                    "end": 552,
                    "text": "10]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 570,
                    "end": 573,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [
                {
                    "start": 365,
                    "end": 371,
                    "text": "Fig. 1",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "Several respiratory viruses, such as influenza virus, parainfluenza virus, or RSV, are clearly more frequently detected in the winter. Some studies have reported that HBoV is also a seasonal virus [1, [4] [5] [6] 8] . In this study, the number of HBoV-infected samples varied monthly over the study period: one in April, June, or October; two in May; four in July; six in September; and 11 in November. This finding suggests that this virus is also frequently detected in the summer, supporting the data found by other authors [2, 3] . This fact could be the cause of the underestimation of the real impact of this virus in several reports.",
            "cite_spans": [
                {
                    "start": 197,
                    "end": 200,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 201,
                    "end": 204,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 205,
                    "end": 208,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 209,
                    "end": 212,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 213,
                    "end": 215,
                    "text": "8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 527,
                    "end": 530,
                    "text": "[2,",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 531,
                    "end": 533,
                    "text": "3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In summary, HBoV is frequently identified between young children with LRTI, and coinfection with other respiratory viruses is a frequently observed behavior. Therefore, it should be included in the diagnosis of viral acute respiratory infections. A study with a control group will be necessary to confirm the role of HBoV in ARI.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Cloning of a human parvovirus by molecular screening of respiratory tract samples",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Allander",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "T"
                    ],
                    "last": "Tammi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Eriksson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Bjerkner",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Tiveljung-Lindell",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Andersson",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Proc Natl Acad Sci",
            "volume": "102",
            "issn": "",
            "pages": "12891--12896",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Human bocavirus infection",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Bastien",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Brandt",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Dust",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ward",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Canada. Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "848--850",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Bocavirus infection in hospitalized children",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "Y"
                    ],
                    "last": "Chung",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "H"
                    ],
                    "last": "Han",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "K"
                    ],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "W"
                    ],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "South Korea. Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "1254--1256",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Human bocavirus in children",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Foulongne",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Rodi\u00e8re",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Segondy",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "862--863",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Detection of human bocavirus in Japanese children with lower respiratory tract infections",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Ma",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Endo",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Ishiguro",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ebihara",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Ishiko",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ariga",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kikuta",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Clin Microbiol",
            "volume": "44",
            "issn": "",
            "pages": "1132--1134",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Epidemiological profile and clinical associations of human bocavirus and other human parvoviruses",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Manning",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Russell",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Eastick",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "H"
                    ],
                    "last": "Leadbetter",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Hallam",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Templeton",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Simmonds",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Infect Dis",
            "volume": "194",
            "issn": "",
            "pages": "1283--1290",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Evidence of human coronavirus HKU1 and human bocavirus in Australian children",
            "authors": [
                {
                    "first": "T",
                    "middle": [
                        "P"
                    ],
                    "last": "Sloots",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Mcerlean",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Speicher",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "E"
                    ],
                    "last": "Arden",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Nissen",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [
                        "M"
                    ],
                    "last": "Mackay",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Clin Virol",
            "volume": "35",
            "issn": "",
            "pages": "99--102",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Human bocavirus in hospitalized children",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Smuts",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Hardie",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "South Africa. Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "1457--1458",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Frequent detection of bocavirus DNA in German children with respiratory tract infections",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Weissbrich",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Neske",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Schubert",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Tollmann",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Blath",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Blessing",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "W"
                    ],
                    "last": "Kreth",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "BMC Infect Dis",
            "volume": "6",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Human bocavirus infection, People's Republic of China",
            "authors": [
                {
                    "first": "X-W",
                    "middle": [],
                    "last": "Qu",
                    "suffix": ""
                },
                {
                    "first": "Z-J",
                    "middle": [],
                    "last": "Duan",
                    "suffix": ""
                },
                {
                    "first": "Z-Y",
                    "middle": [],
                    "last": "Qi",
                    "suffix": ""
                },
                {
                    "first": "Z-P",
                    "middle": [],
                    "last": "Xie",
                    "suffix": ""
                },
                {
                    "first": "H-C",
                    "middle": [],
                    "last": "Gao",
                    "suffix": ""
                },
                {
                    "first": "W-P",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "C-P",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "F-W",
                    "middle": [],
                    "last": "Peng",
                    "suffix": ""
                },
                {
                    "first": "L-S",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "Y-D",
                    "middle": [],
                    "last": "Hou",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Emerg Infect Dis",
            "volume": "13",
            "issn": "1",
            "pages": "165--168",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 1 Relationship between clinical symptoms and the viruses observed. HBoV=human bocavirus; PIV=parainfluenza virus; hMPV=human metapneumovirus; LRTI=lower respiratory tract infection; URTI=upper respiratory tract infection; SRI=symptoms of respiratory tract infection",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "findings are different from a Canadian study",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "Acknowledgments This work was partially supported by project grant 07/0330 from ISCIII (Plan Nacional de I+D+I), Madrid, Spain.The experiments comply with the current Spanish laws.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        }
    ]
}