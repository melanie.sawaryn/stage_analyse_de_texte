{
    "paper_id": "5139670b3f350f49dafcb766516ead7de5b23ca1",
    "metadata": {
        "title": "Incidence of novel coronavirus (2019-nCoV) infection among people under home quarantine in Shenzhen, China",
        "authors": [
            {
                "first": "Jingzhong",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Yi",
                "middle": [],
                "last": "Liao",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Xiaoyang",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Shenzhen Municipal Health Commission",
                    "location": {
                        "settlement": "Shenzhen",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Yichong",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Shenzhen Municipal Health Commission",
                    "location": {
                        "settlement": "Shenzhen",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Dan",
                "middle": [],
                "last": "Jiang",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "BGI Group",
                    "location": {
                        "settlement": "Shenzhen",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Jianfan",
                "middle": [],
                "last": "He",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Shunxiang",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Junjie",
                "middle": [],
                "last": "Xia",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Wang",
                "middle": [
                    "J"
                ],
                "last": "Liao",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Y",
                "middle": [],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Wang",
                "middle": [
                    "X"
                ],
                "last": "Li",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Jiang",
                "middle": [
                    "D"
                ],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "He",
                "middle": [
                    "J"
                ],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Zhang",
                "middle": [
                    "S"
                ],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Xia",
                "middle": [
                    "J"
                ],
                "last": "",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Jinzhong",
                "middle": [],
                "last": "Wang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Background: Since the outbreak of 2019-nCoV in December, Chinese government has implemented various measures including travel bans, centralized treatments, and home quarantines to slowing the transmission across the country. In this study, we aimed to estimate the incidence of 2019-nCoV infection among people under home quarantine in Shenzhen, China. Methods: We used a stratified multistage random sampling method to recruit participants and collected demographic information and laboratory results of people under home quarantine. We conducted descriptive analysis to estimate the basic characteristics and to calculate the incidence in out study population. Results: A total of 2004 people under home quarantine participated in this study, of which 1637 participants finished the questionnaire with a response rate of 81.7%. Mean age of the participants was 33.7 years, ranging from 0.3 to 80.2 years. Of people who provided clear travel history, 129 people have traveled to Wuhan city and 1,046 people have traveled to other cities in Hubei province within 14 days before the home quarantine. Few (less than 1%) participants reported contact history with confirmed or suspected cases during their trip and most of these arrived at",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Since December 2019, a cluster of pneumonia cases infected with novel coronavirus have been reported in Wuhan, China. Through genome-wide sequencing and culture identification of patients' sample, a new type of betacoronavirus has been identified. It was named 2019 novel coronavirus (2019-nCoV), and became the seventh member of the Coronavirus family that can infect humans 1 .",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Because the outbreak of this new coronavirus pneumonia coincided with the Chinese Lunar New Year, the cross-provincial transfers of people in China have provided excellent opportunities for the virus to spread throughout the country. Current evidence shows that the new coronavirus has a high infectivity, with the basic reproduction number R0 = 2.2 (95% CI:",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "1.4 to 3.9) 2 . In order to prevent the transmission of 2019-nCoV, the Chinese National Health Commission 3 has implemented different measures, including centralized treatment for confirmed cases, home-isolated or centralized medical observation for close contacts, suspected cases, mild cases, and asymptomatic cases.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "As a city with a large number of immigrants, millions of migrant workers are returning to Shenzhen after the Chinese Lunar New Year, which bringing great challenges to the prevention and control of the novel pneumonia. People with travel history in Hubei province or close contacts with confirmed cases were investigated and isolated at home or at designated hotels as required in Shenzhen. However, the basic characteristics, contact history, self-protect measures, and overall infection rate of people who receiving medical observation at home is not yet well understood.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "In this study, we estimated the demographic characteristics, travel history, contact history, self-protection measures, infection situation and other key information of people who were quarantine at home, to provide information for prevention and control of coronavirus disease 2019 (COVID-19).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "People who were identified as close contacts with confirmed cases or had travel history in epidemic provinces were at higher risk of infection, both of them need to be self-isolated for 14 days. According to the Shenzhen's guideline for COVID-19 prevention and control, people who had travel history in epidemic provinces are required to be isolated at home and receive medical observations including regular temperature measurement and syndromic surveillance. We regarded this group of people as our target population and used the formula = / \u2022 (1 \u2212 ) to calculate sample size. We set infection rate of 19-nCoV as 5%, the relative error \u03b4 as 10%, and \u03b1 as 0.05, the sample size was calculated to be 1825 people.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Study design and sampling"
        },
        {
            "text": "Considering the sample attrition, we planned to recruit 2000 people in our study.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Study design and sampling"
        },
        {
            "text": "We applied a stratified, multistage random sampling process to recruit participants in this study. First, we randomly selected 2 streets from 10 districts in Shenzhen. Then we randomly sampled 2 communities from the selected streets; if the total number of people who receiving medical observation at home in selected community was less than 50, we merged the community with its neighboring community to ensure that the total number of target people in the sampling unit at this stage was more than 50 people. Finally, we used simple random 5 sampling method to choose 50 target people each from the selected community (or merged communities).",
            "cite_spans": [
                {
                    "start": 541,
                    "end": 542,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Study design and sampling"
        },
        {
            "text": "We collected data from Feb 5, 2020 to Feb 10, 2020. Self-reported questionnaires and nasopharyngeal swab specimens were used to collect information of people under home quarantine. Considering that our participants might have been infected, we developed online questionnaire and asked respondents to fill the questionnaire by themselves. We collected nasopharyngeal swab specimens and employed the 2019-nCoV Real-Time PCR kit to detect virus. All the laboratory tests were conducted by the Beijing Genomics institution (BGI), Shenzhen.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Data collection"
        },
        {
            "text": "Proportions and frequencies were used to report the results of categorical variables, mean and range were used to express continuous variable. Statistical analyses were conducted using the Stata software, version15.1.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Statistical analysis"
        },
        {
            "text": "A total of 2004 people were involved in our study, of which 1637 participants finished the questionnaire with a response rate of 81.7%. Table 1 shows the demographic characteristics of the study sample. The prevalence of underlying diseases in this sample was 9.47%, and the mean age was 33.7 (range 0.3 to 80.2 years old). Travel history and contact history of the sample Excluding people who were long-term resident in the epidemic areas, the average length of their stay was 4.2 days. Most of the participants arrived at Shenzhen between Jan 24, 2020 to Jan 27, 2020 ( Figure 1 ). Less than 1% of the participants reported any contact history with confirmed or suspected cases. Note: * means study, work, and live in the epidemic areas before coming to Shenzhen Figure 1 . Distribution of date of arrival at Shenzhen",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 136,
                    "end": 143,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 572,
                    "end": 580,
                    "text": "Figure 1",
                    "ref_id": null
                },
                {
                    "start": 765,
                    "end": 773,
                    "text": "Figure 1",
                    "ref_id": null
                }
            ],
            "section": "Characteristics of the sample"
        },
        {
            "text": "In our study sample, more than 85% have taken various self-protection measures, such as wearing masks, washing hands, and reducing outdoor events, recommended by the specialist.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Self-protection behaviors of the participants"
        },
        {
            "text": "However, there were still 96 subjects (5.88%) who seldomly wearing masks. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Self-protection behaviors of the participants"
        },
        {
            "text": "We conducted nucleic acid testing for a total of 2004 people and three of these in our study tested positive for 2019nCoV. After recheck by Shenzhen Centers for Disease Control and Prevention, these persons were confirmed as COVID-19 cases. The incidence of COVID-19",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Incidence of nCoV-19 infections"
        },
        {
            "text": "in the sample was 1.5\u2030 (95% CI: 0.31\u2030-4.37\u2030). None of the three patients had obvious symptoms during the time of home quarantine (table 4) . Also, they did not report any history of contacts with confirmed cases during their stay in Wuhan / Hubei. ",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 129,
                    "end": 138,
                    "text": "(table 4)",
                    "ref_id": "TABREF4"
                }
            ],
            "section": "Incidence of nCoV-19 infections"
        },
        {
            "text": "This is the first population-based study to estimate the characteristics and the incidence of 2019-nCoV infection among people who were quarantined at home, using data collected from a stratified multistage random sample. It presents results from a comprehensive descriptive analysis of the travel history and laboratory results and provides information for policy makers to produce guideline for the management of people who have travel history in epidemic areas.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Three quarters (74.07%) of our study population had travel history in January and February, 2020, of which 96.70% have been to Hubei province. Most of the subjects (59.01%) arrived at Shenzhen between Jan 24 to Jan 27, 2020, right after the travel bans on Jan 23, 2020.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Considering that 2019-nCoV is a highly contagious infectious disease 4 and the number of confirmed cases increased rapidly in Hubei province, people participated in our study have potentially been exposed to the virus during their travel. Therefore, it is important to provide effective management measures to prevent the outbreak onset in our study population, especially to stop transmission during the incubation period 5 . We also recruited 424 people who did not have any travel histories but lived together with those persons.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Incidence of confirmed COVID-19 cases in our study population is 1.5 \u2030 (95% CI: 0.31\u2030-4.37\u2030), which is lower than the incidence (1.1%, 95%CI: 0.4%-3.1%) calculated from the evacuation of Korea, Japan, Germany, Singapore, and France 6 . This may be explained in two ways. First, most of participants in our study returned to Shenzhen after the travel bans and the first level emergency response of Guangdong province began on Jan 23, 2020, which resulted in restricted travel and strict temperature screening in and out of Hubei province. People who had symptoms such fever or cough were detected and isolated before entering Shenzhen. Second, less than 1% of the participants reported contact history with confirmed case and more than 85% of the subjects have taken self-protection measures (wearing masks, washing hands, reducing outdoor events, cancelling gatherings, and following cough etiquette) recommended by the government during their stay in epidemic areas. Since the common person-to-person transmission routes of 2019-nCoV are direct transmission and contact transmission 7 , the risk of getting infected is low without close contacts.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Three out of 2,004 persons have confirmed 2019-nCoV infection in our study, which highlighted the importance of home quarantine. Quarantine is defined as restricting movement of people who have potentially been exposed to infectious diseases, to prevent the transmission 8, 9 . It has been widely used in response to plague, e.g the 'Black Death\", severe acute respiratory syndrome (SARS), and Ebola outbreaks [10] [11] [12] [13] . In current COVID-19 outbreak, the \"Three in One\" Task Force, composed of community work stations, community health centers and community police, is responsible for home quarantine and medical observation in As of March 31, 2020, there was no other case reported in the study population or in close contacts with our study population, there was also no case reported in people who finished home quarantine in Shenzhen. This indicates that although there was case tested positive after 14 days of home quarantine in our study, the risk of second outbreak due to people who complete home quarantine may be low. However, further information and study is required to estimate if asymptomatic patients are highly contagious. 13 The main limitation of this study is that nasopharyngeal swab sample was collected only once.",
            "cite_spans": [
                {
                    "start": 271,
                    "end": 273,
                    "text": "8,",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 274,
                    "end": 275,
                    "text": "9",
                    "ref_id": null
                },
                {
                    "start": 410,
                    "end": 414,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 415,
                    "end": 419,
                    "text": "[11]",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 420,
                    "end": 424,
                    "text": "[12]",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 425,
                    "end": 429,
                    "text": "[13]",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 1151,
                    "end": 1153,
                    "text": "13",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "According to previous reports 15, 16 , there were cases who were tested negative several times before they confirmed as 2019-nCoV infections, and laboratory results may show false negatives. However, as of Feb 29, 2020, there was no other case reported in the study population. Another limitation is that this is a sampling survey not a census, incidence calculated in this study may not be representative for all the people under home quarantine in Shenzhen. We used a stratified multistage random sampling method and recruited participants who traveled from epidemic areas within 14 days before the home quarantine. Although further study is needed to get a more accurate incidence, our findings provides preliminary evidence to improve measures of prevention and control of COVID-19, especially for areas and cities that need to reduce early transmission from imported cases.",
            "cite_spans": [
                {
                    "start": 30,
                    "end": 33,
                    "text": "15,",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 34,
                    "end": 36,
                    "text": "16",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "This study suggests that home quarantine has been effective in reducing the early transmission of COVID-19, but that more needs to be done to improve early detection of COVID-19 infection. Regular training and systematic supervision for community health workers, as well as comprehensive and feasible instructions for people under home quarantine should be provided. Since the 2019-nCoV has been transmitted in multiple countries, caused severe illness and huge disease burden, experiences in China should be considered across the world.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "A Novel Coronavirus from Patients with Pneumonia in China",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "8",
            "pages": "727--733",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Early Transmission Dynamics in Wuhan, China, of Novel Coronavirus-Infected Pneumonia",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Guideline for New Coronavirus Infection Prevention and Control, the third edition",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Team -nCR. Initial Public Health Response and Interim Clinical Guidance for the 2019 Novel Coronavirus Outbreak -United States",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Patel",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "B"
                    ],
                    "last": "Jernigan",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "MMWR Morb Mortal Wkly Rep",
            "volume": "69",
            "issn": "5",
            "pages": "140--146",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Feasibility of controlling COVID-19 outbreaks by isolation of cases and contacts",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Hellewell",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Abbott",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Gimma",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet Glob Health",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Epidemic size of novel coronavirus-infected pneumonia in the Epicenter Wuhan: using data of five-countries' evacuation action",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Hongxin",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Sailimai",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Bo",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Yi",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "02",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "2019-nCoV transmission through the ocular surface must not be ignored",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "W"
                    ],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [
                        "F"
                    ],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "F"
                    ],
                    "last": "Jia",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "The psychological impact of quarantine and how to reduce it: rapid review of the evidence",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "K"
                    ],
                    "last": "Brooks",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "K"
                    ],
                    "last": "Webster",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "E"
                    ],
                    "last": "Smith",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Mother Nature versus human nature: public compliance with evacuation and quarantine",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "E"
                    ],
                    "last": "Manuell",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Cukor",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Disasters",
            "volume": "35",
            "issn": "2",
            "pages": "417--442",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Shutt up: bubonic plague and quarantine in early modern England",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "L"
                    ],
                    "last": "Newman",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "J Soc Hist",
            "volume": "45",
            "issn": "3",
            "pages": "809--834",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Understanding, compliance and psychological impact of the SARS quarantine experience",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "L"
                    ],
                    "last": "Reynolds",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "R"
                    ],
                    "last": "Garay",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "Deamond",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "K"
                    ],
                    "last": "Moran",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Gold",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Styra",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Epidemiol Infect",
            "volume": "136",
            "issn": "7",
            "pages": "997--1007",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Modeling the impact of quarantine during an outbreak of Ebola virus disease",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "D\u00e9nes",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "B"
                    ],
                    "last": "Gumel",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Infect Dis Model",
            "volume": "4",
            "issn": "",
            "pages": "12--27",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "China. Lancet",
            "volume": "395",
            "issn": "",
            "pages": "497--506",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "2020. 16. account Yo. Positive after several negative nucleic acid tests, experts analyze the cause of such new cases of coronary pneumonia",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Winichakoon",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Chaiwarith",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Liwsrisakun",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "J Clin Microbiol",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF1": {
            "text": "Shenzhen. During the 14 days of quarantine, people are required to stay at home and daily necessities are ordered and delivered by community workers. The task force members take their body temperature and record their health status twice a day, and health workers in the task force need to assess if he or she need further investigation. People who have travel history in epidemic areas and their cohabitants have red code on a smart phone application called iShenzhen, people who staye at Shenzhen have green code. Community workers and community polices take temperature and check codes for everyone in and out of the community at the entrance, and only people with green code could pass. After 14 days of home quarantine, if people with travel history in epidemic areas do not have any symptoms, they will receive a green code and a proof of complete quarantine. Consistent with previous experience, home quarantine for people who have travel history in epidemic areas has been proven effective to prevent community transmission during the incubation period and the period of early onset.However, one of the three cases was confirmed as 2019-nCoV infection 16 days after his arrival (case 1). This patient had a transient high body temperature(37.4\u00b0C) and another case had dry cough during the home quarantine. Moreover, previous case reports suggested a high risk of severe illness for elderly people and those who had underlying health conditions14 . In our study, 4.46% of the sample aged above 60 years old and 9.47% had at least one type of underlying disease such as hypertension, diabetes, chronic obstructive pulmonary disease, et al. Awareness of incipient symptoms of COVID-19 during the quarantine is important to speed up early detection and reduce poor clinical outcomes. Therefore, regular training for community health workers and comprehensive instruction for people under home quarantine are required.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Demographic properties of the sample",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "shows the travel history of the sample in this study. A total of 1,213 subjects (74.10%) traveled to epidemic areas within 14 days before the home quarantine. From them 129 people have traveled to Wuhan city, 1,046 people have traveled to other cities in Hubei province, and 40 participants have been to other provinces such as Sichuan and Beijing. Most of the subjects drove private car for traveling and 15.20% of the sample used public transportations.",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "Travel history and contact history of the sample Did not travel to epidemic areas within 14 days before the medical",
            "latex": null,
            "type": "table"
        },
        "TABREF3": {
            "text": "Self-protection behaviors of the participants",
            "latex": null,
            "type": "table"
        },
        "TABREF4": {
            "text": "Characteristics of confirmed cases in the sample",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": " 14 We would like to express our gratitude to Shenzhen Municipal Health Commission and Shenzhen Center for Disease Control and Prevention, which provided most of the data and gave us the opportunity to do this research. We also owe our sincere gratitude to BGI group in Shenzhen that carried out all the laboratory tests in the survey. Last my thanks go to all the ten Districts Center for Disease Control and Prevention who helped with data collection.",
            "cite_spans": [
                {
                    "start": 1,
                    "end": 3,
                    "text": "14",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [],
            "section": "Acknowledgements"
        }
    ]
}