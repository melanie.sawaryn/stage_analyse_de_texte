{
    "paper_id": "7b3861344a4f0aa3d3e3a58010025ac2fc05430b",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "M ycoplasma pneumoniae is een belangrijk verwekker van 'community-acquired' pneumonie bij kinderen en jongvolwassenen. De infectie verloopt meestal niet ernstig; de belangrijkste klachten zijn niet-productief hoesten en malaise. 5-10% van de pati\u00ebnten ontwikkelt een pneumonie en bij 2% van de pati\u00ebnten is een ziekenhuisopname nodig. De symptomen ontstaan geleidelijk in meerdere dagen tot een week en de ziekteduur loopt uiteen van een paar dagen tot meer dan een maand. M. pneumoniae veroorzaakt uitbraken in gesloten instellingen, studentenhuizen en op militaire bases. 1 In dit artikel beschrijven we een uitbraak van M. pneumoniae in een instelling voor mensen met een verstandelijk beperking.",
            "cite_spans": [
                {
                    "start": 574,
                    "end": 575,
                    "text": "1",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In week 40 van 2013 ontwikkelden 5 cli\u00ebnten en personeelsleden van een instelling voor mensen met een verstandelijke beperking klachten van hoesten en koorts. Vanwege deze klachten werden 3 pati\u00ebnten opgenomen in het ziekenhuis, van wie 2 op de IC-afdeling met beademing. De GGD Rotterdam-Rijnmond werd ge\u00efnformeerd over deze uitbraak. In deze instelling was een maand eerder bij een routinecontrole van de waterleidingen Legionella pneumophila type 1 en Legionella non-pneumophila geconstateerd.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Uitbraak"
        },
        {
            "text": "achtergrond In oktober 2013 meldde een instelling voor mensen met een verstandelijke beperking een uitbraak van Mycoplasma pneumoniae-infecties bij de GGD Rotterdam.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Uitbraak"
        },
        {
            "text": "casUs In totaal werden 58 mogelijke besmettingen vastgesteld, waarvan 12 werden bevestigd in het laboratorium: 5 met PCR-bepalingen op keeluitstrijkjes, 3 door een verhoogde IgM-waarde in het serum, 2 via IgM-seroconversie en 2 door een verhoogde IgG-titer in opeenvolgende serummonsters. Om de uitbraak te bestrijden werden maatregelen getroffen in samenwerking met de GGD. Iedere pati\u00ebnt die hoestte en koorts of malaise had werd beschouwd als 'mogelijk ge\u00efnfecteerd' en werd direct behandeld met antibiotica; dit vond zo veel mogelijk plaats in cohortverpleging. Het personeel spande zich in om de aangescherpte hand-en hoesthygi\u00ebnemaatregelen uit te leggen aan de bewoners.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Uitbraak"
        },
        {
            "text": "conclUsie We bestreden met succes een uitbraak van Mycoplasma pneumoniae-infecties in een instelling voor mensen met een verstandelijke beperking door actieve surveillance, behandeling van mogelijke infecties en hygi\u00ebnemaatregelen. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Uitbraak"
        },
        {
            "text": "Tijdens overleg tussen de instelling, arts-microbioloog, GGD en het Centrum voor Infectieziektebestrijding van het RIVM werden afspraken gemaakt over het te voeren beleid. De werkdiagnose was 'M. pneumoniae-uitbraak' en in de differentiaaldiagnose van mogelijke verwekkers van de uitbraak stonden L. pneumophila en influenzavirus. Een mogelijke infectie met M. pneumoniae definieerden wij als 'een cli\u00ebnt die of een personeelslid dat hoestte en koorts of algehele malaise had vanaf 30 september'. Een bevestigd geval werd gedefinieerd als 'een mogelijk ge\u00efnfecteerde pati\u00ebnt die M. pneumoniae-positief testte op een keeluitstrijk via PCR-onderzoek of bij wie er serologisch aanwijzingen waren voor een recente infectie'. De serologische aanwijzingen voor een recente infectie bestonden uit: (a) eenmalig een positieve IgMwaarde, (b) seroconversie in serummonsters die onafhankelijk van elkaar in de tijd waren afgenomen (een serumpaar), of (c) een verdubbeling van de IgA-of IgGwaarde in een serumpaar. Met seroconversie wordt bedoeld dat in het eerste serummonster van een serumpaar de IgM-waarde negatief was, terwijl deze in het volgende monster positief is geworden. In het overleg spraken wij ook het beleid af om de uitbraak te bestrijden. Dit bestond uit: (a) het actief benaderen van het personeel en de cli\u00ebnten om zich te melden bij klachten van hoesten, koorts of malaise; (b) het direct inzetten van diagnostiek bij een mogelijke infectie en directe behandeling van deze pati\u00ebnt met antibiotica; en (c) het instellen van cohortverpleging voor zieke cli\u00ebnten, het thuishouden van ziek personeel en het aanscherpen van de hand-en hoesthygi\u00ebne in de instelling. De standaard-antibioticabehandeling van mogelijk ge\u00efnfecteerde pati\u00ebnten bestond uit doxycycline 200 mg 1 dd gedurende de 1e dag, gevolgd door 100 mg 1 dd gedurende 6 dagen conform de NHG-standaard 'Acuut hoesten'. 2 Later werd dit gewijzigd naar azitromycine 500 mg 1 dd gedurende 3 dagen, omdat deze behandeling als alternatief werd gegeven in de SWAB-richtlijn en enkele dagen korter is. 3 2 cli\u00ebnten zonder klachten die vanwege hun handicap een sterk verhoogd risico op een gecompliceerd beloop van een luchtweginfectie hadden, behandelden we profylactisch met azitromycine 500 mg 1 dd gedurende de 1e dag, gevolgd door 250 mg 1 dd gedurende 4 dagen.",
            "cite_spans": [
                {
                    "start": 1886,
                    "end": 1887,
                    "text": "2",
                    "ref_id": null
                },
                {
                    "start": 2062,
                    "end": 2063,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Uitbraakbeleid"
        },
        {
            "text": "De instelling bestond uit 12 woningen met een gedeelde woonkamer en sanitair, hierin woonden 100 cli\u00ebnten. De zieke cli\u00ebnten werden samengebracht in 4 woningen. Cohortverpleging werd overwogen, maar dit kon niet volledig gerealiseerd worden. Het personeel werd niet uitgewisseld tussen de woningen, maar isolatie van patienten was beperkt haalbaar omdat dit leidde tot onbegrip en pati\u00ebnten moeite hadden zich hieraan aan te passen. Zieke cli\u00ebnten werden tijdens gezamenlijke activiteiten, zoals de maaltijd, gescheiden gehouden van de niet-zieke bewoners. Verder konden zij zich, afhankelijk van hun beperking, vrij in de woning bewegen. De verzorging vond plaats op de kamer. Zieke cli\u00ebnten konden niet deelnemen aan dagbestedingsactiviteiten in een ander gebouw. Alle woningen ontvingen schriftelijke informatie waarin het belang van goede hand-en hoesthygi\u00ebne en ventilatie werd uitgelegd. Posters met instructies voor handhygi\u00ebne werden opgehangen in de keukens, toiletten en badkamers en de woningen werden dagelijks doorgelucht. In week 42 werden 26 zieke cli\u00ebnten behandeld. Tijdens overleg tussen de instelling en de GGD in week 45 stelden wij vast dat het aantal mogelijke infecties per week leek af te nemen. Daarom besloten wij om door te gaan ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Uitvoering Uitbraakbeleid"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Epidemie van luchtweginfecties door Mycoplasma pneumoniae in een instelling voor verstandelijk gehandicapten onderzocht met polymerasekettingreactie op keeluitstrijk",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "W"
                    ],
                    "last": "Dorigo-Zetsma",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "De Wit",
                    "suffix": ""
                },
                {
                    "first": "Timmerman-N\u00e1ray",
                    "middle": [],
                    "last": "Szab\u00f3",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Schneeberger",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "M"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Ned Tijdschr Geneeskd. 1999",
            "volume": "12",
            "issn": "",
            "pages": "1261--1266",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "The Dutch Working Party on Antibiotic Policy and the Dutch Association of Chest Physicians. Dutch guidelines on the management of communityacquired pneumonia in adults",
            "authors": [],
            "year": 2011,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Utility of an internal control for the polymerase chain reaction. Application to detection of Mycoplasma pneumoniae in clinical specimens",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "P"
                    ],
                    "last": "Ursi",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ursi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ieven",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "R"
                    ],
                    "last": "Pattyn",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "APMIS",
            "volume": "100",
            "issn": "",
            "pages": "635--644",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Polymerase chain reaction is superior to serology for the diagnosis of acute Mycoplasma pneumoniae infection and reveals a high rate of persistent infection",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "C"
                    ],
                    "last": "Nilsson",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Bj\u00f6rkman",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Persson",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "BMC Microbiol",
            "volume": "8",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Comparison of PCR, culture, and serological tests for diagnosis of Mycoplasma pneumoniae respiratory tract infection in children",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "W"
                    ],
                    "last": "Dorigo-Zetsma",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "A"
                    ],
                    "last": "Zaat",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "M"
                    ],
                    "last": "Wertheim-Van Dillen",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Spanjaard",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rijntjes",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Van Waveren",
                    "suffix": ""
                }
            ],
            "year": 1999,
            "venue": "J Clin Microbiol",
            "volume": "37",
            "issn": "",
            "pages": "14--21",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Enhanced control of an outbreak of Mycoplasma pneumoniae pneumonia with azithromycin prophylaxis",
            "authors": [
                {
                    "first": "Jd1",
                    "middle": [],
                    "last": "Klausner",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Passaro",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rosenberg",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "L"
                    ],
                    "last": "Thacker",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "F"
                    ],
                    "last": "Talkington",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "B"
                    ],
                    "last": "Werner",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Vugia",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "J Infect Dis",
            "volume": "177",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Prevalence of Mycoplasma pneumoniae in subjectively healthy individuals",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Gnarpe",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Lundb\u00e4ck",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Sundel\u00f6f",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Gnarpe",
                    "suffix": ""
                }
            ],
            "year": 1992,
            "venue": "Scand J Infect Dis",
            "volume": "24",
            "issn": "",
            "pages": "161--165",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Carriage of Mycoplasma pneumoniae in the upper respiratory tract of symptomatic and asymptomatic children: an observational study",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "N"
                    ],
                    "last": "Adrichem",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "PLoS Med",
            "volume": "10",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Sensitivity of urinary antigen test in relation to clinical severity in a large outbreak of Legionella pneumonia in Spain",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "M"
                    ],
                    "last": "Bl\u00e1zquez",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "J"
                    ],
                    "last": "Espinosa",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Mart\u00ednez-Toldos",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Alemany",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "C"
                    ],
                    "last": "Garc\u00eda-Orenes",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Segovia",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Eur J Clin Microbiol Infect Dis",
            "volume": "24",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Epidemische curve van de uitbraak van Mycoplasma pneumoniainfecties in een instelling voor verstandelijk gehandicapten met uitgezet in de tijd het aantal mogelijke infecties ( ), de bevestigde infecties ( ) en de bevestigde infecties met ziekenhuisopname ( ). De pati\u00ebnt met de eerste bevestigde infectie in week 36 beschouwden wij als de indexpati\u00ebnt. inzetten van diagnostiek en het behandelen van mogelijke infecties tot week 48, dat wil zeggen: tot 2 incubatieperiodes na het laatste bevestigde ziektegeval. Daarna werd het uitbraakbeleid be\u00ebindigd.diagnostiek tijdens de Uitbraak Van de eerste besmette pati\u00ebnten werd het serum onderzocht op M. pneumoniae en Legionella. Daarnaast werd het L.pneumophila-urineantigeen bepaald en op een keeluitstrijk werden PCR-tests gedaan voor M.pneumoniae en het influenzavirus. 4 Nadat 3 van de 10 geteste pati\u00ebnten positief waren bevonden voor M. pneumoniae werd alleen nog diagnostiek ingezet naar deze verwekker. Daarbij werd het serologisch onderzoek gestopt, omdat dit geen therapeutische consequenties had en voor sommige cli\u00ebnten belastend was. In alle beschikbare serummonsters werd Chlamydophila pneumoniae nabepaald. In totaal identificeerden wij 58 mogelijke infecties met M. pneumoniae, waarvan 12 infecties werden bevestigd. Het grootste aantal mogelijke infecties werd in week 42 ge\u00efdentificeerd (n = 26), waarna het aantal infecties per week snel afnam (figuur). Van de 12 pati\u00ebnten met bevestigde infecties hadden 8 pati\u00ebnten een longontsteking, die bij 6 van hen werd bevestigd met een r\u00f6ntgenfoto van de thorax. 4 besmette pati\u00ebnten werden opgenomen in het ziekenhuis, van wie 2 werden opgenomen op de IC-afdeling met beademing. 1 van de beademde pati\u00ebnten had een onderliggende hartafwijking. De opname van de andere IC-pati\u00ebnt werd gecompliceerd door een longembolie. In totaal werden 48 keeluitstrijkjes bij mogelijk besmette pati\u00ebnten afgenomen op 0-22 dagen na de 1e ziektedag. In 5 van deze keeluitstrijkjes liet PCR-onderzoek M. pneumoniae zien. Daarnaast waren van 4 pati\u00ebnten 2 verschillende serummonsters in de tijd beschikbaar.Deze bevestigden een recente M. pneumoniae-infectie; bij 2 pati\u00ebnten in de vorm van seroconversie en bij de 2 andere pati\u00ebnten door sterke stijgingen van de IgA-en IgG-waarden. Verder was van 10 verschillende pati\u00ebnten 1 serummonster beschikbaar, afgenomen 3-42 dagen na de 1e ziektedag (mediaan: 7 dagen). In 3 van deze serummonsters wees de IgM-bepaling op een M. pneumoniaeinfectie; deze serummonsters waren telkens langer dan 7 dagen na de 1e ziektedag afgenomen. Kijk voor de tabel met de laboratoriumuitslagen van de pati\u00ebnten bij wie een M. pneumoniae-infectie werd vastgesteld op www.ntvg.nl/A7888. In aanvulling op de M. pneumoniae-diagnostiek werd bij 9 pati\u00ebnten ook een influenzatest verricht. Ook werd legionella-diagnostiek gedaan: bij 11 pati\u00ebnten met de sneltest op L. pneumophila type 1 en bij 13 pati\u00ebnten tabel Laboratoriumuitslagen van de pati\u00ebnten bij wie een Mycoplasma pneumoniae-infectie werd bevestigd (n = 12)",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": ".B. N.B. N.B. N.B. N.B. N.B. N.B. N.V.T. nee nee nee geen pati\u00ebnt 9 pos. 5 N.B. N.B. N.B. N.B. N.B. N.B. N.B. N.V.T. nee nee nee geen pati\u00ebnt 10 pos. 11 neg. N.B. neg. 17 N.B. N.B. N.B. N.V..B. N.B. N.B. N.V.T. klinisch nee nee geen pati\u00ebnt 12 pos. 18 N.B. N.B. N.B. N.B. N.B. N.B. N.B. N.V.T. nee nee nee geen N.B. = niet beschikbaar; N.V.T. = niet van toepassing; pos. = positief; neg. = negatief.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Drs. H.C.C. de Jonge, arts infectieziektebestrijding in opleiding;I.van  der Meer, sociaal verpleegkundige; dr. A. Tjon-A-Tsien, arts maatschappij en gezondheid. Erasmus Medisch Centrum, Rotterdam. Afd. Huisartsgeneeskunde: drs. E.I. van der Zon-van Welzenis, arts voor verstandelijk gehandicapten. Afd. Medische Microbiologie: dr. C.F.M. van der Donk; artsmicrobioloog in opleiding. Maasstad Ziekenhuis, afd. Medische Microbiologie, Rotterdam. Dr. O. Pontesilli, arts-microbioloog. Contactpersoon: drs. H.C.C. de Jonge (h.c.c.dejonge@gmail.com). Een uitbraak van Mycoplasma pneumoniae in een instelling voor mensen met een verstandelijke beperking H.C.C. (Erik) de Jonge, Ilse van der Meer, aim\u00e9e Tjon-a-Tsien, Evelien I. van der Zon-van Welzenis, Christel F.M. van der Donk en Oscar Pontesilli KLINISCHE PR AKTIJK Daarom was de arts van deze instelling bang dat de patienten legionellose hadden. De legionella-diagnostiek bij de pati\u00ebnten die in het ziekenhuis waren opgenomen, was echter negatief. 1 van de opgenomen pati\u00ebnten onderging een broncho-alveolaire lavage (BAL). Met PCR-onderzoek op de BAL-vloeistof werd M. pneumoniae gedetecteerd.",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "de ernst van hun luchtweginfectie werden opgenomen in het ziekenhuis. Daarnaast kan tijdens een uitbraak het aantal urineantigeentests worden meegenomen in de diagnostische afwegingen, dit in tegenstelling tot bij individuele diagnostiek. Tijdens deze uitbraak was de urineantigeentestuitslag negatief bij 11 pati\u00ebnten. Legionellose leek daarmee uitgesloten. Later bleken ook de uitslagen van de serologische bepalingen negatief bij alle pati\u00ebnten.De literatuur suggereert dat de maatregelen die wij hebben toegepast effectief zijn, maar het is lastig om dit vast te stellen. 1 Opvallend is dat aan het begin van de uitbraak 4 mensen ernstig ziek werden, terwijl een M. pneumoniae-infectie meestal licht verloopt. Bij 2 van deze patienten was er een onderliggend lijden, maar bij 2 pati\u00ebnten niet. Het actief benaderen van nieuwe pati\u00ebnten met klachten van een luchtweginfectie heeft er mogelijk voor gezorgd dat bij een aantal pati\u00ebnten de ziekte in een vroe-ger stadium is behandeld en dat complicaties zijn voorkomen, maar dit kan niet bevestigd worden. De definitie van een mogelijke infectie was breed, waardoor mogelijk een aantal infecties onterecht als zodanig benoemd is. De implementatie van hygi\u00ebnebeleid in een instelling voor mensen met een verstandelijke beperking is maatwerk. Het succesvol invoeren van aangescherpte handen hoesthygi\u00ebne en ventilatie vereisen inzicht in de dagelijkse gang van zaken, en inspanning van het personeel om de maatregelen uit te leggen aan bewoners. Zo kan het moeilijk zijn voor een pati\u00ebnt met een verstandelijke beperking om te begrijpen en te accepteren dat hij of zij niet naar het dagbestedingsprogramma mag. Een werkbezoek aan de instelling en regelmatig overleg tussen de GGD en de medische en verpleegkundige staf van de instelling was noodzakelijk om te komen tot een passend beleid.De bestrijding van een uitbraak van een atypische luchtweginfectie in instellingen vereist goede samenwerking en afstemming tussen de betrokken professionals met ruime aandacht voor de context van de uitbraak. Dit is mogelijk de sleutel geweest tot het succesvol onderdrukken van de besproken M. pneumoniae-uitbraak in een instelling voor mensen met een verstandelijke beperking. en financi\u00eble ondersteuning: geen gemeld.Aanvaard op 15 oktober 2014 Citeer als: Ned Tijdschr Geneeskd. 2014;158:A7888 \u2022 > kijk ook oP www.ntvg.nl/a7888 \u25bc leerPUnten \u25bc \u2022 Als een infectieziekte uitbreekt in een instelling voor mensen met een verstandelijke beperking, moet extra aandacht worden besteed aan de implementatie van bestrijdingsmaatregelen. \u2022 Het succesvol invoeren van aangescherpte hand-en hoesthygi\u00ebne en adequate ventilatie in een instelling vereisen inzicht in de dagelijkse gang van zaken, en inspanning van het personeel om de maatregelen uit te leggen aan bewoners. \u2022 Regelmatig overleg tussen de GGD en de medische en verpleegkundige staf van de instelling draagt bij aan succesvolle implementatie van bestrijdingsmaatregelen. \u2022 Een uitbraak van Mycoplasma pneumoniae-infecties kan worden vastgesteld via PCR-onderzoek op keeluitstrijkjes, maar hierbij moet rekening worden gehouden met de mogelijkheid van asymptomatisch dragerschap.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}