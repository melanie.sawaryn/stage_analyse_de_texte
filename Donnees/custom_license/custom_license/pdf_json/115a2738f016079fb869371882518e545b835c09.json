{
    "paper_id": "115a2738f016079fb869371882518e545b835c09",
    "metadata": {
        "title": "Comment",
        "authors": [
            {
                "first": "Yongwen",
                "middle": [],
                "last": "Luo",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Kai",
                "middle": [],
                "last": "Yin",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Since December, 2019, the outbreak of coronavirus disease 2019 (COVID-19), which originated in Wuhan, China, has become a global public health threat. 1 On Feb 28, 2020, WHO upgraded their assessment of the risk of spread and the risk of impact of COVID-19 to very high at global level. By March 10, 2020, 116 166 cases have been reported globally, causing 4088 deaths. The epidemic has spread to 118 countries around the world. 2 With immunocompromised status and physiological adaptive changes during pregnancy, pregnant women could be more susceptible to COVID-19 infection than the general population. As COVID-19 is rapidly spreading, maternal management and fetal safety become a major concern, but there is scarce information of assessment and management of pregnant women infected with COVID-19, and the potential risk of vertical transmission is unclear. In The Lancet Infectious Diseases, Nan Yu and colleagues 3 report the clinical features and obstetric and neonatal outcomes of pregnancy with COVID-19 pneumonia in Wuhan, China. Seven pregnant women with COVID-19 pneumonia were assessed and the onset symptoms were similar to those reported in non-pregnant adults with COVID-19. All patients received oxygen therapy and antiviral treatment in isolation. All patients had caesarean section after consultation with a multidisciplinary team and the outcomes of the pregnant women and neonates were good. Three neonates were tested for severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2), and one was found to be infected with COVID-19 36 h after birth. The findings of the study provide some indications for clinical assessment and management of pregnant women with COVID-19, but questions remain on how to manage pregnant women infected with COVID-19.",
            "cite_spans": [
                {
                    "start": 429,
                    "end": 430,
                    "text": "2",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Management of pregnant women infected with COVID-19"
        },
        {
            "text": "As Yu and colleagues 3 reported, five pregnant women were treated with steroids after caesarean section. Two were also treated with traditional Chinese medicine. However, no reliable evidence recommends any specific COVID-19 treatment for pregnant women. WHO guidance and some clinical evidence does not recommend the use of corticosteroids for COVID-19. 4, 5 Use of drugs in pregnant women needs to be on the basis of solid evidence. Clinical trials are needed to prove the effectiveness of drugs and the effects on the fetus to establish a standardised treatment for pregnant women with COVID-19. More evidence of the safety of traditional Chinese medicine is also warranted.",
            "cite_spans": [
                {
                    "start": 355,
                    "end": 357,
                    "text": "4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 358,
                    "end": 359,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Management of pregnant women infected with COVID-19"
        },
        {
            "text": "The time of delivery in the study was 37 weeks to 41 weeks plus 5 days, all by caesarean section. In cases of pregnant women with COVID-19, more evidence is needed to establish when to deliver and when caesarean sections should be recommended. Previous treatment experience has been inconclusive about which delivery method is safer in this patient population. Zhu and colleagues 6 reported nine pregnant women with COVID-19. Seven of the women delivered their babies by cesarean section and two by vaginal delivery. All three neonates delivered vaginally (including two who were twins) had an Apgar score of at least 9 and negative nucleic acid test. Yudin and colleagues 7 reported a pregnant woman with SARS at 31 weeks of gestation; the patient stayed for 21 days in the hospital and did not require intensive care admission or ventilatory support, and a healthy baby girl was delivered by vaginal birth. It is unknown whether vaginal delivery increases the infection risk. Further research is needed to assess the risk and to produce guidelines for delivery times and methods in patients with COVID-19.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Management of pregnant women infected with COVID-19"
        },
        {
            "text": "As discussed in the study, although all mothers and infants showed good outcomes, all enrolled pregnant women were in the third trimester, and all had only mild symptoms. Hence, the effect of SARS-CoV-2 infection on the fetus in the first or second trimester or in patients with moderate to severe infection is unknown. As a previous study reported, SARS coronavirus infection during pregnancy might cause preterm birth, intrauterine growth restriction, intrauterine death, and neonatal death. 8 Considering that the potential of SARS-CoV-2 to cause severe obstetric and neonatal adverse outcomes is unknown, rigorous screening of suspected cases during pregnancy and long-term follow-up of confirmed mothers and their neonates are needed.",
            "cite_spans": [
                {
                    "start": 494,
                    "end": 495,
                    "text": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Management of pregnant women infected with COVID-19"
        },
        {
            "text": "In the study by Yu and colleagues, 3 three neonates were tested for SARS-CoV-2, of whom two were negative. One neonate was positive, but the viral nucleic acid tests of the placenta and cord blood in this case were negative. At the end of follow-up, no pneumonia and other clinical symptoms and signs were reported in any of the seven neonates. No reliable evidence has been provided in support of the possibility of vertical transmission of COVID-19 infection from mother to baby. The outcomes are consistent with previous reports. 9, 10 But all these studies only assessed a small number of cases. Future studies should include a larger number of samples across multiple centres to establish whether vertical transmission can occur between mother and child.",
            "cite_spans": [
                {
                    "start": 533,
                    "end": 535,
                    "text": "9,",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 536,
                    "end": 538,
                    "text": "10",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Management of pregnant women infected with COVID-19"
        },
        {
            "text": "Yu and colleagues' 3 report of the clinical features and obstetric and neonatal outcomes of pregnant women with COVID-19 provides a reference for clinical assessment and management of this patient population. However, understanding of SARS-CoV-2, especially the effect on pregnant women and neonates, is still insufficient. We need to further strengthen research to provide an evidence-based foundation for the medical management of pregnant patients with COVID-19.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Management of pregnant women infected with COVID-19"
        },
        {
            "text": "We declare no competing interests. We provided the management and followup information for the neonate positive for SARS-CoV-2 described in the paper by Yu and colleagues; we were not involved in the management of the pregnant women described in the paper.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Management of pregnant women infected with COVID-19"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Early transmission dynamics in Wuhan, China, of novel coronavirus-infected pneumonia",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "/NEJMoa2001316. 2 WHO. Coronavirus disease 2019 (COVID-2019) situation report",
            "volume": "46",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Clinical features and obstetric and neonatal outcomes of pregnant patients with COVID-19 in Wuhan, China: a retrospective, single-centre, descriptive study",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Kang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Clinical management of severe acute respiratory infection when novel coronavirus (nCoV) infection is suspected",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Clinical evidence does not support corticosteroid treatment for 2019-nCoV lung injury",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "D"
                    ],
                    "last": "Russell",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "E"
                    ],
                    "last": "Millar",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "K"
                    ],
                    "last": "Baillie",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "473--75",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Clinical analysis of 10 neonates born to mothers with 2019-nCoV pneumonia",
            "authors": [
                {
                    "first": "H P",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "L W",
                    "middle": [],
                    "last": "Cheng",
                    "suffix": ""
                },
                {
                    "first": "Z F",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Transl Pediatr",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.21037/tp.2020.02.06"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Severe acute respiratory syndrome in pregnancy",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "H"
                    ],
                    "last": "Yudin",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "M"
                    ],
                    "last": "Steele",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Sgro",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "E"
                    ],
                    "last": "Read",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Kopplin",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "A"
                    ],
                    "last": "Gough",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Obstet Gynecol",
            "volume": "105",
            "issn": "",
            "pages": "124--151",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "A case-controlled study comparing clinical course and outcomes of pregnant and non-pregnant women with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Lam",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "F"
                    ],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "N"
                    ],
                    "last": "Leung",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "BJOG",
            "volume": "111",
            "issn": "",
            "pages": "771--74",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Pregnant women with new coronavirus infection: a clinical characteristics and placental pathological analysis of three cases",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Luo",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Zhonghua Bing Li Xue Za Zhi",
            "volume": "49",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Clinical characteristics and intrauterine vertical transmission potential of COVID-19 infection in nine pregnant women: a retrospective review of medical records",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Guo",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "809--824",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}