{
    "paper_id": "80a1ed4accf4600bd8376afb9db68d0c795d7407",
    "metadata": {
        "title": "Surveillance of Viral Gastroenteritis in Japan: Pediatric Cases and Outbreak Incidents",
        "authors": [
            {
                "first": "S",
                "middle": [],
                "last": "Inouye",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "K",
                "middle": [],
                "last": "Yamashita",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "S",
                "middle": [],
                "last": "Yamadera",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "M",
                "middle": [],
                "last": "Yoshikawa",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "N",
                "middle": [],
                "last": "Kato",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "N",
                "middle": [],
                "last": "Okabe",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Surveillance results from pediatric cases and outbreaks of viral gastroenteritis in Japan are presented. In winter, both small round structured virus (SRSV, or Norwalk-like viruses) and rotavirus were detected from infants with gastroenteritis; however, in recent years, the prevailing time of SRSV infection has preceded that of rotavirus infection. Most nonbacterial gastroenteritis outbreaks were related to SRSV infection, and 160% of the outbreaks were caused by contaminated food. In small-sized outbreaks, raw oysters were the primary source of transmission. In large-sized outbreaks, school lunches and catered meals that were served at schools, banquet halls, and hospitals were most often implicated in the transmission of foodborne gastroenteritis.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "in Japan, when an outbreak of food poisoning occurs, the attending physician must notify the nearby health center. The health center conducts field and laboratory investigations. If the gastroenteritis is suspected to be of nonbacterial origin, the specimens are transferred to PHIs for virologic examination. Laboratory data on each outbreak, together with the epidemiology data, are electronically reported by PHIs to IDSC even if the outbreaks are not confirmed positive for a virus.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Pediatric cases of gastroenteritis. For the period from October 1994 to March 1999, reports from sentinel clinics of viral gastroenteritis due to rotavirus or SRSVs have been plotted by week (figure 1). The number of patients reported each week indicates a distinct winter/spring peak. SRSV and group A rotavirus were detected in samples from gastroenteritis patients, but the peak time of SRSV detection was earlier than that of rotavirus (figure 1B, 1C). In the 51st week of 1997, when patient numbers peaked, gastroenteritis was occurring throughout the country (figure 2); this geographic pattern was similar to that observed in the last weeks of 1995 [3] .",
            "cite_spans": [
                {
                    "start": 656,
                    "end": 659,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Results and Discussion"
        },
        {
            "text": "The age distribution of virus-positive patients in the 1997/ 1998 season demonstrates a difference in the age pattern with SRSVs versus rotavirus infections (figure 3.) Half of the SRSVpositive patients and three-fourths of the rotavirus-positive patients were \u04401 year old.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results and Discussion"
        },
        {
            "text": "This surveillance system for children's gastroenteritis does not allow for estimation of the total number of patients throughout Japan. Therefore, in April 1999, a new Infectious Diseases Control Law was implemented that assigned surveillance to the national and local governments and reassigned sentinel clinics so that the total number of patients can be calculated [4] . If the proportion of SRSV-related gastroenteritis patients in the population can be determined, then the burden of SRSV infection to society can be estimated. Outbreak incidents of gastroenteritis. The monthly number of outbreaks of nonbacterial gastroenteritis reported from January 1997 to March 1999 also shows a distinct winter peak (figure 4). In June 1997, the Food Sanitation Law was amended so that gastroenteritis-causing virus in food is now considered to be a form of food poisoning. In addition, microbiology staffs at some PHIs have received training in SRSV detection techniques (i.e., electron microscopy and RT-PCR). Currently, 41 PHIs can detect SRSV in fecal specimens from gastroenteritis outbreaks. As a consequence, the number of outbreaks of unknown cause were greatly decreased after the 1997 amendment to the Food Sanitation Law ( figure 4) . The winter SRSV outbreaks occurred simultaneously with pediatric cases of SRSV gastroenteritis, but the peak infection times did not exactly JID 2000;181 (Suppl 2) From January 1997 to March 1999, 265 gastroenteritis outbreaks were reported to be SRSV-related (figure 4). Only 7 outbreaks were associated with other viruses: 3 with group A rotavirus, 2 with group C rotavirus, 1 with coronavirus, and 1 with coxsackievirus A9. Records for the number of affected persons were available for 208 of these 265 outbreaks (figure 5). More than half were small, involving fewer than 20 cases per outbreak.",
            "cite_spans": [
                {
                    "start": 368,
                    "end": 371,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [
                {
                    "start": 1229,
                    "end": 1238,
                    "text": "figure 4)",
                    "ref_id": "FIGREF3"
                }
            ],
            "section": "Results and Discussion"
        },
        {
            "text": "The suspected place of origin of the 208 SRSV outbreaks was related to the size of the outbreak ( figure 6 ). In small-sized outbreaks, restaurants were most often implicated, whereas in large-scale outbreaks (150 patients), more schools, hospitals, and banquet halls were involved.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 98,
                    "end": 106,
                    "text": "figure 6",
                    "ref_id": "FIGREF6"
                }
            ],
            "section": "Results and Discussion"
        },
        {
            "text": "The suspected mode of virus transmission in the SRSV outbreaks and the food or event implicated in the foodborne outbreaks also varied by the size of the outbreak (figure 7). In 60%-80% of the outbreaks, foodborne transmission, with a single-exposure point-source pattern, was suspected. Outbreaks with person-to-person spread and of prolonged duration represented a small proportion of the total. In outbreaks without known vehicles of transmission, including person-to-person spread, vomitus may have played a role in spreading virus.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results and Discussion"
        },
        {
            "text": "Among foodborne outbreaks, more than half of the smallsized outbreaks were related to the consumption of oysters. In the larger-sized outbreaks, the proportion of illness related to oysters decreased, but the proportion related to school lunches and catered meals increased.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results and Discussion"
        },
        {
            "text": "Among 145 SRSV outbreaks that occurred in the 1997/1998 season, 57 (39%) were associated with oysters. The seasonality of these oyster-associated and -unassociated outbreaks was compared by plotting their occurrence by week with the occurrence of SRSV-positive pediatric gastroenteritis patients (figure 8) . Oyster-associated outbreaks occurred mainly in December and January when oyster consumption is highest, while oyster-unassociated outbreaks took place during a broader period, almost the same period as pediatric cases, with a peak in January.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 296,
                    "end": 306,
                    "text": "(figure 8)",
                    "ref_id": "FIGREF8"
                }
            ],
            "section": "Results and Discussion"
        },
        {
            "text": "SRSV is a causative agent for gastroenteritis in children in the winter season. In Japan, the peak of SRSV infection pre- cedes that of rotavirus infection, and most outbreaks of nonbacterial gastroenteritis are related to SRSV infection. More than 60% of the outbreaks in Japan were foodborne, and in small-sized outbreaks, raw oysters were most often implicated.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusion"
        },
        {
            "text": "In large-sized outbreaks, school lunches and catered meals served at schools, banquet halls, and hospitals were the major vehicle of transmission.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conclusion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "National Institute of Infectious Diseases and Infectious Diseases Control Division, Ministry of Health and Welfare. Viral gastroenteritis of children",
            "authors": [],
            "year": 1993,
            "venue": "Infect Agents Surveil Report",
            "volume": "19",
            "issn": "",
            "pages": "1--2",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "National Institute of Infectious Diseases and Infectious Diseases Control Division, Ministry of Health and Welfare. Outbreaks of viral gastroenteritis",
            "authors": [],
            "year": 1997,
            "venue": "Infect Agents Surveil Report",
            "volume": "19",
            "issn": "",
            "pages": "248--257",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "National Institute of Infectious Diseases and Infectious Diseases Control Division, Ministry of Health and Welfare. Viral gastroenteritis",
            "authors": [],
            "year": 1995,
            "venue": "Infect Agents Surveil Report",
            "volume": "17",
            "issn": "",
            "pages": "21--23",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "National Institute of Infectious Diseases and Infectious Diseases Control Division, Ministry of Health and Welfare. The national epidemiological surveillance of infectious diseases in compliance with the enforcement of the new infectious diseases control law",
            "authors": [],
            "year": 1999,
            "venue": "Infect Agents Surveil Report",
            "volume": "20",
            "issn": "",
            "pages": "88--90",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Viral gastroenteritis trends in Japan. A, Nos. of gastroenteritis patients per week reported by pediatric sentinel clinics. Nos. of patients clinically diagnosed per week at sentinel clinics are electronically reported to Infectious Disease Surveillance Center (IDSC), National Institute of Infectious Diseases (Tokyo). B, Nos. of patients per week positive for small round structured viruses (SRSV). C, Nos. of group A rotavirus-positive patients per week. Fecal specimens from some patients are sent to prefectural/municipal Public Health Institutes for laboratory diagnosis, and virus-positive results are electronically reported to IDSC.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Age (in years) distribution of virus-positive gastroenteritis patients in 1997/1998 season-Japan. SRSV = small round structured viruses.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Occurrence of gastroenteritis by prefecture in Japan during peak of outbreak in 51st week of 1997.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF3": {
            "text": "Nos. of nonbacterial gastroenteritis outbreaks, by month-Japan, 1997 to March 1999. Data are based on reports sent by Public Health Institutes to Infectious Disease Surveillance Center by 11 March 1999. SRSV = small round structured virus; Rota A and C = group A and C rotavirus, respectively; Corona = coronavirus; CA9 = coxsackievirus A9.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF4": {
            "text": "Size distribution of gastroenteritis outbreaks related to small round structured virus-Japan, 1997 to March 1999. 208 outbreaks with known nos. of patients are classified according to size of outbreak. Data are based on reports sent by Public Health Institutes to Infectious Disease Surveillance Center by 11 March 1999.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF5": {
            "text": "correspond with each other (for more details, seefigure 8). (As of 11 March 1999, the reported numbers in the 1998/1999 season were small because of a delay in reporting laboratory diagnoses; if all of the reports had been in, the number would have been higher.)",
            "latex": null,
            "type": "figure"
        },
        "FIGREF6": {
            "text": "Setting for 208 outbreaks of gastroenteritis associated with small round structured viruses-Japan, 1997 to March 1999. Outbreaks are classified into 3 size groups. Data are based on reports sent by Public Health Institutes to Infectious Disease Surveillance Center by 11 March 1999.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF7": {
            "text": "Suspected modes of virus transmission and implicated foods in 208 outbreaks of small round structured virus-related gastroenteritis-Japan, 1997 to March 1999. Transmission routes are classified into foodborne, person-to-person, and unknown modes. Data are based on reports sent by Public Health Institutes to Infectious Disease Surveillance Center by 11 March 1999.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF8": {
            "text": "Comparison of trends for oyster-associated and -unassociated small round structured virus (SRSV) gastroenteritis outbreaks and trends of pediatric cases with SRSV gastroenteritis in 1997/1998 season-Japan. A, Nos. of SRSV-positive outbreaks per week with or without oyster association. B, Nos. of SRSV-positive pediatric cases per week (from figure 1B).",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "We are grateful to the staff of the health centers and Public Health Institutes who conducted epidemiologic and virologic examinations.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgments"
        }
    ]
}