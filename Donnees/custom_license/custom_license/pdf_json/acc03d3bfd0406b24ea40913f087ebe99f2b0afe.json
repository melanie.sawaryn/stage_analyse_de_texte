{
    "paper_id": "acc03d3bfd0406b24ea40913f087ebe99f2b0afe",
    "metadata": {
        "title": "CHARACTERIZATION OF PERSISTENT SARS-CoV",
        "authors": [
            {
                "first": "Tetsuya",
                "middle": [],
                "last": "Mizutani",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Shuetsu",
                "middle": [],
                "last": "Fukushi",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Masayuki",
                "middle": [],
                "last": "Saijo",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Ichiro",
                "middle": [],
                "last": "Kurane",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Shigeru",
                "middle": [],
                "last": "Morikawa",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Severe acute respiratory syndrome (SARS) is a newly discovered infectious disease caused by a novel coronavirus, SARS coronavirus (SARS-CoV). 1, 2 Understanding the molecular mechanisms of the pathogenicity of SARS-CoV is a rational approach for the prevention of SARS. As the gene organization of SARS-CoV is similar to those of other coronaviruses, previous scientific data regarding coronaviruses can help in understanding the virological features of SARS-CoV. A human intestinal cell line, LoVo, was shown to permit SARS-CoV infection, resulting in the establishment of persistent infection. 3 However, the mechanism of persistence has yet to be clarified. The monkey kidney cell line, Vero E6, is often used in SARS-CoV research because of the high degree of infected Vero E6 cells requires activation of JNK and Akt signaling pathways.",
            "cite_spans": [
                {
                    "start": 142,
                    "end": 144,
                    "text": "1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 145,
                    "end": 146,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 596,
                    "end": 597,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "INTRODUCTION"
        },
        {
            "text": "sensitivity of these cells to the virus. This cell line expresses the viral receptor ACE-2 4 at high levels, and SARS-CoV infection of Vero E6 causes cytopathic effects within 24 h. [5] [6] [7] Recently, we showed that establishment of SARS-CoV persistently infected 8 8 ",
            "cite_spans": [
                {
                    "start": 182,
                    "end": 185,
                    "text": "[5]",
                    "ref_id": null
                },
                {
                    "start": 186,
                    "end": 189,
                    "text": "[6]",
                    "ref_id": null
                },
                {
                    "start": 190,
                    "end": 193,
                    "text": "[7]",
                    "ref_id": null
                },
                {
                    "start": 267,
                    "end": 270,
                    "text": "8 8",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "NFECTION IN VERO E6 CELLS I"
        },
        {
            "text": "Previously, we showed that ACE-2 was not detected in a persistently infected cell line on Western blotting analysis. ACE-2 expression was also shown to be reduced in the acute phase of SARS-CoV infection. These results suggested that virus particles produced by persistently infected cells could not infect other cells due to a lack of the receptor, resulting in a decrease in the number of virus-infected cells.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Downregulation of ACE-2 Viral Receptor by SARS-CoV Infection"
        },
        {
            "text": "The anti-apoptotic protein, Bcl-2, is activated in cells persistently infected with viruses. In the present study, we established a persistently SARS-CoV-infected cell line after passage 6. However, this cell line did not show significant activation of Bcl-2 (data not shown). On the other hand, Bcl-xL, which is also an anti-apoptotic protein, showed a different migration pattern in the persistently infected cell line as compared with mock and acutely infected cells (Fig. 1) . Previous studies indicated that the fast-migrating Bcl-xL band is unphosphorylated Bcl-xL, which has been shown to have anti-apoptotic roles. The anti-Bcl-xL antibody (Cell Signaling Co. Ltd.) recognizes both phosphorylated and unphosphorylated Bcl-xL. The slowly migrating band shown in Fig. 1 may be an inactivated form of Bcl-xL. Thus, Bcl-xL may be involved in maintenance of persistent infection.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 470,
                    "end": 478,
                    "text": "(Fig. 1)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 769,
                    "end": 775,
                    "text": "Fig. 1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "Downregulation of ACE-2 Viral Receptor by SARS-CoV Infection"
        },
        {
            "text": "Previously, we concluded that a population of cells produced from parental Vero E6 cells had the potential to support persistent infection, and that acute infection caused by a major population of seed virus was necessary for persistent infection. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DISCUSSION"
        },
        {
            "text": "p38 MAPK, SB203580, and an inhibitor of MEK, PD98059, did not affect the establishment of persistent infection, whereas no surviving cells were observed after treatment with the JNK inhibitor, SP600125, or the PI3K/Akt inhibitor, LY294002. 8 The anti-apoptotic protein, Bcl-2, is capable of blocking apoptosis caused by RNA virus infection.",
            "cite_spans": [
                {
                    "start": 240,
                    "end": 241,
                    "text": "8",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "324"
        },
        {
            "text": "Bcl-2 plays a key role in the death or survival of virus-infected cells. Moreover, some studies indicated that Bcl-2 determines the establishment of persistence. A persistent strain of Sindbis virus induces upregulation of Bcl-2, whereas a virulent strain induces an increase in Bax. On the other hand, the present study of one cell line persistently infected with SARS-CoV suggested accumulation of a form of Bcl-xL that was different in size of from that in parental Vero E6 cells, but significant activation of Bcl-2 was not observed. Bcl-xL is known to be phosphorylated at one site, Ser62, by treatment with taxol and 2-ME. Further studies are necessary to confirm the dephosphorylation of Bcl-xL at the site in the persistently infected cells.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Difference in Migration of Bcl-x L in a Persistently Infected Cell Line"
        },
        {
            "text": "Here, we reported a possible mechanism of the establishment of persistent SARS-CoV infection in Vero E6 cells (Fig. 2) . Although the majority of cells died due to apoptosis after SARS-CoV infection, activation of JNK and PI3K/Akt signaling pathways aided a minor population of cells with the potential to support persistent infection to establish persistence. One strategy for cell survival on viral infection is activation of Bcl-xL.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 110,
                    "end": 118,
                    "text": "(Fig. 2)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "Difference in Migration of Bcl-x L in a Persistently Infected Cell Line"
        },
        {
            "text": "We thank Ms. M. Ogata (National Institute of Infectious Diseases, Japan) for her assistance. This work was supported in part by a grant-in-aid from the Ministry of Health, Labor, and Welfare of Japan and the Japan Health Science Foundation, Tokyo, Japan. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ACKNOWLEDGMENTS"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Characterization of a novel coronavirus associated with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "A"
                    ],
                    "last": "Rota",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "S"
                    ],
                    "last": "Oberste",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "S"
                    ],
                    "last": "Monroe",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": "1394--1399",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The genome sequence of the SARS-associated coronavirus",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Marra",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "J"
                    ],
                    "last": "Jones",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "R"
                    ],
                    "last": "Astell",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Science",
            "volume": "300",
            "issn": "",
            "pages": "1399--1404",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Persistent infection of SARS coronavirus in colonic cells in vitro",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "K"
                    ],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "F"
                    ],
                    "last": "To",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "W"
                    ],
                    "last": "Lo",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "L"
                    ],
                    "last": "Cheung",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "W"
                    ],
                    "last": "Au",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "H"
                    ],
                    "last": "Tong",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Tam",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "J"
                    ],
                    "last": "Sung",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "K"
                    ],
                    "last": "Ng",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J. Med. Virol",
            "volume": "74",
            "issn": "",
            "pages": "187--192",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Differential regulation of Bcl-2 and Bax expression in cells infected with virulent and nonvirulent strains of sindbis virus",
            "authors": [],
            "year": null,
            "venue": "Virology",
            "volume": "276",
            "issn": "",
            "pages": "238--242",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "sensitivity of taxol-or 2-methoxyestradiol-induced apoptosis",
            "authors": [],
            "year": null,
            "venue": "J. Biol. Chem",
            "volume": "58",
            "issn": "",
            "pages": "3667--3673",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "apoptosis in bcl-2-expressing cells: role of a single amino acid change in the E2 glycoprotein",
            "authors": [],
            "year": null,
            "venue": "Proc. Natl. Acad. Sci",
            "volume": "91",
            "issn": "",
            "pages": "5202--5206",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "La Crosse virus infection and role of neuronal differentiation and human bcl-2 expression in its prevention",
            "authors": [],
            "year": null,
            "venue": "J. Virol",
            "volume": "70",
            "issn": "",
            "pages": "450--454",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Tyrosine dephosphorylation of STAT3 in SARS coronavirus-infected Vero E6 cells, FEBS Lett. pathways are required for establishing persistent SARS-CoV-infection in Vero E6 cells",
            "authors": [],
            "year": null,
            "venue": "SARS coronavirus-infected cells",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Angiotensin-converting enzyme 2 is a functional receptor",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Moore",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Vasilieva",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Antiapoptic but not antiviral function of human bcl-2 assists establishment of Japanese 10",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Mizutani",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Fukushi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Saijo",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Kurane",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Morikawa",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Akt Signaling Appel",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Katzoff",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ben-Moshe",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Kazimirsky",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Kobiler",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Lustig",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Brodie",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "K"
                    ],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Translocation of SAPK/JNK to mitochondria and 13",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Kharbanda",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Saxena",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Yoshida",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Induction of apoptosis by Acta",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pekosz",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Phillips",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Pleasure",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Merry",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Gonzalez-Scarano",
                    "suffix": ""
                }
            ],
            "year": 1996,
            "venue": "",
            "volume": "1741",
            "issn": "",
            "pages": "4--10",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "and encephalitis virus persistence in cultured cells",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "L"
                    ],
                    "last": "Liao",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "L"
                    ],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "C"
                    ],
                    "last": "Shen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "Y"
                    ],
                    "last": "Shen",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "L"
                    ],
                    "last": "Su",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "L"
                    ],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "H"
                    ],
                    "last": "Ma",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [
                        "C"
                    ],
                    "last": "Sun",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "P"
                    ],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "J Virol",
            "volume": "72",
            "issn": "",
            "pages": "9844--9854",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Activation of Bcl-xL in persistently infected cells.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Model of establishing persistent infection of SARS-CoV.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}