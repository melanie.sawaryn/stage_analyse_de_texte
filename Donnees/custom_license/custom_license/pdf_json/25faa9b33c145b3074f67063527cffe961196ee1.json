{
    "paper_id": "25faa9b33c145b3074f67063527cffe961196ee1",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Danilo Buonsenso and colleagues 1 described in their Correspondence how lung ultrasound could replace stetho scopes in the ongoing coronavirus disease 2019 (COVID19) pandemic, which could possibly reduce the risk of exposure. 1 Indeed, pointofcare ultrasound (POCUS) has an exemplary role in many specialties, especially emergency and critical care medicine. Nevertheless, this technology is still relatively new so we would like to highlight the pearls and pitfalls for POCUS users to use this tool to its full potential and ensure optimal patient care and safety. There should be dedicated mach ines for patients with COVID19. Nowadays, many handheld models are affordable even for lowresource regions and some can cost only one twentieth of an ordinary midrange or highend machine. In the intensive care unit (ICU), if the department can afford the expense, it is optimal to assign one machine to each ICU patient, similar to common practice of avoiding cross contamination by allocating a single stethoscope at the bedside for each individual patient. Another measure to minimise the possibility of cross contamination is to use individually packaged, singleuse ultrasound gel.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "POCUS in COVID-19: pearls and pitfalls"
        },
        {
            "text": "There are several features clinicians should look for when selecting the ideal pocket machine. First, wireless models are better than those with a cable because they are easier to manipulate (even if put in a plastic probe cover) and remove the risk of the cable contacting the surrounding environment. Second, machines that can be wirelessly charged are preferable because they obviate the need of removing the plastic cover unnecessarily for recharging. Third, the devices that possess multiple probe functionalities in a single unit are desirable because they can be used to do various clinical tasks without additional tools-eg, for patients with COVID19 who require central venous catheter insertion as part of shock management, bed side cardiac ultrasound to assess ventricular function due to the risk of myocarditis, 2 or have acute pulmonary heart disease from their demand of higher positive endexpiratory pressure.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "POCUS in COVID-19: pearls and pitfalls"
        },
        {
            "text": "Lung ultrasound has high sensitivity for detecting pleural thickening, sub pleural consolidation, and groundglass opacification equivalent in CT; 3 however, one pitfall is that there are occasions in which other imaging modalities are needed. For instance, lung ultrasound might not be able to detect a centrally located consolidation from bacterial superinfection. Furthermore, like many POCUS applications, lung ultrasound is often unable to discern the chronicity of a lesion, limiting its power of early COVID19 diagnosis in the population with preexisting pulmonary con ditions. Patients with underlying asthma can have respiratory wheeze, which is not visible by lung ultrasound. Apart from using a stethoscope, the degree of airway obstruction in a mechanically ventilated patient could be assessed by various parameters, such as expiratory flow and waveform capnography.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "POCUS in COVID-19: pearls and pitfalls"
        },
        {
            "text": "Proper documentation is one aspect POCUS users should be aware of. The POCUS images should be stored for later review for as long as possible. Smartphones or tablets provided by the institution-instead of personal ones-should be used to connect to the handheld POCUS device and for image storage. This practice will protect confidentiality and minimise the risk of the clinician's belongings being accidentally contaminated.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "POCUS in COVID-19: pearls and pitfalls"
        },
        {
            "text": "For lung ultrasound, it is particularly important for the department to adopt a scanning protocol that every clinician agrees on, because there are different options available, including 8zone, 12zone, and 28zone protocols, each applying different terminologies. 4 The operator should also annotate the cine loops accordingly otherwise it would be difficult for reviewers to determine the exact location being scanned.",
            "cite_spans": [
                {
                    "start": 263,
                    "end": 264,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "POCUS in COVID-19: pearls and pitfalls"
        },
        {
            "text": "We embrace the extra precision and safety new technology brings but its limitations and optimisation must be understood to get the most benefits out of it without compromising standard of care.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "POCUS in COVID-19: pearls and pitfalls"
        },
        {
            "text": "We declare no competing interests.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "POCUS in COVID-19: pearls and pitfalls"
        },
        {
            "text": "Intensive Care Unit, North District Hospital, Sheung Shui, Hong Kong Special Administrative Region, China",
            "cite_spans": [],
            "ref_spans": [],
            "section": "jonathanchunheicheung@gmail.com"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "COVID19 outbreak: less stethoscope, more ultrasound",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Buonsenso",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Pata",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Chiaretti",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet Respir Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/S22132600(20"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Cardiovascular considerations for patients, health care workers, and health systems during the coronavirus disease 2019 (COVID19) pandemic",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Driggin",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "V"
                    ],
                    "last": "Madhavan",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Bikdeli",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Am Coll Cardiol",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Is there a role for lung ultrasound during the COVID19 pandemic?",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Soldati",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Smargiassi",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Inchingolo",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Ultrasound Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "International evidencebased recommendations for pointofcares lung ultrasound",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Volpicelli",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Elbarbary",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Blaivas",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Intensive Care Med",
            "volume": "38",
            "issn": "",
            "pages": "577--91",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Correspondence www.thelancet.com/respiratory Published online April 7, 2020 https://doi.org/10.1016/S2213-2600(20)30166-1 1",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}