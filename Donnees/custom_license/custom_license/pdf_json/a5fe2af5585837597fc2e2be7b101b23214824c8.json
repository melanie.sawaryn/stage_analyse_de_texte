{
    "paper_id": "a5fe2af5585837597fc2e2be7b101b23214824c8",
    "metadata": {
        "title": "Correspondence",
        "authors": [
            {
                "first": "Wei",
                "middle": [],
                "last": "Cui",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Peking Union Medical College Hospital",
                    "location": {}
                },
                "email": "mawx@csc.pumch.ac.cn."
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Severe Acute Respiratory Syndrome?",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "Sir-Cui et al. [1] recently described pronounced lymphopenia and low counts of CD4 + cells, CD8 + cells, and B cells in patients with severe acute respiratory syndrome (SARS). On the basis of these low cell counts, Cui et al. [1] suggested that SARS coronavirus (SARS-CoV) might damage lymphocytes and concluded that the immune system was impaired during the course of SARS. However, Cui et al. [1] did not provide direct evidence to support their hypothesis.",
            "cite_spans": [
                {
                    "start": 15,
                    "end": 18,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 226,
                    "end": 229,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 395,
                    "end": 398,
                    "text": "[1]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "Low counts of both CD4 + and CD8 + cells in the peripheral circulation do not always indicate that the immune system is impaired: redistribution of lymphocytes among peripheral and secondary lymphoid organs and migration of these cells to inflamed tissues caused by infections may also result in lymphopenia. Postthymus naive T cells do not reside in any single lymphoid organ but, rather, circulate continuously between blood and lymph through a specialized T cell zone in secondary lymphoid tissues, which forms part of the \"recirculating lymphocyte pool.\"",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "Neither splenectomy nor ablation of bone marrow by radioactive isotope therapy reduces the number of lymphocytes, and thymectomy in adult mice causes only a very slow decrease in the size of the recirculating lymphocyte pool. These findings suggest that splenic atrophy and pathological changes in lymph nodes observed in patients with SARS [2] [3] [4] were not the causes of lymphopenia. Although SARS-CoV RNA was detected in PBMCs obtained from patients with SARS [5] , no SARS-CoV was recovered from splenic, lymphatic, and bone marrow specimens obtained from patients with fatal cases [3] . This finding indicated that the pathological presentations in lymphoid organs were unlikely to have been directly caused by the virus.",
            "cite_spans": [
                {
                    "start": 341,
                    "end": 344,
                    "text": "[2]",
                    "ref_id": null
                },
                {
                    "start": 345,
                    "end": 348,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 349,
                    "end": 352,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 466,
                    "end": 469,
                    "text": "[5]",
                    "ref_id": null
                },
                {
                    "start": 589,
                    "end": 592,
                    "text": "[3]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "If SARS-CoV was directly detrimental to lymphocytes, the damage should start at the beginning of infection and continue through the incubation period, resulting in a decreased lymphocyte count after the onset of SARS. However, although most patients had mild to moderate decreased lymphocyte counts during the early phase of illness [6, 7] , many patients had normal lymphocyte counts at the onset of symptoms, and some even had increased CD4 + and CD8 + cell counts during the first week of illness [3] .",
            "cite_spans": [
                {
                    "start": 333,
                    "end": 336,
                    "text": "[6,",
                    "ref_id": null
                },
                {
                    "start": 337,
                    "end": 339,
                    "text": "7]",
                    "ref_id": null
                },
                {
                    "start": 500,
                    "end": 503,
                    "text": "[3]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "Clinically, there is no evidence that the onset of SARS is associated with impairment of the immune system. SARS is characterized by respiratory symptoms and signs correlated with pulmonary lesions caused by SARS-CoV infection. At the time of writing, no report has shown that the initial manifestation of SARS is caused by immunosuppression (e.g., AIDS), and only a small portion of the patients have had secondary bacterial infections-which were easily treatable-during the late course of illness, despite receipt of corticosteroid treatment [8] . The effectiveness of corticosteroid therapy in stopping the progression of pulmonary lesions in patients with SARS also suggests that SARS is not associated with the impairment of the immune system but is associated with immunopathological damage [9] .",
            "cite_spans": [
                {
                    "start": 544,
                    "end": 547,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 797,
                    "end": 800,
                    "text": "[9]",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "Cui et al. [1] also found that 76% of patients with SARS had a low B cell count. However, the immune function of B cells in patients with SARS appeared not to have been impaired, because specific anti-SARS-CoV was detected as early as day 10 after the onset of illness [8] , 93%-100% of the patients had seroconversion to anti-SARS-CoV after week 3 [8, 10] , and the level of specific IgG remained high for at least 3 months [10] .",
            "cite_spans": [
                {
                    "start": 11,
                    "end": 14,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 269,
                    "end": 272,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 349,
                    "end": 352,
                    "text": "[8,",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 353,
                    "end": 356,
                    "text": "10]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 425,
                    "end": 429,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "In conclusion, although lymphopenia does occur in patients with SARS, no evidence supports the position that SARS-CoV damages lymphocytes. Humoral immune response to SARS-CoV is not damaged in patients with SARS. Before in vitro and in vivo cellular immune responses are investigated, it would be cautious to conclude that the immune system is impaired in patients with SARS. [3] demonstrated that, in patients with SARS, lymphopenia was observed in various lymphoid organs, there were scanty numbers of lymphocytes in exudates from inflamed lungs, and lymphocytosis was not found in any organs. In another article [4] cited by Zhou and Chen [1] , hemorrhagic necrosis and attenuation of lymphocytes were also evident in lymph nodes and spleen specimens obtained from patients with SARS, which is direct evidence of damaged immune organs and tissues in patients with SARS. On the basis of these findings, Lang et al. [4] concluded that the severe damage to the pulmonary and immunological systems was responsible for the clinical features of SARS and might result in patient death [4] . Ding et al. [5] reached the same conclusion. They note that \"SARS is a systemic disease that injures many organs. The lungs, immune organs, and systemic small vessels are the main targets of virus attack so that extensive consolidation of the lung, diffuse alveolar damage with hy-aline membrane formation, respiratory distress, and decreased immune function are the main causes of death\" [5, p. 282 ].",
            "cite_spans": [
                {
                    "start": 376,
                    "end": 379,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 615,
                    "end": 618,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 642,
                    "end": 645,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 917,
                    "end": 920,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 1081,
                    "end": 1084,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 1099,
                    "end": 1102,
                    "text": "[5]",
                    "ref_id": null
                },
                {
                    "start": 1476,
                    "end": 1486,
                    "text": "[5, p. 282",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Is the Immune System Impaired in Patients with"
        },
        {
            "text": "At the time of writing, there is still no evidence to support the view of Zhou and Chen [1] that lymphopenia is due to the redistribution and migration of lymphocytes. With respect to the expression of lymphocytes and their subsets, Li et al. [6] arrived at a conclusion identical to the one we reported [7] . Therefore, we believe that the hypothesis that SARS-CoV may damage the immune system might be supported by recently published studies.",
            "cite_spans": [
                {
                    "start": 88,
                    "end": 91,
                    "text": "[1]",
                    "ref_id": null
                },
                {
                    "start": 243,
                    "end": 246,
                    "text": "[6]",
                    "ref_id": null
                },
                {
                    "start": 304,
                    "end": 307,
                    "text": "[7]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Yi-Hua Zhou and Zhaochun Chen"
        },
        {
            "text": "We believe that Zhou and Chen [1] need more data to support their conclusion that the immune function of B cells in our patients appeared not to be impaired because specific anti-SARS-CoV could be detected as early as 10 days after the onset of illness. Although anti-SARS-CoV was observed in patients with SARS, it was not proper to conclude that B cells in these patients were unimpaired, because the extent of impairment might be different and because the function of B cells was not limited to the production of antibody. The fact that 76% of our patients with SARS had low B cell counts could imply that B cells were damaged by SARS-CoV, although the damage was not too severe to affect the production of antibody.",
            "cite_spans": [
                {
                    "start": 30,
                    "end": 33,
                    "text": "[1]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Yi-Hua Zhou and Zhaochun Chen"
        },
        {
            "text": "There are still too many unanswered questions about SARS. Although the incidence of SARS has subsided in Beijing, more investigations are underway. We welcome different opinions and suggestions as we continue our research.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Yi-Hua Zhou and Zhaochun Chen"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Expression of lymphocytes and lymphocyte subsets in patients with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Cui",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "Y"
                    ],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "P"
                    ],
                    "last": "Ni",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Clin Infect Dis",
            "volume": "37",
            "issn": "",
            "pages": "857--866",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The clinical pathology of severe acute respiratory syndrome (SARS): a report from China",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Ding",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Shen",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Pathol",
            "volume": "200",
            "issn": "",
            "pages": "282--291",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Haematological manifestations in patients with severe acute respiratory syndrome: retrospective analysis",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "F"
                    ],
                    "last": "To",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "BMJ",
            "volume": "326",
            "issn": "",
            "pages": "1358--62",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Pathological study on severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Lang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Chin Med J (Engl)",
            "volume": "116",
            "issn": "",
            "pages": "976--80",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "SARS-coronavirus replicates in mononuclear cells of peripheral blood (PBMCs) from SARS patients",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wo",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Shao",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Clin Virol",
            "volume": "28",
            "issn": "",
            "pages": "239--283",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Severe acute respiratory syndrome (SARS) in Singapore: clinical features of index patient and initial contacts",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "Y"
                    ],
                    "last": "Hsu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Green",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Emerg Infect Dis",
            "volume": "9",
            "issn": "",
            "pages": "718--738",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Clinical features and short-term outcomes of 144 patients with SARS in the greater Toronto area",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Booth",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "M"
                    ],
                    "last": "Matukas",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "A"
                    ],
                    "last": "Tomlinson",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "JAMA",
            "volume": "289",
            "issn": "",
            "pages": "2801--2810",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Clinical progression and viral load in a community outbreak of coronavirus-associated SARS pneumonia: a prospective study",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Peiris",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "C"
                    ],
                    "last": "Cheng",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1767--72",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Development of a standard treatment protocol for severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "K"
                    ],
                    "last": "So",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "C"
                    ],
                    "last": "Lau",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "Y"
                    ],
                    "last": "Yam",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1615--1622",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Profile of specific antibodies to the SARS-associated coronavirus",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "349",
            "issn": "",
            "pages": "508--517",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Is the immune system impaired in patients with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "Y-H",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Clin Infect Dis",
            "volume": "38",
            "issn": "",
            "pages": "921--923",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "SARS-coronavirus replicates in mononuclear cells of peripheral blood (PBMCs) from SARS patients",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wo",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Shao",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Clin Virol",
            "volume": "28",
            "issn": "",
            "pages": "239--283",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Haematological manifestations in patients with severe acute respiratory syndrome: retrospective analysis",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "F"
                    ],
                    "last": "To",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "BMJ",
            "volume": "326",
            "issn": "",
            "pages": "1358--62",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Pathological study on severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Lang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Chin Med J (Engl)",
            "volume": "116",
            "issn": "",
            "pages": "976--80",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "The clinical pathology of severe acute respiratory syndrome (SARS): a report from China",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Ding",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Shen",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Pathol",
            "volume": "200",
            "issn": "",
            "pages": "282--291",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "Rapid loss of both CD4 + and CD8 + T lymphocyte subsets during the acute phase of severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Qiu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Han",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Chin Med J (Engl)",
            "volume": "116",
            "issn": "",
            "pages": "985--992",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Expression of lymphocytes and lymphocyte subsets in patients with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Cui",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Fan",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "Y"
                    ],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "P"
                    ],
                    "last": "Ni",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Clin Infect Dis",
            "volume": "37",
            "issn": "",
            "pages": "857--866",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "Laboratory of Infectious Diseases, National Institute of Allergy and Infectious Diseases, NationalInstitutes of Health, Bethesda, Maryland",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "Beijing, People's Republic of China",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Department of Clinical Laboratory, Peking Union Medical College Hospital,"
        }
    ]
}