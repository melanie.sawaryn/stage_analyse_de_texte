{
    "paper_id": "83027092a6c82925ccbebe1ee5eabee4f573798f",
    "metadata": {
        "title": "Detection of Respiratory Tract Pathogens with Molecular Biology Methods",
        "authors": [
            {
                "first": "A",
                "middle": [],
                "last": "Wozniak-Kosek",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J",
                "middle": [],
                "last": "Kosek",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "B",
                "middle": [],
                "last": "Zielnik-Jurkiewicz",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "This paper describes the use in routine diagnosis of virological kit, which was designed to identify the 15 most common respiratory viruses in clinical specimens of nasopharyngeal aspirates, swabs, and bronchoalveolar lavage. It is a one-step multiplex RT-PCR system for the detection of influenza virus type A and type B, human respiratory syncytial virus type A, B; human adenovirus, human metapneumovirus, human coronaviruses 229E/NL63 and OC43, human parainfluenza type 1, 2, 3, human rhinovirus type A, B, human enterovirus, and bocavirus 1, 2, 3, 4. The article presents research conducted on the basis of swabs collected from patients who came to the Ear, Nose, and Throat Emergency Care Unit at the Department of Otolaryngology, Military Medical Institute in Warsaw, in February 2013. Due to the nature of work in an laryngological emergency ward, the material was collected only from those patients who reported problems associated with rhinitis or any dysfunction of the upper respiratory tract. The study shows that patients who came to seek laryngological assistance were usually infected with viruses having affinity for the airway epithelium.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "The laboratory diagnostics of virological infections in the respiratory system is currently based primarily on modern methods of molecular biology. Serological methods, and in the case of influenza virus infection also a viral culture in chick embryo, constitute a smaller value for diagnostics, because of the time needed to obtain a result, although they are still considered to be the gold standard in virological studies of the respiratory system. The respiratory tract infection is divided into upper and lower respiratory tract infection. However, the upper respiratory tract infections constitute a preliminary stage to the infection of the bronchi and lung parenchyma. Chronic heart and lung diseases, diabetes, cancer, renal failure, hematopoietic system diseases, and other chronic diseases occur more frequently as complications in the case of patients with severe lower respiratory tract infections involving the respiratory viruses (Kim et al. 2013 ). In such a case, especially when the patient with severe symptoms of respiratory tract infection and associated immune disorders or the comorbidities comes to the laryngological ER, the virological laboratory diagnostics allowing for the identification of the etiological agent is extremely helpful.",
            "cite_spans": [
                {
                    "start": 945,
                    "end": 961,
                    "text": "(Kim et al. 2013",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Respiratory tract infections are the most frequent cause of visits to the doctor. Pneumonia is still a threat to life and health, especially in children and elderly patients affected by comorbidities. The pathogens responsible for respiratory infections include viruses, bacteria, and in exceptional cases, fungi and parasites. Table 1 shows the most common viruses that are the cause of respiratory tract infections.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 328,
                    "end": 335,
                    "text": "Table 1",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Selected Pathogens Responsible for Respiratory Tract Infections"
        },
        {
            "text": "The material consisted of swabs from the nose and throat collected from patients during the ear, nose, and throat (ENT) emergency care in the period January- February ",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 158,
                    "end": 166,
                    "text": "February",
                    "ref_id": null
                }
            ],
            "section": "Methods"
        },
        {
            "text": "The RV15 One",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Carrying Out a PCR Reaction"
        },
        {
            "text": "Step ACE Detection Kit (Seegene, Seoul, South Korea) is a qualitative in vitro test for the detection of 15 types of respiratory viruses in the aspirates from nasopharynx and nasopharyngeal swabs or samples from bronchopulmonary tree lavage in patients with clinical symptoms. The kit contains a set of reagents due to which it is possible to perform a Multiplex PCR. Simplification of the two-step method results in an increased repeatability of analysis and efficiency of reaction. The kit for determining 15 respiratory viruses in a sample of a clinical material is based on a process of reverse transcription (RT) and amplification of the target DNA by PCR using Dual Priming Oligonucleotide (DPO) primers, which provides freedom in a primer design and PCR optimisation and maximizes PCR specificity and sensitivity by fundamentally blocking non-specific priming. The DPO-based multiplex assay that permits the simultaneous amplification of target sequences of 15 viruses, presented after the amplification reaction, uses e.g., electrophoresis in an agarose gel preceded by nucleic acid isolation (Kim et al. 2013) .",
            "cite_spans": [
                {
                    "start": 1101,
                    "end": 1118,
                    "text": "(Kim et al. 2013)",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Carrying Out a PCR Reaction"
        },
        {
            "text": "The kit consists of three panels/sets A, B, C, each containing reagents and primers for the detection of five respiratory viruses (details are given in Table 2 ) and includes two internal controls: PCR control and the control of the whole process. PCR controls were added to the A and B panel in order to identify the substances contained in the tested samples and to determine whether they might interfere with PCR amplification. Panel C has as an internal control a human RNase P, which allows the inspection of the whole process from the extraction of nucleic acids to RT-PCR. Additionally, 8-methoxypsoralen was used, which suppresses the activity of a DNA template. The 8-methoxypsoralen (8-MOP) reagent binds to the double-stranded structure of nucleic acids, forming covalent bonds between the strands upon activation by ultraviolet light.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 152,
                    "end": 159,
                    "text": "Table 2",
                    "ref_id": "TABREF3"
                }
            ],
            "section": "Carrying Out a PCR Reaction"
        },
        {
            "text": "When one-stage PCR was carried out in a thermocycler, the amplification products were placed onto a previously prepared 1.5 % agarose gel. The preparation of the gel consisted of dissolving the agarose in TAE (Tris-acetate-EDTA) buffer. After partial cooling, ethidium bromide was added at a concentration of 10 mg/ ml. The gel was solidified using special well combs and mixtures, after amplification, were applied to the resulting holes in the agarose. The mixture also contained a loading dye. This procedure makes it possible to control the electrophoretic separation process using the kit for electrophoresis Wide Mini-SubCell GT/PowerPac Basic System (Bio-Rad Laboratories, Hercules, CA). Thus, the electrophoresis was carried out by controlling the migration of PCR products in a gel by placing a loading dye -bromphenol blue. To get the maximum separation of DNA fragments, the electric field did not exceed 5 V/cm of the gel. After the electrophoresis, the gel was analyzed under UV light in the GelDoc EQ system, and the resulting image was documented using Quantity One software (Bio-Rad Laboratories, Hercules, CA).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Detection of PCR Products by Electrophoresis in Agarose Gel"
        },
        {
            "text": "The presence of 15 respiratory viruses in the RNA samples tested was confirmed by the amplification products of the proper base pairs, as shown in Table 2 .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 147,
                    "end": 154,
                    "text": "Table 2",
                    "ref_id": "TABREF3"
                }
            ],
            "section": "Eligibility Criteria of the Test"
        },
        {
            "text": "We Figure 1 shows a breakdown of viruses. Virological laboratory methods can be a valuable addition to medical clinical diagnostics conducted in patients with respiratory tract infections and avoid the use of unnecessary antibiotic therapy . The commissioning of a microbiological examination must be supported by an appropriate medical decision on whether and how the result will be used. The selection of, or resignation from, the available laboratory testing method should be based on the analysis of the workload, cost, waiting time for the outcome, and the expected clinical benefits for patients (Wozniak-Kosek and Bydak 2013) .",
            "cite_spans": [
                {
                    "start": 602,
                    "end": 632,
                    "text": "(Wozniak-Kosek and Bydak 2013)",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 3,
                    "end": 11,
                    "text": "Figure 1",
                    "ref_id": null
                }
            ],
            "section": "Results and Discussion"
        },
        {
            "text": "The knowledge of available laboratory techniques both virological and bacteriological, and their advantages and limitations is important for decision-making . In cases requiring a rapid response on the part of the laboratory staff, the collaboration with a doctor is crucial to select appropriate tests and to interpret results. The difficulty in the treatment of infections caused by respiratory viruses is related to the lack of effective drugs. The only exception to this end is the treatment of influenza with the neuraminidase inhibitors oseltamivir and zanamivir which are highly effective (Demkow 2008 ; Van-Tam and Sellwood 2013) .",
            "cite_spans": [
                {
                    "start": 596,
                    "end": 608,
                    "text": "(Demkow 2008",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 611,
                    "end": 637,
                    "text": "Van-Tam and Sellwood 2013)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Results and Discussion"
        },
        {
            "text": "The authors declare no conflicts of interest in relation to this article.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflicts of Interest"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Influenza, pandemic flu: myth or a real threat? Rhythm",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "1--492",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Influenza diagnosis and vaccination in Poland",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wozniak-Kosek",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Nitsch-Osuch",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Respir Physiol Neurobiol",
            "volume": "187",
            "issn": "",
            "pages": "88--93",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "The immunological and molecular diagnosis of respiratory tract infections",
            "authors": [
                {
                    "first": "U",
                    "middle": [],
                    "last": "Demkow",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Med News",
            "volume": "77",
            "issn": "",
            "pages": "239--242",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Comparison of anyplex II RV16 with the xTAG respiratory viral panel and Seeplex RV15 for detection of respiratory viruses",
            "authors": [
                {
                    "first": "H-K",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "S-H",
                    "middle": [],
                    "last": "Oh",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "A"
                    ],
                    "last": "Yun",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Sung",
                    "suffix": ""
                },
                {
                    "first": "M-N",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "J Clin Res",
            "volume": "4",
            "issn": "",
            "pages": "1137--1141",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Accuracy of rapid influenza detection test in diagnosis of influenza A and B viruses in children less than 59 months old",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Nitsch-Osuch",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wozniak-Kosek",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Korzeniewski",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Zycinska",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Wardyn",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Adv Exp Med Biol",
            "volume": "788",
            "issn": "",
            "pages": "71--76",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Pandemic influenza",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Van-Tam",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Sellwood",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "1--234",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "WHO Global Influenza Surveillance Network (2011) Manual for the laboratory diagnosis and virological surveillance of influenza",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "1--139",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Virological monitoring of influenza activity and influenza-like illness in the epidemic season 2011-2012 in Poland",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wozniak-Kosek",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Brydak",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Adv Exp Med Biol",
            "volume": "788",
            "issn": "",
            "pages": "77--82",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Table 3 Genetic material of respiratory viruses in nasal and throat swabs taken from hospital patients from Mazovian Voivodeship in Poland Genetic material of viruses Influenza virus type A; RSV A; PIV 3, PIV 2 Influenza virus type A; HRV Influenza virus type A; RSV A, HEV Influenza virus type A; RSV A Influenza virus type A; PIV-3, HEV Influenza virus type A; HEV Influenza virus type B; MPV Coronavirus OC 43",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF1": {
            "text": "Basic clinical and diagnostic methods in selected infections caused by respiratory viruses(Brydak 2008)",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "patients, there were mixed infections with respiratory viruses in 15 cases (62.5 %); the most common co-infection combinations are presented inTable 3. Of the remaining nine samples, only viral genetic materials were identified of: influenza virus type A in 7 (29.1 %) cases, and Coronavirus OC43 in one and HEV virus in one case each.",
            "latex": null,
            "type": "table"
        },
        "TABREF3": {
            "text": "Amplicon information RV 15 One Step ACE Detection (A set)",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": " Fig. 1 Breakdown of respiratory viruses identified in throat and nose swabs",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 1,
                    "end": 7,
                    "text": "Fig. 1",
                    "ref_id": null
                }
            ],
            "section": "annex"
        }
    ]
}