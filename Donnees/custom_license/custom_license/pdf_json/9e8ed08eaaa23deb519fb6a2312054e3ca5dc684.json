{
    "paper_id": "9e8ed08eaaa23deb519fb6a2312054e3ca5dc684",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "Roberto",
                "middle": [
                    "Cazzolla"
                ],
                "last": "Gatti",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "It was New Year's Eve when Chinese authorities alerted WHO that several cases of an unusual pneumonia appeared in Wuhan (Huang et al. 2020 ). The COVID-19 was still unknown at that time but, in the first three months of this year, its outbreak has already infected almost one million people, killing (directly or indirectly) tens of thousands of them, so far (WHO 2020). The panic related to the pandemic distribution of the virus shut down whole regions (in China, Iran, the United States, etc.) and even entire countries (Italy, Spain, Austria, etc.) . Although it is not the worst microscopic killer humanity has ever known (for instance, think about the casualties due to the Black Death and the HIV; Wainberg et al. 2008; Haensch et al. 2010 ), this coronavirus is already changing our state of mind and impacting on our lifestyle, at a global scale.",
            "cite_spans": [
                {
                    "start": 120,
                    "end": 138,
                    "text": "(Huang et al. 2020",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 523,
                    "end": 552,
                    "text": "(Italy, Spain, Austria, etc.)",
                    "ref_id": null
                },
                {
                    "start": 705,
                    "end": 726,
                    "text": "Wainberg et al. 2008;",
                    "ref_id": "BIBREF20"
                },
                {
                    "start": 727,
                    "end": 746,
                    "text": "Haensch et al. 2010",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Title: Coronavirus outbreak is a symptom of Gaia's sickness"
        },
        {
            "text": "But its expanding virulence should not be the scariest issue related to this pathogen. COVID-19 is evidently a symptom of how sick of us is Gaia, our planet (Lovelock 2007) . No need to invoke here even the existence of contended superorganisms, neither to argue in favour of teleological revenge (Boston et al. 2004) . What I fear is that the systematic and long-term impacts we are having on our Earth is, and will continue to, challenging our modern lifestyle, just as dangerous prolonged habits impair a body's health (Cazzolla Gatti 2018).",
            "cite_spans": [
                {
                    "start": 157,
                    "end": 172,
                    "text": "(Lovelock 2007)",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 297,
                    "end": 317,
                    "text": "(Boston et al. 2004)",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Title: Coronavirus outbreak is a symptom of Gaia's sickness"
        },
        {
            "text": "Our massive emission of carbon stored into the ground during millions of years in just a few centuries (Steffen et al. 2007 ), our deep degradation of forest (Betts et al. 2017) and marine (Worm et al. 2006) ecosystems that threatened their integrity and resilience, our increasing urbanization (Seto et al. 2012 ) and pollution that contaminates even the most remote areas of this planet (Cozar et al. 2017) , and our immense pressure on other species that is leading the world's biodiversity towards the sixth mass extinction (Barnosky et al. 2011; Ceballo et al. 2017) , cannot do anything else than harm the global system and trigger dangerous feedbacks (simply, negative adjusting reactions) on our species.",
            "cite_spans": [
                {
                    "start": 103,
                    "end": 123,
                    "text": "(Steffen et al. 2007",
                    "ref_id": "BIBREF19"
                },
                {
                    "start": 158,
                    "end": 177,
                    "text": "(Betts et al. 2017)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 189,
                    "end": 207,
                    "text": "(Worm et al. 2006)",
                    "ref_id": "BIBREF21"
                },
                {
                    "start": 295,
                    "end": 312,
                    "text": "(Seto et al. 2012",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 389,
                    "end": 408,
                    "text": "(Cozar et al. 2017)",
                    "ref_id": null
                },
                {
                    "start": 528,
                    "end": 550,
                    "text": "(Barnosky et al. 2011;",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 551,
                    "end": 571,
                    "text": "Ceballo et al. 2017)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Title: Coronavirus outbreak is a symptom of Gaia's sickness"
        },
        {
            "text": "In a few words, we are too many, travel too often, and consume too much on our planet. These are the conditions when, in ecological systems, population growth is constrained by the environmental carrying capacity (Cohen 1995) and threatened by infective diseases (Jones et al. 2008) , which spread easier and faster in overpopulated areas.",
            "cite_spans": [
                {
                    "start": 213,
                    "end": 225,
                    "text": "(Cohen 1995)",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 263,
                    "end": 282,
                    "text": "(Jones et al. 2008)",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "Title: Coronavirus outbreak is a symptom of Gaia's sickness"
        },
        {
            "text": "It is not a coincidence that the likely origin of this coronavirus is in the human-animal relations that occurred in China (Poon and Peiris 2020) . Although it is not clear yet if either the interaction with seafood, bats or bushmeat played a role in the emergence of this virus, most agree that it crossed from animals to human beings. As for many other infective diseases such as HIV, which was likely transmitted by chimp meat consumption (Hahan et al. 2000) , malaria and dengue fever, which are very sensitive to deforestation and climate change (Yasuoka and Levins 2007; Colon-Gonzalex et al. 2013) , meningitis, which can spread out after prolonged drought (Molesworth 2003) , etc., the overexploitation of habitats and the huge impact we have on wildlife facilitates the sudden appearance of new dangerous sicknesses.",
            "cite_spans": [
                {
                    "start": 123,
                    "end": 145,
                    "text": "(Poon and Peiris 2020)",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 442,
                    "end": 461,
                    "text": "(Hahan et al. 2000)",
                    "ref_id": null
                },
                {
                    "start": 551,
                    "end": 576,
                    "text": "(Yasuoka and Levins 2007;",
                    "ref_id": "BIBREF22"
                },
                {
                    "start": 577,
                    "end": 604,
                    "text": "Colon-Gonzalex et al. 2013)",
                    "ref_id": null
                },
                {
                    "start": 664,
                    "end": 681,
                    "text": "(Molesworth 2003)",
                    "ref_id": "BIBREF14"
                }
            ],
            "ref_spans": [],
            "section": "Title: Coronavirus outbreak is a symptom of Gaia's sickness"
        },
        {
            "text": "All this, associated with the unstoppable human population's growth and fast-moving dispersal of its individuals, creates the perfect conditions for pandemics. As I said: the planet is sick of us and makes us sick; it's a natural negative feedback. As usual, we try to take care of the symptoms once they appear whilst we had time for, but ignored, a more efficient prevention. I am afraid that we can put all our efforts to stop COVID-19 during the next months but, if we will not immediately change our national policies and personal lifestyles, other unpleasant surprises will wait behind Gaia's door to humanity.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Title: Coronavirus outbreak is a symptom of Gaia's sickness"
        },
        {
            "text": "Our responsibility is clear (Fig. 1 ). In the last three months of the virus outbreak, China has reduced its greenhouse gas (GHG) emissions by about 25%, which means more than 200 million tons of carbon dioxide compared with emissions levels in 2019 1 . Nitrogen dioxide and small-particle air pollution, ubiquitous in big Chinese cities in which vehicular traffic and industry are heavy, decreased about 40% 2 . A similar situation is in Pianura Padana, Italy, where satellite images of the most air-polluted European region show a drastic decrease of atmospheric contaminants in the last days 3,4 . Many airlines recently announced plans to cut flights by more than 30% globally for the next months 5 . Airplane traffic has significantly dropped worldwide and, because it accounts for about 3-5% of total GHG emissions, this change could have a major impact on the atmosphere. Similarly, forecasts for oil demand in 2020 has been lowered by energy agencies 6 because coronavirus forces people to stay at home, leave cars in the garage, reduce the shopping, and save energy and resources.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 28,
                    "end": 35,
                    "text": "(Fig. 1",
                    "ref_id": null
                }
            ],
            "section": "Title: Coronavirus outbreak is a symptom of Gaia's sickness"
        },
        {
            "text": "The impact of coronavirus outbreak on atmospheric pollution and resource/energy consumption: a) the decline of mean tropospheric NO 2 (\u03bcmol/m 2 ), a major air pollutant, in Eastern China from January to February 2020 (adapted from NASA 2 ); b) the drop of Nitrogen Dioxide (NO 2 ) over Italy, particularly in Pianura Padana, seen from the Sentinel-5P satellite between January and March 2020 (adapted from ESA 3 ); c) the reduction of air-particle (PM10; \u03bcg/m 3 ) over Lombardia and Milan (Italy) in just 10 days after the area shut down (adapted from ARPA Lombardia 4 ); d) the variation in 7-days moving average of commercial flights from 2019 to 2020 detected by Flighradar24 5 ; e) the daily coal use by six main power companies before and after the Chinese New Year (dashed red line) has not recovered in 2020 after the holidays, when most business close down, as in the period 2014-2020 (adapted from CREA 1 ); f) the global oil demand (historical; grey histograms) from 2011 to 2019 is expected in 2020-2025 to show a decreasing trend (dashed red line) due to 2020 negative consumption (in thousand barrels of oil per day, mb/d; adapted from IEA 6 ) In this scenario of a broad global spread of the COVID-19, modelling says that economic growth will be halved in 2020 7 . Despite this being bad news for world affairs, it may actually be a panacea for Gaia. It might also be true, as analysts recently suggested, that decreased travelling, consumption, and energy demand will limit money and political will from climate efforts. Then, carbon emissions are likely to rise again as soon as the economy restarts its foolish growth. Projections, however, do not take into account a life lesson we are all learning these days: we cannot stop traveling, reproducing, consuming. But we have to do it sustainably and ethically. The economy, as well as our population, cannot continue growing indefinitely in a sustainable and ethical way. The myth of sustainable development, greenwashed by the green economy and certification labels, cannot persist in a planet threatened by only one species, which created the condition for its own extinction. We can only sustainably de-grow, creating an open space only for qualitative growth.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Figure 1"
        },
        {
            "text": "Sustainable degrowth and qualitative growth may still look fancy and utopian ideas but they are what many people are experiencing these weeks of compulsory sobriety. Humanity may rediscover the pleasure of a slower life, spending more time at home with family, reducing useless travelling towards offices when teleworking can be a win-win solution, giving more value to time and more time to values, getting back to nature, spending more time in local, creative purposeful pursuits such as growing food, etc. Our species may also understand that it does not actually need to buy and accumulate cheap, polluting, useless stuff, which are not essential in a pandemic-risk world, and that local groceries and productions are the only life jackets in a globalized world, during an emergency landing to its localized origins. In a time of moderation, we may realize that most of our previous needs and habits, which we thought as unavoidable, were just trifles. A frivolousness for us that multiplied by billions represents a serious risk for Gaia. Think about fish: do we really need to overexploit the stocks in oceans on the other side of the planet to savour sushi in all-you-caneat restaurants all over the world? Think about palm oil: do we really need to harass Southeast Asian forests and their unreplaceable biodiversity to fill our cars and our junk-food with a tropical fat? Nowadays, these answers are within our reach: we don't! We can live without these unnecessary \"privileges\" and this will not be an enormous limitation in our lives but represents a gigantic relief for our Earth.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Figure 1"
        },
        {
            "text": "This pandemic condition makes us clear that human activities and well-being strongly dependent on the health of the global environment and are fully integrated with ecosystem functioning and services. We need to rethink the way we model and manage our ecological and economic systems to better address the sustainable use of natural resources. The ability to predict the ecological consequences of the way we manage natural resources is now essential to address policies and decision-making. Although in the past, ecological models did not often contribute to such predictions (Schuwirth et al. 2019) , there is now urgency to use them for practical applications. The current epidemiological crisis, linked to global environmental degradation, shows us that indiscriminate exploitation and poorly supported management can be detrimental to ecosystems, other species, and our society. Nowadays, humanity has available several modelling approaches (e.g. machine learning, mechanistic and statistical models, etc.) that can be used to predict the response of ecosystems to anthropogenic impacts. Nonetheless, theoretical models unsupported by empirical data and field validation frequently led to either poor predictability or over parameterization, which resulted in catastrophic Nature degradation and misunderstanding about the actual utility of ecological models (Pilkey and Pilkey-Jarvis 2007) .",
            "cite_spans": [
                {
                    "start": 577,
                    "end": 600,
                    "text": "(Schuwirth et al. 2019)",
                    "ref_id": "BIBREF18"
                },
                {
                    "start": 1364,
                    "end": 1395,
                    "text": "(Pilkey and Pilkey-Jarvis 2007)",
                    "ref_id": "BIBREF15"
                }
            ],
            "ref_spans": [],
            "section": "Figure 1"
        },
        {
            "text": "If anything, ecological research that employs models should be able to answer practical and critical questions, besides theoretical ones. More efforts should be put on the variable selection to improve the understanding of causality between actions and impacts. Big data should not be tortured, without preliminary hypothesis, until they confess something useful only to publish flawed papers. They should, instead, improve model elaboration to align them with the management decision-making, quantifying uncertainty, in order to have enough predictive performance. Similarly, advanced computing resources should not be wasted in purposeless data exploration. They should, instead, increase the sensitivity and validity of our analyses, to better understand the adequacy and importance of models in improving forecasting, solving management problems, and advancing theories.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Figure 1"
        },
        {
            "text": "A trans-interdisciplinary holistic approach, which merges the knowledge of theoretical and field biologist, ecological modelers, environmental decision-makers and research, educational, political, and healthcare institutions, would improve our capacity to reduce our impacts on Earth's systems and live more sustainably within Gaia. Improved and reliable ecological models can, for instance, help our species to better protect biodiversity and halt its loss, plan habitat and endangered species conservation, assess and preserve ecosystem services, manage and contrast the spread of alien species, decrease deforestation and overfishing, reduce air, water, and soil pollution, adapt to and limit climate change, andeventuallyenhance our species' health, reducing the risk of pandemic diffusion of resistant viruses and bacteria caused by our mistreatment of wildlife and ecosystems. This can re-establish the importance of ecological modelling in fostering the transfer of theoretical scientific knowledge into practical everyday problems and in enabling environmental policymakers to adopt sustainable actions.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Figure 1"
        },
        {
            "text": "Nowhere it is written that economic growth and environmental exploitation should restart as they were. We are receiving warning messages from Gaia, some of the strongest and clearest of all our evolutionary time. If we ignore them, we can blame only ourselves.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Figure 1"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Has the Earth's sixth mass extinction already arrived?",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "D"
                    ],
                    "last": "Barnosky",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Nature",
            "volume": "471",
            "issn": "7336",
            "pages": "51--57",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Global forest loss disproportionately erodes biodiversity in intact landscapes",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "G"
                    ],
                    "last": "Betts",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Nature",
            "volume": "547",
            "issn": "7664",
            "pages": "441--444",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Scientists debate Gaia: the next century",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Boston",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Margulis",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Lovelock",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "R"
                    ],
                    "last": "Torres",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Is Gaia alive? The future of a symbiotic planet",
            "authors": [
                {
                    "first": "Cazzolla",
                    "middle": [],
                    "last": "Gatti",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Futures",
            "volume": "104",
            "issn": "",
            "pages": "91--99",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Biological annihilation via the ongoing sixth mass extinction signaled by vertebrate population losses and declines",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Ceballos",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "R"
                    ],
                    "last": "Ehrlich",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Dirzo",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Proceedings of the National Academy of Sciences",
            "volume": "114",
            "issn": "30",
            "pages": "6089--6096",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Population growth and earth's human carrying capacity",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "E"
                    ],
                    "last": "Cohen",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Science",
            "volume": "269",
            "issn": "5222",
            "pages": "341--346",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "The effects of weather and climate change on dengue",
            "authors": [
                {
                    "first": "F",
                    "middle": [
                        "J"
                    ],
                    "last": "Col\u00f3n-Gonz\u00e1lez",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Fezzi",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [
                        "R"
                    ],
                    "last": "Lake",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "R"
                    ],
                    "last": "Hunter",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "PLoS neglected tropical diseases",
            "volume": "7",
            "issn": "11",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "The Arctic Ocean as a dead end for floating plastics in the North Atlantic branch of the Thermohaline Circulation",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "C\u00f3zar",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Science advances",
            "volume": "3",
            "issn": "4",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Making ecological models adequate",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "M"
                    ],
                    "last": "Getz",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Ecology letters",
            "volume": "21",
            "issn": "2",
            "pages": "153--166",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Distinct clones of Yersinia pestis caused the black death",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Haensch",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "PLoS pathogens",
            "volume": "6",
            "issn": "10",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "AIDS as a zoonosis: scientific and public health implications",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "H"
                    ],
                    "last": "Hahn",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "M"
                    ],
                    "last": "Shaw",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "M"
                    ],
                    "last": "De",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "M"
                    ],
                    "last": "Sharp",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Science",
            "volume": "287",
            "issn": "5453",
            "pages": "607--614",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Clinical features of patients infected with 2019 novel coronavirus in Wuhan",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "China. The Lancet",
            "volume": "395",
            "issn": "10223",
            "pages": "497--506",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Global trends in emerging infectious diseases",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "E"
                    ],
                    "last": "Jones",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Nature",
            "volume": "",
            "issn": "7181",
            "pages": "990--993",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "The revenge of Gaia: Why the Earth is fighting back and how we can still save humanity",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Lovelock",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "",
            "volume": "36",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Environmental risk and meningitis epidemics in Africa",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "M"
                    ],
                    "last": "Molesworth",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Emerging infectious diseases",
            "volume": "9",
            "issn": "10",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Useless arithmetic: why environmental scientists can't predict the future",
            "authors": [
                {
                    "first": "O",
                    "middle": [
                        "H"
                    ],
                    "last": "Pilkey",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Pilkey-Jarvis",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "Emergence of a novel human coronavirus threatening human health",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "L"
                    ],
                    "last": "Poon",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Peiris",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Nature Medicine",
            "volume": "26",
            "issn": "",
            "pages": "317--319",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Global forecasts of urban expansion to 2030 and direct impacts on biodiversity and carbon pools",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "C"
                    ],
                    "last": "Seto",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "G\u00fcneralp",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "R"
                    ],
                    "last": "Hutyra",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Proceedings of the National Academy of Sciences",
            "volume": "109",
            "issn": "40",
            "pages": "16083--16088",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "How to make ecological models useful for environmental management",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Schuwirth",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Ecological Modelling",
            "volume": "411",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF19": {
            "ref_id": "b19",
            "title": "The Anthropocene: are humans now overwhelming the great forces of nature",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Steffen",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "J"
                    ],
                    "last": "Crutzen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "R"
                    ],
                    "last": "Mcneill",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "AMBIO: A Journal of the Human Environment",
            "volume": "36",
            "issn": "8",
            "pages": "614--621",
            "other_ids": {}
        },
        "BIBREF20": {
            "ref_id": "b20",
            "title": "25 years of HIV-1 research-progress and perspectives",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Wainberg",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "T"
                    ],
                    "last": "Jeang",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Situation Report",
            "volume": "6",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF21": {
            "ref_id": "b21",
            "title": "Impacts of biodiversity loss on ocean ecosystem services. science",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Worm",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "",
            "volume": "314",
            "issn": "",
            "pages": "787--790",
            "other_ids": {}
        },
        "BIBREF22": {
            "ref_id": "b22",
            "title": "Impact of deforestation and agricultural development on anopheline ecology and malaria epidemiology. The American journal of tropical medicine and hygiene",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Yasuoka",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Levins",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "",
            "volume": "76",
            "issn": "",
            "pages": "450--460",
            "other_ids": {}
        },
        "BIBREF23": {
            "ref_id": "b23",
            "title": "\u2612 The authors declare that they have no known competing financial interests or personal relationships that could have appeared to influence the work",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF24": {
            "ref_id": "b24",
            "title": "\u2610The authors declare the following financial interests/personal relationships which may be considered as potential competing interests",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "26",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF25": {
            "ref_id": "b25",
            "title": "?firstlevel=Inquinanti retrieved on March",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "16",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "Author: Roberto Cazzolla Gatti 1,2* Affiliations: Konrad Lorenz Institute for Evolution and Cognition Research, Austria Biological Institute, Tomsk State University, Russia",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}