{
    "paper_id": "28bb772b4c523dafbc18cc2f3c7d1be13479bb89",
    "metadata": {
        "title": "Journal Pre-proof Commentary: Rethinking Surgical Protocols in the Covid-19 Era Commentary: Rethinking Surgical Protocols in the Covid-19 Era",
        "authors": [
            {
                "first": "Daniel",
                "middle": [
                    "T"
                ],
                "last": "Engelman",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Rakesh",
                "middle": [
                    "C"
                ],
                "last": "Arora",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Manitoba",
                    "location": {
                        "settlement": "Winnipeg",
                        "region": "Manitoba",
                        "country": "Canada"
                    }
                },
                "email": "rakeshcarora@gmail.com"
            },
            {
                "first": "Vascular",
                "middle": [],
                "last": "Program",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Baystate",
                "middle": [],
                "last": "Health",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Cr3012 -369",
                "middle": [],
                "last": "Tache",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Ave",
                "middle": [],
                "last": "St",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Hospital",
                "middle": [],
                "last": "Boniface",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "",
                "middle": [],
                "last": "Winnipeg",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Central Message: In the current era of the SARS-CoV-2 (a.k.a. Covid-19) pandemic, routine preoperative nasal swabbing for identification of Staph carriers in our patient population should be reconsidered. In this issue of the Journal, Mayeur and colleagues 2 suggest that the routine practice of nasal screening for Staphyloccus aureus before surgery may compromise healthcare professional safety during the swabbing and testing process. Routine universal decontamination using intranasal povidone-iodine has been shown to decrease the surgical site infection rate by more than 50% among patients undergoing elective orthopedic surgery, 3 and as such under normal circumstances this would seem a prudent approach in cardiac surgical patients. 4 With inadvertent transmission of Covid-19 as a possible consideration, it is imperative to reduce potential risk of infection to our healthcare workers, perhaps our most precious resource in our anti Covid-19 armamentarium. Logic dictates that until a more critical assessment is made of the risk posed by this practice, routine preoperative nasal swabbing for identification of Staph carriers in our patient population should be reconsidered. The risk of bacterial resistance with routine, rather than selective, nasal decontamination is far outweighed by the advantage of avoiding any additional Covid-19 infection risk in our healthcare workers. Thus, we suggest the routine decontamination of all patients needing urgent/emergent procedures, without preoperative testing to determine colonization.",
            "cite_spans": [
                {
                    "start": 637,
                    "end": 638,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 743,
                    "end": 744,
                    "text": "4",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Interim Guidance for Healthcare Facilities: Preparing for Community Transmission of COVID-19 in the United States",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Does nasal screening for Staphyloccus aureus before surgery compromise healthcare professional safety in COVID-19 era?",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Mayeur",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Berthoumieu",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Charbonneau",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Thorac Cardovasc Surg",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Effect of a preoperative decontamination protocol on surgical site infections in patients undergoing elective orthopedic surgery with hardware implantation",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "P"
                    ],
                    "last": "Bebko",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "M"
                    ],
                    "last": "Green",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "S"
                    ],
                    "last": "Awad",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "JAMA Surg",
            "volume": "150",
            "issn": "",
            "pages": "390--395",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Staphylococcus Aureus Prevention Strategies in Cardiac Surgery: A Cost-Effectiveness Analysis",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "Hong",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "K"
                    ],
                    "last": "Saraswat",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "A"
                    ],
                    "last": "Ellison",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Ann Thorac Surg",
            "volume": "105",
            "issn": "",
            "pages": "47--53",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Picture Legend: Daniel T. Engelman and Rakesh C. Arora MD PhD In the current era of the SARS-CoV-2 (a.k.a. Covid-19) pandemic, the Centers for Disease Control and Prevention recently published recommendations for the care of our patients. There were additional modifications provided by the American College of Surgeons particularly pertinent to surgical patients and personnel. 1 Both organizations have recommended stopping elective cases and to implement logical, tiered general precautions, but there has been less granular guidance addressing specific aspects of perioperative care and management. Although by and large the surgical community has complied with this request, the infectious risk of Covid-19 and its life-threatening complications remains a reality for those patients requiring urgent/emergency surgery and for the surgeons and healthcare professionals who perform these operations.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}