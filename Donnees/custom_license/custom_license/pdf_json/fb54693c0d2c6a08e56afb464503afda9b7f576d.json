{
    "paper_id": "fb54693c0d2c6a08e56afb464503afda9b7f576d",
    "metadata": {
        "title": "Original Contribution Risks of Death and Severe Disease in Patients With Middle East Respiratory Syndrome Coronavirus, 2012-2015",
        "authors": [
            {
                "first": "Caitlin",
                "middle": [
                    "M"
                ],
                "last": "Rivers",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Washington State University",
                    "location": {
                        "addrLine": "240 SE Ott Road",
                        "postCode": "99164",
                        "settlement": "Pullman",
                        "region": "WA"
                    }
                },
                "email": ""
            },
            {
                "first": "Maimuna",
                "middle": [
                    "S"
                ],
                "last": "Majumder",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Washington State University",
                    "location": {
                        "addrLine": "240 SE Ott Road",
                        "postCode": "99164",
                        "settlement": "Pullman",
                        "region": "WA"
                    }
                },
                "email": ""
            },
            {
                "first": "Eric",
                "middle": [
                    "T"
                ],
                "last": "Lofgren",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Washington State University",
                    "location": {
                        "addrLine": "240 SE Ott Road",
                        "postCode": "99164",
                        "settlement": "Pullman",
                        "region": "WA"
                    }
                },
                "email": "eric.lofgren@wsu.edu."
            }
        ]
    },
    "abstract": [
        {
            "text": "Middle East respiratory syndrome coronavirus (MERS-CoV) is an emerging pathogen, first recognized in 2012, with a high case fatality risk, no vaccine, and no treatment beyond supportive care. We estimated the relative risks of death and severe disease among MERS-CoV patients in the Middle East between 2012 and 2015 for several risk factors, using Poisson regression with robust variance and a bootstrap-based expectation maximization algorithm to handle extensive missing data. Increased age and underlying comorbidity were risk factors for both death and severe disease, while cases arising in Saudi Arabia were more likely to be severe. Cases occurring later in the emergence of MERS-CoV and among health-care workers were less serious. This study represents an attempt to estimate risk factors for an emerging infectious disease using open data and to address some of the uncertainty surrounding MERS-CoV epidemiology. coronaviruses; emerging infections; MERS-CoV; Middle East respiratory syndrome coronavirus; respiratory infections; zoonotic infections Abbreviation: MERS-CoV, Middle East respiratory syndrome coronavirus.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Middle East respiratory syndrome coronavirus (MERS-CoV) is a stage 3 zoonosis that has been reported in 26 countries, including the United States (1, 2) . The virus was first recognized in Saudi Arabia in 2012, though it may have been circulating in the region much longer (3, 4) . As of August 18, 2015, there have been 1,413 confirmed cases and 502 deaths (5) . The virus causes severe respiratory illness in humans and has a mortality rate of 30%-40% (6) . Treatment for MERS-CoV cases is limited to supportive care.",
            "cite_spans": [
                {
                    "start": 146,
                    "end": 149,
                    "text": "(1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 150,
                    "end": 152,
                    "text": "2)",
                    "ref_id": null
                },
                {
                    "start": 273,
                    "end": 276,
                    "text": "(3,",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 277,
                    "end": 279,
                    "text": "4)",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 358,
                    "end": 361,
                    "text": "(5)",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 454,
                    "end": 457,
                    "text": "(6)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Certain groups may be at higher risk of contracting the virus or of having their cases ascertained due to illness severity, including males and those with comorbid medical conditions, such as diabetes and heart disease. Common symptoms include fever, cough, shortness of breath, chest pain, and diarrhea (7, 8) . The virus is probably transmitted from camels to humans, and stuttering chains (groups of cases linked by a continuous chain of transmission events that arise periodically) of human-to-human transmission are also possible (4, (7) (8) (9) (10) . Human-to-human transmission occurs between 2 people in close contact, a circumstance common in households and health-care settings. Early identification and isolation of cases is critical for limiting spread of the virus.",
            "cite_spans": [
                {
                    "start": 304,
                    "end": 307,
                    "text": "(7,",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 308,
                    "end": 310,
                    "text": "8)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 535,
                    "end": 538,
                    "text": "(4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 539,
                    "end": 542,
                    "text": "(7)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 543,
                    "end": 546,
                    "text": "(8)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 547,
                    "end": 550,
                    "text": "(9)",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 551,
                    "end": 555,
                    "text": "(10)",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Information on the epidemiology of MERS-CoV has been limited to date. Prior work on the 2013 influenza (A)H7N9 outbreak found that line listings of cases aggregated from publicly available sources like media and public health reports compare favorably to official line listings (11) . These public line listings can be used to gain insight into an ongoing outbreak in a timely manner, as official data tend to be released only after outbreaks are over. Real-time analyses are vital to planning and implementing effective public health control measures to prevent the spread of the disease. We used publicly available data to evaluate the risks of death and severe disease among patients with MERS-CoV.",
            "cite_spans": [
                {
                    "start": 278,
                    "end": 282,
                    "text": "(11)",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A publicly accessible line listing of MERS-CoV cases, maintained by Dr. Andrew Rambaut and available online (12) , was accessed on August 4, 2015. This line listing contained 1,291 cases of MERS-CoV infection pulled from a number of sources, including the World Health Organization and the government of the Kingdom of Saudi Arabia. This data set has often been more up-to-date than official World Health Organization case reports, especially early in the epidemic. The outcomes available are as reported and do not necessarily reflect the final status of the patient after prolonged follow-up, so some misclassification of outcomes is possible. The majority of MERS-CoV cases occurred in Saudi Arabia, South Korea, and the United Arab Emirates (Appendix Table 1 ). The outbreak in South Korea was excluded from the analysis because of its unique nature, resulting in 1,105 cases after exclusion.",
            "cite_spans": [
                {
                    "start": 108,
                    "end": 112,
                    "text": "(12)",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 755,
                    "end": 762,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "Data sources"
        },
        {
            "text": "Outcomes of interest were death and severe disease. The status of the patient as either alive or deceased was determined by whether or not the patient had died at the time of initial reporting. Patients with severe disease were considered those who had either died from their infection or required critical care at the time of initial reporting, as opposed to those who experienced few or less serious complications.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Exposure definition and covariate selection"
        },
        {
            "text": "Risk factors considered were the patient's age, the date of onset of the infection, the presence or absence of any underlying comorbidity such as cardiac or renal disease, reported contact with camels or other animals, whether or not the patient was employed as a health-care worker, whether or not the case was a primary or secondary case (based on reported contact with an existing case), whether or not the case arose in Saudi Arabia (the nation in which the majority of cases originated), the patient's sex, the number of days since January 1, 2012, and the time between onset of infection and subsequent hospitalization.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Exposure definition and covariate selection"
        },
        {
            "text": "Because of the emerging nature of the disease, the widely varying sources from which the case reports were drawn, difficulty in case ascertainment, and sparse reporting, the data set used (12) had extensively missing data. There were 920 cases with missing information on 1 or more variables (including outcome variables), making conventional complete-case analysis essentially impossible. Because there was no evidence that these cases were missing data completely at random, estimates could be biased.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Missing data"
        },
        {
            "text": "We used a bootstrap-based expectation maximization method to multiply impute the missing information (13). One hundred imputations were used, based on the assumption that all data for the variables included in the analysis, missing or observed, came from a multivariate normal distribution. A ridge prior of 1% of the empirical data was used to assist with the numerical stability of the algorithm. The ridge prior in essence adds an additional number of observations equal to 1% of the data set with the same mean and variance as the observed data, but with no covariance. This shrinks the covariance between the variables in the imputation model and assists the algorithm in converging on a stable solution, which is sometimes necessary with high degrees of missingness, as in this case. Priors using 0.5% of the data or 2% of the data did not result in meaningful differences in the results (not shown).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Missing data"
        },
        {
            "text": "Poisson regression models using a robust variance estimator (14) were used to estimate the univariate relative risk of either outcome according to each potential risk factor. These models are comparable to those obtained using binomial regression, though often more computationally tractable. Those variables that were moderately associated (P < 0.20) with the outcome were included in a multivariate risk model. All analysis was performed with the R statistical programming language (R Foundation for Statistical Computing, Vienna, Austria) using the Amelia2 package for multiple imputation (15) .",
            "cite_spans": [
                {
                    "start": 60,
                    "end": 64,
                    "text": "(14)",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 592,
                    "end": 596,
                    "text": "(15)",
                    "ref_id": "BIBREF14"
                }
            ],
            "ref_spans": [],
            "section": "Regression models"
        },
        {
            "text": "Because this work used entirely publicly available information with no personal identifiers, it was determined to not require approval by an institutional review board.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Human subjects approval"
        },
        {
            "text": "The distribution of patient ages for both fatal and nonfatal cases is shown in Figure 1 . The distributions of other variables, including the numbers of missing values, are reported in Table 1 .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 79,
                    "end": 87,
                    "text": "Figure 1",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 185,
                    "end": 192,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                }
            ],
            "section": "Demographic characteristics"
        },
        {
            "text": "The estimated relative risk of death and corresponding 95% confidence intervals for the covariates described in the Methods section are shown in Table 2 . As with any emerging infection, both the presence and the absence of associations with putative risk factors warrant reporting. Univariate analysis showed that reported contact with camels or other animals, cases occurring in Saudi Arabia, and case type (a case's being primary vs. secondary) were not associated with reported mortality. Employment as a health-care worker and an increased amount of time between disease onset and hospitalization had minor protective associations with reported mortality. Older age and underlying comorbidity were associated with increased risks of mortality, while female patients and cases with a later time of infection onset (in days since January 1, 2012) had lower risks of mortality. Upon multivariate adjustment, most of the estimated associations were attenuated, and neither female sex nor time between disease onset and hospitalization remained an independent risk factor.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 145,
                    "end": 152,
                    "text": "Table 2",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Risk factors for reported mortality"
        },
        {
            "text": "The estimated relative risks of severe disease and corresponding 95% confidence intervals are shown in Table 2 .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 103,
                    "end": 110,
                    "text": "Table 2",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Risk factors for reported severe disease"
        },
        {
            "text": "Reported contact with camels or other animals, regardless of whether or not the case arose in Saudi Arabia, and longer delays between disease onset and hospitalization were not associated with an increased risk of severe disease. Increased age and the presence of underlying comorbidity were associated with an increased risk of severe disease. Female sex, having a secondary case, having a case arising later in time, and employment as a health-care worker were protective against severe disease.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Risk factors for reported severe disease"
        },
        {
            "text": "As with the risk of reported death, the multivariate associations were largely attenuated from the univariate associations, and notably, female sex was no longer protective once other variables had been controlled for. As compared with the risk of death, the estimated associations for severe disease were frequently closer to the null.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Risk factors for reported severe disease"
        },
        {
            "text": "The emergence of a novel infectious disease presents a particular challenge to timely epidemiologic research, as the existence of sparse and irregularly collected data competes with the need to identify risk factors associated with the disease and its outcomes. A dearth of openly shared data impedes research efforts, such as the construction of mathematical models or broader-scale risk assessments. We have attempted to address this for MERS-CoV, using a regularly updated, publicly available data set. The use of multivariate models with allowance for extensively missing data has allowed the identification of some previously suggested risk factors that do not appear to be so upon adjustment for other covariates. For example, female patients were not necessarily at lower risk for disease after adjustment, nor were primary cases at higher risk for fatal infections. Issues of data quality and \"missingness\" during outbreaks necessitate the use of robust techniques for handling missing data.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DISCUSSION"
        },
        {
            "text": "We found that older age and underlying comorbidity were associated with increased risks of both death and severe disease. While not a surprising finding, this does suggest that older and sicker patients merit heightened vigilance. Additionally, cases arising progressively later during the epidemic have been associated with lower risks of both death and severe disease at the time of initial reporting, suggesting that treatment methods for MERS-CoV may be increasing in efficacy. Alternately, the proportion of mild and asymptomatic cases has been rising over time, suggesting that less severe cases are becoming more likely to be ascertained as a result of epidemiologic investigation. This is supported by temporal trends in the missingness of the data, which grows less severe later in the epidemic.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DISCUSSION"
        },
        {
            "text": "This study was not without limitations, especially those stemming from the data used. Patient outcomes were identified at the time of reporting, rather than based on follow-up, so it is possible that some patients counted as living or without severe disease may have experienced serious or fatal complications after reporting, which would not have been recorded in the data. There is also the possibility of unmeasured confounding biasing these estimates or the multiple imputation model not fully addressing the missingness within the data set. These issues are unlikely to be resolved without more resource-intensive population-based studies.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DISCUSSION"
        },
        {
            "text": "Despite these shortcomings, the study represents an attempt to quantify the known risk factors for MERS-CoV using the best available and open data. While the estimates are imperfect, they are superior to univariate associations that do not control for confounding, or allowing paralysis in the face of difficult and imperfect data to deprive public health planners of potentially useful information. These estimates can and should be revised as more becomes known about the disease, but for the moment, they represent the current state of our knowledge about MERS-CoV and its impact on human health outcomes. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "DISCUSSION"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Epidemic dynamics at the human-animal interface",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "O"
                    ],
                    "last": "Lloyd-Smith",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "George",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "M"
                    ],
                    "last": "Pepin",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Science",
            "volume": "326",
            "issn": "5958",
            "pages": "1362--1367",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Novel coronavirus-Saudi Arabia: human isolate. ProMED-mail 2012",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "M"
                    ],
                    "last": "Zaki",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "",
            "volume": "15",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Middle East respiratory syndrome coronavirus infection in dromedary camels in Saudi Arabia",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "N"
                    ],
                    "last": "Alagaili",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Briese",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Mishra",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "MBio",
            "volume": "5",
            "issn": "2",
            "pages": "884--898",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Middle East respiratory syndrome coronavirus (MERS-CoV)-Saudi Arabia",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "18",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Severe Respiratory Disease Associated With Middle East Respiratory Syndrome Coronavirus (MERS-CoV",
            "authors": [],
            "year": 2014,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Hospital-associated outbreak of Middle East respiratory syndrome coronavirus: a serologic, epidemiologic, and clinical description",
            "authors": [
                {
                    "first": "Al-Abdallat Mm",
                    "middle": [],
                    "last": "Payne",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "C"
                    ],
                    "last": "Alqasrawi",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Clin Infect Dis",
            "volume": "59",
            "issn": "9",
            "pages": "1225--1233",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Hospital outbreak of Middle East respiratory syndrome coronavirus",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Assiri",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mcgeer",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "M"
                    ],
                    "last": "Perl",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "N Engl J Med",
            "volume": "369",
            "issn": "5",
            "pages": "407--416",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Identification of MERS-CoV in dromedary camels",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "M"
                    ],
                    "last": "Ferguson",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Van Kerkhove",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Lancet Infect Dis",
            "volume": "14",
            "issn": "2",
            "pages": "93--94",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Middle East respiratory syndrome coronavirus neutralising serum antibodies in dromedary camels: a comparative serological study",
            "authors": [
                {
                    "first": "Cbem",
                    "middle": [],
                    "last": "Reusken",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "L"
                    ],
                    "last": "Haagmans",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "M\u00fcller",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Lancet Infect Dis",
            "volume": "13",
            "issn": "10",
            "pages": "859--866",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Accuracy of epidemiological inferences based on publicly available information: retrospective comparative analysis of line lists of human cases infected with influenza A(H7N9) in China",
            "authors": [
                {
                    "first": "Ehy",
                    "middle": [],
                    "last": "Lau",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "K"
                    ],
                    "last": "Tsang",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "BMC Med",
            "volume": "12",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "What to do about missing values in time-series cross-section data",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Honaker",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "King",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Am J Pol Sci",
            "volume": "54",
            "issn": "2",
            "pages": "561--581",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "A modified Poisson regression approach to prospective studies with binary data",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Zou",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Am J Epidemiol",
            "volume": "159",
            "issn": "7",
            "pages": "702--706",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Amelia II: a program for missing data",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Honaker",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "King",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Blackwell",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "J Stat Softw",
            "volume": "45",
            "issn": "7",
            "pages": "1--47",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Gaussian kernel-smoothed age distributions of fatal and nonfatal cases of Middle East respiratory syndrome coronavirus from 2012 to 2015. Risk Factors for MERS-CoV 461",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Author affiliations: Network Dynamics and Simulation Science Laboratory, Biocomplexity Institute of Virginia Tech, Virginia Polytechnic Institute and State University, Blacksburg, Virginia (Eric T. Lofgren, Caitlin M. Rivers); and Engineering",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Demographic and Risk Factor Characteristics of Patients With Reported Middle East Respiratory Syndrome Coronavirus Infection, 2012-2015",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Estimated Relative Risks of Death and Severe Disease at the Time of Reporting for Patients With Middle East Respiratory Syndrome Coronavirus, 2012-2015 Abbreviations: aRR, adjusted relative risk; CI, confidence interval; RR, relative risk. a Multivariate model that adjusted for age, presence of comorbidity, reported contact with animals, health-care worker status, case type (primary vs. secondary), and patient sex. b Multivariate model that adjusted for age, time of onset, presence of comorbidity, health-care worker status, case type (primary vs. secondary), and patient sex. c Days since January 1, 2012. d Reported number of days between onset and subsequent hospitalization.",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "This work was supported by the National Institutes of Health (Models of Infectious Disease Agent Study (MIDAS) grant 5U01GM070694-11) and the Defense Threat Reduction Agency (grant HDTRA1-11-1-0016 and Comprehensive National Incident Management System (CNIMS) contract HDTRA1-11-D-0016-0001).We acknowledge Twitter Inc. (San Francisco, California) and the producers of other online collaboration tools for facilitating this research.Conflict of interest: none declared. ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Systems Division, Massachusetts Institute of Technology, Cambridge, Massachusetts (Maimuna S. Majumder)."
        }
    ]
}