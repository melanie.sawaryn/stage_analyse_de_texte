{
    "paper_id": "d2312a63559d0444a8e7e01c5b6f5d401ee73e4f",
    "metadata": {
        "title": "The difference in the incubation period of 2019 novel coronavirus (SARS-CoV-2) infection between travelers to Hubei and non-travelers: The need of a longer quarantine period",
        "authors": [
            {
                "first": "Char",
                "middle": [],
                "last": "Leung",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Deakin University",
                    "location": {
                        "addrLine": "221 Burwood Highway",
                        "postCode": "3125",
                        "country": "Australia"
                    }
                },
                "email": "char.leung@deakin.edu.au"
            }
        ]
    },
    "abstract": [
        {
            "text": "Data collected from the individual cases reported by the media were used to estimate the distribution of the incubation period of travelers to Hubei and non-travelers. Upon the finding of longer and more volatile incubation period in travelers, the duration of quarantine should be extended to three weeks.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "An epidemic of viral pneumonia started in Wuhan, the capital of Hubei province in China, in December 2019. A new coronavirus was identified and named by the World Health Organization as SARS-CoV-2. It has been found that it is genetically similar to SARS-CoV and MERS-CoV 1 . Recently, snakes have been suggested as the natural reservoirs of SARS-CoV-2, assuming that the Huanan Seafood Wholesale Market in Wuhan is the origin of the virus 2 .",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Different preventive measures have been implemented by health authorities with the 14-day quarantine being the commonly used. While previous studies have estimated the incubation period of SARS-CoV-2 to help determining the length of quarantine, it has recently been observed that some patients rather had mild symptoms such as cough and low-grade fever or even no symptoms 3 and that the incubation period might have been 24 days 4 , constituting greater threats to the effectiveness of entry screening. Against this background, the present work estimated the distribution of incubation periods of patients infected in and outside Hubei.",
            "cite_spans": [
                {
                    "start": 374,
                    "end": 375,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Because the details of most cases were reported by the media and were not available on the official web pages of the local health authorities in China, three searches for individual cases reported by the media between 20 th January 2020 and 12 th February (first cases outside Hubei reported on 20 th January 2020) with search terms \"pneumonia\" AND \"Wuhan\" AND \"age\" AND \"new\" in Chinese were performed on Google from 7 th , 8 th , and 9 th February. The inclusion of the search term \"age\" intended to narrow down the search results since the presence of \"age\" in an article implied a description of an individual case.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "Individual cases with time of exposure and symptom onset as well as type of exposure were eligible for inclusion. There was no language restriction. Since most patients did not have complete information about the source of infection, the time of exposure was allowed to be a time interval within which the exposure was believed to lie. In contrast, patients could recall the exact date of symptom onset. The present paper considered two types of exposure, (i) traveling to Hubei, China, and (ii) contact with the source of infection such as an infected person or places where infectious agents stayed. For data accuracy, only confirmed cases outside Hubei province and within China were considered.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "The following data were abstracted, (i) location at which the case was confirmed, (ii) gender, (iii) age, ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Methods"
        },
        {
            "text": "where F and S were the CDF of the incubation period and the time of symptom onset, respectively. Therefore, to find the maximum likelihood estimates of \uf071 , the maxima of the sum of the individual log-likelihood functions, either The results of maximum likelihood estimation are shown in Table 1 . The AIC suggested that the Weibull distribution provided the best fit to the data. Both indicator variables of the shape and scale parameters were significant in the Weibull model, suggesting different incubation period distributions between the two groups of patients.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 287,
                    "end": 294,
                    "text": "Table 1",
                    "ref_id": null
                }
            ],
            "section": "Methods"
        },
        {
            "text": "[ Table 1 here]",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 2,
                    "end": 9,
                    "text": "Table 1",
                    "ref_id": null
                }
            ],
            "section": "Methods"
        },
        {
            "text": "The very first observation of the incubation period of SARS-CoV-2 came from the National Health Such difference might be due to the difference in infectious dose since travelers to Hubei might be exposed to different sources of infection multiple times during their stay in Hubei. In contrast, patients with no travel history to Hubei were temporarily exposed to their infected relatives, friends or colleagues with mild or even no symptoms.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "It is possible that the incubation period of non-travelers was highly volatile, as suggested by the higher variance in the gamma model that provided slightly poorer fit. This could potentially pose a threat to the effectiveness of the existing preventive measures. The duration of quarantine period must be considered with caution.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "As a comparison, previous studies on the incubation period for SARS-CoV-2 are shown in Table 2 .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 87,
                    "end": 94,
                    "text": "Table 2",
                    "ref_id": null
                }
            ],
            "section": "Discussion"
        },
        {
            "text": "The 95 th percentiles reported in previous studies varied between 10.3 and 13.3 days, consistent with the current practice of quarantine period of 2 weeks. However, the present study found that the 95 th percentile of non-travelers could be 14.6 days and up to 17.1 days under 95% level of confidence.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Coupled with the high variability of the incubation period, it is suggested that the duration of the quarantine period of 3 weeks is deemed more suitable.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "[ ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "A novel coronavirus from patients with pneumonia in China",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001017"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Homologous recombination within the spike glycoprotein of the newly identified coronavirus may boost cross-species transmission from snake to human",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Ji",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Med Virol",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1002/jmv.25682"
                ]
            }
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "A familial cluster of pneumonia associated with the 2019 novel coronavirus indicating person-to-person transmission: a study of a family cluster",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "F"
                    ],
                    "last": "Chan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Yuan",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kok",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30154-9"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Clinical characteristics of 2019 novel coronavirus infection in China",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Ni",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1101/2020.02.06.20020974"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Estimating incubation period distributions with coarse data",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "G"
                    ],
                    "last": "Reich",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Lessler",
                    "suffix": ""
                },
                {
                    "first": "Dat",
                    "middle": [],
                    "last": "Cummings",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Stat Med",
            "volume": "28",
            "issn": "22",
            "pages": "2769--2784",
            "other_ids": {
                "DOI": [
                    "10.1002/sim.3659"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Early Transmission Dynamics in Wuhan, China, of Novel Coronavirus-Infected Pneumonia",
            "authors": [
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Wu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMoa2001316"
                ]
            }
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Incubation period of 2019 novel coronavirus (COVID-19) infections among travellers from Wuhan",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Backer",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Klinkenberg",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wallinga",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.2807/1560-7917.ES.2020.25.5.2000062"
                ]
            }
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "iv) time of exposure, (v) time of first symptom onset, (vi) type of exposure (traveler to Hubei and non-traveler) and (vii) symptoms. The incubation period distribution was estimated using maximum likelihood (MLE) where the likelihood function of each observation in the data set is either exact or single interval-censored 5 . For an individual case with exact time of exposure and symptom, the likelihood function for an exact incubation period observation, T , was () fT \uf071 , where f and \uf071 were the PDF of the incubation period and the set of parameters, respectively. For an individual case with exposure lying between 1 E and 2 E , the likelihood function for an incubation observation was 12 (",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Commission of China, reporting an incubation time between 1 and 14 days6 . Statistical estimation of the distribution of incubation periods has been done in two other studies7,8 . The present study further explored the difference in incubation periods among different groups of patients. Clinical data were collected from the individual cases reported by the media as they were not fully available on the official pages of the Chinese health authorities. MLE was used to estimate the distributions of the incubation period.The present work found significant difference in the distribution of the incubation period among travelers to Hubei and non-traveler. The difference was due to both the location and variability, as indicated by the means of 1.8 and 7.2 days and the variances of 0.7 and 16.2 in the Weibull model.",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "on the data type of the observation, were computed with R software.T was assumed to follow lognormal, Weibull and gamma distribution. To ascertain possible difference in distribution between the traveler and non-traveler group, \uf071 was adjusted by includingResultsA total of 1457 results were generated by Google. A hundred and seventy-five cases from 64 web pages were eligible for inclusion. All patients could recall the data of symptom onset. Fifty-one patients were able to recall the exact date of exposure. Many of these patients stayed in Hubei for a day, or had a friend or family gathering on a particular day. The remaining 124 patients could only recall the time interval of exposure largely went to Hubei for sightseeing, work or family visiting, or lived with an infected family member.",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Results of maximum likelihood estimationNote: *Significantly different from zero under 5% **the lower the better fit to the dataTable 2. Estimated incubation periods for SARS-CoV-2 from different studies",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}