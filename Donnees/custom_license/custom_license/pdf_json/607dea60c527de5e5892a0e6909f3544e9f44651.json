{
    "paper_id": "607dea60c527de5e5892a0e6909f3544e9f44651",
    "metadata": {
        "title": "Why transition matters as much as eradication: lessons from global polio surveillance",
        "authors": [
            {
                "first": "Joseph",
                "middle": [
                    "R A"
                ],
                "last": "Fitchett",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Bill & Melinda Gates Foundation",
                    "location": {
                        "settlement": "London",
                        "country": "UK"
                    }
                },
                "email": "*correspondingauthor:e-mail:joseph.fitchett@gatesfoundation.org"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "In 1988, the World Health Assembly launched the Global Polio Eradication Initiative (GPEI). In the year of the announcement, 350 000 cases of paralysis from polio were reported across 125 countries. 1 Today, over 2.5 billion children have since been immunised and, at the time of writing, there were 5 cases of polio in two countries, with only 37 cases reported in all of 2016. 2 Much of the success of global polio eradication efforts has rested on meticulous surveillance in the hardest to reach areas of the globe, and the principle of immunisation of every last child. As a result, between 1988 and 2017, GPEI's polio immunisation and acute flaccid paralysis (ALP) surveillance programme among children under 15 years of age-the gold standard for detecting poliomyelitis-are estimated to have avoided paralysis for 16 million children. 3 Such success has come at a significant financial cost, with over US$9 billion invested in polio eradication efforts since 1988. However, the economic returns to date are estimated to be $27 billion, with a further US$20 billion anticipated by 2035, and $17 billion from vitamin A supplementation delivered in parallel. 4 Witnessing interruption in transmission of polio virus will be a critical turning point for infectious disease control and surveillance. Yet, how the global community reacts and transitions in the coming years presents a unique opportunity to shape the future of surveillance-with ramifications reaching far beyond the immunisation, detection and management of polio alone. The increasing frequency of outbreaks of novel and existing pathogens adds a further imperative. 5 Middle East respiratory syndrome (MERS) coronavirus, Ebola virus disease, Zika virus, yellow fever and the threat of avian influenza have all presented surveillance and infection control challenges over the past 5 years.",
            "cite_spans": [
                {
                    "start": 199,
                    "end": 200,
                    "text": "1",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 379,
                    "end": 380,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 841,
                    "end": 842,
                    "text": "3",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 1162,
                    "end": 1163,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1635,
                    "end": 1636,
                    "text": "5",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "For the next generation of financing, governance and implementation of infectious disease surveillance, polio must learn from the mistakes and missed opportunities of the first and only human disease to be eradicated-smallpox-in transitioning assets, and look to the future. 6 Much of the focus of eradication initiatives are on interrupting transmission of the infectious pathogen. And rightfully so. However, considering the significant amount of resources, both human and financial, which are invested in surveillance and eradication initiatives, how the transition of assets of value are planned, prioritised and protected is key. In a global health landscape of limited resources, transition represents as important a goal as eradication.",
            "cite_spans": [
                {
                    "start": 275,
                    "end": 276,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Part of the reason transition will matter more for surveillance going forward is the evolution of the political and economic landscape for health and development. After the interruption of polio transmission and the certification of polio eradication, the GPEI will cease to exist. Sunsetting of the organisation, after first ensuring safe timely handover of the most valuable assets, is central to the success of development initiatives in general, and polio eradication efforts in particular. A successful sunsetting of the partnership and transition of assets will demonstrate a clear message of success for investing in global health, and the case that high impact development initiatives work, are catalytic and are time bound.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "There is, however, a threat to global investment in surveillance if polio eradication effort is not coupled with an excellent transition plan. This challenge is particularly evident when attempting to raise donor funding from a limited pool of contributors, and to raise domestic funding following successful polio control efforts when so few cases remain visible in comparison to significant other societal challenges. Furthermore, polio eradication alone represents 20% of the current WHO budget. 7 Although, in practice, polio systems have major benefits across health issues and to health systems more broadly, the case to transition valuable polio assets in support of global health governance is critical.",
            "cite_spans": [
                {
                    "start": 499,
                    "end": 500,
                    "text": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Going forward, the need for polio surveillance will diminish but the need for infectious disease surveillance will increase. The value of surveillance to societies, countries and global partnership must be better communicated. Global health, development and surveillance needs good news that transcends scientific, political and geographic boundaries. In the current socio-political and economic climate that is challenging the rationale of official development assistance (ODA), and where contributions to development assistance for health (DAH) are plateauing, 8 successful surveillance and polio eradication couldn't come at a better time.",
            "cite_spans": [
                {
                    "start": 563,
                    "end": 564,
                    "text": "8",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Author's contributions: JF has undertaken all the duties of authorship and is guarantor of the paper.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Eradication & Endgame Strategic Plan",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Gpei",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Polio",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Geneva: World Health Organization",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Polio this week",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Gpei",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "",
            "volume": "12",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Economic Case for Eradicating Polio. Geneva: Global Polio Eradication Initiative",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Gpei",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Economic analysis of the global polio eradication initiative",
            "authors": [
                {
                    "first": "Duintjer",
                    "middle": [],
                    "last": "Tebbens",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "J"
                    ],
                    "last": "Pallansch",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Cochi",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Vaccine",
            "volume": "29",
            "issn": "2",
            "pages": "334--377",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Global rise in human infectious disease outbreaks",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "F"
                    ],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Goldberg",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Rosenthal",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "J R Soc Interface",
            "volume": "11",
            "issn": "101",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "The Global Polio Eradication Initiative: Progress, Lessons Learned, and Polio Legacy Transition Planning",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "L"
                    ],
                    "last": "Cochi",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Hegg",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kaur",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Health Affairs",
            "volume": "35",
            "issn": "",
            "pages": "277--83",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Programme budget. Geneva: World Health Organization",
            "authors": [],
            "year": 2015,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Development assistance for health: past trends, associations, and the future of international financial flows for health",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "L"
                    ],
                    "last": "Dieleman",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "T"
                    ],
                    "last": "Schneider",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Haakenstad",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Lancet",
            "volume": "387",
            "issn": "",
            "pages": "2536--2580",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": [
        {
            "text": "Funding: None.Competing interests: None declared.Ethical approval: Not required.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        }
    ]
}