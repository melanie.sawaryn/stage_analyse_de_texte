{
    "paper_id": "fd0acbb867b91edb6426b6f7c3ba04c1eadf5919",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "To the Editor: With daily media warnings of a looming pandemic, physicians are understandably concerned about immunosuppressive or immunomodulating effects that might render patients receiving biologic therapies more susceptible to COVID-19 infection. At this early stage, we do not have specific data about susceptibility to the virus, but we have data on infectious complications for biologic therapies from their pivotal trials for psoriasis.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "In Table I , we compare overall infection rates as well as rates of upper respiratory infections and nasopharyngitis for each drug versus its placebo control based on published data from pivotal trials.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 3,
                    "end": 10,
                    "text": "Table I",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "For tumor necrosis factor blockers, during the placebo-controlled periods, overall infections and upper respiratory infections were increased by up to 7% compared with placebo, except for etanercept, which showed no increase. Tumor necrosis factor blockers carry a black box warning concerning infection. Ustekinumab showed a small increase in overall infections but not in respiratory tract infections. Ustekinumab blocks interleukin (IL) 12 and IL-23; IL-12 plays an important role in fighting viral infections. 1 IL-23 blockers showed increases in overall infections of up to 9%, but upper respiratory infections were increased slightly in some trials but not in others. IL-17 blockers showed increases in overall infections of up to 11%, but much of that increase could be accounted for by increases in monilial infections. Upper respiratory infections were increased slightly for secukinumab, but not for ixekizumab or brodalumab.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "It is difficult to extrapolate from these data to determine susceptibility to coronavirus infection, and this analysis is further flawed by small numbers of infections and short placebo-controlled periods. Moreover, minor respiratory infections may be underreported, and some infections may be reported doubly as upper respiratory infections and as nasopharyngitis.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "Nonetheless, these data may be used to decide whether to continue biologic therapy during pandemics. We do not know if biologic therapies render patients more susceptible to coronavirus, but we know that in the pre-coronavirus era, respiratory infection rates were comparable to those with placebo. Conversely, discontinuation of some biologics can result in loss of response when treatments are reintroduced or even result in the formation of antibodies to the discontinued biologic. [2] [3] [4] All of these factors must be considered when advising patients about continuing or discontinuing biologic therapies. To the Editor: Adult acne vulgaris (in persons aged $25 years) has a similar pathophysiologic mechanism as teenage acne, with hormonal regulation, cosmetic products, and stress or genetic factors contributing to its pathomechanism. 1 There are a wide variety of treatments for adult acne, including isotretinoin. Interestingly, there are few randomized control trials of isotretinoin compared with other acne treatments. 2 Many studies demonstrate improvements in adolescent acne after isotretinoin treatment. However, we sought to examine its usefulness in treating acne in women. We analyzed 3800 patient charts of female patients $25 years of age with acne vulgaris diagnoses in the Baylor Clinic. Of these patients, 393 met the inclusion criteria of isotretinoin treatment with sufficient documentation. Data was compiled from the electronic medical records and analyzed by using IBM SPSS Statistics version 25 (Armonk, NY). The average age of our patients was 34.6 years, median age 31 years, average 6 standard deviation cumulative dosage 103.83 6 52.78 mg/kg, and duration 6 standard deviation of treatment 3.9 6 1.7 months, respectively.",
            "cite_spans": [
                {
                    "start": 485,
                    "end": 488,
                    "text": "[2]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 489,
                    "end": 492,
                    "text": "[3]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 493,
                    "end": 496,
                    "text": "[4]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 846,
                    "end": 847,
                    "text": "1",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1035,
                    "end": 1036,
                    "text": "2",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "The results showed that 95.4% of patients had a positive response to treatment, with 43.3% experiencing 100% clearance of their lesions and 52.2% experiencing improvement but not complete resolution. The most frequently reported side effect from treatment was cheilitis and xerosis (97.3%). The side effect frequencies are summarized in Table I . Two patients discontinued treatment because of side effects (1 because of increased joint pain and the other a melasma flare while on treatment). In addition, 5 (1.3%) patients had elevations in liver enzymes or lipid panel results requiring discontinuation of therapy.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 337,
                    "end": 344,
                    "text": "Table I",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "Oral contraceptive pills (OCPs) were used as a form of contraception in 35.6% of patients. OCPs are a known therapeutic agent used to treat acne vulgaris. A 2 analysis revealed no difference in the response rates between patients on OCPs and those on nonhormonal contraceptive methods (P \u00bc .763, Pearson 2 \u00bc .540, degrees of freedom \u00bc 2).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "Limitations to our study include a lack of a standardized grading system for acne as well as a high rate of patients lost to follow-up (24.9%). Although these patients did not return for posttreatment follow-up, their last note did document their response to treatment, and these individuals were therefore included in the study.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        },
        {
            "text": "The high response rates demonstrated by our study match other studies, suggesting that isotretinoin is useful for treating acne vulgaris in both the adolescent and adult populations (91%-97.4%). 3, 4 After isotretinoin treatment, 15.5% of patients had a relapse documented at some point, lower than what was seen in a previous study (47.4%). 3 Despite the high frequency of cheilitis and xerosis, no patient elected to discontinue treatment because of these side effects. When combined with the low frequency of other side effects, isotretinoin should be considered a well-tolerated, useful option for physicians to consider when treating acne in adult female patients. ",
            "cite_spans": [
                {
                    "start": 195,
                    "end": 197,
                    "text": "3,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 198,
                    "end": 199,
                    "text": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 342,
                    "end": 343,
                    "text": "3",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Should biologics for psoriasis be interrupted in the era of COVID-19?"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "31 (31)* ,y 24 (7)/5 (5)* ,y 50 (14)/12 (12)* ,y IL-12/IL-23 Ustekinumab 326 (25)/150 (23)* ,y 64 (5)/30 (5)* ,y 105 (8)/29 (8)* ,y IL-23 Guselkumab 191 (23)/90 (21)* 41 (5)/19 (5)* 65 (8)/33 (8)* Tildrakizumab NR 25 (2)/9 (3)* ,y 120 (10)/20 (6)*",
            "authors": [
                {
                    "first": "*",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Certolizumab",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Infliximab",
            "volume": "125",
            "issn": "5",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "The IL-12 family of cytokines in infection, inflammation and autoimmune disorders",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Gee",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Guzzo",
                    "suffix": ""
                },
                {
                    "first": "Che",
                    "middle": [],
                    "last": "Mat",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "F"
                    ],
                    "last": "Ma",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Kumar",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Inflamm Allergy Drug Targets",
            "volume": "8",
            "issn": "1",
            "pages": "40--52",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Successful treatment of moderate to severe plaque psoriasis with the PEGylated Fab' certolizumab pegol: results of a phase II randomized, placebo-controlled trial with a re-treatment extension",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Reich",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "P"
                    ],
                    "last": "Ortonne",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "B"
                    ],
                    "last": "Gottlieb",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Br J Dermatol",
            "volume": "167",
            "issn": "1",
            "pages": "180--190",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Patients with moderateto-severe psoriasis recapture clinical response during re-treatment with etanercept",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "P"
                    ],
                    "last": "Ortonne",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ta\u20ac Ieb",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "D"
                    ],
                    "last": "Ormerod",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Br J Dermatol",
            "volume": "161",
            "issn": "5",
            "pages": "1190--1195",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Continuous dosing versus interrupted therapy with ixekizumab: an integrated analysis of two phase 3 trials in psoriasis",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Blauvelt",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "A"
                    ],
                    "last": "Papp",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Sofen",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "J Eur Acad Dermatol Venereol",
            "volume": "31",
            "issn": "10",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Fernandez, BS, a Brigette Lee, BS, a Jay M. Patel, BS, a Emma Weiss, MD, b Jiating Jiang, BS, a Harry Dao, MD, a and Soo Jung Kim, MD, PhD a",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Mark Lebwohl, MD, a Ryan Rivera-Oyola, MS, a and Dedee F. Murrell, MA, BMBCh, MD, FRCP, FACD b From the Icahn School of Medicine at Mt Sinai Hospital, New York, New York a ; and St. George Hospital, University of New South Wales, Sydney, Australia. b",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Rate of infections in available biologic agents for psoriasis, n (%)",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}