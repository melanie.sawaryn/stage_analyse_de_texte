{
    "paper_id": "22ed4107d83ecb0740bb6bd1df391bd1df602902",
    "metadata": {
        "title": "Journal Pre-proof A Case of Coronavirus Disease 2019 Treated With Ciclesonide Mayo Clinic Proceedings A Case of Coronavirus Disease 2019 Treated With Ciclesonide",
        "authors": [
            {
                "first": "Kento",
                "middle": [],
                "last": "Nakajima",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Fumihiro",
                "middle": [],
                "last": "Ogawa",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Kazuya",
                "middle": [],
                "last": "Sakai",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Munehito",
                "middle": [],
                "last": "Uchiyama",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Yutaro",
                "middle": [],
                "last": "Oyama",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Hideaki",
                "middle": [],
                "last": "Kato",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Yokohama City University Hospital",
                    "location": {
                        "addrLine": "3-9 Fukuura, Kanazawa-ku",
                        "postCode": "236-0004",
                        "settlement": "Yokohama",
                        "region": "Kanagawa",
                        "country": "Japan"
                    }
                },
                "email": ""
            },
            {
                "first": "Ichiro",
                "middle": [],
                "last": "Takeuchi",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "To the Editor: The novel severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) and the disease it causes, coronavirus disease 2019 , are the cause of a current pandemic. 1 At present, no drug has been proven to be effective for the treatment of COVID-19 and no vaccine is available. We report the first case of a Japanese patient with severe COVID-19 pneumonia who had a favorable outcome after receiving treatment with ciclesonide, an anti-inflammatory drug.",
            "cite_spans": [
                {
                    "start": 177,
                    "end": 178,
                    "text": "1",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A 64-year-old Japanese man consulted a local physician for fever lasting 3 days and was initially treated with azithromycin with a presumptive diagnosis of pneumonia. However, 3 days later (illness day 6), he was referred to another hospital because the fever persisted. On illness day 9, the patient began minocycline treatment. However, his respiratory condition worsened. On illness day 11, he was referred to Yokohama City University hospital. The patient had a past history of medicated hypertension. Upon arrival at our hospital, the patient's vital signs were as follows:",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Glasgow Coma Scale, 15; blood pressure, 140/100 mmHg; temperature, 39.4 \u00baC; pulse, 104 beats per minute; respiratory rate, 36 breaths per minute; and oxygen saturation, 96% with a non-rebreather mask (15 L/min oxygen). Lung auscultation was unremarkable. Although his C-reactive protein (CRP) level was high (12.45 mg/L), the patient's blood cell count was within the reference range (5800/\u00b5L), with a relatively high percentage of neutrophils (74%) and a low percentage of lymphocytes (15.6%). Chest radiography performed the same day revealed diffuse infiltrates bilaterally, and chest CT scans showed multiple peripherally dominant ground glass opacities (GGOs) with some infiltrating shadows. These findings were similar to those of other",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "April 9, 2020 Letter to the Editor patients with COVID-19 seen at our hospital. The patient was a taxi driver and reported contact with passengers who had a cough. The possibility of COVID-19 was strongly suspected, and polymerase chain reaction (PCR) testing ordered. The patient was immediately admitted to an intensive care unit with infection control zoning, where he was intubated and ventilated to control hypoxia. Our treatment strategy was based on the World Health Organization recommendations for supportive care, including oxygen therapy, fluid management, and antibiotics for secondary bacterial infections (ceftriaxone and azithromycin). 2 On hospital day 5 (illness day 15), the aspirated sputum tested negative for SARS-CoV-2 by PCR analysis. However, we still strongly suspected SARS-CoV-2 infection because the WBC count was normal, the CRP levels remained elevated, the CT findings were consistent with COVID-19, and oxygenation did not improve. On hospital day 6 (illness day 16), the aspirated sputum tested positive for SARS-CoV-2. Oxygenation disturbances and chest X-ray changes persisted and lopinavir/ritonavir (LPV/r; total dose lopinavir 800 mg/ritonavir 200 mg per day) were prescribed. However, the respiratory status did not improve. Therefore, on hospital day 8 (illness day 18), we prescribed ciclesonide inhalant (400 \u00b5g/day) as an anti-inflammatory drug against peripheral inflammatory lesions. By the following day (illness day 19), oxygenation improved, and the patient was weaned gradually from the ventilator. Finally, on hospital day 19 (illness day 29), he was extubated (Figure) . Since then, his respiratory condition remained stable, and he was discharged home on hospital day 34 (illness day 44) after confirming a negative PCR test.",
            "cite_spans": [
                {
                    "start": 651,
                    "end": 652,
                    "text": "2",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [
                {
                    "start": 1611,
                    "end": 1619,
                    "text": "(Figure)",
                    "ref_id": null
                }
            ],
            "section": "Mayo Clinic Proceedings"
        },
        {
            "text": "Chest X-rays of patients with early-stage COVID-19 may show no severe abnormalities in mild or",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Mayo Clinic Proceedings"
        },
        {
            "text": "April 9, 2020 Letter to the Editor severe disease conditions, whereas CT scans reveal peripheral GGOs in severe cases. These findings seem specific to COVID-19. 3 COVID-19 differs from other causes of acute respiratory distress syndrome and viral pneumonias in that it has fewer interstitial changes. Rather, peripheral alveolar injury is more associated with COVID-19. Indeed, no increase in markers of interstitial disorders (such as KL-6, and pulmonary SP-D) and no decrease in lung compliance were observed in COVID-19. Our patient presented similar features, and we suspected COVID-19, despite first PCR test being negative. Although prior treatment with LPV/r failed to improve oxygenation, treatment with ciclesonide coincided with a positive outcome. In fact, the nasopharyngeal swab specimens before and after LPV/r administration were positive for SARS-CoV-2.",
            "cite_spans": [
                {
                    "start": 161,
                    "end": 162,
                    "text": "3",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Mayo Clinic Proceedings"
        },
        {
            "text": "Ciclesonide is an inhaled steroid drug that suppresses asthmatic attacks by decreasing airway hyperresponsiveness as well as the immediate and delayed forms of pulmonary resistance induced by the inhalation of antigens. 4 Ciclesonide suppresses the production of tumor necrosis factor-\u03b1 and cytokines such as interleukin (IL)-4 and IL-5 and inhibits eosinophil infiltration into the respiratory tract. 5 Ciclesonide is a locally activated drug that is converted into the active metabolite desisobutyryl-ciclesonide by hydrolase esterase after inhalation. It binds to the glucocorticoid receptor and produces potent anti-inflammatory effects. Furthermore, desisobutyryl-ciclesonide reversibly binds to a fatty acid to form a fatty acid conjugate, leading to protraction retention in lung tissue; as an aerosol, it has a high percentage of fine particles that can reach the peripheral airways, with a good lung penetration rate of approximately 52%. Ciclesonide has low systemic side effects. 4",
            "cite_spans": [
                {
                    "start": 220,
                    "end": 221,
                    "text": "4",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Mayo Clinic Proceedings"
        },
        {
            "text": "April 9, 2020 Letter to the Editor may be the peripheral alveoli.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Mayo Clinic Proceedings"
        },
        {
            "text": "In conclusion, we report a case of severe COVID-19 pneumonia that was diagnosed correctly based on typical chest CT findings and other clinical features, with a favorable outcome. Our findings suggest that ciclesonide inhalant may improve the respiratory status in severe COVID-19-induced pneumonia and is worthy of further study in clinical trials.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Mayo Clinic Proceedings"
        }
    ],
    "bib_entries": {
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Genomic characterisation and epidemiology of 2019 novel coronavirus: implications for virus origins and receptor binding",
            "authors": [],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "565--574",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Clinical management of severe acute respiratory infection (SARI) when COVID-19 disease is suspected",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Imaging Profile of the COVID-19 Infection: Radiologic Findings and Literature Review",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "Y"
                    ],
                    "last": "Ng",
                    "suffix": ""
                },
                {
                    "first": "Eyp",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Radiology: Cardiothoracic Imaging",
            "volume": "2",
            "issn": "1",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1148/ryct.2020200034"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Ciclesonide: a review of its use in the management of asthma",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "D"
                    ],
                    "last": "Deeks",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Perry",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Drugs",
            "volume": "68",
            "issn": "",
            "pages": "1741--1770",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}