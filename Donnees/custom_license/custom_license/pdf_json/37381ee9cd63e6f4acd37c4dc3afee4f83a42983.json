{
    "paper_id": "37381ee9cd63e6f4acd37c4dc3afee4f83a42983",
    "metadata": {
        "title": "Mechanism of Demyelination in JHM Virus Encephalomyelitis * Electron Microscopic Studies",
        "authors": [
            {
                "first": "Peter",
                "middle": [
                    "W"
                ],
                "last": "Lampert",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of California",
                    "location": {
                        "addrLine": "San Diego, La Jolla",
                        "region": "California"
                    }
                },
                "email": ""
            },
            {
                "first": "Joel",
                "middle": [
                    "K"
                ],
                "last": "Sims",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of California",
                    "location": {
                        "addrLine": "San Diego, La Jolla",
                        "region": "California"
                    }
                },
                "email": ""
            },
            {
                "first": "Alexis",
                "middle": [
                    "J"
                ],
                "last": "Kniazeff",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of California",
                    "location": {
                        "addrLine": "San Diego, La Jolla",
                        "region": "California"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Weanling mice were given intraperitoneal inoculations of the neurotropic, JHM strain of mouse hepatitis virus, the virulence of which had been altered by repeated mouse passages. Five to seven days later many animals developed hind leg paralysis. The pathology consisted of an acute encephalomyelitis with patchy demyclinating lesions in the brain stem and spinal cord. Virus particles, consistent with the appearance of corona viruses, were found in the cytoplasm of cells that were identified as oligondendrocytes by demonstrating connections of their plasma membranes with myelin lamellae. Following the degeneration of oligodendrocytes the myelin sheaths disintegrated or were stripped off intact axons by cytoplasmic tongues of polymorpho-and mononuclear leucocytes that intruded between myelin lamellae. The findings indicate that JHM virus has an affinity for oligodendroeytes in weanling mice and that demyelination occurs subsequently to the degeneration of the infected oligodendrocytes.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Mice infected with the ncurotropie, JHM strain of mouse hepatitis virus develop an acute demyelinating encephalomyelitis (Cheever et al., 1949) . The pathogenesis of demyelination in this disease has been the subject of several light microscopic studies Kersting and Porte, 1956 ; Waksman and Adams, 1962) . Weiner (1972) demonstrated ~hat the induction of predominantly demyelinating lesions is related to virus dosage, inoculation route and age of mice. By means of immunofluoreseence Weiner (1972) revealed the localization of viral antigen within glial cells in ~he white matter. He also showed that immunosuppressive measures enhance rather than prevent the disease suggesting that demyelination occurs as a result of a virus induced destruction of glial cells and not on the basis of an immunopathologic mechanism. The purpose of this communication is to demonstrate that the myelin sustaining oligodendrocytes are indeed damaged by virus infection and that myelin sheaths are removed in a nonspecific manner by means of infiltrating leucocytes that strip myelin lamellae off intact axons.",
            "cite_spans": [
                {
                    "start": 138,
                    "end": 143,
                    "text": "1949)",
                    "ref_id": null
                },
                {
                    "start": 254,
                    "end": 280,
                    "text": "Kersting and Porte, 1956 ;",
                    "ref_id": null
                },
                {
                    "start": 281,
                    "end": 305,
                    "text": "Waksman and Adams, 1962)",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 308,
                    "end": 321,
                    "text": "Weiner (1972)",
                    "ref_id": null
                },
                {
                    "start": 487,
                    "end": 500,
                    "text": "Weiner (1972)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Mice. Specific Pathogen Free Swiss-Webster weanling mice were purchased from :National Animal Laboratory, Creve Coeur, Missouri. A group of these animals were held for thirty days in an isolated facility. The pooled sera of these mice were checked for the presence of mouse * Supported by United States Public Health Research Grant :NS 09053 from the :National Institutes of Neurological Diseases and Stroke. hepatitis antibody using a virus neutralization system employing the JHM strain of mouse hepatitis virus and a transmissible line of mouse liver cells--NCTC 1469. The tests showed the mice to be free of mouse hepatitis antibody.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "Virus. Three isolates of the JHM strain of mouse hepatitis virus were examined in the course of these studies:",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "1. JHM isolate @ 718 was received as a frozen 10~ suspension of weanling mouse brain (National Institute of Health, courtesy of Dr. J. Hartley).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "2. JHM isolate @ 776 was received as a frozen 10~ suspension of a suckling mouse brain (National Institutes of Health, courtesy of Dr. J. Hartley).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "3. JHM isolate ~ 3238/7 was received as frozen tissue culture fluid from Microbiological Associates, Inc., Bethesda, Maryland (courtesy of Dr. Jack Parker).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Materials and Methods"
        },
        {
            "text": "Intraperitoneal inoculation of 0.1 ml of originally received isolates into weanling mice produced no obvious pathology. The three isolates were then blind passaged. The passaging was both by the intracerebral (IC) and intraperitoneal (IP) routes, using 0.03 ml and 0.1 ml of 20~ brain suspensions respectively. The three isolates produced the following results:",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Virus Passages"
        },
        {
            "text": "Isolate 718. IC inoculum produced the death of some animals within 3 to 6 days after the first passage and partial paralysis in IP inoculated animals after the sixth passage. From this passage on the IF inoculation of IC passaged brain suspension consistently produced hind quarter paralysis in some animals within 5 to 7 days.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Virus Passages"
        },
        {
            "text": "Isolate 776. IC inoculation caused death of some animals within 3 to 6 days but no hind quarter paralysis after seven passages.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Virus Passages"
        },
        {
            "text": "Isolate 3238/7. Some animals died 3 to 6 days after IC inoculations. Beginning with passage six IP inoculation of IC passaged brain material produced paralysis in an occasional animal after 10 days.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Virus Passages"
        },
        {
            "text": "For the electron microscopic studies mice inoculated with isolate 718 were chosen because they developed hind leg paralysis and predominantly demyelinating lesions within one week. The weanling mice were inoculated IP with 0.1 ml of a 20O/o brain suspension from mice of passage nine. The animals were sacrificed at daily intervals from 1 to 14 days after inoculation. Fixation of their brains was achieved by perfusion via the heart with a five percent phosphate buffered glutaraldehyde solution. Blocks for electron microscopy were taken from the spinal cord and medulla. They were post fixed in one percent phosphate buffered osmium tetroxide, dehydrated and embedded in Araldite. Thick sections, cut with a Porter Blum microtome, were stained with paraphenylene diamine. Thin sections from selected blocks were cut with diamond knives using the LKB ultrotome III. The sections were examined with a Siemens Elmiscope 101 operating at 80 Kv.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Virus Passages"
        },
        {
            "text": "Tissue not used for electron microscopy as well as the brains from other mice from preceding virus passages were embedded in paraffin. Sections from these blocks were stained with Hematoxylin Eosin, Luxol Fast Blue for myelin and Holmes' stain for axons.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Virus Passages"
        },
        {
            "text": "Light microscopic examination of the brains of mice from earlier virus passages, which died 3 to 6 days after intracerebral inoculation, revealed necrotizing lesions in both grey and white matter. Necrotic areas were particularly prominent in the cortex of the hippocampus. Perivascular infiltrates of predominantly polymorphonuclear cells were noted around the necrotic areas as well as in subependymal regions, choroid plexus and meninges. Weanling mice, which were given intraperitoneal injections of brain suspensions containing virus altered by nine mouse passages, showed lesions with a more striking predilection for the white matter, particularly in the brain stem and spinal cord. These were first observed in mice 5 days after inoculation and were most numerous in animals that showed hind The leucocytic clusters were found at a distance from vessels or in eccentric positions next to vessels rather than as perivenular sleeves. Demyelinated axons were seen within and adjacent to the cellular aggregates ( Fig. 1) . P a t c h y disintegration of white matter tracts with scanty cellular infiltrates were also noted (Fig. 2) . Preservation of axons and nerve cells was conspicuous in areas where the lesions extended into the grey matter. Vascular and glial proliferation occurred around lesions in mice that were sacrificed two weeks after inoculation. Examination of the sciatic nerves and spinal nerve roots showed no changes. The liver of afflicted animals revealed only scanty changes consisting of a few foci of necrosis with small aggregates of mononuclear cells. Electron microscopic studies of the spinal cord and brain stem from paralysed mice showed numerous glial cells containing virus particles. I n the grey matter the virus was often found in satellite cells adjacent to normal nerve cells (Fig. 3) . I n the white matter the virus was discovered in cells with electron dense, granular cytoplasm and in cytoplasmic processes that contained microtubules. I t was possible to clearly identify some of these cells as oligodendrocytes by showing the continuity of their plasma membranes with myelin lamellae (Fig.4) . Virus was also their close relation to myelinated axons suggested that at least some of them were oligodendroeytes. This interpretation was strengthened by the finding of degenerating inner and outer glial loops of myelin sheaths next to degenerating cells.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 1018,
                    "end": 1025,
                    "text": "Fig. 1)",
                    "ref_id": null
                },
                {
                    "start": 1127,
                    "end": 1135,
                    "text": "(Fig. 2)",
                    "ref_id": null
                },
                {
                    "start": 1817,
                    "end": 1825,
                    "text": "(Fig. 3)",
                    "ref_id": null
                },
                {
                    "start": 2131,
                    "end": 2138,
                    "text": "(Fig.4)",
                    "ref_id": null
                }
            ],
            "section": "Results"
        },
        {
            "text": "The cellular infiltrates consisted of polymorphonuelear leucocytes and mononuclear cells. These cells were seen to traverse vessels walls by passing between separated endothelial junctions. The mononuclear cells had abundant cytoplasm and frequently contained myelin debris within membrane bounded compartments. Cytoplasmic projections of these cells invaded myelin sheaths stripping individual lamellae or the entire sheath off intact axons (Fig. 6) . Polymorphonuclear cells filled with glycogen granules participated in the removal of the myelin lamellae. In more advanced lesions numerous, completely demyelinated but otherwise intact axons were found surrounded by many macrophages including multinucleated giant cells filled with myelin debris (Fig. 7) . In other lesions with scanty cellular infiltrates the debris of disintegrated myelin sheaths were located within very wide extracellular spaces. Severely involved areas were characterized by numerous necrotic cells with swollen, watery cytoplasm, very wide extraeellular spaces and degenerating axons. The axonal changes consisted of granular disintegration of neurofilaments within collapsed axoplasm or of enormous enlargements filled with mitochondria and laminated dense bodies. Phagoeytosis of myelin sheaths and degenerated axons was less pronounced in such lesions.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 442,
                    "end": 450,
                    "text": "(Fig. 6)",
                    "ref_id": null
                },
                {
                    "start": 750,
                    "end": 758,
                    "text": "(Fig. 7)",
                    "ref_id": null
                }
            ],
            "section": "Results"
        },
        {
            "text": "Mice that survived 2 weeks after inoculation showed evidence of infection but the lesions were small and few in number. Macrophages containing myelin debris were numerous. Perivascular cuffing with mononuelear cells and meningeal infiltrates were observed. Astroeytic proliferation occurred about demyelinated axons. Occasional axons were invested by thin cytoplasmic projections suggestive of beginning remyelination.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Results"
        },
        {
            "text": "Demyelination in virus encephalomyelitis could conceivably be caused by: 1. Direct cytotoxic effect of virus on myelin sustaining oligodendrocytes; 2. Necrosis of infected oligodendroeytes after contact with virus specific antibodies or sensitized cells; 3. Immune reaction with release of a cytotoxic factor damaging to oligodendrocytes and myelin sheaths; 4. Induction of an autoimmune reaction directed against myelin sheaths.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The following evidence suggests that demyelination in JHM virus encephalomyelitis is caused by a direct effect of virus on oligodendrocytes: 1. Immunosuppression does not prevent the disease (Weiner, 1972 ). 2. Early lesions do not contain immnnoglobnlins (Weiner, 1972) and cellular infiltrates may be scanty or absent . 3. Perivenular demyelination as observed in experimental allergic encephalomyelitis and in human postinfections leucoeneephalitis is not a conspicuous feature (Waksman and Adams, 1962) . 4. Oligodendrocytes are infected by virus as revealed by immunofluoreseence (Weiner, 1972) and herewith confirmed by electron microscopy.",
            "cite_spans": [
                {
                    "start": 191,
                    "end": 204,
                    "text": "(Weiner, 1972",
                    "ref_id": null
                },
                {
                    "start": 256,
                    "end": 270,
                    "text": "(Weiner, 1972)",
                    "ref_id": null
                },
                {
                    "start": 481,
                    "end": 506,
                    "text": "(Waksman and Adams, 1962)",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 585,
                    "end": 599,
                    "text": "(Weiner, 1972)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The stripping of myelin lamellae by phagocytes is a non-specific change. It is seen after degeneration or necrosis of myelin supporting cells or after focal damage to myelin sheaths. For instance, in peripheral nerves, stripping of myelin lamellae has been observed in lead and tellurium neuropathy after degeneration of the myelin sustaining Schwann cells (Lampert and Schochet, 1968a; Lampert et al., 1970) . In the central nervous system the same demyelinating process has been described after barbotage of spinal fluid (Bunge et al., 1960) , after intoxication with isonicotinic acid hydrazide (Lampert and Schochet, 1968b) and in distemper encephalitis (Wisniewski et al., 1972) . Myelin stripping by phagocytes is also a prominent feature in experimental allergic encephalomyelitis and neuritis. In these conditions the myelin sheaths, rather than the myelin sustaining cells, appear to be the target of the immune reaction (Lampert, 1969) . After focal lysis of myelin !amellae, the remaining compact sheath is removed by non-specific macrophages that intrude between myelin lamellae (Lampert, 1967) .",
            "cite_spans": [
                {
                    "start": 357,
                    "end": 386,
                    "text": "(Lampert and Schochet, 1968a;",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 387,
                    "end": 408,
                    "text": "Lampert et al., 1970)",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 523,
                    "end": 543,
                    "text": "(Bunge et al., 1960)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 598,
                    "end": 627,
                    "text": "(Lampert and Schochet, 1968b)",
                    "ref_id": null
                },
                {
                    "start": 658,
                    "end": 683,
                    "text": "(Wisniewski et al., 1972)",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 930,
                    "end": 945,
                    "text": "(Lampert, 1969)",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 1091,
                    "end": 1106,
                    "text": "(Lampert, 1967)",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The appearance of the JHM virus in the cytoplasm of glial cells is consistent with the size and morphology of corona viruses. The name for this group of viruses has been derived from the characteristic fringe or corona, i.e. the superficial, petal shaped projections radiating from the central core of the virus (Almeida et al., 1968) . The cytoplasmic changes of the infected oligodendrocytes were remarkably similar to those studied in vitro in cells infected with another mouse hepatitis virus (David-Ferreira and Manaker, 1965). The infected cells showed an abundance of microtubules as well as aggregates of reticulated, granular, electron dense material possibly representing viral nueleoprotein. These aggregates were found close to smooth endoplasmic retieulum, which contained complete virus particles in dilated cisterns and showed particles budding from its membranes.",
            "cite_spans": [
                {
                    "start": 312,
                    "end": 334,
                    "text": "(Almeida et al., 1968)",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "The electron microscopic identification of oligodendrocytes is usually based on finding cells with dark, granular cytoplasm that contain microtubules but fail to show features characteristic of neurons or astrocytes. These criteria are not satisfactory since other mononuclear cells (microglia) may be confused with oligodendrocytes, particularly in pathologic conditions. Definite proof can be established, however, by demonstrating connections of the ollgodcndroeyte with myelin sheaths, which is readily accomplished in the developing nervous system but rarely in the adult (Itirano, 1968). The fact that mature but infected oligodendrocytes revealed their connections with myelin sheaths may indicate that the damaged cells retracted their processes which would facilitate the visualization of such connections within the same plane of section.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Corona viruses",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "D"
                    ],
                    "last": "Almeida",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "M"
                    ],
                    "last": "Berry",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "H"
                    ],
                    "last": "Cunningham",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "]-Iambre",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "S"
                    ],
                    "last": "I-Iofstad",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Mallucci",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Meintosh",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "A J"
                    ],
                    "last": "Tyrell",
                    "suffix": ""
                }
            ],
            "year": 1968,
            "venue": "",
            "volume": "220",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "A murine virus (JttM) causing disseminated encephalomyelitis with extensive destruction of myelin. II Pathology",
            "authors": [
                {
                    "first": "O",
                    "middle": [
                        "T"
                    ],
                    "last": "Bailey",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "M"
                    ],
                    "last": "Pappenheimer",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "S"
                    ],
                    "last": "Cheever",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "B"
                    ],
                    "last": "Daniels",
                    "suffix": ""
                }
            ],
            "year": 1949,
            "venue": "J. exp. Med",
            "volume": "90",
            "issn": "",
            "pages": "195--212",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Electron microscopic study of demyelination in an experimentally induced lesion in adult cat spinal cord",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "P"
                    ],
                    "last": "Bunge",
                    "suffix": ""
                },
                {
                    "first": "1~",
                    "middle": [
                        "B"
                    ],
                    "last": "Bunge",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Ris",
                    "suffix": ""
                }
            ],
            "year": 1960,
            "venue": "J. biophys, biochem. Cytol",
            "volume": "7",
            "issn": "",
            "pages": "685--696",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "A murine virus (JHM) causing disseminated encephalomyelitis with extensive destruction of myelin. I. Isolation and biological properties of the virus",
            "authors": [
                {
                    "first": "F",
                    "middle": [
                        "S"
                    ],
                    "last": "Cheerer",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "B"
                    ],
                    "last": "Daniels",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "M"
                    ],
                    "last": "Pappenheimer",
                    "suffix": ""
                },
                {
                    "first": ".",
                    "middle": [
                        "T"
                    ],
                    "last": "Bailey",
                    "suffix": ""
                }
            ],
            "year": 1949,
            "venue": "J. exp. IVied",
            "volume": "90",
            "issn": "",
            "pages": "181--194",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "An electron microscope study of the development of a mouse hepatitis virus in tissue culture cells",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "F"
                    ],
                    "last": "David-Fen&apos;eira",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Manaker",
                    "suffix": ""
                }
            ],
            "year": 1965,
            "venue": "J. Cell Biol",
            "volume": "24",
            "issn": "",
            "pages": "57--78",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "A confirmation of the oligodendroglial origin of myelin in the adult rat",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Hirano",
                    "suffix": ""
                }
            ],
            "year": 1968,
            "venue": "J. Cell Biol",
            "volume": "88",
            "issn": "",
            "pages": "687--640",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Zur Pathohistologie und Pathogenese der experimentellen JHlV[ u des Affen",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Kersting",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Pette",
                    "suffix": ""
                }
            ],
            "year": 1956,
            "venue": "Dtsch. Z. Nervenheilk",
            "volume": "174",
            "issn": "",
            "pages": "238--304",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Electron microscopic studies on ordinary and hyperaeute experimental allergic encephalomyelitis",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lampert",
                    "suffix": ""
                }
            ],
            "year": 1967,
            "venue": "Aeta neuropath. (BEE.)",
            "volume": "9",
            "issn": "",
            "pages": "99--126",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Demyelination and remyelination in lead neuropathy",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lampert",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Schochet",
                    "suffix": ""
                }
            ],
            "year": 1968,
            "venue": "J. Neuropath. exp. Neurol",
            "volume": "27",
            "issn": "",
            "pages": "527--545",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Electron microscopic observations on experimental spongy degeneration of the cerebellar white matter",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lamper~",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Schochet",
                    "suffix": ""
                }
            ],
            "year": 1968,
            "venue": "J. Neuropath. exp. Neurol",
            "volume": "27",
            "issn": "",
            "pages": "210--220",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Mechanism of demyelination in experimental allergic neuritis",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lampert",
                    "suffix": ""
                }
            ],
            "year": 1969,
            "venue": "Lab. Invest",
            "volume": "22",
            "issn": "",
            "pages": "127--138",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Tellurium neuropathy",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Lampert",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Garro",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Pentschew",
                    "suffix": ""
                }
            ],
            "year": 1970,
            "venue": "Acta neuropath. (Berl.)",
            "volume": "15",
            "issn": "",
            "pages": "308--317",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Infectious leukoencephalitis. A critical comparison of certain experimental and naturally occurring viral leuko-encephalitides with experimental allergic encephalomyelitis",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Waksman",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "D"
                    ],
                    "last": "Adams",
                    "suffix": ""
                }
            ],
            "year": 1962,
            "venue": "J. l~europath, exp. l~eurol",
            "volume": "21",
            "issn": "",
            "pages": "491--518",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Pathogenesis of demyelination in mice induced by neurotropic mouse hepatitis virus",
            "authors": [
                {
                    "first": "L",
                    "middle": [
                        "P"
                    ],
                    "last": "Weiner",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Arch. Neurol",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Observations in viral demyelinating encephalomyelitis. Canine distemper",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Wisniewski",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "S"
                    ],
                    "last": "Raine",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "J"
                    ],
                    "last": "Kay",
                    "suffix": ""
                }
            ],
            "year": 1972,
            "venue": "Lab. Invest",
            "volume": "26",
            "issn": "",
            "pages": "589--599",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Small demyelinated area in the spinal cord of a mouse 7 days after virus inoculation. Paraphenylene diamine, \u2022 Patchy subpial areas of demyelination in the spinal cord of a mouse 10 days after virus inoculation. A multinucleated giant cell is indicated by the arrow. Luxol Fast Blue, X 200 leg paralysis. The lesions consisted of clusters of inflammatory cells containing polymorpho-and mononuclear leucocytes as well as multinucleated giant cells.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "A satellite ceil next to a neuron contains numerous virus particles within cytoplasmic vacuoles. Aggregates of granular material as well as numerous microtubules are seen in the cytoplasm of the infected cell. Spinal cord, 6 days after virus inoculation, X 25000 seen in the cytoplasm of astrocytes recognizable b y the presence of characteristic bundles of glial filaments. The virus particles were found within m e m b r a n e bounded vacuoles and within or budding into dilated cisterns of smooth endoplasmic reticulum. The circular profiles averaged a b o u t 80 n m in width, had a central, electronlucent core and an outer surface studded with 20 n m long projections(Fig.3). The infected cells were otherwise conspicuous b y unusually numerous microtubules and a b u n d a n t branching cisterns of smooth endoplasmic reticulum. A n o t h e r outstanding cytoplasmic feature was the presence of large aggregates of Oligodendroeyte containing intracytoplasmic virus particles (upper left) associated with aggregates of granular, electron dense material, \u2022 15 000. The inset magnifies the outlined portion (lower left) revealing the connection of the oligodendroeyte with a myelin sheath. Note continuity of a myelin lamella with the plasma membrane of the oligodendrocyte (arrow in inset). Spinal cord, 6 days after virus inoculation, X 100000 reticular, electron dense material in close p r o x i m i t y to virus containing vacuoles(Fig.4). Necrotic cells were seen within and b e y o n d cellular infiltrates(Fig. 5). Some of t h e m contained virus particles. Their i d e n t i t y was difficult to establish butFig. 5. Necrotic cell, possibly an oligodendrocyte, within the white matter of the spinal cord in a mouse 6 days after virus inoculation, X 20000",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Myelin stripping macrophage showing cytoplasmic processes that intrude between myelin lamellae (~rrows). Spin~l cord, 7 days after virus inoculation, X Completely demyelinated but otherwise intact axons surrounded by macrophages filled with: myelin debris. Spinal cord, 11 days after virus inoculation, x 5000",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}