{
    "paper_id": "18df566482938a71153652c598fb559a5ba24d84",
    "metadata": {
        "title": "Journal Pre-proof A potential inhibitory role for integrin in the receptor targeting of SARS-CoV-2",
        "authors": [
            {
                "first": "Junwen",
                "middle": [],
                "last": "Luan",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Yue",
                "middle": [],
                "last": "Lu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Shan",
                "middle": [],
                "last": "Gao",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Leiliang",
                "middle": [],
                "last": "Zhang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Integrins are heterodimeric proteins comprising \u03b1 and \u03b2 subunits in cell surface. Many integrins recognize Arg-Gly-Asp (RGD) and Lys-Gly-Asp (KGD) motifs which are displayed on the exposed loops of proteins. RGD could associate broader types of integrins such as \u03b1V\u03b21, \u03b1V\u03b23, \u03b1V\u03b25, \u03b1V\u03b26, \u03b1V\u03b28, \u03b1\u2161b\u03b23, \u03b1M\u03b22, \u03b1L\u03b22 and \u03b13\u03b21, while KGD-recognizing integrins are restricted to \u03b1\u2161b\u03b23, \u03b1V\u03b25, \u03b1V\u03b26 and \u03b1V\u03b28 (2) . A recent study proposed a proviral role for integrins in SARS-CoV-2 entry (3). In current study, we found RGD/KGD motif presents not only in S protein but also in its receptor ACE2.",
            "cite_spans": [
                {
                    "start": 398,
                    "end": 401,
                    "text": "(2)",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "We suggested inhibitory roles for integrins in the entry of both SARS-CoV-2 and SARS-CoV.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "RGD and KGD are two classical integrin-binding motifs. First, we performed sequence analysis of S proteins from SARS-CoV-2 (YP_009724390.1) and SARS-CoV (NP_828851.1). An RGD motif (403-405) was identified in SARS-CoV-2 S protein ( Figure 1A ). In SARS-CoV S protein, there is a KGD motif (390-392) ( Figure 1A ). By searching NCBI databases and literatures, we identified similar RGD/KGD motifs in several coronaviruses including Pangolin/MP789/2019 (4), Pangolin/GX/P5L/2017 (4),",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 232,
                    "end": 241,
                    "text": "Figure 1A",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 301,
                    "end": 310,
                    "text": "Figure 1A",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": ""
        },
        {
            "text": "Bat_SARSr-CoV_Rs3367 (AGZ48818.1). These results suggested that RGD/KGD integrin-binding motif is conserved in several coronaviruses including SARS-CoV-2 and SARS-CoV.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In order to associate with integrin, RGD/KGD motif should be exposed to the surface of the protein. To investigate whether RGD/KGD motif in S proteins from SARS-CoV-2",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "and SARS-CoV is on the surface, we analyzed the structures of SARS-CoV-2 S trimer (PDB: 6VSB) (5) and SARS-CoV S trimer (PDB: 5XLR) (6) by Chimera software Ver 1.14. RGD located at the top of protrusions in SARS-CoV-2 S, and KGD is displayed on exposed loop in SARS-CoV S ( Figure 1B and 1C) . These results suggested S proteins are accessible to integrins.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 274,
                    "end": 291,
                    "text": "Figure 1B and 1C)",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": ""
        },
        {
            "text": "RGD/KGD motif in S protein is close to RBM of S protein. Next, we checked the structure of ACE2 with SARS-CoV-2 S RBD (PDB: 6LZG) and ACE2 with SARS-CoV S RBD (PDB: 2AJF) (7) by Chimera software Ver 1.14. RGD/KGD in S protein is near the groove of complex of RBD and ACE2 ( Figure 1D and 1E ). If S protein associates with integrin, there would be no space for ACE2 to contact with S.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 274,
                    "end": 290,
                    "text": "Figure 1D and 1E",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": ""
        },
        {
            "text": "ACE2 is known to associate with integrin. An RGD sequence (203-205) in ACE2 is partially buried inside the protein and thus not responsible for integrin association (8) .",
            "cite_spans": [
                {
                    "start": 165,
                    "end": 168,
                    "text": "(8)",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "We identified a KGD motif in 353-355 of ACE2 (BAB40370.1), which is a key region for binding S protein ( Figure 1F ). This KGD motif is on the exposed small loop in the structure of open ACE2 dimer (PDB: 6M1D) and closed ACE2 dimer (PDB: 6M18) (9), which indicated that integrin could interact with ACE2 through this motif ( Figure 1G ).",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 105,
                    "end": 114,
                    "text": "Figure 1F",
                    "ref_id": "FIGREF1"
                },
                {
                    "start": 325,
                    "end": 334,
                    "text": "Figure 1G",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": ""
        },
        {
            "text": "We proposed the following model for the role of integrin in receptor recognition of S protein ( Figure 2 ). Upon SARS-CoV-2 or SARS-CoV infection, integrin could interact with ACE2 and S protein individually. Integrin associates with ACE2 through its KGD motif including K353, which is one of key AAs for S protein recognition. Integrin associates with S protein by its RGD/KGD motif, which would shield the space of RBM for contacting with ACE2. In those scenarios, both ACE2 K353 (inside KGD) and S protein RBM (near RGD/KGD) are shielded by integrin. Thus, ACE2 could not recognize S protein, leading to a suppressed viral entry. In the condition of ACE2 targeting of S protein, integrin no longer blocks ACE2-S interaction, resulting in a robust virus entry.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 96,
                    "end": 104,
                    "text": "Figure 2",
                    "ref_id": "FIGREF2"
                }
            ],
            "section": ""
        },
        {
            "text": "Several host proteins were predicted to associate with SARS-CoV-2 S protein (3, 10).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Recently, people reported that the SARS-CoV-2 S protein acquired an RGD integrinbinding motif and claimed that this motif was absent from other coronaviruses (3). They further speculated that RGD acquired by SARS-CoV-2 would promote virus entry by association of integrins, thus enhance the transmission ability. We disagree with that because both RGD and KGD could recognize integrins. The difference is that RGD recognizes more types of integrins than KGD. When integrins bind to S protein, the stereo-hindrance effect will prevent ACE2 targeting by S protein. Moreover, ACE2",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "contains a KGD integrin-binding motif inside the S-binding region. When integrins bind to S protein, ACE2 targeting by S protein will also be inhibited. Taken together, our model favors an inhibitory role for integrins in virus entry by associating with both S protein and ACE2.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Potential association of S protein and integrins provide mechanistic insights for the pathogenesis of SARS-CoV-2 and SARS-CoV. Because RGD recognized a broader spectrum of integrins than KGD, more integrins could block receptor binding of SARS-CoV-2 S than that of SARS-CoV S. Consequently, SARS-CoV-2 would infect fewer organs than SARS-CoV, which might partially explain why SARS-CoV-2 caused less mortality than SARS-CoV.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "In conclusion, we identified an RGD/KGD integrin-binding motif in S proteins from SARS-CoV-2 and SARS-CoV. We also discovered a KGD integrin-binding motif in ACE2. Integrins were predicted to inhibit receptor targeting of S proteins from SARS-CoV-2 and SARS-CoV by shielding both S protein and ACE2. We proposed a previous unappreciated inhibitory role for integrin in virus entry, which will improve our understanding on the virus entry for both SARS-CoV-2 and SARS-CoV.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The authors declare that there are no conflicts of interest. In uninfected condition, integrin could associate with ACE2 through KGD motif. Upon SARS-CoV-2 or SARS-CoV infection, integrin could interact with ACE2 (through KGD) and S protein (through RGD/KGD) individually, which will mask the interface between S protein and ACE2. Thus, ACE2 could not recognize S protein, leading to a suppressed viral entry. When ACE2 associates with S protein, a robust virus entry will occur.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflict of interest"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Public health might be endangered by possible prolonged discharge of SARS-CoV-2 in stool",
            "authors": [
                {
                    "first": "Y",
                    "middle": [],
                    "last": "He",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Infect",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jinf.2020.02.031"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "From Discovery of Snake Venom Disintegrins to A Safer Therapeutic Antithrombotic Agent",
            "authors": [
                {
                    "first": "Y",
                    "middle": [
                        "J"
                    ],
                    "last": "Kuo",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "H"
                    ],
                    "last": "Chung",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "F"
                    ],
                    "last": "Huang",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "Toxins (Basel)",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "A potential role for integrins in host cell entry by SARS-CoV-2",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "J"
                    ],
                    "last": "Sigrist",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Bridge",
                    "suffix": ""
                },
                {
                    "first": "Le",
                    "middle": [],
                    "last": "Mercier",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Antiviral Res",
            "volume": "177",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Identification of 2019-nCoV related coronaviruses in Malayan pangolins in southern China",
            "authors": [
                {
                    "first": "Tt-Y",
                    "middle": [],
                    "last": "Lam",
                    "suffix": ""
                },
                {
                    "first": "Mh-H",
                    "middle": [],
                    "last": "Shum",
                    "suffix": ""
                },
                {
                    "first": "H-C",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "Y-G",
                    "middle": [],
                    "last": "Tong",
                    "suffix": ""
                },
                {
                    "first": "X-B",
                    "middle": [],
                    "last": "Ni",
                    "suffix": ""
                },
                {
                    "first": "Y-S",
                    "middle": [],
                    "last": "Liao",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wei",
                    "suffix": ""
                },
                {
                    "first": "Wy-M",
                    "middle": [],
                    "last": "Cheung",
                    "suffix": ""
                },
                {
                    "first": "W-J",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "L-F",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "M"
                    ],
                    "last": "Leung",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "C"
                    ],
                    "last": "Holmes",
                    "suffix": ""
                },
                {
                    "first": "Y-L",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Guan",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1101/2020.02.13.945485%JbioRxiv:2020.02.13.945485"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Cryo-EM structure of the 2019-nCoV spike in the prefusion conformation",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wrapp",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "S"
                    ],
                    "last": "Corbett",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Goldsmith",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "L"
                    ],
                    "last": "Hsieh",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [],
                    "last": "Abiona",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "S"
                    ],
                    "last": "Graham",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Mclellan",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Science",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1126/science.abb2507"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Cryo-electron microscopy structures of the SARS-CoV spike glycoprotein reveal a prerequisite conformational state for receptor binding",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Gui",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Xiang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "Cell Res",
            "volume": "27",
            "issn": "",
            "pages": "119--129",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Structure of SARS coronavirus spike receptor-binding domain complexed with receptor",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Farzan",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "C"
                    ],
                    "last": "Harrison",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Science",
            "volume": "309",
            "issn": "",
            "pages": "1864--1872",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Angiotensin converting enzyme (ACE) and ACE2 bind integrins and ACE2 regulates integrin signalling",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "E"
                    ],
                    "last": "Clarke",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Fisher",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "E"
                    ],
                    "last": "Porter",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "W"
                    ],
                    "last": "Lambert",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Turner",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "PLoS One",
            "volume": "7",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Structural basis for the recognition of the SARS-CoV-2 by full-length human ACE2",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Yan",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Xia",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Guo",
                    "suffix": ""
                },
                {
                    "first": "Q",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Science",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1126/science.abb2762"
                ]
            }
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "COVID-19 spikehost cell receptor GRP78 binding site prediction",
            "authors": [
                {
                    "first": "I",
                    "middle": [
                        "M"
                    ],
                    "last": "Ibrahim",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "H"
                    ],
                    "last": "Abdelmalek",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "E"
                    ],
                    "last": "Elshahat",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "A"
                    ],
                    "last": "Elfiky",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Infect",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jinf.2020.02.026"
                ]
            }
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "the pathogen of COVID-19. Both SARS-CoV-2 and SARS-CoV belong to the subgenus sarbecvirus of Coronaviridae. One major structural protein Spike (S) has attracted great attention because of its function in recognizing receptor, angiotensinconverting enzyme 2 (ACE2). ACE2 binds to the receptor-binding motif (RBM) in the receptor-binding domain (RBD) of SARS-CoV-2 and SARS-CoV. Although ACE2 is distributed in many organs, lung is the major target of both SARS-CoV-2 and SARS-CoV. SARS-CoV-2 tends to infect fewer organs than SARS-CoV. For instance, COVID-19 patients showed less diarrhea than SARS patients (1). The underlying mechanism remains inconclusive.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Identification of integrin-binding motifs in S proteins from SARS-CoV-2/ SARS-CoV and ACE2 protein. (A) RGD/KGD is present in S protein from SARS-CoV-2, SARS-CoV, pangolin coronaviruses, and some bat coronaviruses. RGD/KGD motif is in red box. (B) RGD is in an exposed loop of SARS-CoV-2 S protein. Left: trimer of SARS-CoV-2 S protein (PDB: 6VSB). Right: monomer of SARS-CoV-2 S protein. RGD motif is colored red. (C) KGD is in an exposed loop of SARS-CoV S protein. Left: trimer of SARS-CoV S protein (PDB: 5XLR). Right: monomer of SARS-CoV S protein. KGD motif is colored red. (D) RGD of SARS-CoV-2 S protein locates near the groove of the complex of ACE2 and SARS-CoV-2 RBD (PDB: 6LZG). Red: RGD motif. Grey: ACE2, Green: SARS-CoV-2 RBD. (E) KGD of SARS-CoV S protein locates near the groove of the complex of ACE2 and SARS-CoV RBD (PDB: 2AJF). Red: KGD motif. Grey: ACE2, Green: SARS-CoV RBD. (F) KGD presents in human ACE2. KGD motif is in red box. (G) KGD locates in an exposed loop of ACE2. Left: dimer of full-length human ACE2 in complex with B 0 AT1. Right: monomer of human ACE2. Upper: open conformation (PDB: 6M1D). Lower: closed conformation (PDB: 6M18). KGD motif is colored red.",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "Proposed model for the inhibitory role of integrin in SARS-CoV-2 andSARS-CoV.",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}