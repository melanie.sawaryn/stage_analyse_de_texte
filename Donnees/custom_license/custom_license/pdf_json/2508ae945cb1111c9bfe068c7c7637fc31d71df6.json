{
    "paper_id": "2508ae945cb1111c9bfe068c7c7637fc31d71df6",
    "metadata": {
        "title": "-NC-ND 4.0 license",
        "authors": [
            {
                "first": "Diana",
                "middle": [
                    "Margot"
                ],
                "last": "Rosenthal",
                "suffix": "",
                "affiliation": {},
                "email": "diana.rosenthal@ucl.ac.uk"
            },
            {
                "first": "Marcella",
                "middle": [],
                "last": "Ucci",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Michelle",
                "middle": [],
                "last": "Heys",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Andrew",
                "middle": [],
                "last": "Hayward",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Monica",
                "middle": [],
                "last": "Lakhanpaul",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "There is no doubt that coronavirus disease 2019 (COVID-19) has huge economic implications as highlighted by the media, but there are also a myriad of considerable direct and indirect health, social, and educational consequences for children and families experiencing homelessness, while living in temporary or insecure accommodation (eg, staying with friends or family, sofa surfing, shelters, bed and breakfast lodging).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Impacts of COVID-19 on vulnerable children in temporary accommodation in the UK"
        },
        {
            "text": "In particular, young children (aged \u22645 years) living in temporary accommodation have an invisible plight that might not seem obvious to many people because they are not on the streets as homeless (eg, rough sleepers), but are perhaps the most susceptible to viral infection because of pre-existing conditions (eg, diabetes, asthma, epilepsy, anxiety, depression). 1 Additionally, these children rarely have the ability to self-isolate and adhere to social distancing, with previous extreme inequalities and inequities in accessing health care becoming exacerbated. In 2019, the charity Shelter reported that a child loses their home every 8 min in Great Britain, which is the equivalent of 183 children per day. 2 The total number of children who were homeless and in temporary accommodation increased to 126 020 in England in 2019, of whom 88 080 were in London. 2 The Children's Commissioner suggested that there could be more than 210 000 homeless children in temporary accommodation or sofa surfing and approximately 585 000 who are either homeless or at risk of becoming homeless in England. 3 How do these children cope during this pandemic?",
            "cite_spans": [
                {
                    "start": 712,
                    "end": 713,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 864,
                    "end": 865,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1097,
                    "end": 1098,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Impacts of COVID-19 on vulnerable children in temporary accommodation in the UK"
        },
        {
            "text": "Homeless children aged 5 years and younger are not only at high risk of exposure and transmission due to overcrowding in substandard housing, but also of immediate and long-term effects on growth, optimal health, and brain development. According to UNICEF, \"The first 1000 days can shape a child's future. We have one chance to get it right\", 4 which also extends to the first 5 years. Many children already do not reach development potential or struggle to grow and develop because of multilevel barriers, including those resulting from poverty or homelessness. However, COVID-19 has added a whole new layer of risk.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Impacts of COVID-19 on vulnerable children in temporary accommodation in the UK"
        },
        {
            "text": "First, many families have to live in a single bedroom with shared kitchen and toilet facilities, causing overcrowding and making self-isolation impossible in confined spaces. 5 Often, children have inadequate space to crawl or play and no access to fresh air. Second, no regulation on temporary accommodation exists regarding what is deemed suitable or how long someone can stay in temporary housing. With COVID-19, these children will stay in temporary accommodation for extended periods because no applications or services are being processed or provided. Third, handwashing and hygiene are reduced because of minimal access to soap, water, disinfectants, and bathrooms. Another issue is that no face-to-face contact with general practitioners and health outreach services is available, including health visitors, which limits routine checks such as early identification of need and risk, health and development reviews with screening assessments, immunisations, promotion of social and emotional development, support for parenting, promotion of health and behavioural change, prevention of obesity, and promotion of breastfeeding. 6 Furthermore, for these families, access to basic essentials (eg, food, nappies) is scarce, with no resources to shop online and many charities and dropins now closed. Additionally, they do not have regular support services (eg, legal advice, weekly allowances, housing or immigration, online access to resources from the National Health Service via WiFi). A further consideration is that risks to parental mental health are increased, especially among single mothers, given that housing instability is associated with an increased risk of depression in mothers. 7 Finally, existing vulnerabilities and risk factors for safeguarding are exacerbated by additional factors introduced by the pandemic.",
            "cite_spans": [
                {
                    "start": 175,
                    "end": 176,
                    "text": "5",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1134,
                    "end": 1135,
                    "text": "6",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1698,
                    "end": 1699,
                    "text": "7",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "Impacts of COVID-19 on vulnerable children in temporary accommodation in the UK"
        },
        {
            "text": "Because the duration of the outbreak is unclear and these children are more vulnerable to both primary and secondary effects, it is absolutely vital that they are not further marginalised. The UK Government needs to take necessary steps and work collaboratively with all sectors, health services, and the housing sector (eg, possibly use hotels with space availability due to no incoming tourists) to reduce overcrowding and transmission of COVID-19 to protect some of the most vulnerable in our society.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Impacts of COVID-19 on vulnerable children in temporary accommodation in the UK"
        },
        {
            "text": "We declare no competing interests.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Impacts of COVID-19 on vulnerable children in temporary accommodation in the UK"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Slopes and cliffs in health inequalities: comparative morbidity of housed and homeless people",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Story",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "Lancet",
            "volume": "382",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Generation homeless: the numbers behind the story",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Reynolds",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Dzalto",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Bleak houses: tackling the crisis of family homelessness in England",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Children&apos;s Commissioner",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "UNICEF. Early moments matter",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Self-isolation? Try it as a homeless family living in one room",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Garvie",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Healthy child programme: pregnancy and the first five years of life",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Department",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Health",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Timing of housing crises: impacts on maternal depression",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Marcal",
                    "suffix": ""
                }
            ],
            "year": 2018,
            "venue": "Soc Work Ment Health",
            "volume": "16",
            "issn": "",
            "pages": "266--83",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Comment www.thelancet.com/public-health Published online March 31, 2020 https://doi.org/10.1016/S2468-2667(20)30080-3 1",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}