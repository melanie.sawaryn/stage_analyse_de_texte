{
    "paper_id": "a785255e17c5c1a5a33550a537083105e5551f7d",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Letter to the Editor Effective strategies to prevent coronavirus disease-2019 (COVID-19) outbreak in hospital Sir, Since a novel coronavirus was first identified in Wuhan, Hubei province in China in December 2019, the number of cases has increased abruptly, with the infection spreading rapidly to other Chinese cities and to more than two dozen countries around the world [1] . As of February 23 rd , 2020, 26 cases (16 imported and 10 indigenous) of COVID-19 had been diagnosed in Taiwan [2] . Notably, hospital-associated transmission of COVID-19 to healthcare workers and hospitalized patients has been reported [3] . Furthermore, Zou et al. revealed that similar viral loads are seen in asymptomatic and symptomatic patients, indicating the transmission potential of asymptomatic patients [4] . Herein, we report a patient who presented to an outpatient clinic of Kaohsiung Chang Gung Memorial Hospital (KCGMH) and who, two days later, was confirmed to have COVID-19 infection. We provide valuable strategies which can be effectively implemented to prevent the transmission of COVID-19 in healthcare settings, as well as measures to contain future hospital outbreaks.",
            "cite_spans": [
                {
                    "start": 373,
                    "end": 376,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 490,
                    "end": 493,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 616,
                    "end": 619,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 794,
                    "end": 797,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A 59-year-old Taiwanese businessman who worked in Wuhan, China, returned to Taiwan on January 21 st , 2020. On arrival he managed to pass through fever screening at the airport by taking antipyretic medication. On January 22 nd he attended KCGMH dermatology outpatient clinic with tinea cruris. On the following day he sought medical attention at an external otolaryngology clinic because of worsening respiratory illness. On the next day, a throat swab from the patient was reported as testing positive for COVID-19. The patient was admitted to hospital for management. Contact tracing was immediately started at KCGMH and all medical personnel who had been exposed to the affected patient were quarantined. At the time of writing, the patient remained in hospital, but his clinical condition had improved. None of the quarantined individuals had become infected.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "KCGMH is one of the most reputed medical centres in Taiwan, with 2500 inpatient beds and 5500 staff. The exposure and possible infection of healthcare workers poses a massive challenge to the delivery of medical services at KCGMH. In response to such a situation, strategies to contain the spreading of COVID-19 in hospital are crucial. Past experience with the 2003 severe acute respiratory distress syndrome showed that early identification of suspected cases and rapid implementation of measures are critical to preventing or containing outbreaks in hospital [5] .",
            "cite_spans": [
                {
                    "start": 562,
                    "end": 565,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Infrared thermal camera scanning was introduced at hospital entrances and in the emergency department to recognize any persons (including visitors) with fever at first point of entry ( Figure 1A) . Those identified by scanning have their temperature confirmed with a tympanic thermometer, and a travel and contact history is taken by healthcare workers using a standard checklist. On January 31 st , 2020 Taiwanese health authorities announced that individual immigration data can be accessed by national health insurance cards. This strategy strengthens the information about the personal travel history, and helps close the loophole that people do not disclose their travel histories due to fear of being forcibly isolated. A person having fever, regardless of respiratory illness, and who had a history of traveling to China or Hong Kong/Macau in 14 days prior to symptom onset, or close contact with a confirmed COVID-19 case, is prohibited from entering the hospital, and is directed to the emergency department for isolation in a negatively pressurized room or an outdoor quarantine station ( Figure 1B) for evaluation and management.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 185,
                    "end": 195,
                    "text": "Figure 1A)",
                    "ref_id": "FIGREF0"
                },
                {
                    "start": 1099,
                    "end": 1109,
                    "text": "Figure 1B)",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "All inpatients are carefully assessed, and any travel history to China or Hong Kong/Macau, or history of close contact with laboratory-confirmed COVID-19 in the preceding 14 days, is ascertained. Additionally, a visitor policy, including maintaining a visitor log and limiting visitor numbers, has been implemented. Access control is essential to avoid overcrowding in hospital: only those hospital entrances essential to the effective movement of personnel have been kept open. Furthermore, to reduce the density of patients in outpatient departments, an outdoor pharmacy service was established for the regular maintenance prescription of chronic conditions. The importance of timely education and training of hospital staff including physicians, nurses, laboratory personnel, ambulance paramedics, administrative and other staff are crucial for containing the spread of COVID-19. Also, an emergency response team to organize human resources as well as financial and physical resources was established.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "So far, 147 suspected cases due to COVID-19 have been isolated at KGCMH, and no nosocomial cases have occurred. This is against a background of a total of 77,794 cases (including 76,392 in China) globally, among whom 2359 have died [6] . Failure to identify COVID-19 cases in a timely fashion can paralyse a medical centre, disrupt public health systems, and cause huge economic losses [7, 8] . It is unclear which interventions may eventually control this global COVID-19 outbreak, but sharing of experiences and measures to contain COVID-19 in hospitals is paramount.",
            "cite_spans": [
                {
                    "start": 232,
                    "end": 235,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 386,
                    "end": 389,
                    "text": "[7,",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 390,
                    "end": 392,
                    "text": "8]",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "None declared.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflict of interest statement"
        },
        {
            "text": "None.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Funding sources"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "A novel coronavirus from patients with pneumonia in China",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Song",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "",
            "pages": "727--760",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Novel coronavirus 2019-nCoV outbreak",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Clinical characteristics of 138 hospitalized patients with 2019 novel coronavirusinfected pneumonia in Wuhan, China",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "JAMA",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1001/jama.2020.1585"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "SARS-CoV-2 viral load in upper respiratory specimens of infected patients",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Zou",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Ruan",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Liang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Hong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "N Engl J Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1056/NEJMc2001737"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Epidemiologic study and containment of a nosocomial outbreak of severe acute respiratory syndrome in a medical center in Kaohsiung",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "W"
                    ],
                    "last": "Liu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "N"
                    ],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "S"
                    ],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [
                        "D"
                    ],
                    "last": "Yang",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "C"
                    ],
                    "last": "Lin",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "C"
                    ],
                    "last": "Wu",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Infect Control Hosp Epidemiol",
            "volume": "27",
            "issn": "",
            "pages": "466--72",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "World Health Organization. Coronavirus disease (COVID-2019) situation reports",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Coronavirus covid-19 has killed more people than SARS and MERS combined, despite lower case fatality rate",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "Mahase",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "BMJ",
            "volume": "368",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "A novel coronavirus outbreak of global health concern",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "W"
                    ],
                    "last": "Horby",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "G"
                    ],
                    "last": "Hayden",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "F"
                    ],
                    "last": "Gao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "470--473",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Infrared thermal camera scanning and screening of immigration data by national health insurance cards at hospital entrance (A); outdoor quarantine station at emergency department (B).",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}