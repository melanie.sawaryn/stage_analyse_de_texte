{
    "paper_id": "5158a72d5d578309de182310e1eaf1b6608c762c",
    "metadata": {
        "title": "",
        "authors": []
    },
    "abstract": [],
    "body_text": [
        {
            "text": "In adult patients with respiratory failure refractory to conventional treatment, ECMO represents a potentially lifesaving option, and the CESAR trial indeed indicated that signifi cantly more patients with severe ARDS survived without severe disability if they were transferred to a single ECMO center compared with patients who were managed conventionally at remote hospitals [ 1 , 2 ] . Nevertheless, several questions still remain to be considered when fi guring out a national ECMO network with a structured interhospital transport.",
            "cite_spans": [
                {
                    "start": 377,
                    "end": 386,
                    "text": "[ 1 , 2 ]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "ECMO is a supportive therapy that ensures gas exchange and systemic perfusion, sustains the life of the patient when lung function and the native heart are dangerously compromised, and therefore should be considered to facilitate safe transfer from outlying hospitals to referral centers.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Interhospital transportation of critically ill patients to referral centers is required when local resources and technology are insuffi cient for adequate management. Moreover, many patients requiring transfer are often too unstable to undergo conventional transport [ 3 ] . Cannulation is performed on site by the retrieving team, and the patient is stabilized before transportation. The process requires a specialized team and resources dedicated to retrieval. Therefore, as ECMO is an invasive, intensive form of support, it requires considerable institutional commitment. Consequently, its use is advocated only in those patients believed to be at substantial risk of death.",
            "cite_spans": [
                {
                    "start": 267,
                    "end": 272,
                    "text": "[ 3 ]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "In Australia and New Zealand, during the 2009 infl uenza A(H1N1) winter pandemic, there was a large increase in the use of ECMO for ARDS in patients compared with the winter of 2008, which was predominantly explained by the high M. G. Calabr\u00f2 (*) \u2022 F. Pappalardo [ 4 ] .",
            "cite_spans": [
                {
                    "start": 252,
                    "end": 262,
                    "text": "Pappalardo",
                    "ref_id": null
                },
                {
                    "start": 263,
                    "end": 268,
                    "text": "[ 4 ]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Establishing explicit criteria for patient selection, timing of ECMO initiation, and optimal and safe application are fi rst steps toward the validity of ECMO for adults with ARDS.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "Could a network organization based on preemptive patient centralization allow a higher survival rate of patients with severe ARDS?",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The recent epidemics, severe acute respiratory syndrome (SARS) and pandemic infl uenza A H1N1, have highlighted the potential for respiratory viral infections to cause severe disease with a signifi cant risk of mortality. However, several other viruses cause signifi cant respiratory morbidity annually and have the potential to produce epidemics [ 5 ] .",
            "cite_spans": [
                {
                    "start": 347,
                    "end": 352,
                    "text": "[ 5 ]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Network Organization"
        },
        {
            "text": "Mortality can be reduced by adequate preparation, preventive measures, and specifi c plans for the organization of ICU services. The European Society of Intensive Care Medicine Task Force suggested recommendations and standard operating procedures for the ICUs [ 6 ] .",
            "cite_spans": [
                {
                    "start": 261,
                    "end": 266,
                    "text": "[ 6 ]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Network Organization"
        },
        {
            "text": "In 2009, the Italian Ministry of Health established a national network of selected ICU centers, the Extracorporeal Membrane Oxygenation Network (ECMOnet), and ensured economical, human, and technological resources. Two competent physicians guided ECMOnet organization and development. The ECMOnet organization is offi cially operational since November 5, 2009. The Italian network was set up to centralize all potentially severe patients in a limited number of tertiary hospitals to provide advanced treatment options including ECMO and identify predictors of mortality in order to defi ne the best timing of ECMO institution.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Network Organization"
        },
        {
            "text": "The network consisted of 14 ICUs with ECMO capability and a national call center. The ICU centers were selected based on their (1) experience in treating ARDS patients, (2) experience in respiratory ECMO or presence of a cardiac surgery team expert in ECMO, and (3) territorial distribution. Five centers ensured the interhospital transport through the whole Italian territory whenever the nearest ECMOnet center could not handle a case. The national ECMOnet Call Center Service screened all requests from any hospital in Italy and directed them to the closest ECMOnet center and/or to the transportation ECMO team.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Network Organization"
        },
        {
            "text": "Sessions of ECMO training course, open to physicians, perfusionists, and nurses of the ECMOnet, were organized [ 7 ] .",
            "cite_spans": [
                {
                    "start": 111,
                    "end": 116,
                    "text": "[ 7 ]",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "Network Organization"
        },
        {
            "text": "National recommendations and procedures for patient referral to the ECMOnet (Table 22 .1 and Fig. 22 .1 ) and ECMO eligibility criteria (Table 22 .2 ) were enacted by the Italian Ministry of Health and communicated to all local sanitary authorities and to the administration of all Italian hospitals. If required, an ECMOnet team (2 ICU physicians, 1 perfusionist, 1 ICU nurse) traveled to the referring hospital to take care of the transfer. After an attempt to stabilize/improve the status of the patient, the ECMOnet team would decide to either transport the patient conventionally or establish ECMO at the referring hospital. Transportation was carried out via ambulance, helicopter, or fi xed-wing aircraft, depending on distance, weather conditions, and ECMOnet center resources [ 7 ] .",
            "cite_spans": [
                {
                    "start": 785,
                    "end": 790,
                    "text": "[ 7 ]",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [
                {
                    "start": 76,
                    "end": 85,
                    "text": "(Table 22",
                    "ref_id": "TABREF1"
                },
                {
                    "start": 93,
                    "end": 100,
                    "text": "Fig. 22",
                    "ref_id": null
                },
                {
                    "start": 136,
                    "end": 145,
                    "text": "(Table 22",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "Patient Selection and Referral to the ECMOnet"
        },
        {
            "text": "The ECMO retrieval team must be very skilled and equipped for both venovenous VV ECMO and venoarterial VA ECMO. Percutaneous peripheral VV ECMO is preferred when cardiac function is adequate or mildly depressed. Patients should always be initiated on VV and eventually transitioned to VA ECMO if cardiac support is required. Vessel cannulation for VV ECMO can be confi gurated in several ways: dual-site or single-site approach. Beyond the hemodynamic instability, the ECMO team also will have to assess other conditions such as severe obesity and bleeding.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ECMO Team, Ventilator Management, and Safety"
        },
        {
            "text": "Some basic facilities are required in peripheral hospitals to ensure safety: echocardiography, fl uoroscopy, surgery, and blood bank. The use of a bicaval dual-lumen cannula is recommended only if a safe environment is available [ 8 ] . Settings of mechanical ventilation for patients on VV ECMO should minimize ventilator-associated lung injury and permit higher degrees of protective lung ventilation.",
            "cite_spans": [
                {
                    "start": 229,
                    "end": 234,
                    "text": "[ 8 ]",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": "ECMO Team, Ventilator Management, and Safety"
        },
        {
            "text": "Initiation and target for anticoagulation during transport depend on the availability of ACT. Bleeding should be thoroughly assessed before leaving the remote hospital. Sixty patients (median age 39.7 \u00b1 12, 60 % were male) received ECMO (59 VV ECMO and 1 VA ECMO) according to ECMO eligibility criteria. All patients fulfi lled criteria for ARDS. Median duration of MV (mechanical ventilation) before ECMO was 2 (1-5) days in patients with confi rmed H1N1 (ARDS H1N1 ) and 8 (1) (2) (3) (4) (5) (6) (7) (8) (9) (10) (11) (12) (13) (14) days in patients with other causes of ARDS (ARDS other ). Before ECMO, 42 patients (70 %) had received at least one \"rescue therapy\" (recruitment maneuvers, prone positioning, high-frequency oscillatory ventilation, inhaled nitric oxide, vasoactive drugs, steroid therapy). There were no statistically signifi cant differences between ARDS H1N1 and ARDS other in terms of severity of respiratory failure, treatment, and nonrespiratory organ function before ECMO.",
            "cite_spans": [
                {
                    "start": 475,
                    "end": 478,
                    "text": "(1)",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 479,
                    "end": 482,
                    "text": "(2)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 483,
                    "end": 486,
                    "text": "(3)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 487,
                    "end": 490,
                    "text": "(4)",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 491,
                    "end": 494,
                    "text": "(5)",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 495,
                    "end": 498,
                    "text": "(6)",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 499,
                    "end": 502,
                    "text": "(7)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 503,
                    "end": 506,
                    "text": "(8)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 507,
                    "end": 510,
                    "text": "(9)",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 511,
                    "end": 515,
                    "text": "(10)",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 516,
                    "end": 520,
                    "text": "(11)",
                    "ref_id": null
                },
                {
                    "start": 521,
                    "end": 525,
                    "text": "(12)",
                    "ref_id": null
                },
                {
                    "start": 526,
                    "end": 530,
                    "text": "(13)",
                    "ref_id": null
                },
                {
                    "start": 531,
                    "end": 535,
                    "text": "(14)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "ECMO Team, Ventilator Management, and Safety"
        },
        {
            "text": "Survival to hospital discharge in patients receiving ECMO was 68 %. Survival of patients receiving ECMO within 7 days from the onset of mechanical ventilation was 77 %. Survival rate in patients transported on ECMO was 81 %. There were no statistically signifi cant differences between patients transported on ECMO and patients starting ECMO at the ECMOnet center in terms of severity of respiratory failure, treatment, and outcomes. The length of MV prior to ECMO was an independent predictor of mortality.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ECMO Team, Ventilator Management, and Safety"
        },
        {
            "text": "Among the 60 patients who received ECMO, 49 (82 %) ARDS H1N1 presented a survival rate of 71 %, the remaining 11 (18 %) ARDS other presented a survival rate of 54 %. The median duration of ECMO support was 10 (7-17) days in ARDS H1N1 and 8 (3) (4) (5) (6) (7) (8) (9) (10) (11) (12) (13) (14) (15) (16) (17) (18) (19) (20) (21) days in ARDS other .",
            "cite_spans": [
                {
                    "start": 240,
                    "end": 243,
                    "text": "(3)",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 244,
                    "end": 247,
                    "text": "(4)",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 248,
                    "end": 251,
                    "text": "(5)",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 252,
                    "end": 255,
                    "text": "(6)",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 256,
                    "end": 259,
                    "text": "(7)",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 260,
                    "end": 263,
                    "text": "(8)",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 264,
                    "end": 267,
                    "text": "(9)",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 268,
                    "end": 272,
                    "text": "(10)",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 273,
                    "end": 277,
                    "text": "(11)",
                    "ref_id": null
                },
                {
                    "start": 278,
                    "end": 282,
                    "text": "(12)",
                    "ref_id": null
                },
                {
                    "start": 283,
                    "end": 287,
                    "text": "(13)",
                    "ref_id": null
                },
                {
                    "start": 288,
                    "end": 292,
                    "text": "(14)",
                    "ref_id": null
                },
                {
                    "start": 293,
                    "end": 297,
                    "text": "(15)",
                    "ref_id": null
                },
                {
                    "start": 298,
                    "end": 302,
                    "text": "(16)",
                    "ref_id": null
                },
                {
                    "start": 303,
                    "end": 307,
                    "text": "(17)",
                    "ref_id": null
                },
                {
                    "start": 308,
                    "end": 312,
                    "text": "(18)",
                    "ref_id": null
                },
                {
                    "start": 313,
                    "end": 317,
                    "text": "(19)",
                    "ref_id": null
                },
                {
                    "start": 318,
                    "end": 322,
                    "text": "(20)",
                    "ref_id": null
                },
                {
                    "start": 323,
                    "end": 327,
                    "text": "(21)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "ECMO Team, Ventilator Management, and Safety"
        },
        {
            "text": "Sixteen patients had hemorrhagic complications, and in 10 of them, a major bleeding event occurred, requiring blood transfusions and temporary reduction or suspension of anticoagulation. One patient died of cerebral hemorrhage diagnosed 2 days after cannulation. Blood components were transfused in 47 (78 %) patients.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ECMO Team, Ventilator Management, and Safety"
        },
        {
            "text": "Multiple organ failure associated with sepsis was the most common cause of death (53 %), followed by septic shock (26 %). All nonsurvivors were still on ECMO at the time of death [ 7 ] .",
            "cite_spans": [
                {
                    "start": 179,
                    "end": 184,
                    "text": "[ 7 ]",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "ECMO Team, Ventilator Management, and Safety"
        },
        {
            "text": "All baseline patient characteristics, clinical parameters, and vital signs before ECMO initiation were tested by univariate analysis. Using multivariate analysis, we identifi ed fi ve statistically signifi cant predictors of death: preECMO hospital length of stay, bilirubin value, creatinine level, hematocrit value, and systemic mean arterial pressure.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ECMOnet Score"
        },
        {
            "text": "The ECMOnet score was developed based on these variables. With the aim to be as intuitive as possible, the score was constructed to give a result between 0 and 10 (Table 22. 3 ). Thus, the number resulting from score calculation can be easily associated with the mortality risk. A score of 4.5 was found to be the most appropriate cutoff for mortality risk prediction. The high accuracy of the ECMOnet score was further confi rmed by ROC analysis and by an independent external validation analysis [ 9 ] .",
            "cite_spans": [
                {
                    "start": 498,
                    "end": 503,
                    "text": "[ 9 ]",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [
                {
                    "start": 163,
                    "end": 173,
                    "text": "(Table 22.",
                    "ref_id": "TABREF1"
                }
            ],
            "section": "ECMOnet Score"
        },
        {
            "text": "The role of ECMO in ARDS is now well-defi ned: ECMO support should be considered in patients with respiratory failure refractory to conventional therapy not only to ensure gas exchange but also to minimize ventilator-associated lung injury and its associated multiple organ dysfunction, both crucial determinants of survival for patients with ARDS. Several reports demonstrated that ECMO can be undertaken without the prohibitive morbidity and adverse events seen in the 1970s. To be effective, ECMO must be applied to the appropriate patient (indications, contraindications), timing of ECMO initiation must be the most correct (not too early, not too late), and ECMO must be correctly and safely applied (patient considerations: age, obesity, VV or VA ECMO, dual-site or single-site venovenous cannulation; monitoring and general measures-ultrasound, fl uoroscopy, surgery, and blood bank).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Comment"
        },
        {
            "text": "The centralization of patients to a few selected, specifi cally equipped centers can improve patient outcome, but the risks associated with patient transportation could exceed the benefi ts of centralization [ 10 ] . To reduce these risks, we planned some strategies: transferring in advance the largest proportion of patients potentially at risk of severe respiratory deterioration according to clinical criteria and assigning the patients to expert transportation teams, able to institute ECMO at the referring hospital and provide safe transportation with ECMO according to precise criteria. Some of the patients transported with ECMO might not have needed ECMO if treated from the beginning with other rescue therapies at the referral centers, where more therapeutic options were available. However, most of these patients were considered to be not safely transportable without ECMO.",
            "cite_spans": [
                {
                    "start": 208,
                    "end": 214,
                    "text": "[ 10 ]",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Comment"
        },
        {
            "text": "The Italian ECMO network ensured a high survival rate of patients with severe ARDS due to H1N1 infection treated by ECMO, a safe centralization, and created an organization ready to challenge future possible epidemics with high demand for critical care units with advanced respiratory support [ 7 ] . CESAR randomized trial indicated that signifi cantly more patients with severe ARDS survived without severe disability if they were transferred to a single ECMO center compared with patients who were managed conventionally. Moreover, this trial showed that patients referred for ECMO had roughly two times longer hospital stays and twice the medical costs of those treated in the conventional management group [ 1 ] . One or two patients with infl uenza A (H1N1)-related ARDS can strain the capacity of any intensive care unit (ICU) and ECMO team, particularly when ECMO availability is needed for other patients. ECMO centers should make plans for allocation of resources: identifi cation of early predictors of adverse outcome could allow optimization of criteria for ECMO eligibility and referral.",
            "cite_spans": [
                {
                    "start": 293,
                    "end": 298,
                    "text": "[ 7 ]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 711,
                    "end": 716,
                    "text": "[ 1 ]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": "Comment"
        },
        {
            "text": "The Italian ECMOnet activity showed that mortality of adult patients suffering from infl uenza A (H1N1)-related ARDS undergoing VV ECMO is related to extrapulmonary organ function at the time of cannulation. PreECMO hospital length of stay; bilirubin, creatinine, hematocrit values; and systemic mean arterial pressure were signifi cantly associated with mortality as assessed by multivariate analysis, while respiratory parameters were not associated with survival.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Comment"
        },
        {
            "text": "To improve risk stratifi cation and prediction of mortality risk at time of VV ECMO initiation, we developed a multifactorial scoring system-the ECMOnet score [ 9 ] .",
            "cite_spans": [
                {
                    "start": 159,
                    "end": 164,
                    "text": "[ 9 ]",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": "Comment"
        },
        {
            "text": "These data provide new perspectives concerning the allocation of resources for VV ECMO. We confi rm the strong clinical perception that survival is strongly correlated to extrapulmonary organ function at the time of ECMO initiation. This knowledge may help to identify potential candidates for ECMO support according to their mortality risk and provides guidance to solve crucial economic and ethical issues.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Comment"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Effi cacy and economic assessment of conventional ventilatory support versus extracorporeal membrane oxygenation for severe adult respiratory failure (CESAR): a multicentre randomised controlled trial",
            "authors": [
                {
                    "first": "G",
                    "middle": [
                        "J"
                    ],
                    "last": "Peek",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Mugford",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Tiruvoipati",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Lancet",
            "volume": "374",
            "issn": "",
            "pages": "1351--1363",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Referral to an extracorporeal membrane oxygenation center and mortality among patients with severe 2009 infl uenza A(H1N1)",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Noah",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [
                        "J"
                    ],
                    "last": "Peek",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "J"
                    ],
                    "last": "Finney",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "JAMA",
            "volume": "306",
            "issn": "15",
            "pages": "1659--1668",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Extracorporeal membrane oxygenation for interhospital transfer of severe acute respiratory distress syndrome patients: a 5-year experience",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Isgr\u00f2",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Patroniti",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Bombino",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Int J Artif Organs",
            "volume": "34",
            "issn": "11",
            "pages": "1052--1060",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Extracorporeal membrane oxygenation for 2009 infl uenza A(H1N1) acute respiratory distress syndrome",
            "authors": [],
            "year": 2009,
            "venue": "The Australia and New Zealand Extracorporeal Membrane Oxygenation (ANZ ECMO) Infl uenza Investigators",
            "volume": "302",
            "issn": "",
            "pages": "1888--1895",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Epidemic viral pneumonia",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "E"
                    ],
                    "last": "Lapinsky",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Curr Opin Infect Dis",
            "volume": "23",
            "issn": "",
            "pages": "139--144",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "European Society of Intensive Care Medicine Task Force for Intensive Care Unit Triage during an Infl uenza Epidemic or Mass Disaster (2010) Recommendations for intensive care unit and hospital preparations for an infl uenza epidemic or mass disaster: summary report of the European Society of Intensive Care Medicine's Task Force for intensive care unit triage during an infl uenza epidemic or mass disaster",
            "authors": [
                {
                    "first": "C",
                    "middle": [
                        "L"
                    ],
                    "last": "Sprung",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "L"
                    ],
                    "last": "Zimmerman",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Christian",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Intensive Care Med",
            "volume": "36",
            "issn": "",
            "pages": "428--443",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "The italian ECMO network experience during the 2009 infl uenza A(H1N1) pandemic: preparation for severe respiratory emergency outbreaks",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Patroniti",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Zangrillo",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Pappalardo",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Intensive Care Med",
            "volume": "37",
            "issn": "",
            "pages": "1447--1457",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Use of bicaval dual-lumen catheter for adult venovenous extracorporeal membrane oxygenation",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Javidfar",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Brodie",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "Ann Thorac Surg",
            "volume": "91",
            "issn": "",
            "pages": "1763--1769",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Predicting mortality risk in patients undergoing venovenous ECMO for ARDS due to infl uenza A (H1N1) pneumonia: the ECMOnet score",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Pappalardo",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Pieri",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Greco",
                    "suffix": ""
                }
            ],
            "year": 2013,
            "venue": "on behalf of the Italian ECMOnet",
            "volume": "39",
            "issn": "",
            "pages": "275--281",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Pro/con debate: do the benefi ts of regionalized critical care delivery outweigh the risks of interfacility patient transport",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Singh",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "D"
                    ],
                    "last": "Macdonald",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Crit Care",
            "volume": "13",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "\u2022 A. Zangrillo Cardiothoracic and Vascular Intensive Care , San Raffaele Scientifi c Institute , Via Olgettina 60 , Milan 20132 , Italy e-mail: calabro.mariagrazia@hsr.it; pappalardo.federico@hsr.it; zangrillo.alberto@hsr.itStructure of an ECMO Network for Respiratory SupportMaria Grazia Calabr\u00f2 , Federico Pappalardo , and Alberto Zangrillo number of patients who were transported on ECMO. Despite their illness severity and the prolonged use of life support, most of these patients survived",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "Fig. 22.1 Management algorithm for the referrals to the Italian ECMOnet system",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "Recommended national clinical criteria for ECMO eligibilityECMO inclusion criteriaAll adult and pediatric patients with severe ARDS related to suspected infl uenza A(H1N1) presenting with at least one of the following criteria despite the use of available rescue",
            "latex": null,
            "type": "table"
        },
        "TABREF3": {
            "text": "",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}