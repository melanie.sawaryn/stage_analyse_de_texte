{
    "paper_id": "435c50cdbe063d150c4ade6c3844dd0b0ded0332",
    "metadata": {
        "title": "Chapter 13 Sore Throat",
        "authors": [
            {
                "first": "Lori",
                "middle": [],
                "last": "Ciuffo",
                "suffix": "",
                "affiliation": {
                    "laboratory": "L. Ciuffo",
                    "institution": "North Central Bronx Hospital",
                    "location": {
                        "addrLine": "3424 Kossuth Ave",
                        "postCode": "10467",
                        "settlement": "Bronx",
                        "region": "MD (*), NY",
                        "country": "USA"
                    }
                },
                "email": "lori.ciuffo@nbhn.net"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Although the most common cause of pharyngitis is viral, Group A streptococcal (GAS) pharyngitis is a significant cause of community infections [1] . A majority of patients with pharyngitis receive presumptive antibiotic therapy. One report estimates about 60% of adults seen in the USA in 2010 for a complaint of sore throat received an antibiotic prescription [2] . Group A streptococcus typically presents with abrupt onset of symptoms including sore throat, fever, and anterior neck pain related to lymphadenopathy. These symptoms may occur in association with headache or malaise. Late winter and early spring are peak GAS seasons. The infection is transmitted via respiratory secretions, and the incubation period is 2-4 days. The goal of therapy is to reduce complications including acute rheumatic fever and glomerulonephritis [3, 4] . Many patients with viral pharyngitis also have signs and symptoms associated with a viral upper respiratory infection including nasal congestion, coryza, hoarseness, sinus discomfort, ear pain, or cough. Coinfections with streptococci and viruses may occur [2] . [5, 6] . Other bacterial causes of acute pharyngitis include groups C and G beta-hemolytic streptococci, Corynebacterium diphtheria, Arcanobacterium haemolyticum, Neisseria gonorrhoeae, Chlamydia pneumoniae, Francisella tularensis, Fusobacterium necrophorum, and Mycoplasma pneumoniae [1] . Noninfectious causes include seasonal or environmental allergies, smoking or secondhand smoking, poorly humidified air, and gastroesophageal reflux disease (GERD) [3] .",
            "cite_spans": [
                {
                    "start": 143,
                    "end": 146,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 361,
                    "end": 364,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 834,
                    "end": 837,
                    "text": "[3,",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 838,
                    "end": 840,
                    "text": "4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1100,
                    "end": 1103,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1106,
                    "end": 1109,
                    "text": "[5,",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1110,
                    "end": 1112,
                    "text": "6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 1391,
                    "end": 1394,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1560,
                    "end": 1563,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Introduction"
        },
        {
            "text": "The goal of the evaluation of adults with acute pharyngitis is to exclude potentially dangerous causes, to identify treatable causes, and to prevent complications including acute rheumatic fever. History of exposure to strep pharyngitis with exam findings including pharyngeal erythema, fever, tonsillar exudates with hypertrophy, tender and enlarged anterior cervical lymph nodes, and palatal petechiae is highly suspicious of GAS. Lymphadenopathy in any area other than the anterior cervical chain is not typical of GAS, but is common in mononucleosis. The presence of rash should be noted. The appearance of a whitish exudate in the mouth and pharynx (thrush) suggests fungal infection, seen in immunocompromised patients. Viral symptoms may include conjunctivitis, coryza, cough, diarrhea, coarseness, ulcerative stomatitis, or viral exanthem. Patients who present with unusually severe signs and symptoms, including secretions, drooling, dysphonia, muffled voice, or neck swelling particularly if they have difficulty swallowing, should be evaluated for rare but serious throat infections/local abscesses [2, [4] [5] [6] . College-aged patients should be asked about sexual practices and risks, as they have an increased incidence of oral chlamydia and gonorrhea infections.",
            "cite_spans": [
                {
                    "start": 1110,
                    "end": 1113,
                    "text": "[2,",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1114,
                    "end": 1117,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 1118,
                    "end": 1121,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 1122,
                    "end": 1125,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "Key History and Physical Exam"
        },
        {
            "text": "The modified Centor score (also known as McIsaac score) is a validated decision-making instrument that utilizes patient age and four specific signs and symptoms to determine the likelihood of having GAS [1, 7] The likelihood of GAS increases with the score; however, the score is equally useful for identifying patients for whom neither microbiologic tests nor antimicrobial therapy are necessary. Patients with a score of 1 or less are unlikely to have GAS and should not undergo further RADT testing or receive antibiotic treatment. Patients with a score of 4 or more should not be tested and antibiotics should be started. Patients with scores of 2 or 3 should receive RADT and, if indicated based on test results, given antibiotics. Other factors linked to increased likelihood of GAS infection are recent contact with a person with documented streptococcal infection and residence in a dormitory or group home. Throat culture has been considered the gold standard to establish the microbial cause of acute pharyngitis. However, compared with RADT, culture results are not available for 24-48 and can cause a delay in diagnosis. Throat culture is primarily used as a backup test in patients with negative RADT where clinical concern for GAS or bacterial pharyngitis is high [1, 7] .",
            "cite_spans": [
                {
                    "start": 203,
                    "end": 206,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 207,
                    "end": 209,
                    "text": "7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 1278,
                    "end": 1281,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 1282,
                    "end": 1284,
                    "text": "7]",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": "Diagnosis of Bacterial Pharyngitis"
        },
        {
            "text": "Rapid antigen detection testing is 70-90% sensitive and 90-100% specific. The use of antibiotics should usually be based on RADT results. Since the RADT is not 100% sensitive, if there is such a high level of suspicion of GAS infection, warranting empiric antibiotic treatment and then testing are not economically sensible.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Diagnosis of Bacterial Pharyngitis"
        },
        {
            "text": "Throat culture is 90-95% sensitive and 95-99% specific. For patients with a modified Centor score of 1, but who are high risk (e.g., poorly controlled diabetics, immunocompromised patients, chronic steroid users, patients with a history of rheumatic fever, childcare workers, and the elderly), consider doing throat culture or RADT.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Diagnosis of Bacterial Pharyngitis"
        },
        {
            "text": "Culture results should guide therapy [1, 3] . See Fig. 13 .1 for the modified Centor decision tree.",
            "cite_spans": [
                {
                    "start": 37,
                    "end": 40,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 41,
                    "end": 43,
                    "text": "3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [
                {
                    "start": 50,
                    "end": 57,
                    "text": "Fig. 13",
                    "ref_id": null
                }
            ],
            "section": "Diagnosis of Bacterial Pharyngitis"
        },
        {
            "text": "Antimicrobial therapy is warranted for patients with symptomatic pharyngitis with GAS confirmed by RADT or culture. Antibiotics may also be used to mitigate the clinical course of pharyngitis due to Group C and Group G strepto- [11] cocci. However, treatment need not continue for 10 days since acute rheumatic fever is not a complication due to these organisms; therefore, treatment for 5 days is sufficient.",
            "cite_spans": [
                {
                    "start": 228,
                    "end": 232,
                    "text": "[11]",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "Treatment"
        },
        {
            "text": "Oral antibiotic options for GAS pharyngitis include either a 10-day course of penicillin 500 mg twice a day, amoxicillin 500 mg twice a day, cephalexin 500 mg twice a day, clarithromycin 250 mg twice a day, clindamycin 300 mg three times a day, or a 5-day course of azithromycin 500 mg day 1 followed by 250 mg days 2-5. Intramuscular penicillin G benzathine (1, 200 ,000 U single dose) may be administered to patients who cannot complete a course of oral therapy or to patients at enhanced risk for rheumatic fever or when compliance with therapy is in question. Sulfonamides, fluoroquinolones, and tetracyclines should not be used for treatment of GAS due to high rates of resistance and failure to eradicate the organisms from the pharynx. Patients with GAS pharyngitis generally improve within 3-4 days and are no longer contagious after 24 h of antibiotics [4, [8] [9] [10] . Carriers of GAS do not require antimicrobial therapy because they are unlikely to spread GAS pharyngitis to close contacts and are at little to no risk for developing complications.",
            "cite_spans": [
                {
                    "start": 359,
                    "end": 362,
                    "text": "(1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 363,
                    "end": 366,
                    "text": "200",
                    "ref_id": null
                },
                {
                    "start": 862,
                    "end": 865,
                    "text": "[4,",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 866,
                    "end": 869,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 870,
                    "end": 873,
                    "text": "[9]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 874,
                    "end": 878,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": "Treatment"
        },
        {
            "text": "Supportive therapy, such as an analgesic/antipyretic, is useful in most cases of sore throat, regardless of etiology [3] .",
            "cite_spans": [
                {
                    "start": 117,
                    "end": 120,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Treatment"
        },
        {
            "text": "\u2022 Use the modified Centor score to guide testing and treatment of GAS. \u2022 If there is high suspicion of GAS, start empiric treatment.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Clinical Pearls"
        },
        {
            "text": "\u2022 The goal of treatment is to prevent complications and transmission. Treatment does not significantly shorten the duration of symptoms.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Clinical Pearls"
        },
        {
            "text": "\u2022 Evaluate for serious complications (abscess/local space infection) in patients presenting with sore throat and any of the following concerning symptoms: secretions, drooling, dysphonia, muffled voice, or neck swelling. \u2022 Consider underlying immunodeficiencies in patients presenting with thrush.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Don't Miss This!"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Diagnosis and treatment of streptococcal pharyngitis",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "A"
                    ],
                    "last": "Choby",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "Am Fam Phys",
            "volume": "79",
            "issn": "5",
            "pages": "383--90",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Principles of appropriate antibiotic use for acute pharyngitis in adults",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Snow",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Mottur-Pilson",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "J"
                    ],
                    "last": "Cooper",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Ann Intern Med",
            "volume": "134",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Clinical practice guidelines for the diagnosis and management of Group A Streptococcal pharyngitis: 2012 update by the Infection Disease Society of America",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "T"
                    ],
                    "last": "Shulman",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "L"
                    ],
                    "last": "Bisno",
                    "suffix": ""
                }
            ],
            "year": 2012,
            "venue": "Clin Infect Dis",
            "volume": "55",
            "issn": "10",
            "pages": "86--102",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Treatment and prevention of streptococcal tonsillopharyngitis",
            "authors": [
                {
                    "first": "Michael",
                    "middle": [
                        "E"
                    ],
                    "last": "Pichichero",
                    "suffix": ""
                }
            ],
            "year": 2016,
            "venue": "Up To Date",
            "volume": "",
            "issn": "",
            "pages": "1--24",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Pharyngitis in adults: the presence and coexistence of viruses and bacterial organisms",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Huovinen",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Lahtonen",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ziegler",
                    "suffix": ""
                }
            ],
            "year": 1989,
            "venue": "Ann Intern Med",
            "volume": "110",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Group A streptococci, mycoplasmas and viruses associated with acute pharyngitis",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "P"
                    ],
                    "last": "Glezen",
                    "suffix": ""
                },
                {
                    "first": "Clyde",
                    "middle": [],
                    "last": "Wa",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "J"
                    ],
                    "last": "Senior",
                    "suffix": ""
                }
            ],
            "year": 1967,
            "venue": "JAMA",
            "volume": "201",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "The diagnosis of strep throat in adults in the emergency room",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "M"
                    ],
                    "last": "Centor",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Witherspoon",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "P"
                    ],
                    "last": "Dalton",
                    "suffix": ""
                }
            ],
            "year": 1981,
            "venue": "Med Decis Making",
            "volume": "1",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Principles of appropriate antibiotic use for acute pharyngitis in adults",
            "authors": [
                {
                    "first": "V",
                    "middle": [],
                    "last": "Snow",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Mottur-Pilson",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "J"
                    ],
                    "last": "Cooper",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Ann Intern Med",
            "volume": "134",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Antibiotic prescribing to adults with sore throat in the United States",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Barett",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Linder",
                    "suffix": ""
                }
            ],
            "year": 1997,
            "venue": "JAMA Intern Med",
            "volume": "174",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Antibiotic treatment of adults with sore throat by community primary care physicians: a national survey",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "A"
                    ],
                    "last": "Linder",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "S"
                    ],
                    "last": "Statford",
                    "suffix": ""
                }
            ],
            "year": 1989,
            "venue": "JAMA",
            "volume": "286",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "A clinical score to reduce unnecessary antibiotic use in patients with sore throat",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "J"
                    ],
                    "last": "Mcisaac",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "White",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Tannenbaum",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "E"
                    ],
                    "last": "Low",
                    "suffix": ""
                }
            ],
            "year": 1998,
            "venue": "CMAJ",
            "volume": "158",
            "issn": "1",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Modified Centor score and management options using clinical decision rule. Other factors should be considered (e.g., a score of 1, but recent family contact with documented streptococcal infection). GAS Group A beta-hemolytic streptococcus, RADT rapid antigen detection testing. Adapted from McIsaac WJ, White D, Tannenbaum D, Low DE. A clinical score to reduce unnecessary antibiotic use in patients with sore throat. CMAJ. 1998;158(1):79",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Decision-Making/Differential Diagnosis Viruses including adenovirus, influenza virus, parainfluenza virus, rhinovirus, and respiratory syncytial virus are frequent causes of acute pharyngitis. Other viral agents include coxsackievirus, echovirus, coronavirus, enterovirus, cytomegalovirus (CMV), human immunodeficiency virus (HIV), and herpes simplex virus. Epstein-Barr virus is a frequent cause of acute pharyngitis accompanied by other clinical features of infectious mononucleosis such as generalized fatigue, lymphadenopathy, and splenomegaly. Systemic infections with rubella virus or measles virus can be associated with acute pharyngitis",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": ".",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}