{
    "paper_id": "adbc8757255002df760c308276d92bc7bc969c15",
    "metadata": {
        "title": "Spinned Poisson distribution with health management application Ramalingam Shanmugam",
        "authors": [
            {
                "first": "R",
                "middle": [],
                "last": "Shanmugam",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Texas State University",
                    "location": {
                        "postCode": "78666",
                        "settlement": "San Marcos",
                        "region": "TX",
                        "country": "USA"
                    }
                },
                "email": "rs15@txstate.edu"
            }
        ]
    },
    "abstract": [
        {
            "text": "Consider a data collection setup during a spread of an infectious disease. Examples include severe acute respiratory syndrome (SARS) or influenza A virus H3N2.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Let Y be a random number of cases in an observable set S of non-negative integers with unknown parameters \u0398 \u2208 {\u03b8, \u03c1} in a Poisson type chance mechanism where the infected cases are quickly removed by the health management even before the completion of data. The analysis of data has to be carefully done with a selection of an appropriate underlying probability distribution. The usual Poisson distribution is inappropriate to use because of the impact of removing infected cases on the incidence rate and the chance of observing a new case. The parameters \u03b8>0 and \u03c1\u22650 portray respectively the incidence rate and the impact level of removing infected cases. Consequently, both the random observation count Y and the incidence rate, \u03b8 become size/ length biased. Published articles in the statistical literature deal with only the size/length bias on observation, Y in a marginal sense excluding the impact on the parameter (see pages 149-150 and the reference section of Johnson et al. [3] for a full list of published articles on size/length biased sampling). There is no article in the literature discussing the sampling bias on the incidence rate, \u03b8. The sampling bias on both the observation and the incidence rate should be dealt with simultaneously to be realistic in modeling the situation in which the infected cases are removed.",
            "cite_spans": [
                {
                    "start": 986,
                    "end": 989,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Motivation"
        },
        {
            "text": "For this purpose, assume that the observation Y is proportional to a sampling weight factor w y; r; q \u00f0 \u00de\u00bc 1\u00fery \u00f0 \u00de 1\u00ferq \u00f0 \u00de . An insight for this weight factor arises from the following reasons. The observation y is impacted by the removal bias \u03c1\u22650 due to the removal. A scale shift on one in the weight factor is necessary to avoid degeneration of probabilities to zero when \u03c1=0. The denominator (1+\u03c1\u03b8) in the weight factor echoes the impact of the removal bias on the incidence parameter \u03b8 and it is a necessity for the expression in (1) to be a bona fide probability distribution.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Motivation"
        },
        {
            "text": "In this weighted Poisson sampling framework, when \u03c1=0, it nullifies the impact of removing cases. When \u03c1=1, it is called size/length biased sampling. That is found in count distribution literature. Since the seminal work of Cox [2] , many articles have appeared about size/length biased sampling. These articles considered only the bias in the observation, Y, but not the bias in the parameter. This article is the first one to suggest that the selection bias could influence the parameter(s) as much as the observation.",
            "cite_spans": [
                {
                    "start": 228,
                    "end": 231,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "Motivation"
        },
        {
            "text": "A new Poisson distribution in (1) incorporates the chance mechanism of rare events in which the infected cases are removed.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Motivation"
        },
        {
            "text": "\u00de e \u00c0q q y =y! y \u00bc 0; 1; 2; :::; 1; 0 < q < 1; 0 r < 1 \u00f01\u00de",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Motivation"
        },
        {
            "text": "The probability distribution in (1) is named the spinned Poisson distribution (SPD). Is expression in (1) a bona fide probability distribution? The answer is affirmative. The following arguments prove it. For the specified parameter space 0 < \u03b8 < \u221e; 0 \u2264 \u03c1 < \u221e and the sample space y=0,1,2,\u2026,\u221e, the right side of the expression (1) has clearly non-negative valued functions. In addition, the sum of their non-negative values is equal to one and it is proved below.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Motivation"
        },
        {
            "text": "The SPD is versatile enough to describe data from engineering, finance and economics in which the removal of cases occurs before completing the data collection. For example, in finance studies, a defaulted homeowner may be removed from a potential home mortgage insurance list.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Motivation"
        },
        {
            "text": "The SPD has several interesting statistical properties as explained below. The estimation of its parameters, assessment of its survival function, and the application of the hypothesis testing procedures for the SPD are of interest. Results are demonstrated, using Bailey's [1] data on Nigeria's smallpox incidences in Section 3. The Table 1 Table 1 . The Fig. 2 helps to compare the goodness of fit test results.",
            "cite_spans": [
                {
                    "start": 273,
                    "end": 276,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [
                {
                    "start": 333,
                    "end": 340,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 341,
                    "end": 348,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 355,
                    "end": 361,
                    "text": "Fig. 2",
                    "ref_id": null
                }
            ],
            "section": "Motivation"
        },
        {
            "text": "In this section, several statistical properties are derived and interpreted. The expected value \u03bc = E(Y) is obtained in a straightforward manner as stated below. That is,",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "neglecting the fraction 1 1\u00ferq < 1. When the impact of removing infected cases is negligible (that is, \u03c1=0), the mean in (2) reduces to just the expected value, \u03b8 of the usual Poisson distribution. From (2), note that and it attests to a property that there are triangular relationships among the mean \u03bc, parameters \u03b8 and \u03c1. In a similar manner the variance",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "The expression (3.a) is the second factorial moment. The expression (3.a) could be rewritten equivalently, for \u03c1\u22600,",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "The expression (3.b) illustrates a mean-variance relationship like the one for the usual Poisson distribution in which the mean and variance are equal (that is, \u03c3 2 = \u03bc).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "When there is negligible impact of removing the infected cases (that is, \u03c1=0), the result in (3.a) and (3.b) reduce respectively to the second factorial moment and variance of the usual Poisson distribution. Otherwise, there is an intrinsic relationship among the incidence rate, impact of removing infected cases and the expected number of infected cases.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": ". This result confirms that \u03c1\u21920 when \u03bc\u2192\u03b8, a property of the usual Poisson distribution.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "\u00de is the threshold probability for the event Y=0. However, a recurrence relationship among the spinned Poisson probabilities exists and it is",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "The recurrence relationship in (4) of the SPD implies that",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "and so on. These relationships point out that the datacollection mechanism that includes the removal of infected cases indeed imposes a sampling bias on both the observation and the incidence rate. When the impact of removing infected cases turns out to be negligible (that is, \u03c1\u21920), the SPD in (1) reduces to the usual Poisson distribution",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "See Johnson et al. [3] for properties of the usual Poisson distribution. Now, other properties of the SPD are derived. First, note incidentally that the statistic 1 1\u00ferY is an unbiased estimate of the parametric function 1 1\u00ferq because",
            "cite_spans": [
                {
                    "start": 19,
                    "end": 22,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "\u00de e \u00c0q q y =y!:",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "Furthermore, the variance of the statistic 1 1\u00ferY is obtained using a formula (see Stuart and Ord [4] for details) that",
            "cite_spans": [
                {
                    "start": 83,
                    "end": 101,
                    "text": "Stuart and Ord [4]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "where m 2 j and s 2 j denote the mean and variance respectively of j = U or V. In this set up, note that U=1 and V=1+\u03c1Y.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "Hence, the variance of the statistic 1 1\u00ferY is Var 1",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "\u00de e \u00c0q q y =y!:",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "Consequently, an expression for computing the correlation coefficient between two sample statistics Y and 1 1\u00ferY is obtained and utilized to confirm whether the SPD matches well the given data in Section 3. The correlation coefficient",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "is zero when \u03c1=0. Otherwise (that is for \u03c1\u22600),",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "using (2) ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "using the MLE of the dispersion \u03c3 2 and the mean \u03bc. Consider a random sample y 1 , y 2 ,....,y n from the SPD in Eq. 1. The MLEs are asymptotically efficient. The log likelihood function is ln L \u00bc P n i\u00bc1 lnp y i ; r; q j \u00f0 \u00de. The first derivatives of the log likelihood function with respect to the parameters \u03b8 and \u03c1 give their score functions. Equating the score functions @ q ln L and @ r ln L to zero gives the MLEs. The exact value of the MLEs b q mle and b r mle could be obtained by solving iteratively the equations",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "and",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "The process is tedious for the exact values. The process can be eased if approximate values are acceptable. For this purpose, the expression (9) is approximated using Taylor's series approximation. That is,",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "where the notation @ b r mle \u00bc0 6 b r mle \u00f0 \u00de portrays the derivative of 6 b r mle \u00f0 \u00de with respect to b r mle evaluated at b r mle \u00bc 0 and b q mle is the MLE of the incidence rate. Hence, the approximate values of the MLEs are b q mle % y \u00c0 1 \u00f010\u00de",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "where y is the sample mean and s is the sample standard deviation.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "Next, an expression for the survival function of the SPD is derived. The cumulative distribution function of the usual Poisson distribution is expressed in terms of the cumulative chi-squared distribution function in Johnson et al. [3] , which is tabulated extensively in books. Following this line of thinking, a similar relationship between the cumulative distribution functions (CDF) of the SPD and the chisquared distribution can be established. For this purpose, let CDF m; q; r \u00f0 \u00de\u00bcPr Y m \u00bd be CDF of the SPD for a prespecified m in the sample space. Implicitly, the survival function Sf m \u00fe 1; q; r \u00f0 \u00de\u00bc1 \u00c0 CDF m; q; r \u00f0 \u00deof the spinned Poisson distribution is, after algebraic simplifications,",
            "cite_spans": [
                {
                    "start": 232,
                    "end": 235,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "in which CDF # 2 2m;df 2q \u00f0 \u00de denotes the cumulative chi-squared distribution function with 2 m degrees of freedom (df) and the percentile (2\u03b8).",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "An immediate removal means that the number of days to remove a next infected case is Y=0. The probability for an immediate removal to occur, with m=0, is",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "The scenario, \u03c1=0 is indicative of no impact of the removing the infected cases. Substituting \u03c1=0 in (12) and (13), they reduce to the results for the usual Poisson distribution. On the contrary, the scenario, \u03c1=1 is indicative of the presence of impact on the observation count Y and on the incidence rate \u03b8 due to removing the infected cases. Otherwise (that is when \u03c1>0), there is a significant impact on the observation and on the incidence rate due to removing infected cases. A hypothesis testing procedure about the impact parameter \u03c1 is therefore worthwhile. The likelihood ratio concept can be used to test the null hypothesis H o : \u03c1 = \u03c1 o where \u03c1 o is zero against a research/ alternative hypothesis H 1 : \u03c1 = \u03c1 * \u2260 \u03c1 o . Note that the likelihood ratio under the null hypothesis is \u039b r o \u00bc L y 1 ; y 2 ; ::; y n ; b q r O \u00bc0 ; r o \u00bc 0 L y 1 ; y 2 ; ::; y n ; b q b r mle ; b r mle 6 \u00bc 0 and is \u039b r \u00bb \u00bc L y 1 ; y 2 ; ::; y n ; b q r \u00bb ; r \u00bc r \u00bb L y 1 ; y 2 ; ::; y n ; b q b r mle ; b r mle 6 \u00bc 0 under a research/alternative hypothesis. The MLE of the incidence rate is b q mle; r o \u00bc0 \u00bc y under the null hypothesis and b q mle; r\u00bcr \u00bb % y \u00c0 1 under the alternative/ research hypothesis. The MLE of the impact parameter",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "under the alternative/research hypothesis. Under the null hypothesis, the minus log likelihood ratio is",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "and it follows a non-central chi-squared distribution with one df and the non-centrality parameter",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "See Wald [5] for definition and properties of the non-central chi squared distribution. A unique property of the MLE is that the MLE of a function is the function of the MLE of the parameters. In addition, the variance-covariance matrix of the MLE of the parameters is the inverse of the information matrix ",
            "cite_spans": [
                {
                    "start": 9,
                    "end": 12,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "and (2). The determinant of the information matrix in (16) is",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "using (3.b) and (10). The variance-covariance matrix of the MLE of the parameters is ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "with the MLE of b q mle and b r mle . In addition, the non-central chi squared distribution with one df and non-centrality parameter \u03b4 approximately follows 1 \u00fe d 1\u00fed times a central chi squared distribution with 1\u00fed \u00f0 \u00de 2 1\u00fe2d \u00f0 \u00de df (see Stuart and Ord [4] for details of this equivalence). This means that the null hypothesis H o : \u03c1 = \u03c1 o where \u03c1 o is zero will be rejected in favor of the research/alternative hypothesis",
            "cite_spans": [
                {
                    "start": 240,
                    "end": 258,
                    "text": "Stuart and Ord [4]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "where the right side is the critical value based on the 100 (1\u2212\u03b1) th percentile of the central chi squared distribution",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "1\u00fe2 b dr o \u00c0 \u00c1 df and a significance level \u03b1 \u2208(0,1). The statistical power of the test statistic in (14) can be examined with a selection of a specific value for \u03c1 in the research/alternative hypothesis. Let H 1 : \u03c1 = \u03c1 * \u2260 \u03c1 o in which \u03c1 o is zero. It then suggests that the statistical power is the probability of rejecting the null hypothesis H o : \u03c1 = \u03c1 o in favor of the research/alternative hypothesis H 1 : \u03c1 = \u03c1 * \u2260 \u03c1 o . That is, under the research hypothesis, the minus log likelihood ratio is",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "In addition, it follows a non-central chi-squared distribution with one df and non-centrality parameter ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        },
        {
            "text": "This non-central chi squared distribution with one df and non-centrality parameter b d r \u00bb approximately follows df. The power is the probability of accepting the research/alternative hypothesis H 1 when \u03c1 = \u03c1*. That is, studies in which some cases are removed from the study population due to a screening criterion. The results based on the spinned Poisson distribution could become the foundation for performing further reliability or validity analysis. These and other aspects are currently under investigation and the findings will be reported later.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Main results: spinned Poisson distribution"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "The mathematical theory of infectious diseases and its applications",
            "authors": [
                {
                    "first": "Ntj",
                    "middle": [],
                    "last": "Bailey",
                    "suffix": ""
                }
            ],
            "year": 1975,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Some sampling problems in technology",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "R"
                    ],
                    "last": "Cox",
                    "suffix": ""
                }
            ],
            "year": 1969,
            "venue": "New development in survey sampling",
            "volume": "",
            "issn": "",
            "pages": "506--517",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Univariate discrete distributions",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Johnson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kemp",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Kotz",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Kendall's advanced theory of statistics, volume I",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Stuart",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Ord",
                    "suffix": ""
                }
            ],
            "year": 1994,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Tests of statistical hypotheses concerning several parameters when the number of observations is large",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Wald",
                    "suffix": ""
                }
            ],
            "year": 1943,
            "venue": "Trans Am Math Soc",
            "volume": "54",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "displays data. The Fig. 1 displays the relationship between 1/(1+rho*Y) in terms of Y to capture their correlation (= the linear relationship). The Fig. 2 displays the survival function of the fitted usual Poisson and the fitted spinned Poisson distributions along with the empirical survival function of the data in",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Scatterplot Empirical, fitted Poisson and spinned Poisson survival functions",
            "latex": null,
            "type": "figure"
        },
        "FIGREF2": {
            "text": "and (3.b) and the plot of the data Y versus a statistic 1 1\u00ferY exhibits a trend between them. Their linear relationship is captured by the correlation coefficient. The plot and the correlation coefficient of the collected data reveal how much a deviation might have occurred from the usual Poisson distribution due to removing the infected cases before the completion of data collection.The maximum likelihood estimate (MLE) of the correlation coefficient is",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "",
            "latex": null,
            "type": "table"
        },
        "TABREF2": {
            "text": "\u00feb r mle y 2",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "The author sincerely thanks all three reviewers and the editor for comments and suggestions, which helped to greatly improve the manuscript.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgement"
        },
        {
            "text": "In this section, the data consisting of smallpox cases that are displayed in Bailey [1, p.105 ] are considered for illustration of the results in Section 2. Bailey reported the number, Y of days waited to remove a smallpox case in a closed community of 100 individuals in Abakaliki, Nigeria (as in the Table 1 below). Two graphs and one table are included.The MLEs are b r mle % 0:48 and b q mle % 0:93 using (11) and (10) respectively. It is clear that there is an impact of removing infected smallpox cases on the incidence rate, \u03b8 and on the observation, y as well. The usual Poisson distribution p y; q j \u00f0 \u00de \u00bc Pr Y \u00bc y \u00bd \u00bce \u00c0q q y =y! is therefore inappropriate for this data with the incidence rate \u03b8. Under the usual Poisson distribution, the estimate of incidence rate would have been b q r\u00bc0 % y \u00bc 1:39 . The graph of Y versus 1 1\u00ferY in Fig. 1 confirms the appropriateness of SPD for the given data. Their linear relationship is captured by the correlation coefficient and it is estimated to be b corr Y ; 1 1\u00ferY \u00bc \u00c00:79 due to (6) which is quite different from zero. If the data were to be simple Poisson with no sampling bias on y and no impact on the incidence rate due to the removal of infected cases, the configuration in Fig. 1 would have been horizontal with zero correlation. But, it did not happen.The cumulative distribution function, CDF(m, \u03b8, \u03c1) = Pr [Y\u2264m] or implicitly its survival function, Sf m \u00fe 1; q; r \u00f0 \u00de\u00bc 1 \u00c0 CDF m; q; r \u00f0 \u00de of the spinned Poisson distribution is after algebraic simplifications, in which CDF # 2 2m;df 1:86 \u00f0 \u00de is the cumulative chi-squared distribution function with 2 m df and percentile (1.86).The Fig. 2 illustrates a graphical comparison of the survival functions of the usual Poisson, spinned Poisson and empirical distributions of the data in Table 1 . The vertical and horizontal axes represent survival probability and m+1 respectively. The survival function of the spinned Poisson survival function rather than the survival function of the usual Poisson distribution is closer to the empirical survival function.The immediate removal occurs when the number of waiting days to remove an infected case is zero, meaning that Y=0. The probability for an immediate removal situation to occur isThe null hypothesis is H o : \u03c1 = \u03c1 o =0 and the research/ alternative hypothesis is H 1 : \u03c1 = \u03c1*=1. These are now tested using the likelihood ratio test. Under the null hypothesis, the minus log likelihood ratio is \u00c0 ln \u039b b r mle \u00bc 14:14 and the non-centrality parameter b d r 0 \u00bc0 \u00bc 0:008 This non-central chi squared value with one df and noncentrality parameter is approximately 1 \u00fe dr 0 Using the likelihood ratio criterion, the statistical power of rejecting the null hypothesis in favor of the research hypothesis is calculated using Eq. 21. That is, Power \u00bc Pr # 2 0:991df < 0:34 h i \u00bc 0:55 when the research hypothesis H1: \u03c1 = \u03c1*=1 is true.",
            "cite_spans": [
                {
                    "start": 84,
                    "end": 93,
                    "text": "[1, p.105",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 302,
                    "end": 309,
                    "text": "Table 1",
                    "ref_id": null
                },
                {
                    "start": 846,
                    "end": 852,
                    "text": "Fig. 1",
                    "ref_id": null
                },
                {
                    "start": 1237,
                    "end": 1243,
                    "text": "Fig. 1",
                    "ref_id": null
                },
                {
                    "start": 1650,
                    "end": 1656,
                    "text": "Fig. 2",
                    "ref_id": null
                },
                {
                    "start": 1799,
                    "end": 1806,
                    "text": "Table 1",
                    "ref_id": null
                }
            ],
            "section": "Illustrations"
        },
        {
            "text": "The results of this article are applicable to analyzing data from business, engineering, economics, and sociologic",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Comments and conclusions"
        }
    ]
}