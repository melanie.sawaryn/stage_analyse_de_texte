{
    "paper_id": "11750531efdbbf977ebd5203c6187ee3836e42ae",
    "metadata": {
        "title": "Collapsing glomerulopathy in a COVID-19 patient",
        "authors": [
            {
                "first": "S\u00e9bastien",
                "middle": [],
                "last": "Kissling",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Service of nephrology and hypertension",
                    "institution": "",
                    "location": {}
                },
                "email": ""
            },
            {
                "first": "Samuel",
                "middle": [],
                "last": "Rotman",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Service of clinical pathology",
                    "institution": "",
                    "location": {}
                },
                "email": ""
            },
            {
                "first": "Christel",
                "middle": [],
                "last": "Gerber",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Matthieu",
                "middle": [],
                "last": "Halfon",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Service of nephrology and hypertension",
                    "institution": "",
                    "location": {}
                },
                "email": ""
            },
            {
                "first": "Fr\u00e9d\u00e9ric",
                "middle": [],
                "last": "Lamoth",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Denis",
                "middle": [],
                "last": "Comte",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Lausanne",
                    "location": {
                        "country": "Switzerland"
                    }
                },
                "email": ""
            },
            {
                "first": "Lo\u00efc",
                "middle": [],
                "last": "Lhopitallier",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Service of infectious diseases",
                    "institution": "",
                    "location": {}
                },
                "email": ""
            },
            {
                "first": "Salima",
                "middle": [],
                "last": "Sadallah",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "University of Lausanne",
                    "location": {
                        "country": "Switzerland"
                    }
                },
                "email": ""
            },
            {
                "first": "Fadi",
                "middle": [],
                "last": "Fakhouri",
                "suffix": "",
                "affiliation": {
                    "laboratory": "Service of nephrology and hypertension",
                    "institution": "",
                    "location": {}
                },
                "email": "fadi.fakhouri@unil.ch"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The first available reports indicate that renal involvement is relatively frequent in patients with novel coronavirus disease (COVID-19) due to the emerging severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). Up to 43% of patients present with proteinuria (including 10% with heavy proteinuria), 11% with haematuria and acute kidney injury occurs in 3.5 to 5% of cases 1,2 . Both proteinuria and acute kidney injury are associated with increased mortality 1 . However, the exact mechanisms underlying renal injury in patients with COVID-19 are unclear as renal pathology data are lacking.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Letter to the Editor"
        },
        {
            "text": "We report on a 63-year old black male patient who developed acute kidney injury in the setting of COVID-19. He had a history of hypertension treated with atenolol, nifedipin and olmesartan. He initially presented with intense fatigue, high-grade fever (39.7\u00b0C) and respiratory distress (respiratory rate: 36/minute, arterial blood O2 saturation: 86%) requiring O2 supplementation (4l/min). On admission, his serum creatinine was 1. No specific treatment was implemented. The patient maintained a urinary output and did not require dialysis. Renal function subsequently improved and proteinuria decreased (Figure, A) . The patient was discharged on day 17 with a serum creatinine of 5.5 mg/dL and persistent proteinuria (1.8 g/l).",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 604,
                    "end": 615,
                    "text": "(Figure, A)",
                    "ref_id": null
                }
            ],
            "section": "Letter to the Editor"
        },
        {
            "text": "To the best of our knowledge, this is the first description of the pathological features of renal injury in the setting of COVID-19 outside autopsies series. The most striking finding in our patient was the collapsing FSGS. This finding suggests that FSGS could account for the heavy proteinuria reported in a significant proportion of patients with COVID-19 1 . The collapsing FSGS is a known complication of other viral infections, in particular the human immunodeficiency virus (HIV) 4 , but also the cytomegalovirus 5 and the parvovirus B19 6 . For HIV a direct toxic viral effect on podocytes has been documented 7 . The receptor for SARS-CoV-2, membrane-bound angiotensin-converting enzyme 2 (ACE2), is expressed on podocytes 8, 9 . PCR for SARS-CoV-2 was negative in kidney biopsy samples, but the technique has a notoriously low rate of detection in non-respiratory samples (including blood and urine) 10 and the quality of the extracted RNA material was poor. However, in our patient's case, the presence of viral particles in the podocytes, revealed by electron microscopy study, strongly suggests a direct toxic viral effect on podocytes.",
            "cite_spans": [
                {
                    "start": 487,
                    "end": 488,
                    "text": "4",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 732,
                    "end": 734,
                    "text": "8,",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 735,
                    "end": 736,
                    "text": "9",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 910,
                    "end": 912,
                    "text": "10",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Collapsing FSGS, with or without acute tubular necrosis, can also complicate the course of hemophagocytic syndrome 11 , a disorder characterized by an increased release of a wide range of cytokines. In our patient, normal levels of cytokines, in particular IL6, whilst inflammation markers were still increased, plead against this hypothesis. However, a potential virus-driven intra-renal cytokines release cannot be excluded.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Besides, since our patient is of African origin, it cannot be excluded that APOL1 variant may have contributed to the pathogenesis of collapsing FSGS, since APOL1 is a recognized risk factor for the development collapsing FSGS in HIV and non-HIV patients 4 .",
            "cite_spans": [
                {
                    "start": 255,
                    "end": 256,
                    "text": "4",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "In our patient, acute tubular necrosis developed in the absence of hemodynamic compromise or severe pulmonary involvement. This suggests that tubular injury in COVID-19 patients, unlike that seen in coronavirus-associated severe acute respiratory syndrome (SARS) 12 , is not predominantly ischemic. The possible underlying mechanisms are a direct viral toxicity on tubular cells that also harbour ACE2 or a cytokine-mediated tubular damage.",
            "cite_spans": [
                {
                    "start": 263,
                    "end": 265,
                    "text": "12",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "In addition, the initial heavy proteinuria in our patient may have also contributed to tubular necrosis.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        },
        {
            "text": "Overall, in contrast to lung injury, kidney injury in COVID-19 does not appear to include a predominant inflammatory component. This observation suggests that collapsing FSGS, potentially resulting from a direct viral effect on podocytes, probably belongs to the spectrum of COVID-19-associated renal involvement.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Discussion"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Kidney disease is associated with in-hospital death of patients with COVID-19",
            "authors": [
                {
                    "first": "Yichun",
                    "middle": [],
                    "last": "Cheng",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "L"
                    ],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "Kun",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Meng",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "Zhixiang",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Lei",
                    "middle": [],
                    "last": "Dong",
                    "suffix": ""
                },
                {
                    "first": "Junhua",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "Ying",
                    "middle": [],
                    "last": "Yao",
                    "suffix": ""
                },
                {
                    "first": "Shuwang",
                    "middle": [],
                    "last": "Ge",
                    "suffix": ""
                },
                {
                    "first": "Gang",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Kidney International",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1016/j.kint.2020.03.005"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Clinical Characteristics of 138 Hospitalized Patients With",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Hu",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Novel Coronavirus-Infected Pneumonia in Wuhan, China",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "A Novel Coronavirus from Patients with Pneumonia in China",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Zhu",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "N Engl J Med",
            "volume": "382",
            "issn": "",
            "pages": "727--760",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "HIV-associated nephropathies: epidemiology, pathology, mechanisms and treatment",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "Z"
                    ],
                    "last": "Rosenberg",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Naicker",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "A"
                    ],
                    "last": "Winkler",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "B"
                    ],
                    "last": "Kopp",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Nat Rev Nephrol",
            "volume": "11",
            "issn": "",
            "pages": "150--60",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Acute cytomegalovirus infection complicated by collapsing glomerulopathy",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Tomlinson",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Boriskin",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Mcphee",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Holwill",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Rice",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Nephrol Dial Transplant",
            "volume": "18",
            "issn": "",
            "pages": "187--196",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Parvovirus-B19-associated complications in renal transplant recipients",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Waldman",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "B"
                    ],
                    "last": "Kopp",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Nat Clin Pract Nephrol",
            "volume": "3",
            "issn": "",
            "pages": "540--50",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "HIV-1 Nef disrupts the podocyte actin cytoskeleton by interacting with diaphanous interacting protein",
            "authors": [
                {
                    "first": "T",
                    "middle": [
                        "C"
                    ],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "He",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [
                        "H"
                    ],
                    "last": "Wang",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "J Biol Chem",
            "volume": "283",
            "issn": "",
            "pages": "8173--82",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Glomerular localization and expression of Angiotensin-converting enzyme 2 and Angiotensin-converting enzyme: implications for albuminuria in diabetes",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Ye",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Wysocki",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "William",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Soler",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [],
                    "last": "Cokic",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Batlle",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Am Soc Nephrol",
            "volume": "17",
            "issn": "",
            "pages": "3067--75",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Should COVID-19 Concern Nephrologists? Why and to What Extent? The Emerging Impasse of Angiotensin Blockade",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Perico",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Benigni",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Remuzzi",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Nephron",
            "volume": "2020",
            "issn": "",
            "pages": "1--9",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Detection of SARS-CoV-2 in Different Types of Clinical Specimens",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Xu",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Gao",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "2020",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Nephrotic syndrome associated with hemophagocytic syndrome",
            "authors": [
                {
                    "first": "O",
                    "middle": [],
                    "last": "Thaunat",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Delahousse",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Fakhouri",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Kidney Int",
            "volume": "69",
            "issn": "",
            "pages": "1892--1900",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "He rapidly developed acute kidney injury without hemodynamic compromise. His respiratory status improved but inflammatory syndrome persisted and renal function further deteriorated. Illustrative images of his kidney biopsy are shown in the lower panel B, C and D)",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "H"
                    ],
                    "last": "Chu",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "K"
                    ],
                    "last": "Tsang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "S"
                    ],
                    "last": "Tang",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Kidney Int",
            "volume": "67",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "B) which contained numerous proteins reabsorption vacuoles (*, C). 2) acute tubular lesions (D) with focal tubular necrosis, dilatation and the presence of",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Immunofluorescence study did not show any significant immune deposits",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "73000) disclosed within the podocytes cytoplasm vacuoles containing numerous spherical particles (*, E) measuring between 50 to 110 nm and surrounded by spikes measuring 9-10nm (*, F)",
            "authors": [
                {
                    "first": "E",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "Electron microscopy study",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "2 mg/dL. He was diagnosed with COVID-19 based on a positive reverse transcriptase-polymerase chain reaction (RT-PCR) test for SARS-CoV-2 in a nasopharyngeal swab sample. Shortly after his admission, he developed oliguria and rapidly progressive acute kidney injury (KDIGO stage 3) with a serum creatinine of 4.4mg/dL at day 4 (Figure, A).Laboratory tests showed an increase in the C-reactive protein (CRP) level, lymphopenia, increased D-Dimers serum level, hypoalbuminemia, massive proteinuria (5 g/l consisting of 50% of albumin) and reduced sodium urinary excretion (sodium excretion fraction: 0.4%).The patient had no episodes of hypotension and remained hypertensive for most of his hospital stay. His respiratory condition gradually improved and the O2 supplementation was decreased (0.5 l/min at day 8). Serum levels of a range of cytokines, including IL6, were normal. However, the CRP serum levels further increased associated with systemic complement activation (sC5b-9, Bb fragment) and worsening of acute kidney injury with a serum creatinine peaking at 8.4 mg/dL at day 8(Figure, A). The patient did not receive SARS-CoV-2 specific experimental treatment (protease inhibitors, remdesivir and hydroxychloroquine) or any nephrotoxic drug.A kidney biopsy was performed at day 8. Light microscopy examination disclosed two main features: severe collapsing focal segmental glomerulosclerosis (FSGS)(Figure,B and C) and acute tubular necrosis (Figure, D) without any significant interstitial inflammation. The immunofluorescence study revealed no significant immune deposits (including antiC5b-9 staining). A RT-PCR for SARS-CoV-2 in RNA extracted from a frozen biopsy tissue sample was negative (RT-PCR was similarly negative in blood) (Figure, A). Nevertheless, electron microscopy study (Figure, E and F) disclosed, in the podocytes cytoplasm, vacuoles containing numerous spherical particles that had the typical appearance of viral inclusion bodies reported with SARS-CoV-2 3 .",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": [
        {
            "text": "The authors have no disclosures related to this manuscript to report.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Disclosures:"
        }
    ]
}