{
    "paper_id": "57225416dee77d54a6b9dd0de2ade8f5730ef897",
    "metadata": {
        "title": "Journal of Visceral Surgery Covid-19 crisis?",
        "authors": [
            {
                "first": "K",
                "middle": [],
                "last": "Slim",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Clermont-Ferrand Teaching Hospital",
                    "location": {
                        "addrLine": "1 place Lucie-et-Raymond-Aubrac",
                        "postCode": "63003",
                        "settlement": "Clermont-Ferrand"
                    }
                },
                "email": "kslim@chu-clermontferrand.fr"
            },
            {
                "first": "J",
                "middle": [],
                "last": "Veziant",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Clermont-Ferrand Teaching Hospital",
                    "location": {
                        "addrLine": "1 place Lucie-et-Raymond-Aubrac",
                        "postCode": "63003",
                        "settlement": "Clermont-Ferrand"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "J o u r n a l P r e -p r o o f 3 of appendicitis (diameter just above the limit, doubt about a peritoneal effusion, etc.) that would in principle qualify for emergency surgery. Likewise, there might be a temptation to treat uncomplicated acute cholecystitis medically to defer surgery until after the Covid-19 crisis has passed, or to opt for a colonic stent for an obstructive tumour rather than immediate surgery.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "These three simple situations (we could cite others) raise an ethical question. Do the current critical circumstances permit us to make exceptions to the rules of good practice? In the examples given, the right course would be appendectomy when a patient does not strictly meet the conditions for nonsurgical treatment [5] , emergency cholecystectomy [6] , and first-line surgery for an occlusive colonic tumour [7] . The answer must be no. We cannot let proper management of surgical emergencies be a collateral victim of the Covid-19 crisis.",
            "cite_spans": [
                {
                    "start": 319,
                    "end": 322,
                    "text": "[5]",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 351,
                    "end": 354,
                    "text": "[6]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 412,
                    "end": 415,
                    "text": "[7]",
                    "ref_id": "BIBREF4"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "What can be done? The simplest solution would be to transfer the patients to hospitals (public or private) that are not under pressure from Covid-19. It might be judicious to have separate Covid-19 and non-Covid-19 medical teams. It would also be appropriate to deliver a clear message to the general public that urgent surgery will not be neglected, and that confinement will not impede emergency consultations.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "What solutions?"
        },
        {
            "text": "It is essential that we do not degrade the quality of our surgical practice because of the Covid-19 emergency. Urgent surgery is still urgent and demands diligent care. If provided in safe conditions, such surgery can be performed in an out-patient care or enhanced recovery programme. If no operating room or post-operative recovery room is available, non-surgical treatment must be considered only in cases where it is justified by strong evidences.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "What solutions?"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Covid-19 and the Digestive System",
            "authors": [
                {
                    "first": "S",
                    "middle": [
                        "H"
                    ],
                    "last": "Wong",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "N"
                    ],
                    "last": "Lui",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "J"
                    ],
                    "last": "Sung",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "J Gastroenterol Hepatol",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {
                "DOI": [
                    "10.1111/jgh.15047"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Health crisis related to COVID-19: treatment modalities of acute uncomplicated appendicitis in adults managed by antibiotics alone as an alternative to appendectomy",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Collard",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "H"
                    ],
                    "last": "Lefevre",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Maggiori",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "J Visc Surg",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Clinical practice. Acute appendicitis--appendectomy or the \"antibiotics first\" strategy",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "R"
                    ],
                    "last": "Flum",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "N Engl J Med",
            "volume": "372",
            "issn": "",
            "pages": "1937--1980",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Comparative operative outcomes of early and delayed cholecystectomy for acute cholecystitis: a population-based propensity score analysis",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "De Mestral",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [
                        "D"
                    ],
                    "last": "Rotstein",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Laupacis",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Hoch",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Zagorski",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "S"
                    ],
                    "last": "Alali",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Ann Surg",
            "volume": "259",
            "issn": "",
            "pages": "10--15",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Self-expandable metal stents for obstructing colonic and extracolonic cancer: European Society of Gastrointestinal Endoscopy (ESGE) Clinical Guideline",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "E"
                    ],
                    "last": "Van Hooft",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "E"
                    ],
                    "last": "Van Halsema",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Vanbiervliet",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "G"
                    ],
                    "last": "Beets-Tan",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Dewitt",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Donnellan",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Endoscopy",
            "volume": "46",
            "issn": "",
            "pages": "990--1053",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}