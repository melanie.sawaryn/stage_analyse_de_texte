{
    "paper_id": "e0c3f216d660a5fe00d84faa9f10dbc978ca6d18",
    "metadata": {
        "title": "Human Bocavirus: A New Viral Pathogen",
        "authors": [
            {
                "first": "Larry",
                "middle": [
                    "J"
                ],
                "last": "Anderson",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "In this issue of Clinical Infectious Diseases, Allander et al. [1] report a study of the recently discovered human bocavirus (HBoV) in children hospitalized with wheezing. This study is a nice example of what is likely to be an increasingly common challenge and opportunity in infectious diseases: determining what diseases novel pathogens cause. HBoV was first described in 2005 after large-scale molecular screening for virus genome sequences led to its discovery in respiratory specimens [2] . Among the sequences identified were those from a novel parvovirus related to minute virus of canine and bovine parvovirus that was designated \"human bocavirus.\" Because the virus sequences were amplified from respiratory specimens, the investigators hypothesized that the virus would cause respiratory disease, and they and others have since documented the presence of HBoV in 1.5%-11.3% of respiratory specimens obtained from patients with acute respiratory illness. Another new human parvovirus, PAR4, was recently identified by large-scale molecular screening for viruses, this time in blood specimens obtained from patients with acute HIV syndrome [3] . However, PAR4",
            "cite_spans": [
                {
                    "start": 63,
                    "end": 66,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 491,
                    "end": 494,
                    "text": "[2]",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 1149,
                    "end": 1152,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "has not yet been linked to human disease. Given that there has been improvement in the tools used to detect and characterize pathogens (e.g., microarray technology, consensus PCR assays, and high-throughput sequencing, such as high-density picrolitre reactors [4] ), there is likely to be a growing number of new viruses to study. For example, in the past \u223c5 years, 4 novel viruses (excluding HBoV) have been detected in respiratory specimens obtained from patients with acute respiratory illness. Human metapneumovirus and 2 coronaviruses, severe acute respiratory syndrome coronavirus (SARS CoV) and NL63, were first isolated in tissue culture and were then characterized by different methods designed to amplify and sequence novel viruses [5] [6] [7] [8] [9] [10] . Another human coronavirus, HKU1, and multiple bat coronaviruses were identified with PCR assays designed to amplify any coronavirus genome [11, 12] . Large-scale molecular screening has recently been used to identify numerous novel viruses from coastal waters [13] .",
            "cite_spans": [
                {
                    "start": 260,
                    "end": 263,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 742,
                    "end": 745,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 746,
                    "end": 749,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 750,
                    "end": 753,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 754,
                    "end": 757,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 758,
                    "end": 761,
                    "text": "[9]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 762,
                    "end": 766,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 908,
                    "end": 912,
                    "text": "[11,",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 913,
                    "end": 916,
                    "text": "12]",
                    "ref_id": "BIBREF11"
                },
                {
                    "start": 1029,
                    "end": 1033,
                    "text": "[13]",
                    "ref_id": "BIBREF12"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Once a novel virus has been detected, an important next step is to determine what disease it causes. Many novel viruses have been first detected in specimens obtained from patients with a specific illness, which, in turn, provided clues to possible disease associations. When the initial de-tection of a virus provides few clues to disease associations, looking for possible disease associations is, at best, challenging.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Acute respiratory illness and, now, wheezing are hypothesized to be caused by HBoV infection. Koch's postulates have provided the standard for establishing a causal link between a pathogen and disease. Time-modified postulates that account for changes in our understanding of pathogens and disease in general include (1) consistently finding the pathogen in patients with the disease more often than in control subjects, (2) replicating the disease after challenging an appropriate animal with the pathogen, and (3) reisolating the pathogen from the challenged ill animal. A causal relationship is also supported by demonstration of the pathogen in affected tissue (especially histologically), demonstration of an immune response to the pathogen, and prevention of disease with a specific intervention, such as immune therapy or vaccination.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Progress in establishing a causal link between newly discovered viruses and disease has often been aided by the availability of stored specimens from patients with the disease in question. Well-characterized stored specimens with good clinical and epidemiologic data from patients with a variety of illnesses are a valuable resource for identifying and characterizing virus-disease associations. However, without appropriate controls, such specimens cannot provide evidence of an association between the pathogen and the disease.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Two recent studies of HBoV included control subjects and demonstrated an association between HBoV infection and acute respiratory illness, but whether HBoV actually causes acute respiratory illness remains uncertain, especially given the high rate at which other viruses are detected in HBoV-positive specimens [14] [15] [16] . The study by Allander et al. [1] in this issue of Clinical Infectious Diseases suggests a condition that is possibly associated with HBoV infection: wheezing. Their findings include detection of HBoV in 49 of 259 nasopharyngeal aspirate specimens obtained from children who had been hospitalized with wheezing, a high rate of other respiratory pathogens (37 [76%] of 49 specimens) in the HBoV-positive nasopharyngeal aspirate specimen, HBoV DNA in \u223c50% of acute-phase and 20% of convalescent-phase serum specimens, and no HBoV DNA in specimens obtained from 64 asymptomatic control subjects. These findings add to our understanding of HboV infection, but the control specimens are not sufficiently well matched to demonstrate an association between HBoV and wheezing. The likelihood of detecting most respiratory viruses in children depends on the type of specimen, the age of the child, and time of the year. In this study, differences in the type of specimen collected (nasal swab specimens for the control subjects and nasopharyngeal aspirate specimens for the case patients) and in the subjects' ages (median age for control subjects and case patients, 4.1 and 1.6 years, respectively) were considerable enough to possibly affect detection rates. We are not given sufficient information to know whether differences in the timing of collection of specimens from HBoV-positive subjects and control subjects may have contributed to the differences in detection rates.",
            "cite_spans": [
                {
                    "start": 311,
                    "end": 315,
                    "text": "[14]",
                    "ref_id": "BIBREF13"
                },
                {
                    "start": 316,
                    "end": 320,
                    "text": "[15]",
                    "ref_id": "BIBREF14"
                },
                {
                    "start": 321,
                    "end": 325,
                    "text": "[16]",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 357,
                    "end": 360,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "We are likely to see an increasing number of novel viruses discovered, and it is worth designing studies to take advantage of this opportunity. Appropriately collected, handled, and stored specimen sets with good clinical and epidemiologic data and institutional review board approval for future testing will be great assets in efforts to identify and evaluate novel virusdisease associations. Such specimen sets that also have appropriate controls will be even more valuable; they allow investigators to determine which associations are likely important and worth pursuing.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Human bocavirus and acute wheezing in children",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Allander",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Jartti",
                    "suffix": ""
                },
                {
                    "first": "Hgm",
                    "middle": [],
                    "last": "Niesters",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Clin Infect Dis",
            "volume": "44",
            "issn": "",
            "pages": "904--910",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Cloning of a human parvovirus by molecular screening of respiratory tract samples",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Allander",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "T"
                    ],
                    "last": "Tammi",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Eriksson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Bjerkner",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Tiveljung-Lindell",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Andersson",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "102",
            "issn": "",
            "pages": "12891--12897",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "New DNA viruses identified in patients with acute viral infection syndrome",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "S"
                    ],
                    "last": "Jones",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Kapoor",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [
                        "V"
                    ],
                    "last": "Lukashov",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Simmonds",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Hecht",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Delwart",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "8230--8236",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Genome sequencing in microfabricated highdensity picolitre reactors",
            "authors": [
                {
                    "first": "M",
                    "middle": [],
                    "last": "Margulies",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Egholm",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [
                        "E"
                    ],
                    "last": "Altman",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Nature",
            "volume": "437",
            "issn": "",
            "pages": "376--80",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "A previously undescribed coronavirus associated with respiratory disease in humans",
            "authors": [
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Fouchier",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "G"
                    ],
                    "last": "Hartwig",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "M"
                    ],
                    "last": "Bestebroer",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Proc Natl Acad Sci U S A",
            "volume": "101",
            "issn": "",
            "pages": "6212--6218",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Identification of a new human coronavirus",
            "authors": [
                {
                    "first": "L",
                    "middle": [],
                    "last": "Van Der Hoek",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Pyrc",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "F"
                    ],
                    "last": "Jebbink",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat Med",
            "volume": "10",
            "issn": "",
            "pages": "368--73",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Identification of a novel coronavirus in patients with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "C",
                    "middle": [],
                    "last": "Drosten",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Gunther",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Preiser",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1967--76",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "A novel coronavirus associated with severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "T",
                    "middle": [
                        "G"
                    ],
                    "last": "Ksiazek",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "D"
                    ],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "S"
                    ],
                    "last": "Goldsmith",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "N Engl J Med",
            "volume": "348",
            "issn": "",
            "pages": "1953--66",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Coronavirus as a possible cause of severe acute respiratory syndrome",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Peiris",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "T"
                    ],
                    "last": "Lai",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "L"
                    ],
                    "last": "Poon",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "Lancet",
            "volume": "361",
            "issn": "",
            "pages": "1319--1344",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "A newly discovered human pneumovirus isolated from young children with respiratory tract disease",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "G"
                    ],
                    "last": "Van Den Hoogen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "De Jong",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Groen",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Nat Med",
            "volume": "7",
            "issn": "",
            "pages": "719--743",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Characterization and complete genome sequence of a novel coronavirus, coronavirus HKU1, from patients with pneumonia",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "C"
                    ],
                    "last": "Woo",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "K"
                    ],
                    "last": "Lau",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "M"
                    ],
                    "last": "Chu",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J Virol",
            "volume": "79",
            "issn": "",
            "pages": "884--95",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Review of bats and SARS",
            "authors": [
                {
                    "first": "L-F",
                    "middle": [],
                    "last": "Wang",
                    "suffix": ""
                },
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Shi",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Zhang",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Field",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Daszak",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "T"
                    ],
                    "last": "Eaton",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Emerg Infect Dis",
            "volume": "12",
            "issn": "",
            "pages": "1834--1874",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Metagenomic analysis of coastal RNA virus communities",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "I"
                    ],
                    "last": "Culley",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "S"
                    ],
                    "last": "Lang",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "A"
                    ],
                    "last": "Suttle",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Science",
            "volume": "312",
            "issn": "",
            "pages": "1795--1803",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Human bocavirus infection in young children in the United States: molecular epidemiological profile and clinical characteristics of a newly emerging respiratory virus",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Kesebir",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Vazquez",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Weibel",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Infect Dis",
            "volume": "194",
            "issn": "",
            "pages": "1276--82",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "Epidemiological profile and clinical associations of human bocavirus and other human parvoviruses",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Manning",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Russell",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Eastick",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Infect Dis",
            "volume": "194",
            "issn": "",
            "pages": "1283--90",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Human bocavirus: developing evidence for pathogenicity",
            "authors": [
                {
                    "first": "K",
                    "middle": [],
                    "last": "Mcintosh",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "J Infect Dis",
            "volume": "194",
            "issn": "",
            "pages": "1197--1206",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": [
        {
            "text": "Potential conflicts of interest. L.J.A.: no conflicts.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgments"
        }
    ]
}