{
    "paper_id": "78f43cdff4cb4012ad36a5697a3e2e54aa1b69aa",
    "metadata": {
        "title": "Three clusters of bovine kobuvirus isolated in Korea",
        "authors": [
            {
                "first": "Hye-Young",
                "middle": [],
                "last": "Jeoung",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Ji-Ae",
                "middle": [],
                "last": "Lim",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Wooseog",
                "middle": [],
                "last": "Jeong",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Jae-Ku",
                "middle": [],
                "last": "Oem",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "Dong-Jun",
                "middle": [],
                "last": "An",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "Fecal samples (n = 107) were collected from cattle with ascertained or suspected diarrheal disease on Korean farms during 2008-2010. Of these, 37 samples tested positive for bovine kobuvirus. The 37 positive samples came from 32 cattle that exhibited diarrhea and five cattle that were non-diarrhetic. The majority of the viruspositive feces samples were from calves under 1 month of age (n = 25). Nine of the 37 cattle infected with bovine kobuvirus were confirmed to have a co-infection with other viruses including bovine rotavirus (n = 3), bovine coronavirus (n = 1), bovine viral diarrhea virus (n = 1), and both bovine coronavirus and bovine viral diarrhea virus (n = 4). A neighbor-joining tree grouped 36 of the Korean kobuvirus strains (with the exception of the KB8 strain) into three clusters (G1, G3, and G4), while strains derived from Thailand and Japan (except the U1 strain) were included in the G2 cluster. The results indicated that Korean bovine kobuvirus has diverse lineages regardless of disease status and species.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Gyongbuk (n = 10), and Gyongnam (n = 4). Viral RNA was extracted from feces using TRIzol LS b according to the manufacturer's instructions (Invitrogen, Carlsbad, CA, USA). Bovine kobuvirus was detected from fecal samples using reverse-transcript-polymerase chain reaction (RT-PCR) as previously described [3] . Oligonucleotide primers were designed based on the genome sequence of the U-1 strain (Accession No. AB084788) and have the following sequences: U-1f (sense, 5 0 -CATGCTCCTCGGTGGTCT CA-3 0 ; nt 7,357) and U-1r (antisense, 5 0 -GTCCGGGTC CATCACAGGGT-3 0 ; nt 7,987). Together, these primers amplify a 631-bp region of the 3D protein. PCR products of size 631 bp were visualized by electrophoresis and were cloned using the pGEM-T Vector System II (Promega, Madison, WI, USA). The cloned genes (three per sample) were sequenced, using T7 and SP6 promoter-specific primers, with an ABI Prism \u00d2 3730XI DNA Sequencer (Applied Biosystems, Foster City, CA, USA) at the Macrogen Institute (Macrogen, Seoul, Korea). To investigate the relationship between kobuvirus and other bovine viruses that cause diarrhea in cattle, a screening test was conducted using primers specific for the detection of bovine rotavirus (BRV) [9] , bovine coronavirus (BCV) [10] , and bovine viral diarrhea virus (BVDV) [11] , as previously described. Reverse transcription for the extracted RNA was performed using a cDNA synthesis kit (TaKaRa) and random hexanucleotide primers. The RT-PCR was run according to the following temperaturetime profile: 42\u00b0C for 30 min, then 94\u00b0C for 5 min, followed by 35 cycles of virus-specific conditions, as follows: BRV: 94\u00b0C for 30 s, 54\u00b0C for 1 min and 72\u00b0C for 1 min; BCV: 94\u00b0C for 1 min, 58\u00b0C for 1 min and 72\u00b0C for 2 min; and BVDV: 94\u00b0C for 1 min, 56\u00b0C for 1 min and 72\u00b0C for 1 min. For all viruses, the 35 denaturation-annealingextension cycles were followed by a final extension at 72\u00b0C for 10 min. The resulting amplicon sizes were 309 bp for BRV, 730 bp for BCV, and 288 bp for BVDV. The sizes were assessed by 1% agarose gel electrophoresis and confirmed through the sequencing and analysis of the nucleotide sequence of each amplicon.",
            "cite_spans": [
                {
                    "start": 305,
                    "end": 308,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 1221,
                    "end": 1224,
                    "text": "[9]",
                    "ref_id": null
                },
                {
                    "start": 1252,
                    "end": 1256,
                    "text": "[10]",
                    "ref_id": null
                },
                {
                    "start": 1298,
                    "end": 1302,
                    "text": "[11]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The nucleotide sequences of the Korean bovine kobuviruses were compared to those of kobuvirus reference strains in the GenBank database by BLAST. The nucleotide sequences were aligned using the Clustal W 1.8x program [12] and.aln files were generated. The.aln files were then converted to.meg files using Mega 4 [13] and a neighbor-joining tree was constructed (bootstrap replicates = 1,000) using the Kimura 2 parameter method for pairwise deletion at uniform rates. The nucleotide sequences of Korean bovine kobuvirus strains were deposited in GenBank as accession numbers HQ650164-HQ650200 (Table 1) .",
            "cite_spans": [
                {
                    "start": 217,
                    "end": 221,
                    "text": "[12]",
                    "ref_id": null
                },
                {
                    "start": 312,
                    "end": 316,
                    "text": "[13]",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 593,
                    "end": 602,
                    "text": "(Table 1)",
                    "ref_id": "TABREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "In prior studies, bovine kobuvirus was detected in 12 of 72 (16.7%) stool samples in Japan [3] , 6 of 72 (8.3%) fecal samples in Thailand [5] , and 2 of 32 (6.25%) fecal samples in Hungary [6] . Korean bovine kobuvirus was markedly more prevalent, being detected in 37 of the 107 (34.6%) fecal samples. Furthermore, the yearly frequency of the Korean positive samples was constant: n = 12 in both 2008 and 2009, and n = 13 in 2010 (Table 1) . Thirty-two of the 86 diarrhea samples (37.2%) contained kobuvirus, compared with 5 out of 21 non-diarrhea samples (23.8%). However, this result cannot be taken as evidence of a causal relationship between kobuvirus infection and diarrhea, and such a causal relationship has been questioned in previous analyses [3, [5] [6] [7] .",
            "cite_spans": [
                {
                    "start": 91,
                    "end": 94,
                    "text": "[3]",
                    "ref_id": null
                },
                {
                    "start": 138,
                    "end": 141,
                    "text": "[5]",
                    "ref_id": null
                },
                {
                    "start": 189,
                    "end": 192,
                    "text": "[6]",
                    "ref_id": null
                },
                {
                    "start": 754,
                    "end": 757,
                    "text": "[3,",
                    "ref_id": null
                },
                {
                    "start": 758,
                    "end": 761,
                    "text": "[5]",
                    "ref_id": null
                },
                {
                    "start": 762,
                    "end": 765,
                    "text": "[6]",
                    "ref_id": null
                },
                {
                    "start": 766,
                    "end": 769,
                    "text": "[7]",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 431,
                    "end": 440,
                    "text": "(Table 1)",
                    "ref_id": "TABREF0"
                }
            ],
            "section": ""
        },
        {
            "text": "Infection by kobuvirus occurred in 38.5% (30 of 78) of the Korean native cattle and 24.1% (7 of 29) of the Holstein cattle. This result indicated that kobuvirus infection is not restricted to a single cattle species. Regarding the age of infected cattle, this study clearly showed a predominance of infection in calves under the age of 1 month (n = 25), a result similar to that of a previous study [5] . Kobuvirus prevalence by geographic region was 45.2% (19/42) in Chungnam, 30% (3/10) in Gyongbuk, 28.1% (9/ 32) in Gyonggi, 27.3% (3/11) in Chungbuk, 25% (2/8) in Gangwon, and 25% (1/4) in Gyongnam. In spite of these seemingly substantial differences in prevalence, the low numbers of samples did not provide sufficient statistical power to allow any conclusions regarding geographic predilection. The geographic differences can therefore be considered tenuous at this point, requiring further study with larger sample sizes. The clinical significance of any such differences remains unclear. The combined infection involving bovine kobuvirus and other viruses was observed in nine cattles: BRV (n = 3), BCV (n = 1), BVDV (n = 1), and BCV ? BVDV (n = 4). However, it is unclear whether the other viruses are directly associated with the kobuvirus infection. Neighbor-joining analysis revealed that partial nucleotide sequences (590 bp in length) of the 3D genes of 56 bovine kobuvirus (37 from Korea, 13 from Japan, and 6 from Thailand), along with that of the Aichi virus (as the outgroup), fell into four main lineages (G1, G2, G3, and G4). With the exception of the U1 and KB8 strains, all of the sequences fell into one of these four lineages (Fig. 1) . The four lineages were supported by high bootstrap values (75-99%) at the node of each branch. Interestingly, the 36 Korean kobuvirus strains formed three lineages (G1, G3, and G4), while the 12 Japanese and six Thailand strains all fell within the G2 lineage (Fig. 1) . A future analysis using a larger number of strains may be required to confirm that the U1 and KB8 strains represent the first recognized strains of an additional cluster or two additional clusters.",
            "cite_spans": [
                {
                    "start": 399,
                    "end": 402,
                    "text": "[5]",
                    "ref_id": null
                },
                {
                    "start": 534,
                    "end": 540,
                    "text": "(3/11)",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 1651,
                    "end": 1659,
                    "text": "(Fig. 1)",
                    "ref_id": null
                },
                {
                    "start": 1922,
                    "end": 1930,
                    "text": "(Fig. 1)",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "In conclusion, the findings of this study demonstrate the existence of four phylogenetic lineages of bovine kobuvirus. Korean kobuvirus strains are found in three of the four lineages, with Japanese and Thailand strains being clustered together in the other lineage.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Virus Taxonomy, 8th Report of the ICTV",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Stanway",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [],
                    "last": "Brown",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Christian",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Hovi",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Hyypiae",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "M Q"
                    ],
                    "last": "King",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [
                        "J"
                    ],
                    "last": "Knowles",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "M"
                    ],
                    "last": "Lemon",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "D"
                    ],
                    "last": "Minor",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "A"
                    ],
                    "last": "Pallansch",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "C"
                    ],
                    "last": "Palmenburg",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Skern",
                    "suffix": ""
                },
                {
                    "first": "Picornaviridae",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "757--778",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Fig. 1 Neighbor-joining tree for nucleotide sequences of bovine kobuvirus strains. Partial nucleotide sequences (590 bp in length) from the 3D genes of 56 global bovine kobuvirus (37 Korean, 13 Japanese, and 6 Thailand), along with the Aichi virus as an outgroup, are shown. Bootstrap percentages are shown above those nodes that are supported in at least 60% of the 1,000 replicates. Scale bar indicates nucleotide substitutions per site",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "Summary of bovine kobuvirus strains",
            "latex": null,
            "type": "table"
        },
        "TABREF1": {
            "text": "",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "Acknowledgment The authors are grateful to Ms. Bo-Hye Shin and Ms. Hyen-Jung Kim for their technical assistance.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        }
    ]
}