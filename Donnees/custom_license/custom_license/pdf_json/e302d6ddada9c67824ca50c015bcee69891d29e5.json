{
    "paper_id": "e302d6ddada9c67824ca50c015bcee69891d29e5",
    "metadata": {
        "title": "Establishing a surveillance network for severe lower respiratory tract infections in Korean infants and young children",
        "authors": [
            {
                "first": "J.-K",
                "middle": [],
                "last": "Chun",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J.-H",
                "middle": [],
                "last": "Lee",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "H.-S",
                "middle": [],
                "last": "Kim",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "H.-M",
                "middle": [],
                "last": "Cheong",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "K",
                "middle": [
                    "S"
                ],
                "last": "Kim",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "C",
                "middle": [],
                "last": "Kang",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "D",
                "middle": [
                    "S"
                ],
                "last": "Kim",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [
        {
            "text": "To reduce morbidity and mortality through integrated case management, a pilot study to detect respiratory viruses in patients with acute lower respiratory infections (ALRIs) was designed as part of a nationwide surveillance for this disease in Korea. The study population consisted of hospitalized patients under the age of 5 years with bronchiolitis, pneumonia, croup, or acute respiratory distress syndrome. A prospective 6-month study was performed. Two hundred and ninety-seven nasopharyngeal secretions were collected and multiplex reverse transcriptase polymerase chain reactions (RT-PCR)/polymerase chain reactions (PCR) were performed to detect respiratory viruses. If there were any positive RT-PCR/PCR results, viral cultures were proceeded for confirmation. Respiratory viruses were identified in 49.6% of 296 patients. The detection rates were as follows: respiratory syncytial virus (RSV) was the most commonly detected in 52.7% (87/165), human metapneumovirus (hMPV) in 15.8%, human corona virus (hCoV) in 5.5%, adenovirus in 9.7%, human bocavirus (hBoV) in 5.5%, parainfluenza virus (PIV) in movirus (hMPV), human bocavirus (hBoV), and human corona viruses (hCoV)-OC 43, 229E were scanned by the multiplex reverse transcriptase polymerase chain reactions (RT-PCR)/polymerase chain reactions (PCR) Eur J Clin Microbiol Infect Dis (",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "3.6%, rhinovirus (RV) in 4.2%, and the influenza virus in 3% of the patients with ALRIs. The consistent rate of positive results between RT-PCR and viral culture was 92% (105/114). Using our methods to detect viral causes seemed to be acceptable for the national surveillance of severe acute respiratory infections in infants and children.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Acute lower respiratory infections (ALRIs) has a major impact on health services and account for up to 50% of visits by children to health facilities. Many emerging viral infections causing ALRIs may not be detected until the middle of an outbreak. In 2006 spring, throughout almost every tertiary hospital in South Korea, certain patients were admitted for more than 1 month due to fatal respiratory infections and pediatricians could not determine the causal virus of the outbreak. Rapidly progressive interstitial pneumonia, subsequent pneumothorax, prolonged ventilator care with diagnosis of acute respiratory distress syndrome (ARDS), and multiple organ failures were typical disease progression. Early molecular viral studies on possible patients could be the first key to detecting causal viruses [1, 2] , so a nationwide surveillance system to control the spreading of infections was designed by the Korea Centers for Disease Control and Prevention (KCDC).",
            "cite_spans": [
                {
                    "start": 805,
                    "end": 808,
                    "text": "[1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 809,
                    "end": 811,
                    "text": "2]",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "This pilot study was conducted to establish a nationwide surveillance system of ALRIs in infants and children under 5 years of age in South Korea. Including classic respiratory viruses, such as the respiratory syncytial virus (RSV), parainfluenza virus (PIV), influenza virus, adenovirus, and rhinovirus (RV), and recently discovered viruses, e.g., human metapneu-method. In detecting causal respiratory viruses, this molecular tool has been known to be the most sensitive [3] . In the present study, the feasibility of establishing a hospital-based respiratory viral surveillance system and a prompt reporting system between tertiary hospitals and the KCDC were investigated.",
            "cite_spans": [
                {
                    "start": 473,
                    "end": 476,
                    "text": "[3]",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "ALRI patients under 5 years of age (n=296) admitted in Severance Children's Hospital, which is one of the largest tertiary hospitals in South Korea, were examined prospectively between November 2007 and April 2008 for 6 months. Case definitions of moderate to severe lower respiratory infections were determined according to the World Health Organization (WHO) recommended surveillance standards 2nd edition (1999). As part of the diagnosis, pneumonia, croup, bronchiolitis, and ARDS were included, which were diagnosed by the same physician through physical examination and chest X-rays. Samples were collected by nasopharyngeal aspiration with viral transport media prospectively and informed consent was obtained. This study was approved by the Institutional Review Board of Severance Hospital. Questionnaires asked about the onset and manifestation of symptoms, admission and duration of stay, sample collection, risk factors, e.g., parents smoking, prematurity, formula feeding, gastroesophageal reflux, neurologic deficits, cardiovascular defects, initial diagnosis, radiographic findings, blood tests, and treatments. The information was provided by the same pediatrician and was entered into an Access 2007 database (Microsoft) by a person blinded to the viral results. Immunocompromised patients, such as patients receiving chemotherapy due to various cancers, those taking immunosuppressants, patients who were admitted to the neonatal intensive care unit, and patients with severe complex heart diseases, were excluded. All analyses were performed with GraphPad Prism version 5.00 for Windows (GraphPad Software Inc., San Diego, CA, USA). A P-value<0.05 was considered to be significant. Viral DNA or RNA was extracted with a QIAamp Viral Mini Kit (QIAGEN, Hilden, Germany), and One-step Multiplex PCR/RT-PCR reagents and the Labopass\u2122 RV Detection kit (Cosmo GENETECH, Seoul, South Korea), which was developed by the KCDC influenza and respiratory virus team with Cosmo GENETECH, were used. Samples showing any positive results for adenovirus, influenza virus, PIV, and RSV by multiplex PCR methods underwent viral culture through R-Mix\u2122 ReadyCells (Diagnostic Hybrids, Athens, US).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Among a total of 296 patients, 297 samples were collected. Of 297 samples, 147 samples (49.6%) were positive and, in total, 165 viruses were detected through this study. RSV was the most commonly detected in 52.7% (87/165), hMPV in 15.8% (26/165), adenovirus in 9.7% (16/165), hBoV in 5.5% (9/165), hCoV in 5.5% (9/165), RV in 4.2%(7/165), PIV in 3.6% (6/165), and the influenza virus in 3% (5/165) of the patients with ALRIs. The coinfection rate was 11.6%. The detection frequency of each virus in ALRIs with respect to demographic features and clinical manifestations are depicted in Table 1 . The elapsed period of the initial presentation of symptoms and the time of sampling were not significantly different between the virus-positive samples and the virus-negative samples (data not shown). However, among the detected viruses, significant differences in the elapse of sampling was noted by one-way analysis of variance (ANOVA; P=0.03). The sampling time was prominently different between adenovirus and hMPV (P<0.01), but there was no significant difference between RSV and hMPV or hBo and hMPV (Fig. 1) .",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 587,
                    "end": 594,
                    "text": "Table 1",
                    "ref_id": "TABREF0"
                },
                {
                    "start": 1103,
                    "end": 1111,
                    "text": "(Fig. 1)",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "In the course of this study, RSV outbreak was observed from November to December such that RSV was detected in about 70% of cases enrolled in this period. There was no significant difference in the clinical outcomes, such as length of hospital stay, indices of respiratory difficulty, and oxygen therapy among groups of RSV infections (75 cases), RSV+hMPV coinfections (8 cases), and hMPV infections (13 cases).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Pneumonia was diagnosed in 75%, bronchiolitis in 17%, croup in 7%, and ARDS in 1% of patients. Among the patients, 15/296 (5.4%) needed oxygen therapy and 8/296 (2.7%) were treated with a ventilator. In ARDS patients (n= 4), only two cases were revealed as causal viruses, which were adenovirus and RSV. Two cases resulted in mortality in spite of meticulous treatments.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A total of 211 parents responded to the questionnaires, 144 patients were breast-fed (68.2%) for more than 1 month. The ages of 42 of 211 (20%) patients were less than 6 months old. In this group, breast-feeding did not affect the rate of virus detection (P=0.23). Among formula-fed infants, there was no difference in terms of viral detection between the older than 6 months and younger than 6 months groups (P=0.68). Of the parents, 106/211 smoked (50.2%). The respiratory difficulty index, items such as nasal flaring, tachypnea (RR>40/min), or chest wall retractions, were significantly higher in the smoking group as determined by logistic regression analysis with other cofactors (RSV infection and asthma) (P=0.04). ALRI patients with a history of prematurity (gestational age 26-36 weeks) were found in 12 patients (4%). Their median age was 19 months. One patient had received RSV monoclonal antibody. In this group, there were 6 of 12 virus-positive patients (50%), all of which were RSV, except for one case.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The positive samples in which a single virus was detected by multiplex RT-PCR/PCR were put into viral cultures. RSV, adenovirus, PIV 1,2,3, and influenza A and B were isolated using this method. Viral cultures were performed in 114 samples and consistent results with RT-PCR/PCR were produced in 105 samples. The agreement rate was 92%.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "By using a viral nucleic acid multiplication method with high sensitivity, physicians can manage ALRIs with prolonged fever by only administering supportive care instead of switching to another type of antibiotics. This diagnostic method is being adopted for more accurate diagnosis, although the cost-effectiveness has still not been established. However, inappropriately prescribing antibiotics in viral respiratory infections would be resolved only by rigorous diagnosis [4] . In the present study, amoxicillin +clavulanate was the most frequently prescribed treatment for pneumonia. In case of adenovirus or influenza viral infections, prolonged fever was a striking feature, but we could wait without adding other antibiotics based on viral laboratory data and the coherent features of chest radiographs.",
            "cite_spans": [
                {
                    "start": 474,
                    "end": 477,
                    "text": "[4]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Questions remain as to whether multiplex PCR/RT-PCR methods are exclusively suitable for a respiratory surveillance network. Other methods, such as immunochromatography, direct fluorescent antibody (DFA) staining, or enzyme immunoassays, are producing compatible results to molecular studies in RSV, influenza virus, PIV (93-99%), and hMPV (85-95%) [3, [5] [6] [7] . If it is suitable, a commercial rapid kit could be used first as mass screening and in negative samples, and the adoption of a reflex PCR method could be considered. And the rapid culture kit also enables the physicians to make a timely diagnosis of conventional viral infections. Adenovirus and influenza viruses were isolated well in both rapid shell viral cultures and RT-PCR/PCR methods. In our study, about 92% agreement was obtained between PCR and rapid shell viral culture. In another study of similar populations, 80% concordance was report [8] . However, recently identified viruses such as hBoV, hMPV, RV, or hCoV are known as hardly cultured viruses but well detected by multiplex PCR/ RT-PCR methods. So, the mass screening of respiratory viruses (including unknown viruses) for severe respiratory infections would be more accurate and desirable with viral nucleic acid extractions from samples and subsequent identification with primers [8, 9] . When only one of each viral gene was targeted, true positive samples could be missed by this method [10] . In this study, two kinds of primers were included in the multiplex RT-PCR kits for hMPV, hCoV, and influenza virus A and B.",
            "cite_spans": [
                {
                    "start": 349,
                    "end": 352,
                    "text": "[3,",
                    "ref_id": "BIBREF2"
                },
                {
                    "start": 353,
                    "end": 356,
                    "text": "[5]",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 357,
                    "end": 360,
                    "text": "[6]",
                    "ref_id": "BIBREF5"
                },
                {
                    "start": 361,
                    "end": 364,
                    "text": "[7]",
                    "ref_id": "BIBREF6"
                },
                {
                    "start": 917,
                    "end": 920,
                    "text": "[8]",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1318,
                    "end": 1321,
                    "text": "[8,",
                    "ref_id": "BIBREF7"
                },
                {
                    "start": 1322,
                    "end": 1324,
                    "text": "9]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 1427,
                    "end": 1431,
                    "text": "[10]",
                    "ref_id": "BIBREF9"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Hospital-based surveillance for severe respiratory infections have significant merits. The transmission of SARS in the Toronto outbreak was nosocomial in 80% of cases [9] . Rapid viral diagnosis performed in the same hospital would be helpful for preventing nosocomial infections in immunocompromised patients and for more targeted management. In addition to this, strict diagnosis can affect treatment policy directly. If the government supports the total costs of molecular viral diagnosis in tertiary hospitals for national surveillance, early standardized broad respira- Fig. 1 Elapsed time differences between initial symptom presentation and nasopharyngeal samplings in each of the viruses. The bars represent mean values with 95% confidence intervals tory samplings would be possible, regardless of insurance situations or patients' economic states. Thus, this hospitalbased surveillance network is superior to any other sentinel surveillance system in the aspects of appropriate sampling time, elapse period of sampling and examination, and feedback effects from the viral study.",
            "cite_spans": [
                {
                    "start": 167,
                    "end": 170,
                    "text": "[9]",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [
                {
                    "start": 575,
                    "end": 581,
                    "text": "Fig. 1",
                    "ref_id": null
                }
            ],
            "section": ""
        },
        {
            "text": "In conclusion, this pilot study showed that respiratory viral detection using multiplex RT-PCR/PCR was feasible, with great benefits and few limitations to establish a national hospital-based surveillance system for severe lower respiratory infections, and enabled clinicians to anticipate clinical courses and helped using the minimal administration of antibiotics. For governments, a rigorous viral study might be a viable solution in reducing the high resistance and the total medical costs of antibiotics for severe respiratory infections.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "A newly discovered human pneumovirus isolated from young children with respiratory tract disease",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "G"
                    ],
                    "last": "Van Den Hoogen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "De Jong",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Groen",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kuiken",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "De Groot",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Fouchier",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [
                        "D"
                    ],
                    "last": "Osterhaus",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Nat Med",
            "volume": "7",
            "issn": "",
            "pages": "719--724",
            "other_ids": {
                "DOI": [
                    "10.1038/89098"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Severe acute respiratory syndrome (SARS)-paradigm of an emerging viral infection",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Berger",
                    "suffix": ""
                },
                {
                    "first": "Drosten",
                    "middle": [],
                    "last": "Ch",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [
                        "W"
                    ],
                    "last": "Doerr",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "St\u00fcrmer",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Preiser",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Clin Virol",
            "volume": "29",
            "issn": "",
            "pages": "13--22",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcv.2003.09.011"
                ]
            }
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "A sensitive, specific, and cost-effective multiplex reverse transcriptase-PCR assay for the detection of seven common respiratory viruses in respiratory samples",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "W"
                    ],
                    "last": "Syrmis",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "M"
                    ],
                    "last": "Whiley",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Thomas",
                    "suffix": ""
                },
                {
                    "first": "I",
                    "middle": [
                        "M"
                    ],
                    "last": "Mackay",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Williamson",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Siebert",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Nissen",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "P"
                    ],
                    "last": "Sloots",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J Mol Diagn",
            "volume": "6",
            "issn": "",
            "pages": "125--131",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Reducing antibiotic use in influenza: challenges and rewards",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Low",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Clin Microbiol Infect",
            "volume": "14",
            "issn": "",
            "pages": "298--306",
            "other_ids": {
                "DOI": [
                    "10.1111/j.1469-0691.2007.01910.x"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "Comparison of a lateral-flow immunochromatography assay with real-time reverse transcription-PCR for detection of human metapneumovirus",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kikuta",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Sakata",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Gamo",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ishizaka",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Koga",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Konno",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Ogasawara",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Sawada",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Taguchi",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Takahashi",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Yasuda",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Ishiguro",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Hayashi",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Ishiko",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Kobayashi",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "J Clin Microbiol",
            "volume": "46",
            "issn": "",
            "pages": "928--932",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.01888-07"
                ]
            }
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Comparison of the Directigen flu A+B test, the QuickVue influenza test, and clinical case definition to viral culture and reverse transcription-PCR for rapid diagnosis of influenza virus infection",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Ruest",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Michaud",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Deslandes",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "H"
                    ],
                    "last": "Frost",
                    "suffix": ""
                }
            ],
            "year": 2003,
            "venue": "J Clin Microbiol",
            "volume": "41",
            "issn": "",
            "pages": "3487--3493",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.41.8.3487-3493.2003"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Real-time PCR compared to Binax NOW and cytospin-immunofluorescence for detection of influenza in hospitalized patients",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Landry",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Cohen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ferguson",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "J Clin Virol",
            "volume": "43",
            "issn": "",
            "pages": "148--151",
            "other_ids": {
                "DOI": [
                    "10.1016/j.jcv.2008.06.006"
                ]
            }
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Comparison of the Seeplex reverse transcription PCR assay with the R-mix viral culture and immunofluorescence techniques for detection of eight respiratory viruses",
            "authors": [
                {
                    "first": "K",
                    "middle": [
                        "H"
                    ],
                    "last": "Roh",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "H"
                    ],
                    "last": "Nam",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Yoon",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [
                        "K"
                    ],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Lee",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Yoo",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Kim",
                    "suffix": ""
                },
                {
                    "first": "Y",
                    "middle": [],
                    "last": "Cho",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "Ann Clin Lab Sci",
            "volume": "38",
            "issn": "",
            "pages": "41--46",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Early diagnosis of SARS: lessons from the Toronto SARS outbreak",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "P"
                    ],
                    "last": "Muller",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "E"
                    ],
                    "last": "Richardson",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mcgeer",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Dresser",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Raboud",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [],
                    "last": "Mazzulli",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Loeb",
                    "suffix": ""
                },
                {
                    "first": "M; Canadian Sars Research",
                    "middle": [],
                    "last": "Louie",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Network",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Eur J Clin Microbiol Infect Dis",
            "volume": "25",
            "issn": "",
            "pages": "230--237",
            "other_ids": {
                "DOI": [
                    "10.1007/s10096-006-0127-x"
                ]
            }
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Prospective study of human metapneumovirus detection in clinical samples by use of light diagnostics direct immunofluorescence reagent and real-time PCR",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Landry",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Cohen",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ferguson",
                    "suffix": ""
                }
            ],
            "year": 2008,
            "venue": "J Clin Microbiol",
            "volume": "46",
            "issn": "",
            "pages": "1098--1100",
            "other_ids": {
                "DOI": [
                    "10.1128/JCM.01926-07"
                ]
            }
        }
    },
    "ref_entries": {
        "TABREF0": {
            "text": "The clinical manifestations of acute lower respiratory infections (ALRIs) according to each of the viruses",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": [
        {
            "text": "Acknowledgments We would like to thank Dr. Dong-Won Kang who made the database program for this study, Dr. Dae Ryong Kang who provided helpful consultations for the study design, precious members of the Korean Society of Pediatric Infectious Disease who recommended their brilliant opinions, and all of the members of the International Health Care Center at Severance Hospital who supported us in this study. This study was supported by intramural grant from Korea Centers for Disease Control and Prevention.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "acknowledgement"
        },
        {
            "text": "Competing interests None to be declared.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "annex"
        }
    ]
}