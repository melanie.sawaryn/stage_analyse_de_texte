{
    "paper_id": "f84c225b050bc30a55ed5b0004219b4c6475079e",
    "metadata": {
        "title": "Journal Pre-proof Testing Recommendation for COVID-19 (SARS-CoV-2) in Patients Planned for Surgery-Continuing the Service and 'Suppressing' the Pandemic Testing Recommendation for COVID-19 (SARS-CoV-2) in Patients Planned for Surgery-Continuing the Service and 'Suppressing' the Pandemic",
        "authors": [
            {
                "first": "M",
                "middle": [
                    "A"
                ],
                "last": "Al-Muharraqi",
                "suffix": "",
                "affiliation": {},
                "email": "almuharraqi@doctors.org.uk"
            }
        ]
    },
    "abstract": [
        {
            "text": "Currently, planned surgical procedures have been put on hold or have been reduced dramatically during the COVID-19 pandemic. These include essential surgical services like head and neck cancer surgeries. With increased availability of testing for severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) and extensive efforts made to address the numerous supply challenges associated with testing, we have suggested a possible recommendation for surgeons regarding the implementation of a testing protocol while highlighting the available tests, the implications for the tests, and their possible results. This testing recommendation will be based on the known incubation period of the SARS-CoV-2 and its contagiousness during that period.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Abstract"
        }
    ],
    "body_text": [
        {
            "text": "Since the United Kingdom (UK) elevated its response to the unprecedented coronavirus disease (COVID-19) pandemic, the National Health Service in England has asked trusts to postpone all elective surgical procedures for three months starting from 15 April 2020 [1] . In Wales, Scotland, and Northern Ireland, elective operations have already been stopped temporarily. Due to these exceptional circumstances wherein the surge capacity of hospitals must be taken into consideration, frustration is evident for patients as well as surgeons where essential surgeries like cancer surgery are effectively hampered.",
            "cite_spans": [
                {
                    "start": 260,
                    "end": 263,
                    "text": "[1]",
                    "ref_id": "BIBREF0"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The World Health Organisation (WHO) has criticised countries that have not prioritised testing. Tedros Ghebreyesus, the chief executive of WHO, said, \"You cannot fight a fire blindfolded. Our key message is test, test, test.\" [2] . The UK's limited testing approach for the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) has been due to a 'capacity problem' following a nationwide consolidation in the number of pathology laboratories in the past [3] . Priority has been given to (1) patients in hospital intensive care units with suspected COVID-19, (2) patients with severe respiratory illness, (3) isolated cluster outbreaks e.g. care homes, and (4) random testing for surveillance purposes carried out by over 100 general practices [3] . Public Health England has said that by April 2020, its capacity will be boosted to 25,000/day. With testing becoming increasingly available, the only way forward to continue crucial head and neck services is to initiate routine testing as standard for all essential head and neck patients.",
            "cite_spans": [
                {
                    "start": 226,
                    "end": 229,
                    "text": "[2]",
                    "ref_id": null
                },
                {
                    "start": 461,
                    "end": 464,
                    "text": "[3]",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 750,
                    "end": 753,
                    "text": "[3]",
                    "ref_id": "BIBREF3"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "To this end, surgeons need to understand the types of available tests, the circumstances under which they are useful, and the manner in which the tests would influence their practice. We explain herein the types of available tests and suggest a possible recommendation for surgeons to follow when requesting these tests in pre-booked surgical cases.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "There are two categories of SARS-CoV-2 tests: tests that detect the virus itself (viral ribonucleic acid [RNA] ) and tests that detect the host's response to the virus (serological antibodies).",
            "cite_spans": [
                {
                    "start": 105,
                    "end": 110,
                    "text": "[RNA]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "identifying the viral RNA through nucleic acid amplification, usually using a polymerase chain reaction (PCR) [4] . The most commonly tested sample types are swabs taken from the nasopharynx (more sensitive) and/or oropharynx. Swabs are then placed into a liquid to release viral RNA into the solution and subsequently amplified using reverse transcription-PCR [4] . These tests are the gold standard for acute illness. Accuracy of the test is affected by the quality of the sample and RNA may degrade over time. Since SARS-CoV-2 can infect anyone and result in transmission prior to the onset of symptoms or possibly, even without individuals ever developing symptoms, aggressive testing/screening of asymptomatic patients has been considered [4] .",
            "cite_spans": [
                {
                    "start": 110,
                    "end": 113,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 361,
                    "end": 364,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 744,
                    "end": 747,
                    "text": "[4]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Nucleic acid amplification tests for viral RNA: Detection of the virus is achieved by"
        },
        {
            "text": "The other broad category of tests include those detecting IgM, IgA, IgG, or total antibodies [4] . Clinicians should note that SARS-CoV-2 is not typically present in blood. An infection causes white blood cells to make antibody proteins that help the immune system identify the viruses and stop them or mark infected cells for destruction [5] . Development of an antibody response to infection is host-dependent and takes time. In the case of SARS-CoV-2, studies suggest that most of the patients seroconvert in 7-11 days after exposure to the virus, although some patients may develop antibodies sooner. Therefore, antibody testing is not useful in the setting of an acute illness [4] . It is still unknown whether individuals infected with SARS-CoV-2 who subsequently recover will be J o u r n a l P r e -p r o o f protected, either fully or partially, from future infection with SARS-CoV-2 or for how long the protective immunity may last. It has been recently reported that the UK has ordered 3.5 million serological tests [6] . In the United States of America, on 2 April 2020, the Food and Drug Administration granted approval to a 'rapid' serological test made by Cellex\u00a9 that can allegedly generate results in as little as 15-minutes [7] .",
            "cite_spans": [
                {
                    "start": 93,
                    "end": 96,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 339,
                    "end": 342,
                    "text": "[5]",
                    "ref_id": "BIBREF8"
                },
                {
                    "start": 682,
                    "end": 685,
                    "text": "[4]",
                    "ref_id": null
                },
                {
                    "start": 1027,
                    "end": 1030,
                    "text": "[6]",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1242,
                    "end": 1245,
                    "text": "[7]",
                    "ref_id": "BIBREF10"
                }
            ],
            "ref_spans": [],
            "section": "Antibody detection via serology:"
        },
        {
            "text": "Serological tests are cheaper to run than PCR, which has an estimated cost of \u00a330/test. Serological tests can cost less than \u00a36/test. Recently, enzyme-linked immunosorbent assay kits based on recombinant SARS-CoV-2 nucleocapsid protein and spike protein were used for detecting IgM and IgG antibodies in China. They were reported to be cheap and had a high sensitivity [8] . days after the exposure. Fewer than 2.5% were symptomatic within 2.2 days and the vast majority were symptomatic within 14 days. The estimated median IT to fever was 5.7 days [9] . However, it is known that patients who remain asymptomatic (30% of exposed cases) or mildly symptomatic (56% of exposed cases) can transmit the infection and this group was not included in the aforementioned study [9] .",
            "cite_spans": [
                {
                    "start": 369,
                    "end": 372,
                    "text": "[8]",
                    "ref_id": null
                },
                {
                    "start": 550,
                    "end": 553,
                    "text": "[9]",
                    "ref_id": null
                },
                {
                    "start": 770,
                    "end": 773,
                    "text": "[9]",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "Antibody detection via serology:"
        },
        {
            "text": "Taking these facts into consideration, patients who are scheduled for surgery should always be assumed to be potential carriers of the virus throughout the duration of their hospital stay, even if they pass the pre-assessment triage including normal temperature, no history of exposure or travel, and no respiratory symptoms. Patients are 'screened' with the gold standard PCR test 24 hours before the surgery as well as with an antibody screen and then isolated in their rooms with no visitors allowed. This is later limited to one visitor for 15 minutes a day at a distance of 1 to 2 meters away, wearing appropriate protection such as a surgical mask and a gown. Obviously, if patients show positive result in the PCR test, they are isolated, the national COVID-19 public health protocols are followed (including protocols for exposed staff), and the surgery is postponed. If they show a negative result in the PCR, but are positive for the antibody, they will not require further testing during their hospital stay. If patients show negative results in PCR as well as in antibody tests, they are tested with PCR every week during their stay and always undergo an 'exit test PCR' on discharge ( Figure 1 ). Clinicians must be mindful that a negative test does not negate the possibility that an individual is infected.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 1198,
                    "end": 1206,
                    "text": "Figure 1",
                    "ref_id": "FIGREF1"
                }
            ],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "We are going through a rapidly changing situation that has not been experienced before. Not being aggressive with testing while carrying out surgical services could have catastrophic consequences. The aforementioned recommendations may be expensive, but they can mitigate the risks to patients, staff, and public. These tests, when carried out in all surgical units, can also be a part of a pandemic suppression campaign leveraged to move the current crisis closer to the ideal situation, especially in the absence of therapeutics or vaccines.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "J o u r n a l P r e -p r o o f"
        },
        {
            "text": "Ethics approval not required. Patient permission not applicable J o u r n a l P r e -p r o o f ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Ethics statement/confirmation of patient permission"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "COVID-19: Good Practice for Surgeons and Surgical Teams",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "WHO Director-General's opening remarks at the media briefing on",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "WHO",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Covid-19: What is the UK's testing strategy?",
            "authors": [
                {
                    "first": "G",
                    "middle": [],
                    "last": "Iacobucci",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "The BMJ",
            "volume": "",
            "issn": "368",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Report from the",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Smith",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Bertuzzi",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Value of Diagnostic Testing for SARS-CoV",
            "authors": [
                {
                    "first": "International",
                    "middle": [],
                    "last": "Summit",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Coronavirus test: Covid-19 immunity testing can help people get back to work",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Vox",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "The New Scientist",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Page",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Cellex qSARS-CoV-2 IgG/IgM Rapid Test",
            "authors": [],
            "year": null,
            "venue": "The Food and Drug",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Administration (FDA)",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Evaluation of Nucleocapsid and Spike Protein-based ELISAs for detecting antibodies against SARS-CoV-2",
            "authors": [
                {
                    "first": "Z",
                    "middle": [],
                    "last": "Tang",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Xiong",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Zheng",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Journal of Clinical Microbiology",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "The Incubation Period of Coronavirus Disease",
            "authors": [
                {
                    "first": "N",
                    "middle": [
                        "G"
                    ],
                    "last": "Azman",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Reich",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Lessler",
                    "suffix": ""
                }
            ],
            "year": 2019,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "COVID-19) From Publicly Reported Confirmed Cases: Estimation and Application",
            "authors": [],
            "year": 2020,
            "venue": "Ann Intern Med",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Recommendation for Patients Planned for Surgery: This possible guideline is be based on the known incubation period or incubation time (IT) of the SARS-CoV-2. In March 2020, Stephen A. Lauer et al. published the incubation period estimates from patients connected to the disease-origin province in China. The consensus was that the median IT of the virus was 5.1 days (mean IT: 5.5 days). In most of the patients who became symptomatic (approximately 97.5% of the symptomatic infected persons), symptoms appeared at 11 or 12",
            "latex": null,
            "type": "figure"
        },
        "FIGREF1": {
            "text": "Flowchart of Suggesting COVID-19 Testing for Planned Surgical Patients",
            "latex": null,
            "type": "figure"
        }
    },
    "back_matter": []
}