{
    "paper_id": "e2abe3677cfae339bf4d58606db52cec54936b98",
    "metadata": {
        "title": "The Era of Human-Induced Diseases",
        "authors": [
            {
                "first": "Anne-Lise",
                "middle": [],
                "last": "Chaber",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Wildlife Consultant L.L.C",
                    "location": {
                        "addrLine": "Falaj Hazaa",
                        "settlement": "Al Ain",
                        "country": "UAE"
                    }
                },
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "A global political and research infrastructure has grown around the fight against ''zoonoses,'' or diseases transmitted between vertebrates and humans. The One Health or Ecohealth approach and its integration of human, animal, and environmental health and the concomitant critique of the epistemological fragmentation of the Western scientific tradition can be applauded, but scientific and political focus needs to shift from zoonotic disease to Human-Induced Disease. Human-Induced Disease includes both infectious-i.e., zoonoses and disease from animal origin-and non-infectious anthropogenically driven diseases. Human-Induced Disease as the label for diseases-both infectious and non-infectious-caused by human activities and their environmental impact emphasizes the role of the human in disease transmission and could serve reshaping our approach to disease management and prevention.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Zoonotic diseases have been at the cornerstone of the One Health/Ecohealth approach. Zoonoses incurred an estimated $20 billion in direct costs and over $200 in indirect losses in the last 10 years (World Bank 2010). These events generate moral panic, legitimate new spheres of legal activity, and spur new arguments for the funding of research. The arsenal of disease-combatting weapons grows constantly, evidenced by mass immunization or eradication of animal reservoirs. This dynamic derives from an anthropocentric perception and the psychologically useful imagination of a species barrier.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Most viruses and bacteria are not pathogenic but symbiotic, with humans hosting some 100,000 billion bacteria compared to their own 10 billion cells. Bacteria have a vital role in human bodily function and are integral elements of human cells (mitochondrion), and viruses constitute at least 10% of human DNA. Humans share most of the viruses, bacteria, and fungus with the rest of the animal kingdom, and thus it should come as no surprise that zoonotic pathogens were the cause of more than 65% of emergent infectious disease events in the last 60 years, with 75% of these originating in wild fauna (Keusch et al. 2009 ). The biologically relevant question is what allows or prevents infection by microorganisms, whether or not the infected species is human.",
            "cite_spans": [
                {
                    "start": 601,
                    "end": 620,
                    "text": "(Keusch et al. 2009",
                    "ref_id": "BIBREF11"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The direct link between zoonosis and human activities and demographic growth is established. Land use modification for urbanization, food production, and agricultural change accounts for around 50% of all zoonotic Emerging Infectious Diseases (EIDs) (Keesing et al. 2010 ). Demographic, societal, and behavioral change gave rise to Human Immunodeficiency Virus (HIV/AIDS) (de Sousa et al. 2010 ) and outbreaks of syphilis (D' Angelo-Scott et al. 2015) . Anthropogenic environmental change leads to the emergence of infectious diseases in wildlife (Daszak et al. 2001) The advent of mass travel exacerbates the historically established dissemination of infectious disease along pathways of migration. Examples are bubonic plague (Yersinia pestis), cholera (Vibrio cholerae), seasonal influenza, Severe Acute Respiratory Syndrome (SARS), and malaria (Plas-modium falciparum) (Tatem et al. 2006) . The trade in goods and animals is directly linked to outbreaks such as human monkeypox virus in North America (Karesh et al. 2005) or H5N1 in the United Arab Emirates (Naguib et al. 2015) .",
            "cite_spans": [
                {
                    "start": 250,
                    "end": 270,
                    "text": "(Keesing et al. 2010",
                    "ref_id": "BIBREF10"
                },
                {
                    "start": 376,
                    "end": 393,
                    "text": "Sousa et al. 2010",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 426,
                    "end": 451,
                    "text": "Angelo-Scott et al. 2015)",
                    "ref_id": null
                },
                {
                    "start": 547,
                    "end": 567,
                    "text": "(Daszak et al. 2001)",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 873,
                    "end": 892,
                    "text": "(Tatem et al. 2006)",
                    "ref_id": "BIBREF16"
                },
                {
                    "start": 1005,
                    "end": 1025,
                    "text": "(Karesh et al. 2005)",
                    "ref_id": "BIBREF9"
                },
                {
                    "start": 1062,
                    "end": 1082,
                    "text": "(Naguib et al. 2015)",
                    "ref_id": "BIBREF13"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "It is nevertheless common that cause is attributed to the animal and not to human behavior as evidenced, for example, by the mass culling of badgers and the ibex to control tuberculosis or prevent brucellosis in Europe. This also evidences the diminished value to humans of wild animals and comes despite the Universal Declaration of Animal Rights (UNESCO 1978 and revised, 1989) which states that ''action endangering the survival of a wild species, and every decision leading to such act form a genocide, i.e. a crime against the species'' (Article 8). When human health is concerned, even hypothetically, such principles are quickly abandoned in the urge to displace consequence as cause.",
            "cite_spans": [
                {
                    "start": 348,
                    "end": 364,
                    "text": "(UNESCO 1978 and",
                    "ref_id": null
                },
                {
                    "start": 365,
                    "end": 379,
                    "text": "revised, 1989)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Is reducing the risk of transmission by tackling human activities amplifying disease transmission really out of our hands?",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Human-Induced Diseases do not only encompass infectious disease such as zoonoses whose spread is enhanced by human activities, but it also includes chronic disease linked to environmental perturbation caused by human activities. Environmental degradation is a sensitive topic since environmental health is a concept that is still lacking definition, consensus and true global liability and accountability.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The World Health Organization (WHO) estimates that ambient air pollution caused 3.7 million deaths throughout the world in 2012 (WHO and others 2014). The Organization for Economic Co-operation and Development (OECD) claims this death rate increased by 5% in China, 12% in India (OECD 2014), and 4% worldwide between 2005 and 2010. It estimated that the annual economic cost of illness and premature mortality linked to air pollution is $3600 billion (OECD 2014)-a figure that is 85% of the world's annual public budget for human health. The International Monetary Fund reported the use of fossil energies costs $4900 billion a year (Parry et al. 2014) in disease, premature death, and environmental damage. This figure is 1.2 times more than the combined public health budgets of 193 countries and yet industries that exploit fossil energies are subsidized directly and indirectly by more than $500 billion annually (iMFdirect-The IMF Blog 2016).",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Environmental degradation is estimated to be the cause of 40% of world mortality (Pimentel et al. 2007 ). Human activity gives rise to multiple forms of toxic pollution affecting both our health and the environment. For example, production of garments for international markets takes place mainly in developing countries and involves unregulated tannery operations that generate chromium pollutants known to have carcinogenic effects on both animals and humans. In 2013 industrial plants with poor waste treatment and disposal infrastructure were responsible for the lead pollution linked directly to the death from chronic illness of 853,000 people living mainly in low and middle income countries (Harris and McCartor 2011) . The Institute for Health Metrics and Evaluation also estimated that lead exposure accounted for 9.3% of the global burden of idiopathic intellectual disability, 4% of the global burden of ischemic heart disease, and 6.6% of the global burden of stroke. UNESCO's World Water Assessment Programme estimates that industry is responsible for the annual accumulation of 300-500 million tons of sludge, heavy metals, and other toxic wastes, and that 70% of untreated industrial waste in developing countries is dumped directly into water systems (United Nations Educational, Scientific and Cultural Organization 2016).",
            "cite_spans": [
                {
                    "start": 81,
                    "end": 102,
                    "text": "(Pimentel et al. 2007",
                    "ref_id": "BIBREF15"
                },
                {
                    "start": 699,
                    "end": 725,
                    "text": "(Harris and McCartor 2011)",
                    "ref_id": "BIBREF7"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "[For people of] poor countries, resource degradation is the most tragic, forced as they are to overuse natural resources from which depends their survival. They are driven to sacrifice their future to ensure precarious everyday life. That is why in many countries, acting against poverty includes protecting the environment.-UN Secretary General, 4th June 1992, Rio da Janeiro.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "A plethora of examples is already linking human diseases-both infectious and non-infectious-to environment perturbation in developed and developing countries. Concerns are also raised regarding the impact of climate change on human disease burden. Climate change is shifting ecological dynamics leading to potential increase in vector-borne disease such as Lyme disease (Brownstein et al. 2005) or malaria (Martens et al. 1995) , zoonotic diseases like hantavirus infection (Tian et al. 2017) , and non-infectious disease such as ciguatera fish poisoning (Gingold et al. 2014) .",
            "cite_spans": [
                {
                    "start": 370,
                    "end": 394,
                    "text": "(Brownstein et al. 2005)",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 406,
                    "end": 427,
                    "text": "(Martens et al. 1995)",
                    "ref_id": "BIBREF12"
                },
                {
                    "start": 474,
                    "end": 492,
                    "text": "(Tian et al. 2017)",
                    "ref_id": "BIBREF17"
                },
                {
                    "start": 555,
                    "end": 576,
                    "text": "(Gingold et al. 2014)",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Even though environmental health and human health are deeply connected, environmental health is difficult to protect without consensus on definitions, common objectives between beneficiaries, and long-term planning. Con-ventions of international environmental law introduced the concept of legal protection for species, sites, habitats and ecosystems. Three concepts of nature coexist: It is a Law project (UNESCO 2016) in which the common heritage dimension is underlined; it is a Legal subject (United Nations 2016); or it is a Law purpose (UNEP 2016)? The International Criminal Court announced in September 2016 that it will now prioritize crimes that result in the ''destruction of the environment'', ''exploitation of natural resources'', and the ''illegal dispossession'' of land (International Criminal Court 2016) . Yet legality and illegality are often controversial. Policies intending the liberalization of international trade do not take into account resource consumption, pollution, or negative societal impact in the producing and exporting countries (Pearson 2000) . Traded goods are not priced to incorporate such costs, and as such benefits of trade are distorted. A multitude of supranational environmental agencies, commissions, programs, and secretariats exist, but there is no global authority able to levy an appropriate pollution tax or fines on national governments or economic agents.",
            "cite_spans": [
                {
                    "start": 787,
                    "end": 822,
                    "text": "(International Criminal Court 2016)",
                    "ref_id": null
                },
                {
                    "start": 1066,
                    "end": 1080,
                    "text": "(Pearson 2000)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "It is becoming increasingly apparent to many that solutions and actions cannot be left to government or supranational government agencies. Consumers are boycotting products with negative social, environmental, and health impacts such as slavery in clothing factories, harvesting of exotic timber, or methylmercury contamination in seafood.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Environmental citizenship is a recent and theoretically complex concept (Bell 2005) . It is defined by the United Nations Environmental Programme as an ''attempt to make environmental conservation and sustainability an important duty of citizenship that citizens all over the world should be aware of'' (UNEP). Many multinational enterprises are now engaged in corporate citizenship programs to promote sustainable development, including the simultaneous consideration of economic growth, environmental protection, and social equity in business planning and decision-making. Yet the willingness and ability of governments to reflect environmental values of their citizens vary greatly among countries (Pearson 2000) .",
            "cite_spans": [
                {
                    "start": 72,
                    "end": 83,
                    "text": "(Bell 2005)",
                    "ref_id": null
                },
                {
                    "start": 701,
                    "end": 715,
                    "text": "(Pearson 2000)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The individual remains the fundamental element of society. How powerful can one individual be? The internet and its powerful networking effect is beyond control of established institutions. It creates opportunities for horizontal communication, develops new forms of democracy and social participation, and could bring people closer to having impact on issues of common concern such as health, social justice, and the environment (Barcena 1997) . Could billions of individuals, scattered through the world but connected via the internet, become the strongest and most active pillar of environmental protection and thus enhance health and improve social justice?",
            "cite_spans": [
                {
                    "start": 430,
                    "end": 444,
                    "text": "(Barcena 1997)",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The term Human-Induced Diseases might have the potential to put the human back into perspective and unify concerns and efforts. Human-Induced Disease as the label for diseases (infectious and non-infectious) caused by human activities emphasizes the role of human and could serve bringing together scientists, politicians, industrials and laymen in common pursuit. Human-Induced Diseases need to be named in order to be collectively claimed.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        }
    ],
    "bib_entries": {
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Effect of climate change on Lyme disease risk in North America",
            "authors": [
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Brownstein",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "R"
                    ],
                    "last": "Holford",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Fish",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "EcoHealth",
            "volume": "2",
            "issn": "",
            "pages": "38--46",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Anthropogenic environmental change and the emergence of infectious diseases in wildlife",
            "authors": [
                {
                    "first": "P",
                    "middle": [],
                    "last": "Daszak",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Cunningham",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Hyatt",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Acta Trop",
            "volume": "78",
            "issn": "",
            "pages": "103--116",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "High GUD incidence in the early 20th century created a particularly permissive time window for the origin and initial spread of epidemic HIV strains",
            "authors": [
                {
                    "first": "De",
                    "middle": [],
                    "last": "Sousa",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "D"
                    ],
                    "last": "M\u00fcller",
                    "suffix": ""
                },
                {
                    "first": "V",
                    "middle": [],
                    "last": "Lemey",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Vandamme",
                    "suffix": ""
                },
                {
                    "first": "A-M",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "PLoS ONE",
            "volume": "5",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Ciguatera fish poisoning and climate change: analysis of National Poison Center data in the United States",
            "authors": [
                {
                    "first": "D",
                    "middle": [
                        "B"
                    ],
                    "last": "Gingold",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "J"
                    ],
                    "last": "Strickland",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "J"
                    ],
                    "last": "Hess",
                    "suffix": ""
                }
            ],
            "year": 2001,
            "venue": "Environmental Health Perspectives",
            "volume": "122",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Global Energy Subsidies Are Big-About US$5 Trillion Big, iMFdirect-The IMF Blog",
            "authors": [],
            "year": 2016,
            "venue": "",
            "volume": "24",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "The World's Worst Toxic Pollution Problems Report",
            "authors": [
                {
                    "first": "J",
                    "middle": [],
                    "last": "Harris",
                    "suffix": ""
                },
                {
                    "first": "A",
                    "middle": [],
                    "last": "Mccartor",
                    "suffix": ""
                }
            ],
            "year": 2011,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "1--76",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "International Criminal Court, The Office of the Prosecutor (2016) Policy paper on case selection and prioritisation",
            "authors": [],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF9": {
            "ref_id": "b9",
            "title": "Wildlife trade and global disease emergence",
            "authors": [
                {
                    "first": "W",
                    "middle": [
                        "B"
                    ],
                    "last": "Karesh",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "A"
                    ],
                    "last": "Cook",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "L"
                    ],
                    "last": "Bennett",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Newcomb",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "Emerging Infectious Diseases",
            "volume": "11",
            "issn": "",
            "pages": "1000--1002",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Impacts of biodiversity on the emergence and transmission of infectious diseases",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Keesing",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "K"
                    ],
                    "last": "Belden",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Daszak",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Nature",
            "volume": "468",
            "issn": "",
            "pages": "647--652",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "Sustaining global surveillance and response to emerging zoonotic diseases",
            "authors": [
                {
                    "first": "G",
                    "middle": [
                        "T"
                    ],
                    "last": "Keusch",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Pappaioanou",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "C"
                    ],
                    "last": "Gonzalez",
                    "suffix": ""
                }
            ],
            "year": 2009,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF12": {
            "ref_id": "b12",
            "title": "Potential impact of global climate change on malaria risk",
            "authors": [
                {
                    "first": "W",
                    "middle": [],
                    "last": "Martens",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "W"
                    ],
                    "last": "Niessen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Rotmans",
                    "suffix": ""
                }
            ],
            "year": 1995,
            "venue": "Environmental Health Perspectives",
            "volume": "103",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF13": {
            "ref_id": "b13",
            "title": "Outbreaks of highly pathogenic avian influenza H5N1 clade 2.3. 2.1 c in hunting falcons and kept wild birds in Dubai implicate intercontinental virus spread",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "M"
                    ],
                    "last": "Naguib",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Kinne",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                }
            ],
            "year": 2015,
            "venue": "Journal of General Virology",
            "volume": "96",
            "issn": "",
            "pages": "3212--3222",
            "other_ids": {}
        },
        "BIBREF14": {
            "ref_id": "b14",
            "title": "The Cost of Air Pollution: Health Impacts of Road Transport",
            "authors": [
                {
                    "first": "M",
                    "middle": [
                        "D"
                    ],
                    "last": "Oecd ; Heine",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Lis",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2000,
            "venue": "Economics and the global environment",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF15": {
            "ref_id": "b15",
            "title": "Ecology of increasing diseases: population growth and environmental degradation",
            "authors": [
                {
                    "first": "D",
                    "middle": [],
                    "last": "Pimentel",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Cooperstein",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Randell",
                    "suffix": ""
                }
            ],
            "year": 2007,
            "venue": "Human Ecology",
            "volume": "35",
            "issn": "",
            "pages": "653--668",
            "other_ids": {}
        },
        "BIBREF16": {
            "ref_id": "b16",
            "title": "Global transport networks and infectious disease spread",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "J"
                    ],
                    "last": "Tatem",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "J"
                    ],
                    "last": "Rogers",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Hay",
                    "suffix": ""
                }
            ],
            "year": 2006,
            "venue": "Advances in Parasitology",
            "volume": "62",
            "issn": "",
            "pages": "293--343",
            "other_ids": {}
        },
        "BIBREF17": {
            "ref_id": "b17",
            "title": "Anthropogenically driven environmental changes shift the ecological dynamics of hemorrhagic fever with renal syndrome",
            "authors": [
                {
                    "first": "H",
                    "middle": [],
                    "last": "Tian",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Yu",
                    "suffix": ""
                },
                {
                    "first": "O",
                    "middle": [
                        "N"
                    ],
                    "last": "Bj\u00f8rnstad",
                    "suffix": ""
                }
            ],
            "year": 2017,
            "venue": "PLoS Pathog",
            "volume": "13",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF18": {
            "ref_id": "b18",
            "title": "WHO, others (WHO) (2014) Burden of disease from Ambient Air Pollution for",
            "authors": [],
            "year": 2012,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF19": {
            "ref_id": "b19",
            "title": "Towards a One Health Approach for Controlling Zoonotic Diseases",
            "authors": [
                {
                    "first": "Word",
                    "middle": [],
                    "last": "Bank",
                    "suffix": ""
                }
            ],
            "year": 2010,
            "venue": "Nutrition and Population World Water Development Report",
            "volume": "1",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF20": {
            "ref_id": "b20",
            "title": "UNESCO World Heritage Centre-Convention Concerning the Protection of the World Cultural and Natural Heritage",
            "authors": [],
            "year": 2016,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": [
        {
            "text": "I would like to acknowledge Dr. Fran\u00e7ois Moutou and Prof. Claude Saegerman for their advices and inspiring comments. My sincere thanks also go to Fleur Toulat, Anya Stafford, and Rachel Johnson for editing and polishing the English language.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ACKNOWLEDGEMENTS"
        }
    ]
}