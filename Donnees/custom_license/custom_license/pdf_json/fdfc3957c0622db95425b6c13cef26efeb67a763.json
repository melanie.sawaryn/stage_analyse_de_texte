{
    "paper_id": "fdfc3957c0622db95425b6c13cef26efeb67a763",
    "metadata": {
        "title": "Fighting COVID-19: Enabling Graduating Students to Start Internship Early at Their Own Medical School",
        "authors": [
            {
                "first": "Dawn",
                "middle": [
                    "E"
                ],
                "last": "Dewitt",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "tems affiliated with their medical school. First, medical schools could point students toward existing \"readiness for internship\" content. Many medical schools have a \"capstone\" course in the final weeks before graduation that prepares students for internship, with content geared toward meeting common patient care challenges. Most schools could deliver much of that content online now or via specific programs (3) (4) (5) . In addition, some commonly required certificate courses, such as Advanced Cardiac Life Support and Pediatric Advanced Life Support, can be completed online in less than a week (6) .",
            "cite_spans": [
                {
                    "start": 412,
                    "end": 415,
                    "text": "(3)",
                    "ref_id": null
                },
                {
                    "start": 416,
                    "end": 419,
                    "text": "(4)",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 420,
                    "end": 423,
                    "text": "(5)",
                    "ref_id": "BIBREF4"
                },
                {
                    "start": 602,
                    "end": 605,
                    "text": "(6)",
                    "ref_id": "BIBREF6"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The United States could provide loan repayment or other federal payment programs for any senior students willing (and competent, as judged by their medical school) to begin early. The average U.S. medical student graduates with approximately $200 000 of debt (7), so generous repayment programs would be welcome-and fitting-with potentially less bureaucracy than officially hiring students through health care systems short term. An alternative would be to pay the students a stipend equal to a tuition rebate plus the equivalent of a resident's salary funded by Medicare (8) .",
            "cite_spans": [
                {
                    "start": 572,
                    "end": 575,
                    "text": "(8)",
                    "ref_id": "BIBREF8"
                }
            ],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "Further, delays in transition to residency for this year's students due to chaos, credentialing, and other barriers might generate more problems for graduating students and short-staffed health care systems. We should urgently prepare these all-but-graduated students to help us address the looming workforce shortage as junior physicians during the next few weeks. However, they also should get credit for the experience they will gain and the service they will provide.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "The last big advantage of this plan is that the new junior interns would be working on home turf rather than adjusting to a different hospital or place, as happens for many interns who move across states, or across the country, to start in different health systems. Starting at their home institutions would vastly decrease credentialing and barriers to electronic health record access.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "We would have to rapidly address financial and logistic issues. Potential guarantees for loan repayment and tuition refunds would be key to success. Health profession schools would have to signal which students have the competency to begin working with more independence and agree to supervision requirements similar to those for residents. Supervision might be expanded to appropriate recently retired physicians or This article was published at Annals.org on 7 April 2020.",
            "cite_spans": [],
            "ref_spans": [],
            "section": ""
        },
        {
            "text": "those whose health risks due to COVID-19 make them unable to work on the front lines. Health systems would need to authorize access so that competent students could write orders and access electronic medical records from home. Graduate medical education (GME) leaders would need to discuss potentially giving participating students \"credit\" toward residency completion.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "IDEAS AND OPINIONS"
        },
        {
            "text": "These are bold but relatively straightforward requests, which I am certain academic medicine could tackle nationally in concert with GME leadership. Breaking down bureaucratic barriers must be a priority-a national effort could save many thousands of lives, not to mention being a substantial uplift for exhausted health care providers. Despite the logistic challenges, definitive and organized collective action now may give the United States an edge that we desperately need in this fight.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "IDEAS AND OPINIONS"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Italy rushes to promote new doctors to relieve coronavirus crisis",
            "authors": [
                {
                    "first": "A",
                    "middle": [],
                    "last": "Amante",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Balmer",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Accessed at www .ama-assn.org/delivering-care/public-health/covid-19-states-call-early -medical-school-grads-bolster-workforce on 5",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Murphy",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Accessed at www.wisemed .org/Wise-oncall",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "Nyu Langone Health",
                    "suffix": ""
                },
                {
                    "first": "",
                    "middle": [],
                    "last": "Wise-Oncall",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "The key role of a transition course in preparing medical students for internship",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "R"
                    ],
                    "last": "Teo",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [],
                    "last": "Harleman",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [
                        "S"
                    ],
                    "last": "O&apos;sullivan",
                    "suffix": ""
                }
            ],
            "year": null,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Advanced cardiovascular life support (ACLS) course options",
            "authors": [],
            "year": 2020,
            "venue": "American Heart Association",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "House of Representatives Committee on Small Business. The doctor is out. Rising student loan debt and the decline of the small medical practice",
            "authors": [
                {
                    "first": "U",
                    "middle": [
                        "S"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF8": {
            "ref_id": "b8",
            "title": "Medicare payments for graduate medical education: what every medical student, resident, and advisor needs to know. Accessed at www.aamc.org/data -reports/faculty-institutions/report/medicare-payments-graduate -medical-education-what-every-medical-student-resident-and -advisor",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": []
}