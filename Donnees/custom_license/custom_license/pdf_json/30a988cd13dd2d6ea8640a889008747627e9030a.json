{
    "paper_id": "30a988cd13dd2d6ea8640a889008747627e9030a",
    "metadata": {
        "title": "HUMAN CORONAVIRUS-NL63 INFECTION IS NOT ASSOCIATED WITH ACUTE KAWASAKI DISEASE",
        "authors": [
            {
                "first": "S",
                "middle": [
                    "C"
                ],
                "last": "Baker",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "C",
                "middle": [],
                "last": "Shimizu",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "H",
                "middle": [],
                "last": "Shike",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "F",
                "middle": [],
                "last": "Garcia",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "L",
                "middle": [],
                "last": "Van Der Hoek",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "T",
                "middle": [
                    "W"
                ],
                "last": "Kuijper",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "S",
                "middle": [
                    "L"
                ],
                "last": "Reed",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "A",
                "middle": [
                    "H"
                ],
                "last": "Rowley",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "S",
                "middle": [
                    "T"
                ],
                "last": "Shulman",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "H",
                "middle": [
                    "K B"
                ],
                "last": "Talbot",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J",
                "middle": [
                    "V"
                ],
                "last": "Williams",
                "suffix": "",
                "affiliation": {},
                "email": ""
            },
            {
                "first": "J",
                "middle": [
                    "C"
                ],
                "last": "Burns",
                "suffix": "",
                "affiliation": {},
                "email": ""
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "Two centers in the U.S. (Children's Hospital of San Diego, San Diego, CA; Children's Memorial Hospital, Chicago, IL) and one center in The Netherlands (Academic Medical Center (AMC), Amsterdam) collected respiratory samples from KD patients between December 2000 and March 2005. Seventy-seven percent of the samples were collected during the winter/spring months, which are seasons when HCoV is prevalent. Respiratory samples included throat swabs, nasopharyngeal (NP) swabs, scraped NP epithelial cells, and nasal washings were either archived or collected prospectively specifically for this study. Inclusion criteria for children with KD were more than five days of fever plus four of five standard clinical criteria (rash, conjunctival injection, cervical lymphadenopathy, changes in the extremities, changes in lips or oral mucosa) or three of five criteria with dilated coronary arteries by echocardiogram (z score > 2.5). The research protocol was reviewed and approved by the Institutional Review Boards of each institution. Informed consent was obtained from the parents of all subjects.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "HUMAN SUBJECTS, MATERIALS, AND METHODS"
        },
        {
            "text": "RT-PCR analysis was performed on RNA isolated from KD patient respiratory samples using primers and methods described in detail in Ref. 6 . Degenerate primers were included to detect HCoV-NL63 variants. Samples were also tested using primers for human cellular mRNA sequences such as beta actin, to ensure the quality of the extracted RNA. A schematic diagram of the HCoV-NL63 genome and relative position of the 18 different primers sets used in this study is shown in Figure 1 .",
            "cite_spans": [
                {
                    "start": 136,
                    "end": 137,
                    "text": "6",
                    "ref_id": null
                }
            ],
            "ref_spans": [
                {
                    "start": 470,
                    "end": 478,
                    "text": "Figure 1",
                    "ref_id": "FIGREF0"
                }
            ],
            "section": "HUMAN SUBJECTS, MATERIALS, AND METHODS"
        },
        {
            "text": "We tested a total of 57 samples from 48 KD patients and found that only one of the 48 KD patients (2%) was positive for HCoV-NL63 RNA (Table 1 ). This patient met 4 of 5 classic clinical criteria for KD, but also exhibited symptoms of an upper respiratory tract infection, with cough and coryza which are rare symptoms for KD but common symptoms for HCoV-NL63 infection. Furthermore, although this patient responded with complete defervescence after administration of intravenous gamma globulin and aspirin that are common treatments for KD, his respiratory symptoms persisted. These results suggest that this KD patient was likely co-infected with HCoV-NL63.",
            "cite_spans": [],
            "ref_spans": [
                {
                    "start": 134,
                    "end": 142,
                    "text": "(Table 1",
                    "ref_id": null
                }
            ],
            "section": "RESULTS AND DISCUSSION"
        },
        {
            "text": "Recent studies from Japan 7 and the Centers for Disease Control and Prevention in the USA 8 also report no association between infection with HCoV-NL63 and acute KD. Interestingly, a large study of pediatric patients in Europe found that HCoV-NL63 infection is associated with croup. 9 Thus, although HCoV-NL63 is likely a common respiratory infection in children, we and others found no association with acute KD.",
            "cite_spans": [
                {
                    "start": 284,
                    "end": 285,
                    "text": "9",
                    "ref_id": null
                }
            ],
            "ref_spans": [],
            "section": "RESULTS AND DISCUSSION"
        },
        {
            "text": "We found no association between the detection of HCoV-NL63 genome in the respiratory tract and acute KD. Future studies should continue to address the possibility of a microbe with a respiratory portal of entry as the etiologic agent of KD. b Samples analyzed at Northwestern University and Loyola University, one sample was also tested at Vanderbilt University. c Samples analyzed at Vanderbilt University, one sample was also tested at Northwestern University and Loyola University. d Primer sets A-C, N, O, R = primer sets from HCoV-NL63 nucleocapsid protein gene, D-F, M, and P with nested primer st Q = primer sets from HCoV-NL63 ORF1b; J = primer sets from HCoV-NH ORF1a; K with nested primer set L = primer sets from HCoV-NH spike glycoprotein gene. G-I = degenerate primer sets from conserved regions of the ORF1b shared by HCoV-NL63, severe acute respiratory syndrome (SARS)-CoV (NC_004718), HCoV-E229 (NC_002645), and HCoV-OC43 (NC_005147). e CAA = coronary artery abnormality, patients with aneurysms/ patients with dilatation (internal lumen z score>2.5. f Illness Day 1 = first day of fever.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "CONCLUSION AND FUTURE DIRECTIONS"
        },
        {
            "text": "We thank Drs. Ralph Baric, Michael Buchmeier, Benjamin Neuman, Ron Fouchier, and Christian Drosten for providing materials and technical advice. We also thank Joan Pancheri RN, UCSD, for assistance in collection of clinical samples. This work supported in part by grants from the National Institutes of Health (RO1 HL69413 to J.C.B., AI45798 to S.C.B., and HL63771 and HL67011 to A.H.R.). ",
            "cite_spans": [],
            "ref_spans": [],
            "section": "ACKNOWLEDGMENTS"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Kawasaki Syndrome, in: Krugman's Infectious Diseases of Children",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "H"
                    ],
                    "last": "Rowley",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "323--335",
            "other_ids": {}
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Detection of antigen in bronchial epithelium and macrophages in acute Kawasaki disease by use of synthetic antibody",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Kawasaki",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "B"
                    ],
                    "last": "Kalelkar",
                    "suffix": ""
                },
                {
                    "first": "Crawford",
                    "middle": [],
                    "last": "",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "E"
                    ],
                    "last": "",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "J. Infect. Dis",
            "volume": "190",
            "issn": "",
            "pages": "856--865",
            "other_ids": {}
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Cytoplasmic inclusion bodies are detected by synthetic antibody in acute between a novel human coronavirus and Kawasaki disease",
            "authors": [
                {
                    "first": "A",
                    "middle": [
                        "H"
                    ],
                    "last": "Rowley",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "C"
                    ],
                    "last": "Baker",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "T"
                    ],
                    "last": "Shulman",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Fox",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Takahashi",
                    "suffix": ""
                },
                {
                    "first": "F",
                    "middle": [
                        "L"
                    ],
                    "last": "Garcia",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "E"
                    ],
                    "last": "Crawford",
                    "suffix": ""
                },
                {
                    "first": "P",
                    "middle": [],
                    "last": "Chou",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "M"
                    ],
                    "last": "Orenstein",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Infect. Dis",
            "volume": "191",
            "issn": "",
            "pages": "499--502",
            "other_ids": {}
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Identification of a new human coronavirus",
            "authors": [
                {
                    "first": "P",
                    "middle": [
                        "M E"
                    ],
                    "last": "Wertheim-Van Dillen",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Kaandorp",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Spaargaren",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [],
                    "last": "Berkhout",
                    "suffix": ""
                }
            ],
            "year": 2004,
            "venue": "Nat. Med",
            "volume": "10",
            "issn": "",
            "pages": "368--373",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Lack of association between New Haven coronavirus and Kawasaki disease",
            "authors": [
                {
                    "first": "T",
                    "middle": [],
                    "last": "Ebihara",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [],
                    "last": "Endo",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Ma",
                    "suffix": ""
                },
                {
                    "first": "N",
                    "middle": [],
                    "last": "Ishiguro",
                    "suffix": ""
                },
                {
                    "first": "H",
                    "middle": [],
                    "last": "Kikuta",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Infect. Dis",
            "volume": "192",
            "issn": "",
            "pages": "351--352",
            "other_ids": {}
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Kawasaki disease and human coronavirus",
            "authors": [
                {
                    "first": "E",
                    "middle": [
                        "D"
                    ],
                    "last": "Belay",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [
                        "D"
                    ],
                    "last": "Erdman",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "J"
                    ],
                    "last": "Anderson",
                    "suffix": ""
                },
                {
                    "first": "T",
                    "middle": [
                        "C T"
                    ],
                    "last": "Peret",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [
                        "J"
                    ],
                    "last": "Schrag",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "S"
                    ],
                    "last": "Fields",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "C"
                    ],
                    "last": "Burns",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [
                        "B"
                    ],
                    "last": "Schonberger",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Infect. Dis",
            "volume": "192",
            "issn": "",
            "pages": "352--353",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Croup is associated with the novel coronavirus NL63",
            "authors": [
                {
                    "first": "B",
                    "middle": [],
                    "last": "Berkhout",
                    "suffix": ""
                },
                {
                    "first": "K",
                    "middle": [],
                    "last": "Uberla",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "",
            "volume": "2",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF10": {
            "ref_id": "b10",
            "title": "Association Kawasaki disease ciliated bronchial epithelium",
            "authors": [
                {
                    "first": "F",
                    "middle": [],
                    "last": "Esper",
                    "suffix": ""
                },
                {
                    "first": "E",
                    "middle": [
                        "D"
                    ],
                    "last": "Shapiro",
                    "suffix": ""
                },
                {
                    "first": "C",
                    "middle": [],
                    "last": "Weibel",
                    "suffix": ""
                },
                {
                    "first": "D",
                    "middle": [],
                    "last": "Ferguson",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [
                        "L"
                    ],
                    "last": "Landry",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [
                        "S"
                    ],
                    "last": "Kahn",
                    "suffix": ""
                }
            ],
            "year": 2005,
            "venue": "J. Infect. Dis",
            "volume": "192",
            "issn": "",
            "pages": "1757--1766",
            "other_ids": {}
        },
        "BIBREF11": {
            "ref_id": "b11",
            "title": "detected in the respiratory tract of children with acute Kawasaki disease",
            "authors": [],
            "year": null,
            "venue": "J. Infect. Dis",
            "volume": "192",
            "issn": "",
            "pages": "1767--1771",
            "other_ids": {}
        }
    },
    "ref_entries": {
        "FIGREF0": {
            "text": "Schematic diagram of the organization of HCoV-NL63 genome and the locations of primers used for RT-PCR analysis. Shaded box are open reading frames. Primer sets are shown by arrow sets A-R. [Reproduced with permission from Shimizu et al., \"Human coronavirus NL63 is not detected in the respiratory tract of Table 1. Results of HCoV RT-PCR on respiratory samples from acute Kawasaki disease patients. a a Reproduced with permission from Shimizu et al., \"Human coronavirus NL63 is not detected in the respiratory tract of children with acute Kawasaki disease\", The Journal of Infectious Diseases 2005; 192: (in press).",
            "latex": null,
            "type": "figure"
        },
        "TABREF0": {
            "text": "children with acute Kawasaki disease\", The Journal of Infectious Diseases 2005; 192: 1767-1771].",
            "latex": null,
            "type": "table"
        }
    },
    "back_matter": []
}