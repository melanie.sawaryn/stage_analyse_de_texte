{
    "paper_id": "d72c6f9e595258c0a98fca22b20ea01d9275118e",
    "metadata": {
        "title": "",
        "authors": [
            {
                "first": "Sheyu",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Sichuan University",
                    "location": {
                        "postCode": "610041",
                        "settlement": "Chengdu",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Zhiyong",
                "middle": [],
                "last": "Zong",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Sichuan University",
                    "location": {
                        "postCode": "610041",
                        "settlement": "Chengdu",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Xin",
                "middle": [],
                "last": "Sun",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Sichuan University",
                    "location": {
                        "postCode": "610041",
                        "settlement": "Chengdu",
                        "country": "China"
                    }
                },
                "email": ""
            },
            {
                "first": "Weimin",
                "middle": [],
                "last": "Li",
                "suffix": "",
                "affiliation": {
                    "laboratory": "",
                    "institution": "Sichuan University",
                    "location": {
                        "postCode": "610041",
                        "settlement": "Chengdu",
                        "country": "China"
                    }
                },
                "email": "weimi003@yahoo.com"
            }
        ]
    },
    "abstract": [],
    "body_text": [
        {
            "text": "The outbreak of the coronavirus disease 2019 (COVID-19, previously named 2019-nCoV) has become a serious public health situation in China. 1, 2 As of February 18, 2020, over 70 000 patients were diagnosed with COVID-19, including thousands of healthcare providers. Chinese hospitals, especially those in Hubei Province, are facing increasing admission of infected patients, and the epidemics involve hospitals across all levels, some of which may not be fully prepared. 3 Thus, there has been an urgent need for practical guidance to protect the health of both patients and healthcare professionals, the highest-risk people in the battle against the fatal virus infection. However, most existing documents were based on expert opinions and consensuses, calling for evidence-supported decision-making guidance.",
            "cite_spans": [
                {
                    "start": 139,
                    "end": 141,
                    "text": "1,",
                    "ref_id": "BIBREF0"
                },
                {
                    "start": 142,
                    "end": 143,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 470,
                    "end": 471,
                    "text": "3",
                    "ref_id": "BIBREF2"
                }
            ],
            "ref_spans": [],
            "section": "New evidence-based clinical practice guideline timely supports hospital infection control of coronavirus disease 2019"
        },
        {
            "text": "In response to this emergence, the West China Hospital (WCH) of Sichuan University initiated the urgent evidence-based guideline approach (i.e., West China Urgent Recommendations, English version shown as Supplementary Material # ) that aimed to offer timely recommendations for healthcare practitioners and institutes about the hospital disease control during the viral epidemics. 4 Based on the MAGIC (making GRADE the irresistible choice) evidence ecosystem, 5 this urgent guideline approach was adapted specifically for the present outbreak and future public health emergencies. It should be one of the major contributions of the MAGIC China Center since its establishment in April 2019 with the joint efforts of the MAGIC Evidence Ecosystem Foundation and WCH. 6 In developing the urgent recommendations of hospital infection control of COVID-19, we recruited guideline committee members including clinicians, hospital managers, methodologists and other practitioners. Seven critical questions were raised from first-line healthcare practitioners who participated in a focused, methodologist-mediated topic selection meeting. Prioritizing the public and then the patients, committee members answered these questions using least-torecommendation (LTR) evidence with the help of methodologists. With rigorous evidence-based approaches, the multidisciplinary team completed the urgent recommendations within one week.",
            "cite_spans": [
                {
                    "start": 382,
                    "end": 383,
                    "text": "4",
                    "ref_id": "BIBREF3"
                },
                {
                    "start": 766,
                    "end": 767,
                    "text": "6",
                    "ref_id": "BIBREF5"
                }
            ],
            "ref_spans": [],
            "section": "New evidence-based clinical practice guideline timely supports hospital infection control of coronavirus disease 2019"
        },
        {
            "text": "We recommended the hospital emergency incident command system (HICS), which should be the primary systematic response of the hospital. 2 Though required by the World Health Organization (WHO) as essential for hospitals to reflect rapidly and efficiently, HICS is in fact not widely adopted in primary and secondary hospitals in China. For guideline users who need detailed information about HICS, we here link the hospital emergency response checklist 7 and the hospital incident command system guidebook 8 in our document. We also recommended diagnostic tools, an early warning score recognising patients who may need intensive care, occupational protection protocols, in-hospital sterilization protocols and isolation protocols for hospital managers and practitioners to rearrange their hospital to deal with the crisis. 2 This new evidence-based guideline of hospital infection control for COVID-19 has provided timely guidance on what to do during the emergency. However, since little was known about the novel virus, only indirect evidence was available, which in turn increased the risk of bias for evidence selection in the LTR strategy. As a result, half of our recommendations are weak and systematic reviews will be involved in some of following guidance documents. The recommendations will be updated when necessary.",
            "cite_spans": [
                {
                    "start": 135,
                    "end": 136,
                    "text": "2",
                    "ref_id": "BIBREF1"
                },
                {
                    "start": 823,
                    "end": 824,
                    "text": "2",
                    "ref_id": "BIBREF1"
                }
            ],
            "ref_spans": [],
            "section": "New evidence-based clinical practice guideline timely supports hospital infection control of coronavirus disease 2019"
        },
        {
            "text": "Upon its publication, the urgent recommendations were spread widely, and have received substantial attention by media and hospital managers and practitioners. The main recommendations have been submitted to the authorities to assist in policy decision making. This new form of evidence-based guideline sets an example of urgent translating evidence into policy decision making during the public health crises.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "New evidence-based clinical practice guideline timely supports hospital infection control of coronavirus disease 2019"
        },
        {
            "text": "None declared.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Conflict of interests"
        }
    ],
    "bib_entries": {
        "BIBREF0": {
            "ref_id": "b0",
            "title": "Genomic characterisation and epidemiology of 2019 novel coronavirus: Implications for virus origins and receptor binding",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Lu",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Zhao",
                    "suffix": ""
                },
                {
                    "first": "J",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "",
            "issn": "",
            "pages": "30251--30259",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30251-8"
                ]
            }
        },
        "BIBREF1": {
            "ref_id": "b1",
            "title": "Epidemiological and clinical characteristics of 99 cases of 2019 novel coronavirus pneumonia in Wuhan, China: A descriptive study",
            "authors": [
                {
                    "first": "N",
                    "middle": [],
                    "last": "Chen",
                    "suffix": ""
                },
                {
                    "first": "M",
                    "middle": [],
                    "last": "Zhou",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Dong",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Lancet",
            "volume": "395",
            "issn": "",
            "pages": "30211--30218",
            "other_ids": {
                "DOI": [
                    "10.1016/S0140-6736(20)30211-7"
                ]
            }
        },
        "BIBREF2": {
            "ref_id": "b2",
            "title": "Building an evaluation instrument for China's hospital emergency preparedness: A systematic review of preparedness instruments",
            "authors": [
                {
                    "first": "R",
                    "middle": [],
                    "last": "Tang",
                    "suffix": ""
                },
                {
                    "first": "G",
                    "middle": [],
                    "last": "Fitzgerald",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [
                        "Y"
                    ],
                    "last": "Hou",
                    "suffix": ""
                }
            ],
            "year": 2014,
            "venue": "Disaster Med Public Health Prep",
            "volume": "8",
            "issn": "",
            "pages": "101--110",
            "other_ids": {
                "DOI": [
                    "10.1017/dmp.2014.10"
                ]
            }
        },
        "BIBREF3": {
            "ref_id": "b3",
            "title": "Disease control of 2019-novel coronavirus infection in hospital: West China urgent recommendation",
            "authors": [
                {
                    "first": "S",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "W",
                    "middle": [],
                    "last": "Huang",
                    "suffix": ""
                },
                {
                    "first": "X",
                    "middle": [],
                    "last": "Liao",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Chin J Evidence-based Med",
            "volume": "20",
            "issn": "",
            "pages": "125--158",
            "other_ids": {
                "DOI": [
                    "10.7507/1672-2531.202001121"
                ]
            }
        },
        "BIBREF4": {
            "ref_id": "b4",
            "title": "MAGIC -Trustworthy guidelines, evidence summaries and decision aids that we can all use and share",
            "authors": [],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF5": {
            "ref_id": "b5",
            "title": "Promoting the rapid creation and effective use of high-quality clinical practice guidelines: MAGIC system and China initiative",
            "authors": [
                {
                    "first": "X",
                    "middle": [],
                    "last": "Sun",
                    "suffix": ""
                },
                {
                    "first": "L",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                },
                {
                    "first": "S",
                    "middle": [],
                    "last": "Li",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Chin J Evidence-based Med",
            "volume": "20",
            "issn": "",
            "pages": "2--6",
            "other_ids": {
                "DOI": [
                    "10.7507/1672-2531.201909110"
                ]
            }
        },
        "BIBREF6": {
            "ref_id": "b6",
            "title": "Hospital emergency response checklist: An all-hazards tool for hospital administrators and emergency managers",
            "authors": [
                {
                    "first": "B",
                    "middle": [
                        "S"
                    ],
                    "last": "Sorensen",
                    "suffix": ""
                },
                {
                    "first": "R",
                    "middle": [
                        "D"
                    ],
                    "last": "Zane",
                    "suffix": ""
                },
                {
                    "first": "B",
                    "middle": [
                        "E"
                    ],
                    "last": "Wante",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        },
        "BIBREF7": {
            "ref_id": "b7",
            "title": "Hospital incident command system guidebook",
            "authors": [
                {
                    "first": "",
                    "middle": [],
                    "last": "California",
                    "suffix": ""
                }
            ],
            "year": 2020,
            "venue": "Emergency Medical Services Authority",
            "volume": "",
            "issn": "",
            "pages": "",
            "other_ids": {}
        }
    },
    "ref_entries": {},
    "back_matter": [
        {
            "text": "We thank Dr. Heyue Du from the Department of Nephrology, West China Hospital, Sichuan University and Ms. Yiling Zhou from the Department of Endocrinology and Metabolism, West China Hospital, Sichuan University for translating the Chinese version of the recommendation into English.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Acknowledgments"
        },
        {
            "text": "Supplementary material is available in Precision Clinical Medicine online at https://doi.org/10.1093/pcmedi/ pbaa008.",
            "cite_spans": [],
            "ref_spans": [],
            "section": "Supplementary material"
        }
    ]
}