import json
import pandas as pd
from os import listdir
from os.path import join
from tqdm.autonotebook import tqdm
import re
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import pickle
from os.path import join

english_stop_words = stopwords.words('english')

def load_author(author_dict):
    return " ".join(clean_txt(author_dict["first"]+" "+author_dict["last"]))

def load_authors(authors_list):
    return ", ".join([load_author(author) for author in authors_list])

def load_institutions(authors_list):
    txt = []
    for author in authors_list:
        if "institution" in author["affiliation"]:
            txt.append(" ".join(clean_txt(author["affiliation"]["institution"])))
    return ", ".join(list(set(txt)))

def load_countries(authors_list):
    txt = []
    for author in authors_list:
        if "location" in author["affiliation"]:
            if "country" in author["affiliation"]["location"]:
                txt.append(" ".join(clean_txt(author["affiliation"]["location"]["country"])))
    return ", ".join(list(set(txt)))

def load_text(path):
    dico = json.load(open(path,"r"))
    txt = {}
    txt["title"] = " ".join(clean_txt(dico["metadata"]["title"]))
    txt["abstract"] = ""
    txt["paper_id"] = dico["paper_id"]
    for e in dico["abstract"]:
        txt["abstract"] +=  e["text"]
    txt["body"] = ""
    for e in dico["body_text"]:
        txt["body"] +=  e["text"]
    txt["country"] = load_countries(dico["metadata"]["authors"])
    txt["institution"] = load_institutions(dico["metadata"]["authors"])
    txt["authors"] = load_authors(dico["metadata"]["authors"])
    return(pd.Series(txt))

def load_database(path):
    files = [f for f in listdir(path) if not f.startswith('.')]
    txts = []
    for f in tqdm(files):
        txts.append(load_text(join(path,f)))
    return(pd.DataFrame(txts))

def clean_txt(txt,stopwords=[]):
    """
    Keep only letters and remove stop words
    """
    txt_token = word_tokenize(txt)
    s = [re.sub(r'[^a-zA-Z]+', '', w).lower() for w in txt_token]
    s = [w for w in s if w not in stopwords]
    return(s)

def contexts_union(it, words, sep=" "):
    contexts = []
    ids = set()
    for w in tqdm(words):
        context = it.word_context_extractor(w, sep)
        context.index = context["paper"].astype(str)+":"+context["row"].astype(str)
        local_ids = set(context.index)
        new_ids = local_ids.difference(ids)
        contexts.append(context.loc[new_ids])
        ids = ids.union(new_ids)
    contexts = pd.concat(contexts)
    return(contexts)

def contexts_intersection(it, words, sep=" "):
    contexts = []
    ids = None
    for w in tqdm(words):
        context = it.word_context_extractor(w, sep)
        context.index = context["paper"].astype(str)+":"+context["row"].astype(str)
        local_ids = set(context.index)
        contexts.append(context)
        if ids is not None:
            ids = ids.intersection(local_ids)
        else:
            ids = local_ids
    contexts = contexts[0].loc[ids]
    return(contexts)

class IndexerTokenizer:
    def __init__(self, stopwords=[], sep_words=None, sep_sentences=None):
        self.stopwords = stopwords
        self.sep_words = sep_words
        self.sep_sentences = sep_sentences
        self.vocabulary = {}
        self.sentences_clean = {}

    def clean_txt(self,txt):
        """
        Keep only letters and remove stop words
        """
        txt_token = word_tokenize(txt)
        s = [re.sub(r'[^a-zA-Z]+', '', w).lower() for w in txt_token]
        s = [w for w in s if w not in self.stopwords]
        return(s)

    def update_vocab(self, w, txt_id, sentence_id):
        """
        Update vocabulary by including the word as key, the text_id as second key
        and the sentence row in a list: word -> text ids -> rows
        """
        if w not in self.vocabulary:
            self.vocabulary[w] = {}
        if txt_id not in self.vocabulary[w]:
            self.vocabulary[w][txt_id] = []
        self.vocabulary[w][txt_id].append(sentence_id)

    def tokenize_text(self,txt,txt_id):
        """
        Tokenize sentences, then clean each sentence calling clean_txt, update
        the vocabulary
        """
        sentences = sent_tokenize(txt,language='english')
        self.sentences_clean[txt_id] = []
        i=0
        for sentence in sentences:
            clean_words = self.clean_txt(sentence)
            if len(clean_words)>1:
                for w in clean_words:
                    self.update_vocab(w,txt_id,i)
                if type(self.sep_words) == str:
                    clean_words = self.sep_words.join(clean_words)
                self.sentences_clean[txt_id].append(clean_words)
                i += 1
        if type(self.sep_sentences)==str and type(self.sep_words)==str:
            sent = self.sep_sentences.join(self.sentences_clean[txt_id])
            self.sentences_clean[txt_id] = sent

    def tokenize_dataset(self,df):
        """
        Tokenize each document stored in a dataframe
        """
        dico = df.T.to_dict()
        for txt_id in tqdm(dico):
            txt = dico[txt_id]['title']+". "+dico[txt_id]['abstract']+". "+dico[txt_id]['body']
            self.tokenize_text(txt,txt_id)

    def word_context_extractor(self,w, sep=None):
        """
        Return all the sentences where a word was used
        """
        contexts = {"text":[],"paper":[],"row":[]}
        if w in self.vocabulary:
            for k in self.vocabulary[w]:
                contexts_k = {i:self.sentences_clean[k][i] for i in self.vocabulary[w][k]}
                for i in contexts_k:
                    sent = contexts_k[i]
                    s = [e for e in sent if len(e)]
                    if len(s):
                        if type(sep)==str:
                            s = sep.join(s)
                        contexts["text"].append(s)
                        contexts["paper"].append(k)
                        contexts["row"].append(i)
        contexts = pd.DataFrame(contexts)
        return(contexts)

    def save(self,folder,tag):
        """
        Save the object using pickle
        """
        pickle.dump(obj=self,
        file = open(join(folder,tag+"IndexerTokenizer.pickle"),"wb"))

    def load(self,folder,tag):
        """
        Load the object using pickle
        """
        return pickle.load(open(join(folder,tag+"IndexerTokenizer.pickle"),"rb"))
