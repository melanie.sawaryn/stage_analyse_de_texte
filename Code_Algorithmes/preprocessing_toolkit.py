import json
import pandas as pd
from os import listdir
from os.path import join
from tqdm.autonotebook import tqdm
import re
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import pickle
from os.path import join

english_stop_words = stopwords.words('english')

def load_author(author_dict):
    return " ".join(clean_txt(author_dict["first"]+" "+author_dict["last"]))

def load_authors(authors_list):
    return ", ".join([load_author(author) for author in authors_list])

def load_institutions(authors_list):
    txt = []
    for author in authors_list:
        if "institution" in author["affiliation"]:
            txt.append(" ".join(clean_txt(author["affiliation"]["institution"])))
    return ", ".join(list(set(txt)))

def load_countries(authors_list):
    txt = []
    for author in authors_list:
        if "location" in author["affiliation"]:
            if "country" in author["affiliation"]["location"]:
                txt.append(" ".join(clean_txt(author["affiliation"]["location"]["country"])))
    return ", ".join(list(set(txt)))

def load_text(path):
    dico = json.load(open(path,"r"))

    # Creation du stockage général
    Pandas_list=[]
    compteur=0
    
    # Recuperation de l'abstract
    if "abstract" in dico:
        txt = {}
        compteur+=1
        txt["section"] = "abstract"
        txt["body-section-text"]=""
        for e in dico["abstract"]:
            txt["body-section-text"] +=  e["text"]

        txt["title"] = " ".join(clean_txt(dico["metadata"]["title"]))
        txt["paper_id"] = dico["paper_id"]
        txt["parties"] = compteur

        txt["country"] = load_countries(dico["metadata"]["authors"])
        txt["institution"] = load_institutions(dico["metadata"]["authors"])
        txt["authors"] = load_authors(dico["metadata"]["authors"])
        txt["Identifiant_unique"] = dico["paper_id"]+"_"+str(compteur)+"_"+"abstract"


        Pandas_list.append(pd.Series(txt))
    
    sect=[]
    # Recuperation des sections
    for e in dico["body_text"]:
        if e["section"] in sect:
            a=1
        else:
            sect.append(e["section"])
    for sec in sect:
        txt = {}
        compteur+=1
        txt["section"] = sec
        txt["body-section-text"]=""
        for e in dico["body_text"]:
            if e["section"]==sec:
                txt["body-section-text"] += e["text"]

        txt["title"] = " ".join(clean_txt(dico["metadata"]["title"]))
        txt["paper_id"] = dico["paper_id"]
        txt["parties"] = compteur

        txt["country"] = load_countries(dico["metadata"]["authors"])
        txt["institution"] = load_institutions(dico["metadata"]["authors"])
        txt["authors"] = load_authors(dico["metadata"]["authors"])
        txt["Identifiant_unique"] = str(dico["paper_id"])+"_"+str(compteur)+"_"+str(txt["section"])

        Pandas_list.append(pd.Series(txt))

    # Ajout du texte des tableaux et figures
    if "ref_entries" in dico:
        comp_image=0
        for e in dico["ref_entries"].values():
            txt = {}
            compteur+=1
            comp_image+=1
            txt["section"] = e["type"]+"_"+str(comp_image)
            txt["body-section-text"] = e["text"]

            txt["title"] = " ".join(clean_txt(dico["metadata"]["title"]))
            txt["paper_id"] = dico["paper_id"]
            txt["parties"] = compteur

            txt["country"] = load_countries(dico["metadata"]["authors"])
            txt["institution"] = load_institutions(dico["metadata"]["authors"])
            txt["authors"] = load_authors(dico["metadata"]["authors"])
            txt["Identifiant_unique"] = dico["paper_id"]+"_"+str(compteur)+"_"+e["type"]+str(comp_image)

            Pandas_list.append(pd.Series(txt))
       
     # Ajout de la section back_matter
        
    if "back_matter" in dico:
        txt = {}
        compteur+=1
        txt["section"] = "back_matter"
        txt["body-section-text"]=""
        for e in dico["back_matter"]:
            txt["body-section-text"] +=  e["text"]

        txt["title"] = " ".join(clean_txt(dico["metadata"]["title"]))
        txt["paper_id"] = dico["paper_id"]
        txt["parties"] = compteur

        txt["country"] = load_countries(dico["metadata"]["authors"])
        txt["institution"] = load_institutions(dico["metadata"]["authors"])
        txt["authors"] = load_authors(dico["metadata"]["authors"])
        txt["Identifiant_unique"] = dico["paper_id"]+"_"+str(compteur)+"_"+"back_matter"


        Pandas_list.append(pd.Series(txt))


    

    return(Pandas_list)

def load_database(path):
    files = [f for f in listdir(path) if not f.startswith('.')]
    txts = []
    for f in tqdm(files):
        for i in load_text(join(path,f)):
            txts.append(i)
    return(pd.DataFrame(txts))

def clean_txt(txt,stopwords=[]):
    """
    Keep only letters and remove stop words
    """
    txt_token = word_tokenize(txt)
    # Passage en minuscules en utilisant isalnum
    s =[ w.lower() for w in txt_token if w.isalnum()]
    # Enleve ceux qui ne sont que des nombres
    s =[ w for w in s if not w.isnumeric()]
    s = [w for w in s if w not in stopwords]
    return(s)

