# Text Analysis
This project corresponds to my internship in the summer of 2020. It was carried out by teleworking with my tutor Sergio Peignier. The goal is to work on the algorithmic of the text using scientific articles on the Covid19.

<div style="display: flex; justify-content: center;">
 <img src="Images/Apriori_2.png" height="250">
</div>

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com) 

## IMPORTATION
To carry out this project, you must have access to:
* python 3 
* pandas for dataframe
* nltk : for NLP
* numpy
* spacy : for NLP
* ipywigdets : for jupyter
* re : for regular expressions
* pickle : for the storage of files, lists 
* Collections: for extraction and counter
* mlxtend : for use of apriori
* networkx : graph representation
* marshall : storage of lists in txt.

Use command ``pip install package``
Like: python -m spacy download en

### Tutorials:
No tutorials at the moment but there are already some notebooks if needed! 

### For more information: 
To access the report and follow along the way
[Overleaf]https://www.overleaf.com/4222228986vwtrdbhpybjz

### TODO:
A lot ! :)

### Authors:

+ Mélanie Sawaryn: melanie.sawaryn @ insa-lyon.fr

### Tutor:

+ Sergio Peignier: sergio.peignier @ insa-lyon.fr

